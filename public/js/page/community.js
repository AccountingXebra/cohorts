$(document).ready(function() {
	eventDatatable(base_path()+'community/get_event_details','event_table');
	$('select').material_select();
	coworkingDatatable(base_path()+'community/get_coworking_details','cowork_table');
	$('select').material_select();
	incubatorDatatable(base_path()+'community/get_incubator_details','incubator_table');
	$('select').material_select();

		$("#cowork_name").on('change',function() { if($(this).val()!=''){ coworkingDatatable(base_path()+'community/get_coworking_details','cowork_table');
	$('select').material_select();} } );

	$("#cowork_country").on('change',function() { if($(this).val()!=''){ coworkingDatatable(base_path()+'community/get_coworking_details','cowork_table');
	$('select').material_select();} } );

	$("#cowork_city").on('change',function() { if($(this).val()!=''){ coworkingDatatable(base_path()+'community/get_coworking_details','cowork_table');
	$('select').material_select();} } );

	$("#incub_name").on('change',function() { if($(this).val()!=''){ incubatorDatatable(base_path()+'community/get_incubator_details','incubator_table');
	$('select').material_select();
} } );

	$("#incub_country").on('change',function() { if($(this).val()!=''){ incubatorDatatable(base_path()+'community/get_incubator_details','incubator_table');
	$('select').material_select();
} } );

	$("#incub_city").on('change',function() { if($(this).val()!=''){ incubatorDatatable(base_path()+'community/get_incubator_details','incubator_table');
	$('select').material_select();
} } );
	
$("#search").on('keyup',function(){
eventDatatable(base_path()+'community/get_event_details','event_table');
$('select').material_select();
});

$("#location").on('change',function(){
eventDatatable(base_path()+'community/get_event_details','event_table');
$('select').material_select();
});

$("#nature").on('change',function(){
eventDatatable(base_path()+'community/get_event_details','event_table');
$('select').material_select();
});
$("#c_start_date").on('change',function(){
eventDatatable(base_path()+'community/get_event_details','event_table');
$('select').material_select();
});
$("#c_end_date").on('change',function(){
eventDatatable(base_path()+'community/get_event_details','event_table');
$('select').material_select();
});

$("#reset_event").on('click',function(){
	$('.action-btn-wapper').find('select').prop('selectedIndex',0);
			$('.js-example-basic-single').trigger('change.select2');
			$('.btn-date,.search-hide-show').val('');
eventDatatable(base_path()+'community/get_event_details','event_table');
$('select').material_select();
});

	dealDatatable(base_path()+'community/get_deal_details','deal_table');
	$('select').material_select();


$("#search_offer").on('keyup',function(){
dealDatatable(base_path()+'community/get_deal_details','deal_table');
$('select').material_select();
});

$("#offer_category").on('change',function(){
dealDatatable(base_path()+'community/get_deal_details','deal_table');
$('select').material_select();
});

$("#offer_company").on('change',function(){
dealDatatable(base_path()+'community/get_deal_details','deal_table');
$('select').material_select();
});
$("#offer_start_date").on('change',function(){
dealDatatable(base_path()+'community/get_deal_details','deal_table');
$('select').material_select();
});
$("#offer_end_date").on('change',function(){
	dealDatatable(base_path()+'community/get_deal_details','deal_table');
	$('select').material_select();
});

$("#reset_offer").on('click',function(){
	$('.action-btn-wapper').find('select').prop('selectedIndex',0);
			$('.js-example-basic-single').trigger('change.select2');
			$('.btn-date,.search-hide-show').val('');
dealDatatable(base_path()+'community/get_deal_details','deal_table');
$('select').material_select();
});






	$(function(){
	  $("#add_new_offer").submit(function(e){
	  	e.preventDefault();	
	  	}).validate({
	    rules: {

	      offer_title: {
	        required: true,
	      },
	      offer_category: {
	        required: true,
	      },
	      start_date: {
	        required: true,
	      },
	      end_date: {
	        required: true,
	      },
	      company_name: {
	        required: true,
	      },
	      contact_name: {
	        required: true,
	      },
	      email: {
	        required: true,
	      },
	      mobile_no: {
	        required: true,
	      },
	     
		},

	    messages: {

	      offer_title:{
	        required:"Offer Title is required",
	      },
	      offer_category: {
	        required: "Offer Category is required",
	      },
	      start_date: {
	        required: "Start date is required",
	      },
	      end_date: {
	        required: "End date is required",
	      },
	      company_name: {
	        required: "Company Name is required",
	      },
	      contact_name: {
	        required: "Contact Name is required",
	      },
	      email: {
	        required: "Email is required",
	      },
	      mobile_no: {
	        required: "Mobile number is required",
	      },
	  	    },

	    errorPlacement: function(error, element) {
	      
	      if(element.prop('tagName')  == 'SELECT') {
			          $('#offer_error').html(error);
					  error.insertAfter(element.closest('.input-field').find('.js-example-basic-single'));
					}else{
			           error.insertAfter(element);
			          }
	    },

	    submitHandler: function(form) {
			$("input[name='csrf_test_name']").val(csrf_hash);
	    	form.submit();
	    /*
	    var offer_number 		= $('#offer_number').val();
		var offer_date 			= $('#offer_date').val();
		var offer_title 		= $('#offer_title').val();
		//var offer_image		= $('#offer_image').val();
		var offer_description 	= $('#offer_description').val();
		var offer_category 		= $('#offer_category').val();
		var promo_code			= $('#promo_code').val();
		var website_link		= $('#website_link').val();
		var start_date			= $('#start_date').val();
		var end_date			= $('#end_date').val();
		var start_time			= $('#start_time').val();
		var end_time			= $('#end_time').val();
		var company_name		= $('#company_name').val();
		var contact_name		= $('#contact_name').val();
		var email				= $('#email').val();
		var mobile_no			= $('#mobile_no').val();
		var files				= $('#offer_image').prop('files')[0];

		var fd 					= new FormData();
		fd.append('offer_number', offer_number);
		fd.append('offer_date', offer_date);
		fd.append('offer_title', offer_title);
		fd.append('offer_image', files);
		fd.append('offer_description', offer_description);
		fd.append('offer_category', offer_category);
		fd.append('promo_code', promo_code);
		fd.append('website_link', website_link);
		fd.append('start_date', start_date);
		fd.append('end_date', end_date);
		fd.append('start_time', start_time);
		fd.append('end_time', end_time);
		fd.append('company_name', company_name);
		fd.append('contact_name', contact_name);
		fd.append('email', email);
		fd.append('mobile_no', mobile_no);

		$.ajax({


			type: "POST",
			contentType: false,
			processData: false,
			url:base_url+'Community/add_offer_db',

			//data:{'offer_number':offer_number, 'offer_date':offer_date, 'offer_title':offer_title, 'offer_image':files, 'offer_description':offer_description, 'offer_category':offer_category, 'promo_code':promo_code, 'website_link':website_link, 'start_date':start_date, 'end_date': end_date, 'start_time':start_time, 'end_time':end_time, 'company_name':company_name, 'contact_name':contact_name, 'email':email, 'mobile_no':mobile_no},

			data: fd,

			success: function(data){

				$('#offer_number').val('');
				$('#offer_date').val('');
				$('#offer_title').val('');
				$('#offer_image').val('');
				$('#offer_description').val('');
				$('#offer_category').val('');
				$('#promo_code').val('');
				$('#website_link').val('');
				$('#start_date').val('');
				$('#end_date').val('');
				$('#start_time').val('');
				$('#end_time').val('');
				$('#company_name').val('');
				$('#contact_name').val('');
				$('#email').val('');
				$('#mobile_no').val('');

				Materialize.toast('New offer added successfully', 4000,'green rounded');
				location.href=base_url+'community/offers';

			}
		});
		*/

	    
	    },

	  });
	});

	$(function(){
		
	  $("#edit_offer").submit(function(e){
	  	e.preventDefault();	
	  	}).validate({
	    rules: {

	      offer_title: {
	        required: true,
	      },
	      offer_category: {
	        required: true,
	      },
	      start_date: {
	        required: true,
	      },
	      end_date: {
	        required: true,
	      },
	      company_name: {
	        required: true,
	      },
	      contact_name: {
	        required: true,
	      },
	      email: {
	        required: true,
	      },
	      mobile_no: {
	        required: true,
	      },
	     
		},

	    messages: {

	      offer_title:{
	        required:"Offer Title is required",
	      },
	      offer_category: {
	        required: "Offer Category is required",
	      },
	      start_date: {
	        required: "Start date is required",
	      },
	      end_date: {
	        required: "End date is required",
	      },
	      company_name: {
	        required: "Company Name is required",
	      },
	      contact_name: {
	        required: "Contact Name is required",
	      },
	      email: {
	        required: "Email is required",
	      },
	      mobile_no: {
	        required: "Mobile number is required",
	      },
	  	    },

	    errorPlacement: function(error, element) {
	      
	      if(element.prop('tagName')  == 'SELECT') {
			          $('#offer_error').html(error);
					  error.insertAfter(element.closest('.input-field').find('.js-example-basic-single'));
					}else{
			           error.insertAfter(element);
			          }
	    },

	    submitHandler: function(form) {
			$("input[name='csrf_test_name']").val(csrf_hash);
	    	form.submit();

	    	/*
	    
	    var offer_number 		= $('#offer_number').val();
		var offer_date 			= $('#offer_date').val();
		var offer_title 		= $('#offer_title').val();
		//var offer_image		= $('#offer_image').val();
		var offer_description 	= $('#offer_description').val();
		var offer_category 		= $('#offer_category').val();
		var promo_code			= $('#promo_code').val();
		var website_link		= $('#website_link').val();
		var start_date			= $('#start_date').val();
		var end_date			= $('#end_date').val();
		var start_time			= $('#start_time').val();
		var end_time			= $('#end_time').val();
		var company_name		= $('#company_name').val();
		var contact_name		= $('#contact_name').val();
		var email				= $('#email').val();
		var mobile_no			= $('#mobile_no').val();
		var files				= $('#offer_image').prop('files')[0];

		var fd 					= new FormData();
		fd.append('offer_number', offer_number);
		fd.append('offer_date', offer_date);
		fd.append('offer_title', offer_title);
		fd.append('offer_image', files);
		fd.append('offer_description', offer_description);
		fd.append('offer_category', offer_category);
		fd.append('promo_code', promo_code);
		fd.append('website_link', website_link);
		fd.append('start_date', start_date);
		fd.append('end_date', end_date);
		fd.append('start_time', start_time);
		fd.append('end_time', end_time);
		fd.append('company_name', company_name);
		fd.append('contact_name', contact_name);
		fd.append('email', email);
		fd.append('mobile_no', mobile_no);

		$.ajax({


			type: "POST",
			contentType: false,
			processData: false,
			url:base_url+'Community/edit_deals',

			//data:{'offer_number':offer_number, 'offer_date':offer_date, 'offer_title':offer_title, 'offer_image':files, 'offer_description':offer_description, 'offer_category':offer_category, 'promo_code':promo_code, 'website_link':website_link, 'start_date':start_date, 'end_date': end_date, 'start_time':start_time, 'end_time':end_time, 'company_name':company_name, 'contact_name':contact_name, 'email':email, 'mobile_no':mobile_no},

			data: fd,

			success: function(data){

				$('#offer_number').val('');
				$('#offer_date').val('');
				$('#offer_title').val('');
				$('#offer_image').val('');
				$('#offer_description').val('');
				$('#offer_category').val('');
				$('#promo_code').val('');
				$('#website_link').val('');
				$('#start_date').val('');
				$('#end_date').val('');
				$('#start_time').val('');
				$('#end_time').val('');
				$('#company_name').val('');
				$('#contact_name').val('');
				$('#email').val('');
				$('#mobile_no').val('');

				Materialize.toast('Offer updated successfully', 4000,'green rounded');
				location.href=base_url+'community/offers';

			}
		});
*/
	    
	    },

	  });
	});


	$(function(){
		
	  $("#add_new_event").submit(function(e){
	  	e.preventDefault();	
	  	}).validate({

	    rules: {

	      event_name: {
	        required: true,
	      },
	      event_address: {
	        required: true,
	      },
	      event_city: {
	        required: true,
	      },
	      event_nature: {
	        required: true,
	      },
	      start_date: {
	      	required: true,
	      },
	      end_date: {
	      	required: true,
	      },
	      contact_name: {
	        required: true,
	      },
	      email: {
	        required: true,
	      },
	      mobile_no: {
	        required: true,
	      },
		},

	    messages: {

	      event_name:{
	        required:"Event Name is required",
	      },
	      event_address:{
	        required:"Event Address is required",
	      },
	      event_city:{
	        required:"Event City is required",
	      },
	      event_nature:{
	        required:"Event Nature is required",
	      },
	      start_date: {
	      	required: "Start date is required",
	      },
	      end_date: {
	      	required: "End date is required",
	      },
	      contact_name:{
	        required:"Contact Name is required",
	      },
	      email:{
	        required:"Email is required",
	      },
	      mobile_no:{
	        required:"Mobile Number is required",
	      },
	    },

	    errorPlacement: function(error, element) {
	      
	      if(element.prop('tagName')  == 'SELECT') {
			          $('#event_error').html(error);
					  error.insertAfter(element.closest('.input-field').find('.js-example-basic-single'));
					}else{
			           error.insertAfter(element);
			          }
	    },

	    submitHandler: function(form) {
			$("input[name='csrf_test_name']").val(csrf_hash);
	    	form.submit();
	    
	   /* var event_number 		= $('#event_number').val();
		var event_date 			= $('#event_date').val();
		var event_name 			= $('#event_name').val();
		//var event_image		= $('#event_image').val();
		var event_address 		= $('#event_address').val();
		var event_city			= $('#event_city').val();
		var event_nature		= $('#event_nature').val();
		var event_description 	= $('#event_description').val();
		var website_link		= $('#website_link').val();
		var entry_fee 			= $('#entry_fee').val();
		var start_date			= $('#start_date').val();
		var end_date			= $('#end_date').val();
		var company_name		= $('#company_name').val();
		var contact_name		= $('#contact_name').val();
		var email				= $('#email').val();
		var mobile_no			= $('#mobile_no').val();
		var files				= $('#event_image').prop('files')[0];

		var fd 					= new FormData();
		fd.append('event_number', event_number);
		fd.append('event_date', event_date);
		fd.append('event_name', event_name);
		fd.append('event_image', files);
		fd.append('event_address', event_address);
		fd.append('event_city', event_city);
		fd.append('event_nature', event_nature);
		fd.append('event_description', event_description);
		fd.append('website_link', website_link);
		fd.append('entry_fee', entry_fee);
		fd.append('start_date', start_date);
		fd.append('end_date', end_date);
		fd.append('company_name', company_name);
		fd.append('contact_name', contact_name);
		fd.append('email', email);
		fd.append('mobile_no', mobile_no);
		$.ajax({

			type: "POST",
			contentType: false,
			processData: false,
			url:base_url+'Community/add_event_db',

			//data:{'event_number':event_number, 'event_date':event_date, 'event_name':event_name, 'event_image':files, 'event_address':event_address, 'event_city':event_city, 'event_nature':event_nature, 'event_description':event_description, 'website_link':website_link, 'entry_fee':entry_fee, 'start_date':start_date, 'end_date': end_date, 'company_name':company_name, 'contact_name':contact_name, 'email':email, 'mobile_no':mobile_no},

			data: fd,

			success: function(data){

				$('#event_number').val('');
				$('#event_date').val('');
				$('#event_name').val('');
				$('#event_image').val('');
				$('#event_address').val('');
				$('#event_city').val('');
				$('#event_nature').val('');
				$('#event_description').val('');
				$('#website_link').val('');
				$('#entry_fee').val('');
				$('#start_date').val('');
				$('#end_date').val('');
				$('#company_name').val('');
				$('#contact_name').val('');
				$('#email').val('');
				$('#mobile_no').val('');

				Materialize.toast('New event added successfully', 4000,'green rounded');
				location.href=base_url+'community/events';
			}
		});  */

	    },

	  });
	});

	$("#connection_send").on('click',function(e){

		e.preventDefault();

		var con_client 	= $('#con-client').val();
		var con_vendor 	= $('#con-vendor').val();
		var con_both 	= $('#con-both').val();
		var con_none 	= $('#con-none').val();
		var message 	= $('#message').val();
		var con_to_id	= $('#con_to').val();
		if(csrf_hash===""){
			csrf_hash=csrf_hash;
		}
		$.ajax({

			dataType: 'json',
			type: "POST",
			url:base_url+'Community/add_connection_info',

			data:{'csrf_test_name':csrf_hash,'client':con_client,'vendor':con_vendor,'message':message, 'bus_id_connected_to':con_to_id},

			success: function(data){
				console.log(data);
				if(data['csrf_hash']){
					csrf_hash=data['csrf_hash'];
				}
				$('select#'+prefix+'_conn_id').html(options);
				$('#connect_modal').modal('close');
				$('#'+prefix+'_conn_id').material_select();

				$('#con-client').val('');
				$('#con-vendor').val('');
				$('#con-both').val('');
				$('#con-none').val('');
				$('#message').val('');
				$('#con_to').val('');

				Materialize.toast('Connection request sent successfully', 4000,'green rounded');
			}
		});
	});


	$(function(){
		
	  $("#edit_event").submit(function(e){
	  	e.preventDefault();	
	  	}).validate({

	    rules: {

	      event_name: {
	        required: true,
	      },
	      event_address: {
	        required: true,
	      },
	      event_city: {
	        required: true,
	      },
	      event_nature: {
	        required: true,
	      },
	      start_date: {
	      	required: true,
	      },
	      end_date: {
	      	required: true,
	      },
	      contact_name: {
	        required: true,
	      },
	      email: {
	        required: true,
	      },
	      mobile_no: {
	        required: true,
	      },
		},

	    messages: {

	      event_name:{
	        required:"Event Name is required",
	      },
	      event_address:{
	        required:"Event Address is required",
	      },
	      event_city:{
	        required:"Event City is required",
	      },
	      event_nature:{
	        required:"Event Nature is required",
	      },
	      start_date: {
	      	required: "Start date is required",
	      },
	      end_date: {
	      	required: "End date is required",
	      },
	      contact_name:{
	        required:"Contact Name is required",
	      },
	      email:{
	        required:"Email is required",
	      },
	      mobile_no:{
	        required:"Mobile Number is required",
	      },
	    },

	    errorPlacement: function(error, element) {
	      
	      if(element.prop('tagName')  == 'SELECT') {
			          $('#event_error').html(error);
					  error.insertAfter(element.closest('.input-field').find('.js-example-basic-single'));
					}else{
			           error.insertAfter(element);
			          }
	    },

	    submitHandler: function(form) {
			$("input[name='csrf_test_name']").val(csrf_hash);
	    	form.submit();

	    	/*
	    
	    var event_number 		= $('#event_number').val();
		var event_date 			= $('#event_date').val();
		var event_name 			= $('#event_name').val();
		//var event_image		= $('#event_image').val();
		var event_address 		= $('#event_address').val();
		var event_city			= $('#event_city').val();
		var event_nature		= $('#event_nature').val();
		var event_description 	= $('#event_description').val();
		var website_link		= $('#website_link').val();
		var entry_fee 			= $('#entry_fee').val();
		var start_date			= $('#start_date').val();
		var end_date			= $('#end_date').val();
		var company_name		= $('#company_name').val();
		var contact_name		= $('#contact_name').val();
		var email				= $('#email').val();
		var mobile_no			= $('#mobile_no').val();
		var files				= $('#event_image').prop('files')[0];

		var fd 					= new FormData();
		fd.append('event_number', event_number);
		fd.append('event_date', event_date);
		fd.append('event_name', event_name);
		fd.append('event_image', files);
		fd.append('event_address', event_address);
		fd.append('event_city', event_city);
		fd.append('event_nature', event_nature);
		fd.append('event_description', event_description);
		fd.append('website_link', website_link);
		fd.append('entry_fee', entry_fee);
		fd.append('start_date', start_date);
		fd.append('end_date', end_date);
		fd.append('company_name', company_name);
		fd.append('contact_name', contact_name);
		fd.append('email', email);
		fd.append('mobile_no', mobile_no);
		$.ajax({

			type: "POST",
			contentType: false,
			processData: false,
			url:base_url+'Community/edit_event',

			//data:{'event_number':event_number, 'event_date':event_date, 'event_name':event_name, 'event_image':files, 'event_address':event_address, 'event_city':event_city, 'event_nature':event_nature, 'event_description':event_description, 'website_link':website_link, 'entry_fee':entry_fee, 'start_date':start_date, 'end_date': end_date, 'company_name':company_name, 'contact_name':contact_name, 'email':email, 'mobile_no':mobile_no},

			data: fd,

			success: function(data){

				$('#event_number').val('');
				$('#event_date').val('');
				$('#event_name').val('');
				$('#event_image').val('');
				$('#event_address').val('');
				$('#event_city').val('');
				$('#event_nature').val('');
				$('#event_description').val('');
				$('#website_link').val('');
				$('#entry_fee').val('');
				$('#start_date').val('');
				$('#end_date').val('');
				$('#company_name').val('');
				$('#contact_name').val('');
				$('#email').val('');
				$('#mobile_no').val('');

				Materialize.toast('Event updated successfully', 4000,'green rounded');
				location.href=base_url+'community/events';
			}
		});   */

	    },

	  });
	});

	$("#connection_send").on('click',function(e){

		e.preventDefault();

		var con_client 	= $('#con-client').val();
		var con_vendor 	= $('#con-vendor').val();
		var con_both 	= $('#con-both').val();
		var con_none 	= $('#con-none').val();
		var message 	= $('#message').val();
		var con_to_id	= $('#con_to').val();
		if(csrf_hash===""){
			csrf_hash=csrf_hash;
		}
		$.ajax({

			dataType: 'json',
			type: "POST",
			url:base_url+'Community/add_connection_info',

			data:{'csrf_test_name':csrf_hash,'client':con_client,'vendor':con_vendor,'message':message, 'bus_id_connected_to':con_to_id},

			success: function(data){
				console.log(data);
				if(data['csrf_hash']){
					csrf_hash=data['csrf_hash'];
				}
				$('select#'+prefix+'_conn_id').html(options);
				$('#connect_modal').modal('close');
				$('#'+prefix+'_conn_id').material_select();

				$('#con-client').val('');
				$('#con-vendor').val('');
				$('#con-both').val('');
				$('#con-none').val('');
				$('#message').val('');
				$('#con_to').val('');

				Materialize.toast('Connection request sent successfully', 4000,'green rounded');
			}
		});
	});
	
function eventDatatable(url,id){
	
    $('#'+id).DataTable().destroy();
	if(csrf_hash===""){
		csrf_hash='<?php echo $this->security->get_csrf_hash;?>';
	}
	expmasterTable = $('#'+id).dataTable({
    "bServerSide": true,
    "bProcessing": true,
    "bDeferRender": true,
    "aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0,1,2,]}],
     "aoColumns": [ 
    { "bSortable": false },
    { "bSortable": false },
    { "bSortable": false },
    ],
    oLanguage: { sLengthMenu: "_MENU_", },

    "order": [0,'asc'],
    "bPaginate": true,
    "bLengthChange": true,
    "sDom": 'Rfrtlip',

    "bFilter": true,
    "bInfo": false,
    "searching":false,
    "iDisplayLength": 10,

    lengthMenu: [
      [ 10, 20, 30, 50, -1 ],
      [ 'SHOW 10', 'SHOW 20', 'SHOW 30', 'SHOW 50', 'SHOW ALL' ]
    ],

    "ajax": {
      "url": url,
      "type": "POST",
      "data":{'csrf_test_name':csrf_hash,'search':$('#search').val(), 'c_start_date':$('#c_start_date').val(), 'c_end_date':$('#c_end_date').val(), 'location':$('#location').val(), 'nature':$('#nature').val(),},
    },
	"initComplete":function( settings,json){
				//console.log(json);
				//var csrfdata=jQuery.parseJSON(json);
				console.log(json.csrf_hash);
				if(json.csrf_hash){
					csrf_hash=json.csrf_hash;
				}
				// call your function here
			},
    "dom": "<'row'<'col-md-6'l><'col-md-6'f>><'row'<'col-md-6'B><'col-md-6'p>><'row'<'col-md-12't>><'row'<'col-md-12'i>>",
    "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
    
      //  if($(aData[6]+' div').hasClass('deactive_record')){
      //  $('td', nRow).parent('tr').addClass('deactivate-row');
      // } 

      $('td:first-child', nRow).addClass('bulk');
      //$('td:nth-child(2)', nRow).addClass('profil-img');
      $('td:last-child', nRow).addClass('action-tab');
     },
     "fnDrawCallback": function () {
      $('.dropdown-button, .translation-button, .dropdown-menu').dropdown({
              inDuration: 300,
              outDuration: 225,
              constrainWidth: false,
              hover: true,
              gutter: 0,
              belowOrigin: true,
              alignment: 'left',
              stopPropagation: false
            });

        	$('.deactive_event').on('click',function(){

				var event_id = $(this).data('cd_id');

				 $('#remove_event').modal('open');

				 $('#remove_event #remove_event_id').val(event_id);

			});

			$('#deactive_event').off().on('click',function(){

				var event_id = $('#remove_event_id').val();

				$.ajax({

					url:base_url+'Community/delete_event',

					type:"POST",

					data:{'csrf_test_name':csrf_hash,"event_id":event_id,},

					success:function(res){

						var myArray = res.split("@");

						
                            Materialize.toast('Your Event has been successfully deleted', 2000,'green rounded');
								eventDatatable(base_path()+'community/get_event_details','event_table');
									
							/*if(res == 'true@')
							{
								//window.location.href=base_url+'Community/events';
								Materialize.toast('Your Event has been successfully deleted', 2000,'green rounded');
								eventDatatable(base_path()+'community/get_event_details','event_table');
							}
							else
							{
								//window.location.href=base_url+'Community/deals';
								Materialize.toast('Error while processing!', 2000,'red rounded');
							}*/
						},
					});
			});
		  
		  
			  
            
    }
});
}

function dealDatatable(url,id){
	
    $('#'+id).DataTable().destroy();
    console.log(url);
	expmasterTable = $('#'+id).dataTable({
    "bServerSide": true,
    "bProcessing": true,
    "bDeferRender": true,
    "aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0,1,2,]}],
     "aoColumns": [ 
    { "bSortable": false },
    { "bSortable": false },
    { "bSortable": false },
    ],
    oLanguage: { sLengthMenu: "_MENU_", },

    "order": [0,'asc'],
    "bPaginate": true,
    "bLengthChange": true,
    "sDom": 'Rfrtlip',

    "bFilter": true,
    "bInfo": false,
    "searching":false,
    "iDisplayLength": 10,

    lengthMenu: [
      [ 10, 20, 30, 50, -1 ],
      [ 'SHOW 10', 'SHOW 20', 'SHOW 30', 'SHOW 50', 'SHOW ALL' ]
    ],

    "ajax": {
      "url": url,
      "type": "POST",
      "data":{'csrf_test_name':csrf_hash,'search':$('#search_offer').val(), 'c_start_date':$('#offer_start_date').val(), 'c_end_date':$('#offer_end_date').val(), 'offer_category':$('#offer_category').val(), 'offer_company':$('#offer_company').val(),},
    },

    "dom": "<'row'<'col-md-6'l><'col-md-6'f>><'row'<'col-md-6'B><'col-md-6'p>><'row'<'col-md-12't>><'row'<'col-md-12'i>>",
    "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
    
      //  if($(aData[6]+' div').hasClass('deactive_record')){
      //  $('td', nRow).parent('tr').addClass('deactivate-row');
      // } 

      $('td:first-child', nRow).addClass('bulk');
      //$('td:nth-child(2)', nRow).addClass('profil-img');
      $('td:last-child', nRow).addClass('action-tab');
     },
     "fnDrawCallback": function () {
      $('.dropdown-button, .translation-button, .dropdown-menu').dropdown({
              inDuration: 300,
              outDuration: 225,
              constrainWidth: false,
              hover: true,
              gutter: 0,
              belowOrigin: true,
              alignment: 'left',
              stopPropagation: false
            });

     $('.deactive_deal').on('click',function(){

				var deal_id = $(this).data('deal_id');

				 $('#remove_deal').modal('open');

				 $('#remove_deal #remove_deal_id').val(deal_id);

			});

			$('#deactive_deal').off().on('click',function(){

				var deal_id = $('#remove_deal_id').val();

				$.ajax({

					url:base_url+'Community/delete_deal',

					type:"POST",

					data:{'csrf_test_name':csrf_hash,"deal_id":deal_id,},

					success:function(res){
							if(res == true)
							{
								//window.location.href=base_url+'Community/deals';
								Materialize.toast('Your Deal has been successfully deleted', 2000,'green rounded');
								dealDatatable(base_path()+'community/get_deal_details','deal_table');
							}
							else
							{
								//window.location.href=base_url+'Community/deals';
								Materialize.toast('Error while processing!', 2000,'red rounded');
							}
						},
					});
			});
    }
});
}

function coworkingDatatable(url,id){
    $('#'+id).DataTable().destroy();
	if(csrf_hash===""){
		csrf_hash='<?php echo $this->security->get_csrf_hash;?>';
	}
	expmasterTable = $('#'+id).dataTable({
    "scrollY":299,
	"bServerSide": true,
    "bProcessing": true,
    "bDeferRender": true,
    "aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0,1,2,]}],
     "aoColumns": [ 
    { "bSortable": false },
    { "bSortable": false },
    { "bSortable": false },
    ],
    oLanguage: { sLengthMenu: "_MENU_", },

    "order": [0,'asc'],
    "bPaginate": true,
    "bLengthChange": true,
    "sDom": 'Rfrtlip',

    "bFilter": true,
    "bInfo": false,
    "searching":false,
    "iDisplayLength": 10,

    lengthMenu: [
      [ 10, 20, 30, 50, -1 ],
      [ 'SHOW 10', 'SHOW 20', 'SHOW 30', 'SHOW 50', 'SHOW ALL' ]
    ],

    "ajax": {
      "url": url,
      "type": "POST",
       "data":{'csrf_test_name':csrf_hash,'name':$('#cowork_name').val(), 'state':$('#cowork_country').val(), 'city':$('#cowork_city').val()},
    },
	"initComplete":function( settings,json){
				//console.log(json);
				//var csrfdata=jQuery.parseJSON(json);
				console.log(json.csrf_hash);
				if(json.csrf_hash){
					csrf_hash=json.csrf_hash;
				}
				// call your function here
			},
    "dom": "<'row'<'col-md-6'l><'col-md-6'f>><'row'<'col-md-6'B><'col-md-6'p>><'row'<'col-md-12't>><'row'<'col-md-12'i>>",
    "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
    
      //  if($(aData[6]+' div').hasClass('deactive_record')){
      //  $('td', nRow).parent('tr').addClass('deactivate-row');
      // } 

      $('td:first-child', nRow).addClass('bulk');
      //$('td:nth-child(2)', nRow).addClass('profil-img');
      $('td:last-child', nRow).addClass('action-tab');
     },
     "fnDrawCallback": function () {
      $('.dropdown-button, .translation-button, .dropdown-menu').dropdown({
              inDuration: 300,
              outDuration: 225,
              constrainWidth: false,
              hover: true,
              gutter: 0,
              belowOrigin: true,
              alignment: 'left',
              stopPropagation: false
            });

        	
		  
			  
            
    }
});
}

function incubatorDatatable(url,id){
	
    $('#'+id).DataTable().destroy();
	if(csrf_hash===""){
		csrf_hash='<?php echo $this->security->get_csrf_hash;?>';
	}
	expmasterTable = $('#'+id).dataTable({
    "scrollY":299,
	"bServerSide": true,
    "bProcessing": true,
    "bDeferRender": true,
    "aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0,1,2,]}],
     "aoColumns": [ 
    { "bSortable": false },
    { "bSortable": false },
    { "bSortable": false },
    ],
    oLanguage: { sLengthMenu: "_MENU_", },

    "order": [0,'asc'],
    "bPaginate": true,
    "bLengthChange": true,
    "sDom": 'Rfrtlip',

    "bFilter": true,
    "bInfo": false,
    "searching":false,
    "iDisplayLength": 10,

    lengthMenu: [
      [ 10, 20, 30, 50, -1 ],
      [ 'SHOW 10', 'SHOW 20', 'SHOW 30', 'SHOW 50', 'SHOW ALL' ]
    ],

    "ajax": {
      "url": url,
      "type": "POST",
       "data":{'csrf_test_name':csrf_hash, 'name':$('#incub_name').val(), 'country':$('#incub_country').val(), 'city':$('#incub_city').val()},
    },
	"initComplete":function( settings,json){
				//console.log(json);
				//var csrfdata=jQuery.parseJSON(json);
				console.log(json.csrf_hash);
				if(json.csrf_hash){
					csrf_hash=json.csrf_hash;
				}
				// call your function here
			},
    "dom": "<'row'<'col-md-6'l><'col-md-6'f>><'row'<'col-md-6'B><'col-md-6'p>><'row'<'col-md-12't>><'row'<'col-md-12'i>>",
    "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
    
      //  if($(aData[6]+' div').hasClass('deactive_record')){
      //  $('td', nRow).parent('tr').addClass('deactivate-row');
      // } 

      $('td:first-child', nRow).addClass('bulk');
      //$('td:nth-child(2)', nRow).addClass('profil-img');
      $('td:last-child', nRow).addClass('action-tab');
     },
     "fnDrawCallback": function () {
      $('.dropdown-button, .translation-button, .dropdown-menu').dropdown({
              inDuration: 300,
              outDuration: 225,
              constrainWidth: false,
              hover: true,
              gutter: 0,
              belowOrigin: true,
              alignment: 'left',
              stopPropagation: false
            });

        
		  
		  
			  
            
    }
});
}
	
});