$(document).ready(function() {
		 profile_datatable(base_path()+'profile/get_personal_profiles/','personal-profile');
		 $('select').material_select();
		$('.profile-search').on('keyup','#search_personal_profile',function(){ 
		  profile_datatable(base_path()+'profile/get_personal_profiles/','personal-profile');
		  $('select').material_select();
				
		});
		//$('#search').on('keyup', function() { get_table() } );
	


 $("#reg_admin_type").on("change",function(){
  var reg_admin_type = $(this).val();
  if(reg_admin_type!=''){
    $("#select2-reg_admin_type-container").css("font-size","14px");
    $("#select2-reg_admin_type-container").css("color","#000");
    $("#select2-reg_admin_type-container").css("font-weight","500");
  }
});

	$("body").on('click','#print_multiple_profiles',function(e) {
	var dnwl_ids = $('#deactive_multiple_profiles').attr('data-multi_profiles');
	if(dnwl_ids == '' || dnwl_ids == '0'){
		Materialize.toast('Please select a record first', 2000,'red rounded');
	}
	else
	{
		var words = dnwl_ids.split(",");
		if(words.length < 2){
			Materialize.toast('Select two or more records first..!', 2000,'red rounded');
		}else{
		$('#deactive_multiple_profiles').attr('data-multi_profiles',0);
		$('#personal-profile th').find('input[type="checkbox"]').each(function() {
			$(this).prop('checked', false);
		});
		$("#personal_profile_bulk").prop('checked', false);  
		profile_datatable(base_path()+'profile/get_personal_profiles/','personal-profile');
		$('select').material_select();
		window.location=base_url+'profile/print_personal_profile?ids='+dnwl_ids;
		Materialize.toast('Profile has been printed', 2000,'green rounded');
		}
	}
	});

	$("body").on('click','#download_multiple_profiles',function(e) {
		var dnwl_ids = $('#download_multiple_profiles').attr('data-multi_download');
		if(dnwl_ids == '' || dnwl_ids == '0')
		{
			//Materialize.toast('Please select a record first', 2000,'red rounded');
		}
		else
		{
			var words = dnwl_ids.split(",");
		    if(words.length < 2){
		     	Materialize.toast('Select two or more records first..!', 2000,'red rounded');
		    }else{
				$('#download_multiple_profiles').attr('data-multi_download',0);
				$('#personal-profile th').find('input[type="checkbox"]').each(function() {
					$(this).prop('checked', false);
				});
				$("#personal_profile_bulk").prop('checked', false);  
				profile_datatable(base_path()+'profile/get_personal_profiles/','personal-profile');
				$('select').material_select();
				window.location=base_url+'profile/download_multi_personal_profiles?ids='+dnwl_ids;		
				Materialize.toast('Profile has been downloaded', 2000,'green rounded');
			}
		}
	});


 $("#add_personal_profile").validate({

				rules:{

					reg_username:{

						required:true,

					},

					reg_email:{

						required:true,

						email:true,

					},

					reg_mobile:{

						required:true,

						number:true,

						//minlength:10,

						//maxlength:10,

					},

					reg_password: {

						required:true,
						checklowercase: true,
						checkuppercase: true,
						checknumber: true,
						checkspecialchar: true,
						minlength:8,
					},

					contact_confirm_password: {

						required:true,

						minlength:8,

						equalTo: '#reg_password',

					},

					reg_admin_type:{

						required:true,

					},

					reg_designation:{

						//required:true,

					},

					reg_degital_signature:{

						//required:true,

					},
				},

				messages:{

					reg_username:{

						required:"Contact Name is required",

					},

					reg_email:{

						required:"Email ID is required",

						email:"Please Enter Valid Email ID",

					},

					reg_mobile:{

						required:"Mobile Number is required",

						number:"Please Enter only digits",

						//minlength:"Please Enter Mobile No like (e.g 0123456789)",

						//maxlength:"Please Enter valid 10 digits Mobile Number",

					},

					reg_password: {

						required:"Password is required",
						checklowercase:"At least 1 Lower case",
						checkuppercase:"At least 1 Upper case",
						checknumber:"At least 1 Number",
						checkspecialchar:"At least 1 Special Character",
						minlength:"Password must contain at least 8 characters.",

					},

					contact_confirm_password: {

						required:"Confirm Password is required.",

						minlength:"Password must contain at least 8 characters.",

						equalTo:"Your password doesn't match",

					},

					reg_admin_type:{

						required:"Please Allow any access to New User",

					},

					reg_designation:{

						required:"Please Enter Designation",

					},

					reg_degital_signature:{

						required:"Please upload Degital signature",

					},

				},
				errorPlacement: function(error, element) {
					if(element.prop('name')  == 'reg_username') {
						Materialize.toast('Please fill the mandatory cells', 4000,'red rounded'); 
					}
			          if(element.prop('tagName')  == 'SELECT') {
			          
			          error.insertAfter(element.closest('.input-field').find('.js-example-basic-single'));
			          }else{
			           error.insertAfter(element);
			          }
			    } ,
			    submitHandler: function(form) {
					$("input[name='csrf_test_name']").val(csrf_hash);
			        form.submit();
			      }   
				});
 });

 $("#edit_personal_profile").validate({

				rules:{

					reg_username:{

						required:true,

					},

					reg_email:{

						required:true,

						email:true,

					},

					reg_mobile:{

						required:true,

						number:true,

						//minlength:10,

						//maxlength:10,

					},

					reg_password: {

						required:true,
						checklowercase: true,
						checkuppercase: true,
						checknumber: true,
						checkspecialchar: true,
						minlength:8,
					},

					contact_confirm_password: {

						required:true,

						minlength:8,

						equalTo: '#reg_password',

					},

					reg_admin_type:{

						required:true,

					},

					reg_designation:{

						//required:true,

					},

				},

				messages:{

					reg_username:{

						required:"Contact Name is required",

					},

					reg_email:{

						required:"Email ID is required",

						email:"Please Enter Valid Email ID",

					},

					reg_mobile:{

						required:"Mobile Number is required",

						number:"Please Enter only digits",

						//minlength:"Please Enter Mobile No like (e.g 0123456789)",

						//maxlength:"Please Enter valid 10 digits Mobile Number",

					},

					reg_password: {

						required:"Password is required",
						checklowercase:"At least 1 Lower case",
						checkuppercase:"At least 1 Upper case",
						checknumber:"At least 1 Number",
						checkspecialchar:"At least 1 Special Character",
						minlength:"Password must contain at least 8 characters.",

					},

					contact_confirm_password: {

						required:"Confirm Password is required.",

						minlength:"Password must contain at least 8 characters.",

						equalTo:"Your password doesn't match",

					},

					reg_admin_type:{

						required:"Please Allow any access to New User",

					},

					reg_designation:{

						required:"Please Enter Designation",

					},

				},
				errorPlacement: function(error, element) {
         
			          if(element.prop('tagName')  == 'SELECT') {
			          
			          error.insertAfter(element.closest('.input-field').find('.js-example-basic-single'));
			          }else{
			           error.insertAfter(element);
			          }
			    } ,
			    submitHandler: function(form) {
					$("input[name='csrf_test_name']").val(csrf_hash);
			        form.submit();
			      }   
				});
 


$.validator.addMethod("checklowercase",
    function(value, element) {
        return /^(?=(.*[a-z]){1,}).{1,}$/.test(value);
});
$.validator.addMethod("checkuppercase",
    function(value, element) {
        return /^(?=.*?[A-Z]).{1,}$/.test(value);
});
$.validator.addMethod("checknumber",
    function(value, element) {
        return /^(?=(.*[\d]){1,}).{1,}$/.test(value);
});
$.validator.addMethod("checkspecialchar",
    function(value, element) {
        return /^(?=(.*[\W]){1,})(?!.*\s).{1,}$/.test(value);
});
