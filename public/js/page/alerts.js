$.validator.setDefaults({ ignore: '' });

 $(document).ready(function() {

 	  alertsDatatable(base_path()+'settings/get_my_alerts/','my-alerts');
    ialertsDatatable(base_path()+'settings/get_item_alerts/','my-item-alerts');
    jalertsDatatable(base_path()+'settings/get_jv_alerts/','my-jv-alerts');
    valertsDatatable(base_path()+'settings/get_v_alerts/','my-valerts');
    halertsDatatable(base_path()+'settings/get_ch_alerts/','my-ch-alerts');
    exalertsDatatable(base_path()+'settings/get_exp_alerts/','my_exp_alert');
    balertsDatatable(base_path()+'settings/get_ba_alerts/','my-ba-alerts');
    aalertsDatatable(base_path()+'settings/get_generic_alerts/','my-generic-alerts');

    $('#calerts_customer,#calerts_start_date,#calerts_end_date').change( function() {
    if($(this).attr('id')=="calerts_customer" ){ alertsDatatable(base_path()+'settings/get_my_alerts/','my-alerts'); $('select').material_select();
    }else{ if($(this).val()!=''){ alertsDatatable(base_path()+'settings/get_my_alerts/','my-alerts');$('select').material_select(); }}
    });

    $('#ialerts_customer,#ialerts_start_date,#ialerts_end_date').change( function() {
    if($(this).attr('id')=="ialerts_customer"){ ialertsDatatable(base_path()+'settings/get_item_alerts/','my-item-alerts'); $('select').material_select();
    }else{ if($(this).val()!=''){ ialertsDatatable(base_path()+'settings/get_item_alerts/','my-item-alerts'); $('select').material_select(); }}
    });

    $('#aalerts_customer,#aalerts_start_date,#aalerts_end_date').change( function() {
    if($(this).attr('id')=="aalerts_customer"){ aalertsDatatable(base_path()+'settings/get_generic_alerts/','my-generic-alerts'); $('select').material_select();
    }else{ if($(this).val()!=''){ aalertsDatatable(base_path()+'settings/get_generic_alerts/','my-generic-alerts'); $('select').material_select(); }}
    });

    $('#jalerts_customer,#jalerts_start_date,#jalerts_end_date').change( function() {
    if($(this).attr('id')=="jalerts_customer"){ jalertsDatatable(base_path()+'settings/get_jv_alerts/','my-jv-alerts');$('select').material_select();
    }else{ if($(this).val()!=''){ jalertsDatatable(base_path()+'settings/get_jv_alerts/','my-jv-alerts');$('select').material_select(); }}
    });

    $('#valerts_customer,#valerts_start_date,#valerts_end_date').change( function() {
    if($(this).attr('id')=="valerts_customer" ){ valertsDatatable(base_path()+'settings/get_v_alerts/','my-valerts'); $('select').material_select();
    }else{ if($(this).val()!=''){ alertsDatatable(base_path()+'settings/get_v_alerts/','my-alerts');$('select').material_select(); }}
    });

    $('#halerts_customer,#halerts_start_date,#halerts_end_date').change( function() {
    if($(this).attr('id')=="halerts_customer"){ halertsDatatable(base_path()+'settings/get_ch_alerts/','my-ch-alerts');$('select').material_select();
    }else{ if($(this).val()!=''){ halertsDatatable(base_path()+'settings/get_ch_alerts/','my-ch-alerts');$('select').material_select(); }}
    });

    $('#balerts_occasion,#balerts_customer,#balerts_start_date,#balerts_end_date').change( function() {
    if($(this).attr('id')=="balerts_occasion" || $(this).attr('id')=="balerts_customer"){ balertsDatatable(base_path()+'settings/get_ba_alerts/','my-ba-alerts');$('select').material_select();
    }else{ if($(this).val()!=''){ balertsDatatable(base_path()+'settings/get_ba_alerts/','my-ba-alerts');$('select').material_select(); }}
    });

    $('.alerts-search').on('keyup','#search_calerts',function(){
      alertsDatatable(base_path()+'settings/get_my_alerts/','my-alerts');
      $('select').material_select();
    });

    $('.alerts-search').on('keyup','#search_ialerts',function(){
      ialertsDatatable(base_path()+'settings/get_item_alerts/','my-item-alerts');
      $('select').material_select();
    });

    $('.alerts-search').on('keyup','#search_jalerts',function(){
      jalertsDatatable(base_path()+'settings/get_jv_alerts/','my-jv-alerts');
      $('select').material_select();
    });

    $('.alerts-search').on('keyup','#search_valerts',function(){
      jalertsDatatable(base_path()+'settings/get_v_alerts/','my-alerts');
      $('select').material_select();
    });

    $('.alerts-search').on('keyup','#search_halerts',function(){
      halertsDatatable(base_path()+'settings/get_ch_alerts/','my-ch-alerts');
      $('select').material_select();
    });

    $('.alerts-search').on('keyup','#search_balerts',function(){
      balertsDatatable(base_path()+'settings/get_ba_alerts/','my-ba-alerts');
      $('select').material_select();
    });

    $('.alerts-search').on('keyup','#search_aalerts',function(){
      aalertsDatatable(base_path()+'settings/get_generic_alerts/','my-generic-alerts');
      $('select').material_select();
    });

 		 $("#settings_alerts").change(function(){
		    var alerts=$(this).val();

		    if(alerts!=''){

		    window.location=base_url+"settings/"+alerts;
		    }
		  });


     $("#parent_name").change(function(){
        var name=$(this).val();
          $.ajax({
            url:base_url+'settings/get_client_vendor',
            type:"POST",
            dataType: 'json',
            data:{'csrf_test_name':csrf_hash,"name":name},
            success:function(res){
              if(res['type']=='client'){
              var str='<option value="">SELECT CLIENT *</option>';
                for(i=0;i<res['client'].length;i++){
                    str+='<option value="'+res['client'][i].cust_id+'">'+res['client'][i].cust_name+'</option>';
                }
              }else if(res['type']=='vendor'){
                var str='<option value="">SELECT VENDOR *</option>';
                for(i=0;i<res['client'].length;i++){
                    str+='<option value="'+res['client'][i].vendor_id+'">'+res['client'][i].vendor_name+'</option>';
                }
              }else if(res['type']=='item'){
                var str='<option value="">SELECT ITEM *</option>';
                for(i=0;i<res['services'].length;i++){
                    str+='<option value="'+res['services'][i].service_id+'">'+res['services'][i].service_name+'</option>';
                }
              }else if(res['type']=='expense'){
                var str='<option value="">SELECT EXPENSE *</option>';
                for(i=0;i<res['expenses'].length;i++){
                    str+='<option value="'+res['expenses'][i].exp_id+'">'+res['expenses'][i].exp_name+'</option>';
                }
              }else{
                //
              }
                $('#alert_number').val(res['alertNo']);
                $('#parent_id').html(str);

                  $('#parent_id').material_select();

              },
          });

      });

     $('#alert_mobile').focus(function(){
      if($(this).attr('readonly')){
        $(".noti_type").each(function () {
           if($(this).find("img").attr('src')==base_path()+"asset/css/img/icons/msg-grey.png"){
            $(this).find("img").css('border','1px solid red');
           }
        });
      }
     });

     $('#alert_mobile').focusout(function(){
      if($(this).attr('readonly')){
        $(".noti_type").each(function () {
           if($(this).find("img").attr('src')==base_path()+"asset/css/img/icons/msg-grey.png"){
            $(this).find("img").css('border','none');
           }
        });
      }
     });

     $('#alert_email').focus(function(){
      if($(this).attr('readonly')){
        $(".noti_type").each(function () {
          if($(this).find("img").attr('src')==base_path()+"asset/css/img/icons/mail-grey.png"){
            $(this).find("img").css('border','1px solid red');
          }
        });
      }
     });

      $('#alert_email').focusout(function(){
      if($(this).attr('readonly')){
        $(".noti_type").each(function () {
           if($(this).find("img").attr('src')==base_path()+"asset/css/img/icons/mail-grey.png"){
            $(this).find("img").css('border','none');
           }
        });
      }
     });

 		 $('.noti_type').on('click',function(){
 		 		var notification_type= $('#notification_type').val();



         var email=$('#email').val();
          var mobile=$('#mobile').val();

				var type = $(this).data('noti_type');
				if($(this).find('img').hasClass('selected_notification')){
					$(this).find('img').removeClass('selected_notification');
					console.log('re:'+type);
				if(type=='2'){

          $(this).find('img').attr('src',base_path()+"asset/css/img/icons/msg-grey.png");
					$('#alert_msg').val('');
          $('#alert_mobile').val('');

					$('#alert_mobile').attr('readonly','readonly').parents('.input-field').children().removeClass('active');
        }else if(type=='3'){
           var email=$('#email').val();
          $(this).find('img').attr('src',base_path()+"asset/css/img/icons/mail-grey.png");
					$('#alert_email').val('');
          $('#alert_mail').val('');
					$('#alert_email').attr('readonly','readonly').parents('.input-field').children().removeClass('active');
				}else{
          $(this).find('img').attr('src',base_path()+"asset/css/img/icons/bell-grey.png");
					$('#alert_notification').val('');
				}
				}else{
					if(type=='2'){
          $(this).find('img').attr('src',base_path()+"asset/css/img/icons/msg-gicon.png");
					$('#alert_msg').val(1);

          $('#alert_mobile').val(mobile);
					$('#alert_mobile').removeAttr('readonly').focus();
        }else if(type=='3'){
            $(this).find('img').attr('src',base_path()+"asset/css/img/icons/mail-icon.png");
						$('#alert_mail').val(1);
              $('#alert_email').val(email);

						$('#alert_email').removeAttr('readonly').focus();
					}else{
            $(this).find('img').attr('src',base_path()+"asset/css/img/icons/bell-icon.png");
						$('#alert_notification').val(1);
					}

					console.log('add:'+type);
					$(this).find('img').addClass('selected_notification');
				}
				$('#notification_type').val(type);
				//$(this).find('img').addClass('selected_notification');
			});

      /*-----------For Customer Alerts ------------*/
      var bulk_calerts = [];
      $("input[id='calerts_bulk']").on("click",function(){
      if($(this).is(':checked')) {
        $(".calerts_bulk_action").prop('checked', true);
        bulk_calerts = [];
        $(".calerts_bulk_action:checked").each(function() {
            bulk_calerts.push($(this).val());
          });
          bulk_calerts = bulk_calerts.join(",");
          $('#deactive_multiple_calerts').attr('data-multi_calerts',bulk_calerts);
        }
        else {
          $('#my-alerts tr').find('input[type="checkbox"]').each(function() {
          $(this).prop('checked', false);
          });
          bulk_calerts = [];
          $('#deactive_multiple_calerts').attr('data-multi_calerts',0);
        }
    });
     $("#deactive_multiple_calerts").on('click', function() {
        var calerts_values = $(this).attr('data-multi_calerts');
        if(calerts_values == '' || calerts_values == '0')
        {
          Materialize.toast('Please select a record first', 2000,'red rounded');
        }
        else
        {
          var words = calerts_values.split(",");
          if(words.length < 2){
             Materialize.toast('Select two or more records first..!', 2000,'red rounded');
          }else{
          $.ajax({
            url:base_url+'settings/deactive_multiple_alerts',
            type:"POST",
            data:{'csrf_test_name':csrf_hash,"alerts_values":calerts_values,"alert_type":"customers","status":"Inactive"},
            success:function(res){
                  alertsDatatable(base_path()+'settings/get_my_alerts/','my-alerts');
                  $('select').material_select();
                  $('#deactive_multiple_calerts').attr('data-multi_calerts',0);
                  $('#my-alerts th').find('input[type="checkbox"]').each(function() {
                      $(this).prop('checked', false);
                    });
					$("#calerts_bulk").prop('checked', false);
                  Materialize.toast('Cients Alerts has been successfully deactivated', 2000,'green rounded');
              },
          });
          }
        }
      });
      $("#email_multiple_calerts").on('click', function() {
        var calerts_values = $('#deactive_multiple_calerts').attr('data-multi_calerts');
        if(calerts_values == '' || calerts_values == '0')
        {
          Materialize.toast('Please select a record first', 2000,'red rounded');
        }
        else
        {
          var words = calerts_values.split(",");
          if(words.length < 2){
             Materialize.toast('Select two or more records first..!', 2000,'red rounded');
          }else{
            // $.ajax({
            //   url:base_url+'settings/email_multiple_alerts',
            //   type:"POST",
            //   data:{"alerts_values":calerts_values},
            //   success:function(res){
            //         alertsDatatable(base_path()+'settings/get_my_alerts/','my-alerts');
            //         $('select').material_select();
            //         $('#deactive_multiple_calerts').attr('data-multi_calerts',0);
            //         $('#my-alerts th').find('input[type="checkbox"]').each(function() {
            //             $(this).prop('checked', false);
            //           });
            //         Materialize.toast('Clients Alerts has been successfully email', 2000,'green rounded');
            //     },
            // });
            email_alerts(calerts_values);
          }
        }
      });
     $("body").on('click','#download_multiple_calerts',function(e) {

      var dnwl_ids = $('#deactive_multiple_calerts').attr('data-multi_calerts');

      if(dnwl_ids == '' || dnwl_ids == '0')
      {

        Materialize.toast('Please select a record first', 2000,'red rounded');
      }
      else
      {
        var words = dnwl_ids.split(",");
        if(words.length < 2){
             Materialize.toast('Select two or more records first..!', 2000,'red rounded');
        }else{
          $('#deactive_multiple_calerts').attr('data-multi_calerts',0);
          $('#my-alerts th').find('input[type="checkbox"]').each(function() {
              $(this).prop('checked', false);
            });
			$("#calerts_bulk").prop('checked', false);
          window.location=base_url+'settings/download-multiple-alerts?ids='+dnwl_ids;
          alertsDatatable(base_path()+'settings/get_my_alerts/','my-alerts');
          $('select').material_select();
          Materialize.toast('Clients Alerts has been successfully downloaded', 2000,'green rounded');
        }
      }
    });
    $("body").on('click','#print_multiple_calerts',function(e) {

      var dnwl_ids = $('#deactive_multiple_calerts').attr('data-multi_calerts');

      if(dnwl_ids == '' || dnwl_ids == '0')
      {

        Materialize.toast('Please select a record first', 2000,'red rounded');
      }
      else
      {
        var words = dnwl_ids.split(",");
        if(words.length < 2){
             Materialize.toast('Select two or more records first..!', 2000,'red rounded');
        }else{
        $('#deactive_multiple_calerts').attr('data-multi_calerts',0);
        $('#my-alerts th').find('input[type="checkbox"]').each(function() {
            $(this).prop('checked', false);
          });
		  $("#calerts_bulk").prop('checked', false);
        window.location=base_url+'settings/print-multiple-alerts?ids='+dnwl_ids;
        alertsDatatable(base_path()+'settings/get_my_alerts/','my-alerts');
        $('select').material_select();
        Materialize.toast('Clients Alerts has been successfully print', 2000,'green rounded');
       }
      }
    });
     $('.active_deactive_calerts').off().on('click', function(){
          var calert_id = $(this).data('calerts_id');
          var cstatus = $(this).data('cstatus');
          var alert_type = $(this).data('alert_type');

         $('#view_deactivate_calerts').modal('open');

         $('#view_deactivate_calerts #remove_calert_id').val(calert_id);
         $('#view_deactivate_calerts #alt_status').val(cstatus);
         $('#view_deactivate_calerts #alt_type').val(alert_type);
         if(cstatus!="Active"){
          cstatus="Remove";
         }
         $('#view_deactivate_calerts .cstatus').text(cstatus);
        });
      $('.active_deactive_calerts_yes').off().on('click', function(){

        var calert_id = $('#view_deactivate_calerts #remove_calert_id').val();
        var cstatus = $('#view_deactivate_calerts #alt_status').val();
        var alert_type = $('#view_deactivate_calerts #alt_type').val();

        $.ajax({

          url:base_url+'settings/deactive_alerts',

          type:"POST",

          data:{'csrf_test_name':csrf_hash,"calert_id":calert_id,"alert_type":alert_type,"status":cstatus},

          success:function(res){
              if(res == true)
              {
               if(alt_status=="Active"){

                Materialize.toast(''+alert_type+' Alert has been successfully Activated', 2000,'green rounded');
                location. reload(true);
               }else{

                Materialize.toast(''+alert_type+' Alert has been successfully Deleted', 2000,'green rounded');
                location. reload(true);
               }
              }
              else
              {

               Materialize.toast('Error while processing!', 2000,'red rounded');
              }
            },

          });
      });

      /*-----------For Activity Alerts ------------*/

      var bulk_aalerts = [];

      $("input[id='aalerts_bulk']").on("click",function() {
        if($(this).is(':checked')) {
          $(".aalerts_bulk_action").prop('checked', true);
          bulk_calerts = [];
          $(".aalerts_bulk_action:checked").each(function() {
          bulk_aalerts.push($(this).val());
          });
          bulk_aalerts = bulk_aalerts.join(",");
          $('#deactive_multiple_aalerts').attr('data-multi_aalerts',bulk_calerts);
        } else {
          $('#my-generic-alerts tr').find('input[type="checkbox"]').each(function() {
          $(this).prop('checked', false);
          });
          bulk_aalerts = [];
          $('#deactive_multiple_aalerts').attr('data-multi_aalerts',0);
        }
      });

      $("#deactive_multiple_aalerts").on('click', function() {

        var aalerts_values = $(this).attr('data-multi_aalerts');

        if(aalerts_values == '' || aalerts_values == '0') {
          Materialize.toast('Please select a record first', 2000,'red rounded');
        } else {
          var words = aalerts_values.split(",");
          if(words.length < 2) {
            Materialize.toast('Select two or more records first..!', 2000,'red rounded');
          } else {
            $.ajax({
              url:base_url+'settings/deactive_multiple_aalerts',
              type:"POST",
              data:{'csrf_test_name':csrf_hash,"alerts_values":aalerts_values,"alert_type":"activity","status":"Inactive"},
              success:function(res){
                alertsDatatable(base_path()+'settings/get_activity_alerts/','my-generic-alerts');
                $('select').material_select();
                $('#deactive_multiple_aalerts').attr('data-multi_aalerts',0);
                $('#my-generic-alerts th').find('input[type="checkbox"]').each(function() {
                $(this).prop('checked', false);
                });
                $("#aalerts_bulk").prop('checked', false);
                Materialize.toast('Activity Alerts has been successfully deactivated', 2000,'green rounded');
              },
            });
          }
        }
      });

      $("#email_multiple_aalerts").on('click', function() {

        var aalerts_values = $('#deactive_multiple_aalerts').attr('data-multi_aalerts');
        if(aalerts_values == '' || aalerts_values == '0') {
          Materialize.toast('Please select a record first', 2000,'red rounded');
        } else {
          var words = aalerts_values.split(",");
          if(words.length < 2){
            Materialize.toast('Select two or more records first..!', 2000,'red rounded');
          }else{
            // $.ajax({
            //   url:base_url+'settings/email_multiple_aalerts',
            //   type:"POST",
            //   data:{"alerts_values":aalerts_values},
            //   success:function(res){
            //         aalertsDatatable(base_path()+'settings/get_my_alerts/','my-alerts');
            //         $('select').material_select();
            //         $('#deactive_multiple_calerts').attr('data-multi_calerts',0);
            //         $('#my-alerts th').find('input[type="checkbox"]').each(function() {
            //             $(this).prop('checked', false);
            //           });
            //         Materialize.toast('Clients Alerts has been successfully email', 2000,'green rounded');
            //     },
            // });
            email_alerts(aalerts_values);
          }
        }
      });

      $("body").on('click','#download_multiple_aalerts',function(e) {

        var dnwl_ids = $('#deactive_multiple_aalerts').attr('data-multi_aalerts');

        if(dnwl_ids == '' || dnwl_ids == '0') {
          Materialize.toast('Please select a record first', 2000,'red rounded');
        } else {
          var words = dnwl_ids.split(",");
          if(words.length < 2){
            Materialize.toast('Select two or more records first..!', 2000,'red rounded');
          } else {
            $('#deactive_multiple_aalerts').attr('data-multi_aalerts',0);

            $('#my-generic-alerts th').find('input[type="checkbox"]').each(function() {
              $(this).prop('checked', false);
            });

            $("#aalerts_bulk").prop('checked', false);
            window.location=base_url+'settings/download-multiple-aalerts?ids='+dnwl_ids;
            aalertsDatatable(base_path()+'settings/get_generic_alerts/','my-generic-alerts');
            $('select').material_select();
            Materialize.toast('Clients Alerts has been successfully downloaded', 2000,'green rounded');
          }
        }
      });

      $("body").on('click','#print_multiple_aalerts',function(e) {

        var dnwl_ids = $('#deactive_multiple_aalerts').attr('data-multi_aalerts');

        if(dnwl_ids == '' || dnwl_ids == '0') {
          Materialize.toast('Please select a record first', 2000,'red rounded');
        } else {
          var words = dnwl_ids.split(",");
          if(words.length < 2){
            Materialize.toast('Select two or more records first..!', 2000,'red rounded');
          } else {
            $('#deactive_multiple_calerts').attr('data-multi_calerts',0);
            $('#my-alerts th').find('input[type="checkbox"]').each(function() {
              $(this).prop('checked', false);
            });
            $("#calerts_bulk").prop('checked', false);
            window.location=base_url+'settings/print-multiple-alerts?ids='+dnwl_ids;
            aalertsDatatable(base_path()+'settings/get_my_alerts/','my-alerts');
            $('select').material_select();
            Materialize.toast('Clients Alerts has been successfully print', 2000,'green rounded');
          }
        }
      });

      $('.active_deactive_aalerts').off().on('click', function(){
        var aalert_id = $(this).data('aalerts_id');
        var astatus = $(this).data('astatus');
        var alert_type = $(this).data('alert_type');

        $('#view_deactivate_aalerts').modal('open');

        $('#view_deactivate_aalerts #remove_aalert_id').val(aalert_id);
        $('#view_deactivate_aalerts #alt_status').val(astatus);
        $('#view_deactivate_aalerts #alt_type').val(alert_type);

        if(cstatus!="Active"){
          astatus="Remove";
        }

        $('#view_deactivate_aalerts .astatus').text(astatus);
      });

      $('.active_deactive_aalerts_yes').off().on('click', function(){

        var aalert_id = $('#view_deactivate_aalerts #remove_aalert_id').val();
        var astatus = $('#view_deactivate_aalerts #alt_status').val();
        var alert_type = $('#view_deactivate_aalerts #alt_type').val();

        $.ajax({

          url:base_url+'settings/deactive_aalerts',
          type:"POST",
          data:{'csrf_test_name':csrf_hash,"aalert_id":aalert_id,"alert_type":alert_type,"status":astatus},

          success:function(res){
            if(res == true) {
              if(alt_status=="Active"){
                Materialize.toast(''+alert_type+' Alert has been successfully Activated', 2000,'green rounded');
                location. reload(true);
              }else{
                Materialize.toast(''+alert_type+' Alert has been successfully Deleted', 2000,'green rounded');
                location. reload(true);
              }
            } else {
              Materialize.toast('Error while processing!', 2000,'red rounded');
            }
          },
        });
      });


      /*-----------For Item Alerts ------------*/
      var bulk_ialerts = [];
      $("input[id='ialerts_bulk']").on("click",function(){
      if($(this).is(':checked')) {
        $(".ialerts_bulk_action").prop('checked', true);
        bulk_ialerts = [];
        $(".ialerts_bulk_action:checked").each(function() {
            bulk_ialerts.push($(this).val());
          });
          bulk_ialerts = bulk_ialerts.join(",");
          $('#deactive_multiple_ialerts').attr('data-multi_ialerts',bulk_ialerts);
        }
        else {
          $('#my-item-alerts tr').find('input[type="checkbox"]').each(function() {
          $(this).prop('checked', false);
          });
          bulk_ialerts = [];
          $('#deactive_multiple_ialerts').attr('data-multi_ialerts',0);
        }
    });
      $("#deactive_multiple_ialerts").on('click', function() {
        var ialerts_values = $(this).attr('data-multi_ialerts');
        if(ialerts_values == '' || ialerts_values == '0')
        {
          Materialize.toast('Please select a record first', 2000,'red rounded');
        }
        else
        {
          var words = ialerts_values.split(",");
          if(words.length < 2){
               Materialize.toast('Select two or more records first..!', 2000,'red rounded');
          }else{
            $.ajax({
              url:base_url+'settings/deactive_multiple_alerts',
              type:"POST",
              data:{'csrf_test_name':csrf_hash,"alerts_values":ialerts_values,"alert_type":"services","status":"Inactive"},
              success:function(res){
                    ialertsDatatable(base_path()+'settings/get_item_alerts/','my-item-alerts');
                    $('select').material_select();
                    $('#deactive_multiple_ialerts').attr('data-multi_ialerts',0);
                    $('#my-item-alerts th').find('input[type="checkbox"]').each(function() {
                        $(this).prop('checked', false);
                      });
					  $("#ialerts_bulk").prop('checked', false);
                    Materialize.toast('Item Alerts has been successfully deactivated', 2000,'green rounded');
                },
            });
          }
        }
      });
      $("#email_multiple_ialerts").on('click', function() {
        var ialerts_values = $('#deactive_multiple_ialerts').attr('data-multi_ialerts');
        if(ialerts_values == '' || ialerts_values == '0')
        {
          Materialize.toast('Please select a record first', 2000,'red rounded');
        }
        else
        {
          var words = ialerts_values.split(",");
          if(words.length < 2){
               Materialize.toast('Select two or more records first..!', 2000,'red rounded');
          }else{
            // $.ajax({
            //   url:base_url+'settings/email_multiple_ialerts',
            //   type:"POST",
            //   data:{"alerts_values":ialerts_values},
            //   success:function(res){
            //         ialertsDatatable(base_path()+'settings/get_item_alerts/','my-item-alerts');
            //         $('select').material_select();
            //         $('#deactive_multiple_ialerts').attr('data-multi_ialerts',0);
            //         $('#my-item-alerts th').find('input[type="checkbox"]').each(function() {
            //             $(this).prop('checked', false);
            //           });
            //         Materialize.toast('Item Alerts has been successfully email', 2000,'green rounded');
            //     },
            // });
            email_ialerts(ialerts_values);

          }
        }
      });
     $("body").on('click','#download_multiple_ialerts',function(e) {

      var dnwl_ids = $('#deactive_multiple_ialerts').attr('data-multi_ialerts');

      if(dnwl_ids == '' || dnwl_ids == '0')
      {

        Materialize.toast('Please select a record first', 2000,'red rounded');
      }
      else
      {
        var words = dnwl_ids.split(",");
          if(words.length < 2){
               Materialize.toast('Select two or more records first..!', 2000,'red rounded');
          }else{
            $('#deactive_multiple_ialerts').attr('data-multi_ialerts',0);
            $('#my-item-alerts th').find('input[type="checkbox"]').each(function() {
                $(this).prop('checked', false);
              });
			  $("#ialerts_bulk").prop('checked', false);
            window.location=base_url+'settings/download-multiple-ialerts?ids='+dnwl_ids;
            ialertsDatatable(base_path()+'settings/get_item_alerts/','my-item-alerts');
            $('select').material_select();
            Materialize.toast('Item Alerts has been successfully downloaded', 2000,'green rounded');
          }
      }
    });
    $("body").on('click','#print_multiple_ialerts',function(e) {

      var dnwl_ids = $('#deactive_multiple_ialerts').attr('data-multi_ialerts');

      if(dnwl_ids == '' || dnwl_ids == '0')
      {

        Materialize.toast('Please select a record first', 2000,'red rounded');
      }
      else
      {
        var words = dnwl_ids.split(",");
        if(words.length < 2){
             Materialize.toast('Select two or more records first..!', 2000,'red rounded');
        }else{
          $('#deactive_multiple_ialerts').attr('data-multi_ialerts',0);
          $('#my-item-alerts th').find('input[type="checkbox"]').each(function() {
              $(this).prop('checked', false);
            });
			$("#ialerts_bulk").prop('checked', false);
          window.location=base_url+'settings/print-multiple-ialerts?ids='+dnwl_ids;
          ialertsDatatable(base_path()+'settings/get_item_alerts/','my-item-alerts');
          $('select').material_select();
          Materialize.toast('Item Alerts has been successfully print', 2000,'green rounded');
        }
      }
    });
     /*-----------For Journal Vouchar Alerts ------------*/
     var bulk_jalerts = [];
      $("input[id='jalerts_bulk']").on("click",function(){
      if($(this).is(':checked')) {
        $(".jalerts_bulk_action").prop('checked', true);
        bulk_jalerts = [];
        $(".jalerts_bulk_action:checked").each(function() {
            bulk_jalerts.push($(this).val());
          });
          bulk_jalerts = bulk_jalerts.join(",");
          $('#deactive_multiple_jalerts').attr('data-multi_jalerts',bulk_jalerts);
        }
        else {
          $('#my-jv-alerts tr').find('input[type="checkbox"]').each(function() {
          $(this).prop('checked', false);
          });
          bulk_jalerts = [];
          $('#deactive_multiple_jalerts').attr('data-multi_jalerts',0);
        }
    });
      $("#deactive_multiple_jalerts").on('click', function() {
        var jalerts_values = $(this).attr('data-multi_jalerts');
        if(jalerts_values == '' || jalerts_values == '0')
        {
          Materialize.toast('Please select a record first', 2000,'red rounded');
        }
        else
        {
          var words = jalerts_values.split(",");
          if(words.length < 2){
               Materialize.toast('Select two or more records first..!', 2000,'red rounded');
          }else{
            $.ajax({
              url:base_url+'settings/deactive_multiple_alerts',
              type:"POST",
              data:{'csrf_test_name':csrf_hash,"alerts_values":jalerts_values,"alert_type":"journal_voucher","status":"Inactive"},
              success:function(res){
                    jalertsDatatable(base_path()+'settings/get_jv_alerts/','my-jv-alerts');
                    $('select').material_select();
                    $('#deactive_multiple_jalerts').attr('data-multi_jalerts',0);
                    $('#my-jv-alerts th').find('input[type="checkbox"]').each(function() {
                        $(this).prop('checked', false);
                      });
                    Materialize.toast('Journal Vouchar Alerts has been successfully deactivated', 2000,'green rounded');
                },
            });
          }
        }
      });
      $("#email_multiple_jalerts").on('click', function() {
        var jalerts_values = $('#deactive_multiple_jalerts').attr('data-multi_jalerts');
        if(jalerts_values == '' || jalerts_values == '0')
        {
          Materialize.toast('Please select a record first', 2000,'red rounded');
        }
        else
        {
          var words = jalerts_values.split(",");
          if(words.length < 2){
               Materialize.toast('Select two or more records first..!', 2000,'red rounded');
          }else{
            $.ajax({
              url:base_url+'settings/email_multiple_alerts',
              type:"POST",
              data:{'csrf_test_name':csrf_hash,"alerts_values":jalerts_values},
              success:function(res){
                    jalertsDatatable(base_path()+'settings/get_jv_alerts/','my-jv-alerts');
                    $('select').material_select();
                    $('#deactive_multiple_jalerts').attr('data-multi_jalerts',0);
                    $('#my-jv-alerts th').find('input[type="checkbox"]').each(function() {
                        $(this).prop('checked', false);
                      });
                    Materialize.toast('Journal Vouchar Alerts has been successfully email', 2000,'green rounded');
                },
            });
          }
        }
      });
     $("body").on('click','#download_multiple_jalerts',function(e) {

      var dnwl_ids = $('#deactive_multiple_jalerts').attr('data-multi_jalerts');

      if(dnwl_ids == '' || dnwl_ids == '0')
      {

        Materialize.toast('Please select a record first', 2000,'red rounded');
      }
      else
      {
        var words = dnwl_ids.split(",");
        if(words.length < 2){
             Materialize.toast('Select two or more records first..!', 2000,'red rounded');
        }else{
          $('#deactive_multiple_jalerts').attr('data-multi_jalerts',0);
          $('#my-jv-alerts th').find('input[type="checkbox"]').each(function() {
              $(this).prop('checked', false);
            });
          window.location=base_url+'settings/download-multiple-jvalerts?ids='+dnwl_ids;
          jalertsDatatable(base_path()+'settings/get_jv_alerts/','my-jv-alerts');
          $('select').material_select();
          Materialize.toast('Journal Vouchar Alerts has been successfully downloaded', 2000,'green rounded');
        }
      }
    });
    $("body").on('click','#print_multiple_jalerts',function(e) {

      var dnwl_ids = $('#deactive_multiple_jalerts').attr('data-multi_jalerts');

      if(dnwl_ids == '' || dnwl_ids == '0')
      {

        Materialize.toast('Please select a record first', 2000,'red rounded');
      }
      else
      {
        var words = dnwl_ids.split(",");
        if(words.length < 2){
             Materialize.toast('Select two or more records first..!', 2000,'red rounded');
        }else{
          $('#deactive_multiple_jalerts').attr('data-multi_jalerts',0);
          $('#my-jv-alerts th').find('input[type="checkbox"]').each(function() {
              $(this).prop('checked', false);
            });
          window.location=base_url+'settings/print-multiple-jvalerts?ids='+dnwl_ids;
          jalertsDatatable(base_path()+'settings/get_jv_alerts/','my-jv-alerts');
          $('select').material_select();
          Materialize.toast('Journal Vouchar Alerts has been successfully print', 2000,'green rounded');
        }
      }
    });
     /*-----------For  Credit History Alerts ------------*/
     var bulk_halerts = [];
      $("input[id='halerts_bulk']").on("click",function(){
      if($(this).is(':checked')) {
        $(".halerts_bulk_action").prop('checked', true);
        bulk_halerts = [];
        $(".halerts_bulk_action:checked").each(function() {
            bulk_halerts.push($(this).val());
          });
          bulk_halerts = bulk_halerts.join(",");
          $('#deactive_multiple_halerts').attr('data-multi_halerts',bulk_halerts);
        }
        else {
          $('#my-ch-alerts tr').find('input[type="checkbox"]').each(function() {
          $(this).prop('checked', false);
          });
          bulk_halerts = [];
          $('#deactive_multiple_halerts').attr('data-multi_halerts',0);
        }
    });
      $("#deactive_multiple_halerts").on('click', function() {
        var halerts_values = $(this).attr('data-multi_halerts');
        if(halerts_values == '' || halerts_values == '0')
        {
          Materialize.toast('Please select a record first', 2000,'red rounded');
        }
        else
        {
          var words = halerts_values.split(",");
          if(words.length < 2){
               Materialize.toast('Select two or more records first..!', 2000,'red rounded');
          }else{
            $.ajax({
              url:base_url+'settings/deactive_multiple_alerts',
              type:"POST",
              data:{'csrf_test_name':csrf_hash,"alerts_values":halerts_values,"alert_type":"credit_history","status":"Inactive"},
              success:function(res){
                    halertsDatatable(base_path()+'settings/get_ch_alerts/','my-ch-alerts');
                    $('select').material_select();
                    $('#deactive_multiple_halerts').attr('data-multi_halerts',0);
                    $('#my-ch-alerts th').find('input[type="checkbox"]').each(function() {
                        $(this).prop('checked', false);
                      });
					  $("#halerts_bulk").prop('checked', false);
                    Materialize.toast('Credit History Alerts has been successfully deactivated', 2000,'green rounded');
                },
            });
          }
        }
      });
      $("#email_multiple_halerts").on('click', function() {
        var halerts_values = $('#deactive_multiple_halerts').attr('data-multi_halerts');
        if(halerts_values == '' || halerts_values == '0')
        {
          Materialize.toast('Please select a record first', 2000,'red rounded');
        }
        else
        {
          var words = halerts_values.split(",");
          if(words.length < 2){
               Materialize.toast('Select two or more records first..!', 2000,'red rounded');
          }else{
            // $.ajax({
            //   url:base_url+'settings/email_multiple_cpalerts',
            //   type:"POST",
            //   data:{"alerts_values":halerts_values},
            //   success:function(res){
            //         halertsDatatable(base_path()+'settings/get_ch_alerts/','my-ch-alerts');
            //         $('select').material_select();
            //         $('#deactive_multiple_halerts').attr('data-multi_halerts',0);
            //         $('#my-ch-alerts th').find('input[type="checkbox"]').each(function() {
            //             $(this).prop('checked', false);
            //           });
            //         Materialize.toast('Credit History Alerts has been successfully email', 2000,'green rounded');
            //     },
            // });
            email_cpalerts(halerts_values);
          }
        }
      });
     $("body").on('click','#download_multiple_halerts',function(e) {

      var dnwl_ids = $('#deactive_multiple_halerts').attr('data-multi_halerts');

      if(dnwl_ids == '' || dnwl_ids == '0')
      {

        Materialize.toast('Please select a record first', 2000,'red rounded');
      }
      else
      {
        var words = dnwl_ids.split(",");
        if(words.length < 2){
             Materialize.toast('Select two or more records first..!', 2000,'red rounded');
        }else{
          $('#deactive_multiple_halerts').attr('data-multi_halerts',0);
          $('#my-ch-alerts th').find('input[type="checkbox"]').each(function() {
              $(this).prop('checked', false);
            });
		$("#halerts_bulk").prop('checked', false);
		window.location=base_url+'settings/download-multiple-cpalerts?ids='+dnwl_ids;
          halertsDatatable(base_path()+'settings/get_ch_alerts/','my-ch-alerts');
          $('select').material_select();
          Materialize.toast('Credit History Alerts has been successfully downloaded', 2000,'green rounded');
        }
      }
    });

    $("body").on('click','#print_multiple_halerts',function(e) {

      var dnwl_ids = $('#deactive_multiple_halerts').attr('data-multi_halerts');

      if(dnwl_ids == '' || dnwl_ids == '0')
      {
        Materialize.toast('Please select a record first', 2000,'red rounded');
      }
      else
      {
        var words = dnwl_ids.split(",");
        if(words.length < 2){
             Materialize.toast('Select two or more records first..!', 2000,'red rounded');
        }else{
        $('#deactive_multiple_halerts').attr('data-multi_halerts',0);
          $('#my-ch-alerts th').find('input[type="checkbox"]').each(function() {
              $(this).prop('checked', false);
            });
			$("#halerts_bulk").prop('checked', false);
          window.location=base_url+'settings/print-multiple-cpalerts?ids='+dnwl_ids;
          halertsDatatable(base_path()+'settings/get_ch_alerts/','my-ch-alerts');
          $('select').material_select();
          Materialize.toast('Credit History Alerts has been successfully printed', 2000,'green rounded');
        }
      }
    });

    /*-----------End Credit History Alerts ------------*/
    /*-----------Start Birthday/Anniversary Alerts ------------*/

     var bulk_balerts = [];
      $("input[id='balerts_bulk']").on("click",function(){
      if($(this).is(':checked')) {
        $(".balerts_bulk_action").prop('checked', true);
        bulk_balerts = [];
        $(".balerts_bulk_action:checked").each(function() {
            bulk_balerts.push($(this).val());
          });
          bulk_balerts = bulk_balerts.join(",");
          $('#deactive_multiple_balerts').attr('data-multi_balerts',bulk_balerts);
        }
        else {
          $('#my-ba-alerts tr').find('input[type="checkbox"]').each(function() {
          $(this).prop('checked', false);
          });
          bulk_balerts = [];
          $('#deactive_multiple_balerts').attr('data-multi_balerts',0);
        }
    });
      $("#deactive_multiple_balerts").on('click', function() {
        var balerts_values = $(this).attr('data-multi_balerts');
        if(balerts_values == '' || balerts_values == '0')
        {
          Materialize.toast('Please select a record first', 2000,'red rounded');
        }
        else
        {
          var words = balerts_values.split(",");
          if(words.length < 2){
               Materialize.toast('Select two or more records first..!', 2000,'red rounded');
          }else{
            $.ajax({
              url:base_url+'settings/deactive_multiple_alerts',
              type:"POST",
              data:{'csrf_test_name':csrf_hash,"alerts_values":balerts_values,"alert_type":"birthday_anniversary","status":"Inactive"},
              success:function(res){
                    balertsDatatable(base_path()+'settings/get_ba_alerts/','my-ba-alerts');
                    $('select').material_select();
                    $('#deactive_multiple_balerts').attr('data-multi_balerts',0);
                    $('#my-ba-alerts th').find('input[type="checkbox"]').each(function() {
                        $(this).prop('checked', false);
                      });
					  $("#balerts_bulk").prop('checked', false);
                    Materialize.toast('Birthday alert deactivated successfully', 2000,'green rounded');
                },
            });
          }
        }
      });
      $("#email_multiple_balerts").on('click', function() {
        var balerts_values = $('#deactive_multiple_balerts').attr('data-multi_balerts');
        if(balerts_values == '' || balerts_values == '0')
        {
          Materialize.toast('Please select a record first', 2000,'red rounded');
        }
        else
        {
          var words = balerts_values.split(",");
          if(words.length < 2){
               Materialize.toast('Select two or more records first..!', 2000,'red rounded');
          }else{
            // $.ajax({
            //   url:base_url+'settings/email_multiple_baalerts',
            //   type:"POST",
            //   data:{"alerts_values":balerts_values},
            //   success:function(res){
            //         balertsDatatable(base_path()+'settings/get_ba_alerts/','my-ba-alerts');
            //         $('select').material_select();
            //         $('#deactive_multiple_balerts').attr('data-multi_balerts',0);
            //         $('#my-ba-alerts th').find('input[type="checkbox"]').each(function() {
            //             $(this).prop('checked', false);
            //           });
            //         Materialize.toast('Birthday/Anniversary Alerts has been successfully email', 2000,'green rounded');
            //     },
            // });
            email_baalerts(balerts_values);
          }
        }
      });
     $("body").on('click','#download_multiple_balerts',function(e) {

      var dnwl_ids = $('#deactive_multiple_balerts').attr('data-multi_balerts');

      if(dnwl_ids == '' || dnwl_ids == '0')
      {

        Materialize.toast('Please select a record first', 2000,'red rounded');
      }
      else
      {
        var words = dnwl_ids.split(",");
        if(words.length < 0){
             Materialize.toast('Select two or more records first..!', 2000,'red rounded');
        }else{
          $('#deactive_multiple_balerts').attr('data-multi_balerts',0);
          $('#my-ba-alerts th').find('input[type="checkbox"]').each(function() {
              $(this).prop('checked', false);
            });
			$('#balerts_bulk').prop('checked', false);
          window.location=base_url+'settings/download-multiple-baalerts?ids='+dnwl_ids;
          balertsDatatable(base_path()+'settings/get_ba_alerts/','my-ba-alerts');
          $('select').material_select();
          Materialize.toast('Birthday/Anniversary Alerts has been successfully downloaded', 2000,'green rounded');
        }
      }
    });
    $("body").on('click','#print_multiple_balerts',function(e) {

      var dnwl_ids = $('#deactive_multiple_balerts').attr('data-multi_balerts');

      if(dnwl_ids == '' || dnwl_ids == '0')
      {

        Materialize.toast('Please select a record first', 2000,'red rounded');
      }
      else
      {
        var words = dnwl_ids.split(",");
        if(words.length < 0){
             Materialize.toast('Select two or more records first..!', 2000,'red rounded');
        }else{
          $('#deactive_multiple_balerts').attr('data-multi_balerts',0);
          $('#my-ba-alerts th').find('input[type="checkbox"]').each(function() {
              $(this).prop('checked', false);
            });
			$('#balerts_bulk').prop('checked', false);
          balertsDatatable(base_path()+'settings/get_ba_alerts/','my-ba-alerts');
          $('select').material_select();
          window.location=base_url+'settings/print-multiple-baalerts?ids='+dnwl_ids;
          Materialize.toast('Birthday/Anniversary Alerts has been successfully print', 2000,'green rounded');
        }
      }
    });
     /*-----------End Birthday/Anniversary Alerts ------------*/
 });

$.validator.addMethod("alert_time",
    function(value, element) {
        return this.optional(element) ||/^([0-1]?[0-9]|2[0-4]):([0-5][0-9])(:[0-5][0-9])?$/.test(inputField.value);
  }, "Time Format is not proper." );

$(function(){
  $("#add_activity_alert,#edit_activity_alert").validate({

    rules: {

      alert_number: {
        required: true,
      },

      alert_date: {
        required: true,
      },

      alert_date_time: {
        required: true,
      },

      alert_time: {
        required: true,
      },

      alert_reminder: {
        required: true,
      },

      alert_mobile: {
        maxlength: 10,
        required: function(e) {
          if($('.msg').hasClass('selected_notification')) {
            return true;
          } else {
            return false;
          }
        },
      },

      alert_email: {
        email: true,
        required: function(e) {
          if($('.email').hasClass('selected_notification')) {
            return true;
          } else {
            return false;
          }
        },
      },

      alert_notification: {
        required: function(e) {
          if($('#alert_notification').val()=='' && $('#alert_msg').val()=='' && $('#alert_mail').val()=='') {
            return true;
          } else {
            return false;
          }
        }
      },

    },

    messages: {

      alert_number: {
        required:"Alert Number is required",
      },

      alert_date:{
        required:"Alert Date is required",
      },

      alert_date_time:{
        required:"Alert Date is required",
      },

      alert_time: {
        required: "Alert Time is required",
      },

      alert_reminder:{
        required:"Alert Reminder is required",
      },

      alert_notification:{
        required:"Alert Type is required",
      },

    },

    errorPlacement: function(error, element) {
      if(element.prop('name')  == 'alert_number') {
        error.insertAfter(element.closest('.input-field').find('.js-example-basic-single'));
        Materialize.toast('Please fill the mandatory cells', 4000,'red rounded');
      } else if(element.attr("name") == "alert_notification") {
        $("#notification_error").html(error)
      } else {
        error.insertAfter(element);
        element.closest('.input-field').find('label.error').addClass('active');
      }
    },

    submitHandler: function(form) {
		$("input[name='csrf_test_name']").val(csrf_hash);
      form.submit();
    },

  });
});

$(function() {
  $("#add_customer_alert,#edit_customer_alert").validate({
      rules: {
        alert_number:{

            required:true,
          },

        alert_date:{

            required:true,
        },
        parent_id:{

            required:true,
        },

        alert_reminder:{

            required:true,
        },
        alert_target:{

            required:true,
        },
        alert_target:{

            required:true,
        },
        alert_mobile:{
			maxlength:10,
			required:function(e){
              if($('.msg').hasClass('selected_notification')){
                return true;
              }else{
                return false;
              }
			},
          },
        alert_email:{
          email:true,
		  required:function(e){
              if($('.email').hasClass('selected_notification')){
                return true;
              }else{
                return false;
              }
		  },
        },
        alert_notification:{

            required:function(e){
            	if($('#alert_notification').val()=='' && $('#alert_msg').val()=='' && $('#alert_mail').val()==''){
            		return true;
            	}else{
            		return false;
            	}
            }
        },

      },
      messages:{

      	 alert_number:{

            required:"Alert Number is required",

          },
          alert_date:{

            required:"Alert Date is required",

          },

          parent_id:{

            required:"Please Select a Client",
          },
          alert_reminder:{

            required:"Reminder Date is required",
          },
          alert_target:{

            required:"Set Target is required",
          },
		  alert_mobile:{
			//required:"Mobile Number is required",
		  },
          alert_notification:{

            required:"Alert Type is required",
          },
        },
        errorPlacement: function(error, element) {
         if (element.prop('name')  == 'parent_id') {

          error.insertAfter(element.closest('.input-field').find('.js-example-basic-single'));
		  Materialize.toast('Please fill the mandatory cells', 4000,'red rounded');
          }else if (element.attr("name") == "alert_notification") {
           	  $("#notification_error").html(error)
           }else{
           	error.insertAfter(element);
            element.closest('.input-field').find('label.error').addClass('active');
           }
        },
      submitHandler: function(form) {
		$("input[name='csrf_test_name']").val(csrf_hash);
        form.submit();
      },

    });
  });
 /*---Start Validation of Item Alert ---*/
$(function() {
  $("#add_item_alert,#edit_item_alert").validate({
      rules: {
        alert_number:{

            required:true,
          },

        alert_date:{

            required:true,
        },
        parent_id:{

            required:true,
        },

        alert_reminder:{

            required:true,
        },
        alert_target:{

            required:true,
        },
        alert_target:{

            required:true,
        },
        alert_mobile:{
            //required:true,
			maxlength:10,
			required:function(e){
              if($('.msg').hasClass('selected_notification')){
                return true;
              }else{
                return false;
              }
			},
          },
        alert_email:{
          email:true,
		  required:function(e){
              if($('.email').hasClass('selected_notification')){
                return true;
              }else{
                return false;
              }
		  },
        },
        alert_notification:{

            required:function(e){
              if($('#alert_notification').val()=='' && $('#alert_msg').val()=='' && $('#alert_mail').val()==''){
                return true;
              }else{
                return false;
              }
            }
        },

      },
      messages:{

         alert_number:{

            required:"Alert Number is required",

          },
          alert_date:{

            required:"Alert Date is required",

          },

          parent_id:{

            required:"Please Select a Item",
          },
          alert_reminder:{

            required:"Reminder Date is required",
          },
		  alert_mobile:{
			//required:"Mobile number is required",
		  },
          alert_target:{

            required:"Set Target is required",
          },
          alert_notification:{

            required:"Alert Type is required",
          },
        },
        errorPlacement: function(error, element) {
         if (element.prop('name')  == 'parent_id') {

          error.insertAfter(element.closest('.input-field').find('.js-example-basic-single'));
		  Materialize.toast('Please fill the mandatory cells', 4000,'red rounded');
          }else if (element.attr("name") == "alert_notification") {
              $("#notification_error").html(error)
           }else{
            error.insertAfter(element);
            element.closest('.input-field').find('label.error').addClass('active');
           }
        },
      submitHandler: function(form) {
		$("input[name='csrf_test_name']").val(csrf_hash);
        form.submit();
      },

    });
  });
/*---End Validation of Item Alert---*/
/*---Start Validation of Journal Vouchar---*/
$(function() {
  $("#add_jv_alert,#edit_jv_alert").validate({
      rules: {
        alert_number:{

            required:true,
          },

        alert_date:{

            required:true,
        },
        parent_id:{

            required:true,
        },

        alert_reminder:{

            required:true,
        },
        alert_mobile:{
            //required:true,
			 maxlength:10,
          },
        alert_email:{
          email:true,
        },
        alert_notification:{

            required:function(e){
              if($('#alert_notification').val()=='' && $('#alert_msg').val()=='' && $('#alert_mail').val()==''){
                return true;
              }else{
                return false;
              }
            }
        },

      },
      messages:{

         alert_number:{

            required:"Alert Number is required",

          },
          alert_date:{

            required:"Alert Date is required",

          },

          parent_id:{

            required:"Please Select a Module",
          },

          alert_reminder:{

            required:"Reminder Date is required",
          },
		  alert_mobile:{
			  //required:"Mobile Number is required",
		  },
          alert_notification:{

            required:"Alert Type is required",
          },
        },
        errorPlacement: function(error, element) {
           if (element.prop('name')  == 'parent_id') {

          error.insertAfter(element.closest('.input-field').find('.js-example-basic-single'));
          }else if (element.attr("name") == "alert_notification") {
              $("#notification_error").html(error)
           }else{
            error.insertAfter(element);
            element.closest('.input-field').find('label.error').addClass('active');
           }
        },
      submitHandler: function(form) {
		$("input[name='csrf_test_name']").val(csrf_hash);
        form.submit();
      },

    });
  });
 /*---End Validation of Journal Vouchar---*/
 /*---Start Validation of Credit History ---*/
$(function() {
  $("#add_ch_alert,#edit_ch_alert").validate({
      rules: {
        alert_number:{

            required:true,
          },

        alert_date:{

            required:true,
        },
        parent_id:{

            required:true,
        },

        alert_reminder:{

            required:true,
        },
        alert_mobile:{
            //required:true,
			//maxlength:10,
			//mobileno:true,
			required:function(e){
              if($('.msg').hasClass('selected_notification')){
                return true;
              }else{
                return false;
              }
            }

          },
        alert_email:{
		  required:function(e){
              if($('.email').hasClass('selected_notification')){
                return true;
              }else{
                return false;
              }
          },
		  email:true,
        },
        alert_notification:{

            required:function(e){
              if($('#alert_notification').val()=='' && $('#alert_msg').val()=='' && $('#alert_mail').val()==''){
                return true;
              }else{
                return false;
              }
            }
        },

      },
      messages:{

         alert_number:{

            required:"Alert Number is required",

          },
          alert_date:{

            required:"Alert Date is required",

          },

          parent_id:{

            required:"Please Select a Client",
          },

          alert_reminder:{

            required:"Reminder is required",
          },
		  alert_mobile:{
            required:"Mobile Number is required",
          },
          alert_notification:{

            required:"Alert Type is required",
          },
        },
        errorPlacement: function(error, element) {
           if (element.prop('name')  == 'parent_id') {

          error.insertAfter(element.closest('.input-field').find('.js-example-basic-single'));
		  Materialize.toast('Please fill the mandatory cells', 4000,'red rounded');
          }else if (element.attr("name") == "alert_notification") {
              $("#notification_error").html(error)
           }else{
            error.insertAfter(element);
            element.closest('.input-field').find('label.error').addClass('active');
           }
        },
      submitHandler: function(form) {
		$("input[name='csrf_test_name']").val(csrf_hash);
        form.submit();
      },

    });
  });
 /*---End Validation of Credit History---*/
 $(function() {
  $("#add_vendor_alert,#edit_vendor_alert").validate({
      rules: {
        alert_number:{

            required:true,
          },

        alert_date:{

            required:true,
        },
        alert_name:{

            required:true,
        },
         alert_target:{

            required:true,
        },
        alert_reminder:{

            required:true,
        },


        alert_mobile:{
            //required:true,
			 maxlength:10,
          },
        alert_email:{
          email:true,
        },
        alert_notification:{

            required:function(e){
              if($('#alert_notification').val()=='' && $('#alert_msg').val()=='' && $('#alert_mail').val()==''){
                return true;
              }else{
                return false;
              }
            }
        },

      },
      messages:{

         alert_number:{

            required:"Alert Number is required",

          },
          alert_date:{

            required:"Alert Date is required",

          },

          alert_target:{

            required:"Set Target is required",
        },
          alert_reminder:{

            required:"Reminder is required",
          },
		  alert_mobile:{
			  //required:"Mobile Number is required",
		  },
          alert_notification:{

            required:"Alert Type is required",
          },
        },
        errorPlacement: function(error, element) {
           if (element.prop('name')  == 'vendor_id') {

          error.insertAfter(element.closest('.input-field').find('.js-example-basic-single'));
      Materialize.toast('Please fill the mandatory cells', 4000,'red rounded');
          }else if (element.attr("name") == "alert_notification") {
              $("#notification_error").html(error)
           }else{
            error.insertAfter(element);
            element.closest('.input-field').find('label.error').addClass('active');
                 Materialize.toast('Vendor Alerts has been successfully print', 2000,'green rounded');
           }
        },
      submitHandler: function(form) {
		$("input[name='csrf_test_name']").val(csrf_hash);
        form.submit();
      },

    });
  });

 /*---Start Validation of Birthday/Anniversary---*/

 $(function() {
  $("#add_ba_alert,#edit_ba_alert").validate({
      rules: {
        alert_number:{

            required:true,
          },

        alert_date:{

            required:true,
        },
        alert_occasion:{

            required:true,
        },
        occasion_date:{

            required:true,
        },
        alert_stakeholder:{

            required:true,
        },
        alert_name:{

            required:true,
        },
        parent_id:{
			required: {
				depends: function(element){
					return $("#alert_stakeholder option:selected").val()=="client";
				}
			}
        },
        alert_reminder:{

            required:true,
        },
        alert_mobile:{
            //required:true,
			maxlength:10,
			required:function(e){
              if($('.msg').hasClass('selected_notification')){
                return true;
              }else{
                return false;
              }
			},
          },
        alert_email:{
          email:true,
		  required:function(e){
              if($('.email').hasClass('selected_notification')){
                return true;
              }else{
                return false;
              }
			},
        },
		emp_code:{
			required: {
				depends: function(element){
					return $("#alert_stakeholder option:selected").val()=="employee";
				}
			}
		},
		contact_per:{
			required: {
				depends: function(element){
					return $("#alert_stakeholder option:selected").val()=="client";
				}
			}
		},
        alert_notification:{

            required:function(e){
              if($('#alert_notification').val()=='' && $('#alert_msg').val()=='' && $('#alert_mail').val()==''){
                return true;
              }else{
                return false;
              }
            }
        },

      },
      messages:{

         alert_number:{

            required:"Alert Number is required",

          },
          alert_date:{

            required:"Alert Date is required",

          },
          alert_occasion:{

            required:"Occasion is required",

          },
          occasion_date:{

            required:"Occasion Date is required",

          },
          alert_stakeholder:{

            required:"Please Select a Stakeholder",
          },
           alert_name:{

             required:"Contact Person Name Is required",
          },
          parent_id:{

            required:"Please Select a Client",
          },
          alert_reminder:{

            required:"Reminder Date is required",
          },
		  alert_mobile:{
			  //required:"Mobile Number is required",
		  },
          alert_notification:{

            required:"Alert Type is required",
          },
		  contact_per:{
			  required:"Contact person name is required",
		  },
		  emp_code:{
			  required:"Employee Name & Code is required",
		  },
        },
        errorPlacement: function(error, element) {
          /*if (element.prop('name')  == 'alert_occasion') {

			error.insertAfter(element.closest('.input-field').find('.country-dropdown'));
			Materialize.toast('Please fill the mandatory cells', 4000,'red rounded');
          }else*/ if (element.attr("name") == "alert_notification") {
              $("#notification_error").html(error)
           }else{
            error.insertAfter(element);
            element.closest('.input-field').find('label.error').addClass('active');
           }
        },
      submitHandler: function(form) {
		  $("input[name='csrf_test_name']").val(csrf_hash);
        form.submit();
      },

    });
  });


function aalertsDatatable(url,id){

  $('#'+id).DataTable().destroy();

  table_aalerts = $('#'+id).dataTable({

    "scrollY":299,
    "bServerSide": true,
    "bProcessing": true,
    "bDeferRender": true,
    "aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0 , 1 , 2, 3 , 4 ,5, 6] }],
    "aoColumns": [
      { "bSortable": false },
      { "bSortable": false },
      { "bSortable": false },
      { "bSortable": false },
      { "bSortable": false },
      { "bSortable": false },
      { "bSortable": false },
    ],

    oLanguage: { sLengthMenu: "_MENU_", },

    "order": [0,'DESC'],
    "bPaginate": true,
    "bLengthChange": true,
    "bFilter": true,
    "sDom": 'Rfrtlip',
    "bInfo": false,
    "searching":false,
    "iDisplayLength": 10,

    lengthMenu: [
      [ 10, 20, 30, 50, -1 ],
      [ 'Show 10', 'Show 20', 'Show 30', 'Show 50', 'Show all' ]
    ],

    "ajax": {
      "url": url,
      "type": "POST",
      "data": {'csrf_test_name':csrf_hash,'search': $("#search_aalerts").val(),'aalerts_start_date':$("#aalerts_start_date").val(),'aalerts_end_date':$("#aalerts_end_date").val(), 'alert_type':'activity'},
    },

    "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
      $('td:first-child', nRow).addClass('bulk');
      $('td:last-child', nRow).addClass('action-tab');
      $("#fixedHeader").removeClass('sticky');
    },

    "fnDrawCallback": function() {
      $('.dropdown-button, .translation-button, .dropdown-menu').dropdown({
        inDuration: 300,
        outDuration: 225,
        constrainWidth: false,
        hover: true,
        gutter: 0,
        belowOrigin: true,
        alignment: 'left',
        stopPropagation: false
      });

      $('.deactive_aalerts').off().on('click', function(){
        //alert("hi");
        var aalert_id = $(this).data('aalert_id');
        console.log(aalert_id);
        $('#deactivate_aalerts').modal('open');
        $('#deactivate_aalerts #remove_aalert_id').val(aalert_id);
      });

      $('.delete_aalerts').off().on('click', function(){
        var aalert_id = $('#deactivate_aalerts #remove_aalert_id').val();
        $.ajax({

          url:base_url+'settings/deactive_aalerts',
          type:"POST",
          data:{'csrf_test_name':csrf_hash,"aalert_id":aalert_id,"alert_type":"activity","status":'Inactive'},

          success:function(res){
            if(res == true) {
              aalertsDatatable(base_path()+'settings/get_generic_alerts/','my-generic-alerts');
              $('select').material_select();
              Materialize.toast('Activity Alert has been successfully deleted', 2000,'green rounded');
            } else {
             aalertsDatatable(base_path()+'settings/get_generic_alerts/','my-generic-alerts');
             $('select').material_select();
             Materialize.toast('Error while processing!', 2000,'red rounded');
            }
          },
        });
      });

      var aalerts = [];
      var aalerts_values = "";

      $(".aalerts_bulk_action").on('click', function(e) {
        if($(this).is(':checked')){
          var total_checked = $(".aalerts_bulk_action:checked").length;
          aalerts=[];
          $(".aalerts_bulk_action:checked").each(function() {
            aalerts.push($(this).val());
          });
        } else {
          aalerts=$('#deactive_multiple_aalerts').attr('data-multi_aalerts').split(',');
          $(this).prop('checked',false);
          var remove_id = $(this).val();
          aalerts = jQuery.grep(aalerts, function(value) {
            return value != remove_id;
          });
        }
        aalerts_values = aalerts.join(",");
        $('#deactive_multiple_aalerts').attr('data-multi_aalerts',aalerts_values);
      });
    },
  });
}


function alertsDatatable(url,id){

  $('#'+id).DataTable().destroy();
 // $('#deactive_multiple_invoices').attr('data-multi_invoices',0);

  var customer = $('#calerts_customer option:selected').text();
  var str = customer;
  var res = str.substring(0,3);
  if (res == "VDR") {
    var alert_type="vendors";
  }
  else{
    var alert_type="customers";
  }
  //alert(res);

  table_alerts = $('#'+id).dataTable({
	"scrollY":299,
   "bServerSide": true,
  "bProcessing": true,
  "bDeferRender": true,
    "aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0 , 1 , 2, 3 , 4 ,5, 6, 7,8] }],
   "aoColumns": [
    { "bSortable": false },
    { "bSortable": false },
    { "bSortable": false },
    { "bSortable":  true},
    { "bSortable": true },
    { "bSortable": false },
    { "bSortable": false },
    { "bSortable": false },

  ],
  oLanguage: { sLengthMenu: "_MENU_", },
    "order": [0,'DESC'],
    "bPaginate": true,
    "bLengthChange": true,
    "bFilter": true,
    "sDom": 'Rfrtlip',
    "bInfo": false,
    "searching":false,
  "iDisplayLength": 10,
    lengthMenu: [
            [ 10, 20, 30, 50, -1 ],
            [ 'Show 10', 'Show 20', 'Show 30', 'Show 50', 'Show all' ]
        ],

  "ajax": {
      "url": url,
      "type": "POST",
      "data": {'csrf_test_name':csrf_hash,'search': $("#search_calerts").val(),'calerts_customer':$("#calerts_customer").val(),'calerts_start_date':$("#calerts_start_date").val(),'calerts_end_date':$("#calerts_end_date").val(), 'alert_type':alert_type},

    },
	"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
		$('td:first-child', nRow).addClass('bulk');
		$('td:last-child', nRow).addClass('action-tab');
		$("#fixedHeader").removeClass('sticky');
	},
	"fnDrawCallback": function () {
		$('.dropdown-button, .translation-button, .dropdown-menu').dropdown({
              inDuration: 300,
              outDuration: 225,
              constrainWidth: false,
              hover: true,
              gutter: 0,
              belowOrigin: true,
              alignment: 'left',
              stopPropagation: false
            });

               //var num = table_alerts.fnSettings().fnRecordsTotal();
        //$('#alert_count').html(num);

      $('.deactive_calerts').off().on('click', function(){
          var calert_id = $(this).data('calert_id');
          console.log(calert_id);
         $('#deactivate_calerts').modal('open');

         $('#deactivate_calerts #remove_calert_id').val(calert_id);
        });
      $('.delete_calerts').off().on('click', function(){
        var calert_id = $('#deactivate_calerts #remove_calert_id').val();
        $.ajax({

          url:base_url+'settings/deactive_alerts',

          type:"POST",

          data:{'csrf_test_name':csrf_hash,"calert_id":calert_id,"alert_type":"customers","status":'Inactive'},

          success:function(res){
              if(res == true)
              {
                alertsDatatable(base_path()+'settings/get_my_alerts/','my-alerts');
                $('select').material_select();
                Materialize.toast('Customer Alert has been successfully deactivated', 2000,'green rounded');
              }
              else
              {
               alertsDatatable(base_path()+'settings/get_my_alerts/','my-alerts');
               $('select').material_select();
               Materialize.toast('Error while processing!', 2000,'red rounded');
              }
            },

          });
      });


      var calerts = [];
        var calerts_values = "";
        $(".calerts_bulk_action").on('click', function(e) {
          if($(this).is(':checked')){
            var total_checked = $(".calerts_bulk_action:checked").length;
              calerts=[];
              $(".calerts_bulk_action:checked").each(function() {
                calerts.push($(this).val());
              });
          }
          else
          {
            calerts=$('#deactive_multiple_calerts').attr('data-multi_calerts').split(',');
            $(this).prop('checked',false);
            var remove_id = $(this).val();
            calerts = jQuery.grep(calerts, function(value) {
              return value != remove_id;
            });
          }
            calerts_values = calerts.join(",");
            $('#deactive_multiple_calerts').attr('data-multi_calerts',calerts_values);
          });
   },
  });
}
function ialertsDatatable(url,id){

  $('#'+id).DataTable().destroy();
 // $('#deactive_multiple_invoices').attr('data-multi_invoices',0);



  table_ialerts = $('#'+id).dataTable({
	"scrollY":299,
   "bServerSide": true,
  "bProcessing": true,
  "bDeferRender": true,
    "aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0 , 1 , 2, 3 , 4 ,5, 6, 7,8] }],
   "aoColumns": [
    { "bSortable": false },
    { "bSortable": false },
    { "bSortable": false },
    { "bSortable":  true},
    { "bSortable": true},
    { "bSortable": false},
    { "bSortable": false },
    { "bSortable": false },

  ],
    oLanguage: { sLengthMenu: "_MENU_", },
    "order": [0,'DESC'],
    "bPaginate": true,
    "bLengthChange": true,
    "bFilter": true,
    "sDom": 'Rfrtlip',
    "bInfo": false,
    "searching":false,
  "iDisplayLength": 10,
    lengthMenu: [
            [ 10, 20, 30, 50, -1 ],
            [ 'Show 10', 'Show 20', 'Show 30', 'Show 50', 'Show all' ]
        ],
  //"aaSorting": [[1, "desc" ]],
  "ajax": {
      "url": url,
      "type": "POST",
      "data": {'csrf_test_name':csrf_hash,'search': $("#search_ialerts").val(),'ialerts_customer':$("#ialerts_customer").val(),'ialerts_start_date':$("#ialerts_start_date").val(),'ialerts_end_date':$("#ialerts_end_date").val()},

    },
  "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {


    $('td:first-child', nRow).addClass('bulk');
    $('td:last-child', nRow).addClass('action-tab');
	$("#fixedHeader").removeClass('sticky');
     },
   "fnDrawCallback": function () {
      $('.dropdown-button, .translation-button, .dropdown-menu').dropdown({
              inDuration: 300,
              outDuration: 225,
              constrainWidth: false,
              hover: true,
              gutter: 0,
              belowOrigin: true,
              alignment: 'left',
              stopPropagation: false
            });

      $('.deactive_ialerts').off().on('click', function(){
          var ialert_id = $(this).data('ialert_id');
         $('#deactivate_ialerts').modal('open');

         $('#deactivate_ialerts #remove_ialert_id').val(ialert_id);
        });
      $('.delete_ialerts').off().on('click', function(){
        var ialert_id = $('#deactivate_ialerts #remove_ialert_id').val();
        $.ajax({

          url:base_url+'settings/deactive_alerts',

          type:"POST",

          data:{'csrf_test_name':csrf_hash,"calert_id":ialert_id,"alert_type":"services","status":'Inactive'},

          success:function(res){
              if(res == true)
              {
                ialertsDatatable(base_path()+'settings/get_item_alerts/','my-item-alerts');
                $('select').material_select();
                Materialize.toast('Item Alert has been successfully deactivated', 2000,'green rounded');
              }
              else
              {
               ialertsDatatable(base_path()+'settings/get_item_alerts/','my-item-alerts');
               $('select').material_select();
               Materialize.toast('Error while processing!', 2000,'red rounded');
              }
            },

          });
      });


      var ialerts = [];
        var ialerts_values = "";
        $(".ialerts_bulk_action").on('click', function(e) {
          if($(this).is(':checked')){
            var total_checked = $(".ialerts_bulk_action:checked").length;
              ialerts=[];
              $(".ialerts_bulk_action:checked").each(function() {
                ialerts.push($(this).val());
              });
          }
          else
          {
            ialerts=$('#deactive_multiple_ialerts').attr('data-multi_ialerts').split(',');
            $(this).prop('checked',false);
            var remove_id = $(this).val();
            ialerts = jQuery.grep(ialerts, function(value) {
              return value != remove_id;
            });
          }
            ialerts_values = ialerts.join(",");
            $('#deactive_multiple_ialerts').attr('data-multi_ialerts',ialerts_values);
          });
   },
  });
}
function jalertsDatatable(url,id){

  $('#'+id).DataTable().destroy();
 // $('#deactive_multiple_invoices').attr('data-multi_invoices',0);
  table_ialerts = $('#'+id).dataTable({
   "bServerSide": true,
  "bProcessing": true,
  "bDeferRender": true,
    "aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0 , 1 , 2, 3 , 4 ,5, 6] }],
   "aoColumns": [
    { "bSortable": false },
    { "bSortable": false },
    { "bSortable": true },
    { "bSortable":  true},
    { "bSortable": true },
    { "bSortable": false },
    { "bSortable": false },

  ],
    oLanguage: { sLengthMenu: "_MENU_", },
    "order": [0,'DESC'],
    "bPaginate": true,
    "bLengthChange": true,
    "sDom": 'Rfrtlip',
    "bFilter": true,
    "bInfo": false,
    "sDom": 'Rfrtlip',
    "searching":false,
  "iDisplayLength": 10,
    lengthMenu: [
            [ 10, 20, 30, 50, -1 ],
            [ 'Show 10', 'Show 20', 'Show 30', 'Show 50', 'Show all' ]
        ],
  "aaSorting": [[1, "desc" ]],
  "ajax": {
      "url": url,
      "type": "POST",
      "data": {'csrf_test_name':csrf_hash,'search': $("#search_jalerts").val(),'jalerts_customer':$("#jalerts_customer").val(),'jalerts_start_date':$("#jalerts_start_date").val(),'jalerts_end_date':$("#jalerts_end_date").val()},

    },
  "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {

    $('td:first-child', nRow).addClass('bulk');
    $('td:last-child', nRow).addClass('action-tab');
	$("#fixedHeader").removeClass('sticky');
     },
   "fnDrawCallback": function () {
      $('.dropdown-button, .translation-button, .dropdown-menu').dropdown({
              inDuration: 300,
              outDuration: 225,
              constrainWidth: false,
              hover: true,
              gutter: 0,
              belowOrigin: true,
              alignment: 'left',
              stopPropagation: false
            });


      $('.deactive_jalerts').off().on('click', function(){
          var jalert_id = $(this).data('jalert_id');
         $('#deactivate_jalerts').modal('open');

         $('#deactivate_jalerts #remove_jalert_id').val(jalert_id);
        });
      $('.delete_jalerts').off().on('click', function(){
        var jalert_id = $('#deactivate_jalerts #remove_jalert_id').val();
        $.ajax({

          url:base_url+'settings/deactive_alerts',

          type:"POST",

          data:{'csrf_test_name':csrf_hash,"calert_id":jalert_id,"alert_type":"journal_voucher","status":'Inactive'},

          success:function(res){
              if(res == true)
              {
                jalertsDatatable(base_path()+'settings/get_jv_alerts/','my-item-alerts');
                $('select').material_select();
                Materialize.toast('Journal Vouchar Alert has been successfully deactivated', 2000,'green rounded');
              }
              else
              {
               jalertsDatatable(base_path()+'settings/get_jv_alerts/','my-item-alerts');
               $('select').material_select();
               Materialize.toast('Error while processing!', 2000,'red rounded');
              }
            },

          });
      });


      var jalerts = [];
        var jalerts_values = "";
        $(".jalerts_bulk_action").on('click', function(e) {
          if($(this).is(':checked')){
            var total_checked = $(".jalerts_bulk_action:checked").length;
              jalerts=[];
              $(".jalerts_bulk_action:checked").each(function() {
                jalerts.push($(this).val());
              });
          }
          else
          {
            jalerts=$('#deactive_multiple_jalerts').attr('data-multi_jalerts').split(',');
            $(this).prop('checked',false);
            var remove_id = $(this).val();
            jalerts = jQuery.grep(jalerts, function(value) {
              return value != remove_id;
            });
          }
            jalerts_values = i=jalerts.join(",");
            $('#deactive_multiple_jalerts').attr('data-multi_jalerts',jalerts_values);
          });
   },
  });
}

function valertsDatatable(url,id){

  $('#'+id).DataTable().destroy();
 // $('#deactive_multiple_invoices').attr('data-multi_invoices',0);
  table_alerts = $('#'+id).dataTable({
  "scrollY":299,
   "bServerSide": true,
  "bProcessing": true,
  "bDeferRender": true,
    "aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0 , 1 , 2, 3 , 4 ,5, 6, 7,8] }],
   "aoColumns": [
    { "bSortable": false },
    { "bSortable": false },
    { "bSortable": false },
    { "bSortable":  true},
    { "bSortable": true },
    { "bSortable": false },
    { "bSortable": false },
    { "bSortable": false },

  ],
  oLanguage: { sLengthMenu: "_MENU_", },
    "order": [0,'DESC'],
    "bPaginate": true,
    "bLengthChange": true,
    "bFilter": true,
    "sDom": 'Rfrtlip',
    "bInfo": false,
    "searching":false,
  "iDisplayLength": 10,
    lengthMenu: [
            [ 10, 20, 30, 50, -1 ],
            [ 'Show 10', 'Show 20', 'Show 30', 'Show 50', 'Show all' ]
        ],

  "ajax": {
      "url": url,
      "type": "POST",
      "data": {'csrf_test_name':csrf_hash,'search': $("#search_valerts").val(),'valerts_customer':$("#valerts_customer").val(),'valerts_start_date':$("#valerts_start_date").val(),'valerts_end_date':$("#valerts_end_date").val()},

    },
  "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {


    $('td:first-child', nRow).addClass('bulk');
    $('td:last-child', nRow).addClass('action-tab');
	$("#fixedHeader").removeClass('sticky');
     },
   "fnDrawCallback": function () {
      $('.dropdown-button, .translation-button, .dropdown-menu').dropdown({
              inDuration: 300,
              outDuration: 225,
              constrainWidth: false,
              hover: true,
              gutter: 0,
              belowOrigin: true,
              alignment: 'left',
              stopPropagation: false
            });


      $('.deactive_calerts').off().on('click', function(){
          var calert_id = $(this).data('calert_id');
          console.log(calert_id);
         $('#deactivate_calerts').modal('open');

         $('#deactivate_calerts #remove_calert_id').val(calert_id);
        });
      $('.delete_calerts').off().on('click', function(){
        var calert_id = $('#deactivate_calerts #remove_calert_id').val();
        $.ajax({

          url:base_url+'settings/deactive_alerts',

          type:"POST",

          data:{'csrf_test_name':csrf_hash,"calert_id":calert_id,"alert_type":"customers","status":'Inactive'},

          success:function(res){
              if(res == true)
              {
                valertsDatatable(base_path()+'settings/get_v_alerts/','my-valerts');
                $('select').material_select();
                Materialize.toast('vendor Alert has been successfully deactivated', 2000,'green rounded');
              }
              else
              {
               valertsDatatable(base_path()+'settings/get_v_alerts/','my-valerts');
               $('select').material_select();
               Materialize.toast('Error while processing!', 2000,'red rounded');
              }
            },

          });
      });


      var calerts = [];
        var calerts_values = "";
        $(".calerts_bulk_action").on('click', function(e) {
          if($(this).is(':checked')){
            var total_checked = $(".calerts_bulk_action:checked").length;
              calerts=[];
              $(".calerts_bulk_action:checked").each(function() {
                calerts.push($(this).val());
              });
          }
          else
          {
            calerts=$('#deactive_multiple_calerts').attr('data-multi_calerts').split(',');
            $(this).prop('checked',false);
            var remove_id = $(this).val();
            calerts = jQuery.grep(calerts, function(value) {
              return value != remove_id;
            });
          }
            calerts_values = calerts.join(",");
            $('#deactive_multiple_calerts').attr('data-multi_calerts',calerts_values);
          });
   },
  });
}


function exalertsDatatable(url,id){

  $('#'+id).DataTable().destroy();
 // $('#deactive_multiple_invoices').attr('data-multi_invoices',0);
  table_alerts = $('#'+id).dataTable({
  "scrollY":299,
   "bServerSide": true,
  "bProcessing": true,
  "bDeferRender": true,
    "aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0 , 1 , 2, 3 , 4 ,5, 6, 7,8] }],
   "aoColumns": [
    { "bSortable": false },
    { "bSortable": false },
    { "bSortable": false },
    { "bSortable":  true},
    { "bSortable": true },
    { "bSortable": false },
    { "bSortable": false },
    { "bSortable": false },
    { "bSortable": false },
  ],
  oLanguage: { sLengthMenu: "_MENU_", },
    "order": [0,'DESC'],
    "bPaginate": true,
    "bLengthChange": true,
    "bFilter": true,
    "sDom": 'Rfrtlip',
    "bInfo": false,
    "searching":false,
  "iDisplayLength": 10,
    lengthMenu: [
            [ 10, 20, 30, 50, -1 ],
            [ 'Show 10', 'Show 20', 'Show 30', 'Show 50', 'Show all' ]
        ],

  "ajax": {
      "url": url,
      "type": "POST",
      "data": {'csrf_test_name':csrf_hash,'search': $("#search_ealerts").val(),'ealerts_customer':$("#ealerts_customer").val(),'ealerts_start_date':$("#ealerts_start_date").val(),'ealerts_end_date':$("#ealerts_end_date").val()},

    },
  "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {


    $('td:first-child', nRow).addClass('bulk');
    $('td:last-child', nRow).addClass('action-tab');
	$("#fixedHeader").removeClass('sticky');
     },
   "fnDrawCallback": function () {
      $('.dropdown-button, .translation-button, .dropdown-menu').dropdown({
              inDuration: 300,
              outDuration: 225,
              constrainWidth: false,
              hover: true,
              gutter: 0,
              belowOrigin: true,
              alignment: 'left',
              stopPropagation: false
            });


      $('.deactive_calerts').off().on('click', function(){
          var calert_id = $(this).data('calert_id');
          console.log(calert_id);
         $('#deactivate_calerts').modal('open');

         $('#deactivate_calerts #remove_calert_id').val(calert_id);
        });
      $('.delete_calerts').off().on('click', function(){
        var calert_id = $('#deactivate_calerts #remove_calert_id').val();
        $.ajax({

          url:base_url+'settings/deactive_alerts',

          type:"POST",

          data:{'csrf_test_name':csrf_hash,"calert_id":calert_id,"alert_type":"customers","status":'Inactive'},

          success:function(res){
              if(res == true)
              {
                exalertsDatatable(base_path()+'settings/get_v_alerts/','my-valerts');
                $('select').material_select();
                Materialize.toast('vendor Alert has been successfully deactivated', 2000,'green rounded');
              }
              else
              {
               exalertsDatatable(base_path()+'settings/get_v_alerts/','my-valerts');
               $('select').material_select();
               Materialize.toast('Error while processing!', 2000,'red rounded');
              }
            },

          });
      });


      var calerts = [];
        var calerts_values = "";
        $(".calerts_bulk_action").on('click', function(e) {
          if($(this).is(':checked')){
            var total_checked = $(".calerts_bulk_action:checked").length;
              calerts=[];
              $(".calerts_bulk_action:checked").each(function() {
                calerts.push($(this).val());
              });
          }
          else
          {
            calerts=$('#deactive_multiple_calerts').attr('data-multi_calerts').split(',');
            $(this).prop('checked',false);
            var remove_id = $(this).val();
            calerts = jQuery.grep(calerts, function(value) {
              return value != remove_id;
            });
          }
            calerts_values = calerts.join(",");
            $('#deactive_multiple_calerts').attr('data-multi_calerts',calerts_values);
          });
   },
  });
}





function halertsDatatable(url,id) {

  $('#'+id).DataTable().destroy();
  $('#deactive_multiple_halerts').attr('data-multi_halerts',0);

  table_halerts = $('#'+id).dataTable({

    //"scrollY":299,
    "bServerSide": true,
    "bProcessing": false,
    "bDeferRender": true,
    "aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0 , 1 , 2, 3 , 4 ,5,6] }],
    "aoColumns": [
      { "bSortable": false },
      { "bSortable": false },
      { "bSortable": false },
      { "bSortable": false },
      { "bSortable": false },
      { "bSortable": false },
      { "bSortable": false },
    ],

    oLanguage: { sLengthMenu: "_MENU_", },

    "order": [0,'DESC'],
    "bPaginate": true,
    "bLengthChange": true,
    "sDom": 'Rfrtlip',
    "bFilter": true,
    "sDom": 'Rfrtlip',
    "bInfo": false,
    "searching":false,
    "iDisplayLength": 10,

    lengthMenu: [
      [ 10, 20, 30, 50, -1 ],
      [ 'Show 10', 'Show 20', 'Show 30', 'Show 50', 'Show all' ]
    ],

    "aaSorting": [[1, "desc" ]],

    "ajax": {
      "url": url,
      "type": "POST",
      "data": {'csrf_test_name':csrf_hash,'search': $("#search_halerts").val(),'halerts_customer':$("#halerts_customer").val(),'halerts_start_date':$("#halerts_start_date").val(),'halerts_end_date':$("#halerts_end_date").val()},
    },

    "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
      $('td:first-child', nRow).addClass('bulk');
      $('td:last-child', nRow).addClass('action-tab');
      $("#fixedHeader").removeClass('sticky');
    },

    "fnDrawCallback": function () {
      $('.dropdown-button, .translation-button, .dropdown-menu').dropdown({
        inDuration: 300,
        outDuration: 225,
        constrainWidth: false,
        hover: true,
        gutter: 0,
        belowOrigin: true,
        alignment: 'left',
        stopPropagation: false
      });

      var num = table_halerts.fnSettings().fnRecordsTotal();
      $('#rev_count').html(num);

      var tracker = [];
      var tracker_values = "";

      $(".halerts_bulk_action").on('click',function(e) {
        if($(this).is(':checked')){

          var total_checked = $(".halerts_bulk_action:checked").length;

          tracker=[];

          $(".halerts_bulk_action:checked").each(function() {
            tracker.push($(this).val());
          });

        } else {

          tracker=$('#deactive_multiple_halerts').attr('data-multi_halerts').split(',');

          $(this).prop('checked',false);

          var remove_id = $(this).val();

          tracker = jQuery.grep(tracker, function(value) {
            return value != remove_id;
          });
        }

        tracker_values = tracker.join(",");
        $('#deactive_multiple_halerts').attr('data-multi_halerts',tracker_values);
      });

      //Single Delete
      $('.delete_ch_alerts').off().on('click', function(){
        var halert_id = $(this).data('halert_id');

        $('#deactivate_halerts').modal('open');
        $('#deactivate_halerts #remove_halert_id').val(halert_id);
      });

      $('.delete_halerts').off().on('click', function(){

        var halert_id = $('#deactivate_halerts #remove_halert_id').val();

        $.ajax({

          url:base_url+'Settings/delete_ch_alerts',

          type:"POST",

          data:{'csrf_test_name':csrf_hash,"halert_id":halert_id},

          success:function(res){
            if(res == true) {
              halertsDatatable(base_path()+'Settings/get_ch_alerts', 'my-ch-alerts');
              $('select').material_select();
              Materialize.toast('Your Credit History Alert has been deleted', 2000, 'green rounded');
            } else {
              halertsDatatable(base_path()+'Settings/get_ch_alerts', 'my-ch-alerts');
              $('select').material_select();
              Materialize.toast('Error while processing!', 2000, 'red rounded');
            }
          },
        });
      });
    },
  });
}



function balertsDatatable(url,id){

  $('#'+id).DataTable().destroy();
 // $('#deactive_multiple_invoices').attr('data-multi_invoices',0);
  table_balerts = $('#'+id).dataTable({
	"scrollY":299,
   "bServerSide": true,
  "bProcessing": true,
  "bDeferRender": true,
    "aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0, 1, 2, 3, 4, 5, 6, 7, 8,] }],
   "aoColumns": [
    { "bSortable": false },
    { "bSortable": false },
    { "bSortable": false },
    { "bSortable": false },
    { "bSortable": false },
    { "bSortable": false },
    { "bSortable": false },
    { "bSortable": false },
	{ "bSortable": false },
  ],
    oLanguage: { sLengthMenu: "_MENU_", },
    "order": [0,'DESC'],
    "bPaginate": true,
    "bLengthChange": true,
    "sDom": 'Rfrtlip',
    "bFilter": true,
    "sDom": 'Rfrtlip',
    "bInfo": false,
    "searching":false,
  "iDisplayLength": 10,
    lengthMenu: [
            [ 10, 20, 30, 50, -1 ],
            [ 'Show 10', 'Show 20', 'Show 30', 'Show 50', 'Show all' ]
        ],
  "aaSorting": [[1, "desc" ]],
  "ajax": {
      "url": url,
      "type": "POST",
      "data": {'csrf_test_name':csrf_hash,'search': $("#search_balerts").val(),'balerts_occasion':$("#balerts_occasion").val(),'balerts_customer':$("#balerts_customer").val(),'balerts_start_date':$("#balerts_start_date").val(),'balerts_end_date':$("#balerts_end_date").val()},

    },
  "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {


    $('td:first-child', nRow).addClass('bulk');
    $('td:last-child', nRow).addClass('action-tab');
	$("#fixedHeader").removeClass('sticky');
     },
   "fnDrawCallback": function () {
      $('.dropdown-button, .translation-button, .dropdown-menu').dropdown({
              inDuration: 300,
              outDuration: 225,
              constrainWidth: false,
              hover: true,
              gutter: 0,
              belowOrigin: true,
              alignment: 'left',
              stopPropagation: false
            });


      $('.deactive_balerts').off().on('click', function(){
          var balert_id = $(this).data('balert_id');
          console.log(balert_id);
         $('#deactivate_balerts').modal('open');

         $('#deactivate_balerts #remove_balert_id').val(balert_id);
        });
      $('.delete_balerts').off().on('click', function(){
        var balert_id = $('#deactivate_balerts #remove_balert_id').val();
        $.ajax({

          url:base_url+'settings/deactive_alerts',

          type:"POST",

          data:{'csrf_test_name':csrf_hash,"calert_id":balert_id,"alert_type":"birthday_anniversary","status":'Inactive'},

          success:function(res){
              if(res == true)
              {
                balertsDatatable(base_path()+'settings/get_ba_alerts/','my-ba-alerts');
                $('select').material_select();
                Materialize.toast('Birthday Anniversary Alert has been successfully deactivated', 2000,'green rounded');
              }
              else
              {
               balertsDatatable(base_path()+'settings/get_ba_alerts/','my-ba-alerts');
               $('select').material_select();
               Materialize.toast('Error while processing!', 2000,'red rounded');
              }
            },

          });
      });


      var balerts = [];
        var balerts_values = "";
        $(".balerts_bulk_action").on('click', function(e) {
          if($(this).is(':checked')){
            var total_checked = $(".balerts_bulk_action:checked").length;
              balerts=[];
              $(".balerts_bulk_action:checked").each(function() {
                balerts.push($(this).val());
              });
          }
          else
          {
            balerts=$('#deactive_multiple_balerts').attr('data-multi_balerts').split(',');
            $(this).prop('checked',false);
            var remove_id = $(this).val();
            balerts = jQuery.grep(balerts, function(value) {
              return value != remove_id;
            });
          }
            balerts_values = balerts.join(",");
            $('#deactive_multiple_balerts').attr('data-multi_balerts',balerts_values);
          });
   },
  });
}

function galertsDatatable(url,id){

  $('#'+id).DataTable().destroy();
  table_balerts = $('#'+id).dataTable({
	"scrollY":299,
	"bServerSide": true,
	"bProcessing": true,
	"bDeferRender": true,
	"aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0, 1, 2, 3, 4, 5, 6, 7,] }],
	"aoColumns": [
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
	],
    oLanguage: { sLengthMenu: "_MENU_", },
    "order": [0,'DESC'],
    "bPaginate": true,
    "bLengthChange": true,
    "sDom": 'Rfrtlip',
    "bFilter": true,
    "sDom": 'Rfrtlip',
    "bInfo": false,
    "searching":false,
  "iDisplayLength": 10,
    lengthMenu: [
            [ 10, 20, 30, 50, -1 ],
            [ 'Show 10', 'Show 20', 'Show 30', 'Show 50', 'Show all' ]
        ],
  "aaSorting": [[1, "desc" ]],
  "ajax": {
      "url": url,
      "type": "POST",
      "data": {'csrf_test_name':csrf_hash,'search': $("#search_galerts").val(),'galerts_start_date':$("#galerts_start_date").val(),'galerts_end_date':$("#galerts_end_date").val()},

    },
	"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
		$('td:first-child', nRow).addClass('bulk');
		$('td:last-child', nRow).addClass('action-tab');
		$("#fixedHeader").removeClass('sticky');
	},
	"fnDrawCallback": function () {
		$('.dropdown-button, .translation-button, .dropdown-menu').dropdown({
              inDuration: 300,
              outDuration: 225,
              constrainWidth: false,
              hover: true,
              gutter: 0,
              belowOrigin: true,
              alignment: 'left',
              stopPropagation: false
		});
	},
  });
}

function email_alerts($id){

//    $.ajax({
//     type: "POST",
//      url:base_url+'settings/email_alert/'+$id,
//     //data:{'cmp_id':$id},
//     beforeSend: function(){
//       Materialize.toast('Sending Email.......', 2000,'green rounded');
//     },
//     success: function(data){
//       Materialize.toast('Mail has been sent!', 2000,'green rounded');
//     },
// });

var alert_id = $id;
$('#email_popup_client_modal').modal('open');
$('#email_popup_client_modal #alert_id').val(alert_id);

}

function email_aalerts($id){

//    $.ajax({
//     type: "POST",
//      url:base_url+'settings/email_ialert/'+$id,
//     //data:{'cmp_id':$id},
//     beforeSend: function(){
//       Materialize.toast('Sending Email.......', 2000,'green rounded');
//     },
//     success: function(data){
//       Materialize.toast('Mail has been sent!', 2000,'green rounded');
//     },
// });

  var alert_id = $id;
  $('#email_popup_activity_modal').modal('open');
  $('#email_popup_activity_modal #alert_id').val(alert_id);

}

function email_ialerts($id){

//    $.ajax({
//     type: "POST",
//      url:base_url+'settings/email_ialert/'+$id,
//     //data:{'cmp_id':$id},
//     beforeSend: function(){
//       Materialize.toast('Sending Email.......', 2000,'green rounded');
//     },
//     success: function(data){
//       Materialize.toast('Mail has been sent!', 2000,'green rounded');
//     },
// });

  var alert_id = $id;
  $('#email_popup_item_modal').modal('open');
  $('#email_popup_item_modal #alert_id').val(alert_id);

}

function email_cpalerts($id){

//    $.ajax({
//     type: "POST",
//      url:base_url+'settings/email_cpalert/'+$id,
//     //data:{'cmp_id':$id},
//     beforeSend: function(){
//       Materialize.toast('Sending Email.......', 2000,'green rounded');
//     },
//     success: function(data){
//       Materialize.toast('Mail has been sent!', 2000,'green rounded');
//     },
// });

var alert_id = $id;
$('#email_popup_chalert_modal').modal('open');
$('#email_popup_chalert_modal #alert_id').val(alert_id);

}

function email_baalerts($id){
//    $.ajax({
//     type: "POST",
//      url:base_url+'settings/email_baalert/'+$id,
//     //data:{'cmp_id':$id},
//     beforeSend: function(){
//       Materialize.toast('Sending Email.......', 2000,'green rounded');
//     },
//     success: function(data){
//       Materialize.toast('Mail has been sent!', 2000,'green rounded');
//     },
// });
var alert_id = $id;
$('#email_popup_ba_modal').modal('open');
$('#email_popup_ba_modal #alert_id').val(alert_id);
}

function reset_alertsfilter(alert){
  $('.action-btn-wapper').find('select').prop('selectedIndex',0);
  $('.js-example-basic-single').trigger('change.select2');
  $('.btn-date,.search-hide-show').val('');
  // /$('.search-hide-show').closest('a').toggleClass('active');
 /* var select = $('select');
  $('select').prop('selectedIndex',0);*/
  $('select').material_select();
  if(alert=="calert"){
    alertsDatatable(base_path()+'settings/get_my_alerts/','my-alerts');
    $('select').material_select();
  }else if(alert=="ialert"){
    ialertsDatatable(base_path()+'settings/get_item_alerts/','my-item-alerts');
    $('select').material_select();
  }else if(alert=="jalert"){
    jalertsDatatable(base_path()+'settings/get_jv_alerts/','my-jv-alerts');
    $('select').material_select();
  }else if(alert=="halert"){
    halertsDatatable(base_path()+'settings/get_ch_alerts/','my-ch-alerts');
    $('select').material_select();
  }else if(alert=="balert"){
    balertsDatatable(base_path()+'settings/get_ba_alerts/','my-ba-alerts');
    $('select').material_select();
  }else if(alert=="aalert"){
     aalertsDatatable(base_path()+'settings/get_generic_alerts/','my-generic-alerts');
    $('select').material_select();
  }



}
$("#parent_id").on("change",function(){
  var parent_id = $(this).val();
  if(parent_id!=''){
    $("#select2-parent_id-container").css("font-size","14px");
    $("#select2-parent_id-container").css("color","#000");
    $("#select2-parent_id-container").css("font-weight","500");
  }
});


 $("#parent_id").change(function () {
    var id = $("#parent_id").val();

    $.ajax({

    url:base_url+'settings/get_credit_period',

    type:"POST",

    data:{'csrf_test_name':csrf_hash,"id":id,},

    success:function(res){
        $(".update_cp").html(res);
      },

    });
 });
