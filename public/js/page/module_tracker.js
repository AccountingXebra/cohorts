 $.validator.setDefaults({ ignore: '' });
$(document).ready(function() {
	


	var url = $(location).attr('href'),
	    parts = url.split("/"),
	    last_part = parts[parts.length-1];	
	    //soTable = $('#myactivity').DataTable(); 

		myActivity(base_path()+'module_tracker/get_activity/','myactivity');
		$('select').material_select();

	$('.filter-search').on('keyup','#search_activity',function(){ 

		myActivity(base_path()+'module_tracker/get_activity/','myactivity');
		$('select').material_select();
	});


	$('#module_name').on('change', function() {
	  
		myActivity(base_path()+'module_tracker/get_activity/','myactivity');
		$('select').material_select();

	});

	$('#person_name').on('change', function() {
	  
		myActivity(base_path()+'module_tracker/get_activity/','myactivity');
		$('select').material_select();
	  
	});


	$('#act_start_date,#act_end_date').off().change( function() {
		// var as = $('#act_start_date').val();
		// alert(as);
	 	if($(this).val()!=''){

			myActivity(base_path()+'module_tracker/get_activity/','myactivity');
			$('select').material_select();
		}
	});

	var bulk_activity = [];
	$("input[id='activity_bulk']").on("click", function(){
	if($(this).is(':checked',true)) {
	  bulk_activity= [];
		$(".activity_bulk_action").prop('checked', true);
		$(".activity_bulk_action:checked").each(function() {
				bulk_activity.push($(this).val());
			});
			bulk_activity = bulk_activity.join(",");
			//$('#deactive_multiple_activity').attr('data-multi_customer',bulk_activity);
	    $('#download_multiple_activity').attr('data-multi_download',bulk_activity);
		}
		else {
	    $(".activity_bulk_action:checked").each(function() {
	      $(this).prop('checked', false);
	    });
			bulk_activity = [];
			//$('#deactive_multiple_activity').attr('data-multi_customer',0);
	    $('#download_multiple_activity').attr('data-multi_download',0);
		}
	});

	$("#download_multiple_activity").on('click', function(e) {
		  var activities = $(this).attr('data-multi_download');
		  if(activities == '' || activities == '0')
		  {
		    Materialize.toast('Please select a record first', 2000,'red rounded');
		  }
		  else
		  {
		    var words = activities.split(",");
		    //alert(words);
		    if(words.length < 2){
		      Materialize.toast('Select two or more records first..!', 2000,'red rounded');
		     }else{
		    //$('#deactive_multiple_customers').attr('data-multi_customer',0);
		    $('#download_multiple_activity').attr('data-multi_download',0);
		    //$('#company-profile').find('input:checkbox').attr('checked',false);
		    $('#myactivity th').find('input[type="checkbox"]').each(function() {
		        $(this).prop('checked', false);
		      });
			  $("#activity_bulk").prop('checked', false);
		    $(".activity_bulk_action").prop('checked', false);
		    window.location=base_url+'module_tracker/download-multiple-activity?ids='+activities;
		    myActivity(base_path()+'module_tracker/get_activity/','myactivity');
		    $('select').material_select();
		    Materialize.toast('Login details have been downloaded', 2000,'green rounded');
		    }    
		  }
		});

		$("#print_multiple_activity").on('click', function(e) {
		  var activities = $('#download_multiple_activity').attr('data-multi_download');
		  if(activities == '' || activities == '0')
		  {
		    Materialize.toast('Please select a record first', 2000,'red rounded');
		  }
		  else
		  {
		    var words = activities.split(",");
		    //alert(words);
		    if(words.length < 2){
		      Materialize.toast('Select two or more records first..!', 2000,'red rounded');
		     }else{
		    //$('#deactive_multiple_customers').attr('data-multi_customer',0);
		    $('#download_multiple_activity').attr('data-multi_download',0);
		    //$('#company-profile').find('input:checkbox').attr('checked',false);
		    $('#myactivity th').find('input[type="checkbox"]').each(function() {
		        $(this).prop('checked', false);
		      });
			  $("#activity_bulk").prop('checked', false);
		    $(".activity_bulk_action").prop('checked', false);
		    window.location=base_url+'module_tracker/print-multiple-activity?ids='+activities;
		    myActivity(base_path()+'module_tracker/get_activity/','myactivity');
		    $('select').material_select();
		    Materialize.toast('Login details have been printed', 2000,'green rounded');
		    }    
		  }
		});

	$("#email_multiple_activity").on('click', function(e) {
		  var activities = $('#download_multiple_activity').attr('data-multi_download');
		  if(activities == '' || activities == '0')
		  {
		    Materialize.toast('Please select a record first', 2000,'red rounded');
		  }
		  else
		  {
		    var words = activities.split(",");
		    //alert(words);
		    if(words.length < 2){
		      Materialize.toast('Select two or more records first..!', 2000,'red rounded');
		     }else{
		    //$('#deactive_multiple_customers').attr('data-multi_customer',0);
		    // $('#download_multiple_activity').attr('data-multi_download',0);
		    // //$('#company-profile').find('input:checkbox').attr('checked',false);
		    // $('#myactivity th').find('input[type="checkbox"]').each(function() {
		    //     $(this).prop('checked', false);
		    //   });
		    // $(".activity_bulk_action").prop('checked', false);
		    // window.location=base_url+'module_tracker/email_multiple_activity?ids='+activities;
		    // myActivity(base_path()+'module_tracker/get_activity/','myactivity');
		    // $('select').material_select();
		    // Materialize.toast('Login details have been Emailed', 2000,'green rounded');
		    email_activity(activities);
		    }    
		  }
		});

	function reset_itmfilter($id){

  		$('.btn-date,.search-hide-show').val('');
	 	// $('.search-hide-show').toggle();
	  	myActivity(base_path()+'module_tracker/get_activity/','myactivity');
	  	$('select').material_select();
	}

	$("#subscription_name").on("change",function(){
	  var subscription_name = $(this).val();
	  if(subscription_name !=''){
	    $("#select2-subscription_name-container").css("font-size","14px");
	    $("#select2-subscription_name-container").css("color","#000");
	    $("#select2-subscription_name-container").css("font-weight","500");
	  }
	});

	function selectItem(val) {
		$('#subscription_name').val(val);
		$("#suggesstion-box-item").hide();
	}

	//  function email_activity($id){
	//   var activity_id = $id;
	//   $('#send_email_activity_modal').modal('open');
	//   $('#send_email_activity_modal #activity_id').val(activity_id);

	// }



});
			


function email_activity($id){
  var activity_id = $id;
  $('#send_email_activity_modal').modal('open');
  $('#send_email_activity_modal #activity_id').val(activity_id);

}



