	$(document).ready(function() {
		console.log("Feedback Page");

		// adminprofDatatable(base_path()+'admin-dashboard/get_admin_profile/','personal-profile');
		// $('select').material_select();

		feedbackDatatable(base_path()+'admin-dashboard/get_feedback_details/','feedback_table');
		$('select').material_select();

		$('.searchbtn').on('keyup','#search_feedback',function(){ 
		feedbackDatatable(base_path()+'admin-dashboard/get_feedback_details/','feedback_table');
		$('select').material_select();
	   });

		$('.feedbackdate').on('change','#feedback_start_date,#feedback_end_date',function() { if($(this).val()!=''){ feedbackDatatable(base_path()+'admin-dashboard/get_feedback_details/','feedback_table');
 	$('select').material_select();} } );
	
		
		var bulk_activity = [];
		$("input[id='feedback_bulk']").on("click",function(){
			if($(this).is(':checked',true)) {
				$(".feedb_bulk_action").prop('checked', true);
				$(".feedb_bulk_action:checked").each(function() {
					bulk_activity.push($(this).val());
				});
				bulk_activity = bulk_activity.join(",");
				$('#download_multiple_feedback').attr('data-multi_feed',bulk_activity);
			}
			else {
				$(".feedb_bulk_action").prop('checked',false);
				bulk_activity = [];
				$('#download_multiple_feedback').attr('data-multi_feed',0);
			}
		});



      $("body").on('click','#download_multiple_feedback',function(e) {

		var dnwl_ids = $('#download_multiple_feedback').attr('data-multi_feed');
		//alert(dnwl_ids);
		if(dnwl_ids == '' || dnwl_ids == '0')
		{

			Materialize.toast('Please select a record first', 2000,'red rounded');
		}
		else
		{
			var words = dnwl_ids.split(",");
		    if(words.length < 2){
		     	Materialize.toast('Select two or more records first..!', 2000,'red rounded');
		    }else{
				$('#download_multiple_feedback').attr('data-multi_feed',0);
				//$('#company-profile').find('input:checkbox').attr('checked',false);
				$('#feedback_table th').find('input[type="checkbox"]').each(function() {
				    $(this).prop('checked', false);
				  });
				feedbackDatatable(base_path()+'admin-dashboard/get_feedback_details/','feedback_table');
 				$('select').material_select();	

				window.location=base_url+'admin-dashboard/download_multiple_feedback?ids='+dnwl_ids;
						
				Materialize.toast('Feedback Details has been downloaded', 2000,'green rounded');
			}
		}
	});

		
	});
	
	function feedbackDatatable(url,id){
	
	$('#'+id).DataTable().destroy();
	$('#download_multiple_feedback').attr('data-multi_feed',0);
	
	table_coupon_code = $('#'+id).dataTable({	
	"bServerSide": true,
	"bProcessing": true,
	"bDeferRender": true,
    "aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0 , 1 , 2, 3 , 4 ,5, 6,7] }],
	 "aoColumns": [	
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
	],
	oLanguage: { sLengthMenu: "_MENU_", },
    "order": [0,'DESC'],
    "bPaginate": true,
    "bLengthChange": true,
    "sDom": 'Rfrtlip',
    "bFilter": true,
    "bInfo": false,
    "searching":false,

	"iDisplayLength": 10,

		lengthMenu: [
            [ 10,20, 30, 50, -1 ],
            [ 'Show 10','Show 20', 'Show 30', 'Show 50', 'Show all' ]
        ],
	//"aaSorting": [[ 1, "desc" ]],
	"ajax": {
			"url": url,
			"type": "POST",
			//"data":'',
			"data": {'csrf_test_name':csrf_hash,'search_feedback':$('#search_feedback').val(),'feedback_start_date':$('#feedback_start_date').val(),'feedback_end_date':$('#feedback_end_date').val()},
		},
	"dom": "<'row'<'col-md-6'l><'col-md-6'f>><'row'<'col-md-6'B><'col-md-6'p>><'row'<'col-md-12't>><'row'<'col-md-12'i>>",
	"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
		if($(aData[10]+' div').hasClass('deactive_record')){
				$('td', nRow).parent('tr').addClass('deactivate-row');
			}
			$('td:first-child', nRow).addClass('bulk');
		$('td:nth-child(2)', nRow).addClass('profil-img');
		$('td:last-child', nRow).addClass('action-tab');
     },
	 "fnDrawCallback": function () {
		  $('.dropdown-button, .translation-button, .dropdown-menu').dropdown({
						  inDuration: 300,
						  outDuration: 225,
						  constrainWidth: false,
						  hover: true,
						  gutter: 0,
						  belowOrigin: true,
						  alignment: 'left',
						  stopPropagation: false
						});
									
			var num = table_coupon_code.fnSettings().fnRecordsTotal();
		  	$('#rev_count').html(num);
			
			var coupon = [];
				var coupon_values = "";
				$(".feedb_bulk_action").on('click', function(e) {
					if($(this).is(':checked')){
						var total_checked = $(".feedb_bulk_action:checked").length;
						 
						  coupon=[];
						  $(".feedb_bulk_action:checked").each(function() {
			
							coupon.push($(this).val());
						}); 
					}
					else
					{
						coupon=$('#download_multiple_feedback').attr('data-multi_feed').split(',');
						$(this).prop('checked',false);
						var remove_id = $(this).val();	
						coupon = jQuery.grep(coupon, function(value) {
						  return value != remove_id;
						});
					}
						coupon_values = coupon.join(",");
						$('#download_multiple_feedback').attr('data-multi_feed',coupon_values);
					});
					
	 },
  }); 
}

