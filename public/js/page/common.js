$.validator.setDefaults({ ignore: '' });
 $(document).ready(function() {
    var prefix_sale =$('#prefix_sales').data('prefix');
 $("body").on('click','.navbar-toggler',function(){ 
  
  $('table').css('width','100%');
 });
  /*----START Item Dropdown---*/
    /* $(".outer1").on('mouseover','.service_drp',function(){
         $("#scrollbar-restable").css("overflow", "inherit");
         $(".fix").css("display", "none");
        // $(".select-dropdown").css("max-height", "200px");
        // $("#scrollbar-restable").css("margin-bottom", "-260px");
     });
     
     $(".outer1").on('mouseleave','.service_drp',function(){
         $("#scrollbar-restable").css("overflow", "");
         $(".fix").css("display", "");
         //$("#scrollbar-restable").css("margin-bottom", "");
     });*/
  /*----END Item Dropdown---*/  
  /*----START GSTIN NUMBER---*/
  $(".addgstin").on('keyup','.js-typeahead',function(){
  var type_id=$(this).attr('id');
  if(!($(this).val().length > 3)){
    //console.log($(this).val().length);
    $("#suggesstion-box_"+type_id).hide();
  }
  if($(this).val().length >= 3){
    $.ajax({
    type: "POST",
     url:base_url+'profile/searchCity',
    data:{'csrf_test_name':csrf_hash,'keyword':$(this).val(),'type_id':type_id},
    beforeSend: function(){
     // $("#add_gst_from #location1").css("background","#FFF url(LoaderIcon.gif) no-repeat 165px");
    },
    success: function(data){
      
      $("#suggesstion-box_"+type_id).show();
      $("#suggesstion-box_"+type_id).html(data);
      //$("#add_gst_from #location1").css("background","#FFF");
    }
    });
  }
  });

$(".addgstin").on('keyup','.statecode_gst',function(){
  var state_code=$(this).val();
  var sid=$(this).attr('id');
  var lastChar = sid[sid.length-1];
  var scod= state_code.substr(0, 2);

  if($(this).val().length >= 2){
   
    $.ajax({
    type: "POST",
    url:base_url+'profile/get_state_from_code',
    data:{'csrf_test_name':csrf_hash,'state_code':scod},
    
    success: function(result){
      var data = JSON.parse(result);
      if(data != '')
      {
      $("#location"+lastChar).parents('.input-field').children().addClass('active');
      $('.addgstin').find("#location"+lastChar).val(data[0]['state_name']);
      $('.addgstin').find("#statecode"+lastChar).val(data[0]['state_gst_code']);
     }else{
      $('.addgstin').find("#location"+lastChar).val('');
       $('.addgstin').find("#statecode"+lastChar).val('');
     }
    }
    });
  }
  });
$("#gst_no_edit").keyup(function(){
  var state_code=$(this).val();
 
  var scod= state_code.substr(0, 2);
  if($(this).val().length >= 2){
   
    $.ajax({
    type: "POST",
    url:base_url+'profile/get_state_from_code',
    data:{'csrf_test_name':csrf_hash,'state_code':scod},
    
    success: function(result){
      var data = JSON.parse(result);
      if(data != '')
      {
      $("#gst_edit_location").parents('.input-field').children().addClass('active');
      $("#gst_edit_location").val(data[0]['state_name']);

     }else{
      $("#gst_edit_location").val('');
     }
    }
    });
  }
});
$("#gst_edit_location").on('keyup',function(){
  var type_id='gst_edit_location';
  if(!($(this).val().length > 3)){
    //console.log($(this).val().length);
    $("#suggesstion_edit_location").hide();
  }
  if($(this).val().length >= 3){
    $.ajax({
    type: "POST",
     url:base_url+'profile/searchCity',
    data:{'csrf_test_name':csrf_hash,'keyword':$(this).val(),'type_id':type_id},
    beforeSend: function(){
     // $("#add_gst_from #location1").css("background","#FFF url(LoaderIcon.gif) no-repeat 165px");
    },
    success: function(data){
      
      $("#suggesstion_edit_location").show();
      $("#suggesstion_edit_location").html(data);
      //$("#add_gst_from #location1").css("background","#FFF");
    }
    });
  }
});
$('.contact_close').on('click',function(){
   if($('#hidden-cover').hasClass('active')){
    $('.changeicon').html('keyboard_arrow_down');
    $('#hidden-cover').removeClass('active');
  }                 
});
 /*----End GSTIN NUMBER---*/
 /*----NUUMBERIC TEXTBOX---*/
 $("body").on("keypress keyup blur",'.numeric_number',function (event) {
    $(this).val($(this).val().replace(/[^0-9\.]/g,''));
           if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
               event.preventDefault();
           }
 }); 
/*----NUUMBERIC TEXTBOX---*/

/*~~~~~~~~~~~~~~ONLY LETTERS AND NUMBER~~~~~~~~~~~~~~~~*/
jQuery.validator.addMethod("letternumber", function(value, element) {
              return this.optional(element) || /^[a-z0-9\\s]+$/i.test(value);
          }, "No special characters allowed");

/*~~~~~~~~~~~~~~ONLY LETTERS AND NUMBER~~~~~~~~~~~~~~~~*/

    /*var prefix=$('#prefix_sales').data('prefix');
   var sales_counter=1;
     if($('#'+prefix_sale+'_counter').val()!=''){
      sales_counter=$('#'+prefix_sale+'_counter').val();
    }*/
  /*  $('#add'+prefix_sale).off().on('change','.services_'+prefix_sale,function() {
        var tr=$(this).closest('tr').attr('id');
        if($('#'+prefix_sale+'_customer_id').val() == '')
          {
           
          } else{
       		 $.ajax({
                    url: base_url+'sales/get_hsn',
                    type: 'POST',
                    data: {'service_id':$(this).val()},
                    success: function(result) {
                         $('#hsn'+tr).attr('value',result[0]['service_hsn_no']);

                         if($('#tax_type').val()=="India"){
                         var half_gst = parseFloat((result[0]['service_gst'])/2);
                         $('#cgst_per'+tr).attr('value',half_gst);
                         $('#sgst_per'+tr).attr('value',half_gst);
                     	}else{
                         $('#igst_per'+tr).attr('value',result[0]['service_gst']);
                     	}
                         
                    },
                    error: function(result) {
                        alert(result);
                    },
                });
    	}
         return false;

    });

     $('#add'+prefix_sale).on('change','.services_'+prefix_sale+',.quantity,.rate,.discount_rs',function() {
              var tr=$(this).closest('tr').attr('id');
             if($('#'+prefix_sale+'_customer_id').val() == '')
              {
                Materialize.toast('Please select client', 2000,'red rounded');
              } else if($("#sales_services"+tr).val()==''){
                Materialize.toast('Please select Service first..!', 2000,'red rounded');
              }else{
              var qty=$('#qty'+tr).val();
              var rate=$('#rate'+tr).val();
              var discount=0;
              if($('#discount'+tr).val()){
               discount=$('#discount'+tr).val();
              }

              
              var main_taxt=0;
              if(qty!='' && rate!=''){

                var tot_price=parseFloat(qty) * parseFloat(rate);
                
                if($('#tax_type').val()=="India"){
                    var cgst_per=0;
                    var sgst_per=0;
                    if($('#cgst_per'+tr).val()){
                    cgst_per=$('#cgst_per'+tr).val();
                    }
                    if($('#sgst_per'+tr).val()){
                    sgst_per=$('#sgst_per'+tr).val();
                    }
                    
                    var total_cgst_rs=(parseFloat(tot_price)*parseFloat(cgst_per))/100;
                    var total_sgst_rs=(parseFloat(tot_price)*parseFloat(sgst_per))/100;
                    main_taxt = (parseFloat(total_cgst_rs)+parseFloat(total_sgst_rs));
                    $('#cgst'+tr).attr('value',total_cgst_rs);
                    $('#sgst'+tr).attr('value',total_sgst_rs);
                }else{
                    var igst_per=0;
                    if($('#igst_per'+tr).val()){
                    igst_per=$('#igst_per'+tr).val();
                    }
                    
                    var total_igst_rs=(parseFloat(tot_price)*parseFloat(igst_per))/100;
                    main_taxt = parseFloat(total_igst_rs);
                    $('#igst'+tr).attr('value',total_igst_rs);
                }

                var sub_amount = parseFloat((tot_price + main_taxt) - discount);
                $('#sub_amount'+tr).attr('value',Number(sub_amount));
                var total_discount=0;
                $(".discount_rs").each(function(){
                  if($(this).val()!=''){
                     total_discount+=parseFloat($(this).val());
                  }
                });
                $('#total_discount').attr('value',parseFloat(total_discount));
                if($('#tax_type').val()=="India"){
                var total_cgst=0;
                $(".cgst_rs").each(function(){
                  if($(this).val()!=''){
                     total_cgst+=parseFloat($(this).val());
                  }
                });
                var total_sgst=0;
                $(".sgst_rs").each(function(){
                  if($(this).val()!=''){
                     total_sgst+=parseFloat($(this).val());
                  }
                });
                $('#tot_cgst').attr('value',total_cgst);
                $('#tot_sgst').attr('value',total_sgst);
                }else{
                var total_igst=0;
                $(".igst_rs").each(function(){
                  if($(this).val()!=''){
                     total_igst+=parseFloat($(this).val());
                  }
                });
                $('#tot_igst').attr('value',total_igst); 
                }
               
                var total_amount=0;
                $(".samount_rs").each(function(){
                  if($(this).val()!='' ){
                    
                    total_amount+=parseFloat($(this).val());
                  }
                });
                
              
                $('#tot_amount').attr('value',parseFloat(total_amount));
                $('#grand_total_'+prefix_sale).text(parseFloat(total_amount));
                var words=toWords(parseFloat(total_amount));
                    if(words==''){
                        $('#to_words_'+prefix_sale).text('ZERO');
                    }else{
                     $('#to_words_'+prefix_sale).text(words.toUpperCase()+' ONLY');
                    }
                }else{
                   
                }
              }
      });*/
     /* $('.add_div').on('click', '.addnewrow_'+prefix_sale, function() {  
        if($('#'+prefix_sale+'_customer_id').val() == '')
          {
            Materialize.toast('Please select client', 2000,'red rounded');
          } else{
        var exp = $('#add tbody tr').eq(1).clone();
        document.activeElement.blur();

            sales_counter++;
            exp.attr('id',sales_counter);
            
                var section = exp.clone().find(':input,select,a').each(function() {
                        //set id to store the updated section number
                        var newId = this.id;
                        newId = newId.slice(0,-1)+ sales_counter;
                      //  var newName = this.name + expcounter;
                        //update for label
                        $(this).prev().attr('for', newId);
                       // $(this).prev().attr('required', 'required');
                        //$(this).prev().attr('for', newName);
                        //update id
                        this.id = newId;
                        this.value = '';
                        //this.name = newName;
                    }).end();
                   // .appendTo('#expense_form tbody');
                   if($('.reverse_charge_chkbox').is(':checked') && prefix=="inv"){
                    var test = $('#add').closest('table').find('tr:last').prev().prev().after(section);
                  }else{
                     var test = $('#add').closest('table').find('tr:last').prev().after(section);
                  }
                   $('select').material_select();
          }      return false;
               
        });*/
   /*   $('#add'+prefix_sale).on('click','.delete_row'+prefix_sale,function(){
         
          var id=$(this).attr('id');
           //alert(id);
        
          $(this).closest("tr").remove();
            calculate_amount(prefix);
        });*/
    $("#edit_addresses_form").submit(function(e){
        e.preventDefault();
    }).validate({
        rules:{
            new_dc_address:{
                required:true
            },
        },
        messages:{
            new_dc_address:{
                required:"Billing Address is required",
            },
        },
        submitHandler:function(form){
                var frm=$(form).serialize();
                        $.ajax({
                        url:base_url+'sales/update_billing_shipping_address',
                        type:"POST",
                        data:{'csrf_test_name':csrf_hash,"frm":frm,},
                        success:function(res){
                            $('#addresses_model').modal('close');
                            if( $('#sip-same-as-bill').is(':checked')){
                                $('.billing_address span.bill_add_'+$('#addresses_model #address_prefix').val()).html(res);
                                $('.shipping_address span.ship_add_'+$('#addresses_model #address_prefix').val()).html(res);
                                Materialize.toast('New Billing and Shipping Address have been set!!', 2000,'green rounded');
                            }else if($('#addresses_model #address_type').val()=="billing"){
                                $('.billing_address span.bill_add_'+$('#addresses_model #address_prefix').val()).html(res);
                                Materialize.toast('New billing address set', 2000,'green rounded');
                            }else{
                                $('.shipping_address span.ship_add_'+$('#addresses_model #address_prefix').val()).html(res);
                                Materialize.toast('New Shipping Address have been set!!', 2000,'green rounded');
                            }
                            
                            
                    },                      
            });
        },
    });
    /* Common js */
    $("#sales_billing_document").change(function(){
    var bill_doc=$(this).val();
   
    if(bill_doc!=''){
      
    window.location=base_url+"sales/"+bill_doc;
    }
  });
    $('#sip-same-as-bill').click(function(){
      var prefix=$(this).data('prefix');
      if($(this).is(':checked')){
        
        if($('#'+prefix+'_customer_id').val() == ''){
        Materialize.toast('Please Select Customer First!!', 2000,'red rounded');
        //$('.checkmark').css("display","none");
        $(this).prop('checked',false);
        } else{
        $(".hide_bill_"+prefix).hide();
        $.ajax({
            url:base_url+'sales/same_billing_shipping_session',
            type:"POST",
            data:{'csrf_test_name':csrf_hash,'prefix':prefix},
            success:function(res){
              var address=$('.billing_address span.bill_add_'+prefix).text();
              $('.shipping_address span.ship_add_'+prefix).html(address);
              Materialize.toast('Shipping Address has been change successfully!!',2000,'green rounded');
            },            
        });
        
        }

      }else{
        $(".hide_bill_"+prefix).show();
      }

    }); 
    /* $(".billing_address_add,.shipping_address_add").on("click",function(){
      var type= $(this).data('type');
      var prefix= $(this).data('prefix');
      $('#addresses_model #add_type').text(type);
        var cust_id=$('#'+prefix+'_customer_id').val();
        if(cust_id == ''){
          Materialize.toast('Please Select Customer First!!', 2000,'red rounded');
        } else {
          $('#addresses_model #new_dc_country').material_select();
          $('#addresses_model').modal('open');
          $.ajax({
            url:base_url+'sales/get_customer_from_id/'+cust_id,
            type:"POST",
            data:{'type':type,'prefix':prefix},
            success:function(result){
              $("#new_dc_state > option").remove();
              $("#new_dc_city > option").remove();
              $('#addresses_model #address_type').val(type);
              $('#addresses_model #address_prefix').val(prefix);
              
              var res=jQuery.parseJSON(result);
              $("#addresses_model #new_dc_pincode,#addresses_model #new_dc_address").parents('.input-field').children().addClass('active');

              $("#addresses_model #new_dc_state,#addresses_model #new_dc_city").parents('.input-field').addClass('label-active');
              
              $("#addresses_model #new_dc_address").val(res[0]['cust_'+type+'_address']);
              $('#addresses_model #new_dc_country option[value="'+res[0]['cust_'+type+'_country']+'"]').attr('selected','selected');
              $('#new_dc_country').material_select();
            
            
                var opt = $('<option />'); 
                opt.val(res[0]['cust_'+type+'_state']);
                  if(type=="billing"){
                  opt.text(res[0]['state_name']);
                  }else{
                  opt.text(res[0]['sstate']);
                  }
                $('#new_dc_state').append(opt); 
                opt.attr('selected','selected');
                $('#new_dc_state').material_select(); 

                var opt1 = $('<option />'); 
                opt1.val(res[0]['cust_'+type+'_city']);
                  if(type=="billing"){
                  opt1.text(res[0]['name']);
                  }else{
                  opt1.text(res[0]['scity']);
                  }
                opt1.attr('selected','selected');
                $('#new_dc_city').append(opt1); 
                $('#new_dc_city').material_select();

                    
              $("#addresses_model #new_dc_pincode").val(res[0]['cust_'+type+'_zipcode']);

              },
            });
            
        }
    });*/
    $(".billing_address_add,.shipping_address_add").on("click",function(){
      var type= $(this).data('type');
      var prefix= $(this).data('prefix');
      $('#addresses_model #add_type').text(type);
        var cust_id=$('#'+prefix+'_customer_id').val();
        if(cust_id == ''){
          Materialize.toast('Please Select Customer First!!', 2000,'red rounded');
        } else {
          $('#addresses_model #new_dc_country').material_select();
          $('#addresses_model').modal('open');
          $.ajax({
            url:base_url+'sales/get_customer_from_id/'+cust_id,
            type:"POST",
            data:{'csrf_test_name':csrf_hash,'type':type,'prefix':prefix},
            success:function(result){
              $("#new_dc_state > option").remove();
              $("#new_dc_city > option").remove();
              $('#addresses_model #address_type').val(type);
              $('#addresses_model #address_prefix').val(prefix);
              
             var res=jQuery.parseJSON(result);
             
              $("#addresses_model #new_dc_pincode,#addresses_model #new_dc_address").parents('.input-field').children().addClass('active');

              $("#addresses_model #new_dc_state,#addresses_model #new_dc_city").parents('.input-field').addClass('label-active');
              
              $("#addresses_model #new_dc_address").val(res[prefix+'_'+type+'_address']);
              $('#addresses_model #new_dc_country option[value="'+res[prefix+'_'+type+'_country']+'"]').attr('selected','selected');
              $('#new_dc_country').material_select();
            
            
                var opt = $('<option />'); 
                opt.val(res[prefix+'_'+type+'_state']);
                opt.text(res['state_name']);
                  
                $('#new_dc_state').append(opt); 
                opt.attr('selected','selected');
                $('#new_dc_state').material_select(); 

                var opt1 = $('<option />'); 
                opt1.val(res[prefix+'_'+type+'_city']);
                opt1.text(res['city_name']);
                opt1.attr('selected','selected');
                $('#new_dc_city').append(opt1); 
                $('#new_dc_city').material_select();
              $("#addresses_model #new_dc_pincode").val(res[prefix+'_'+type+'_zipcode']);

              },
            });
            
        }
    });
    $('#'+prefix_sale+'_reference_po_no').on('change' ,function(){
      var legal_id = $(this).val();
       $('#'+prefix_sale+'_reference_po_date').html('');
      if(legal_id!=''){
        $.ajax({
          url:base_url+'sales/get_po_date',
          type:"POST",
          data:{'csrf_test_name':csrf_hash,"legal_id":legal_id,},
          success:function(res){
            $('#'+prefix_sale+'_reference_po_date').val(res);
          },
        });
      } else{
       Materialize.toast('Please select Redference P.O NO. ..!', 2000,'red rounded');
     }
    });
    $('#'+prefix_sale+'_staturory_po_no').on('change' ,function(){
      var legal_id = $(this).val();
       $('#'+prefix_sale+'_staturory_po_date').html('');
      if(legal_id!=''){
        $.ajax({
          url:base_url+'sales/get_po_date',
          type:"POST",
          data:{'csrf_test_name':csrf_hash,"legal_id":legal_id,},
          success:function(res){
           $('#'+prefix_sale+'_staturory_po_date').val(res);
          },
        });
     }else{
      Materialize.toast('Please select statutory P.O number', 2000,'red rounded');
    }
    });
      $('#'+prefix_sale+'_bank_name').on('change' ,function(){
      var bank_id = $(this).val();
      if(bank_id!=''){
      $.ajax({
        url:base_url+'sales/get_cmp_bank_details/'+bank_id,
        type:"POST",
        success:function(res){
          var data=JSON.parse(res);
          $('#'+prefix_sale+'_bank_account_no').val(data[0]['cbank_account_no']);
           $('#'+prefix_sale+'_bank_branch_no').val(data[0]['cbank_branch_name']);
         // $('#'+prefix_sale+'_bank_branch_no option[value="'+bank_id+'"]').attr('selected','selected');
         // $('#'+prefix_sale+'_bank_branch_no').material_select();
          $('#'+prefix_sale+'_bank_ifcs_code').val(data[0]['cbank_ifsc_code']);
          $('#'+prefix_sale+'_bank_swift_code').val(data[0]['cbank_swift_code']);
        },
      });
    }else{
      Materialize.toast('Please select Bank..!', 2000,'red rounded');
    }
    });
      $("#new_dc_country").on("change",function(){
            var country_id = $(this).val();
            $.ajax({
              url:base_url+'index/get_states',
              type:"POST",
              data:{'csrf_test_name':csrf_hash,'country_id':country_id},
              success:function(res){
                $("#new_dc_state").html(res);
                $("#new_dc_state").parents('.input-field').addClass('label-active');
                $('#new_dc_state').material_select();
              },
            });
          });
          $("#new_dc_state").on("change",function(){
            var state_id = $(this).val();
            $.ajax({
              url:base_url+'index/get_cities',
              type:"POST",
              data:{'csrf_test_name':csrf_hash,'state_id':state_id},
              success:function(res){
               $("#new_dc_city").html(res);
               $("#new_dc_city").parents('.input-field').addClass('label-active');
               $('#new_dc_city').material_select();
              },
            });
          });
          
      $('#add').off().on('change','.services_'+prefix_sale,function() {
          var tr=$(this).closest('tr').attr('id');
          var idd=$(this).attr('id');
          $('#hsn'+tr).attr('value','');
          if($('#'+prefix_sale+'_customer_id').val() == '')
            {
             Materialize.toast('Please select client', 2000,'red rounded');
             $("#"+idd).find('option:eq(0)').prop('selected', true);
             $("#"+idd).material_select();
            }else if($(this).val()=='create_item'){
              //
              $("#new-item").show();
    $(".modal-overlay").show();
    $("#create_item")[0].click();
            }else{

             $.ajax({
                      url: base_url+'sales/get_hsn',
                      type: 'POST',
                      data: {'csrf_test_name':csrf_hash,'service_id':$(this).val()},
                      success: function(result) {
                           $('#hsn'+tr).attr('value',result[0]['service_hsn_no']);
                          if($('#'+prefix_sale+'_customer_gstin').val()!=''){
                           $('#cess_per'+tr).attr('value',result[0]['service_cess_tax']);
                          }else{
                           $('#cess_per'+tr).attr('value',0);
                          }

                           
                           if($('#tax_type').val()=="true"){
                          /* if($('#'+prefix_sale+'_customer_gstin').val()!=''){
                            var half_gst = parseFloat((result[0]['service_gst'])/2);
                           }else{
                           var half_gst = 0;
                           }*/
                           var half_gst = parseFloat((result[0]['service_gst'])/2);
                           $('#cgst_per'+tr).attr('value',half_gst);
                           $('#sgst_per'+tr).attr('value',half_gst);
                            $('#other_per'+tr).attr('value',result[0]['stax_percentage']);
                        }else{
                          /*if($('#'+prefix_sale+'_customer_gstin').val()!=''){
                            $('#igst_per'+tr).attr('value',result[0]['service_gst']);
                           }else{
                           $('#igst_per'+tr).attr('value',0);
                           }*/
                            $('#igst_per'+tr).attr('value',result[0]['service_gst']);
                              $('#other_per'+tr).attr('value',result[0]['stax_percentage']);
                        }
                        $('.quantity').trigger('change');  
                      },
                      error: function(result) {
                         // alert(result);
                         alert('Error Processing');
                      },
                  });
        }
           return false;

      });  
    $('#add').on('change','.quantity,.rate,.taxable_amt,.discount_rs',function() {
      
             var tr=$(this).closest('tr').attr('id');
             if($('#'+prefix_sale+'_customer_id').val() == '')
              {
                Materialize.toast('Please select client', 2000,'red rounded');
             }/* else if($('#'+prefix_sale+'_customer_place').val()==''){
                Materialize.toast('Please select Place of Supply first..!', 2000,'red rounded');
              }*/else if($("#sales_services"+tr).val()==''){
                Materialize.toast('Please select Service first!', 2000,'red rounded');
              }else{
               
              var qty=$('#qty'+tr).val();
              var rate=$('#rate'+tr).val();
               var taxable_amt=$('#taxable_amt'+tr).val();
              var discount=0;
              if($('#discount'+tr).val()){
               discount=parseFloat($('#discount'+tr).val());
              }

              
              var main_taxt=0;
              if((qty!='' && rate!='') || taxable_amt!=''){
                var tot_price=0;
                if(qty!='' && rate!=''){
                 tot_price=parseFloat(parseFloat(qty) * parseFloat(rate)).toFixed(2);
                  $('#taxable_amt'+tr).val(parseFloat(tot_price)-discount);
                  tot_price=parseFloat(tot_price)-discount;
                }else{
                  tot_price=parseFloat(taxable_amt).toFixed(2);
                }
               /* if(taxable_amt==''){
                tot_price=parseFloat(qty) * parseFloat(rate);
                $('#taxable_amt'+tr).val(tot_price);
                }else{
                  tot_price=parseFloat($('#taxable_amt'+tr).val());
                }*/
                //var tot_price=parseFloat(tot_price);
                if($('#tax_type').val()=="true"){
                    var cgst_per=0;
                    var sgst_per=0;
                    if($('#cgst_per'+tr).val()){
                    cgst_per=$('#cgst_per'+tr).val();
                    }
                    if($('#sgst_per'+tr).val()){
                    sgst_per=$('#sgst_per'+tr).val();
                    }
                    
                    var total_cgst_rs=parseFloat((parseFloat(tot_price)*parseFloat(cgst_per))/100).toFixed(2);
                    var total_sgst_rs=parseFloat((parseFloat(tot_price)*parseFloat(sgst_per))/100).toFixed(2);
                    main_taxt = parseFloat(parseFloat(total_cgst_rs)+parseFloat(total_sgst_rs)).toFixed(2);

                    $('#cgst'+tr).attr('value',parseFloat(total_cgst_rs));
                    $('#sgst'+tr).attr('value',parseFloat(total_sgst_rs));
                }else{
                    var igst_per=0;
                    if($('#igst_per'+tr).val()){
                    igst_per=$('#igst_per'+tr).val();
                    }
                    
                    var total_igst_rs=parseFloat((parseFloat(tot_price)*parseFloat(igst_per))/100).toFixed(2);
                    main_taxt = total_igst_rs;
                    $('#igst'+tr).attr('value',parseFloat(total_igst_rs));
                }
                 var cess_per=0;
                  if($('#cess_per'+tr).val()){
                  cess_per=$('#cess_per'+tr).val();
                  }


                var total_cess_rs=parseFloat((parseFloat(tot_price)*parseFloat(cess_per))/100).toFixed(2);
                $('#cess'+tr).attr('value',parseFloat(total_cess_rs));

                var other_per=0;
                  if($('#other_per'+tr).val()){
                  other_per=$('#other_per'+tr).val();
                  }
                 var total_other_rs=parseFloat((parseFloat(tot_price)*parseFloat(other_per))/100).toFixed(2);
                $('#other'+tr).attr('value',parseFloat(total_other_rs));

                var sub_amount= 0;
                if($('.reverse_charge_chkbox').is(':checked')){
                  sub_amount = parseFloat(parseFloat(tot_price)).toFixed(2);
                }else{
                  var main_tax=parseFloat(parseFloat(main_taxt) + parseFloat(total_cess_rs)+ parseFloat(total_other_rs)).toFixed(2);
                  sub_amount = parseFloat((parseFloat(tot_price) + parseFloat(main_tax))).toFixed(2);
                }
                //console.log(tr);
                $('#sub_amount'+tr).val(parseFloat(sub_amount));
                //console.log($('#sub_amount'+tr).val());
                var total_taxable=0;
                $(".taxable_amt").each(function(){
                  if($(this).val()!=''){
                    total_taxable=parseFloat(parseFloat(total_taxable)+parseFloat($(this).val())).toFixed(2);
                    
                  }
                });
                $('#total_taxable').attr('value',parseFloat(total_taxable));
                var total_discount=0;
                $(".discount_rs").each(function(){
                  if($(this).val()!=''){
                     total_discount=parseFloat(parseFloat(total_discount)+parseFloat($(this).val())).toFixed(2);
                  }
                });
                $('#total_discount').attr('value',parseFloat(total_discount));
                if($('#tax_type').val()=="true"){
                var total_cgst=0;
                $(".cgst_rs").each(function(){
                  if($(this).val()!=''){
                     total_cgst=parseFloat(parseFloat(total_cgst)+parseFloat($(this).val())).toFixed(2);
                  }
                });
                var total_sgst=0;
                $(".sgst_rs").each(function(){
                  if($(this).val()!=''){
                     total_sgst=parseFloat(parseFloat(total_sgst)+parseFloat($(this).val())).toFixed(2);
                  }
                });
                var total_cess=0;
                $(".cess_rs").each(function(){
                  if($(this).val()!=''){
                     total_cess=parseFloat(parseFloat(total_cess)+parseFloat($(this).val())).toFixed(2);
                  }
                });
                 var total_other=0;
                $(".other_rs").each(function(){
                  if($(this).val()!=''){
                     total_other=parseFloat(parseFloat(total_other)+parseFloat($(this).val())).toFixed(2);
                  }
                });
                $('#tot_cgst').attr('value',parseFloat(total_cgst));
                $('#tot_sgst').attr('value',parseFloat(total_sgst));
                $('#tot_cess').attr('value',parseFloat(total_cess));
                 $('#tot_other').attr('value',parseFloat(total_other));
                if($('.reverse_charge_chkbox').is(':checked')){
                  $('#tax_cgst').attr('value',parseFloat(total_cgst));
                  $('#tax_sgst').attr('value',parseFloat(total_sgst));
                  $('#tax_cess').attr('value',parseFloat(total_cess));
                  $('#tax_other').attr('value',parseFloat(total_other));
                  $('#tax_amount').attr('value',parseFloat(total_cgst)+parseFloat(total_sgst)+parseFloat(total_cess));
                 }
                }else{
                var total_igst=0;
                $(".igst_rs").each(function(){
                  if($(this).val()!=''){
                     total_igst=parseFloat(parseFloat(total_igst)+parseFloat($(this).val())).toFixed(2);
                  }
                });
                var total_cess=0;
                $(".cess_rs").each(function(){
                  if($(this).val()!=''){
                     total_cess=parseFloat(parseFloat(total_cess)+parseFloat($(this).val())).toFixed(2);
                  }
                });
                
                 var total_other=0;
                $(".other_rs").each(function(){
                  if($(this).val()!=''){
                     total_other=parseFloat(parseFloat(total_other)+parseFloat($(this).val())).toFixed(2);
                  }
                });
                $('#tot_igst').attr('value',parseFloat(total_igst));
                $('#tot_cess').attr('value',parseFloat(total_cess));
                $('#tot_other').attr('value',parseFloat(total_other));
                 if($('.reverse_charge_chkbox').is(':checked')){
                    $('#tax_igst').attr('value',parseFloat(total_igst));
                    $('#tax_cess').attr('value',parseFloat(total_cess));
                     $('#tax_other').attr('value',parseFloat(total_other));
                    var tx=parseFloat(parseFloat(total_igst)+parseFloat(total_cess)+parseFloat(total_other)).toFixed(2);
                    $('#tax_amount').attr('value',parseFloat(tx));
                 } 
                }
                
                var total_amount=0;
                $(".samount_rs").each(function(){
                  if($(this).val()!='' ){
                    total_amount=parseFloat(parseFloat(total_amount)+parseFloat($(this).val())).toFixed(2);
                  }
                });
                
               
    var tot_amt= add_commas(total_amount);
              
                $('#tot_amount').attr('value',parseFloat(total_amount));
                $('#grand_total_'+prefix_sale).text(tot_amt);
                var currency= $('#grand_total_sym_'+prefix_sale).text();
               
                var words=toWords(parseFloat(total_amount),currency);
                    if(words==''){
                        $('#to_words_'+prefix_sale).text('ZERO');
                    }else{
                     $('#to_words_'+prefix_sale).text(words.toUpperCase()+' ONLY');
                    }
                }else{
                   
                }
              }
      });
 });
//To select state name
function selectCountry(val,type_id) {

  if(type_id=="gst_edit_location"){
$('#gst_edit_location').val(val);
$("#suggesstion_edit_location").hide();
  }else{
    
$('.addgstin').find("#"+type_id).val(val);
$("#suggesstion-box_"+type_id).hide();
}
}
function calculate_amount_inv(prefix) {
    var total_taxable=0;
    $(".taxable_amt").each(function(){
      if($(this).val()!=''){
         total_taxable=parseFloat(parseFloat(total_taxable)+parseFloat($(this).val())).toFixed(2);
         
      }
    });
    $('#total_taxable').attr('value',parseFloat(total_taxable));
    var total_discount=0;
    
    $(".discount_rs").each(function(){
      if($(this).val()!=''){
        total_discount=parseFloat(parseFloat(total_discount)+parseFloat($(this).val())).toFixed(2);
         
      }
    });
    $('#total_discount').attr('value',parseFloat(total_discount));
    var total_cess=0;
    $(".cess_rs").each(function(){
      if($(this).val()!=''){
         total_cess=parseFloat(parseFloat(total_cess)+parseFloat($(this).val())).toFixed(2);
      }
    });

    var total_other=0;
    $(".other_rs").each(function(){
      if($(this).val()!=''){
         total_other=parseFloat(parseFloat(total_other)+parseFloat($(this).val())).toFixed(2);
      }
    });

    if($('#tax_type').val()=="true"){
    var total_cgst=0;
    $(".cgst_rs").each(function(){
      if($(this).val()!=''){
         total_cgst=parseFloat(parseFloat(total_cgst)+parseFloat($(this).val())).toFixed(2);
      }
    });
    var total_sgst=0;
    $(".sgst_rs").each(function(){
      if($(this).val()!=''){
         total_sgst=parseFloat(parseFloat(total_sgst)+parseFloat($(this).val())).toFixed(2);
      }
    });

    $('#tot_cgst').attr('value',parseFloat(total_cgst));
    $('#tot_sgst').attr('value',parseFloat(total_sgst));
   
    if($('.reverse_charge_chkbox').is(':checked')){
    $('#tax_cgst').attr('value',parseFloat(total_cgst));
    $('#tax_sgst').attr('value',parseFloat(total_sgst));
    var tx=parseFloat(parseFloat(total_cgst)+parseFloat(total_sgst)+parseFloat(total_cess)).toFixed(2);
    $('#tax_amount').attr('value',parseFloat(tx));
    }
    }else{
    var total_igst=0;
    $(".igst_rs").each(function(){
      if($(this).val()!=''){
         total_igst=parseFloat(parseFloat(total_igst)+parseFloat($(this).val())).toFixed(2);
      }
    });
    $('#tot_igst').attr('value',parseFloat(total_igst)); 
     if($('.reverse_charge_chkbox').is(':checked')){
        $('#tax_igst').attr('value',parseFloat(total_igst));
         var tx=parseFloat(parseFloat(total_igst)+parseFloat(total_cess)).toFixed(2);
        $('#tax_amount').attr('value',parseFloat(tx));
      } 
    }
    $('#tot_cess').attr('value',parseFloat(total_cess));
     $('#tot_other').attr('value',parseFloat(total_other));
    var total_amount=0;
    $(".samount_rs").each(function(){
      if($(this).val()!='' ){
        total_amount=parseFloat(parseFloat(total_amount)+parseFloat($(this).val())).toFixed(2);
        
      }
    });

   
  

    $('#tot_amount').attr('value',parseFloat(total_amount));
    $('#grand_total_'+prefix).text(parseFloat(total_amount));
     var currency= $('#grand_total_sym_'+prefix).text();
    var words=toWords(parseFloat(total_amount),currency);
    if(words==''){
        $('#to_words_'+prefix).text('ZERO');
    }else{
         $('#to_words_'+prefix).text(words.toUpperCase()+' ONLY');
    }
                 
}



/*function calculate_amount(prefix) {

    var total_discount=0;
    $(".discount_rs").each(function(){
      if($(this).val()!=''){
         total_discount+=parseFloat($(this).val());
      }
    });
    $('#total_discount').attr('value',parseFloat(total_discount));
    if($('#tax_type').val()=="India"){
    var total_cgst=0;
    $(".cgst_rs").each(function(){
      if($(this).val()!=''){
         total_cgst+=parseFloat($(this).val());
      }
    });
    var total_sgst=0;
    $(".sgst_rs").each(function(){
      if($(this).val()!=''){
         total_sgst+=parseFloat($(this).val());
      }
    });
    $('#tot_cgst').attr('value',total_cgst);
    $('#tot_sgst').attr('value',total_sgst);
    }else{
    var total_igst=0;
    $(".dc_igst_rs").each(function(){
      if($(this).val()!=''){
         total_igst+=parseFloat($(this).val());
      }
    });
    $('#tot_igst').attr('value',total_igst); 
        }

    var total_amount=0;
    $(".samount_rs").each(function(){
      if($(this).val()!='' ){
        
        total_amount+=parseFloat($(this).val());
      }
    });


    $('#tot_amount').attr('value',parseFloat(total_amount));
    $('#grand_total_'+prefix_sale).text(parseFloat(total_amount));
    var words=toWords(parseFloat(total_amount));
    if(words==''){
        $('#to_words_'+prefix_sale).text('ZERO');
    }else{
         $('#to_words_'+prefix_sale).text(words.toUpperCase()+' ONLY');
    }
                 
}
*/