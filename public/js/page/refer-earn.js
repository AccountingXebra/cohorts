$(document).ready(function() {
	console.log("Referal code page");
	$('#create_referal_code').validate({
		rules:{
			contact_name:{
				required:true,
			},
			email_id:{
				required:true,
			},
			cell_no:{
				required:true,
			},
			pan_no:{
				required:true,
			},
		},
		messages:{
			contact_name:{
				required:"Contact Name is required",
			},
			email_id:{
				required:"Email is required",
			},
			cell_no:{
				required:"Cell Number is required",
			},
			pan_no:{
				required:"PAN Number is required",
			},
		},

		errorPlacement: function(error, element) {

			          if(element.prop('tagName')  == 'SELECT') {
			          	error.insertAfter(element.closest('.input-field').find('.js-example-basic-single'));
			          }else{
			            error.insertAfter(element);
			          }
			    } ,
			    submitHandler: function(form) {
					$("input[name='csrf_test_name']").val(csrf_hash);
			        form.submit();
			    }



	});

	$('#referal_code_table');
	referEarnDatatable(base_path()+'refer_earn/get_referal_details','referal_code_table');
	$('select').material_select();

	// START END Date
	$('.referal-search').on('change','#referal_start_date,#referal_end_date',function() {

		if($(this).val()!='') {
			referEarnDatatable(base_path()+'refer_earn/get_referal_details','referal_code_table');
			$('select').material_select();
		}
	});

	//searching filter
	$('.referal-search').on('keyup', '#search_referal', function() {

		referEarnDatatable(base_path()+'refer_earn/get_referal_details','referal_code_table');
		$('select').material_select();
	});

	$('#invite_email').on('click', function() {
		if($('#email').val()!=''){
			// add to databbase
			console.log('hey there its working...');
			if(csrf_hash===""){
				csrf_hash=csrf_hash;
			}
			$.ajax({
				type: "POST",
				data: {'csrf_test_name':csrf_hash,'email': $('#email').val(), 'refer_id': $('#referal_code').val()},
				url: base_url+'refer_earn/invite_by_email/',
				success:function(result) {
					Materialize.toast('Referral code sent along with invite', 2000,'green rounded');
					$('#email').val('');
					//update the datatables
					referEarnDatatable(base_path()+'refer_earn/get_referal_details','referal_code_table');
					$('select').material_select();
				},

			});
		}
	});


	$('#invite_phone_no').on('click', function() {
		if($('#phone_no').val()!=''){
			// add to databbase
			console.log('hey there its working...');
			if(csrf_hash===""){
				csrf_hash=csrf_hash;
			}
			$.ajax({
				type: "POST",
				data: {'csrf_test_name':csrf_hash,'phone_no': $('#phone_no').val(), 'refer_id': $('#referal_code').val()},
				url: base_url+'refer_earn/invite_by_sms/',
				success:function(result) {
						Materialize.toast('Referral code sent along with invite', 2000,'green rounded');
					$('#phone_no').val('');
					//update the datatables
					referEarnDatatable(base_path()+'refer_earn/get_referal_details','referal_code_table');
					$('select').material_select();
				},

			});
		}
	});


	// download bulk action
	$("body").on('click','#download_multiple_refer_list',function(e) {

		var dnwl_ids = $('#download_multiple_refer_list').attr('data-multi_srec');
		var i = 0;
		var length = $('input[type="checkbox"]:checked').length;
		var array = [];
		$('.referal_bulk_action:checked').each(function(){
			array.push($(this).attr('id'));
		});
		console.log(array);
		var id = [];
		for(;i<array.length;i++) {
			id.push(array[i].substring(14, array[i].length));
			console.log(id);
		}
		window.location=base_url+'Refer_earn/download-multiple-referearn?id='+id;
		Materialize.toast('Sales Receipts has been downloaded', 2000,'green rounded');
	});

});


function reset_referalfilter($id){

  $('.btn-date,.search-hide-show').val('');
  $('#search_referal').val('');
 // $('.search-hide-show').toggle();
	referEarnDatatable(base_path()+'refer_earn/get_referal_details','referal_code_table')
	$('select').material_select();

}

var bulk_referals = [];
$("input[id='referal_bulk']").on("click", function(){
if($(this).is(':checked',true)) {
  bulk_referals= [];
	$(".referal_bulk_action").prop('checked', true);
	$(".referal:checked").each(function() {
			bulk_referals.push($(this).val());
		});
		bulk_referals = bulk_referals.join(",");
		$('#deactive_multiple_referals').attr('data-multi_referal',bulk_referals);
    //$('#download_multiple_customers').attr('data-multi_download',bulk_referals);
	}
	else {
    $(".referal_bulk_action:checked").each(function() {
      $(this).prop('checked', false);
    });
		bulk_referals = [];
		$('#deactive_multiple_referals').attr('data-multi_referal',0);
    $('#download_multiple_referals').attr('data-multi_referal',0);
	}
});




function referEarnDatatable(url,id) {
	$('#'+id).DataTable().destroy();
	$('#deactive_multiple_referals').attr('data-multi_vendors',0);
	table_sales_inv = $('#'+id).dataTable({
	  "scrollY": 299,
	 "bServerSide": true,
	"bProcessing": true,
		"bDeferRender": true,
    "aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0 , 1 , 2, 3 , 4 , 5, 6, 7] }],
	 "aoColumns": [
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
	],
	oLanguage: { sLengthMenu: "_MENU_", },
    "order": [0,'DESC'],
    "bPaginate": true,
    "bLengthChange": true,
    "sDom": 'Rfrtlip',

    "bFilter": true,
    "bInfo": false,
    "searching":false,
	"iDisplayLength": 10,
		lengthMenu: [
            [ 10, 20, 30, 50, -1 ],
            [ 'SHOW 10', 'SHOW 20', 'SHOW 30', 'SHOW 50', 'SHOW ALL' ]
        ],
	//"aaSorting": [[ 1, "desc" ]],
	"ajax": {
			"url": url,
			"type": "POST",
			"data": {'csrf_test_name':csrf_hash,'search': $('#search_referal').val(), 'referal_start_date': $('#referal_start_date').val(), 'referal_end_date': $('#referal_end_date').val()},
		},
	"dom": "<'row'<'col-md-6'l><'col-md-6'f>><'row'<'col-md-6'B><'col-md-6'p>><'row'<'col-md-12't>><'row'<'col-md-12'i>>",
	"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {

		/*if($(aData[7]+' div').hasClass('deactive_record')){
					console.log(aData[7]);
			$('td', nRow).parent('tr').addClass('deactivate-row');
		}
*/
		if($(aData[5]+'span').hasClass('green-c')){

			$('td:nth-child(6)', nRow).addClass('green-c');
		}else{
			$('td:nth-child(6)', nRow).addClass('red-c');
		}
		$('td:first-child', nRow).addClass('bulk');
		//$('td:nth-child(7)', nRow).addClass('green-c');
		//$('td:nth-child(2)', nRow).addClass('profil-img');
		$('td:last-child', nRow).addClass('action-tab');
     }
  });
}
