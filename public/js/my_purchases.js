
$("#add_supplier").validate({
				rules:{
					supplier_name:{
						required:true,
					},
					pan_no:{
						required:true,
						pannoregex:true,
					},
					state:{
						required:true,
					},
				},
				messages:{
					supplier_name:{
						required:"Supplier Name is required",
					},
					pan_no:{
						required:"PAN Number is required",
						pannoregex:"Enter PAN no in a valid format",
					},
					state:{
						required:"Billing State is required",
					},
				},
			});
			
$("#add_supplier_gstin").submit(function(e){
				e.preventDefault();
			}).validate({
				rules:{
					'gstin[]':{
						required:true,
						gstregex:true,
					},
					'location[]':{
						required:true,
					},
				},
				messages:{

					gstin:{
						required:"GST Number is required",
						gstregex:"Enter GSTIN in a valid format",
					},
					location:{
						required:"Place of supply is required",
					},
				},
				submitHandler:function(form){
						var frm=$(form).serialize();
								$.ajax({
								url:base_url+'My_suppliers/add_gst_info',
								type:"POST",
								data:{'csrf_test_name':csrf_hash,"frm":frm,},
								success:function(res){
									console.log(res);
									if(res!= false)
									{
										var data=JSON.parse(res);
										var html_1 ='';
										var final_html ='';
										if(data != '')
										{
											for(var i=0;i<data.length;i++){
											if(i == 0)
											   {
												html_1='<div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">GST No.</span><p>'+data[i].gst_no+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">PLACE OF SUPPLY</span><p>'+data[i].place_of_supply+'</p></div><div class="col l1 s1 m1 gstaction"><a href="#" class="remove_gst_model" data-location="'+data[i].place_of_supply+'" data-gst_no="'+data[i].gst_no+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a></div></div></div>';
											   }
											   else
											   {
												 html_1='<div class="gstmore"><div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">GST No.</span><p>'+data[i].gst_no+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">PLACE OF SUPPLY</span><p>'+data[i].place_of_supply+'</p></div><div class="col l1 s1 m1 gstaction"><a href="#" class="remove_gst_model" data-location="'+data[i].place_of_supply+'" data-gst_no="'+data[i].gst_no+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a></div></div></div></div>';
											  
											   }
												 final_html = final_html + html_1;
												 $("#show_gst_list").html(final_html);
												 $(".supp_tot_gst").html(data.length);
											}
										}
										else
										{
											//html_1='<div class="gst-boxs add-gst">Add GSTIN Data</div>';
											//$("#show_gst_list").html(html_1);
										}
									}
									else
									{
										Materialize.toast('Error while processing. Please try again', 2000,'red rounded');
									}
									$("#add_supplier_gstin").find("input[type=text], textarea").val("");
									$("#supplier_gst_modal").modal('close');
							},
							complete:function(res){
								
								$(document).ready( function () {
								$(document).on('click','.remove_gst_model', function(){
									var gst_no = $(this).data('gst_no');
									var location = $(this).data('location');
									
									$('#remove_supp_gst_data').modal('open');
									$('#remove_supp_gst_data #remove_gst_no').val(gst_no);
									$('#remove_supp_gst_data #remove_location').val(location);
								});
								
								$('.remove_gst').off().on('click',function(){
										var gst_no = $('#remove_gst_no').val();
										var location = $('#remove_location').val();
										$.ajax({
											url:base_url+'My_suppliers/remove_gst_info',
											type:"post",
											data:{'csrf_test_name':csrf_hash,"gst_no":gst_no,"location":location},
											success:function(res){
													if(res!= false)
													{
														var data=JSON.parse(res);
														var html_1 ='';
														var final_html ='';
														if(data != '')
														{
															for(var i=0;i<data.length;i++){
															if(i == 0)
															   {
																html_1='<div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
															   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">GST No.</span><p>'+data[i].gst_no+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">PLACE OF SUPPLY</span><p>'+data[i].place_of_supply+'</p></div><div class="col l1 s1 m1 gstaction"><a href="#" class="remove_gst_model" data-location="'+data[i].place_of_supply+'" data-gst_no="'+data[i].gst_no+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a></div></div></div>';
															   }
															   else
															   {
																 html_1='<div class="gstmore"><div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
															   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">GST No.</span><p>'+data[i].gst_no+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">PLACE OF SUPPLY</span><p>'+data[i].place_of_supply+'</p></div><div class="col l1 s1 m1 gstaction"><a href="#" class="remove_gst_model" data-location="'+data[i].place_of_supply+'" data-gst_no="'+data[i].gst_no+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a></div></div></div></div>';
															  
															   }
																 final_html = final_html + html_1;
																 $("#show_gst_list").html(final_html);
																 $(".supp_tot_gst").html(data.length);
															}
															Materialize.toast('GSTIN Details deactivated', 2000,'green rounded');
														}
														else
														{
															Materialize.toast('GSTIN Details deactivated', 2000,'green rounded');
															//html_1='<div class="gst-boxs add-gst">Add GSTIN Data</div>';
															//$("#show_gst_list").html(html_1);
															$(".supp_tot_gst").html(0);
														}
													}
													else
														{
															Materialize.toast('GSTIN Details deactivated', 2000,'green rounded');
															//html_1='<div class="gst-boxs add-gst">Add GSTIN Data</div>';
															//$("#show_gst_list").html(html_1);
															$(".supp_tot_gst").html(0);
														}
												},
						
											});										
									});
									});
							},
					});
					
				},
			});
			
			$("#add_contact_person").submit(function(e){
				e.preventDefault();
			}).validate({
				rules:{

					name:{
						required:true,
					},
					mobile:{
						required:true,
					},
					email:{
						required:true,
						email:true
					},
					password:{
						required:true,
						minlength:8,
						maxlength:12
					},
				},
				messages:{

					name:{
						required:"Name is required",
					},
					mobile:{
						required:"Mobile Number is required",
					},
					email:{
						required:"Email ID is required",
						email:'Enter valid Email ID',
					},
					password:{
						required:"Password is required",
						minlength:'Enter minimum 8 characters for password.',
						maxlength:"Maximum 12 character long password allowed.",
					},
				},
				submitHandler:function(form){
						var frm=$(form).serialize();
								$.ajax({
								url:base_url+'My_suppliers/add_contact_person_info',
								type:"POST",
								data:{'csrf_test_name':csrf_hash,"frm":frm,},
								success:function(res){
									if(res!= false)
									{
										var data=JSON.parse(res);
										var html_person ='';
										var final_html_1 ='';
										if(data != '')
										{
											for(var i=0;i<data.length;i++){
											if(i == 0)
											{
												html_person='<div class="row"><div class="contact-person-box"><div class="col l1 s1 m1"><input type="radio" id="contact-person" checked name="default"><label class="fill-green-radio" for="contact-person-1"></label></div><div class="col l10 s10 m10 content-area"><div class="col l3 s3 m3"><div class="gradient-wrapper"><div class="maincircle">100%</div></div></div><div class="col l9 s9 m9"><p class="name"><strong>'+data[i].name+'</strong></p><p class="email">'+data[i].email+'</p></div></div><div class="col l1 s1 m1"><a href="#" class="remove_contact_person" data-name="'+data[i].name+'" data-email="'+data[i].email+'"><i class="material-icons">close</i></a></div></div></div>';
												
											   }
											   else if(i == 1)
											   {
												  html_person='<div class="row"><div class="contact-person-box"><div class="col l1 s1 m1"><input type="radio" id="contact-person'+(i+1)+'" name="default"><label class="fill-green-radio" for="contact-person'+(i+1)+'"></label></div><div class="col l10 s10 m10 content-area"><div class="col l3 s3 m3"><div class="gradient-wrapper"><div class="maincircle">100%</div></div></div><div class="col l9 s9 m9"><p class="name"><strong>'+data[i].name+'</strong></p><p class="email">'+data[i].email+'</p></div></div><div class="col l1 s1 m1"><a href="#" class="remove_contact_person" data-name="'+data[i].name+'" data-email="'+data[i].email+'"><i class="material-icons">close</i></a></div></div></div>'; 
											   }
											   else
											   {
												  html_person='<div class="contactmore"><div class="row"><div class="contact-person-box"><div class="col l1 s1 m1"><input type="radio" id="contact-person'+(i+1)+'" name="default"><label class="fill-green-radio" for="contact-person'+(i+1)+'"></label></div><div class="col l10 s10 m10 content-area"><div class="col l3 s3 m3"><div class="gradient-wrapper"><div class="maincircle">100%</div></div></div><div class="col l9 s9 m9"><p class="name"><strong>'+data[i].name+'</strong></p><p class="email">'+data[i].email+'</p></div></div><div class="col l1 s1 m1"><a href="#" class="remove_contact_person" data-name="'+data[i].name+'" data-email="'+data[i].email+'"><i class="material-icons">close</i></a></div></div></div></div>'; 											  
											   }
												 final_html_1 = final_html_1 + html_person;
												 $("#show_contact_list").html(final_html_1);
												 $(".tot_contact_person").html(data.length);
											}											
										}
										else
										{
											html_person='<div class="gst-boxs add-gst">Add Contact Person Data</div>';
											$("#show_contact_list").html(html_person);
											$(".tot_contact_person").html(0);
										}
									}
									else
									{
										Materialize.toast('Error while processing. Please try again', 2000,'red rounded');
									}
									$("#add_contact_person").find("input[type=text],input[type=password], textarea").val("");
									$("#supplier_contactmodal").modal('close');
								
							},
							complete:function(res){
										
								$(document).on('click','.remove_contact_person',function(){				
									var name = $(this).data('name');
									var email = $(this).data('email');
									$('#remove_supp_contact_person_model').modal('open');
									$('#remove_supp_contact_person_model #remove_contact_name').val(name);
									$('#remove_supp_contact_person_model #remove_contact_email').val(email);
								});
			
									$('.remove_person').off().on('click',function(){
						
										var name = $('#remove_contact_name').val();
										var email = $('#remove_contact_email').val();
						
										$.ajax({
						
											url:base_url+'My_suppliers/remove_contact_person',
						
											type:"post",
						
											data:{'csrf_test_name':csrf_hash,"name":name,"email":email},
						
											success:function(res){
												console.log(res);
													if(res!= false)
													{
														var data=JSON.parse(res)
														var html_person ='';
														var final_html_1 ='';
														if(data != '')
														{
															//$("#show_contact_list").html("");
															for(var i=0;i<data.length;i++){
															if(i == 0)
															{
																html_person='<div class="row"><div class="contact-person-box"><div class="col l1 s1 m1"><input type="radio" id="contact-person" checked name="default"><label class="fill-green-radio" for="contact-person-1"></label></div><div class="col l10 s10 m10 content-area"><div class="col l3 s3 m3"><div class="gradient-wrapper"><div class="maincircle">100%</div></div></div><div class="col l9 s9 m9"><p class="name"><strong>'+data[i].name+'</strong></p><p class="email">'+data[i].email+'</p></div></div><div class="col l1 s1 m1"><a href="#" class="remove_contact_person" data-name="'+data[i].name+'" data-email="'+data[i].email+'"><i class="material-icons">close</i></a></div></div></div>';			
															   }
															   else if(i == 1)
															   {
																  html_person='<div class="row"><div class="contact-person-box"><div class="col l1 s1 m1"><input type="radio" id="contact-person'+(i+1)+'" name="default"><label class="fill-green-radio" for="contact-person'+(i+1)+'"></label></div><div class="col l10 s10 m10 content-area"><div class="col l3 s3 m3"><div class="gradient-wrapper"><div class="maincircle">100%</div></div></div><div class="col l9 s9 m9"><p class="name"><strong>'+data[i].name+'</strong></p><p class="email">'+data[i].email+'</p></div></div><div class="col l1 s1 m1"><a href="#" class="remove_contact_person" data-name="'+data[i].name+'" data-email="'+data[i].email+'"><i class="material-icons">close</i></a></div></div></div>'; 	   
															   }
															   else
															   {
																  html_person='<div class="contactmore"><div class="row"><div class="contact-person-box"><div class="col l1 s1 m1"><input type="radio" id="contact-person'+(i+1)+'" name="default"><label class="fill-green-radio" for="contact-person'+(i+1)+'"></label></div><div class="col l10 s10 m10 content-area"><div class="col l3 s3 m3"><div class="gradient-wrapper"><div class="maincircle">100%</div></div></div><div class="col l9 s9 m9"><p class="name"><strong>'+data[i].name+'</strong></p><p class="email">'+data[i].email+'</p></div></div><div class="col l1 s1 m1"><a href="#" class="remove_contact_person" data-name="'+data[i].name+'" data-email="'+data[i].email+'"><i class="material-icons">close</i></a></div></div></div></div>'; 
															  
															   }
															   Materialize.toast('Contact details removed', 2000,'green rounded');
																 final_html_1 = final_html_1 + html_person;
																 $("#show_contact_list").html(final_html_1);
																 $(".tot_contact_person").html(data.length);
															}
														}
														else
														{
															Materialize.toast('Contact details removed', 2000,'green rounded');
															html_person='<div class="gst-boxs add-gst">Add Contact Person Data</div>';
															$("#show_contact_list").html(html_person);
															$(".tot_contact_person").html("0");
														}
													}
													else
													{
														Materialize.toast('Contact details removed', 2000,'green rounded');
														html_person='<div class="gst-boxs add-gst">Add Contact Person Data</div>';
														$("#show_contact_list").html(html_person);
														$(".tot_contact_person").html("0");
													}
												},
						
											});										
									});
							},	
					});
				},
			});
			
			$("#add_supp_bank_details").submit(function(e){
				e.preventDefault();
			}).validate({
				rules:{
					supp_bank_name:{
						required:true,
					},
					supp_bank_branch_name:{
						required:true,
					},
					supp_bank_account_no:{
						required:true,
					},
				},
				messages:{
					supp_bank_name:{
						required:"Bank Name is required",
					},
					supp_bank_branch_name:{
						required:"Bank Branch is required",
					},
					supp_bank_account_no:{
						required:"Bank Account Number is required",
					},
				},
				submitHandler:function(form){
						var frm=$(form).serialize();
								 //$(form).ajaxSubmit();
								$.ajax({
								url:base_url+'My_suppliers/add_bank_info',
								type:"POST",
								data:{'csrf_test_name':csrf_hash,"frm":frm,},
								success:function(res){
									console.log(res);
									if(res!= false)
									{
										var data=JSON.parse(res);
										var html_1 ='';
										var final_html ='';
										if(data != '')
										{
											console.log(data);
											for(var i=0;i<data.length;i++){
											if(i == 0)
											   {
												html_1='<div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">BANK ACCOUNT NUMBER</span><p>'+data[i].bank_account_no+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">BANK NAME</span><p>'+data[i].bank_name+'</p></div><div class="col l1 s1 m1 gstaction"><a href="#" class="remove_bank_info_model" data-account_no="'+data[i].bank_account_no+'" data-bank="'+data[i].bank_name+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a></div></div></div>';
											   }
											   else
											   {
												 html_1='<div class="gstmore"><div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">BANK ACCOUNT NUMBER</span><p>'+data[i].bank_account_no+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">BANK NAME</span><p>'+data[i].bank_name+'</p></div><div class="col l1 s1 m1 gstaction"><a href="#" class="remove_bank_info_model" data-account_no="'+data[i].bank_account_no+'" data-bank="'+data[i].bank_name+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a></div></div></div></div>';
											  
											   }
												 final_html = final_html + html_1;
												 $("#show_bank_details").html(final_html);
												 $(".bank_info").html(data.length);
											}
											Materialize.toast('Bank details updated successfully', 2000,'green rounded');
										}
										else
										{
											html_1='<div class="gst-boxs add-gst">Add Bank Data</div>';
											$("#show_bank_details").html(html_1);
										}
									}
									else
									{
										Materialize.toast('Error while processing. Please try again', 2000,'red rounded');
									}
											$("#add_supp_bank_details").find("input[type=text],textarea").val("");
											$('#add-bank').modal('close');
									},
									complete:function(res){
										
									$(document).ready( function () {
									$(document).on('click','.remove_bank_info_model',function(){
										var account_no = $(this).data('account_no');
										var bank = $(this).data('bank');
										
										$('#remove_supp_bank_data').modal('open');
										$('#remove_supp_bank_data #remove_account_no').val(account_no);
										$('#remove_supp_bank_data #remove_bank_name').val(bank);
									});
									
									$('.remove_bank_info').off().on('click',function(){
										var account_no = $('#remove_account_no').val();
										var bank = $('#remove_bank_name').val();
										$.ajax({
											url:base_url+'My_suppliers/remove_bank_info',
											type:"post",
											data:{'csrf_test_name':csrf_hash,"account_no":account_no,"bank":bank},
											success:function(res){
												console.log(res);
													if(res!= false)
													{
														console.log('first');
														var data=JSON.parse(res);
														var html_1 ='';
														var final_html ='';
														if(data != '')
														{
															console.log('first 1');
															for(var i=0;i<data.length;i++){
															if(i == 0)
															   {
																html_1='<div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">BANK ACCOUNT NUMBER</span><p>'+data[i].bank_account_no+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">BANK NAME</span><p>'+data[i].bank_name+'</p></div><div class="col l1 s1 m1 gstaction"><a href="#" class="remove_bank_info_model" data-account_no="'+data[i].bank_account_no+'" data-bank="'+data[i].bank_name+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a></div></div></div>';
															   }
															   else
															   {
																 html_1='<div class="gstmore"><div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">BANK ACCOUNT NUMBER</span><p>'+data[i].bank_account_no+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">BANK NAME</span><p>'+data[i].bank_name+'</p></div><div class="col l1 s1 m1 gstaction"><a href="#" class="remove_bank_info_model" data-account_no="'+data[i].bank_account_no+'" data-bank="'+data[i].bank_name+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a></div></div></div></div>';  
															   }
																 final_html = final_html + html_1;
																 $("#show_bank_details").html(final_html);
																 $(".bank_info").html(data.length);
															}
															Materialize.toast('Bank details deactivated', 2000,'green rounded');
														}
														else
														{
															Materialize.toast('Bank details deactivated', 2000,'green rounded');
															html_1='<div class="gst-boxs add-gst">Add Bank Data</div>';
															$("#show_bank_details").html(html_1);
															$(".bank_info").html(0);
														}
													}
													else
														{
															Materialize.toast('Bank details deactivated', 2000,'green rounded');
															html_1='<div class="gst-boxs add-gst">Add Bank Data</div>';
															$("#show_bank_details").html(html_1);
															$(".bank_info").html(0);
														}
												},
						
											});										
									});
									});
							},
					});
				},
			});
			
			
var newImageObjs = [];
var ImageNo = 0;
$("#Legaldocuments-sup").change(function () {
   var fileUpload = document.getElementById("Legaldocuments-sup");

  if (typeof (FileReader) != "undefined") {
      for (var i = 0; i < fileUpload.files.length; i++) {
          var j = 0;
          var file = fileUpload.files[i];
          var NewFile = fileUpload.files[i];
		  var filesize = this.files[0].size;
		  var kbfilesize = Math.round(filesize / 1000);
          var reader = new FileReader();
          reader.onload = function (e) {

          var html ="<div class='docubox " + ImageNo + "_CClass'>";
          html +="<div class='col s4 m4 l4 divr-border'><div class='pdf-file'><div class='col l3 s3 m3'><img src='"+base_url+"/asset/images/pdf.png' alt='' class='pdf'></div><div class='col l9 s9 m9'><p class='uploadfilename'>"+fileUpload.files[j].name+"</p><span>File Size "+kbfilesize+" kb</span></div></div></div><div class='col s2 m2 l2 divr-border'><div class='date-start setdate' data-position='top' data-delay='50' data-tooltip='Start Date'><input type='text' id='start_date"+ImageNo+"' name='start_date"+ImageNo+"' class='idatepicker hidendate'><img src='"+base_url+"/asset/css/img/icons/calendar-green.png' alt=''></div></div><div class='col s2 m2 l2 divr-border'><div class='date-start setdate end-date' data-position='top' data-delay='50' data-tooltip='End Date'><input type='text' class='idatepicker hidendate' id='end_date"+ImageNo+"' name='end_date"+ImageNo+"'><img src='"+base_url+"/asset/css/img/icons/calendar-green.png' alt=''></div></div>";
          html +="<div class='col s2 m2 l2 alarom-col divr-border'><div class='date-start end-date'><a class='dropdown-button' data-activates='reminder"+ImageNo+"'><img src='"+base_url+"/asset/css/img/icons/alarmclock.png' alt=''><input type='hidden' class='legal-rm-"+ImageNo+"'></a><ul id='reminder"+ImageNo+"' class='dropdown-content legal-rmul-"+ImageNo+" border-radius-0'><li></li><li data-remainder='1' data-docno="+ImageNo+"><a href='#!' class='grey-text text-darken-1'>One week before</a></li><li data-remainder='2' data-docno="+ImageNo+"><a href='#!' class='grey-text text-darken-1'>Fortnight</a></li><li data-remainder='3' data-docno="+ImageNo+"><a href='#!' class='grey-text text-darken-1'>One month before</a></li><li data-remainder='4' data-docno="+ImageNo+"><a href='#!' class='grey-text text-darken-1'>Two months before</a></li></ul><select name='legal_remainder"+ImageNo+"' id='legal_remainder"+ImageNo+"' class='country-dropdown check-label'><option value='1'>One week before</option><option value='2'>Fortnight</option><option value='3'>One month before</option><option value='4'>Two months before</option></select></div></div>";
          html +="<div class='col s2 m2 l2 delete-col'><a href='javascript:void(0);' onclick='LegalRemoveImages(\"" + ImageNo + "_CClass\",\"" + fileUpload.files[j].name.toLowerCase() + "\")'> <img src='"+base_url+"/asset/images/delete.png' alt=''></a></div>";
          html +="</div>";
		  
          $("#Legaldocumentscover-sup").append(html);
		  $('#legal_remainder'+ImageNo).material_select();
			
              j = j + 1;

              ImageNo = ImageNo + 1;
          }

          newImageObjs.push(file);

          reader.readAsDataURL(file);
		
      }
  } else {
      alert("This browser does not support HTML5 FileReader.");
  }
});
function LegalRemoveImages(objclass, ImageName) {
  $.each(newImageObjs, function (e, element) {
      if ($(this)[0].name.toLowerCase().trim() == ImageName.trim()) {
          newImageObjs.pop(this);
      }
  });
  $("#Legaldocumentscover-sup").find("." + objclass + "").remove();
}


var newPurchaseObj = [];
var ImagePurchaseNo = 0;
$("#purchaseorderdocs").change(function () {

  var fileUpload = document.getElementById("purchaseorderdocs");

  if (typeof (FileReader) != "undefined") {
      for (var i = 0; i < fileUpload.files.length; i++) {
          var j = 0;
          var file = fileUpload.files[i];
          var NewFile = fileUpload.files[i];
			var filesize = this.files[0].size;
		  var kbfilesize = Math.round(filesize / 1000);
          var reader = new FileReader();
          reader.onload = function (e) {

          var html ="<div class='docubox " + ImagePurchaseNo + "_CClass'>";
          html +="<div class='col s4 m4 l4 divr-border'><div class='pdf-file'><div class='col l3 s3 m3'><img src='"+base_url+"/asset/images/pdf.png' alt='' class='pdf'></div><div class='col l9 s9 m9'><p class='uploadfilename'>"+fileUpload.files[j].name+"</p><span>File Size "+kbfilesize +" kb</span></div></div></div><div class='col s3 m3 l3 divr-border'><input type='text' placeholder='Purchase Order' class='no-border-field' name='purchase_order"+ImagePurchaseNo+"' id='purchase_order"+ImagePurchaseNo+"'></div><div class='col s1 m1 l1 divr-border'><div class='date-start setdate' data-position='top' data-delay='50' data-tooltip='Purchase Order Date'><input type='text' class='idatepicker hidendate' name='po_date"+ImagePurchaseNo+"' id='po_date"+ImagePurchaseNo+"'><img src='"+base_url+"/asset/css/img/icons/calendar-green.png' alt=''></div></div><div class='col s1 m1 l1 divr-border'><div class='date-start setdate' data-position='top' data-delay='50' data-tooltip='Start Date'><input type='text' class='idatepicker hidendate' name='po_startdate"+ImagePurchaseNo+"' id='po_startdate"+ImagePurchaseNo+"'><img src='"+base_url+"/asset/css/img/icons/calendar-green.png' alt=''></div></div><div class='col s1 m1 l1 divr-border'><div class='date-start setdate end-date' data-position='top' data-delay='50' data-tooltip='End Date'><input type='text' class='idatepicker hidendate' id='po_enddate"+ImagePurchaseNo+"' name='po_enddate"+ImagePurchaseNo+"'><img src='"+base_url+"/asset/css/img/icons/calendar-green.png' alt=''></div></div>";
          html +="<div class='col s1 m1 l1 alarom-col divr-border'><div class='date-start end-date'><a href='javascript:void(0);' class='dropdown-button reminderbtn' data-set='reminder"+ImagePurchaseNo+"' data-activates='reminder"+ImagePurchaseNo+"'><img src='"+base_url+"/asset/css/img/icons/alarmclock.png' alt=''><input type='hidden' class='legal-rm-"+ImagePurchaseNo+"' name=''></a><ul id='reminder"+ImagePurchaseNo+"' class='dropdown-content legal-rmul-"+ImagePurchaseNo+" border-radius-0'><li></li><li data-po_remainder='1' data-po_docno="+ImagePurchaseNo+"><a href='#!' class='grey-text text-darken-1'>One week before</a></li><li data-po_remainder='2' data-po_docno="+ImagePurchaseNo+"><a href='#!' class='grey-text text-darken-1'>Fortnight</a></li><li data-po_remainder='3' data-po_docno="+ImagePurchaseNo+"><a href='#!' class='grey-text text-darken-1'>One month before</a></li data-po_remainder='4' data-po_docno="+ImagePurchaseNo+"><li data-po_remainder='4' data-po_docno="+ImagePurchaseNo+"><a href='#!' class='grey-text text-darken-1'>Two months before</a></li></ul><select name='purchase_remainder"+ImagePurchaseNo+"' id='purchase_remainder"+ImagePurchaseNo+"' class='country-dropdown check-label'><option value='1'>One week before</option><option value='2'>Fortnight</option><option value='3'>One month before</option><option value='4'>Two months before</option></select></div></div>";
          html +="<div class='col s1 m1 l1 delete-col'><a href='javascript:void(0);' onclick='PurchaseRemoveImage(\"" + ImagePurchaseNo + "_CClass\",\"" + fileUpload.files[j].name.toLowerCase() + "\")'> <img src='"+base_url+"/asset/images/delete.png' alt=''></a></div>";
          html +="</div>";
          $("#purchaseorderdoccover-sup").append(html);
		  $('#purchase_remainder'+ImagePurchaseNo).material_select();
			
              j = j + 1;

              ImagePurchaseNo = ImagePurchaseNo + 1;
          }

          newPurchaseObj.push(file);

          reader.readAsDataURL(file);
      }
  } else {
      alert("This browser does not support HTML5 FileReader.");
  }
});

function PurchaseRemoveImage(objclass, ImageName) {

  $.each(newPurchaseObj, function (e, element) {
      if ($(this)[0].name.toLowerCase().trim() == ImageName.trim()) {
        newPurchaseObj.pop(this);
      }
  });

  $("#purchaseorderdoccover-sup").find("." + objclass + "").remove();
}

var newotherdocObjs = [];
var ImageotherdocNo = 0;
$("#otherdocs").change(function () {

  var fileUpload = document.getElementById("otherdocs");

  if (typeof (FileReader) != "undefined") {
      for (var i = 0; i < fileUpload.files.length; i++) {
           var j = 0;
          var file = fileUpload.files[i];
          var NewFile = fileUpload.files[i];
			var filesize = this.files[0].size;
		  var kbfilesize = Math.round(filesize / 1000);
          var reader = new FileReader();
          reader.onload = function (e) {
			  
			  var html ="<div class='docubox " + ImageotherdocNo + "_CClass'>";
          html +="<div class='col s6 m6 l6 divr-border'><div class='pdf-file'><div class='col l3 s3 m3'><img src='"+base_url+"/asset/images/pdf.png' alt='' class='pdf'></div><div class='col l9 s9 m9'><p class='uploadfilename'>"+fileUpload.files[j].name+"</p><span>File Size "+kbfilesize +" kb</span></div></div></div><div class='col s4 m4 l4 divr-border'><input type='text' placeholder='Document Name' class='no-border-field' name='otherdoc_name"+ImageotherdocNo+"' id='otherdoc_name"+ImageotherdocNo+"'></div><div class='col s2 m2 l2'></div>";
          html +="<div class='col s2 m2 l2 alarom-col'></div>";
          html +="<div class='col s2 m2 l2 delete-col'><a href='javascript:void(0);' onclick='otherRemoveImages(\"" + ImageotherdocNo + "_CClass\",\"" + fileUpload.files[j].name.toLowerCase() + "\")'> <img src='"+base_url+"/asset/images/delete.png' alt=''></a></div>";
          html +="</div>";
	
          $("#otherdoccover-sup").append(html);

              j = j + 1;

              ImageotherdocNo = ImageotherdocNo + 1;
          }

          newotherdocObjs.push(file);

          reader.readAsDataURL(file);
      }
  } else {
      alert("This browser does not support HTML5 FileReader.");
  }
});


function otherRemoveImages(objclass, ImageName) {

  $.each(newotherdocObjs, function (e, element) {
      if ($(this)[0].name.toLowerCase().trim() == ImageName.trim()) {
        newotherdocObjs.pop(this);
      }
  });

  $("#otherdoccover-sup").find("." + objclass + "").remove();
}

$(document).on('click' , '.edit_supplier_gst', function(){
	$('#edit_supp_gstmodal').modal('open');
	var gst_id = $(this).data('gst_id');
	var gst_no = $(this).data('gst_no');
	var location = $(this).data('location');
	var supplier_id = $(this).data('supplier_id');
	
	$('#edit_supp_gstmodal #gst_id').val(gst_id);
	$('#edit_supp_gstmodal #gst_supplier_id').val(supplier_id);
	$('#edit_supp_gstmodal #gst_edit_location').val(location);
	$('#edit_supp_gstmodal #gst_no_edit').val(gst_no);
});
$("#edit_supplier_gstin").submit(function(e){
				e.preventDefault();
			}).validate({
				rules:{

					gst_no_edit:{
						required:true,
						gstregex:true,
					},
					location:{
						required:true,
					},
				},
				messages:{

					gst_no_edit:{
						required:"GST Number is required",
						gstregex:"Enter GSTIN in a valid format",
					},
					gst_edit_location:{
						required:"Place of supply is required",
					},
				},
				submitHandler:function(form){
						var frm=$(form).serialize();
								$.ajax({
								url:base_url+'My_suppliers/edit_supplier_gst_info',
								type:"POST",
								data:{'csrf_test_name':csrf_hash,"frm":frm,},
								success:function(res){
									if(res!= false)
									{
										var data=JSON.parse(res);
										var html_1 ='';
										var final_html ='';
										if(data != '')
										{
											for(var i=0;i<data.length;i++){
											if(i == 0)
											   {
												html_1='<div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">GST No.</span><p>'+data[i].gst_no+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">PLACE OF SUPPLY</span><p>'+data[i].place_of_supply+'</p></div><div class="col l1 s1 m1 gstaction"><a href="#" class="remove_gst_model" data-location="'+data[i].place_of_supply+'" data-gst_no="'+data[i].gst_no+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a><a href="#" class="edit_supplier_gst" data-supplier_id="'+data[i].supplier_id+'" data-gst_id="'+data[i].id+'" data-location="'+data[i].place_of_supply+'" data-gst_no="'+data[i].gst_no+'"><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div>';
											   }
											   else
											   {
												 html_1='<div class="gstmore"><div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">GST No.</span><p>'+data[i].gst_no+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">PLACE OF SUPPLY</span><p>'+data[i].place_of_supply+'</p></div><div class="col l1 s1 m1 gstaction"><a href="#" class="remove_gst_model" data-location="'+data[i].place_of_supply+'" data-gst_no="'+data[i].gst_no+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a><a href="#" class="edit_supplier_gst" data-supplier_id="'+data[i].supplier_id+'" data-gst_id="'+data[i].id+'" data-location="'+data[i].place_of_supply+'" data-gst_no="'+data[i].gst_no+'"><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div></div>';
											  
											   }
												 final_html = final_html + html_1;
												 $("#show_gst_list").html(final_html);
												 $(".supp_tot_gst").html(data.length);
											}
											Materialize.toast('GSTIN info updated successfully', 2000,'green rounded');
										}
										else
										{
											//html_1='<div class="gst-boxs add-gst">Add GSTIN Data</div>';
											//$("#show_gst_list").html(html_1);
										}
									}
									else
									{
										Materialize.toast('Error while processing. Please try again', 2000,'red rounded');
									}
									$("#edit_supplier_gstin").find("input[type=text], textarea").val("");
									$("#edit_supp_gstmodal").modal('close');
									
							},
							
					});
				},
			});
			
$(document).on('click', '.edit_remove_supp_gst_data' ,function(){						
	var gst_id = $(this).data('gst_id');
	var supplier_id = $(this).data('supplier_id');

	$('#editpage_remove_supp_gst_data').modal('open');
	$('#editpage_remove_supp_gst_data #remove_supp_gst_id').val(gst_id);
	$('#editpage_remove_supp_gst_data #remove_supp_id').val(supplier_id);
});
	$('.edit_remove_supp_gst').on('click',function(){
						
		var gst_id = $('#remove_supp_gst_id').val();
		var supplier_id = $('#remove_supp_id').val();
		$.ajax({
			url:base_url+'My_suppliers/delete_supplier_gst_info',
			type:"post",
			data:{'csrf_test_name':csrf_hash,"gst_id":gst_id,"supplier_id":supplier_id},
			success:function(res){
					console.log(res);
					if(res!= false)
									{
										var data=JSON.parse(res);
										var html_1 ='';
										var final_html ='';
										if(data != '')
										{
											for(var i=0;i<data.length;i++){
											if(i == 0)
											   {
												html_1='<div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">GST No.</span><p>'+data[i].gst_no+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">PLACE OF SUPPLY</span><p>'+data[i].place_of_supply+'</p></div><div class="col l1 s1 m1 gstaction"><a href="#" class="remove_gst_model" data-location="'+data[i].place_of_supply+'" data-gst_no="'+data[i].gst_no+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a><a href="#" class="edit_supplier_gst" data-supplier_id="'+data[i].supplier_id+'" data-gst_id="'+data[i].id+'" data-location="'+data[i].place_of_supply+'" data-gst_no="'+data[i].gst_no+'"><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div>';
											   }
											   else
											   {
												 html_1='<div class="gstmore"><div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">GST No.</span><p>'+data[i].gst_no+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">PLACE OF SUPPLY</span><p>'+data[i].place_of_supply+'</p></div><div class="col l1 s1 m1 gstaction"><a href="#" class="remove_gst_model" data-location="'+data[i].place_of_supply+'" data-gst_no="'+data[i].gst_no+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a><a href="#" class="edit_supplier_gst" data-supplier_id="'+data[i].supplier_id+'" data-gst_id="'+data[i].id+'" data-location="'+data[i].place_of_supply+'" data-gst_no="'+data[i].gst_no+'"><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div></div>';
											  
											   }
												 final_html = final_html + html_1;
												 $("#show_gst_list").html(final_html);
												 $(".supp_tot_gst").html(data.length);
											}
											Materialize.toast('GSTIN info has been removed', 2000,'green rounded');
										}
										else
										{
											Materialize.toast('GSTIN info has been removed', 2000,'green rounded');
											//html_1='<div class="gst-boxs add-gst">Add GSTIN Data</div>';
											//$("#show_gst_list").html(html_1);
											$(".supp_tot_gst").html(0);
										}
									}
									else
									{
										//html_1='<div class="gst-boxs add-gst">Add GSTIN Data</div>';
										//$("#show_gst_list").html(html_1);
										$(".supp_tot_gst").html(0);
										Materialize.toast('Error while processing. Please try again', 2000,'red rounded');
									}
				},
			});										
	});
$(document).on('click', '.add_new_gst_info' ,function(){						
	var supplier_id = $(this).data('supplier_id');

	$('#supplier_gst_modal_edit').modal('open');
	$('#supplier_gst_modal_edit #add_for_supplier_id').val(supplier_id);
});
$("#add_supplier_gstin_edit").submit(function(e){
				e.preventDefault();
			}).validate({
				rules:{

					'gstin[]':{
						required:true,
						gstregex:true,
					},
					'location[]':{
						required:true,
					},
				},
				messages:{

					gstin:{
						required:"GST Number is required",
						gstregex:"Enter GSTIN in a valid format",
					},
					location:{
						required:"Place of supply is required",
					},
				},
				submitHandler:function(form){
						var frm=$(form).serialize();
								$.ajax({
								url:base_url+'My_suppliers/add_gst_info_2',
								type:"POST",
								data:{'csrf_test_name':csrf_hash,"frm":frm,},
								success:function(res){
										if(res!= false)
									{
										var data=JSON.parse(res);
										var html_1 ='';
										var final_html ='';
										if(data != '')
										{
											for(var i=0;i<data.length;i++){
											if(i == 0)
											   {
												html_1='<div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">GST No.</span><p>'+data[i].gst_no+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">PLACE OF SUPPLY</span><p>'+data[i].place_of_supply+'</p></div><div class="col l1 s1 m1 gstaction"><a href="#" class="remove_gst_model" data-location="'+data[i].place_of_supply+'" data-gst_no="'+data[i].gst_no+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a><a href="#" class="edit_supplier_gst" data-supplier_id="'+data[i].supplier_id+'" data-gst_id="'+data[i].id+'" data-location="'+data[i].place_of_supply+'" data-gst_no="'+data[i].gst_no+'"><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div>';
											   }
											   else
											   {
												 html_1='<div class="gstmore"><div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">GST No.</span><p>'+data[i].gst_no+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">PLACE OF SUPPLY</span><p>'+data[i].place_of_supply+'</p></div><div class="col l1 s1 m1 gstaction"><a href="#" class="remove_gst_model" data-location="'+data[i].place_of_supply+'" data-gst_no="'+data[i].gst_no+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a><a href="#" class="edit_supplier_gst" data-supplier_id="'+data[i].supplier_id+'" data-gst_id="'+data[i].id+'" data-location="'+data[i].place_of_supply+'" data-gst_no="'+data[i].gst_no+'"><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div></div>';
											  
											   }
												 final_html = final_html + html_1;
												 $("#show_gst_list").html(final_html);
												 $(".supp_tot_gst").html(data.length);
											}
											Materialize.toast('GSTIN information added successfully', 2000,'green rounded');
										}
										else
										{
											//html_1='<div class="gst-boxs add-gst">Add GSTIN Data</div>';
											//$("#show_gst_list").html(html_1);
										}
									}
									else
									{
										Materialize.toast('Error while processing. Please try again', 2000,'red rounded');
									}
									$("#add_supplier_gstin_edit").find("input[type=text], textarea").val("");
									$("#supplier_gst_modal_edit").modal('close');
							},
							
					});
				},
			});
$(document).on('click', '.contactmodal_edit' ,function(){						
	var supplier_id = $(this).data('supplier_id');
	$('#supplier_contactmodal_edit').modal('open');
	$('#supplier_contactmodal_edit #contact_supp_id').val(supplier_id);
});
$("#add_contact_person_edit").submit(function(e){
				e.preventDefault();
			}).validate({
				rules:{
					name:{
						required:true,
					},
					mobile:{
						required:true,
					},
					email:{
						required:true,
						email:true
					},
					password:{
						required:true,
						minlength:8,
						maxlength:12
					},
				},
				messages:{
					name:{
						required:"Name is required",
					},
					mobile:{
						required:"Mobile Number is required",
					},
					email:{
						required:"Email ID is required",
						email:'Enter valid Email ID',
					},
					password:{
						required:"Password is required",
						minlength:'Enter minimum 8 characters for password.',
						maxlength:"Maximum 12 character long password allowed.",
					},
				},
				submitHandler:function(form){
						var frm=$(form).serialize();
								$.ajax({
								url:base_url+'My_suppliers/add_new_contact_persons',
								type:"POST",
								data:{'csrf_test_name':csrf_hash,"frm":frm,},
								success:function(res){;
									if(res!= false)
									{
										var data=JSON.parse(res);
										var html_person ='';
										var final_html_1 ='';
										if(data != '')
										{
											for(var i=0;i<data.length;i++){
												var admin = '';
												if(data[i].is_admin == '1')
												{
													admin = 'checked';	
												} else {
													admin = '';	
												}
											if(i == 0)
											{
												html_person='<div class="row"><div class="contact-person-box"><div class="col l1 s1 m1"><input type="radio" id="is_admin_'+data[i].id+'" name="is_admin_'+data[i].id+'" '+admin+' ><label class="fill-green-radio" for="contact-person-1"></label></div><div class="col l10 s10 m10 content-area"><div class="col l3 s3 m3"><div class="gradient-wrapper"><div class="maincircle">100%</div></div></div><div class="col l9 s9 m9"><p class="name"><strong>'+data[i].name+'</strong></p><p class="email">'+data[i].email+'</p></div></div><div class="col l1 s1 m1"><a href="#" class="remove_supp_contact_person_edit" data-name="'+data[i].name+'" data-email="'+data[i].email+'"><i class="material-icons">close</i></a><a href="#" class="edit_supplier_contact" data-supplier_id="'+data[i].supplier_id+'" data-contact_id="'+data[i].id+'" data-name="'+data[i].name+'" data-mobile="'+data[i].mobile+'" data-email="'+data[i].email+'" data-birthday="'+data[i].birthday+'" data-birthday_reminder="'+data[i].birthday_reminder+'" data-anniversary="'+data[i].anniversary+'" data-anniversary_reminder="'+data[i].anniversary_reminder+'"><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div>';
												
											   }
											   else if(i == 1)
											   {
												  html_person='<div class="row"><div class="contact-person-box"><div class="col l1 s1 m1"><input type="radio" id="is_admin_'+data[i].id+'" name="is_admin_'+data[i].id+'" '+admin+' ><label class="fill-green-radio" for="contact-person'+(i+1)+'"></label></div><div class="col l10 s10 m10 content-area"><div class="col l3 s3 m3"><div class="gradient-wrapper"><div class="maincircle">100%</div></div></div><div class="col l9 s9 m9"><p class="name"><strong>'+data[i].name+'</strong></p><p class="email">'+data[i].email+'</p></div></div><div class="col l1 s1 m1"><a href="#" class="remove_supp_contact_person_edit" data-name="'+data[i].name+'" data-email="'+data[i].email+'"><i class="material-icons">close</i></a><a href="#" class="edit_supplier_contact" data-supplier_id="'+data[i].supplier_id+'" data-contact_id="'+data[i].id+'" data-name="'+data[i].name+'" data-mobile="'+data[i].mobile+'" data-email="'+data[i].email+'" data-birthday="'+data[i].birthday+'" data-birthday_reminder="'+data[i].birthday_reminder+'" data-anniversary="'+data[i].anniversary+'" data-anniversary_reminder="'+data[i].anniversary_reminder+'"><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div>'; 
											   }
											   else
											   {
												  html_person='<div class="contactmore"><div class="row"><div class="contact-person-box"><div class="col l1 s1 m1"><input type="radio" id="is_admin_'+data[i].id+'" name="is_admin_'+data[i].id+'" '+admin+'><label class="fill-green-radio" for="contact-person'+(i+1)+'"></label></div><div class="col l10 s10 m10 content-area"><div class="col l3 s3 m3"><div class="gradient-wrapper"><div class="maincircle">100%</div></div></div><div class="col l9 s9 m9"><p class="name"><strong>'+data[i].name+'</strong></p><p class="email">'+data[i].email+'</p></div></div><div class="col l1 s1 m1"><a href="#" class="remove_supp_contact_person_edit" data-name="'+data[i].name+'" data-email="'+data[i].email+'"><i class="material-icons">close</i></a><a href="#" class="edit_supplier_contact" data-supplier_id="'+data[i].supplier_id+'" data-contact_id="'+data[i].id+'" data-name="'+data[i].name+'" data-mobile="'+data[i].mobile+'" data-email="'+data[i].email+'" data-birthday="'+data[i].birthday+'" data-birthday_reminder="'+data[i].birthday_reminder+'" data-anniversary="'+data[i].anniversary+'" data-anniversary_reminder="'+data[i].anniversary_reminder+'"><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div></div>'; 											  
											   }
												 final_html_1 = final_html_1 + html_person;
												 $("#show_contact_list").html(final_html_1);
												 $(".tot_contact_person").html(data.length);
											}											
										}
										else
										{
											html_person='<div class="gst-boxs add-gst">Add Contact Person Data</div>';
											$("#show_contact_list").html(html_person);
											$(".tot_contact_person").html(0);
										}
									}
									else
									{
										Materialize.toast('Error while processing. Please try again', 2000,'red rounded');
									}
									$("#add_contact_person_edit").find("input[type=text],input[type=password], textarea").val("");
									$("#supplier_contactmodal_edit").modal('close');
							},
							
					});
				},
			});
			
			$(document).on('click', '.edit_supplier_contact', function(){				
				var id = $(this).data('id');
				var name = $(this).data('name');
				var supplier_id = $(this).data('supplier_id');
				var mobile = $(this).data('mobile');
				var email = $(this).data('email');
				var birthday = $(this).data('birthday');
				var birthday_remainder = $(this).data('birthday_remainder');
				var anniversary = $(this).data('anniversary');
				var anniversary_reminder = $(this).data('anniversary_reminder');
				var is_admin = $(this).data('is_admin');
				var password = $(this).data('password');
				
				$('#supplier_contact_edit_details').modal('open');
				$('#supplier_contact_edit_details #edit_contact_id').val(id);
				$('#supplier_contact_edit_details #edit_name').val(name);
				$('#supplier_contact_edit_details #edit_mobile').val(mobile);
				$('#supplier_contact_edit_details #edit_email').val(email);
				$('#supplier_contact_edit_details #edit_old_password').val(password);
				$('#supplier_contact_edit_details #edit_birthday').val(birthday);
				$('#supplier_contact_edit_details #edit_birthday_reminder option[value='+birthday_remainder+']').attr('selected','selected');
				$('#supplier_contact_edit_details #edit_anniversary').val(anniversary);
				$('#supplier_contact_edit_details #edit_anniversary_reminder option[value='+anniversary_reminder+']').attr('selected','selected');
				if(is_admin == 1){
					$('#supplier_contact_edit_details #primary').prop('checked', true);
				}
				$('#supplier_contact_edit_details #remove_cust_id').val(is_admin);
				$('#supplier_contact_edit_details #edit_contact_supp_id').val(supplier_id);
				//$('#supplier_contact_edit_details #edit_password').val(password);
			});
			
			$("#edit_contact_person_details").submit(function(e){
				e.preventDefault();
			}).validate({
				rules:{
					edit_name:{
						required:true,
					},
					edit_mobile:{
						required:true,
					},
					edit_email:{
						required:true,
						email:true
					},
				},
				messages:{
					edit_name:{
						required:"Name is required",
					},
					edit_mobile:{
						required:"Mobile Number is required",
					},
					edit_email:{
						required:"Email ID is required",
						email:'Enter valid Email ID',
					},
				},
				submitHandler:function(form){
						var frm=$(form).serialize();
								$.ajax({
								url:base_url+'My_suppliers/edit_contact_person',
								type:"POST",
								data:{'csrf_test_name':csrf_hash,"frm":frm,},
								success:function(res){
									if(res!= false)
									{
										var data=JSON.parse(res);
										var html_person ='';
										var final_html_1 ='';
										if(data != '')
										{
											console.log(data);
											for(var i=0;i<data.length;i++){
												var admin = '';
												if(data[i].is_admin == '1')
												{
													admin = 'checked';	
												} else {
													admin = '';	
												}
												
											if(i == 0)
											{
												html_person='<div class="row"><div class="contact-person-box"><div class="col l1 s1 m1"><input type="radio" id="is_admin_'+data[i].id+'" name="is_admin_'+data[i].id+'" '+admin+'><label class="fill-green-radio" for="contact-person-1"></label></div><div class="col l10 s10 m10 content-area"><div class="col l3 s3 m3"><div class="gradient-wrapper"><div class="maincircle">100%</div></div></div><div class="col l9 s9 m9"><p class="name"><strong>'+data[i].name+'</strong></p><p class="email">'+data[i].email+'</p></div></div><div class="col l1 s1 m1"><a href="#" class="remove_supp_contact_person_edit" data-name="'+data[i].name+'" data-email="'+data[i].email+'"><i class="material-icons">close</i></a><a href="#" class="edit_supplier_contact" data-supplier_id="'+data[i].supplier_id+'" data-contact_id="'+data[i].id+'" data-name="'+data[i].name+'" data-mobile="'+data[i].mobile+'" data-email="'+data[i].email+'" data-birthday="'+data[i].birthday+'" data-birthday_reminder="'+data[i].birthday_reminder+'" data-anniversary="'+data[i].anniversary+'" data-anniversary_reminder="'+data[i].anniversary_reminder+'"><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div>';
												
											   }
											   else if(i == 1)
											   {
												  html_person='<div class="row"><div class="contact-person-box"><div class="col l1 s1 m1"><input type="radio" id="is_admin_'+data[i].id+'" name="is_admin_'+data[i].id+'" '+admin+'><label class="fill-green-radio" for="contact-person'+(i+1)+'"></label></div><div class="col l10 s10 m10 content-area"><div class="col l3 s3 m3"><div class="gradient-wrapper"><div class="maincircle">100%</div></div></div><div class="col l9 s9 m9"><p class="name"><strong>'+data[i].name+'</strong></p><p class="email">'+data[i].email+'</p></div></div><div class="col l1 s1 m1"><a href="#" class="remove_supp_contact_person_edit" data-name="'+data[i].name+'" data-email="'+data[i].email+'"><i class="material-icons">close</i></a><a href="#" class="edit_supplier_contact" data-supplier_id="'+data[i].supplier_id+'" data-contact_id="'+data[i].id+'" data-name="'+data[i].name+'" data-mobile="'+data[i].mobile+'" data-email="'+data[i].email+'" data-birthday="'+data[i].birthday+'" data-birthday_reminder="'+data[i].birthday_reminder+'" data-anniversary="'+data[i].anniversary+'" data-anniversary_reminder="'+data[i].anniversary_reminder+'"><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div>'; 
											   }
											   else
											   {
												  html_person='<div class="contactmore"><div class="row"><div class="contact-person-box"><div class="col l1 s1 m1"><input type="radio" id="is_admin_'+data[i].id+'" name="is_admin_'+data[i].id+'" '+admin+'><label class="fill-green-radio" for="contact-person'+(i+1)+'"></label></div><div class="col l10 s10 m10 content-area"><div class="col l3 s3 m3"><div class="gradient-wrapper"><div class="maincircle">100%</div></div></div><div class="col l9 s9 m9"><p class="name"><strong>'+data[i].name+'</strong></p><p class="email">'+data[i].email+'</p></div></div><div class="col l1 s1 m1"><a href="#" class="remove_supp_contact_person_edit" data-name="'+data[i].name+'" data-email="'+data[i].email+'"><i class="material-icons">close</i></a><a href="#" class="edit_supplier_contact" data-supplier_id="'+data[i].supplier_id+'" data-contact_id="'+data[i].id+'" data-name="'+data[i].name+'" data-mobile="'+data[i].mobile+'" data-email="'+data[i].email+'" data-birthday="'+data[i].birthday+'" data-birthday_reminder="'+data[i].birthday_reminder+'" data-anniversary="'+data[i].anniversary+'" data-anniversary_reminder="'+data[i].anniversary_reminder+'"><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div></div>'; 											  
											   }
												 final_html_1 = final_html_1 + html_person;
												 $("#show_contact_list").html(final_html_1);
												 $(".tot_contact_person").html(data.length);
											}
											Materialize.toast('Contact Info updated successfully!!', 2000,'green rounded');											
										}
										else
										{
											html_person='<div class="gst-boxs add-gst">Add Contact Person Data</div>';
											$("#show_contact_list").html(html_person);
											$(".tot_contact_person").html(0);
										}
									}
									else
									{
										Materialize.toast('Error while processing. Please try again', 2000,'red rounded');
									}
									$("#edit_contact_person_details").find("input[type=text],input[type=password], textarea").val("");
									$("#supplier_contact_edit_details").modal('close');
							},
							
					});
				},
			});
$(document).on('click', '.remove_supp_contact_person_edit', function(){
	var id = $(this).data('id');
	var supplier_id = $(this).data('supplier_id');
	
	$('#remove_supplier_contact').modal('open');
	$('#remove_supplier_contact #remove_contact_id').val(id);
	$('#remove_supplier_contact #remove_contact_supp_id').val(supplier_id);
});
$('.remove_supp_person_edit').on('click',function(){
						
			var id = $('#remove_contact_id').val();
			var supplier_id = $('#remove_contact_supp_id').val();

			$.ajax({

				url:base_url+'My_suppliers/delete_contact_info',

				type:"post",

				data:{'csrf_test_name':csrf_hash,"id":id,"supplier_id":supplier_id},

				success:function(res){
						if(res!= false)
						{
							var data=JSON.parse(res)
							var html_person ='';
							var final_html_1 ='';
							if(data != '')
							{
								for(var i=0;i<data.length;i++){
									var admin = '';
									if(data[i].is_admin == '1')
									{
										admin = 'checked';	
									} else {
										admin = '';	
									}
									
								if(i == 0)
								{
									html_person='<div class="row"><div class="contact-person-box"><div class="col l1 s1 m1"><input type="radio" id="is_admin_'+data[i].id+'" name="is_admin_'+data[i].id+'" '+admin+'><label class="fill-green-radio" for="contact-person-1"></label></div><div class="col l10 s10 m10 content-area"><div class="col l3 s3 m3"><div class="gradient-wrapper"><div class="maincircle">100%</div></div></div><div class="col l9 s9 m9"><p class="name"><strong>'+data[i].name+'</strong></p><p class="email">'+data[i].email+'</p></div></div><div class="col l1 s1 m1"><a href="#" class="remove_supp_contact_person_edit" data-name="'+data[i].name+'" data-email="'+data[i].email+'"><i class="material-icons">close</i></a><a href="#" class="edit_supplier_contact" data-supplier_id="'+data[i].supplier_id+'" data-contact_id="'+data[i].id+'" data-name="'+data[i].name+'" data-mobile="'+data[i].mobile+'" data-email="'+data[i].email+'" data-birthday="'+data[i].birthday+'" data-birthday_reminder="'+data[i].birthday_reminder+'" data-anniversary="'+data[i].anniversary+'" data-anniversary_reminder="'+data[i].anniversary_reminder+'"><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div>';
									
								   }
								   else if(i == 1)
								   {
									  html_person='<div class="row"><div class="contact-person-box"><div class="col l1 s1 m1"><input type="radio" id="is_admin_'+data[i].id+'" name="is_admin_'+data[i].id+'" '+admin+'><label class="fill-green-radio" for="contact-person'+(i+1)+'"></label></div><div class="col l10 s10 m10 content-area"><div class="col l3 s3 m3"><div class="gradient-wrapper"><div class="maincircle">100%</div></div></div><div class="col l9 s9 m9"><p class="name"><strong>'+data[i].name+'</strong></p><p class="email">'+data[i].email+'</p></div></div><div class="col l1 s1 m1"><a href="#" class="remove_supp_contact_person_edit" data-name="'+data[i].name+'" data-email="'+data[i].email+'"><i class="material-icons">close</i></a><a href="#" class="edit_supplier_contact" data-supplier_id="'+data[i].supplier_id+'" data-contact_id="'+data[i].id+'" data-name="'+data[i].name+'" data-mobile="'+data[i].mobile+'" data-email="'+data[i].email+'" data-birthday="'+data[i].birthday+'" data-birthday_reminder="'+data[i].birthday_reminder+'" data-anniversary="'+data[i].anniversary+'" data-anniversary_reminder="'+data[i].anniversary_reminder+'"><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div>';  
									   
								   }
								   else
								   {
									   html_person='<div class="contactmore"><div class="row"><div class="contact-person-box"><div class="col l1 s1 m1"><input type="radio" id="is_admin_'+data[i].id+'" name="is_admin_'+data[i].id+'" '+admin+'><label class="fill-green-radio" for="contact-person'+(i+1)+'"></label></div><div class="col l10 s10 m10 content-area"><div class="col l3 s3 m3"><div class="gradient-wrapper"><div class="maincircle">100%</div></div></div><div class="col l9 s9 m9"><p class="name"><strong>'+data[i].name+'</strong></p><p class="email">'+data[i].email+'</p></div></div><div class="col l1 s1 m1"><a href="#" class="remove_supp_contact_person_edit" data-name="'+data[i].name+'" data-email="'+data[i].email+'"><i class="material-icons">close</i></a><a href="#" class="edit_supplier_contact" data-supplier_id="'+data[i].supplier_id+'" data-contact_id="'+data[i].id+'" data-name="'+data[i].name+'" data-mobile="'+data[i].mobile+'" data-email="'+data[i].email+'" data-birthday="'+data[i].birthday+'" data-birthday_reminder="'+data[i].birthday_reminder+'" data-anniversary="'+data[i].anniversary+'" data-anniversary_reminder="'+data[i].anniversary_reminder+'"><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div></div>';
								  
								   }
									  final_html_1 = final_html_1 + html_person;
									 $("#show_contact_list").html(final_html_1);
									 $(".tot_contact_person").html(data.length);
								}
								Materialize.toast('Contact Detail has been successfully removed!', 2000,'green rounded');
							}
							else
							{
								html_person='<div class="gst-boxs add-gst">Add Contact Person Data</div>';
								$("#show_contact_list").html(html_person);
								$(".tot_contact_person").html(0);
							}
						}
						else
						{
							html_person='<div class="gst-boxs add-gst">Add Contact Person Data</div>';
							$("#show_contact_list").html(html_person);
							$(".tot_contact_person").html(0);
							Materialize.toast('Error while processing!', 2000,'red rounded');
						}
					},

				});										
		});
$(document).on('click', '.add_supp_bank_info' ,function(){						
	var supplier_id = $(this).data('supplier_id');
	$('#add-bank-edit').modal('open');
	$('#add-bank-edit #bank_details_for_supplier').val(supplier_id);
});

$("#add_supp_bank_details_edit").submit(function(e){
				e.preventDefault();
			}).validate({
				rules:{
					supp_bank_name:{
						required:true,
					},
					supp_bank_branch_name:{
						required:true,
					},
					supp_bank_account_no:{
						required:true,
					},
				},
				messages:{
					supp_bank_name:{
						required:"Bank Name is required",
					},
					supp_bank_branch_name:{
						required:"Bank Branch is required",
					},
					supp_bank_account_no:{
						required:"Bank Account Number is required",
					},
				},
				submitHandler:function(form){
						var frm=$(form).serialize();
								$.ajax({
								url:base_url+'My_suppliers/add_new_bank_info',
								type:"POST",
								data:{'csrf_test_name':csrf_hash,"frm":frm,},
								success:function(res){
									//console.log(res);
									if(res!= false)
									{
										var data=JSON.parse(res);
										var html_1 ='';
										var final_html ='';
										if(data != '')
										{
											console.log(data);
											for(var i=0;i<data.length;i++){
											if(i == 0)
											   {
												html_1='<div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">BANK ACCOUNT NUMBER</span><p>'+data[i].bank_account_no+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">BANK NAME</span><p>'+data[i].bank_name+'</p></div><div class="col l1 s1 m1 gstaction"><a href="#" class="edit_remove_supp_bank_data" data-account_no="'+data[i].bank_account_no+'" data-bank="'+data[i].bank_name+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a><a href="#" class="edit_supp_bank_data" data-supplier_id="'+data[i].supplier_id+'" data-id="'+data[i].id+'" data-acc_no="'+data[i].bank_account_no+'" data-bank_name="'+data[i].bank_name+'" data-branch_name="'+data[i].bank_branch_name+'" data-acc_type="'+data[i].bank_acc_type+'" data-ifsc_code="'+data[i].bank_ifsc_code+'"><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div>';
											   }
											   else
											   {
												 html_1='<div class="gstmore"><div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">BANK ACCOUNT NUMBER</span><p>'+data[i].bank_account_no+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">BANK NAME</span><p>'+data[i].bank_name+'</p></div><div class="col l1 s1 m1 gstaction"><a href="#" class="edit_remove_supp_bank_data" data-account_no="'+data[i].bank_account_no+'" data-bank="'+data[i].bank_name+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a><a href="#" class="edit_supp_bank_data" data-supplier_id="'+data[i].supplier_id+'" data-id="'+data[i].id+'" data-acc_no="'+data[i].bank_account_no+'" data-bank_name="'+data[i].bank_name+'" data-branch_name="'+data[i].bank_branch_name+'" data-acc_type="'+data[i].bank_acc_type+'" data-ifsc_code="'+data[i].bank_ifsc_code+'"><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div></div>';
											  
											   }
												 final_html = final_html + html_1;
												 $("#show_bank_details").html(final_html);
												 $(".bank_info").html(data.length);
											}
											Materialize.toast('Bank Details has been inserted successfully!.', 2000,'green rounded');
										}
										else
										{
											html_1='<div class="gst-boxs add-gst">Add Bank Data</div>';
											$("#show_bank_details").html(html_1);
										}
									}
									else
									{
										Materialize.toast('Error while processing. Please try again', 2000,'red rounded');
									}
									$("#add_supp_bank_details_edit").find("input[type=text],textarea").val("");
									$('#add-bank-edit').modal('close');
							},
							
					});
				},
			});
$(document).on('click', '.edit_remove_supp_bank_data' ,function(){						
	var supplier_id = $(this).data('supplier_id');
	var id = $(this).data('id');
	$('#remove_supplier_bank_details').modal('open');
	$('#remove_supplier_bank_details #remove_bank_info_id').val(id);
	$('#remove_supplier_bank_details #remove_bank_supp_id').val(supplier_id);
});
$('.remove_supp_bank_edit').on('click',function(){
						
			var id = $('#remove_bank_info_id').val();
			var supplier_id = $('#remove_bank_supp_id').val();

			$.ajax({

				url:base_url+'My_suppliers/delete_bank_info',

				type:"post",

				data:{'csrf_test_name':csrf_hash,"id":id,"supplier_id":supplier_id},

				success:function(res){
						if(res!= false)
						{
							var data=JSON.parse(res);
							var html_1 ='';
							var final_html ='';
							if(data != '')
							{
								console.log(data);
								for(var i=0;i<data.length;i++){
								if(i == 0)
								   {
									html_1='<div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
								   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">BANK ACCOUNT NUMBER</span><p>'+data[i].bank_account_no+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">BANK NAME</span><p>'+data[i].bank_name+'</p></div><div class="col l1 s1 m1 gstaction"><a href="#" class="edit_remove_supp_bank_data" data-account_no="'+data[i].bank_account_no+'" data-bank="'+data[i].bank_name+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a><a href="#" class="edit_supp_bank_data" data-supplier_id="'+data[i].supplier_id+'" data-id="'+data[i].id+'" data-acc_no="'+data[i].bank_account_no+'" data-bank_name="'+data[i].bank_name+'" data-branch_name="'+data[i].bank_branch_name+'" data-acc_type="'+data[i].bank_acc_type+'" data-ifsc_code="'+data[i].bank_ifsc_code+'"><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div>';
								   }
								   else
								   {
									 html_1='<div class="gstmore"><div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
								   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">BANK ACCOUNT NUMBER</span><p>'+data[i].bank_account_no+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">BANK NAME</span><p>'+data[i].bank_name+'</p></div><div class="col l1 s1 m1 gstaction"><a href="#" class="edit_remove_supp_bank_data" data-account_no="'+data[i].bank_account_no+'" data-bank="'+data[i].bank_name+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a><a href="#" class="edit_supp_bank_data" data-supplier_id="'+data[i].supplier_id+'" data-id="'+data[i].id+'" data-acc_no="'+data[i].bank_account_no+'" data-bank_name="'+data[i].bank_name+'" data-branch_name="'+data[i].bank_branch_name+'" data-acc_type="'+data[i].bank_acc_type+'" data-ifsc_code="'+data[i].bank_ifsc_code+'"><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div></div>';
								  
								   }
									 final_html = final_html + html_1;
									 $("#show_bank_details").html(final_html);
									 $(".bank_info").html(data.length);
								}
								Materialize.toast('Bank details removed', 2000,'green rounded');
							}
							else
							{
								html_1='<div class="gst-boxs add-gst">Add Bank Data</div>';
								$("#show_bank_details").html(html_1);
							}
						}
						else
						{
							html_person='<div class="gst-boxs add-gst">Add Bank Data</div>';
							$("#show_bank_details").html(html_person);
							$(".bank_info").html(0);
							Materialize.toast('Error while processing!', 2000,'red rounded');
						}
						$('#remove_supplier_bank_details').modal('close');
					},

				});										
		});
$(document).on('click', '.edit_supp_bank_data', function(){
	var bank_name = $(this).data('bank_name');
	var branch_name = $(this).data('branch_name');
	var acc_no = $(this).data('acc_no');
	var ifsc_code = $(this).data('ifsc_code');
	var bank_acc_type = $(this).data('acc_type');
	var supplier_id = $(this).data('supplier_id');
	
	$('#edit_supp_bank_info').modal('open');
	$('#edit_supp_bank_info #edit_bank_account_no').val(acc_no);
	$('#edit_supp_bank_info #edit_supp_bank_name').val(bank_name);
	$('#edit_supp_bank_info #edit_supp_bank_branch_name').val(branch_name);
	$('#edit_supp_bank_info #edit_bank_ifsc_code').val(ifsc_code);
	$('#edit_supp_bank_info #edit_bank_account_type option[value="'+bank_acc_type+'"]').attr('selected','selected');
	$('#edit_supp_bank_info #edit_bank_details_for_supp').val(supplier_id);
	$('#edit_supp_bank_info #edit_bank_account_type').material_select();
});		
		
$("#supp_bank_details_edit").submit(function(e){
				e.preventDefault();
			}).validate({
				rules:{
					edit_bank_account_no:{
						required:true,
					},
					edit_supp_bank_name:{
						required:true,
					},
					edit_supp_bank_branch_name:{
						required:true,
					},
				},
				messages:{
					edit_bank_account_no:{
						required:"Bank Account No. is required",
					},
					edit_supp_bank_name:{
						required:"Bank Name is required",
					},
					edit_supp_bank_branch_name:{
						required:"Bank Branch is required",
					},
				},
				submitHandler:function(form){
						var frm=$(form).serialize();
								$.ajax({
								url:base_url+'My_suppliers/edit_supplier_bank_info',
								type:"POST",
								data:{'csrf_test_name':csrf_hash,"frm":frm,},
								success:function(res){
									console.log(res);
										if(res!= false)
										{
											var data=JSON.parse(res);
											var html_1 ='';
											var final_html ='';
											if(data != '')
											{
												console.log(data);
												for(var i=0;i<data.length;i++){
												if(i == 0)
												   {
													html_1='<div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
												   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">BANK ACCOUNT NUMBER</span><p>'+data[i].bank_account_no+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">BANK NAME</span><p>'+data[i].bank_name+'</p></div><div class="col l1 s1 m1 gstaction"><a href="#" class="edit_remove_supp_bank_data" data-account_no="'+data[i].bank_account_no+'" data-bank="'+data[i].bank_name+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a><a href="#" class="edit_supp_bank_data" data-supplier_id="'+data[i].supplier_id+'" data-id="'+data[i].id+'" data-acc_no="'+data[i].bank_account_no+'" data-bank_name="'+data[i].bank_name+'" data-branch_name="'+data[i].bank_branch_name+'" data-acc_type="'+data[i].bank_acc_type+'" data-ifsc_code="'+data[i].bank_ifsc_code+'"><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div>';
												   }
												   else
												   {
													 html_1='<div class="gstmore"><div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
												   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">BANK ACCOUNT NUMBER</span><p>'+data[i].bank_account_no+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">BANK NAME</span><p>'+data[i].bank_name+'</p></div><div class="col l1 s1 m1 gstaction"><a href="#" class="edit_remove_supp_bank_data" data-account_no="'+data[i].bank_account_no+'" data-bank="'+data[i].bank_name+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a><a href="#" class="edit_supp_bank_data" data-supplier_id="'+data[i].supplier_id+'" data-id="'+data[i].id+'" data-acc_no="'+data[i].bank_account_no+'" data-bank_name="'+data[i].bank_name+'" data-branch_name="'+data[i].bank_branch_name+'" data-acc_type="'+data[i].bank_acc_type+'" data-ifsc_code="'+data[i].bank_ifsc_code+'"><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div></div>';
												  
												   }
													 final_html = final_html + html_1;
													 $("#show_bank_details").html(final_html);
													 $(".bank_info").html(data.length);
												}
												Materialize.toast('Bank Details has been updated successfully!.', 2000,'green rounded');
											}
											else
											{
												html_1='<div class="gst-boxs add-gst">Add Bank Data</div>';
												$("#show_bank_details").html(html_1);
											}
										}
										else
										{
											Materialize.toast('Error while processing. Please try again', 2000,'red rounded');
										}
										$("#supp_bank_details_edit").find("input[type=text],textarea").val("");
										$('#edit_supp_bank_info').modal('close');									
									},
					});
				},
			});
		
			$(document).ready( function () {
				$(document).on('click', '.delete_supp_legal_doc',function(){
						var legal_id = $(this).data('legal_id');
					
						$('#remove_supp_legal_doc').modal('open');
						$('#remove_supp_legal_doc #supp_legal_doc_id').val(legal_id);
					});
					$('.delete_this_doc').on('click',function(){
	
					var supp_legal_doc_id = $('#supp_legal_doc_id').val();
					$.ajax({
						url:base_url+'My_suppliers/remove_legal_document',
						type:"POST",
						data:{'csrf_test_name':csrf_hash,"supp_legal_doc_id":supp_legal_doc_id,},
						success:function(res){
								location.reload();
							},
						});										
					});
			
			$(document).on('click', '.delete_supp_po_doc' ,function(){
				var po_id = $(this).data('po_id');
				
				$('#remove_supp_po_doc').modal('open');
				$('#remove_supp_po_doc #supp_po_doc_id').val(po_id);
			});
			
			$('.delete_this_po_doc').on('click',function(){

				var supp_po_doc_id = $('#supp_po_doc_id').val();
				$.ajax({
					url:base_url+'My_suppliers/remove_po_document',
					type:"POST",
					data:{'csrf_test_name':csrf_hash,"supp_po_doc_id":supp_po_doc_id,},
					success:function(res){
							location.reload();
						},
					});										
			});
			
			$(document).on('click', '.delete_supp_other_doc' ,function(){
				var other_id = $(this).data('other_id');
				
				$('#remove_supp_other_doc').modal('open');
				$('#remove_supp_other_doc #supp_other_doc_id').val(other_id);
			});
			
				$('.delete_this_other_doc').on('click',function(){
	
					var supp_other_doc_id = $('#supp_other_doc_id').val();
					$.ajax({
						url:base_url+'My_suppliers/remove_other_document',
						type:"POST",
						data:{'csrf_test_name':csrf_hash,"supp_other_doc_id":supp_other_doc_id,},
						success:function(res){
								location.reload();
							},
						});										
				});
			});