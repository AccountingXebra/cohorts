/*var th = ['', 'thousand', 'million', 'billion', 'trillion'];

var dg = ['zero', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine'];
var tn = ['ten', 'eleven', 'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen'];
var tw = ['twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety'];
function toWords(s) {
    s = s.toString();
    s = s.replace(/[\, ]/g, '');
    if (s != parseFloat(s)) return 'not a number';
    var x = s.indexOf('.');
    if (x == -1) x = s.length;
    if (x > 15) return 'too big';
    var n = s.split('');
    var str = '';
    var sk = 0;
    for (var i = 0; i < x; i++) {
        if ((x - i) % 3 == 2) {
            if (n[i] == '1') {
                str += tn[Number(n[i + 1])] + ' ';
                i++;
                sk = 1;
            } else if (n[i] != 0) {
                str += tw[n[i] - 2] + ' ';
                sk = 1;
            }
        } else if (n[i] != 0) {
            str += dg[n[i]] + ' ';
            if ((x - i) % 3 == 0) str += 'hundred ';
            sk = 1;
        }
        if ((x - i) % 3 == 1) {
            if (sk) str += th[(x - i - 1) / 3] + ' ';
            sk = 0;
        }
    }
    if (x != s.length) {
        var y = s.length;
        str += 'point ';
        for (var i = x + 1; i < y; i++) str += dg[n[i]] + ' ';
    }
    return str.replace(/\s+/g, ' ');
}*/
function add_commas(number){

	var x=number;
x=x.toString();
var afterPoint = '';
if(x.indexOf('.') > 0)
   afterPoint = x.substring(x.indexOf('.'),x.length);
x = Math.floor(x);
x=x.toString();
var lastThree = x.substring(x.length-3);
var otherNumbers = x.substring(0,x.length-3);
if(otherNumbers != '')
    lastThree = ',' + lastThree;
var res = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree + afterPoint;
    return res;
}

function toWords(amount,currency) {
    var words = new Array();
    words[0] = '';
    words[1] = 'One';
    words[2] = 'Two';
    words[3] = 'Three';
    words[4] = 'Four';
    words[5] = 'Five';
    words[6] = 'Six';
    words[7] = 'Seven';
    words[8] = 'Eight';
    words[9] = 'Nine';
    words[10] = 'Ten';
    words[11] = 'Eleven';
    words[12] = 'Twelve';
    words[13] = 'Thirteen';
    words[14] = 'Fourteen';
    words[15] = 'Fifteen';
    words[16] = 'Sixteen';
    words[17] = 'Seventeen';
    words[18] = 'Eighteen';
    words[19] = 'Nineteen';
    words[20] = 'Twenty';
    words[30] = 'Thirty';
    words[40] = 'Forty';
    words[50] = 'Fifty';
    words[60] = 'Sixty';
    words[70] = 'Seventy';
    words[80] = 'Eighty';
    words[90] = 'Ninety';
    
    amount = amount.toString();

    var atemp = amount.split(".");
    var number = atemp[0].split(",").join("");
    var n_length = number.length;
     var d_length = atemp.length;
    var words_string = "";
   
    if (n_length <= 9) {
        var n_array = new Array(0, 0, 0, 0, 0, 0, 0, 0, 0);
        var received_n_array = new Array();
        for (var i = 0; i < n_length; i++) {
            received_n_array[i] = number.substr(i, 1);
        }
        for (var i = 9 - n_length, j = 0; i < 9; i++, j++) {
            n_array[i] = received_n_array[j];
        }
        for (var i = 0, j = 1; i < 9; i++, j++) {
            if (i == 0 || i == 2 || i == 4 || i == 7) {
                if (n_array[i] == 1) {
                    n_array[j] = 10 + parseInt(n_array[j]);
                    n_array[i] = 0;
                }
            }
        }
        value = "";
        for (var i = 0; i < 9; i++) {
            if (i == 0 || i == 2 || i == 4 || i == 7) {
                value = n_array[i] * 10;
            } else {
                value = n_array[i];
            }
            if (value != 0) {
                words_string += words[value] + " ";
            }
            if ((i == 1 && value != 0) || (i == 0 && value != 0 && n_array[i + 1] == 0)) {
                words_string += "Crore ";
            }
            if ((i == 3 && value != 0) || (i == 2 && value != 0 && n_array[i + 1] == 0)) {
                words_string += "Lakh ";
            }
            if ((i == 5 && value != 0) || (i == 4 && value != 0 && n_array[i + 1] == 0)) {
                words_string += "Thousand ";
            }
            if (i == 6 && value != 0 && (n_array[i + 1] != 0 && n_array[i + 2] != 0)) {
                words_string += "Hundred ";
            } else if (i == 6 && value != 0) {
                words_string += "Hundred ";
            }
        }
        words_string = words_string.split("  ").join(" ");
    }
//

       if(currency=="INR"){
       point="Paise";
       }else if(currency=="USD"){
        point="Cents";
       }else if(currency=="EUR"){
        point="Cents";
       }else if(currency=="GBP"){
        point="Pence";
       }else{
       	//
       }

     if(d_length==2){
     	var fraction = toWords(atemp[1]);
        return words_string + 'and ' + fraction +' '+point ;
    }else{
    	return words_string;
    }
    
}

var oTable;


function startLoading(){
  $.blockUI({
		message: 'Please Wait...',	
		css: { 
            border: 'none', 
            padding: '20px',
            backgroundColor: '#000', 
            '-webkit-border-radius': '10px', 
            '-moz-border-radius': '10px', 
            opacity: .5, 
            color: '#fff' 
        } });
}
function stopLoading(){
  $.unblockUI();
}

$(document).ready(function(){
	var oTable='';
	$(window).scroll(function(){
	  var sticky = $('.content-header'),
		  scroll = $(window).scrollTop();

	  if (scroll >= 100) sticky.addClass('fixed');
	  else sticky.removeClass('fixed');
	});
});

function ajaxDataTableInit(url,id)
{
	$('#'+id).DataTable().destroy();
	oTable = $('#'+id).dataTable( {
		"scrollY":299,
		"processing": true,
		"serverSide": true,
		
		"aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0 , 1 , 3 , 4 ,5, 6] }],
		 "aoColumns": [	
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		
		],
		oLanguage: { sLengthMenu: "_MENU_", },
		"order": [0,'asc'],
		"bPaginate": true,
		"bLengthChange": true,
		"sDom": 'Rfrtlip',

		"bFilter": true,
		"bInfo": false,
		"searching":false,
		"aaSorting": [[ 1, "desc" ]],
		
		"iDisplayLength": 10,
		lengthMenu: [
            [ 10, 20, 30, 50, -1 ],
            [ 'Show 10', 'Show 20', 'Show 30', 'Show 50', 'Show all' ]
        ],

		"ajax": {
			"url": url,
			"type": "POST",
			"data": {'csrf_test_name':csrf_hash,'search':$("#search_cprofile").val(),'country':$("#allcountry").val(),'state':$("#allstate").val()},
		},
		"dom": "<'row'<'col-md-6'l><'col-md-6'f>><'row'<'col-md-6'B><'col-md-6'p>><'row'<'col-md-12't>><'row'<'col-md-12'i>>",
		"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
		
			 if($(aData[7]+' div').hasClass('deactive_record')){
				$('td', nRow).parent('tr').addClass('deactivate-row');
			} 

			$('td:first-child', nRow).addClass('bulk');
			$('td:nth-child(2)', nRow).addClass('profil-img');
			$('td:last-child', nRow).addClass('action-tab');
		 },
		 "fnDrawCallback": function () {
		  $('.dropdown-button, .translation-button, .dropdown-menu').dropdown({
						  inDuration: 300,
						  outDuration: 225,
						  constrainWidth: false,
						  hover: true,
						  gutter: 0,
						  belowOrigin: true,
						  alignment: 'left',
						  stopPropagation: false
						});
									
			$('.deactive_company_profile').off().on('click', function(){

				var profile = $(this).data('profileid');

				 $('#deactive_company_modal').modal('open');

				 $('#deactive_company_modal #company_profile').val(profile);

			});
			
			$('.deactive_company_status').off().on('click', function(){

				var profile_id = $('#company_profile').val();
			
				$.ajax({

					url:base_url+'profile/active_deactive_company',

					type:"POST",

					data:{'csrf_test_name':csrf_hash,"profile_id":profile_id,"status":"Inactive"},

					success:function(res){
							if(res == true)
							{
								get_table();
								Materialize.toast('Company profile deactivated successfully', 2000,'green rounded');
							}
							else
							{
								get_table();
								Materialize.toast('Error while processing!', 2000,'red rounded');
							}
						},

					});										
			});
			
			$('.active_company_profile').off().on('click',function(){

				var profile = $(this).data('profileid');

				 $('#active_company_profile').modal('open');

				 $('#active_company_profile #activate_cmp_profile_id').val(profile);

			});
			
			$('.active_cmp_profile').off().on('click',function(){

				var profile_id = $('#activate_cmp_profile_id').val();

				$.ajax({

					url:base_url+'profile/active_deactive_company',

					type:"POST",

					data:{'csrf_test_name':csrf_hash,"profile_id":profile_id,"status":"Active"},

					success:function(res){
							if(res == true)
							{
								get_table();
								Materialize.toast('Company Profile has been successfully re-activated', 2000,'green rounded');
							}
							else
							{
								get_table();
								Materialize.toast('Error while processing!', 2000,'red rounded');
							}

						},

					});										
			});
			
			var cmp_profiles = [];
				var cmp_profiles_values = "";
				$(".company_profile_bulk_action").on('click', function(e) {
					if($(this).is(':checked')){
						var total_checked = $(".company_profile_bulk_action:checked").length;
						  /*if (jQuery.inArray(cmp_profiles, $(this).val())== '-1') {
							cmp_profiles.push($(this).val());
						  } */
						  cmp_profiles=[];
						  $(".company_profile_bulk_action:checked").each(function() {
			
							cmp_profiles.push($(this).val());
						}); 
					}
					else
					{ 
						cmp_profiles=$('#deactive_multiple_cmp_profiles').attr('data-multi_profiles').split(',');
						 $(this).prop('checked',false);
						var remove_id = $(this).val();
						cmp_profiles = jQuery.grep(cmp_profiles, function(value) {
						  return value != remove_id;
						});
					}
						
						cmp_profiles_values = cmp_profiles.join(",");
						$('#deactive_multiple_cmp_profiles').attr('data-multi_profiles',cmp_profiles_values);
						$('#download_multiple_cmp_profiles').attr('data-multi_download',cmp_profiles_values);
					});
			
	 },
		/* aoColumnDefs: [
		  {
			 bSortable: false,
			 aTargets: [ -1 ]
		  }
		] */
	} );
}

function profile_datatable(url,id)
{

	$('#'+id).DataTable().destroy();
	oTable = $('#'+id).dataTable( {
		"scrollY":299,
		"processing": true,
		"serverSide": true,
		
		"aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0,1,2,3,4,5,6]}],
		 "aoColumns": [	
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },		
		{ "bSortable": false },		
		],
		oLanguage: { sLengthMenu: "_MENU_", },

		"order": [0,'asc'],
		"bPaginate": true,
		"bLengthChange": true,
		"sDom": 'Rfrtlip',

		"bFilter": true,
		"bInfo": false,
		"searching":false,
		"iDisplayLength": 10,
		lengthMenu: [
            [ 10, 20, 30, 50, -1 ],
            [ 'Show 10', 'Show 20', 'Show 30', 'Shows 50', 'Show all' ]
        ],

		
		"ajax": {
			"url": url,
			"type": "POST",
			"data": {'csrf_test_name':csrf_hash,'search':$("#search_personal_profile").val(),'name':$("#name_pr").val()},
			
		},
		"dom": "<'row'<'col-md-6'l><'col-md-6'f>><'row'<'col-md-6'B><'col-md-6'p>><'row'<'col-md-12't>><'row'<'col-md-12'i>>",
		"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
		
			 if($(aData[6]+' div').hasClass('deactive_record')){
				$('td', nRow).parent('tr').addClass('deactivate-row');
			} 

			$('td:first-child', nRow).addClass('bulk');
			$('td:nth-child(2)', nRow).addClass('profil-img');
			$('td:last-child', nRow).addClass('action-tab');
			$("#fixedHeader").removeClass('sticky');
		 },
		 "fnDrawCallback": function () {
		  $('.dropdown-button, .translation-button, .dropdown-menu').dropdown({
						  inDuration: 300,
						  outDuration: 225,
						  constrainWidth: false,
						  hover: true,
						  gutter: 0,
						  belowOrigin: true,
						  alignment: 'left',
						  stopPropagation: false
						});
									
			
			$('.deactive_personal_profile').off().on('click',function(){
						var profile = $(this).data('profileid');		
						 $('#deactive_profile').modal('open');
						 $('#deactive_profile #profile_id').val(profile);
					});
		
			
			$('.deactive_user').off().on('click',function(){
		
						var profile_id = $('#profile_id').val();
		
						$.ajax({
		
							url:base_url+'profile/deactive_personal_profile/' + profile_id,
		
							type:"POST",
		
							data:{'csrf_test_name':csrf_hash,"profile_id":profile_id,},
		
							success:function(res){

									if(res == true)
									{
										//window.location.href=base_url+'profile/manage_personal_profile';
										profile_datatable(base_path()+'profile/get_personal_profiles/','personal-profile');
										$('select').material_select();
										Materialize.toast('Profile has been successfully deactivated', 2000,'green rounded');
									}
									else if(res == 'lastadmin'){
                                       Materialize.toast('There should be at least 1 active admin profile', 2000,'red rounded');   
									}
									else
									{
										//window.location.href=base_url+'profile/manage_personal_profile';
										profile_datatable(base_path()+'profile/get_personal_profiles/','personal-profile');
										$('select').material_select();
										Materialize.toast('Error while processing. Please try again', 2000,'red rounded');
									}
								},
		
							});										
		
					});

				$('.active_personal_profile').off().on('click',function(){
		
						var profile = $(this).data('profileid');
		
						 $('#active_profile').modal('open');
		
						 $('#active_profile #activate_profile_id').val(profile);
		
					});
		
					$('.active_user').off().on('click',function(){
		
						var profile_id = $('#activate_profile_id').val();
		
						$.ajax({
		
							url:base_url+'profile/activate_personal_profile/' + profile_id,
		
							type:"POST",
		
							data:{'csrf_test_name':csrf_hash,"profile_id":profile_id,},
		
							success:function(res){
									if(res == true)
									{
										//window.location.href=base_url+'profile/manage_personal_profile';
										profile_datatable(base_path()+'profile/get_personal_profiles/','personal-profile');
										$('select').material_select();
										Materialize.toast('Your profile has been successfully re-activated', 2000,'green rounded');
									}
									else
									{
										//window.location.href=base_url+'profile/manage_personal_profile';
										profile_datatable(base_path()+'profile/get_personal_profiles/','personal-profile');
										$('select').material_select();
										Materialize.toast('Error while processing. Please try again', 2000,'red rounded');
									}
		
								},
		
							});										
					});
				
				var profiles = [];				
				var profiles_values = "";
				$(".personal_profile_bulk_action").off().on('click', function(e) {
				//alert('wow');	
					if($(this).is(':checked')){
						var total_checked = $(".personal_profile_bulk_action:checked").length;
						  if (jQuery.inArray(profiles, $(this).val())== '-1') {
							profiles.push($(this).val());
						  } 
					}
					else
					{
						profiles=$('#deactive_multiple_profiles').attr('data-multi_profiles').split(',');
						var remove_id = $(this).val();	
						profiles = jQuery.grep(profiles, function(value) {
						  return value != remove_id;
						});
					}
						profiles_values = profiles.join(",");
						$('#deactive_multiple_profiles').attr('data-multi_profiles',profiles_values);
						$('#download_multiple_profiles').attr('data-multi_download',profiles_values);
					});

/*
				var down_profiles = [];				
				var multi_download = "";
				$(".personal_profile_bulk_action").off().on('click', function(e) {
				alert('wow');	
					if($(this).is(':checked')){
						var total_checked = $(".personal_profile_bulk_action:checked").length;
						  if (jQuery.inArray(down_profiles, $(this).val())== '-1') {
							down_profiles.push($(this).val());
						  } 
					}
					else
					{
						var remove_id = $(this).val();	
						down_profiles = jQuery.grep(down_profiles, function(value) {
						  return value != remove_id;
						});
					}
						multi_download = down_profiles.join(",");
						
					});*/
	 },
		/* aoColumnDefs: [
		  {
			 bSortable: false,
			 aTargets: [ -1 ]
		  }
		] */
	} );
}

function expense_datatable(url,id)
{

	$('#'+id).DataTable().destroy();
	table_sales_inv =  $('#'+id).dataTable( {
		 "scrollY":299,
		"processing": true,
		"serverSide": true,
		
		"aoColumnDefs": [{ "bSortable": false, "aTargets": [0,1,2,3,4]}],
		 "aoColumns": [	
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		],
		
		"order": [0,'asc'],
		"bPaginate": true,
		"bLengthChange": false,
		"bFilter": true,
		"bInfo": false,
		"searching":false,
		"iDisplayLength": 10,
		"aLengthMenu": [10, 20, 40],
		"aaSorting": [[ 1, "desc" ]],
		
		"ajax": {
			"url": url,
			"type": "POST",
			"data": {'csrf_test_name':csrf_hash,'search':$("#search").val()},
			
		},
		"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
		
			 if($(aData[4]+' div').hasClass('deactive_record')){
				$('td', nRow).parent('tr').addClass('deactivate-row');
			} 

			$('td:first-child', nRow).addClass('bulk');
			//$('td:nth-child(2)', nRow).addClass('profil-img');
			$('td:last-child', nRow).addClass('action-tab');
		 },
		 "fnDrawCallback": function () {
		  $('.dropdown-button, .translation-button, .dropdown-menu').dropdown({
						  inDuration: 300,
						  outDuration: 225,
						  constrainWidth: false,
						  hover: true,
						  gutter: 0,
						  belowOrigin: true,
						  alignment: 'left',
						  stopPropagation: false
						});

		   var num = table_sales_inv.fnSettings().fnRecordsTotal();

		  	$('#totVouch').html(num);

		      $('.deactive_expense').off().on('click',function(){
							var exp_id = $(this).data('expid');		
							$('#deactivate_exp').modal('open');
							$('#deactivate_exp #expid').val(exp_id);
						});
					
			$('.remove_exp_type').off().on('click',function(){
				
						var exp_id = $('#expid').val();
		
						$.ajax({
		
							 url:base_url+'expense/deactive_expense/' + exp_id,
		
							type:"POST",
			
							data:{'csrf_test_name':csrf_hash,"exp_id":exp_id,},
							 success:function(res){
					                  if(res == true)
					                  {
					                    //location.reload();
					                    expense_datatable(base_path()+'expense/get_expense_list/','my-expense-list');
										$('select').material_select();
					                    Materialize.toast('Expense has been successfully deactivated', 2000,'green rounded');
					                  }
					                  else
					                  {
					                    Materialize.toast('Error while processing!', 2000,'red rounded');
					                  }
					                },

							});	

					});




				$('.active_expense').off().on('click',function(){
							var exp_id = $(this).data('expid');		
							$('#activate_exp').modal('open');
							$('#activate_exp #act_expid').val(exp_id);
						});
		
					$('.active_exp_type').off().on('click',function(){
		
						var exp_id = $('#act_expid').val();
		
						$.ajax({
		
							url:base_url+'expense/active_expense/' + exp_id,
		
							type:"POST",
		
							data:{'csrf_test_name':csrf_hash,"exp_id":exp_id,},
		
							 success:function(res){
			                  if(res == true)
			                  {
			                  	
			                  	//location.reload();
			                  	expense_datatable(base_path()+'expense/get_expense_list/','my-expense-list');
								$('select').material_select();
			                    Materialize.toast('Expense has been successfully Activated', 2000,'green rounded');
			                  }
			                  else
			                  {
			                    Materialize.toast('Error while processing!', 2000,'red rounded');
			                  }
			                },

		
							});										
					});
				
				var expense = [];				
				var expense_values = "";
				$(".expense_bulk_action").off().on('click', function(e) {
				//alert('wow');	
					if($(this).is(':checked')){
						var total_checked = $(".expense_bulk_action:checked").length;
						  if (jQuery.inArray(expense, $(this).val())== '-1') {
							expense.push($(this).val());
						  } 
					}
					else
					{
						var remove_id = $(this).val();	
						expense = jQuery.grep(expense, function(value) {
						  return value != remove_id;
						});
					}
						expense_values = expense.join(",");
						$('#deactive_multiple_expense').attr('data-multi_expense',expense_values);
					});


	 },
		/* aoColumnDefs: [
		  {
			 bSortable: false,
			 aTargets: [ -1 ]
		  }
		] */
	} );
}
function expenseForm(url,id)
{

	$('#'+id).DataTable().destroy();
	oTable = $('#'+id).dataTable( {
		"scrollY":299,
		"processing": true,
		"serverSide": true,
		
		"aoColumnDefs": [{ "bSortable": false, "aTargets": [0,1,2,3,4,5,6]}],
		 "aoColumns": [	
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		],
		
		"order": [0,'asc'],
		"bPaginate": true,
		"bLengthChange": false,
		"bFilter": true,
		"bInfo": false,
		"searching":false,
		"iDisplayLength": 10,
		"aLengthMenu": [10, 20, 40],
		"aaSorting": [[ 1, "desc" ]],
		
		"ajax": {
			"url": url,
			"type": "POST",
			"data": {'csrf_test_name':csrf_hash,'start_date':$("#start_date").val(),'end_date':$("#end_date").val(),'pay_status':$("#pay_status").val(),'expense_type':$("#expense_type").val(),'search':$("#search").val()},
			
		},
		"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
		
			 if($(aData[6]+' div').hasClass('deactive_record')){
				$('td', nRow).parent('tr').addClass('deactivate-row');
			} 
			if($(aData[5]+'span').hasClass('green-c')){
				$('td:nth-child(6)', nRow).addClass('green-c');
			}else if($(aData[5]+' span').hasClass('hash-c')){
				$('td:nth-child(6)', nRow).addClass('hash-c');
			}else if($(aData[5]+' span').hasClass('yellow-c')){
				$('td:nth-child(6)', nRow).addClass('yellow-c');
			}else{
				$('td:nth-child(6)', nRow).addClass('red-c');
			}	
			$('td:first-child', nRow).addClass('bulk');
			//$('td:nth-child(2)', nRow).addClass('profil-img');
			$('td:last-child', nRow).addClass('action-tab');
		 },
		 "fnDrawCallback": function () {
		  $('.dropdown-button, .translation-button, .dropdown-menu').dropdown({
						  inDuration: 300,
						  outDuration: 225,
						  constrainWidth: false,
						  hover: true,
						  gutter: 0,
						  belowOrigin: true,
						  alignment: 'left',
						  stopPropagation: false
						});

		     		 $('.deactive_expenseForm').off().on('click',function(){
						var eformid = $(this).data('eformid');		
						$('#da_expenseform').modal('open');
						$('#da_expenseform #eform_id').val(eformid);
						$('#da_expenseform .status_type').text('deactivate');
					});
					$('.active_expenseForm').off().on('click',function(){
						var eformid = $(this).data('eformid');		
						$('#da_expenseform').modal('open');
						$('#da_expenseform #eform_id').val(eformid);
						$('#da_expenseform .status_type').text('activate');
					});
		
					$('.active_deactive').off().on('click',function(){
					
						var eformid = $('#eform_id').val();
						var status_type = $('#status_type').text();
						var status="Active";
						if(status_type=="deactivate"){
							status="Inactive";
						}
						$.ajax({
		
							 url:base_url+'expense/active_deactive_expenseForm/' + eformid,
		
							type:"POST",
			
							data:{'csrf_test_name':csrf_hash,"eform_id":eformid,"status":status},
							 success:function(res){
					                  if(res == true)
					                  {
					                   expense_form();
					                    Materialize.toast('Expense Form has been successfully '+status_type+'d', 2000,'green rounded');
					                  }
					                  else
					                  {
					                    Materialize.toast('Error while processing!', 2000,'red rounded');
					                  }
					                },						
							});												
					});
				var expenseForm = [];				
				var expenseForm_values = "";
				$(".expense_form_bulk_action").off().on('click', function(e) {
				//alert('wow');	
					if($(this).is(':checked')){
						var total_checked = $(".expense_form_bulk_action:checked").length;
						  if (jQuery.inArray(expenseForm, $(this).val())== '-1') {
							expenseForm.push($(this).val());
						  } 
					}
					else
					{
						var remove_id = $(this).val();	
						expenseForm = jQuery.grep(expenseForm, function(value) {
						  return value != remove_id;
						});
					}
						expenseForm_values = expenseForm.join(",");
						$('#deactive_multiple_expenseForm').attr('data-multi_expenseform',expenseForm_values);
					});
	 },
		/* aoColumnDefs: [
		  {
			 bSortable: false,
			 aTargets: [ -1 ]
		  }
		] */
	} );
}
function customerDatatable(url,id)
{
	$('#deactive_multiple_customers').attr('data-multi_customer',0);
    $('#download_multiple_customers').attr('data-multi_download',0);
	$('#'+id).DataTable().destroy();
	oTable = $('#'+id).dataTable({
		"scrollY":299,
		"bServerSide": true,
		"bProcessing": true,
		"bDeferRender": true,
		"aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0 , 1 , 3 , 4 ,5, 6,7] }],
		 "aoColumns": [	
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		],

		oLanguage: { sLengthMenu: "_MENU_", },
		"order": [0,'asc'],
		"bPaginate": true,
		"bLengthChange": true,
		"sDom": 'Rfrtlip',
		"bFilter": true,
		"bInfo": false,
		"searching":false,
		
		"aaSorting": [[ 1, "desc" ]],
		"iDisplayLength": 10,
		lengthMenu: [
            [ 10, 20, 30, 50, -1 ],
            [ 'Show 10', 'Show 20', 'Show 30', 'Show 50', 'Show all' ]
        ],
		"ajax": {
			"url": url,
			"type": "POST",
			"data": {'csrf_test_name':csrf_hash,'search':$("#search_cli").val(),'status':$("#status").val(),'country':$("#allcountry").val(),'state':$("#allstate").val()},
		},
		"dom": "<'row'<'col-md-6'l><'col-md-6'f>><'row'<'col-md-6'B><'col-md-6'p>><'row'<'col-md-12't>><'row'<'col-md-12'i>>",
		
		"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
		
			 if($(aData[7]+' div').hasClass('deactive_record')){
				$('td', nRow).parent('tr').addClass('deactivate-row');
			} 

			$('td:first-child', nRow).addClass('bulk');
			$('td:last-child', nRow).addClass('action-tab');
			$("#fixedHeader").removeClass('sticky');
		 },
		 "fnDrawCallback": function () {
		  $('.dropdown-button, .translation-button, .dropdown-menu').dropdown({
						  inDuration: 300,
						  outDuration: 225,
						  constrainWidth: false,
						  hover: true,
						  gutter: 0,
						  belowOrigin: true,
						  alignment: 'left',
						  stopPropagation: false
						});
									
					$('.deactive_customer').off().on('click',function(){
		
						var customer_id = $(this).data('customer_id');
						
						 $('#da_customer_model').modal('open');
					
						 $('#da_customer_model #ccustomer_id').val(customer_id);
						 $('#da_customer_model .cstatus_type').text('deactivate');	
					});
		
					
					$('.activate_customer').off().on('click',function(){
		
						var customer_id = $(this).data('customer_id');
						 $('#da_customer_model').modal('open');
					
						 $('#da_customer_model #ccustomer_id').val(customer_id);
						 $('#da_customer_model .cstatus_type').text('re-activate');	
		
					});
					$('.active_deactive_customer').off().on('click',function(){
		
						var customer_id = $('#ccustomer_id').val();
						var status_type = $('#cstatus_type').text();
						var status="Active";
						if(status_type=="deactivate"){
							status="Inactive";
						}
						$.ajax({
		
							url:base_url+'sales/active_deactive_customer',
		
							type:"POST",
		
							data:{'csrf_test_name':csrf_hash,"customer_id":customer_id,"status":status},
		
							success:function(res){
									if(res == true)
									{
										getCustomers();

										Materialize.toast('Client has been '+status_type+'d', 2000,'green rounded');
									}
									else
									{
										getCustomers();
										Materialize.toast('Error while processing!', 2000,'red rounded');
									}
								},
							});										
					});
					
					
					$('.send_invitation').on('click', function(){
						var customer_id = $(this).data('cust_id');
						var email = $(this).data('email');
						$('#invite_customer_modal').modal('open');
						$('#invite_customer_modal #customer_id').val(customer_id);
						$('#invite_customer_modal #mailto_id').val(email);
					});	
					
					$('.invite_customer').off().on('click',function(){
						var customer_id = $('#customer_id').val();
						var email = $('#con_email').val();
						$.ajax({
							url:base_url+'sales/invite_customers',
							type:"POST",
							data:{'csrf_test_name':csrf_hash,"customer_id":customer_id,"email":email},
							success:function(res){
									if(res == true)
									{
										Materialize.toast('Your Client has been invited to experience the convenience of My Easy Invoices', 5000,'green rounded');
										getCustomers();
									}
									else
									{
										Materialize.toast('Error while processing!', 2000,'red rounded');
										getCustomers();
									}
								},
							});										
					});	
					/*$(document).off().on('click', '.send_mail' ,function(){
						var customer_id = $(this).data('customer_id');
						var customer_email = $(this).data('cust_email');
						$('#email_customer_modal').modal('open');
						$('#email_customer_modal #email_cust_id').val(customer_id);
						$('#email_customer_modal #email_cust_email').val(customer_email);
					});
					
					$('.send_mail_customer').off().on('click',function(){
						var customer_id = $('#email_cust_id').val();
						var customer_email = $('#email_cust_email').val();
						$.ajax({
							url:base_url+'My_customers/send_email_to_customer',
							type:"POST",
							data:{"customer_id":customer_id,"customer_email":customer_email,},
							success:function(res){
									if(res == true)
									{
										Materialize.toast('Customer Details sent successfully!', 2000,'green rounded');
									}
									else
									{
										Materialize.toast('Error while processing!', 2000,'red rounded');
									}
								},
							});										
					});	
					*/

					
				var customer_profiles = [];
				var customer_profiles_values = "";

				$(".customer_profile_bulk_action").off().on('click', function() {
					if($(this).is(':checked')){
						  /*if (jQuery.inArray(customer_profiles, $(this).val())== '-1') {
							customer_profiles.push($(this).val());
						  } */
						  customer_profiles=[];
						  $(".customer_profile_bulk_action:checked").each(function() {
			
							customer_profiles.push($(this).val());
						});
					}
					else
					{
						customer_profiles=$('#deactive_multiple_customers').attr('data-multi_customer').split(',');
						$(this).prop('checked',false);
						var remove_id = $(this).val();
						customer_profiles = jQuery.grep(customer_profiles, function(value) {
						  return value != remove_id;
						});
					}
						customer_profiles_values = customer_profiles.join(",");
						$('#deactive_multiple_customers').attr('data-multi_customer',customer_profiles_values);
						$('#download_multiple_customers').attr('data-multi_download',customer_profiles_values);
					});
				$('body').on('click', '.view_accessport' ,function(){
				var change_status=0;
				if($(this).prop('checked')==true){
				$('#view_add_modal-popup').modal('open');
				  change_status=1;	
				}else{
					$('#view_modal-popup').modal('open');
				}
				var customer_id = $(this).val();
				
				$('.portal_access_yes').off().on('click',function(){
				$.ajax({
					url:base_url+'sales/edit_portal_access',
					type:"POST",
					data:{'csrf_test_name':csrf_hash,"customer_id":customer_id,"change_status":change_status},
					success:function(res){
						var ab = res;
						if(ab == 11)
						{
							Materialize.toast('Access portal details has been send on client Email', 2000,'green rounded');
						}if(ab == 1)
						{
							Materialize.toast('Client Portal Access has been changed successfully!', 2000,'green rounded');
						}
						//alert(ab);

							getCustomers();
						},
					});
				});	
				$('.portal_access_no').off().on('click',function(){
					getCustomers();
				});
				
				});	
				
			
			 },
		  }); 

	}

function ev_datatable(url,id)
      {
       	$('#'+id).DataTable().destroy();
       	$('#delete_multiple_voucher').attr('data-multi_voucher',0);
		var table_sales_inv= oTable = $('#'+id).dataTable({
		"scrollY":299,
        "processing": true,
          "serverSide": true,
          
          "aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0,1,2,3,4,5,6,7,8]}],
           "aoColumns": [ 
          { "bSortable": false },
          { "bSortable": false },
          { "bSortable": false },
          { "bSortable": false },
          { "bSortable": false },
		  { "bSortable": false },   
		  { "bSortable": false },
		  { "bSortable": false },
		  { "bSortable": false },
          ],
          
         oLanguage: { sLengthMenu: "_MENU_", },
    "order": [0,'DESC'],
    "bPaginate": true,
    "bLengthChange": true,
    "sDom": 'Rfrtlip',

    "bFilter": true,
    "bInfo": false,
    "searching":false,
	"iDisplayLength": 10,
		lengthMenu: [
            [ 10, 20, 30, 50, -1 ],
            [ 'Show 10', 'Show 20', 'Show 30', 'Show 50', 'Show all' ]
        ],
          "aaSorting": [[ 1, "desc" ]],
          
          "ajax": {
            "url": url,
            "type": "POST",
            // "data": {'search_dc':$("#search_dc").val()},
            "data": {'csrf_test_name':csrf_hash,'search':$("#search_dc").val(),'search_by_emp_name':$("#search_by_emp_name").val(),'search_by_client_name':$("#search_by_client_name").val(),'inv_start_date':$("#inv_start_date").val(),'inv_end_date':$("#inv_end_date").val()},
          },
          "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
          
             if($(aData[6]+' div').hasClass('deactive_record')){
              $('td', nRow).parent('tr').addClass('deactivate-row');
            } 

            $('td:first-child', nRow).addClass('bulk');
           // $('td:nth-child(2)', nRow).addClass('profil-img');
            $('td:last-child', nRow).addClass('action-tab');
			$("#fixedHeader").removeClass('sticky');
           },
           "fnDrawCallback": function () {
            $('.dropdown-button, .translation-button, .dropdown-menu').dropdown({
                    inDuration: 300,
                    outDuration: 225,
                    constrainWidth: false,
                    hover: true,
                    gutter: 0,
                    belowOrigin: true,
                    alignment: 'left',
                    stopPropagation: false
                  });
                        
            var num = table_sales_inv.fnSettings().fnRecordsTotal();
            $("#totVouch").html(num);
            $('.deactive_expense_voucher').on('click',function(){
                  var ev_id = $(this).data('ev_id');    
                   $('#deactive_ev').modal('open');
                   $('#deactive_ev #ev').val(ev_id);
                });
          
            
            $('.del_exp_vou').off().on('click',function(){
          
                  var ev_id = $('#ev').val();
          
                  $.ajax({
          
                    url:base_url+'sales/deactive_expense_voucher/' + ev_id,
          
                    type:"POST",
          
                    data:{'csrf_test_name':csrf_hash,"ev_id":ev_id,},
          
                    success:function(res){
                        if(res == true)
                        {
                          Materialize.toast('Deleted Successfully', 5000,'green rounded');
                          ev_datatable(base_path()+'sales/get_expense_voucher/','my-expense-voucher');
						  $('select').material_select();
                        }
                        else
                        {
                          ev_datatable(base_path()+'sales/get_expense_voucher/','my-expense-voucher');
						  $('select').material_select();
                          Materialize.toast('Error while processing. Please try again', 5000,'red rounded');
                        }
                      },
          
                    });                   
          
                });

             
             var vouchers = [];
        var vouchers_values = "";
        $(".evs_bulk_action").on('click', function(e) {
          if($(this).is(':checked')){
            var total_checked = $(".evs_bulk_action:checked").length;
             /* if (jQuery.inArray(invoices, $(this).val())== '-1') {
              invoices.push($(this).val());
              }*/
              vouchers=[];
              $(".evs_bulk_action:checked").each(function() {
      
              vouchers.push($(this).val());
            }); 
          }
          else
          {
            vouchers=$('#delete_multiple_voucher').attr('data-multi_voucher').split(',');

            console.log(vouchers);
            $(this).prop('checked',false);
            var remove_id = $(this).val();  
            vouchers = jQuery.grep(vouchers, function(value) {
              return value != remove_id;
            });
          }
            vouchers_values = vouchers.join(",");
            $('#delete_multiple_voucher').attr('data-multi_voucher',vouchers_values);
         
          });
         },
        
        } );
      }  	 

	
function myServices(url,id)
{
	$('#'+id).DataTable().destroy();
 	oTable = $('#'+id).dataTable({
		"scrollY": 400,
	 "bServerSide": true,
	"bProcessing": true,
	"bDeferRender": true,
    "aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0 , 1 , 3 , 4 , 5,6] }],
	 "aoColumns": [	
	 { "bSortable": false },
		{ "bSortable": true },
		{ "bSortable": true },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
	],
	oLanguage: { sLengthMenu: "_MENU_", },
   "order": [0,'asc'],
    "bPaginate": true,
    "bLengthChange": true,
    "sDom": 'Rfrtlip',

    "bFilter": true,
    "bInfo": false,
    "searching":false,
	"iDisplayLength": 10,
	lengthMenu: [
        [ 10, 20, 30, 50, -1 ],
        [ 'Show 10', 'Show 20', 'Show 30', 'Show 50', 'Show all' ]
    ],


		"ajax": {
			"url": url,
			"type": "POST",
			"data": {'csrf_test_name':csrf_hash,'search':$("#search_itm").val(),'status':$("#status").val(),'search_by_item':$("#search_by_item").val(),'ser_start_date':$("#ser_start_date").val(),'ser_end_date':$("#ser_end_date").val()},
		},
	"dom": "<'row'<'col-md-6'l><'col-md-6'f>><'row'<'col-md-6'B><'col-md-6'p>><'row'<'col-md-12't>><'row'<'col-md-12'i>>",
	"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
		if($(aData[8]+' div').hasClass('deactive_record')){
			$('td', nRow).parent('tr').addClass('deactivate-row');
		}

		$('td:first-child', nRow).addClass('bulk');
		
		$('td:nth-child(4)', nRow).addClass('center_cls');
		$('td:last-child', nRow).addClass('action-tab');
		$("#fixedHeader").removeClass('sticky');
     },
	 "fnDrawCallback": function () {
		  $('.dropdown-button, .translation-button, .dropdown-menu').dropdown({
						  inDuration: 300,
						  outDuration: 225,
						  constrainWidth: false,
						  hover: true,
						  gutter: 0,
						  belowOrigin: true,
						  alignment: 'left',
						  stopPropagation: false
						});
			
			$('.active_service').off().on('click',function(){

				var service_id = $(this).data('service_id');

				 $('#active_deactive_service').modal('open');

				 $('#active_deactive_service #active_service_id').val(service_id);
				 $('#active_deactive_service .sstatus_type').text('re-activate');
				 $('#active_deactive_service #sstatus_type').text('Re-activate');

			});						
			$('.deactive_service').off().on('click', function(){

				var service_id = $(this).data('service_id');

				 $('#active_deactive_service').modal('open');

				 $('#active_deactive_service #active_service_id').val(service_id);
				 $('#active_deactive_service .sstatus_type').text('deactivate');
				 $('#active_deactive_service #sstatus_type').text('deactivate');
			});
			
			$('.active_deactive_service').off().on('click', function(){

				var service_id = $('#active_service_id').val();
				var status_type = $('#sstatus_type').text();
						var sstatus ="Active";
						if(status_type=="deactivate"){
							sstatus="Inactive";
						}
				$.ajax({

					url:base_url+'services/active_deactive_service',

					type:"POST",

					data:{'csrf_test_name':csrf_hash,"service_id":service_id,"status":sstatus},

					success:function(res){
							if(res == true)
							{
									
								myServices(base_path()+'services/get_services/','myservices');
								$('select').material_select();
								Materialize.toast('Item has been '+status_type+'d', 2000,'green rounded');
							}
							else
							{
									
								myServices(base_path()+'services/get_services/','myservices');
								$('select').material_select();
								Materialize.toast('Error while processing!', 2000,'red rounded');
							}
						},

					});										
			});
									
				var services = [];
				var services_values = "";
				$(".services_bulk_action").on("click", function() {
					if($(this).is(':checked')){
						var total_checked = $(".services_bulk_action:checked").length;
						  /*if (jQuery.inArray(services, $(this).val())== '-1') {
							services.push($(this).val());
						  } */
						   services=[];
						  $(".services_bulk_action:checked").each(function() {
			
							services.push($(this).val());
						}); 
					}
					else
					{
						services=$('#deactive_multiple_services').attr('data-multi_services').split(',');
						//console.log(invoices);
						$(this).prop('checked',false);
						var remove_id = $(this).val();	
						var remove_id = $(this).val();	
						services = jQuery.grep(services, function(value) {
						  return value != remove_id;
						});
					}
						services_values = services.join(",");
						$('#deactive_multiple_services').attr('data-multi_services',services_values);
					});
			
	 },
  }); 
}

function mySubscription(url,id)
{
	$('#'+id).DataTable().destroy();
	if(csrf_hash===""){
		csrf_hash='<?php echo $this->security->get_csrf_hash;?>';
	}
 	oTable = $('#'+id).dataTable({
	"scrollY":299,	
	 "bServerSide": true,
	"bProcessing": true,
	"bDeferRender": true,
    "aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0 , 1 , 2 , 3 , 4 , 5, 6, 7] }],
	 "aoColumns": [	
	 { "bSortable": false },
		{ "bSortable": true },
		{ "bSortable": true },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
	],
	oLanguage: { sLengthMenu: "_MENU_", },
   "order": [0,'desc'],
    "bPaginate": true,
    "bLengthChange": true,
    "sDom": 'Rfrtlip',

    "bFilter": true,
    "bInfo": false,
    "searching":false,
	"iDisplayLength": 10,
	lengthMenu: [
        [ 10, 20, 30, 50, -1 ],
        [ 'SHOW 10', 'SHOW 20', 'SHOW 30', 'SHOW 50', 'SHOW ALL' ]
    ],


		"ajax": {
			"url": url,
			"type": "POST",
			"data": {'csrf_test_name':csrf_hash,'search':$("#search_itm").val(),'sub_start_date':$("#sub_start_date").val(),'sub_end_date':$("#sub_end_date").val()},
		},
		"initComplete":function( settings,json){
				//console.log(json);
				//var csrfdata=jQuery.parseJSON(json);
				console.log(json.csrf_hash);
				if(json.csrf_hash){
					csrf_hash=json.csrf_hash;
				}
				// call your function here
			},
	"dom": "<'row'<'col-md-6'l><'col-md-6'f>><'row'<'col-md-6'B><'col-md-6'p>><'row'<'col-md-12't>><'row'<'col-md-12'i>>",
	"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
		if($(aData[8]+' div').hasClass('deactive_record')){
			$('td', nRow).parent('tr').addClass('deactivate-row');
		}

		$('td:first-child', nRow).addClass('bulk');
		
		//$('td:nth-child(4)', nRow).addClass('center_cls');
		$('td:last-child', nRow).addClass('action-tab');
		$("#fixedHeader").removeClass('sticky');
     },
	 "fnDrawCallback": function () {
		  $('.dropdown-button, .translation-button, .dropdown-menu').dropdown({
						  inDuration: 300,
						  outDuration: 225,
						  constrainWidth: false,
						  hover: true,
						  gutter: 0,
						  belowOrigin: true,
						  alignment: 'left',
						  stopPropagation: false
						});	
				var subscription = [];
				var subscription_values = "";
				$(".subscription_bulk_action").on("click", function() {
					if($(this).is(':checked')){
						var total_checked = $(".subscription_bulk_action:checked").length;
						  if (jQuery.inArray(subscription, $(this).val())== '-1') {
							subscription.push($(this).val());
						  } 
						   subscription=[];
						  $(".subscription_bulk_action:checked").each(function() {
			
							subscription.push($(this).val());
						}); 
					}
					else
					{
						subscription=$('#download_multiple_subscription').attr('data-multi_subscription').split(',');
						//console.log(invoices);
						$(this).prop('checked',false);
						var remove_id = $(this).val();	
						var remove_id = $(this).val();	
						subscription = jQuery.grep(subscription, function(value) {
						  return value != remove_id;
						});
					}
						subscription_values = subscription.join(",");
						$('#download_multiple_subscription').attr('data-multi_subscription',subscription_values);
					});
			//$('select').select2();
	 }, 
  }); 
}


function myActivity(url,id)
{
	//alert('ssss');
	$('#'+id).DataTable().destroy();
 	oTable = $('#'+id).dataTable({
	 "scrollY":299,
	 "bServerSide": true,
	"bProcessing": true,
	"bDeferRender": true,
    "aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0 , 1 , 2 , 3 , 4 , 5, 6, 7] }],
	 "aoColumns": [	
	 { "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
	],
	oLanguage: { sLengthMenu: "_MENU_", },
   "order": [0,'asc'],
    "bPaginate": true,
    "bLengthChange": true,
    "sDom": 'Rfrtlip',

    "bFilter": true,
    "bInfo": false,
    "searching":false,
	"iDisplayLength": 10,
	lengthMenu: [
        [ 10, 20, 30, 50, -1 ],
        [ 'Show 10', 'Show 20', 'Show 30', 'Show 50', 'Show all' ]
    ],


		"ajax": {
			"url": url,
			"type": "POST",
			"data": {'csrf_test_name':csrf_hash,'search':$("#search_activity").val(),'module_name':$("#module_name").val(),'person_name':$("#person_name").val(),'act_start_date':$("#act_start_date").val(),'act_end_date':$("#act_end_date").val()},
		},
	"dom": "<'row'<'col-md-6'l><'col-md-6'f>><'row'<'col-md-6'B><'col-md-6'p>><'row'<'col-md-12't>><'row'<'col-md-12'i>>",
	"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
		if($(aData[8]+' div').hasClass('deactive_record')){
			$('td', nRow).parent('tr').addClass('deactivate-row');
		}

		$('td:first-child', nRow).addClass('bulk');
		
		$('td:nth-child(4)', nRow).addClass('');
		$('td:last-child', nRow).addClass('action-tab');
		$("#fixedHeader").removeClass('sticky');
     },
	 "fnDrawCallback": function () {
		  		$('.dropdown-button, .translation-button, .dropdown-menu').dropdown({
						  inDuration: 300,
						  outDuration: 225,
						  constrainWidth: false,
						  hover: true,
						  gutter: 0,
						  belowOrigin: true,
						  alignment: 'left',
						  stopPropagation: false
					});
		  		
									
				var activity = [];
				var activity_values = "";
				$(".activity_bulk_action").on("click", function() {
					if($(this).is(':checked')){
						var total_checked = $(".activity_bulk_action:checked").length;
						  if (jQuery.inArray(activity, $(this).val())== '-1') {
							activity.push($(this).val());
						  } 
						   activity=[];
						  $(".activity_bulk_action:checked").each(function() {
			
							activity.push($(this).val());
						}); 
					}
					else
					{
						activity=$('#download_multiple_activity').attr('data-multi_download').split(',');
						//console.log(invoices);
						$(this).prop('checked',false);
						var remove_id = $(this).val();	
						var remove_id = $(this).val();	
						activity = jQuery.grep(activity, function(value) {
						  return value != remove_id;
						});
					}
						activity_values = activity.join(",");
						$('#download_multiple_activity').attr('data-multi_download',activity_values);
					});
			
	 },


  }); 


}

function myManufServices(url,id){
	

	$('#'+id).DataTable().destroy();
 	table_manuf_services = $('#'+id).dataTable({	
	"scrollY":299,
	"bServerSide": true,
	"bProcessing": true,
	"bDeferRender": true,
    "aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0 , 1 , 3 , 4 , 5, 6] }],
	 "aoColumns": [	
			{ "bSortable": false },
				{ "bSortable": true },
				{ "bSortable": false },
				{ "bSortable": false },
				{ "bSortable": false },
				{ "bSortable": false },
				{ "bSortable": false },
				{ "bSortable": false },
				{ "bSortable": false },
				//null,null,
				{ "bSortable": false },
				{"bVisible": false},
				{ "bSortable": false },
				{ "bSortable": false },
	],

	"order": [0,'asc'],
    "bPaginate": true,
    "bLengthChange": false,
    "bFilter": true,
    "bInfo": false,
    "searching":true,
	"iDisplayLength": 10,
	"aLengthMenu": [10, 20, 40],
	"aaSorting": [[ 1, "desc" ]],
	"ajax": {
			"url": url,
			"type": "POST",
			"data": {'csrf_test_name':csrf_hash,'search':$("#search_itm").val(),'search_by_item':$("#search_by_item").val(),'ser_start_date':$("#ser_start_date").val(),'ser_end_date':$("#ser_end_date").val()},
			
		},
	"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
		if($(aData[12]+' div').hasClass('deactive_record')){
			$('td', nRow).parent('tr').addClass('deactivate-row');
		}

		$('td:first-child', nRow).addClass('bulk');
		$('td:nth-child(2)', nRow).addClass('profil-img');
		$('td:last-child', nRow).addClass('action-tab');
     },
			"fnDrawCallback": function () {
				//actDltLink();
				  $('.dropdown-button, .translation-button, .dropdown-menu').dropdown({
						  inDuration: 300,
						  outDuration: 225,
						  constrainWidth: false,
						  hover: true,
						  gutter: 0,
						  belowOrigin: true,
						  alignment: 'left',
						  stopPropagation: false
						});
			$('.active_service').off().on('click',function(){

				var service_id = $(this).data('service_id');

				 $('#active_deactive_service').modal('open');

				 $('#active_deactive_service #active_service_id').val(service_id);
				 $('#active_deactive_service .sstatus_type').text('activate');
				 $('#active_deactive_service #sstatus_type').text('activate');

			});						
			$('.deactive_service').off().on('click', function(){

				var service_id = $(this).data('service_id');

				 $('#active_deactive_service').modal('open');

				 $('#active_deactive_service #active_service_id').val(service_id);
				 $('#active_deactive_service .sstatus_type').text('remove');
				 $('#active_deactive_service #sstatus_type').text('deactivate');
			});
			
			$('.active_deactive_service').off().on('click', function(){

				var service_id = $('#active_service_id').val();
				var status_type = $('#sstatus_type').text();
						var sstatus ="Active";
						if(status_type=="deactivate"){
							sstatus="Inactive";
						}
				$.ajax({

					url:base_url+'services/active_deactive_service',

					type:"POST",

					data:{'csrf_test_name':csrf_hash,"service_id":service_id,"status":sstatus},

					success:function(res){
							if(res == true)
							{
									
								myManufServices(base_path()+'services/get_manufacturing_services/','manufacturing_services');
								$('select').material_select();
								Materialize.toast('Service has been deactivated successfully', 2000,'green rounded');
							}
							else
							{
									
								myManufServices(base_path()+'services/get_manufacturing_services/','manufacturing_services');
								$('select').material_select();
								Materialize.toast('Error while processing!', 2000,'red rounded');
							}
						},

					});										
			});
									
				var services = [];
				var services_values = "";
				$(".services_bulk_action").on("click", function() {
					if($(this).is(':checked')){
						var total_checked = $(".services_bulk_action:checked").length;
						  if (jQuery.inArray(services, $(this).val())== '-1') {
							services.push($(this).val());
						  } 
					}
					else
					{
						var remove_id = $(this).val();	
						services = jQuery.grep(services, function(value) {
						  return value != remove_id;
						});
					}
						services_values = services.join(",");
						$('#deactive_multiple_services').attr('data-multi_services',services_values);
					});			
						
		},	
    });

}
function creditDebitDatatable(url,id){
	$('#'+id).DataTable().destroy();
	$('#deactive_multiple_notes').attr('data-multi_note',0);
	$('#download_multiple_notes').attr('data-multi_download',0);
 	table_credit_notes = $('#'+id).dataTable({	
		"scrollY":299,
		 "bServerSide": true,
		"bProcessing": true,
		"bDeferRender": true,
		"aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0 ,1, 2 , 3, 4, 5] }],
		 "aoColumns": [	
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			//{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			
		],
		
	
		oLanguage: { sLengthMenu: "_MENU_", },
		"order": [0,'asc'],
		"bPaginate": true,
		"bLengthChange": true,
		
		"sDom": 'Rfrtlip',
		"bFilter": true,
		"bInfo": false,
		"searching":false,
		"iDisplayLength": 10,
		lengthMenu: [
            [ 10, 20, 30, 50, -1 ],
            [ 'Show 10', 'Show 20', 'Show 30', 'Show 50', 'Show all' ]
        ],

		"aaSorting": [[ 1, "desc" ]],
		"ajax": {
			"url": url,
			"type": "POST",
			"data": {'csrf_test_name':csrf_hash,'search':$("#search_cdn").val(),'search_by_customer':$("#search_by_customer").val(),'search_by_type':'CN','cd_start_date':$("#cdp_start_date").val(),'cd_end_date':$("#cdp_end_date").val()},
		},
		"dom": "<'row'<'col-md-6'l><'col-md-6'f>><'row'<'col-md-6'B><'col-md-6'p>><'row'<'col-md-12't>><'row'<'col-md-12'i>>",
		"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
			/*if($(aData[5]+' div').hasClass('deactive_record')){
				$('td', nRow).parent('tr').addClass('deactivate-row');
			}*/
		
			$('td:first-child', nRow).addClass('bulk');
			//$('td:nth-child(2)', nRow).addClass('profil-img');
			$('td:last-child', nRow).addClass('action-tab');
			$("#fixedHeader").removeClass('sticky');
		 },
		 "fnDrawCallback": function () {
			  $('.dropdown-button, .translation-button, .dropdown-menu').dropdown({
							  inDuration: 300,
							  outDuration: 225,
							  constrainWidth: false,
							  hover: true,
							  gutter: 0,
							  belowOrigin: true,
							  alignment: 'left',
							  stopPropagation: false
							});
			
			$('.deactive_credit_debit').off().on('click', function(){
				var dc_id = $(this).data('cd_id');
				$('#deactivate_note').modal('open');
				$('#deactivate_note #deactivate_note_id').val(dc_id);
			});
			$('.de-act-debit').off().on('click', function(){
				var db_id = $(this).data('debit_id');
				$('#deactivate_note').modal('open');
				$('#deactivate_note #deactivate_note_id').val(db_id);
			});
			
			$('.remove_cn').off().on('click', function(){

				var dc_id = $('#deactivate_note_id').val();
			
				$.ajax({

					url:base_url+'sales/deactive_cd_note',

					type:"POST",

					data:{'csrf_test_name':csrf_hash,"cd_id":dc_id,"status":"Inactive"},

					success:function(res){
							if(res == true)
							{
								creditDebitDatatable(base_path()+'sales/get_credit_debit_notes/','my-credit-debit');
								$('select').material_select();
								Materialize.toast('The record has been deleted', 2000,'green rounded');
							}
							else
							{
								creditDebitDatatable(base_path()+'sales/get_credit_debit_notes/','my-credit-debit');
								$('select').material_select();
								Materialize.toast('Error while processing!', 2000,'red rounded');
							}
						},
					});										
			});
			
			$('.remove_debit').off().on('click', function(){

				var dc_id = $('#deactivate_note_id').val();
				$.ajax({

					url:base_url+'expense/deactive_debit_note',

					type:"POST",

					data:{'csrf_test_name':csrf_hash,"debit_id":dc_id,"status":"Inactive"},

					success:function(res){
							if(res == true)
							{
								creditDebitDatatable(base_path()+'expense/get_debit_notes/','my-debit');
								$('select').material_select();
								Materialize.toast('The record has been deleted', 2000,'green rounded');
							}
							else
							{
								creditDebitDatatable(base_path()+'expense/get_debit_notes/','my-debit');
								$('select').material_select();
								Materialize.toast('Error while processing!', 2000,'red rounded');
							}
						},
					});										
			});
			
			/*$('.activate_cn').off().on('click',function(){
				var rc_id = $(this).data('cn_id');;
				 $('#activate_note').modal('open');
				 $('#activate_note #activate_note_id').val(rc_id);
			});
			
			$('.active_cn').off().on('click', function(){
				var dc_id = $('#activate_note_id').val();
				$.ajax({
					url:base_url+'My_credit_debit_notes/activate_note',
					type:"POST",
					data:{"dc_id":dc_id,},
					success:function(res){
							if(res == true)
							{
								window.location.href=base_url+'My_credit_debit_notes/manage_credit_debit_notes';
								Materialize.toast('Credit / Debit Note has been successfully activated', 2000,'green rounded');
							}
							else
							{
								window.location.href=base_url+'My_credit_debit_notes/manage_credit_debit_notes';
								Materialize.toast('Error while processing!', 2000,'red rounded');
							}
						},

					});										
			});*/
				var notes = [];
				var notes_values = "";
				$(".cn_bulk_action").on('click', function(e) {
					if($(this).is(':checked')){
						notes= [];
						var total_checked = $(".cn_bulk_action:checked").length;
						  /*if (jQuery.inArray(notes, $(this).val())== '-1') {
							notes.push($(this).val());
						  } */
					    $(".cn_bulk_action:checked").each(function() {
			
							notes.push($(this).val());
						});
					}
					else
					{
						notes=$('#deactive_multiple_notes').attr('data-multi_note').split(',');
						$(this).prop('checked',false);
						var remove_id = $(this).val();	
						notes = jQuery.grep(notes, function(value) {
						  return value != remove_id;
						});
					}
						notes_values = notes.join(",");
						$('#deactive_multiple_notes').attr('data-multi_note',notes_values);
						$('#download_multiple_notes').attr('data-multi_download',notes_values);
					});

				var debit_notes = [];
				var debit_notes_values = "";
				$(".debit_bulk_action").on('click', function(e) {
					if($(this).is(':checked')){
						debit_notes= [];
						var total_checked = $(".debit_bulk_action:checked").length;
						  /*if (jQuery.inArray(notes, $(this).val())== '-1') {
							notes.push($(this).val());
						  } */
					    $(".debit_bulk_action:checked").each(function() {
			
							debit_notes.push($(this).val());
						});
					}
					else
					{
						debit_notes=$('#deactive_multiple_notes').attr('data-multi_note').split(',');
						$(this).prop('checked',false);
						var remove_id = $(this).val();	
						debit_notes = jQuery.grep(debit_notes, function(value) {
						  return value != remove_id;
						});
					}
						debit_notes_values = debit_notes.join(",");
						$('#deactive_multiple_notes').attr('data-multi_note',debit_notes_values);
						$('#download_multiple_notes').attr('data-multi_download',debit_notes_values);
					});
						
		 },
	  }); 
	}

function mySalesReturn(url,id)
	{
	$('#'+id).DataTable().destroy();
 	oTable = $('#'+id).dataTable({
	 "bServerSide": true,
	"bProcessing": true,
	"bDeferRender": true,
    "aoColumnDefs": [{ "bSortable": false, "aTargets": [0,1,2,3,4,5,6,7,8]}],
	 "aoColumns": [	
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
	],
	
   "order": [0,'asc'],
    "bPaginate": true,
    "bLengthChange": false,
    "bFilter": true,
    "bInfo": false,
    "searching":true,
	"iDisplayLength": 10,
	"aLengthMenu": [10, 20, 40],
	"aaSorting": [[ 1, "desc" ]],

		"ajax": {
			"url": url,
			"type": "POST",
			//"data": {'search':$("#search").val()},
		},
	"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
		if($(aData[5]+' div').hasClass('deactive_record')){
			$('td', nRow).parent('tr').addClass('deactivate-row');
		}

		$('td:first-child', nRow).addClass('bulk');
		
		$('td:last-child', nRow).addClass('action-tab');
     },
	 "fnDrawCallback": function () {
		  $('.dropdown-button, .translation-button, .dropdown-menu').dropdown({
						  inDuration: 300,
						  outDuration: 225,
						  constrainWidth: false,
						  hover: true,
						  gutter: 0,
						  belowOrigin: true,
						  alignment: 'left',
						  stopPropagation: false
						});
			
			$('.active_return').off().on('click',function(){

				var service_id = $(this).data('rtn_id');

				 $('#active_deactive_sales_rtn').modal('open');

				 $('#active_deactive_sales_rtn #active_service_id').val(service_id);
				 $('#active_deactive_sales_rtn .sstatus_type').text('activate');
				 $('#active_deactive_sales_rtn #sstatus_type').text('activate');

			});						
			$('.deactive_service').off().on('click', function(){

				var service_id = $(this).data('service_id');

				 $('#active_deactive_sales_rtn').modal('open');

				 $('#active_deactive_sales_rtn #active_service_id').val(service_id);
				 $('#active_deactive_sales_rtn .sstatus_type').text('remove');
				 $('#active_deactive_sales_rtn #sstatus_type').text('deactivate');
			});

			$('.deactive_sales_return').off().on('click',function(){
                  var rtn_id =$(this).data('rtn_id');
                  $.ajax({
                    url:base_url+'sales/deactive_sales_return/' + rtn_id,
                    type:"POST",
                    data:{'csrf_test_name':csrf_hash,"rtn_id":rtn_id,},
                    success:function(res){
                        if(res == true)
                        {
                          Materialize.toast('Deleted Successfully', 5000,'green rounded');
						  $('select').material_select();
                          mySalesReturn(base_path()+'sales/get_sales_return/','my-sales-return');
                        }
                        else
                        {
                          mySalesReturn(base_path()+'sales/get_sales_return/','my-sales-return');
						  $('select').material_select();
                          Materialize.toast('Error while processing. Please try again', 5000,'red rounded');
                        }
                      },
                    });                   
                });
									
				var services = [];
				var services_values = "";
				$(".services_bulk_action").on("click", function() {
					if($(this).is(':checked')){
						var total_checked = $(".services_bulk_action:checked").length;
						  if (jQuery.inArray(services, $(this).val())== '-1') {
							services.push($(this).val());
						  } 
					}
					else
					{
						var remove_id = $(this).val();	
						services = jQuery.grep(services, function(value) {
						  return value != remove_id;
						});
					}
						services_values = services.join(",");
						$('#deactive_multiple_services').attr('data-multi_services',services_values);
					});
			
	 },
  }); 
}

function billSupply(url,id)
{
	$('#'+id).DataTable().destroy();
 	oTable = $('#'+id).dataTable({
	 "bServerSide": true,
	"bProcessing": true,
	"bDeferRender": true,
    "aoColumnDefs": [{ "bSortable": false, "aTargets": [0,1,2,3,4,5,6,7]}],
	 "aoColumns": [	
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
	],
	
   "order": [0,'asc'],
    "bPaginate": true,
    "bLengthChange": false,
    "bFilter": true,
    "bInfo": false,
    "searching":true,
	"iDisplayLength": 10,
	"aLengthMenu": [10, 20, 40],
	"aaSorting": [[ 1, "desc" ]],

		"ajax": {
			"url": url,
			"type": "POST",
			//"data": {'search':$("#search").val()},
		},
	"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
		if($(aData[5]+' div').hasClass('deactive_record')){
			$('td', nRow).parent('tr').addClass('deactivate-row');
		}

		$('td:first-child', nRow).addClass('bulk');
		
		$('td:last-child', nRow).addClass('action-tab');
     },
	 "fnDrawCallback": function () {
		  $('.dropdown-button, .translation-button, .dropdown-menu').dropdown({
						  inDuration: 300,
						  outDuration: 225,
						  constrainWidth: false,
						  hover: true,
						  gutter: 0,
						  belowOrigin: true,
						  alignment: 'left',
						  stopPropagation: false
						});
			
			$('.active_bill_supply').off().on('click',function(){

				var service_id = $(this).data('rtn_id');


				 $('#active_deactive_bill_supply').modal('open');

				 $('#active_deactive_bill_supply #active_service_id').val(service_id);
				 $('#active_deactive_bill_supply .sstatus_type').text('activate');
				 $('#active_deactive_bill_supply #sstatus_type').text('activate');

			});						
			$('.deactive_bill_supply').off().on('click', function(){

				var service_id = $(this).data('service_id');

				 $('#active_deactive_bill_supply').modal('open');

				 $('#active_deactive_sales_rtn #active_service_id').val(service_id);
				 $('#active_deactive_sales_rtn .sstatus_type').text('remove');
				 $('#active_deactive_sales_rtn #sstatus_type').text('deactivate');
			});

			$('.deactive_bill_supply').off().on('click',function(){
                  var bos_id =$(this).data('bos_id');
                  $.ajax({
                    url:base_url+'sales/deactive_bill_supply/' + bos_id,
                    type:"POST",
                    data:{'csrf_test_name':csrf_hash,"bos_id":bos_id,},
                    success:function(res){
                        if(res == true)
                        {
                          Materialize.toast('Deleted Successfully', 5000,'green rounded');
						  $('select').material_select();
                          billSupply(base_path()+'sales/get_bill_supply/','my-bill-supply');
                        }
                        else
                        {
                          billSupply(base_path()+'sales/get_bill_supply/','my-bill-supply');
						  $('select').material_select();
                          Materialize.toast('Error while processing. Please try again', 5000,'red rounded');
                        }
                      },
                    });                   
                });
									
				var services = [];
				var services_values = "";
				$(".services_bulk_action").on("click", function() {
					if($(this).is(':checked')){
						var total_checked = $(".services_bulk_action:checked").length;
						  if (jQuery.inArray(services, $(this).val())== '-1') {
							services.push($(this).val());
						  } 
					}
					else
					{
						var remove_id = $(this).val();	
						services = jQuery.grep(services, function(value) {
						  return value != remove_id;
						});
					}
						services_values = services.join(",");
						$('#deactive_multiple_services').attr('data-multi_services',services_values);
					});
			
	 },
  }); 
}

function deliveryChalanDatatable(url,id)
{
 		$('#'+id).DataTable().destroy();
 		table_delivery_challlan = $('#'+id).dataTable({	
		"bServerSide": true,
		"bProcessing": true,
		"bDeferRender": true,
		"aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0 , 2 , 3, 4, 5, 6, 7] }],
		 "aoColumns": [	
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			//{"bVisible": false},
			{ "bSortable": false },
			//{ "bSortable": false },
			
		],
		
		"order": [0,'asc'],
		"bPaginate": true,
		"bLengthChange": false,
		"bFilter": true,
		"bInfo": false,
		"searching":true,
		"iDisplayLength": 20,
		lengthMenu: [
            [ 20, 30, 50, -1 ],
            [ 'Show 20', 'Show 30', 'Show 50', 'Show all' ]
        ],

		"aaSorting": [[ 1, "desc" ]],
		"ajax": {
			"url": url,
			"type": "POST",
			"data": {'csrf_test_name':csrf_hash,'search_by_customer':$("#search_by_dccustomer").val()},
		},
		"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
	
			if($(aData[12]+' div').hasClass('deactive_record')){
				$('td', nRow).parent('tr').addClass('deactivate-row');
			}
		
			$('td:first-child', nRow).addClass('bulk');
			
			$('td:last-child', nRow).addClass('action-tab');
		 },
		 "fnDrawCallback": function () {
			  $('.dropdown-button, .translation-button, .dropdown-menu').dropdown({
							  inDuration: 300,
							  outDuration: 225,
							  constrainWidth: false,
							  hover: true,
							  gutter: 0,
							  belowOrigin: true,
							  alignment: 'left',
							  stopPropagation: false
							});
		
			
			$('.deactive_delivery_challan').off().on('click', function(){
				var dc_id = $(this).data('dc_id');
				$('#deactivate_dc').modal('open');
				$('#deactivate_dc #deactivate_dc_id').val(dc_id);
			});
			
			$('.remove_dc').off().on('click', function(){

				var dc_id = $('#deactivate_dc_id').val();
			
				$.ajax({

					url:base_url+'sales/deactive_delivery_challan',

					type:"POST",

					data:{'csrf_test_name':csrf_hash,"dc_id":dc_id,"status":"Inactive"},

					success:function(res){
							if(res == true)
							{
								deliveryChalanDatatable(base_path()+'sales/get_delivery_challan/','my-delivery-challan');
								$('select').material_select();
								Materialize.toast('Delivery Challan has been successfully deactivated', 2000,'green rounded');
							}
							else
							{
								deliveryChalanDatatable(base_path()+'sales/get_delivery_challan/','my-delivery-challan');
								$('select').material_select();
								Materialize.toast('Error while processing!', 2000,'red rounded');
							}
						},

					});										
			});
			
			/*$('.activate_dc').off().on('click',function(){
				var dc_id = $(this).data('dc_id');;
				 $('#activate_delivery_challan').modal('open');
				 $('#activate_delivery_challan #activate_dc_id').val(dc_id);
			});
			
			$('.active_dc').off().on('click', function(){
				var dc_id = $('#activate_dc_id').val();
				$.ajax({
					url:base_url+'My_delivery_challan/activate_delivery_challan',
					type:"POST",
					data:{"dc_id":dc_id,},
					success:function(res){
							if(res == true)
							{
								window.location.href=base_url+'My_delivery_challan/manage_delivery_challan';
								Materialize.toast('Delivery Challan has been successfully activated', 2000,'green rounded');
							}
							else
							{
								window.location.href=base_url+'My_delivery_challan/manage_delivery_challan';
								Materialize.toast('Error while processing!', 2000,'red rounded');
							}
						},

					});										
			});*/
					var challans = [];
					var challans_values = "";
					$(".dc_bulk_action").on('click', function(e) {
						if($(this).is(':checked')){
							var total_checked = $(".dc_bulk_action:checked").length;
							  if (jQuery.inArray(challans, $(this).val())== '-1') {
								challans.push($(this).val());
							  } 
						}
						else
						{
							var remove_id = $(this).val();	
							challans = jQuery.grep(challans, function(value) {
							  return value != remove_id;
							});
						}
							challans_values = challans.join(",");
							$('#deactive_multiple_dc').attr('data-multi_dc',challans_values);
						});
						
		 },
	  }); 
	}

/*  -------------ADVANCE RECEIPT----------*/
function advanceReceipt(url,id)
{

	$('#'+id).DataTable().destroy();
 	oTable = $('#'+id).dataTable({
	 "bServerSide": true,
	"bProcessing": true,
	"bDeferRender": true,
    "aoColumnDefs": [{ "bSortable": false, "aTargets": [0,1,2,3,4,5,6,7]}],
	 "aoColumns": [	
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
	],
	
   "order": [0,'asc'],
    "bPaginate": true,
    "bLengthChange": false,
    "bFilter": true,
    "bInfo": false,
    "searching":true,
	"iDisplayLength": 10,
	"aLengthMenu": [10, 20, 40],
	"aaSorting": [[ 1, "desc" ]],

		"ajax": {
			"url": url,
			"type": "POST",
			"data": {'csrf_test_name':csrf_hash,'search_by_customer':$("#search_by_customer_ar").val()},
			"data": {'csrf_test_name':csrf_hash,'search_by_status':$("#search_by_ar_status").val()},
		},
	"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {

		if($(aData[3]+' span').hasClass('red-c')){
					
			$('td:nth-child(4)', nRow).addClass('red-c');
		}else{
			$('td:nth-child(4)', nRow).addClass('green-c');
		}

		if($(aData[5]+' div').hasClass('deactive_record')){
			$('td', nRow).parent('tr').addClass('deactivate-row');
		}
		$('td:nth-child(5)', nRow).addClass('green-c');
		$('td:first-child', nRow).addClass('bulk');
		
		$('td:last-child', nRow).addClass('action-tab');
     },
	 "fnDrawCallback": function () {
		  $('.dropdown-button, .translation-button, .dropdown-menu').dropdown({
						  inDuration: 300,
						  outDuration: 225,
						  constrainWidth: false,
						  hover: true,
						  gutter: 0,
						  belowOrigin: true,
						  alignment: 'left',
						  stopPropagation: false
						});
			
			$('.active_advance_receipts').off().on('click',function(){

				var service_id = $(this).data('rtn_id');


				 $('#active_deactive_advance_recepits').modal('open');

				 $('#active_deactive_advance_recepits #active_service_id').val(service_id);
				 $('#active_deactive_advance_recepits .sstatus_type').text('activate');
				 $('#active_deactive_advance_recepits #sstatus_type').text('activate');

			});						
			$('.deactive_advance_receipts').off().on('click', function(){

				var service_id = $(this).data('service_id');

				 $('#active_deactive_advance_recepits').modal('open');

				 $('#active_deactive_advance_recepits #active_service_id').val(service_id);
				 $('#active_deactive_advance_recepits .sstatus_type').text('remove');
				 $('#active_deactive_advance_recepits #sstatus_type').text('deactivate');
			});

			$('.deactive_advance_receipts').off().on('click',function(){
                  var ar_id =$(this).data('ar_id');
                  $.ajax({
                    url:base_url+'sales/deactive_advance_receipts/' + ar_id,
                    type:"POST",
                    data:{'csrf_test_name':csrf_hash,"ar_idar_id":ar_id,},
                    success:function(res){
                        if(res == true)
                        {
                          Materialize.toast('Deleted Successfully', 5000,'green rounded');
                          advanceReceipt(base_path()+'sales/get_advance_receipts/','my-advance-receipts');
						  $('select').material_select();
                        }
                        else
                        {
                          advanceReceipt(base_path()+'sales/get_advance_receipts/','my-advance-receipts');
						  $('select').material_select();
                          Materialize.toast('Error while processing. Please try again', 5000,'red rounded');
                        }
                      },
                    });                   
                });
									
				var services = [];
				var services_values = "";
				$(".services_bulk_action").on("click", function() {
					if($(this).is(':checked')){
						var total_checked = $(".services_bulk_action:checked").length;
						  if (jQuery.inArray(services, $(this).val())== '-1') {
							services.push($(this).val());
						  } 
					}
					else
					{
						var remove_id = $(this).val();	
						services = jQuery.grep(services, function(value) {
						  return value != remove_id;
						});
					}
						services_values = services.join(",");
						$('#deactive_multiple_services').attr('data-multi_services',services_values);
					});
			
	 },
  }); 

}


function simpleDataTableInit(id)
{
	oTable = $('#'+id).dataTable( {
		"processing": true,
		"serverSide": false,
		"scrollX": true,
		aoColumnDefs: [
		  {
			 bSortable: false,
			 aTargets: [ -1 ]
		  }
		]
	} );
}
	