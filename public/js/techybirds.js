$("#add_personal_profile").validate({

				rules:{

					reg_username:{

						required:true,

					},

					reg_email:{

						required:true,

						email:true,

					},

					contact_mobile:{

						required:true,

						number:true,

						minlength:10,

						maxlength:10,

					},

					reg_password: {

						required:true,

						minlength:8,

					},

					contact_confirm_password: {

						required:true,

						minlength:8,

						equalTo: '#reg_password',

					},

					reg_admin_type:{

						required:true,

					},
				},

				messages:{

					contact_name:{

						required:"Contact Name is required",

					},

					contact_email:{

						required:"Email ID is required",

						email:"Please Enter Valid Email ID",

					},

					contact_mobile:{

						required:"Mobile Number is required",

						number:"Please Enter only digits",

						minlength:"Please Enter Mobile No like (e.g 0123456789)",

						maxlength:"Please Enter valid 10 digits Mobile Number",

					},

					contact_password: {

						required:"Password is required.",

						minlength:"Password must contain at least 8 characters.",

					},

					contact_confirm_password: {

						required:"Confirm Password is required.",

						minlength:"Password must contain at least 8 characters.",

						equalTo:"Password Does Not Matched..!!",

					},

					access:{

						required:"Please Allow any access to New User",

					},

				},

			});
			
			 $("#edit_user").validate({

				rules:{

					contact_name:{

						required:true,

					},

					contact_email:{

						required:true,

						email:true,

					},

					contact_mobile:{

						required:true,

						number:true,

						minlength:10,

						maxlength:10,

					},

					contact_password: {

						required:true,

						minlength:8,

					},

					contact_confirm_password: {

						required:true,

						minlength:8,

						equalTo: '#contact_password',

					},

					access:{

						required:true,

					},
				},

				messages:{

					contact_name:{

						required:"Contact Name is required",

					},

					contact_email:{

						required:"Email ID is required",

						email:"Please Enter Valid Email ID",

					},

					contact_mobile:{

						required:"Mobile Number is required",

						number:"Please Enter only digits",

						minlength:"Please Enter Mobile No like (e.g 0123456789)",

						maxlength:"Please Enter valid 10 digits Mobile Number",

					},

					contact_password: {

						required:"Password is required.",

						minlength:"Password must contain at least 8 characters.",

					},

					contact_confirm_password: {

						required:"Confirm Password is required.",

						minlength:"Password must contain at least 8 characters.",

						equalTo:"Password Does Not Matched..!!",

					},

					access:{

						required:"Please Allow any access to New User",

					},

				},

			});
			
			
			$("#otp_check").validate({

				rules:{

					otp:{

						required:true,

					},
				},

				messages:{

					otp:{

						required:"OTP is required",

					},
				},

			});
			
			$('.deactive_profile_2').on('click',function(){

				var profile = $(this).data('profileid');

				 $('#deactive_profile_2').modal('open');

				 $('#deactive_profile_2 #personal_profile_id_2').val(profile);

			});

			$('.deactive_user_2').on('click',function(){

				var profile_id = $('#personal_profile_id_2').val();
				$.ajax({

					url:base_url+'Personal_profile/deactive_profile_2',

					type:"POST",

					data:{'csrf_test_name':csrf_hash,"profile_id":profile_id,},

					success:function(res){
							//	console.log(res);
							window.location.href=base_url+'Personal_profile/manage_profile';

						},

					});										
			});

			$('.active_profile_2').on('click',function(){

				var profile = $(this).data('profileid');

				 $('#active_profile_2').modal('open');



				 $('#active_profile_2 #activate_personal_profile_id_2').val(profile);

			});

			$('.active_user_2').on('click',function(){

				var profile_id = $('#activate_personal_profile_id_2').val();
				$.ajax({

					url:base_url+'Personal_profile/activate_profile_2',

					type:"POST",

					data:{'csrf_test_name':csrf_hash,"profile_id":profile_id,},

					success:function(res){

							window.location.href=base_url+'Personal_profile/manage_profile';

						},

					});										

			});
			
			
			$("#change_password_frm").submit(function(e){
				e.preventDefault();
			}).validate({

				rules:{

					old_password:{
						required:true,
						minlength:8,
					},

					new_password:{
						required:true,
						minlength:8,
					},
					confirm_password:{
						required:true,
						equalTo: '#new_password',
					},
				},

				messages:{

					old_password:{
						required:"Old Password is required",
						minlength:"Please Enter Minimum 8 characters long password",
					},

					new_password:{
						required:"New Password is required",
						minlength:"Please Enter Minimum 8 characters long password",
					},

					confirm_password:{
						required:"Confirm your new password",
						equalTo:"Password Does Not Matched..!!",
					},
				},
				submitHandler:function(form){
						var frm=$(form).serialize();
								 //$(form).ajaxSubmit();
								$.ajax({
								url:base_url+'Dashboard/change_password',
								type:"POST",
								data:{'csrf_test_name':csrf_hash,"frm":frm,},
								success:function(res){
										if(res == true)
										{
											 $('#pass-succ').modal({
												dismissible: false, // Modal can be dismissed by clicking outside of the modal
											  });
					  
											 $('#pass-succ').modal('open');
										}
										else
										{
											$("#change_password_frm #old_password").val('');
											$("#change_password_frm #new_password").val('');
											$("#change_password_frm #confirm_password").val('');
											
											$('#change_pass_error').modal('open');											
										}
									},
					});
				},

			});
			
			$("#otp_password_frm").submit(function(e){
				e.preventDefault();
			}).validate({

				rules:{

					password_otp:{
						required:true,
						minlength:6,
						maxlength:6,
					},
				},

				messages:{

					password_otp:{
						required:"OTP is required",
						minlength:"Please Enter 6 digit OTP code",
						maxlength:"Please Enter only 6 digit OTP code",
					},
				},
				submitHandler:function(form){
						var frm=$(form).serialize();
								 //$(form).ajaxSubmit();
								$.ajax({
								url:base_url+'Dashboard/check_otp_for_password',
								type:"POST",
								data:{'csrf_test_name':csrf_hash,"frm":frm,},
								success:function(res){
								
										if(res == true)
										{
											location.reload();
										}
										else
										{
											$("#otp_password_frm #password_otp").val('');
											$('#otp_error').modal('open');
											
										}
									},						
					});
				},
			});

			$('.show_password_chkbox').on('change',function(){

				$('.show_password').attr('type','Password');

			  var isChecked = $(this).prop('checked');

			  //console.log(isChecked);

			  if (isChecked) {

				$('.show_password').attr('type','text');

			  } else {

				$('.show_password').attr('type','Password');

			  }

			});
			
					
			$('.add_gst').on('click',function(){
				$('#add_gst_frm').modal('open');
			});
			
			$("#add_gst_from").submit(function(e){
				e.preventDefault();
			}).validate({

				rules:{
					'gstin[]':{
						required:true,
						gstregex:true,
					},
					'location[]':{
						required:true,
					},
				},

				messages:{

					'gstin[]':{
						required:"GST Number is required",
						gstregex:"Enter GSTIN in a valid format",
					},
					'location[]':{
						required:"Place of supply is required",
					},
				},
				submitHandler:function(form){
						var frm=$(form).serialize();
								 //$(form).ajaxSubmit();
								$.ajax({
								url:base_url+'Company_profile/add_gstin_info',
								type:"POST",
								data:{'csrf_test_name':csrf_hash,"frm":frm,},
								success:function(res){
									if(res!= false)
									{
										var data=JSON.parse(res);
										
										var html_1 ='';
										var final_html ='';
										if(data != '')
										{
											for(var i=0;i<data.length;i++){
												if(data[i].status=="Inactive"){
													   var deac="deactive_record";
												   }else{
													    var deac="";
												   }
												   console.log(deac);
											if(i == 0)
											   {
												html_1='<div class="gst-boxs add-gst '+deac+'"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">GST No.</span><p>'+data[i].gst_no+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">PLACE OF SUPPLY</span><p>'+data[i].place_of_supply+'</p></div><div class="col l1 s1 m1 gstaction"><a class="remove_cmp_gst_model" data-location="'+data[i].place_of_supply+'" data-gst_no="'+data[i].gst_no+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a></div></div></div>';
											   }
											   else
											   {
												 html_1='<div class="gstmore"><div class="gst-boxs add-gst '+deac+'"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">GST No.</span><p>'+data[i].gst_no+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">PLACE OF SUPPLY</span><p>'+data[i].place_of_supply+'</p></div><div class="col l1 s1 m1 gstaction"><a class="remove_cmp_gst_model" data-location="'+data[i].place_of_supply+'" data-gst_no="'+data[i].gst_no+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a></div></div></div></div>';
											  
											   }
												 final_html = final_html + html_1;
												 $("#company_gst_array_list").html(final_html);
												 $(".tot_gst").html(data.length);
											}
											Materialize.toast('GSTIN Details have been set.', 2000,'green rounded');
										}
										else
										{
											//html_1='<div class="gst-boxs add-gst">Add GSTIN Data</div>';
											//$("#company_gst_array_list").html(html_1);
										}
									}
									else
									{
										Materialize.toast('Error while processing. Please try again', 2000,'red rounded');
									}
									$("#add_gst_from").find("input[type=text], textarea").val("");
									$("#add_gst_frm").modal('close');
										
								$(document).on('click','.remove_cmp_gst_model',function(){
									
									var gst_no = $(this).data('gst_no');
									var location = $(this).data('location');
									
									$('#remove_company_gst_data').modal('open');
									$('#remove_company_gst_data #gst_no').val(gst_no);
									$('#remove_company_gst_data #location').val(location);
									
								});
			
									$('.remove_company_gst_data').off().on('click',function(){
						
										var gst_no = $('#gst_no').val();
										var location = $('#location').val();
						
										$.ajax({
						
											url:base_url+'Company_profile/remove_company_gst_info',
						
											type:"post",
						
											data:{'csrf_test_name':csrf_hash,"gst_no":gst_no,"location":location},
						
											success:function(res){
													if(res!= false)
													{
														var data=JSON.parse(res);
														var html_1 ='';
														var final_html ='';
														if(data != '')
														{
															for(var i=0;i<data.length;i++){
															if(i == 0)
															   {
																html_1='<div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
															   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">GST No.</span><p>'+data[i].gst_no+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">PLACE OF SUPPLY</span><p>'+data[i].place_of_supply+'</p></div><div class="col l1 s1 m1 gstaction"><a class="remove_cmp_gst_model" data-location="'+data[i].place_of_supply+'" data-gst_no="'+data[i].gst_no+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a></div></div></div>';
															   }
															   else
															   {
																 html_1='<div class="gstmore"><div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
															   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">GST No.</span><p>'+data[i].gst_no+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">PLACE OF SUPPLY</span><p>'+data[i].place_of_supply+'</p></div><div class="col l1 s1 m1 gstaction"><a class="remove_cmp_gst_model" data-location="'+data[i].place_of_supply+'" data-gst_no="'+data[i].gst_no+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a></div></div></div></div>';
															  
															   }
																 final_html = final_html + html_1;
																 $("#company_gst_array_list").html(final_html);
																 $(".tot_gst").html(data.length);
															}
															Materialize.toast('GSTIN Details deactivated', 2000,'green rounded');
														}
														else
														{
															//html_1='<div class="gst-boxs add-gst">Add GSTIN Data</div>';
															//$("#company_gst_array_list").html(html_1);
															$(".tot_gst").html(0);
														}
													}
													else
													{
														//html_1='<div class="gst-boxs add-gst">Add GSTIN Data</div>';
														//$("#company_gst_array_list").html(html_1);
														$(".tot_gst").html(0);
													}
												},
						
											});										
									});
									
									},
						
					});
				},

			});
			
									
			$("#add_company_profile").validate({

				rules:{

					company_name:{

						required:true,
					},

					state:{

						required:true,
					},

					pan_num:{

						required:true,
						pannoregex:true,
					},
				},

				messages:{

					company_name:{

						required:"Company Name is required",

					},

					state:{

						required:"State is required",
					},

					pan_num:{

						required:"PAN Number is required",
						pannoregex:"Enter PAN no in a valid format",
					},
				},
			});
		
			$('.show_social').hide();
			$('.edit_social_media').on('click',function(){
				$('.show_social').toggle();
			});
			
			$("#edit_company_profile").validate({

				rules:{

					company_name:{

						required:true,
					},

					state:{

						required:true,
					},

					pan_num:{

						required:true,
						pannoregex:true,
					},
				},

				messages:{

					company_name:{

						required:"Company Name is required",

					},

					state:{

						required:"State is required",
					},

					pan_num:{

						required:"PAN Number is required",
						pannoregex:"Enter PAN no in a valid format",
					},
				},
			});
	
	$(document).ready(function () {		
			$(document).on('click','.edit_gst_info',function(){
				
				var id = $(this).data('id');
				var userid = $(this).data('userid');
				var companyid = $(this).data('companyid');
				var gst_no = $(this).data('gst_no');
				var place = $(this).data('gst_place');
				var status = $(this).data('status');
				
				$('#edit_company_gstmodal').modal('open');
				$('#edit_company_gstmodal #cmp_gst_id').val(id);
				$('#edit_company_gstmodal #cmp_id_edit_gst').val(companyid);
				$('#edit_company_gstmodal #cmp_user_id').val(userid);
				$('#edit_company_gstmodal #gst_no_edit').val(gst_no);
				$('#edit_company_gstmodal #gst_edit_location').val(place);
				$('#edit_company_gstmodal #status').val(status);
			});
			
			$("#edit_gst_from").submit(function(e){
				e.preventDefault();
			}).validate({

				rules:{
					gst_no_edit:{
						required:true,
						gstregex:true,
					},
					gst_edit_location:{
						required:true,
					},
				},
				messages:{
					gst_no_edit:{
						required:"GST Number is required",
						gstregex:"Enter GSTIN in a valid format",
					},
					gst_edit_location:{
						required:"GST Percentage is required",
					},
				},
				submitHandler:function(form){
						var frm=$(form).serialize();
								$.ajax({
								url:base_url+'Company_profile/edit_gstin_info',
								type:"POST",
								data:{'csrf_test_name':csrf_hash,"frm":frm,},
								success:function(res){
										if(res!= false)
										{
											var data=JSON.parse(res);
											var html_1 ='';
											var final_html ='';
											if(data != '')
											{
												for(var i=0;i<data.length;i++){
													if(data[i].status=="Inactive"){
													   var deac="deactive_record";
												   }else{
													    var deac="";
												   }
												if(i == 0)
												   {
													html_1='<div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
												   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum '+deac+'"><span class="gstlabel">GST No.</span><p>'+data[i].gst_no+'</p></div><div class="col l6 s6 m6 gstplace '+deac+'"><span class="gstlabel">PLACE OF SUPPLY</span><p>'+data[i].place_of_supply+'</p></div><div class="col l1 s1 m1 gstaction"><a class="edit_remove_cmpgst '+deac+'" data-id="'+data[i].id+'" data-userid="'+data[i].user_id+'" data-companyid="'+data[i].company_id+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a><a class="edit_gst_info" data-id="'+data[i].id+'" data-userid="'+data[i].user_id+'" data-companyid="'+data[i].company_id+'" data-gst_no="'+data[i].gst_no+'" data-gst_place="'+data[i].place_of_supply+'" data-status="'+data[i].status+'"><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div>';
												   }
												   else
												   {
													 html_1='<div class="gstmore"><div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
												   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum '+deac+'"><span class="gstlabel">GST No.</span><p>'+data[i].gst_no+'</p></div><div class="col l6 s6 m6 gstplace '+deac+'"><span class="gstlabel">PLACE OF SUPPLY</span><p>'+data[i].place_of_supply+'</p></div><div class="col l1 s1 m1 gstaction"><a class="edit_remove_cmpgst '+deac+'" data-id="'+data[i].id+'" data-userid="'+data[i].user_id+'" data-companyid="'+data[i].company_id+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a><a class="edit_gst_info" data-id="'+data[i].id+'" data-userid="'+data[i].user_id+'" data-companyid="'+data[i].company_id+'" data-gst_no="'+data[i].gst_no+'" data-gst_place="'+data[i].place_of_supply+'" data-status="'+data[i].status+'"><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div></div>';
												  
												   }
													 final_html = final_html + html_1;
													 $("#company_gst_array_list_edit").html(final_html);
													 $(".tot_gst").html(data.length);
												}
												Materialize.toast('GSTIN Details has been updated.', 2000,'green rounded');
											}
											else
											{
												//html_1='<div class="gst-boxs add-gst">Add GSTIN Data</div>';
												//$("#company_gst_array_list_edit").html(html_1);
											}
										}
										else
										{
											Materialize.toast('Error while processing. Please try again', 2000,'red rounded');
										}
										$("#edit_gst_from").find("input[type=text], textarea").val("");
										$("#edit_company_gstmodal").modal('close');
									},
					});
				},
				});
			});
	
	$(document).ready( function() {	
			$('.add_gst_2').on('click',function(){
				$('#add_gst_frm_2').modal('open');
				var userid = $(this).data('user_id');
				var companyid = $(this).data('company_id');
				
				$('#add_gst_frm_2 #company_id_gst_2').val(companyid);
				$('#add_gst_frm_2 #user_id_gst_2').val(userid);
				
			});
			$("#add_gst_from_2").submit(function(e){
				e.preventDefault();
			}).validate({
				rules:{
					'gstin[]':{
						required:true,
						gstregex:true,
					},
					'location[]':{
						required:true,
					},
				},
				messages:{

					'gstin[]':{
						required:"GST Number is required",
						gstregex:"Enter GSTIN in a valid format",
					},
					'location[]':{
						required:"Place of supply is required",
					},
				},
				submitHandler:function(form){
						var frm=$(form).serialize();
								$.ajax({
								url:base_url+'Company_profile/add_another_gstin_info',
								type:"post",
								data:{'csrf_test_name':csrf_hash,"frm":frm,},
								success:function(res){
										if(res!= false)
										{
											var data=JSON.parse(res);
											var html_1 ='';
											var final_html ='';
											if(data != '')
											{
												for(var i=0;i<data.length;i++){
													if(data[i].status=="Inactive"){
													   var deac="deactive_record";
												   }else{
													    var deac="";
												   }
												if(i == 0)
												   {
													html_1='<div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
												   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum '+deac+'"><span class="gstlabel">GST No.</span><p>'+data[i].gst_no+'</p></div><div class="col l6 s6 m6 gstplace '+deac+'"><span class="gstlabel">PLACE OF SUPPLY</span><p>'+data[i].place_of_supply+'</p></div><div class="col l1 s1 m1 gstaction"><a class="edit_remove_cmpgst '+deac+'" data-location="'+data[i].place_of_supply+'" data-gst_no="'+data[i].gst_no+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a><a class="edit_gst_info" data-id="'+data[i].id+'" data-userid="'+data[i].user_id+'" data-companyid="'+data[i].company_id+'" data-gst_no="'+data[i].gst_no+'" data-gst_place="'+data[i].place_of_supply+'" data-status="'+data[i].status+'"><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div>';
												   }
												   else
												   {
													 html_1='<div class="gstmore"><div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
												   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum '+deac+'"><span class="gstlabel">GST No.</span><p>'+data[i].gst_no+'</p></div><div class="col l6 s6 m6 gstplace '+deac+'"><span class="gstlabel">PLACE OF SUPPLY</span><p>'+data[i].place_of_supply+'</p></div><div class="col l1 s1 m1 gstaction"><a class="edit_remove_cmpgst '+deac+'" data-location="'+data[i].place_of_supply+'" data-gst_no="'+data[i].gst_no+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a><a class="edit_gst_info" data-id="'+data[i].id+'" data-userid="'+data[i].user_id+'" data-companyid="'+data[i].company_id+'" data-gst_no="'+data[i].gst_no+'" data-gst_place="'+data[i].place_of_supply+'" data-status="'+data[i].status+'"><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div></div>';
												  
												   }
													 final_html = final_html + html_1;
													 $("#company_gst_array_list_edit").html(final_html);
													 $(".tot_gst").html(data.length);
												}
												Materialize.toast('GSTIN Details has been added.', 2000,'green rounded');
											}
											else
											{
												//html_1='<div class="gst-boxs add-gst">Add GSTIN Data</div>';
												//$("#company_gst_array_list_edit").html(html_1);
											}
										}
										else
										{
											Materialize.toast('Error while processing. Please try again', 2000,'red rounded');
										}
										$("#add_gst_from_2").find("input[type=text], textarea").val("");
										$("#add_gst_frm_2").modal('close');
							},
					});
				},
			});
		});	
			//My_Customers
			$("#add_customer").validate({

				rules:{

					company_name:{

						required:true,

					},

					pan_no:{

						required:true,

						pannoregex:true,

					},

					state:{
						required:true,
					},
				},

				messages:{

					company_name:{

						required:"Company Name is required",

					},

					pan_no:{

						required:"PAN Number is required",

						pannoregex:"Enter PAN no in a valid format",

					},

					state:{

						required:"Billing State is required",

					},

				},

			});
			
			$("#add_customer_gstin").submit(function(e){
				e.preventDefault();
			}).validate({
				rules:{

					'gstin[]':{
						required:true,
						gstregex:true,
						gstreg:true,
					},
					'location[]':{
						required:true,
					},
				},
				messages:{

					gstin:{
						required:"GST Number is required",
						gstregex:"Enter GSTIN in a valid format",
					},
					location:{
						required:"Place of supply is required",
					},
				},
				submitHandler:function(form){
						var frm=$(form).serialize();
								$.ajax({
								url:base_url+'My_customers/add_gst_info',
								type:"POST",
								data:{'csrf_test_name':csrf_hash,"frm":frm,},
								success:function(res){
									if(res!= false)
									{
										var data=JSON.parse(res);
										var html_1 ='';
										var final_html ='';
										if(data != '')
										{
											for(var i=0;i<data.length;i++){
												if(data[i].status=="Inactive"){
													   var deac="deactive_record";
												   }else{
													    var deac="";
												   }
												   console.log(deac);
											if(i == 0)
											   {
												html_1='<div class="gst-boxs add-gst '+deac+'"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">GST No.</span><p>'+data[i].gst_no+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">PLACE OF SUPPLY</span><p>'+data[i].place_of_supply+'</p></div><div class="col l1 s1 m1 gstaction"><a href="#" class="remove_gst_model" data-location="'+data[i].place_of_supply+'" data-gst_no="'+data[i].gst_no+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a></div></div></div>';
											   }
											   else
											   {
												 html_1='<div class="gstmore"><div class="gst-boxs add-gst '+deac+'"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">GST No.</span><p>'+data[i].gst_no+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">PLACE OF SUPPLY</span><p>'+data[i].place_of_supply+'</p></div><div class="col l1 s1 m1 gstaction"><a href="#" class="remove_gst_model" data-location="'+data[i].place_of_supply+'" data-gst_no="'+data[i].gst_no+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a></div></div></div></div>';
											  
											   }
												 final_html = final_html + html_1;
												 $("#gst_array_list").html(final_html);
												 $(".tot_gst").html(data.length);
											}
										}
										else
										{
											//html_1='<div class="gst-boxs add-gst">Add GSTIN Data</div>';
											//$("#gst_array_list").html(html_1);
										}
									}
									else
									{
										Materialize.toast('Error while processing. Please try again', 2000,'red rounded');
									}
									$("#add_customer_gstin").find("input[type=text], textarea").val("");
									$("#gstmodal").modal('close');
									
							
							$(document).ready( function() {		
								$(document).on('click','.remove_gst_model',function(){
									
									var gst_no = $(this).data('gst_no');
									var location = $(this).data('location');
									
									$('#remove_gst_data').modal('open');
									$('#remove_gst_data #gst_no').val(gst_no);
									$('#remove_gst_data #location').val(location);
									
								});
			
									$('.remove_gst_data').off().on('click',function(){
						
										var gst_no = $('#gst_no').val();
										var location = $('#location').val();
						
										$.ajax({
						
											url:base_url+'My_customers/remove_gst_info',
						
											type:"post",
						
											data:{'csrf_test_name':csrf_hash,"gst_no":gst_no,"location":location},
						
											success:function(res){
													if(res!= false)
													{
														var data=JSON.parse(res);
														var html_1 ='';
														var final_html ='';
														if(data != '')
														{
															for(var i=0;i<data.length;i++){
																if(data[i].status=="Inactive"){
													   var deac="deactive_record";
												   }else{
													    var deac="";
												   }
												   console.log(deac);
															if(i == 0)
															   {
																html_1='<div class="gst-boxs add-gst '+deac+'"><div class="col l1 s1 m1 sno"><div class="row">'+
															   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">GST No.</span><p>'+data[i].gst_no+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">PLACE OF SUPPLY</span><p>'+data[i].place_of_supply+'</p></div><div class="col l1 s1 m1 gstaction"><a href="#" class="remove_gst_model" data-location="'+data[i].place_of_supply+'" data-gst_no="'+data[i].gst_no+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a></div></div></div>';
															   }
															   else
															   {
																 html_1='<div class="gstmore"><div class="gst-boxs add-gst '+deac+'"><div class="col l1 s1 m1 sno"><div class="row">'+
															   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">GST No.</span><p>'+data[i].gst_no+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">PLACE OF SUPPLY</span><p>'+data[i].place_of_supply+'</p></div><div class="col l1 s1 m1 gstaction"><a href="#" class="remove_gst_model" data-location="'+data[i].place_of_supply+'" data-gst_no="'+data[i].gst_no+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a></div></div></div></div>';
															  
															   }
																 final_html = final_html + html_1;
																 $("#gst_array_list").html(final_html);
																 $(".tot_gst").html(data.length);
															}
															Materialize.toast('GSTIN Details deactivated', 2000,'green rounded');
														}
														else
														{
															Materialize.toast('GSTIN Details deactivated', 2000,'green rounded');
															//html_1='<div class="gst-boxs add-gst">Add GSTIN Data</div>';
															//$("#gst_array_list").html(html_1);
															$(".tot_gst").html(0);
														}
													}
													else
														{
															Materialize.toast('GSTIN Details deactivated', 2000,'green rounded');
															//html_1='<div class="gst-boxs add-gst">Add GSTIN Data</div>';
															//$("#gst_array_list").html(html_1);
															$(".tot_gst").html(0);
														}
												},
						
											});										
									});
									});
							},
							
					});
				},
			});
			
			$("#add_contactperson").submit(function(e){
				e.preventDefault();
			}).validate({
				rules:{

					name:{
						required:true,
					},
					mobile:{
						required:true,
					},
					email:{
						required:true,
						email:true
					},
					password:{
						required:true,
						minlength:8,
						maxlength:12
					},
				},
				messages:{

					name:{
						required:"Name is required",
					},
					mobile:{
						required:"Mobile Number is required",
					},
					email:{
						required:"Email ID is required",
						email:'Enter valid Email ID',
					},
					password:{
						required:"Password is required",
						minlength:'Enter minimum 8 characters for password.',
						maxlength:"Maximum 12 character long password allowed.",
					},
				},
				submitHandler:function(form){
						var frm=$(form).serialize();
								$.ajax({
								url:base_url+'My_customers/add_contact_person_info',
								type:"POST",
								data:{'csrf_test_name':csrf_hash,"frm":frm,},
								success:function(res){
									if(res!= false)
									{
										var data=JSON.parse(res);
										var html_person ='';
										var final_html_1 ='';
										if(data != '')
										{
											for(var i=0;i<data.length;i++){
											if(i == 0)
											{
												html_person='<div class="row"><div class="contact-person-box"><div class="col l1 s1 m1"><input type="radio" id="contact-person" checked name="default"><label class="fill-green-radio" for="contact-person-1"></label></div><div class="col l10 s10 m10 content-area"><div class="col l3 s3 m3"><div class="gradient-wrapper"><div class="maincircle">100%</div></div></div><div class="col l9 s9 m9"><p class="name"><strong>'+data[i].name+'</strong></p><p class="email">'+data[i].email+'</p></div></div><div class="col l1 s1 m1"><a href="#" class="remove_contact_person" data-name="'+data[i].name+'" data-email="'+data[i].email+'"><i class="material-icons">close</i></a></div></div></div>';
												
											   }
											   else if(i == 1)
											   {
												  html_person='<div class="row"><div class="contact-person-box"><div class="col l1 s1 m1"><input type="radio" id="contact-person'+(i+1)+'" name="default"><label class="fill-green-radio" for="contact-person'+(i+1)+'"></label></div><div class="col l10 s10 m10 content-area"><div class="col l3 s3 m3"><div class="gradient-wrapper"><div class="maincircle">100%</div></div></div><div class="col l9 s9 m9"><p class="name"><strong>'+data[i].name+'</strong></p><p class="email">'+data[i].email+'</p></div></div><div class="col l1 s1 m1"><a href="#" class="remove_contact_person" data-name="'+data[i].name+'" data-email="'+data[i].email+'"><i class="material-icons">close</i></a></div></div></div>'; 
												   
											   }
											   else
											   {
												  html_person='<div class="contactmore"><div class="row"><div class="contact-person-box"><div class="col l1 s1 m1"><input type="radio" id="contact-person'+(i+1)+'" name="default"><label class="fill-green-radio" for="contact-person'+(i+1)+'"></label></div><div class="col l10 s10 m10 content-area"><div class="col l3 s3 m3"><div class="gradient-wrapper"><div class="maincircle">100%</div></div></div><div class="col l9 s9 m9"><p class="name"><strong>'+data[i].name+'</strong></p><p class="email">'+data[i].email+'</p></div></div><div class="col l1 s1 m1"><a href="#" class="remove_contact_person" data-name="'+data[i].name+'" data-email="'+data[i].email+'"><i class="material-icons">close</i></a></div></div></div></div>'; 
											  
											   }
												 final_html_1 = final_html_1 + html_person;
												 $(".person_list").html(final_html_1);
												 $(".tot_person").html(data.length);
											}
											
										}
										else
										{
											html_person='<div class="gst-boxs add-gst">Add Contact Person Data</div>';
											$(".person_list").html(html_person);
											$(".tot_person").html(0);
										}
									}
									else
									{
										Materialize.toast('Error while processing. Please try again', 2000,'red rounded');
									}
									$("#add_contactperson").find("input[type=text],input[type=password], textarea").val("");
									$("#contactmodal").modal('close');
									
					
						$(document).ready( function() {		
								$(document).on('click','.remove_contact_person',function(){				
								//$('.remove_contact_person').on('click',function(){
									
									var name = $(this).data('name');
									var email = $(this).data('email');
									$('#remove_contact_person_model').modal('open');
									$('#remove_contact_person_model #contact_name').val(name);
									$('#remove_contact_person_model #contact_email').val(email);
									
								});
			
									$('.remove_person').on('click',function(){
						
										var name = $('#contact_name').val();
										var email = $('#contact_email').val();
						
										$.ajax({
						
											url:base_url+'My_customers/remove_contact_person',
						
											type:"post",
						
											data:{'csrf_test_name':csrf_hash,"name":name,"email":email},
						
											success:function(res){
													if(res!= false)
													{
														var data=JSON.parse(res)
														var html_1 ='';
														var final_html ='';
														if(data != '')
														{
															for(var i=0;i<data.length;i++){
															if(i == 0)
															{
																html_person='<div class="row"><div class="contact-person-box"><div class="col l1 s1 m1"><input type="radio" id="contact-person" checked name="default"><label class="fill-green-radio" for="contact-person-1"></label></div><div class="col l10 s10 m10 content-area"><div class="col l3 s3 m3"><div class="gradient-wrapper"><div class="maincircle">100%</div></div></div><div class="col l9 s9 m9"><p class="name"><strong>'+data[i].name+'</strong></p><p class="email">'+data[i].email+'</p></div></div><div class="col l1 s1 m1"><a href="#" class="remove_contact_person" data-name="'+data[i].name+'" data-email="'+data[i].email+'"><i class="material-icons">close</i></a></div></div></div>';
																
															   }
															   else if(i == 1)
															   {
																  html_person='<div class="row"><div class="contact-person-box"><div class="col l1 s1 m1"><input type="radio" id="contact-person'+(i+1)+'" name="default"><label class="fill-green-radio" for="contact-person'+(i+1)+'"></label></div><div class="col l10 s10 m10 content-area"><div class="col l3 s3 m3"><div class="gradient-wrapper"><div class="maincircle">100%</div></div></div><div class="col l9 s9 m9"><p class="name"><strong>'+data[i].name+'</strong></p><p class="email">'+data[i].email+'</p></div></div><div class="col l1 s1 m1"><a href="#" class="remove_contact_person" data-name="'+data[i].name+'" data-email="'+data[i].email+'"><i class="material-icons">close</i></a></div></div></div>'; 
																   
															   }
															   else
															   {
																  html_person='<div class="contactmore"><div class="row"><div class="contact-person-box"><div class="col l1 s1 m1"><input type="radio" id="contact-person'+(i+1)+'" name="default"><label class="fill-green-radio" for="contact-person'+(i+1)+'"></label></div><div class="col l10 s10 m10 content-area"><div class="col l3 s3 m3"><div class="gradient-wrapper"><div class="maincircle">100%</div></div></div><div class="col l9 s9 m9"><p class="name"><strong>'+data[i].name+'</strong></p><p class="email">'+data[i].email+'</p></div></div><div class="col l1 s1 m1"><a href="#" class="remove_contact_person" data-name="'+data[i].name+'" data-email="'+data[i].email+'"><i class="material-icons">close</i></a></div></div></div></div>'; 
															  
															   }
															   Materialize.toast('Contact details removed', 2000,'green rounded');
																 final_html_1 = final_html_1 + html_person;
																 $(".person_list").html(final_html_1);
																 $(".tot_person").html(data.length);
															}
														}
														else
														{
															Materialize.toast('Contact details removed', 2000,'green rounded');
															html_person='<div class="gst-boxs add-gst">Add Contact Person Data</div>';
															$(".person_list").html(html_person);
															$(".tot_person").html("0");
														}
													}
													else
													{
														html_person='<div class="gst-boxs add-gst">Add Contact Person Data</div>';
														$(".person_list").html(html_person);
														$(".tot_person").html("0");
													}
												},
						
											});										
									});
									});
							},
							
					});
				},
			});
			
			
			$(".cancel_personal_profile").on("click",function(){
				window.location.href=base_url+'Personal_profile/manage_profile';	
			});
			
		/*$('input[type="checkbox"]').click(function(){
		//$(document).on("click","#same_bill",function(){
			//$("#same_bill").on("click",function(){
					var country = $("#country").val();
					var state = $("#state").val();
					var city = $("#city").val();
				if($(this).is(':checked')){
					$("#shipping_address").parents('.full-bg-label').addClass('active');
					$("#shipping_address").val($("#billing_address").val());
					$("#shipping_pincode").parents('.full-bg-label').addClass('active');
					$("#shipping_pincode").val($("#pincode").val());
					$("#shipping_country").parents('.input-field').addClass('label-active');
					$('#shipping_country option[value="'+country+'"]').attr('selected','selected');
					$('#shipping_country').material_select();
					$.ajax({
						url:base_url+'Company_profile/get_states',
						type:"POST",
						data:{'country_id':$("#country").val()},
						success:function(res){
							$("#shipping_state").html(res);
							$("#shipping_state").parents('.input-field').addClass('label-active');
							$('#shipping_state option[value="'+state+'"]').attr('selected',true);
							$('#shipping_state').material_select();
						},
					});
					$.ajax({
						url:base_url+'Company_profile/get_cities',
						type:"POST",
						data:{'state_id':$("#state").val()},
						success:function(res){
							$("#shipping_city").html(res);
							$("#shipping_city").parents('.input-field').addClass('label-active');
							$('#shipping_city option[value="'+city+'"]').attr('selected',true);
							$('#shipping_city').material_select();
						},
					});
					//console.log($("#shipping_state").val());
				}
				else{
					$("#shipping_pincode").val("");
					$("#shipping_address").val("");
					$("#shipping_country").parents('.input-field').removeClass('label-active');
					$('#shipping_country option[value="'+country+'"]').attr('selected',false);
					$("#shipping_state").parents('.input-field').removeClass('label-active');
					$('#shipping_state option[value="'+state+'"]').attr('selected',false);
					$("#shipping_city").parents('.input-field').removeClass('label-active');
					$('#shipping_city option[value="'+city+'"]').attr('selected',false);
					$('.shipping_values input').val("");
				}
			});*/
		
		$('input[name="same-bill-customer"]').click(function(){
		//$(document).on("click","#same_bill",function(){
			//$("#same_bill").on("click",function(){
					var country = $("#new_cust_country").val();
					var state = $("#new_cust_state").val();
					var city = $("#new_cust_city").val();
				if($(this).is(':checked')){
					$("#new_cust_ship_address").parents('.full-bg-label').addClass('active');
					$("#new_cust_ship_address").val($("#new_cust_billing_address").val());
					$("#new_cust_ship_pincode").parents('.full-bg-label').addClass('active');
					$("#new_cust_ship_pincode").val($("#new_cust_pincode").val());
					$("#new_cust_ship_country").parents('.input-field').addClass('label-active');
					$('#new_cust_ship_country option[value="'+country+'"]').attr('selected','selected');
					$('#new_cust_ship_country').material_select();
					$.ajax({
						url:base_url+'Company_profile/get_states',
						type:"POST",
						data:{'csrf_test_name':csrf_hash,'country_id':$("#new_cust_country").val()},
						success:function(res){
							$("#new_cust_ship_state").html(res);
							$("#new_cust_ship_state").parents('.input-field').addClass('label-active');
							$('#new_cust_ship_state option[value="'+state+'"]').attr('selected',true);
							$('#new_cust_ship_state').material_select();
						},
					});
					$.ajax({
						url:base_url+'Company_profile/get_cities',
						type:"POST",
						data:{'csrf_test_name':csrf_hash,'state_id':$("#new_cust_state").val()},
						success:function(res){
							$("#new_cust_ship_city").html(res);
							$("#new_cust_ship_city").parents('.input-field').addClass('label-active');
							$('#new_cust_ship_city option[value="'+city+'"]').attr('selected',true);
							$('#new_cust_ship_city').material_select();
						},
					});
					//console.log($("#shipping_state").val());
				}
				else{
					$("#new_cust_ship_pincode").val("");
					$("#new_cust_ship_address").val("");
					$("#new_cust_ship_country").parents('.input-field').removeClass('label-active');
					$('#new_cust_ship_country option[value="'+country+'"]').attr('selected',false);
					$("#new_cust_ship_state").parents('.input-field').removeClass('label-active');
					$('#new_cust_ship_state option[value="'+state+'"]').attr('selected',false);
					$("#new_cust_ship_city").parents('.input-field').removeClass('label-active');
					$('#new_cust_ship_city option[value="'+city+'"]').attr('selected',false);
					$('.shipping_values input').val("");
				}
			});
		$(document).ready(function () {	
			
			$(document).on('click' , '.edit_cust_gst', function(){
				$('#edit_gstmodal').modal('open');
				var gst_id = $(this).data('gst_id');
				var gst_no = $(this).data('gst_no');
				var location = $(this).data('location');
				var cust_id = $(this).data('cust_id');
				
				$('#edit_gstmodal #gst_id').val(gst_id);
				$('#edit_gstmodal #cust_id_edit_gst').val(cust_id);
				$('#edit_gstmodal #location').val(location);
				$('#edit_gstmodal #gst_no_edit').val(gst_no);
			});
			
			$("#edit_customer_gstin").submit(function(e){
				e.preventDefault();
			}).validate({
				rules:{

					gst_no_edit:{
						required:true,
						gstregex:true,
					},
					location:{
						required:true,
					},
				},
				messages:{

					gst_no_edit:{
						required:"GST Number is required",
						gstregex:"Enter GSTIN in a valid format",
					},
					location:{
						required:"Place of supply is required",
					},
				},
				submitHandler:function(form){
						var frm=$(form).serialize();
								$.ajax({
								url:base_url+'My_customers/edit_gst_info',
								type:"POST",
								data:{'csrf_test_name':csrf_hash,"frm":frm,},
								success:function(res){
									//console.log(res);
									if(res!= false)
									{
										var data=JSON.parse(res);
										var html_1 ='';
										var final_html ='';
										if(data != '')
										{
											for(var i=0;i<data.length;i++){
											if(i == 0)
											   {
												html_1='<div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">GST No.</span><p>'+data[i].gst_no+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">PLACE OF SUPPLY</span><p>'+data[i].place_of_supply+'</p></div><div class="col l1 s1 m1 gstaction"><a href="#" class="edit_remove_gst_data" data-cust_id="'+data[i].cust_id+'" data-gst_id="'+data[i].id+'" data-location="'+data[i].place_of_supply+'" data-gst_no="'+data[i].gst_no+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a><a href="#" class="edit_cust_gst" data-cust_id="'+data[i].cust_id+'" data-gst_id="'+data[i].id+'" data-gst_no="'+data[i].gst_no+'" data-location="'+data[i].place_of_supply+'"><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div>';
											   }
											   else
											   {
												 html_1='<div class="gstmore"><div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">GST No.</span><p>'+data[i].gst_no+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">PLACE OF SUPPLY</span><p>'+data[i].place_of_supply+'</p></div><div class="col l1 s1 m1 gstaction"><a href="#" class="edit_remove_gst_data" data-cust_id="'+data[i].cust_id+'" data-gst_id="'+data[i].id+'" data-location="'+data[i].place_of_supply+'" data-gst_no="'+data[i].gst_no+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a><a href="#" class="edit_cust_gst" data-cust_id="'+data[i].cust_id+'" data-gst_id="'+data[i].id+'" data-gst_no="'+data[i].gst_no+'" data-location="'+data[i].place_of_supply+'"><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div></div>';
											  
											   }
												 final_html = final_html + html_1;
												 $("#gst_array_list_edit").html(final_html);
												 $(".tot_gst").html(data.length);
											}
											 Materialize.toast('GST Info updated successfully !', 2000,'green rounded');
										}
										else
										{
											//html_1='<div class="gst-boxs add-gst">Add GSTIN Data</div>';
											//$("#gst_array_list").html(html_1);
											 Materialize.toast('ADD GSTIN DATA!', 2000,'green rounded');
										}
									}
									else
									{
										Materialize.toast('Error while processing. Please try again', 2000,'red rounded');
									}
									$("#edit_customer_gstin").find("input[type=text], textarea").val("");
									$("#edit_gstmodal").modal('close');
									
							},
							
					});
				},
			});
		});
			$('.editpage_gstmodal_cls').on('click' ,function(){

				var cust_id = $(this).data('cust_id');
				
				$('#editpage_gstmodal').modal('open');
				$('#editpage_gstmodal #cust_id').val(cust_id);
				
			});
			
			$("#editpage_customer_gstin").submit(function(e){
				e.preventDefault();
			}).validate({
				rules:{

					'gstin[]':{
						required:true,
						gstregex:true,
					},
					'location[]':{
						required:true,
					},
					'billing_address1':{
						required:true,
					},
					'country1':{
						required:true,
					},
					'state1':{
						required:true,
					},
					'city1':{
						required:true,
					},
					'pincode1':{
						required:true,
					},
				},
				messages:{

					gstin:{
						required:"GST Number is required",
						gstregex:"Enter GSTIN in a valid format",
					},
					location:{
						required:"Place of supply is required",
					},
					billing_address1:{
						required:"Billing Address is required",
					},
					country1:{
						required:"Country is required",
					},
					state1:{
						required:"State is required",
					},
					city1:{
						required:"City is required",
					},
					pincode1:{
						required:"Pin Code is required",
					},
				},
				submitHandler:function(form){
						var frm=$(form).serialize();
								$.ajax({
								url:base_url+'My_customers/add_gst_info_2',
								type:"POST",
								data:{'csrf_test_name':csrf_hash,"frm":frm,},
								success:function(res){
									if(res!= false)
									{
										var data=JSON.parse(res);
										var html_1 ='';
										var final_html ='';
										if(data != '')
										{
											for(var i=0;i<data.length;i++){
												if(data[i].status=="Inactive"){
													   var deac="deactive_record";
												   }else{
													    var deac="";
												   }
												   console.log(deac);
											if(i == 0)
											   {
												html_1='<div class="gst-boxs add-gst '+deac+'"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">GST No.</span><p>'+data[i].gst_no+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">PLACE OF SUPPLY</span><p>'+data[i].place_of_supply+'</p></div><div class="col l1 s1 m1 gstaction"><a href="#" class="edit_remove_gst_data" data-cust_id="'+data[i].cust_id+'" data-gst_id="'+data[i].id+'" data-location="'+data[i].place_of_supply+'" data-gst_no="'+data[i].gst_no+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a><a href="#" class="edit_cust_gst" data-cust_id="'+data[i].cust_id+'" data-gst_id="'+data[i].id+'" data-gst_no="'+data[i].gst_no+'" data-location="'+data[i].place_of_supply+'"><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div>';
											   }
											   else
											   {
												 html_1='<div class="gstmore"><div class="gst-boxs add-gst '+deac+'"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">GST No.</span><p>'+data[i].gst_no+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">PLACE OF SUPPLY</span><p>'+data[i].place_of_supply+'</p></div><div class="col l1 s1 m1 gstaction"><a href="#" class="edit_remove_gst_data" data-location="'+data[i].place_of_supply+'" data-cust_id="'+data[i].cust_id+'" data-gst_id="'+data[i].id+'" data-gst_no="'+data[i].gst_no+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a><a href="#" class="edit_cust_gst" data-cust_id="'+data[i].cust_id+'" data-gst_id="'+data[i].id+'" data-gst_no="'+data[i].gst_no+'" data-location="'+data[i].place_of_supply+'"><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div></div>';
											  
											   }
												 final_html = final_html + html_1;
												 $("#gst_array_list_edit").html(final_html);
												 $(".tot_gst").html(data.length);
											}
											Materialize.toast('GSTIN Info inserted successfully!', 2000,'green rounded');
										}
										else
										{
											//html_1='<div class="gst-boxs add-gst">Add GSTIN Data</div>';
											//$("#gst_array_list_edit").html(html_1);
											$(".tot_gst").html(0);
											Materialize.toast('ADD GSTIN DATA', 2000,'green rounded');
										}
									}
									else
									{
										Materialize.toast('Error while processing. Please try again', 2000,'red rounded');
									}
									$("#editpage_customer_gstin").find("input[type=text], textarea").val("");
									$("#editpage_gstmodal").modal('close');
							},
							
					});
				},
			});
							$(document).ready( function()  {
								$("#create_invoice #company_name").val($("#header_company_profiles").val());
								$(document).on('click', '.edit_remove_gst_data' ,function(){
									
									var gst_id = $(this).data('gst_id');
									var cust_id = $(this).data('cust_id');
									var gst_no = $(this).data('gst_no');
									var location = $(this).data('location');
									$('#editpage_remove_gst_data').modal('open');
									$('#editpage_remove_gst_data #remove_gst_no').val(gst_no);
									$('#editpage_remove_gst_data #remove_location').val(location);
									$('#editpage_remove_gst_data #remove_gst_id').val(gst_id);
									$('#editpage_remove_gst_data #remove_cust_id').val(cust_id);
									
								});
								
								$('.edit_remove_gst').on('click',function(){
						
										var gst_id = $('#remove_gst_id').val();
										var cust_id = $('#remove_cust_id').val();

										$.ajax({
						
											url:base_url+'My_customers/delete_gst_info',
						
											type:"post",
						
											data:{'csrf_test_name':csrf_hash,"gst_id":gst_id,"cust_id":cust_id},
						
											success:function(res){
													if(res!= false)
													{
														var data=JSON.parse(res);
														var html_1 ='';
														var final_html ='';
														if(data != '')
														{
															for(var i=0;i<data.length;i++){
																if(data[i].status=="Inactive"){
													   var deac="deactive_record";
												   }else{
													    var deac="";
												   }
												   console.log(deac);
															if(i == 0)
															   {
																html_1='<div class="gst-boxs add-gst '+deac+'"><div class="col l1 s1 m1 sno"><div class="row">'+
															   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">GST No.</span><p>'+data[i].gst_no+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">PLACE OF SUPPLY</span><p>'+data[i].place_of_supply+'</p></div><div class="col l1 s1 m1 gstaction"><a href="#" class="edit_remove_gst_data" data-cust_id="'+data[i].cust_id+'" data-gst_id="'+data[i].id+'" data-location="'+data[i].place_of_supply+'" data-gst_no="'+data[i].gst_no+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a><a href="#" class="edit_cust_gst" data-cust_id="'+data[i].cust_id+'" data-gst_id="'+data[i].id+'" data-gst_no="'+data[i].gst_no+'" data-location="'+data[i].place_of_supply+'"><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div>';
															   }
															   else
															   {
																 html_1='<div class="gstmore"><div class="gst-boxs add-gst '+deac+'"><div class="col l1 s1 m1 sno"><div class="row">'+
															   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">GST No.</span><p>'+data[i].gst_no+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">PLACE OF SUPPLY</span><p>'+data[i].place_of_supply+'</p></div><div class="col l1 s1 m1 gstaction"><a href="#" class="edit_remove_gst_data" data-cust_id="'+data[i].cust_id+'" data-gst_id="'+data[i].id+'" data-location="'+data[i].place_of_supply+'" data-gst_no="'+data[i].gst_no+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a><a href="#" class="edit_cust_gst" data-cust_id="'+data[i].cust_id+'" data-gst_id="'+data[i].id+'" data-gst_no="'+data[i].gst_no+'" data-location="'+data[i].place_of_supply+'"><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div></div>';
															  
															   }
																 final_html = final_html + html_1;
																 $("#gst_array_list_edit").html(final_html);
																 $(".tot_gst").html(data.length);
															}
															Materialize.toast('GST Info removed successfully !', 2000,'green rounded');
														}
														else
														{
															//html_1='<div class="gst-boxs add-gst">Add GSTIN Data</div>';
															//$("#gst_array_list_edit").html(html_1);
															Materialize.toast('ADD GSTIN DATA!', 2000,'green rounded');
															$(".tot_gst").html(0);
														}
													}
													else
													{
														//html_1='<div class="gst-boxs add-gst">Add GSTIN Data</div>';
														//$("#gst_array_list_edit").html(html_1);
														Materialize.toast('Error while processing!', 2000,'red rounded');
													}
												},
						
											});										
									});
								});	
									$('.edit_contactmodal').on('click' ,function(){
									
									var id = $(this).data('id');
									var name = $(this).data('name');
									var cust_id = $(this).data('cust_id');
									var mobile = $(this).data('mobile');
									var email = $(this).data('email');
									var birthday = $(this).data('birthday');
									var birthday_remainder = $(this).data('birthday_remainder');
									var anniversary = $(this).data('anniversary');
									var anniversary_reminder = $(this).data('anniversary_reminder');
									var is_admin = $(this).data('is_admin');
									var password = $(this).data('password');
				
									$('#edit_contactmodal').modal('open');
									$('#edit_contactperson #contact_id').val(id);
									$('#edit_contactperson #edit_name').val(name);
									$('#edit_contactperson #edit_mobile').val(mobile);
									$('#edit_contactperson #edit_email').val(email);
									$('#edit_contactperson #edit_birthday').val(birthday);
									//$('#edit_contactperson #edit_birthday_reminder option[value='+birthday_remainder+']').attr('selected','selected');
									$('#edit_contactperson #edit_anniversary').val(anniversary);
									//$('#edit_contactperson #edit_anniversary_reminder option[value='+anniversary_reminder+']').attr('selected','selected');
									if(is_admin == 1){
										$('#edit_contactperson #primary').prop('checked', true);
									}
									$('#edit_contactperson #remove_cust_id').val(is_admin);
									$('#edit_contactperson #edit_cust_id').val(cust_id);
									$('#edit_contactperson #edit_old_password').val(password);
									
								});
								
								
		$("#edit_contactperson").submit(function(e){
				e.preventDefault();
			}).validate({
				rules:{

					name:{
						required:true,
					},
					mobile:{
						required:true,
					},
					email:{
						required:true,
						email:true
					},
					/*password:{
						required:true,
						minlength:8,
						maxlength:12
					},*/
				},
				messages:{

					name:{
						required:"Name is required",
					},
					mobile:{
						required:"Mobile Number is required",
					},
					email:{
						required:"Email ID is required",
						email:'Enter valid Email ID',
					},
					/*password:{
						required:"Password is required",
						minlength:'Enter minimum 8 characters for password.',
						maxlength:"Maximum 12 character long password allowed.",
					},*/
				},
				submitHandler:function(form){
						var frm=$(form).serialize();
								$.ajax({
								url:base_url+'My_customers/edit_contact_person_info',
								type:"POST",
								data:{'csrf_test_name':csrf_hash,"frm":frm,},
								success:function(res){
									if(res!= false)
									{
										var data=JSON.parse(res);
										var html_person ='';
										var final_html_1 ='';
										if(data != '')
										{
											for(var i=0;i<data.length;i++){
											if(i == 0)
											{
												html_person='<div class="row"><div class="contact-person-box"><div class="col l1 s1 m1"><input type="radio" id="contact-person" checked name="default"><label class="fill-green-radio" for="contact-person-1"></label></div><div class="col l10 s10 m10 content-area"><div class="col l3 s3 m3"><div class="gradient-wrapper"><div class="maincircle">100%</div></div></div><div class="col l9 s9 m9"><p class="name"><strong>'+data[i].name+'</strong></p><p class="email">'+data[i].email+'</p></div></div><div class="col l1 s1 m1"><a href="#" class="remove_contact_person_editpage" data-cust_id="'+data[i].cust_id+'" data-id="'+data[i].id+'"><i class="material-icons">close</i></a><a href="#" class="edit_contactmodal"  data-id="'+data[i].id+'" data-cust_id="'+data[i].cust_id+'" data-name="'+data[i].name+'" data-mobile="'+data[i].mobile+'" data-email="'+data[i].email+'" data-password="'+data[i].password+'" data-birthday="'+data[i].birthday+'" data-birthday_remainder="'+data[i].birthday_remainder+'" data-anniversary="'+data[i].anniversary+'" data-anniversary_reminder="'+data[i].anniversary_reminder+'" data-is_admin="'+data[i].is_admin+'"><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div>';
												
											   }
											   else if(i == 1)
											   {
												  html_person='<div class="row"><div class="contact-person-box"><div class="col l1 s1 m1"><input type="radio" id="contact-person'+(i+1)+'" name="default"><label class="fill-green-radio" for="contact-person'+(i+1)+'"></label></div><div class="col l10 s10 m10 content-area"><div class="col l3 s3 m3"><div class="gradient-wrapper"><div class="maincircle">100%</div></div></div><div class="col l9 s9 m9"><p class="name"><strong>'+data[i].name+'</strong></p><p class="email">'+data[i].email+'</p></div></div><div class="col l1 s1 m1"><a href="#" class="remove_contact_person_editpage" data-id="'+data[i].id+'" data-cust_id="'+data[i].cust_id+'"><i class="material-icons">close</i></a><a href="#" class="edit_contactmodal"  data-id="'+data[i].id+'" data-cust_id="'+data[i].cust_id+'" data-name="'+data[i].name+'" data-mobile="'+data[i].mobile+'" data-email="'+data[i].email+'" data-password="'+data[i].password+'" data-birthday="'+data[i].birthday+'" data-birthday_remainder="'+data[i].birthday_remainder+'" data-anniversary="'+data[i].anniversary+'" data-anniversary_reminder="'+data[i].anniversary_reminder+'" data-is_admin="'+data[i].is_admin+'"><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div>'; 
												   
											   }
											   else
											   {
												  html_person='<div class="contactmore"><div class="row"><div class="contact-person-box"><div class="col l1 s1 m1"><input type="radio" id="contact-person'+(i+1)+'" name="default"><label class="fill-green-radio" for="contact-person'+(i+1)+'"></label></div><div class="col l10 s10 m10 content-area"><div class="col l3 s3 m3"><div class="gradient-wrapper"><div class="maincircle">100%</div></div></div><div class="col l9 s9 m9"><p class="name"><strong>'+data[i].name+'</strong></p><p class="email">'+data[i].email+'</p></div></div><div class="col l1 s1 m1"><a href="#" class="remove_contact_person_editpage" data-id="'+data[i].id+'" data-cust_id="'+data[i].cust_id+'"><i class="material-icons">close</i></a><a href="#" class="edit_contactmodal"  data-id="'+data[i].id+'" data-cust_id="'+data[i].cust_id+'" data-name="'+data[i].name+'" data-mobile="'+data[i].mobile+'" data-email="'+data[i].email+'" data-password="'+data[i].password+'" data-birthday="'+data[i].birthday+'" data-birthday_remainder="'+data[i].birthday_remainder+'" data-anniversary="'+data[i].anniversary+'" data-anniversary_reminder="'+data[i].anniversary_reminder+'" data-is_admin="'+data[i].is_admin+'"><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div></div>'; 
											  
											   }
												 final_html_1 = final_html_1 + html_person;
												 $(".person_list").html(final_html_1);
												 $(".tot_person").html(data.length);
											}
											Materialize.toast('Contact Details has been successfully updated!', 2000,'green rounded');
										}
										else
										{
											html_person='<div class="gst-boxs add-gst">Add Contact Person Data</div>';
											$(".person_list").html(html_person);
											Materialize.toast('Add contact information', 2000,'green rounded');
										}
									}
									else
									{
										Materialize.toast('Error while processing. Please try again', 2000,'red rounded');
									}
									$("#edit_contactperson").find("input[type=text],input[type=password], textarea").val("");
									$("#edit_contactmodal").modal('close');
			
							},
							
					});
				},
			});
			
								$(document).on('click', '.remove_contact_person_editpage', function(){
									
									var id = $(this).data('id');
									var cust_id = $(this).data('cust_id');
									
									$('#remove_contact_person_model_editpage').modal('open');
									$('#remove_contact_person_model_editpage #remove_contact_id').val(id);
									$('#remove_contact_person_model_editpage #remove_contact_cust_id').val(cust_id);
									
								});
								
									$('.remove_person_edit').on('click',function(){
						
										var id = $('#remove_contact_id').val();
										var cust_id = $('#remove_contact_cust_id').val();
						
										$.ajax({
						
											url:base_url+'My_customers/delete_contact_info',
						
											type:"post",
						
											data:{'csrf_test_name':csrf_hash,"id":id,"cust_id":cust_id},
						
											success:function(res){
													if(res!= false)
													{
														var data=JSON.parse(res)
														var html_1 ='';
														var final_html_1 ='';
														if(data != '')
														{
															for(var i=0;i<data.length;i++){
															if(i == 0)
															{
																html_person='<div class="row"><div class="contact-person-box"><div class="col l1 s1 m1"><input type="radio" id="contact-person" checked name="default"><label class="fill-green-radio" for="contact-person-1"></label></div><div class="col l10 s10 m10 content-area"><div class="col l3 s3 m3"><div class="gradient-wrapper"><div class="maincircle">100%</div></div></div><div class="col l9 s9 m9"><p class="name"><strong>'+data[i].name+'</strong></p><p class="email">'+data[i].email+'</p></div></div><div class="col l1 s1 m1"><a href="#" class="remove_contact_person_editpage" data-cust_id="'+data[i].cust_id+'" data-id="'+data[i].id+'"><i class="material-icons">close</i></a><a href="#" class="edit_contactmodal"  data-id="'+data[i].id+'" data-cust_id="'+data[i].cust_id+'" data-name="'+data[i].name+'" data-mobile="'+data[i].mobile+'" data-email="'+data[i].email+'" data-password="'+data[i].password+'" data-birthday="'+data[i].birthday+'" data-birthday_remainder="'+data[i].birthday_remainder+'" data-anniversary="'+data[i].anniversary+'" data-anniversary_reminder="'+data[i].anniversary_reminder+'" data-is_admin="'+data[i].is_admin+'"><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div>';
																
															   }
															   else if(i == 1)
															   {
																  html_person='<div class="row"><div class="contact-person-box"><div class="col l1 s1 m1"><input type="radio" id="contact-person'+(i+1)+'" name="default"><label class="fill-green-radio" for="contact-person'+(i+1)+'"></label></div><div class="col l10 s10 m10 content-area"><div class="col l3 s3 m3"><div class="gradient-wrapper"><div class="maincircle">100%</div></div></div><div class="col l9 s9 m9"><p class="name"><strong>'+data[i].name+'</strong></p><p class="email">'+data[i].email+'</p></div></div><div class="col l1 s1 m1"><a href="#" class="remove_contact_person_editpage" data-cust_id="'+data[i].cust_id+'" data-id="'+data[i].id+'"><i class="material-icons">close</i></a><a href="#" class="edit_contactmodal"  data-id="'+data[i].id+'" data-cust_id="'+data[i].cust_id+'" data-name="'+data[i].name+'" data-mobile="'+data[i].mobile+'" data-email="'+data[i].email+'" data-password="'+data[i].password+'" data-birthday="'+data[i].birthday+'" data-birthday_remainder="'+data[i].birthday_remainder+'" data-anniversary="'+data[i].anniversary+'" data-anniversary_reminder="'+data[i].anniversary_reminder+'" data-is_admin="'+data[i].is_admin+'"><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div>'; 
																   
															   }
															   else
															   {
																  html_person='<div class="contactmore"><div class="row"><div class="contact-person-box"><div class="col l1 s1 m1"><input type="radio" id="contact-person'+(i+1)+'" name="default"><label class="fill-green-radio" for="contact-person'+(i+1)+'"></label></div><div class="col l10 s10 m10 content-area"><div class="col l3 s3 m3"><div class="gradient-wrapper"><div class="maincircle">100%</div></div></div><div class="col l9 s9 m9"><p class="name"><strong>'+data[i].name+'</strong></p><p class="email">'+data[i].email+'</p></div></div><div class="col l1 s1 m1"><a href="#" class="remove_contact_person_editpage" data-cust_id="'+data[i].cust_id+'" data-id="'+data[i].id+'"><i class="material-icons">close</i></a><a href="#" class="edit_contactmodal"  data-id="'+data[i].id+'" data-cust_id="'+data[i].cust_id+'" data-name="'+data[i].name+'" data-mobile="'+data[i].mobile+'" data-email="'+data[i].email+'" data-password="'+data[i].password+'" data-birthday="'+data[i].birthday+'" data-birthday_remainder="'+data[i].birthday_remainder+'" data-anniversary="'+data[i].anniversary+'" data-anniversary_reminder="'+data[i].anniversary_reminder+'" data-is_admin="'+data[i].is_admin+'"><i class="material-icons" style="color: #000;">mode_edit</i></a>></div></div></div></div>'; 
															  
															   }
																 final_html_1 = final_html_1 + html_person;
																 $(".person_list").html(final_html_1);
																 $(".tot_person").html(data.length);
															}
															Materialize.toast('Contact Detail has been successfully removed!', 2000,'green rounded');
														}
														else
														{
															html_person='<div class="gst-boxs add-gst">Add Contact Person Data</div>';
															$(".person_list").html(html_person);
															$(".tot_person").html("0");
														}
													}
													else
													{
														html_person='<div class="gst-boxs add-gst">Add Contact Person Data</div>';
														$(".person_list").html(html_person);
														$(".tot_person").html("0");
														Materialize.toast('Error while processing!', 2000,'red rounded');
													}
												},
						
											});										
									});
									
			$('.add_contactmodal_for_edit').on('click' ,function(){
				var cust_id = $(this).data('cust_id');
				
				$('#add_contactmodal_for_edit').modal('open');
				$('#add_contactperson_for_edit #editpage_cust_id').val(cust_id);
			});
			
			$("#add_contactperson_for_edit").submit(function(e){
				e.preventDefault();
			}).validate({
				rules:{

					new_contact_name:{
						required:true,
					},
					new_contact_mobile:{
						required:true,
					},
					new_contact_email:{
						required:true,
						email:true
					},
					new_contact_password:{
						required:true,
						minlength:8,
						maxlength:12
					},
				},
				messages:{

					new_contact_name:{
						required:"Name is required",
					},
					new_contact_mobile:{
						required:"Mobile Number is required",
					},
					new_contact_email:{
						required:"Email ID is required",
						email:'Enter valid Email ID',
					},
					new_contact_password:{
						required:"Password is required",
						minlength:'Enter minimum 8 characters for password.',
						maxlength:"Maximum 12 character long password allowed.",
					},
				},
				submitHandler:function(form){
						var frm=$(form).serialize();
								$.ajax({
								url:base_url+'My_customers/add_new_contact_persons',
								type:"POST",
								data:{'csrf_test_name':csrf_hash,"frm":frm,},
								success:function(res){;
									if(res!= false)
									{
										var data=JSON.parse(res);
										var html_person ='';
										var final_html_1 ='';
										if(data != '')
										{
											for(var i=0;i<data.length;i++){
											if(i == 0)
											{
												html_person='<div class="row"><div class="contact-person-box"><div class="col l1 s1 m1"><input type="radio" id="contact-person" checked name="default"><label class="fill-green-radio" for="contact-person-1"></label></div><div class="col l10 s10 m10 content-area"><div class="col l3 s3 m3"><div class="gradient-wrapper"><div class="maincircle">100%</div></div></div><div class="col l9 s9 m9"><p class="name"><strong>'+data[i].name+'</strong></p><p class="email">'+data[i].email+'</p></div></div><div class="col l1 s1 m1"><a href="#" class="remove_contact_person_editpage" data-cust_id="'+data[i].cust_id+'" data-id="'+data[i].id+'"><i class="material-icons">close</i></a><a href="#" class="edit_contactmodal"  data-id="'+data[i].id+'" data-cust_id="'+data[i].cust_id+'" data-name="'+data[i].name+'" data-mobile="'+data[i].mobile+'" data-email="'+data[i].email+'" data-password="'+data[i].password+'" data-birthday="'+data[i].birthday+'" data-birthday_remainder="'+data[i].birthday_remainder+'" data-anniversary="'+data[i].anniversary+'" data-anniversary_reminder="'+data[i].anniversary_reminder+'" data-is_admin="'+data[i].is_admin+'"><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div>';
												
											   }
											   else if(i == 1)
											   {
												  html_person='<div class="row"><div class="contact-person-box"><div class="col l1 s1 m1"><input type="radio" id="contact-person" checked name="default"><label class="fill-green-radio" for="contact-person-1"></label></div><div class="col l10 s10 m10 content-area"><div class="col l3 s3 m3"><div class="gradient-wrapper"><div class="maincircle">100%</div></div></div><div class="col l9 s9 m9"><p class="name"><strong>'+data[i].name+'</strong></p><p class="email">'+data[i].email+'</p></div></div><div class="col l1 s1 m1"><a href="#" class="remove_contact_person_editpage" data-cust_id="'+data[i].cust_id+'" data-id="'+data[i].id+'"><i class="material-icons">close</i></a><a href="#" class="edit_contactmodal"  data-id="'+data[i].id+'" data-cust_id="'+data[i].cust_id+'" data-name="'+data[i].name+'" data-mobile="'+data[i].mobile+'" data-email="'+data[i].email+'" data-password="'+data[i].password+'" data-birthday="'+data[i].birthday+'" data-birthday_remainder="'+data[i].birthday_remainder+'" data-anniversary="'+data[i].anniversary+'" data-anniversary_reminder="'+data[i].anniversary_reminder+'" data-is_admin="'+data[i].is_admin+'"><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div>';
												   
											   }
											   else
											   {
												 html_person='<div class="row"><div class="contact-person-box"><div class="col l1 s1 m1"><input type="radio" id="contact-person" checked name="default"><label class="fill-green-radio" for="contact-person-1"></label></div><div class="col l10 s10 m10 content-area"><div class="col l3 s3 m3"><div class="gradient-wrapper"><div class="maincircle">100%</div></div></div><div class="col l9 s9 m9"><p class="name"><strong>'+data[i].name+'</strong></p><p class="email">'+data[i].email+'</p></div></div><div class="col l1 s1 m1"><a href="#" class="remove_contact_person_editpage" data-cust_id="'+data[i].cust_id+'" data-id="'+data[i].id+'"><i class="material-icons">close</i></a><a href="#" class="edit_contactmodal"  data-id="'+data[i].id+'" data-cust_id="'+data[i].cust_id+'" data-name="'+data[i].name+'" data-mobile="'+data[i].mobile+'" data-email="'+data[i].email+'" data-password="'+data[i].password+'" data-birthday="'+data[i].birthday+'" data-birthday_remainder="'+data[i].birthday_remainder+'" data-anniversary="'+data[i].anniversary+'" data-anniversary_reminder="'+data[i].anniversary_reminder+'" data-is_admin="'+data[i].is_admin+'"><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div>';
											   }
												 final_html_1 = final_html_1 + html_person;
												 $(".person_list").html(final_html_1);
												 $(".tot_person").html(data.length);
											}
											Materialize.toast('Contact Details successfully inserted!', 2000,'green rounded');
										}
										else
										{
											html_person='<div class="gst-boxs add-gst">Add Contact Person Data</div>';
											$(".person_list").html(html_person);
										}
									}
									else
									{
										Materialize.toast('Error while processing. Please try again', 2000,'red rounded');
									}
									$("#add_contactperson_for_edit").find("input[type=text],input[type=password], textarea").val("");
									$("#add_contactmodal_for_edit").modal('close');
							},
							
					});
				},
			});
			
			$(document).ready( function () {
				$(document).on('click', '.delete_legal_doc',function(){
						var legal_id = $(this).data('legal_id');
					
						$('#remove_cust_legal_doc').modal('open');
						$('#remove_cust_legal_doc #cust_legal_doc_id').val(legal_id);
					});
					$('.remove_legal_doc').on('click',function(){
	
					var cust_legal_doc_id = $('#cust_legal_doc_id').val();
					$.ajax({
						url:base_url+'My_customers/remove_legal_document',
						type:"POST",
						data:{'csrf_test_name':csrf_hash,"cust_legal_doc_id":cust_legal_doc_id,},
						success:function(res){
								location.reload();
							},
						});										
					});
			
			
			$(document).on('click', '.delete_po_doc' ,function(){
				var po_id = $(this).data('po_id');
				
				$('#remove_cust_po_doc').modal('open');
				$('#remove_cust_po_doc #cust_po_doc_id').val(po_id);
			});
			
			$('.remove_po_doc').on('click',function(){

				var cust_po_doc_id = $('#cust_po_doc_id').val();
				$.ajax({
					url:base_url+'My_customers/remove_po_document',
					type:"POST",
					data:{'csrf_test_name':csrf_hash,"cust_po_doc_id":cust_po_doc_id,},
					success:function(res){
							location.reload();
						},
					});										
			});
			
			$(document).on('click', '.delete_other_doc' ,function(){
				var other_id = $(this).data('other_id');
				
				$('#remove_cust_other_doc').modal('open');
				$('#remove_cust_other_doc #cust_other_doc_id').val(other_id);
			});
			
				$('.remove_other_doc').on('click',function(){
	
					var cust_other_doc_id = $('#cust_other_doc_id').val();
					$.ajax({
						url:base_url+'My_customers/remove_other_document',
						type:"POST",
						data:{'csrf_test_name':csrf_hash,"cust_other_doc_id":cust_other_doc_id,},
						success:function(res){
								location.reload();
							},
						});										
				});
			});
			
			$("#bank_details").submit(function(e){
				e.preventDefault();
			}).validate({
				rules:{
					bank_name:{
						required:true,
					},
					bank_branch_name:{
						required:true,
					},
					acc_type:{
						required:true,
					},
					bus_bank_country:{
						required:true,
					},
					ifsc_code:{
						required:true,
					},
					bank_cur_check:{
						required:true,
					},
					opening_bank_balance:{
						required:true,
					},
					opening_balance_date:{
						required:true,
					},
				},
				messages:{
					bank_name:{
						required:"Bank Name is required",
					},
					bank_branch_name:{
						required:"Bank Branch is required",
					},
					acc_type:{
						required:"Account type is required",
					},
					bus_bank_country:{
						required:"Country is required",
					},
					ifsc_code:{
						required:"IFSC is required",
					},
					bank_cur_check:{
						required:"Currency is required",
					},
					opening_bank_balance:{
						required:"Opening Bank Balance is required",
					},
					opening_balance_date:{
						required:"Bank Balance Date is required",
					},
				},
				submitHandler:function(form){
						var frm=$(form).serialize();
								 //$(form).ajaxSubmit();
								$.ajax({
								url:base_url+'Company_profile/add_bank_info',
								type:"POST",
								data:{'csrf_test_name':csrf_hash,"frm":frm,},
								success:function(res){
										if(res == true)
										{
											$("#addbranch").find("input[type=text],textarea").val("");
											$('#addbranch').modal('close');
											$('#editbranch').modal('close');
											
											Materialize.toast('Bank details updated successfully', 2000,'green rounded');
										}
										
									},
						
					});
				},
			});


			$("#assets_bank_details").submit(function(e){
				e.preventDefault();
			}).validate({
				rules:{
					acc_number:{
						required:true,
					},
					bank_name:{
						required:true,
					},
				},
				messages:{
					acc_number:{
						required:"Bank Name Account number is required",
					},
					bank_name:{
						required:"Bank Name is required",
					},
				},
				submitHandler:function(form){
						var frm=$(form).serialize();
								 //$(form).ajaxSubmit();
								$.ajax({
								url:base_url+'Assets/add_bank_details',
								type:"POST",
								data:{'csrf_test_name':csrf_hash,"frm":frm,},
								success:function(res){
										if(res == true)
										{
											$("#addbranch").find("input[type=text],textarea").val("");
											$('#addbranch').modal('close');
											$('#editbranch').modal('close');
											
											Materialize.toast('Bank details updated successfully', 2000,'green rounded');
										}
										
									},
						
					});
				},
			});



			$("#add_new_cards").submit(function(e){
				e.preventDefault();
			}).validate({
				rules:{
					
				},
				messages:{
					
				},
				submitHandler:function(form){
						var frm=$(form).serialize();
								 //$(form).ajaxSubmit();
								$.ajax({
								url:base_url+'Company_profile/add_card_info',
								type:"POST",
								data:{'csrf_test_name':csrf_hash,"frm":frm,},
								success:function(res){
										if(res == true)
										{
											$("#dd-card").find("input[type=text],textarea,select").val("");
											$('#add-card').modal('close');
											$('#edit-card').modal('close');
											
											Materialize.toast('Card details added successfully', 2000,'green rounded');
										}
										
									},
						
					});
				},
			});
			
			$('.edit_bankinfo').on('click',function(){
				var bank_name = $(this).data('bank_name');
				var bank_branch_name = $(this).data('bank_branch_name');
				var bank_account_no = $(this).data('bank_account_no');
				var bank_swift_code = $(this).data('bank_swift_code');
				var bank_acc_type = $(this).data('bank_acc_type');
				var opening_bank_balance = $(this).data('opening_bank_balance');
				var opening_balance_date = $(this).data('opening_balance_date');
				var cmpid = $(this).data('cmpid');


				
				$('#edit_bank_branch').modal('open');
				$('#edit_bank_branch #edit_ac_number').val(bank_account_no);
				$('#edit_bank_branch #edit_bank_name').val(bank_name);
				$('#edit_bank_branch #edit_bank_branch_name').val(bank_branch_name);
				$('#edit_bank_branch #edit_ifsc_code').val(bank_ifsc_code);
				$('#edit_bank_branch #bank_swift_code').val(bank_swift_code);
				$('#edit_bank_branch #edit_acc_type option[value="'+bank_acc_type+'"]').attr('selected','selected');
				$('#edit_bank_branch #edit_cmpid_bankinfo').val(cmpid);
				$('#edit_bank_branch #opening_bank_balance').val(opening_bank_balance);
				$('#edit_bank_branch #opening_balance_date').val(opening_balance_date);
				$('#edit_bank_branch #edit_acc_type').material_select();
				
			});
			
			$("#edit_bank_details").submit(function(e){
				e.preventDefault();
			}).validate({
				rules:{
					edit_bank_name:{
						required:true,
					},
					edit_bank_branch_name:{
						required:true,
					},
					opening_bank_balance:{
						required:true,
					},

					opening_balance_date:{
						required:true,
					},
				},
				messages:{
					edit_bank_name:{
						required:"Bank Name is required",
					},
					edit_bank_branch_name:{
						required:"Bank Branch is required",
					},
					opening_bank_balance:{
						required:"Opening Bank Balance is required",
					},

					opening_balance_date:{
						required:"Bank Balance Date is required",
					},
				},
				submitHandler:function(form){
						var frm=$(form).serialize();
								$.ajax({
								url:base_url+'Company_profile/edit_bank_info',
								type:"POST",
								data:{'csrf_test_name':csrf_hash,"frm":frm,},
								success:function(res){
										if(res == true)
										{
											$("#edit_bank_details").find("input[type=text],textarea,option").val("");
											$('#edit_bank_branch').modal('close');
											Materialize.toast('Bank details updated successfully', 2000,'green rounded');
										}										
									},
					});
				},
			});
			
			$(document).on('click', '.delete_company_legal_doc' ,function(){
				var legal_id = $(this).data('cmp_legal_id');
				$('#remove_company_legal_doc').modal('open');
				$('#remove_company_legal_doc #cmp_legal_doc_id').val(legal_id);
			});
			
			$('.remove_cmp_legal_doc').on('click',function(){
				var cmp_legal_doc_id = $('#cmp_legal_doc_id').val();
				$.ajax({
					url:base_url+'Company_profile/remove_company_legal_document',
					type:"POST",
					data:{'csrf_test_name':csrf_hash,"cmp_legal_doc_id":cmp_legal_doc_id,},
					success:function(res){
							location.reload();
						},
					});										
			});
			
			$(document).on('click', '.delete_company_po_doc' ,function(){
				var cmp_po_id = $(this).data('cmp_po_id');
				$('#remove_company_po_doc').modal('open');
				$('#remove_company_po_doc #cmp_po_doc_id').val(cmp_po_id);
			});
			
			$('.remove_cmp_po_doc').on('click',function(){
				var cmp_po_doc_id = $('#cmp_po_doc_id').val();
				$.ajax({
					url:base_url+'Company_profile/remove_company_po_document',
					type:"POST",
					data:{'csrf_test_name':csrf_hash,"cmp_po_doc_id":cmp_po_doc_id,},
					success:function(res){
							location.reload();
						},
					});										
			});
			$(document).on('click', '.delete_company_other_doc' ,function(){
				var cmp_other_id = $(this).data('cmp_other_id');
				$('#remove_company_other_doc').modal('open');
				$('#remove_company_other_doc #cmp_other_doc_id').val(cmp_other_id);
			});
			$('.remove_cmp_other_doc').on('click',function(){
				var cmp_other_doc_id = $('#cmp_other_doc_id').val();
				$.ajax({
					url:base_url+'sales/remove_company_other_document',
					type:"POST",
					data:{'csrf_test_name':csrf_hash,"cmp_other_doc_id":cmp_other_doc_id,},
					success:function(res){
							location.reload();
						},
					});										
			});
			$(document).on('click', '.delete_company_memo_doc' ,function(){
				var cmp_memo_id = $(this).data('cmp_memo_id');
				$('#remove_company_memo_doc').modal('open');
				$('#remove_company_memo_doc #cmp_memo_doc_id').val(cmp_memo_id);
			});
			$('.remove_cmp_memo_doc').on('click',function(){
				var cmp_memo_doc_id = $('#cmp_memo_doc_id').val();
				$.ajax({
					url:base_url+'Company_profile/remove_company_memo_document',
					type:"POST",
					data:{'csrf_test_name':csrf_hash,"cmp_memo_doc_id":cmp_memo_doc_id,},
					success:function(res){
							location.reload();
						},
					});										
			});
			
			$(document).on('click', '.view_deactive_company_profile',function(){
				var cmp_profile = $(this).data('cmp_profile');
				 $('#view_deactive_cmp_profile').modal('open');
				 $('#view_deactive_cmp_profile #cmp_profile').val(cmp_profile);
			});

			$('.deactive_cmp').on('click',function(){

				var cmp_profile = $('#cmp_profile').val();

				$.ajax({

					url:base_url+'Company_profile/deactive_company_2',

					type:"POST",

					data:{'csrf_test_name':csrf_hash,"cmp_profile":cmp_profile,},

					success:function(res){
							window.location.href=base_url+'Company_profile/manage_company_profile';
						},
					});										
			});
			$(document).on('click', '.view_activate_cmp_profile',function(){
				var cmp_profile = $(this).data('cmp_profile');
				 $('#view_active_cmp_profile').modal('open');
				 $('#view_active_cmp_profile #cmp_profile_2').val(cmp_profile);
			});
			$('.active_cmp').on('click',function(){

				var cmp_profile = $('#cmp_profile_2').val();

				$.ajax({

					url:base_url+'Company_profile/activate_company_2',

					type:"POST",

					data:{'csrf_test_name':csrf_hash,"cmp_profile":cmp_profile,},

					success:function(res){
							window.location.href=base_url+'Company_profile/manage_company_profile';
						},
					});										
			});

						
		//$("input[type='checkbox']").click(function(){
				var bulk_profiles = [];
				$("input[id='personal_profile_bulk']").on("click",function(){
				if($(this).is(':checked',true)) {
					$(".personal_profile_bulk_action").prop('checked', true);
					$(".personal_profile_bulk_action:checked").each(function() {
							bulk_profiles.push($(this).val());
						});
						bulk_profiles = bulk_profiles.join(",");
						$('#deactive_multiple_profiles').attr('data-multi_profiles',bulk_profiles);
					}
					else {
						$(".personal_profile_bulk_action").prop('checked',false);
						$("#personal_profile_bulk").prop('checked', false);  
						bulk_profiles = [];
						$('#deactive_multiple_profiles').attr('data-multi_profiles',0);
					}
				});
					
				$("#deactive_multiple_profiles").on('click', function(e) {
					var profiles_values = $(this).data('multi_profiles');
					if(profiles_values == '' || profiles_values == '0')
					{
						Materialize.toast('Please select a record first', 2000,'red rounded');
					}
					else
					{
						$.ajax({
							url:base_url+'Personal_profile/deactive_multiple',
							type:"POST",
							data:{'csrf_test_name':csrf_hash,"profiles_values":profiles_values,},
							success:function(res){
									location.reload();
								},
						});		
					}
				});
					
				var bulk_cmp_profiles = [];
				$("input[id='company_profile_bulk']").on("click",function(){
				if($(this).is(':checked',true)) {
					$(".company_profile_bulk_action").prop('checked', true);
					$(".company_profile_bulk_action:checked").each(function() {
							bulk_cmp_profiles.push($(this).val());
						});
						bulk_cmp_profiles = bulk_cmp_profiles.join(",");
						$('#deactive_multiple_cmp_profiles').attr('data-multi_profiles',bulk_cmp_profiles);
					}
					else {
						$(".company_profile_bulk_action").prop('checked',false);
						bulk_cmp_profiles = [];
						$('#deactive_multiple_cmp_profiles').attr('data-multi_profiles',0);
					}
				});
					
				$("#deactive_multiple_cmp_profiles").on('click', function(e) {
					var profiles_values = $(this).data('multi_profiles');
					if(profiles_values == '' || profiles_values == '0')
					{
						Materialize.toast('Please select a record first', 2000,'red rounded');
					}
					else
					{
						$.ajax({
							url:base_url+'profile/deactive_multiple',
							type:"POST",
							data:{'csrf_test_name':csrf_hash,"profiles_values":profiles_values,},
							success:function(res){
									location.reload();
								},
						});		
					}
				});
								
				var bulk_customer_profiles = [];
				$("input[id='customer_profile_bulk']").on("click", function(){
				if($(this).is(':checked',true)) {
					$(".customer_profile_bulk_action").prop('checked', true);
					$(".customer_profile_bulk_action:checked").each(function() {
							bulk_customer_profiles.push($(this).val());
						});
						bulk_customer_profiles = bulk_customer_profiles.join(",");
						$('#deactive_multiple_customer_profiles').attr('data-multi_profiles',bulk_customer_profiles);
					}
					else {
						$(".customer_profile_bulk_action").prop('checked',false);
						bulk_customer_profiles = [];
						$('#deactive_multiple_customer_profiles').attr('data-multi_profiles',0);
					}
				});

				$( "#deactive_multiple_customer_profiles").on('click', function(e) {
					var profiles_values_1 = $(this).data('multi_profiles');
					if(profiles_values_1 == '' || profiles_values_1 == '0')
					{
						Materialize.toast('Please select a record first', 2000,'red rounded');
					}
					else
					{
						$.ajax({
							url:base_url+'My_customers/deactive_multiple',
							type:"POST",
							data:{'csrf_test_name':csrf_hash,"profiles_values":profiles_values_1,},
							success:function(res){
									location.reload();
								},
						});		
					}
				});
								
				$('.send_mail_to_customer').on('click',function(){
					$.ajax({
						url:base_url+'My_customers/send_email_to_all_customer',
						type:"POST",
						data:{'csrf_test_name':csrf_hash,},
						success:function(res){
									Materialize.toast('Email sent to all clients', 2000,'green rounded');
							},
						});										
				});
				
				$('.deactive_account').on('click',function(){
					 $.ajax({
							type:"POST", 
							url: base_url + 'Dashboard/deactive_my_account',
							success: function (data) {
								if (data == true) {
									$('#deactivate-notification').modal('open');
									location.reload();
								}
							}
						});
				});
				
				$('.deactive_customer_2').on('click',function(){
				var cust_profile = $(this).data('customer_profile');
				 $('#view_deactive_customer_profile').modal('open');
				 $('#view_deactive_customer_profile #customer_profile').val(cust_profile);
				});
				
				$('.deactive_customer_view').on('click',function(){

				var customer_profile = $('#customer_profile').val();

				$.ajax({

					url:base_url+'My_customers/deactive_customer_profile',

					type:"POST",

					data:{'csrf_test_name':csrf_hash,"customer_profile":customer_profile,},

					success:function(res){
							window.location.href=base_url+'My_customers/list_customers';
						},
					});										
			});
			
			$('.activate_customer_2').on('click',function(){
				var cust_profile = $(this).data('customer_profile');
				 $('#view_active_customer_profile').modal('open');
				 $('#view_active_customer_profile #active_customer_profile').val(cust_profile);
				});
				
				$('.active_customer_view').on('click',function(){

				var customer_profile = $('#active_customer_profile').val();

				$.ajax({

					url:base_url+'My_customers/active_customer_profile',

					type:"POST",

					data:{'csrf_test_name':csrf_hash,"customer_profile":customer_profile,},

					success:function(res){
							window.location.href=base_url+'My_customers/list_customers';
						},
					});										
			});

			
			$(document).on('click', '#edit_accessport' ,function(){
				if($('#edit_accessport').prop('checked')==true){
					$('#edit_add_modal-popup').modal('open');
				} else {
					$('#edit_modal-popup').modal('open');
				}
			});	
			$(document).on('click', '.view_accessport' ,function(){
				if($(this).prop('checked')==true){
					var customer_id = $(this).val();
					$('#view_add_modal-popup').modal('open');
						$('.portal_access_yes').on('click',function(){
						$.ajax({
							url:base_url+'My_customers/edit_portal_access',
							type:"POST",
							data:{'csrf_test_name':csrf_hash,"customer_id":customer_id,"change_status":1},
							success:function(res){
									location.reload();
								},
							});
						});		
				} else {
					var customer_id = $(this).val();
					$('#view_modal-popup').modal('open');
					$('.portal_access_yes').on('click',function(){
						$.ajax({
							url:base_url+'My_customers/edit_portal_access',
							type:"POST",
							data:{'csrf_test_name':csrf_hash,"customer_id":customer_id,"change_status":0},
							success:function(res){
									location.reload();
								},
							});
						});	
				}
			});
			
			


			$("#header_company_profiles").off().on('change',function(){
				var company_id=$(this).val();
				var customer_id=$('#customer_id').val();
				var supplier_id=$('#supplier_id').val();
				$("#create_invoice #company_name").val(company_id);
				
				$('#company_profile').val(company_id);
				$.ajax({
					url:base_url+'Dashboard/get_branch_gst_info',
					type:"POST",
					data:{'csrf_test_name':csrf_hash,'company_id':company_id},
					success:function(res){
						$("#branch_gst_list").html(res);
						$("#branch_gst_list").parents('.input-field').addClass('label-active');
						$('#branch_gst_list').material_select();
					},
					complete:function(res){
						$('#company_gst').val($('#branch_gst_list').val());
					},
				});
				$.ajax({
					'url':base_url+'My_bills/generate_bill_no',
					type:"POST",
					data:{
						'csrf_test_name':csrf_hash,
						'company_id':$('#company_profile').val(),
					},
					success:function(res){
						$("#bs_number").val(res);
					},
				});
				if(customer_id){
					$.ajax({
						url:base_url+'My_invoices/get_customer_gst_info',
						type:"POST",
						data:{'csrf_test_name':csrf_hash,"customer_id":customer_id,},
						success:function(res){
							 var res = $.parseJSON(res);
							$("#customer_gstin").html(res[0]);
							$('#customer_gstin').material_select();
							$("#customer_place").html(res[1]);
							$('#customer_place').material_select();
							$('.billing_address span').html(res[2]);
							$('#po_num').html(res[4]);
							$('#po_num').material_select();
							//$('.shipping_address span').html(res[3]);
							//$('.shipping_address').closest('span').html(res[3]);
							//$('.chkbox').parent('.ship_add').html(res[3]);
						},
						complete:function(res){
							$.ajax({
								url:base_url+'My_invoices/get_gst_value',
								type:"POST",
								data:{'csrf_test_name':csrf_hash,},
								success:function(res){
									var data = JSON.parse(res);
									$("#tot_gst").val(data.gst);
									var tot_gst = data.gst;
										$.ajax({
											url:base_url+'My_invoices/fetch_gst',
											type:"POST",
											data:{'csrf_test_name':csrf_hash,"customer_id":customer_id,"company_id":company_id,},
											success:function(res1){
												res1=$.parseJSON(res1);
												var half_gst = parseFloat($("#tot_gst").val())/2;
												if($("#document_type").val()==5){
													$("#igst").val(parseFloat($('#tot_gst').val()));
													$("#sgst").val("0.00");
													$("#cgst").val("0.00");
													$(".branch_igst").val(parseFloat($('#tot_gst').val()));
													$(".branch_sgst").val("0.00");
													$(".branch_cgst").val("0.00");
												}
												else if(res1==true && $("#document_type").val()!=5){
													$("#igst").val("0.00");
													$("#sgst").val(half_gst);
													$("#cgst").val(half_gst);
													$(".branch_igst").val("0.00");
													$(".branch_sgst").val(half_gst);
													$(".branch_cgst").val(half_gst);
												}
												else
												{
													$("#igst").val(parseFloat($('#tot_gst').val()));
													$("#sgst").val("0.00");
													$("#cgst").val("0.00");
													$(".branch_igst").val(parseFloat($('#tot_gst').val()));
													$(".branch_sgst").val("0.00");
													$(".branch_cgst").val("0.00");
												}
												
											},
											complete:function(res1){
												$('input[name="invoice_array[]"]').each(function(){
													calculate_total($(this).val());
												});
											},
										});
								},
							});
						},
					});
				}
				if(supplier_id!='' && supplier_id!=undefined){
					$.ajax({
						url:base_url+'my_purchase_invoices/get_supplier_gst_info',
						type:"POST",
						data:{'csrf_test_name':csrf_hash,"supplier_id":supplier_id,},
						success:function(res){
							 var res = $.parseJSON(res);
							$("#supplier_gstin").html(res[0]);
							$('#supplier_gstin').material_select();
							$("#supplier_place").html(res[1]);
							$('#supplier_place').material_select();
							$('.billing_address span.bill_add').html(res[2]);
							if($("#same-as-bill-invoice").is(':checked')){
								$('.shipping_address span.ship_add').html(res[2]);
							}
							else{
								$('.shipping_address span.ship_add').html(res[3]);
							}
							$('#po_numbers').html(res[4]);
							$('#po_numbers').material_select();
							//$('.shipping_address span').html(res[3]);
							//$('.shipping_address').closest('span').html(res[3]);
							//$('.chkbox').parent('.ship_add').html(res[3]);
						},
						complete:function(res){
							$.ajax({
								url:base_url+'my_purchase_invoices/get_gst_value',
								type:"POST",
								data:{'csrf_test_name':csrf_hash,},
								success:function(res){
									var data = JSON.parse(res);
									$("#tot_gst").val(data.gst);
									var tot_gst = data.gst;
										$.ajax({
											url:base_url+'my_purchase_invoices/fetch_gst',
											type:"POST",
											data:{'csrf_test_name':csrf_hash,"supplier_id":supplier_id,"company_id":company_id,},
											success:function(res1){
												res1=$.parseJSON(res1);
												var half_gst = parseFloat($("#tot_gst").val())/2;
												if($("#invoice_type").val()==5){
													$("#igst").val(parseFloat($('#tot_gst').val()));
													$("#sgst").val("0.00");
													$("#cgst").val("0.00");
													$(".branch_igst").val(parseFloat($('#tot_gst').val()));
													$(".branch_sgst").val("0.00");
													$(".branch_cgst").val("0.00");
												}
												else if(res1==true && $("#invoice_type").val()!=5){
													$("#igst").val("0.00");
													$("#sgst").val(half_gst);
													$("#cgst").val(half_gst);
													$(".branch_igst").val("0.00");
													$(".branch_sgst").val(half_gst);
													$(".branch_cgst").val(half_gst);
												}
												else
												{
													$("#igst").val(parseFloat($('#tot_gst').val()));
													$("#sgst").val("0.00");
													$("#cgst").val("0.00");
													$(".branch_igst").val(parseFloat($('#tot_gst').val()));
													$(".branch_sgst").val("0.00");
													$(".branch_cgst").val("0.00");
												}
												
											},
											complete:function(res1){
												$('input[name="invoice_array[]"]').each(function(){
													calculate_pur_inv_total($(this).val());
												});
											},
										});
								},
							});
						},
					});
				}
			});
			$('#customer_gstin').on('change' ,function(){
				var customer_id = $(this).val();
				$('#customer_place option[value="'+customer_id+'"]').attr('selected','selected');
				$('#customer_place').material_select();
			});
			$('#po_num').on('change' ,function(){
				var customer_id = $(this).val();
				$.ajax({
					url:base_url+'My_invoices/get_po_date',
					type:"POST",
					data:{'csrf_test_name':csrf_hash,"customer_id":customer_id,},
					success:function(res){
						$("#po_date").val(res);
					},
				});
			});	
			
			 $("#create_services").validate({
				rules:{
					service_name:{
						required:true,
					},
					hsn_no:{
						required:true,
					},
				},
				messages:{
					service_name:{
						required:"Service Name is required",
					},
					hsn_no:{
						required:"HSN / SAC is required",
					},
				},
			});
			
			$("#add_other_tax").submit(function(e){
				e.preventDefault();
			}).validate({
				rules:{

					'tax_name[]':{
						required:true,
					},
					'tax_perc[]':{
						required:true,
					},
				},
				messages:{

					'tax_name[]':{
						required:"Tax Name is required",
					},
					'tax_perc[]':{
						required:"Tax Percentage is required",
					},
				},
				submitHandler:function(form){

						var frm=$(form).serialize();
								$.ajax({
								url:base_url+'My_services/add_other_tax',
								type:"POST",
								data:{'csrf_test_name':csrf_hash,"frm":frm,},
								success:function(res){
									if(res!= false)
									{
										var data=JSON.parse(res);
										var html_1 ='';
										var final_html ='';
										
										if(data != '')
										{
											for(var i=0;i<data.length;i++){
											if(i == 0)
											   {
												html_1='<div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">NAME OF TAX</span><p>'+data[i].tax_name+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">TAX PERCENTAGE</span><p>'+data[i].tax_percentage+'</p></div><div class="col l1 s1 m1 gstaction"><a href="#" class="remove_tax" data-tax_name="'+data[i].tax_name+'" data-tax_perc="'+data[i].tax_percentage+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a></div></div></div>';
											   }
											   else
											   {
												 html_1='<div class="gstmore"><div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">NAME OF TAX</span><p>'+data[i].tax_name+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">TAX PERCENTAGE</span><p>'+data[i].tax_percentage+'</p></div><div class="col l1 s1 m1 gstaction"><a href="#" class="remove_tax" data-tax_name="'+data[i].tax_name+'" data-tax_perc="'+data[i].tax_percentage+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a></div></div></div></div>';
											  
											   }
												 final_html = final_html + html_1;
												 $("#tax_info_array").html(final_html);
												 $(".two-i").html(data.length);
											}
											 Materialize.toast('Tax Information have been set', 2000,'green rounded');
										}
										else
										{
											html_1='<div class="gst-boxs add-gst">Add TAX Data</div>';
											$("#tax_info_array").html(html_1);
										}
									}
									else
									{
										Materialize.toast('Error while processing. Please try again', 2000,'red rounded');
									}
									$("#add_other_tax").find("input[type=text], textarea").val("");
									$("#other-alert").modal('close');
									
									
								$('.remove_tax').on('click',function(){
									
									var tax_name = $(this).data('tax_name');
									var tax_perc = $(this).data('tax_perc');
									
									$('#remove_tax_data').modal('open');
									$('#remove_tax_data #remove_tax_name').val(tax_name);
									$('#remove_tax_data #remove_tax_perc').val(tax_perc);
									
								});
			
									$('.remove_tax_data').off().on('click',function(){
						
										var tax_name = $('#remove_tax_name').val();
										var tax_perc = $('#remove_tax_perc').val();
						
										$.ajax({
						
											url:base_url+'My_services/remove_tax_info',
						
											type:"post",
						
											data:{'csrf_test_name':csrf_hash,"tax_name":tax_name,"tax_perc":tax_perc},
						
											success:function(res){
													if(res!= false)
													{
														var data=JSON.parse(res);
														var html_1 ='';
														var final_html ='';
														if(data != '')
														{
															for(var i=0;i<data.length;i++){
															if(i == 0)
															   {
																html_1='<div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">NAME OF TAX</span><p>'+data[i].tax_name+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">TAX PERCENTAGE</span><p>'+data[i].tax_percentage+'</p></div><div class="col l1 s1 m1 gstaction"><a href="#" class="remove_tax" data-tax_name="'+data[i].tax_name+'" data-tax_perc="'+data[i].tax_percentage+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a></div></div></div>';
															   }
															   else
															   {
																html_1='<div class="gstmore"><div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">NAME OF TAX</span><p>'+data[i].tax_name+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">TAX PERCENTAGE</span><p>'+data[i].tax_percentage+'</p></div><div class="col l1 s1 m1 gstaction"><a href="#" class="remove_tax" data-tax_name="'+data[i].tax_name+'" data-tax_perc="'+data[i].tax_percentage+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a></div></div></div></div>';
															  
															   }
																 final_html = final_html + html_1;
																 $("#tax_info_array").html(final_html);
																 $(".two-i").html(data.length);
															}
															 Materialize.toast('Tax Information has been removed', 2000,'green rounded');
														}
														else
														{
															html_1='<div class="gst-boxs add-gst">Add TAX Data</div>';
															$("#tax_info_array").html(html_1);
															
														}
													}
													else
													{
														html_1='<div class="gst-boxs add-gst">Add TAX Data</div>';
														$("#tax_info_array").html(html_1);
													}
												},
						
											});										
									});
							},
							
					});
				},
			});



///////////////////////////////////////////////////////////////////
$("#add_other_tax_exp").submit(function(e){
				e.preventDefault();
			}).validate({
				rules:{

					'tax_name[]':{
						required:true,
					},
					'tax_perc[]':{
						required:true,
					},
				},
				messages:{

					'tax_name[]':{
						required:"Tax Name is required",
					},
					'tax_perc[]':{
						required:"Tax Percentage is required",
					},
				},
				submitHandler:function(form){

						var frm=$(form).serialize();
								$.ajax({
								url:base_url+'expense/add_other_tax',
								type:"POST",
								data:{'csrf_test_name':csrf_hash,"frm":frm,},
								success:function(res){
									if(res!= false)
									{
										var data=JSON.parse(res);
										var html_1 ='';
										var final_html ='';
										
										if(data != '')
										{
											for(var i=0;i<data.length;i++){
											if(i == 0)
											   {
												html_1='<div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">NAME OF TAX</span><p>'+data[i].tax_name+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">TAX PERCENTAGE</span><p>'+data[i].tax_percentage+'</p></div><div class="col l1 s1 m1 gstaction"><a href="#" class="remove_tax" data-tax_name="'+data[i].tax_name+'" data-tax_perc="'+data[i].tax_percentage+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a></div></div></div>';
											   }
											   else
											   {
												 html_1='<div class="gstmore"><div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">NAME OF TAX</span><p>'+data[i].tax_name+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">TAX PERCENTAGE</span><p>'+data[i].tax_percentage+'</p></div><div class="col l1 s1 m1 gstaction"><a href="#" class="remove_tax" data-tax_name="'+data[i].tax_name+'" data-tax_perc="'+data[i].tax_percentage+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a></div></div></div></div>';
											  
											   }
												 final_html = final_html + html_1;
												 $("#tax_info_array").html(final_html);
												 $(".two-i").html(data.length);
											}
											 Materialize.toast('Tax Information have been set', 2000,'green rounded');
										}
										else
										{
											html_1='<div class="gst-boxs add-gst">Add TAX Data</div>';
											$("#tax_info_array").html(html_1);
										}
									}
									else
									{
										Materialize.toast('Error while processing. Please try again', 2000,'red rounded');
									}
									$("#add_other_tax").find("input[type=text], textarea").val("");
									$("#other-alert").modal('close');
									
									
								$('.remove_tax').on('click',function(){
									
									var tax_name = $(this).data('tax_name');
									var tax_perc = $(this).data('tax_perc');
									
									$('#remove_tax_data').modal('open');
									$('#remove_tax_data #remove_tax_name').val(tax_name);
									$('#remove_tax_data #remove_tax_perc').val(tax_perc);
									
								});
			
									$('.remove_tax_data').off().on('click',function(){
						
										var tax_name = $('#remove_tax_name').val();
										var tax_perc = $('#remove_tax_perc').val();
						
										$.ajax({
						
											url:base_url+'My_services/remove_tax_info',
						
											type:"post",
						
											data:{'csrf_test_name':csrf_hash,"tax_name":tax_name,"tax_perc":tax_perc},
						
											success:function(res){
													if(res!= false)
													{
														var data=JSON.parse(res);
														var html_1 ='';
														var final_html ='';
														if(data != '')
														{
															for(var i=0;i<data.length;i++){
															if(i == 0)
															   {
																html_1='<div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">NAME OF TAX</span><p>'+data[i].tax_name+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">TAX PERCENTAGE</span><p>'+data[i].tax_percentage+'</p></div><div class="col l1 s1 m1 gstaction"><a href="#" class="remove_tax" data-tax_name="'+data[i].tax_name+'" data-tax_perc="'+data[i].tax_percentage+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a></div></div></div>';
															   }
															   else
															   {
																html_1='<div class="gstmore"><div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">NAME OF TAX</span><p>'+data[i].tax_name+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">TAX PERCENTAGE</span><p>'+data[i].tax_percentage+'</p></div><div class="col l1 s1 m1 gstaction"><a href="#" class="remove_tax" data-tax_name="'+data[i].tax_name+'" data-tax_perc="'+data[i].tax_percentage+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a></div></div></div></div>';
															  
															   }
																 final_html = final_html + html_1;
																 $("#tax_info_array").html(final_html);
																 $(".two-i").html(data.length);
															}
															 Materialize.toast('Tax Information has been removed', 2000,'green rounded');
														}
														else
														{
															html_1='<div class="gst-boxs add-gst">Add TAX Data</div>';
															$("#tax_info_array").html(html_1);
															
														}
													}
													else
													{
														html_1='<div class="gst-boxs add-gst">Add TAX Data</div>';
														$("#tax_info_array").html(html_1);
													}
												},
						
											});										
									});
							},
							
					});
				},
			});


////////////////////////////////////////////////////////////////////
			
			$('.notif_type').on('click',function(){
				$('.notif_type img').removeClass('selected_notification');
				var type = $(this).data('notif_type');
				$('#notification_type').val(type);
				$(this).find('img').addClass('selected_notification');
			});
			$("#add_alert_details").submit(function(e){
				e.preventDefault();
			}).validate({
				rules:{
					alert_name:{
						required:true,
					},
				},
				messages:{
					alert_name:{
						required:"Alert Name is required",
					},
				},
				submitHandler:function(form){

						var frm=$(form).serialize();
								$.ajax({
								url:base_url+'My_services/add_alert_info',
								type:"post",
								data:{'csrf_test_name':csrf_hash,"frm":frm,},
								success:function(res){
									if(res!= false)
									{
										var data=JSON.parse(res);
										var html_1 ='';
										var final_html ='';
										if(data != '')
										{
											for(var i=0;i<data.length;i++){
												var notification = '';
												   if(data[i].notification_type == '1') {
													notification = 'Remainder';
												   }else if(data[i].notification_type == '2') {
														notification = 'Message';   
												   } else if(data[i].notification_type == '3'){
														notification = 'Email';   
												   } else {
													   notification = '';
												   }
												if(i == 0)
											   {
												html_1='<div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
							   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">ALERT NAME</span><p>'+data[i].alert_name+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">NOTIFICATION TYPE</span><p>'+notification+'</p></div><div class="col l1 s1 m1 gstaction"><a href="#" class="remove_service_alert_session" data-alert_name="'+data[i].alert_name+'" data-alert_no="'+data[i].alert_number+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a></div></div></div>';
											   }
											   else
											   {
												 html_1='<div class="alertmore"><div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
							   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">ALERT NAME</span><p>'+data[i].alert_name+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">NOTIFICATION TYPE</span><p>'+notification+'</p></div><div class="col l1 s1 m1 gstaction"><a href="#" class="remove_service_alert_session" data-alert_name="'+data[i].alert_name+'" data-alert_no="'+data[i].alert_number+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a></div></div></div>';
											  
											   }
												 final_html = final_html + html_1;
												 $("#alert_info_array").html(final_html);
												// $(".two-i").html(data.length);
											}
											Materialize.toast('An alert has been set!', 2000,'green rounded');
										}
										else
										{
											html_1='<div class="gst-boxs add-gst">Add Alert Data</div>';
											$("#alert_info_array").html(html_1);
											Materialize.toast('Add alert information!', 2000,'green rounded');
										}
									}
									else
									{
										html_1='<div class="gst-boxs add-gst">Add Alert Data</div>';
										$("#alert_info_array").html(html_1);
										Materialize.toast('Error while processing!', 2000,'red rounded');
									}
									$("#add_alert_details").find("input[type=text], textarea").val("");
									$('.notif_type img').removeClass('selected_notification');
									$("#add-alert").modal('close');
							},
					});
				},
			});
			
			$("#edit_services").validate({
				rules:{
					service_name:{
						required:true,
					},
					hsn_no:{
						required:true,
					},
				},
				messages:{
					service_name:{
						required:"Service Name is required",
					},
					hsn_no:{
						required:"HSN / SAC is required",
					},
				},
			});
			
			$(document).on('click', '.edit_service_tax' ,function(){
				$('#edit_tax_modal').modal('open');
				var tax_id = $(this).data('tax_id');
				var service_id = $(this).data('service_id');
				var tax_name = $(this).data('tax_name');
				var tax_percentage = $(this).data('tax_percentage');
				
				$('#edit_tax_modal #edit_tax_id').val(tax_id);
				$('#edit_tax_modal #edit_tax_service_id').val(service_id);
				$('#edit_tax_modal #edit_tax_name').val(tax_name);
				$('#edit_tax_modal #edit_tax_perc').val(tax_percentage);
			});
			
			$("#edit_tax_service").submit(function(e){
				e.preventDefault();
			}).validate({
				rules:{

					edit_tax_name:{
						required:true,
					},
					edit_tax_perc:{
						required:true,
					},
				},
				messages:{

					edit_tax_name:{
						required:"TAX Name is required",
					},
					edit_tax_perc:{
						required:"TAX Percentage is required",
					},
				},
				submitHandler:function(form){
						var frm=$(form).serialize();
								$.ajax({
								url:base_url+'My_services/edit_tax_info',
								type:"POST",
								data:{'csrf_test_name':csrf_hash,"frm":frm,},
								success:function(res){
									//console.log(res);
									if(res!= false)
									{
										var data=JSON.parse(res);
										var html_1 ='';
										var final_html ='';
										if(data != '')
										{
											for(var i=0;i<data.length;i++){
											if(i == 0)
											   {
												html_1='<div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">NAME OF TAX</span><p>'+data[i].tax_name+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">TAX PERCENTAGE</span><p>'+data[i].tax_percentage+'</p></div><div class="col l1 s1 m1 gstaction"><a href="#" class="remove_tax_edit" data-tax_id="'+data[i].id+'" data-service_id="'+data[i].service_id+'" data-tax_name="'+data[i].tax_name+'" data-tax_perc="'+data[i].tax_percentage+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a><a href="#" class="edit_service_tax" data-tax_id="'+data[i].id+'" data-service_id="'+data[i].service_id+'" data-tax_name="'+data[i].tax_name+'" data-tax_percentage="'+data[i].tax_percentage+'""><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div>';
											   }
											   else
											   {
												 html_1='<div class="gstmore"><div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">NAME OF TAX</span><p>'+data[i].tax_name+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">TAX PERCENTAGE</span><p>'+data[i].tax_percentage+'</p></div><div class="col l1 s1 m1 gstaction"><a href="#" class="remove_tax_edit" data-tax_id="'+data[i].id+'" data-service_id="'+data[i].service_id+'" data-tax_name="'+data[i].tax_name+'" data-tax_percentage="'+data[i].tax_percentage+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a><a href="#" class="edit_service_tax" data-tax_id="'+data[i].id+'" data-service_id="'+data[i].service_id+'" data-tax_name="'+data[i].tax_name+'" data-tax_percentage="'+data[i].tax_percentage+'""><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div></div>';
											  
											   }
												 final_html = final_html + html_1;
												 $("#tax_info_array_edit").html(final_html);
												 $(".two-i").html(data.length);
											}
											 Materialize.toast('Tax information updated successfully !', 2000,'green rounded');
										}
										else
										{
											html_1='<div class="gst-boxs add-gst">Add TAX Data</div>';
											$("#tax_info_array_edit").html(html_1);
											 Materialize.toast('ADD Tax DATA!', 2000,'green rounded');
										}
									}
									else
									{
										Materialize.toast('Error while processing. Please try again', 2000,'red rounded');
									}
									$("#edit_tax_service").find("input[type=text], textarea").val("");
									$("#edit_tax_modal").modal('close');
									
							},
							
					});
				},
			});
			
			
			$(document).on('click', '.remove_tax_edit' ,function(){
					
				var tax_id = $(this).data('tax_id');
				var service_id = $(this).data('service_id');
				
				$('#edit_remove_tax_data').modal('open');
				$('#edit_remove_tax_data #remove_tax_id').val(tax_id);
				$('#edit_remove_tax_data #remove_service_id').val(service_id);

				});
				
				$('.edit_remove_tax').on('click',function(){
						
										var tax_id = $('#remove_tax_id').val();
										var service_id = $('#remove_service_id').val();

										$.ajax({
						
											url:base_url+'My_services/delete_tax_info',
						
											type:"post",
						
											data:{'csrf_test_name':csrf_hash,"tax_id":tax_id,"service_id":service_id},
						
											success:function(res){
													if(res!= false)
													{
														var data=JSON.parse(res);
														var html_1 ='';
														var final_html ='';
														if(data != '')
														{
															for(var i=0;i<data.length;i++){
															if(i == 0)
															   {
																html_1='<div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">NAME OF TAX</span><p>'+data[i].tax_name+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">TAX PERCENTAGE</span><p>'+data[i].tax_percentage+'</p></div><div class="col l1 s1 m1 gstaction"><a href="#" class="remove_tax_edit" data-tax_id="'+data[i].id+'" data-service_id="'+data[i].service_id+'" data-tax_name="'+data[i].tax_name+'" data-tax_perc="'+data[i].tax_percentage+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a><a href="#" class="edit_service_tax" data-tax_id="'+data[i].id+'" data-service_id="'+data[i].service_id+'" data-tax_name="'+data[i].tax_name+'" data-tax_percentage="'+data[i].tax_percentage+'""><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div>';
															   }
															   else
															   {
																 html_1='<div class="gstmore"><div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">NAME OF TAX</span><p>'+data[i].tax_name+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">TAX PERCENTAGE</span><p>'+data[i].tax_percentage+'</p></div><div class="col l1 s1 m1 gstaction"><a href="#" class="remove_tax_edit" data-tax_id="'+data[i].id+'" data-service_id="'+data[i].service_id+'" data-tax_name="'+data[i].tax_name+'" data-tax_percentage="'+data[i].tax_percentage+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a><a href="#" class="edit_service_tax" data-tax_id="'+data[i].id+'" data-service_id="'+data[i].service_id+'" data-tax_name="'+data[i].tax_name+'" data-tax_percentage="'+data[i].tax_percentage+'""><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div></div>';
															  
															   }
																 final_html = final_html + html_1;
																 $("#tax_info_array_edit").html(final_html);
																 $(".two-i").html(data.length);
															}
															Materialize.toast('TAX Info removed successfully !', 2000,'green rounded');
														}
														else
														{
															//html_1='<div class="gst-boxs add-gst">Add GSTIN Data</div>';
															//$("#tax_info_array_edit").html(html_1);
															Materialize.toast('ADD TAX DATA!', 2000,'green rounded');
														}
													}
													else
													{
														//html_1='<div class="gst-boxs add-gst">Add GSTIN Data</div>';
														//$("#tax_info_array_edit").html(html_1);
														Materialize.toast('Error while processing!', 2000,'red rounded');
													}
												},
						
											});										
									});
									
									$(document).on('click', '.edit_alert_info' ,function(){
										var alert_id = $(this).data('alert_id');
										var service_id = $(this).data('service_id');
										var alert_name = $(this).data('alert_name');
										var alert_on = $(this).data('alert_on');
										var condition = $(this).data('set_condition');
										var alert_number = $(this).data('alert_number');
										var alert_interval = $(this).data('alert_interval');
										var notification_type = $(this).data('notification_type');
										
										$('#edit_alert').modal('open');
										$('#edit_alert #edit_alert_id').val(alert_id);
										$('#edit_alert #edit_alert_name').val(alert_name);
										$('#edit_alert #edit_alert_service_id').val(service_id);
										$('#edit_alert #edit_set_alert_on option[value="'+alert_on+'"]').attr('selected','selected');
										$('#edit_alert #edit_set_alert_on').material_select();
										
										$('#edit_alert #edit_condition option[value="'+condition+'"]').attr('selected','selected');
										$('#edit_alert #edit_condition').material_select();
										
										$('#edit_alert #edit_alert_number').val(alert_number);
										
										$('#edit_alert #edit_alert_interval option[value="'+alert_interval+'"]').attr('selected','selected');
										$('#edit_alert #edit_alert_interval').material_select();
										
										$('#edit_alert #edit_notification_type').val(notification_type);
										$(".edit_notif_type").each(function() {
											var selecetd_type = ($(this).data('notif_type'));
											if(notification_type == selecetd_type){
												$(this).find('img').addClass('selected_notification');	
											}
										});
										
									});
									$('.edit_notif_type').on('click',function(){
										$('.edit_notif_type img').removeClass('selected_notification');
										var type = $(this).data('notif_type');
										$('#edit_notification_type').val(type);
										$(this).find('img').addClass('selected_notification');
									});
									$("#edit_alert_details").submit(function(e){
											e.preventDefault();
										}).validate({
											rules:{
												edit_alert_name:{
													required:true,
												},
											},
											messages:{
												edit_alert_name:{
													required:"Alert Name is required",
												},
											},
											submitHandler:function(form){
							
													var frm=$(form).serialize();
															$.ajax({
															url:base_url+'My_services/update_alert_info',
															type:"POST",
															data:{'csrf_test_name':csrf_hash,"frm":frm,},
															success:function(res){
																if(res!= false)
																{
																	var data=JSON.parse(res);
																	var html_1 ='';
																	var final_html ='';
																	if(data != '')
																	{
																		for(var i=0;i<data.length;i++){
																			var notification = '';
																			   if(data[i].notification_type == '1') {
																				notification = 'Remainder';
																			   }else if(data[i].notification_type == '2') {
																					notification = 'Message';   
																			   } else if(data[i].notification_type == '3'){
																					notification = 'Email';   
																			   } else {
																				   notification = '';
																			   }
																		if(i == 0)
																		   {
																			html_1='<div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
														   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">ALERT NAME</span><p>'+data[i].alert_name+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">NOTIFICATION TYPE</span><p>'+notification+'</p></div><div class="col l1 s1 m1 gstaction"><a href="#" class="remove_service_alert_edit" data-alert_id="'+data[i].id+'" data-service_id="'+data[i].service_id+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a><a href="#" class="edit_alert_info" data-alert_id="'+data[i].id+'" data-service_id="'+data[i].service_id+'" data-alert_name="'+data[i].alert_name+'" data-alert_on="'+data[i].alert_on+'" data-set_condition="'+data[i].set_condition+'" data-alert_number="'+data[i].alert_number+'" data-alert_interval="'+data[i].alert_interval+'" data-notification_type="'+data[i].notification_type+'"><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div>';
																		   }
																		   else
																		   {
																			 html_1='<div class="alertmore"><div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+(i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">ALERT NAME</span><p>'+data[i].alert_name+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">NOTIFICATION TYPE</span><p>'+notification+'</p></div><div class="col l1 s1 m1 gstaction"><a href="#" class="remove_service_alert_edit" data-alert_id="'+data[i].id+'" data-service_id="'+data[i].service_id+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a><a href="#" class="edit_alert_info" data-alert_id="'+data[i].id+'" data-service_id="'+data[i].service_id+'" data-alert_name="'+data[i].alert_name+'" data-alert_on="'+data[i].alert_on+'" data-set_condition="'+data[i].set_condition+'" data-alert_number="'+data[i].alert_number+'" data-alert_interval="'+data[i].alert_interval+'" data-notification_type="'+data[i].notification_type+'"><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div></div></div>';
																		  
																		   }
																			 final_html = final_html + html_1;
																			 $("#alert_info_array_edit").html(final_html);
																			// $(".two-i").html(data.length);
																		}
																		Materialize.toast('Alert Information updated successfully', 2000,'green rounded');
																	}
																	else
																	{
																		html_1='<div class="gst-boxs add-gst">Add Alert Data</div>';
																		$("#alert_info_array_edit").html(html_1);
																		Materialize.toast('Add alert information!', 2000,'green rounded');
																	}
																}
																else
																{
																	html_1='<div class="gst-boxs add-gst">Add Alert Data</div>';
																	$("#alert_info_array_edit").html(html_1);
																	Materialize.toast('Error while processing!', 2000,'red rounded');
																}
																$("#edit_alert_details").find("input[type=text], textarea").val("");
																$("#edit_alert").modal('close');
																//location.reload();
														},
												});
											},
										});
									
										$('.open_add_tax').on('click',function(){
											var service = $(this).data('service_id');
											$('#editpage_other-alert').modal('open');
											$('#editpage_other-alert #editpage_tax_service_id').val(service);
										});
										
							$("#edit_add_other_tax").submit(function(e){
								e.preventDefault();
							}).validate({
								rules:{
									tax_name1: {
										required:true,	
									},
									'tax_name[]':{
										required:true,
									},
									'tax_perc[]':{
										required:true,
									},
								},
								messages:{
									tax_name1: {
										required:"Tax Name is required",
									},
									'tax_name[]':{
										required:"Tax Name is required",
									},
									'tax_perc[]':{
										required:"Tax Percentage is required",
									},
								},
								submitHandler:function(form){

										var frm=$(form).serialize();
												$.ajax({
												url:base_url+'My_services/add_tax_info_2',
												type:"POST",
												data:{'csrf_test_name':csrf_hash,"frm":frm,},
												success:function(res){
													if(res!= false)
													{
														var data=JSON.parse(res);
														var html_1 ='';
														var final_html ='';
														if(data != '')
														{
															for(var i=0;i<data.length;i++){
															if(i == 0)
															   {
																html_1='<div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">NAME OF TAX</span><p>'+data[i].tax_name+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">TAX PERCENTAGE</span><p>'+data[i].tax_percentage+'</p></div><div class="col l1 s1 m1 gstaction"><a href="#" class="remove_tax_edit" data-tax_id="'+data[i].id+'" data-service_id="'+data[i].service_id+'" data-tax_name="'+data[i].tax_name+'" data-tax_perc="'+data[i].tax_percentage+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a><a href="#" class="edit_service_tax" data-tax_id="'+data[i].id+'" data-service_id="'+data[i].service_id+'" data-tax_name="'+data[i].tax_name+'" data-tax_percentage="'+data[i].tax_percentage+'""><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div>';
															   }
															   else
															   {
																html_1='<div class="gstmore"><div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">NAME OF TAX</span><p>'+data[i].tax_name+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">TAX PERCENTAGE</span><p>'+data[i].tax_percentage+'</p></div><div class="col l1 s1 m1 gstaction"><a href="#" class="remove_tax_edit" data-tax_id="'+data[i].id+'" data-service_id="'+data[i].service_id+'" data-tax_name="'+data[i].tax_name+'" data-tax_percentage="'+data[i].tax_percentage+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a><a href="#" class="edit_service_tax" data-tax_id="'+data[i].id+'" data-service_id="'+data[i].service_id+'" data-tax_name="'+data[i].tax_name+'" data-tax_percentage="'+data[i].tax_percentage+'""><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div></div>';
															  
															   }
																 final_html = final_html + html_1;
																 $("#tax_info_array_edit").html(final_html);
																 $(".two-i").html(data.length);
															}
															Materialize.toast('TAX Info inserted successfully!', 2000,'green rounded');
														}
														else
														{
															html_1='<div class="gst-boxs add-gst">Add TAX Data</div>';
															$("#tax_info_array_edit").html(html_1);
															Materialize.toast('ADD TAX DATA', 2000,'green rounded');
														}
													}
													else
													{
														Materialize.toast('Error while processing. Please try again', 2000,'red rounded');
													}
													$("#edit_add_other_tax").find("input[type=text], textarea").val("");
													$("#editpage_other-alert").modal('close');
											},
											
									});
								},
							});

								
							var bulk_services = [];
							$("input[id='services_bulk']").on("click",function(){
							if($(this).is(':checked',true)) {
								$(".services_bulk_action").prop('checked', true);
								$(".services_bulk_action:checked").each(function() {
										bulk_services.push($(this).val());
									});
									bulk_services = bulk_services.join(",");
									$('#deactive_multiple_services').attr('data-multi_profiles',bulk_services);
								}
								else {
									$(".services_bulk_action").prop('checked',false);
									bulk_services = [];
									$('#deactive_multiple_services').attr('data-multi_profiles',0);
								}
							});

								
							$("#deactive_multiple_services").on('click', function(e) {
								var services_values = $(this).data('multi_profiles');
								if(services_values == '' || services_values == '0')
								{
									Materialize.toast('Please select a record first', 2000,'red rounded');
								}
								else
								{
									$.ajax({
										url:base_url+'My_services/deactive_multiple',
										type:"POST",
										data:{'csrf_test_name':csrf_hash,"services_values":services_values,},
										success:function(res){
												location.reload();
											},
									});		
								}
							});
							
							$("#set_services_alert").validate({
								rules:{
									select_service:{
										required:true,
									},
									alert_name:{
										required:true,
									},
								},
								messages:{
									select_service:{
										required:"Please Select Service.",
									},
									alert_name:{
										required:"Alert Name is required",
									},
								},
							});
						
						$(document).ready( function () {	
							$(document).on('click' ,'.edit_remove_cmpgst', function(){
									
									var id = $(this).data('id');
									var userid = $(this).data('userid');
									var companyid = $(this).data('companyid');
									
									$('#remove_company_gstin_modal').modal('open');
									$('#remove_company_gstin_modal #remove_cmp_gst_id').val(id);
									$('#remove_company_gstin_modal #remove_cmp_gst_user_id').val(userid);
									$('#remove_company_gstin_modal #remove_cmp_gst_cmp_id').val(companyid);
									
								});
								
								$('.remove_company_gstin_info').on('click',function(){
						
										var gst_id = $('#remove_cmp_gst_id').val();
										var userid = $('#remove_cmp_gst_user_id').val();
										var companyid = $('#remove_cmp_gst_cmp_id').val();

										$.ajax({
											url:base_url+'Company_profile/delete_gst_info',
											type:"post",						
											data:{'csrf_test_name':csrf_hash,"gst_id":gst_id,"userid":userid,"companyid":companyid},
											success:function(res){
												console.log(res);
													//location.reload();
													if(res!= false)
													{
														var data=JSON.parse(res);
														var html_1 ='';
														var final_html ='';
														if(data != '')
														{
															for(var i=0;i<data.length;i++){
																if(data[i].status=="Inactive"){
													   var deac="deactive_record";
												   }else{
													    var deac="";
												   }
												   console.log(deac);
															if(i == 0)
															   {
															    html_1='<div class="gst-boxs add-gst "><div class="col l1 s1 m1 sno"><div class="row">'+
															   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum '+deac+'"><span class="gstlabel">GST No.</span><p>'+data[i].gst_no+'</p></div><div class="col l6 s6 m6 gstplace '+deac+'"><span class="gstlabel">PLACE OF SUPPLY</span><p>'+data[i].place_of_supply+'</p></div><div class="col l1 s1 m1 gstaction"><a class="edit_remove_cmpgst '+deac+'"  data-id="'+data[i].id+'" data-userid="'+data[i].user_id+'" data-companyid="'+data[i].company_id+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a><a class="edit_gst_info" data-id="'+data[i].id+'" data-userid="'+data[i].user_id+'" data-companyid="'+data[i].company_id+'" data-gst_no="'+data[i].gst_no+'" data-gst_place="'+data[i].place_of_supply+'" data-status="'+data[i].status+'"><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div>';
															   }
															   else
															   {
																 html_1='<div class="gstmore"><div class="gst-boxs add-gst "><div class="col l1 s1 m1 sno"><div class="row">'+
															   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum '+deac+'"><span class="gstlabel">GST No.</span><p>'+data[i].gst_no+'</p></div><div class="col l6 s6 m6 gstplace '+deac+'"><span class="gstlabel">PLACE OF SUPPLY</span><p>'+data[i].place_of_supply+'</p></div><div class="col l1 s1 m1 gstaction"><a class="edit_remove_cmpgst '+deac+'"  data-id="'+data[i].id+'" data-userid="'+data[i].user_id+'" data-companyid="'+data[i].company_id+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a><a class="edit_gst_info" data-id="'+data[i].id+'" data-userid="'+data[i].user_id+'" data-companyid="'+data[i].company_id+'" data-gst_no="'+data[i].gst_no+'" data-gst_place="'+data[i].place_of_supply+'" data-status="'+data[i].status+'"><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div></div>';
															   }
																 final_html = final_html + html_1;
																 $("#company_gst_array_list_edit").html(final_html);
																 $(".tot_gst").html(data.length);
															}
															Materialize.toast('GSTIN Details deactivated', 2000,'green rounded');
														}
														else
														{
															html_1='<div class="gst-boxs add-gst">Add GSTIN Data</div>';
															$("#company_gst_array_list_edit").html(html_1);
															$(".tot_gst").html(0);
														}
													}
													else
													{
														//html_1='<div class="gst-boxs add-gst">Add GSTIN Data</div>';
														//$("#company_gst_array_list_edit").html(html_1);
														Materialize.toast('Error while processing!', 2000,'red rounded');
													}
												},
											});										
									});
								
								});	
								$('.editcmp_remove_service').on('click' ,function(){
									
									var id = $(this).data('service_id');
									var company_id = $(this).data('company_id');
									
									$('#remove_company_service_modal').modal('open');
									$('#remove_company_service_modal #remove_cmp_service_id').val(id);
									$('#remove_company_service_modal #remove_service_cmp_id').val(company_id);
								});
								
								$('.remove_company_service_info').on('click',function(){
						
									var service_id = $('#remove_cmp_service_id').val();
									var company_id = $('#remove_service_cmp_id').val();

									$.ajax({
										url:base_url+'Company_profile/delete_service_info',
										type:"post",						
										data:{'csrf_test_name':csrf_hash,"service_id":service_id,"company_id":company_id},
										success:function(res){
												console.log(res);
												if(res != false) {
													Materialize.toast('Service Keyword has been removed!!', 2000,'green rounded');	
												} else {
													Materialize.toast('Error while processing...!!', 2000,'red rounded');	
												}
												location.reload();
											},
										});										
								});
								
									$('.add_new_service_2').on('click',function(){
										var company_id = $(this).data('company_id');
										$('#edit-add-new-service').modal('open');
										$('#edit-add-new-service #add_for_company_id').val(company_id);
									});
								$("#add_new_service_keywords_2").submit(function(e){
										e.preventDefault();
									}).validate({
										rules:{
											keyword_1:{
												required:true,
											},
										},
										messages:{
											keyword_1:{
												required:"Add at least one service keyword",
											},
										},
										submitHandler:function(form){
						
												var frm=$(form).serialize();
														$.ajax({
														url:base_url+'Company_profile/add_service_keywords_2',
														type:"POST",
														data:{'csrf_test_name':csrf_hash,"frm":frm,},
														success:function(res){
															if(res != false)
															{
																Materialize.toast('Service Info has been added', 2000,'green rounded');
																$("#add_new_service_keywords_2").find("input[type=text], textarea").val("");
																$("#edit-add-new-service").modal('close');	
															} else {
																Materialize.toast('Error while processing...!!', 2000,'red rounded');	
															}
															location.reload();
														},
											});
										},
									});
											
								$('.add_new_branch_cmp').on('click',function(){
										var company_id = $(this).data('company_id');
										var user_id = $(this).data('user_id');
										$('#edit-new-branch').modal('open');
										$('#edit-new-branch #add_branch_for_cmp').val(company_id);
										$('#edit-new-branch #add_branch_for_user').val(user_id);
								});
								$("#add_branch_info_2").submit(function(e){
										e.preventDefault();
									}).validate({
										rules:{
											branch_name:{
												required:true,
											},
											branch_city:{
												required:true,
											},
										},
										messages:{
											branch_name:{
												required:"Branch Name is required",
											},
											branch_city:{
												required:"Branch City is required",
											},
										},
										submitHandler:function(form){
						
												var frm=$(form).serialize();
														$.ajax({
														url:base_url+'Company_profile/add_branch_info_2',
														type:"POST",
														data:{'csrf_test_name':csrf_hash,"frm":frm,},
														success:function(res){
															console.log(res);
															if(res != false)
															{
																Materialize.toast('Branch information added', 2000,'green rounded');
																$("#add_branch_info_2").find("input[type=text], textarea").val("");
																$("#edit-new-branch").modal('close');	
															} else {
																Materialize.toast('Error while processing...!!', 2000,'red rounded');	
															}
															location.reload();
														},
											});
										},
									});
									$('.editcmp_remove_branch').on('click' ,function(){
										
										var branch_id = $(this).data('branch_id');
										var company_id = $(this).data('company_id');
										
										$('#remove_company_branch_modal').modal('open');
										$('#remove_company_branch_modal #remove_cmp_branch_id').val(branch_id);
										$('#remove_company_branch_modal #remove_branch_cmp_id').val(company_id);
									});
									
									$('.remove_company_branch_info').on('click',function(){
							
										var branch_id = $('#remove_cmp_branch_id').val();
										var company_id = $('#remove_branch_cmp_id').val();
	
										$.ajax({
											url:base_url+'Company_profile/delete_branch_info',
											type:"post",						
											data:{'csrf_test_name':csrf_hash,"branch_id":branch_id,"company_id":company_id},
											success:function(res){
													console.log(res);
													if(res != false) {
														Materialize.toast('Branch information removed', 2000,'green rounded');	
													} else {
														Materialize.toast('Error while processing...!!', 2000,'red rounded');	
													}
													location.reload();
												},
											});										
									});		
									
								$(document).on('click', '.deactivate_service_2',function(){
										var service_id = $(this).data('service_id');
										$('#viewpage_deactive_service').modal('open');
										$('#viewpage_deactive_service #service_id').val(service_id);
								});
										
								$('.view_deactive_service').on('click',function(){
						
										var service = $('#service_id').val();
						
										$.ajax({
						
											url:base_url+'My_services/deactive_service_2',
						
											type:"POST",
						
											data:{'csrf_test_name':csrf_hash,"service":service,},
						
											success:function(res){
													window.location.href=base_url+'My_services/manage_services';
												},
											});										
								});
								
								$(document).on('click', '.activate_service_2',function(){
										var service_id = $(this).data('service_id');
										$('#viewpage_active_service').modal('open');
										$('#viewpage_active_service #activate_service_id').val(service_id);
								});
								$('.view_active_service').on('click',function(){
						
										var service = $('#activate_service_id').val();
						
										$.ajax({
						
											url:base_url+'My_services/activate_service_2',
						
											type:"POST",
						
											data:{'csrf_test_name':csrf_hash,"service":service,},
						
											success:function(res){
													window.location.href=base_url+'My_services/manage_services';
												},
											});										
								});
								
								$(document).off().on('click', '.print_service' ,function(){
									var service_id = $(this).data('service_id');
									var url = '&flag=' + 1;
									window.location.href = base_url+'My_services/view_service/' + service_id + '?' + url;
								});
						
						$(document).ready( function () {		
								$(document).on('click', '.remove_service_alert_edit' ,function(){
									var alert_id = $(this).data('alert_id');
									var service_id = $(this).data('service_id');
									
									$('#remove_alert_info').modal('open');
									$('#remove_alert_info #remove_alert_id').val(alert_id);
									$('#remove_alert_info #remove_alert_service_id').val(service_id);
								});
				
							$('.edit_remove_alert').on('click',function(){
						
										var alert_id = $('#remove_alert_id').val();
										var service_id = $('#remove_alert_service_id').val();

										$.ajax({
						
											url:base_url+'My_services/delete_alert_info',
						
											type:"post",
						
											data:{'csrf_test_name':csrf_hash,"alert_id":alert_id,"service_id":service_id},
						
											success:function(res){
													if(res!= false)
													{
														var data=JSON.parse(res);
														var html_1 ='';
														var final_html ='';
														if(data != '')
														{
															for(var i=0;i<data.length;i++){
																var notification = '';
																   if(data[i].notification_type == '1') {
																   	notification = 'Remainder';
																   }else if(data[i].notification_type == '2') {
																		notification = 'Message';   
																   } else if(data[i].notification_type == '3'){
																		notification = 'Email';   
																   } else {
																	   notification = '';
																   }
															if(i == 0)
															   {
																html_1='<div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">ALERT NAME</span><p>'+data[i].alert_name+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">NOTIFICATION TYPE</span><p>'+notification+'</p></div><div class="col l1 s1 m1 gstaction"><a href="#" class="remove_service_alert_edit" data-alert_id="'+data[i].id+'" data-service_id="'+data[i].service_id+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a><a href="#" class="edit_alert_info" data-alert_id="'+data[i].id+'" data-service_id="'+data[i].service_id+'" data-alert_name="'+data[i].alert_name+'" data-alert_on="'+data[i].alert_on+'" data-set_condition="'+data[i].set_condition+'" data-alert_number="'+data[i].alert_number+'" data-alert_interval="'+data[i].alert_interval+'" data-notification_type="'+data[i].notification_type+'"><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div>';
															   }
															   else
															   {
																 html_1='<div class="alertmore"><div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">ALERT NAME</span><p>'+data[i].alert_name+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">NOTIFICATION TYPE</span><p>'+notification+'</p></div><div class="col l1 s1 m1 gstaction"><a href="#" class="remove_service_alert_edit" data-alert_id="'+data[i].id+'" data-service_id="'+data[i].service_id+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a><a href="#" class="edit_alert_info" data-alert_id="'+data[i].id+'" data-service_id="'+data[i].service_id+'" data-alert_name="'+data[i].alert_name+'" data-alert_on="'+data[i].alert_on+'" data-set_condition="'+data[i].set_condition+'" data-alert_number="'+data[i].alert_number+'" data-alert_interval="'+data[i].alert_interval+'" data-notification_type="'+data[i].notification_type+'"><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div>';
															  
															   }
																 final_html = final_html + html_1;
																 $("#alert_info_array_edit").html(final_html);
																// $(".two-i").html(data.length);
															}
															Materialize.toast('Alert Info removed successfully !', 2000,'green rounded');
														}
														else
														{
															html_1='<div class="gst-boxs add-gst">Add Alert Data</div>';
															$("#alert_info_array_edit").html(html_1);
															Materialize.toast('Add alert information!', 2000,'green rounded');
														}
													}
													else
													{
														html_1='<div class="gst-boxs add-gst">Add Alert Data</div>';
														$("#alert_info_array_edit").html(html_1);
														Materialize.toast('Error while processing!', 2000,'red rounded');
													}
												},
						
											});										
									});
								});	
									$(document).on('click', '.edit_add_alert_info' ,function(){
										var service_id = $(this).data('service_id');
										
										$('#edit_add-alert').modal('open');
										$('#edit_add-alert #add_alert_for_service').val(service_id);
									});
									$("#edit_add_alert_details").submit(function(e){
										e.preventDefault();
									}).validate({
										rules:{
											alert_name:{
												required:true,
											},
										},
										messages:{
											alert_name:{
												required:"Alert Name is required",
											},
										},
										submitHandler:function(form){
						
												var frm=$(form).serialize();
														$.ajax({
														url:base_url+'My_services/add_alert_info_2',
														type:"POST",
														data:{'csrf_test_name':csrf_hash,"frm":frm,},
														success:function(res){
															if(res!= false)
															{
																var data=JSON.parse(res);
																var html_1 ='';
																var final_html ='';
																if(data != '')
																{
																	for(var i=0;i<data.length;i++){
																		var notification = '';
																		   if(data[i].notification_type == '1') {
																			notification = 'Remainder';
																		   }else if(data[i].notification_type == '2') {
																				notification = 'Message';   
																		   } else if(data[i].notification_type == '3'){
																				notification = 'Email';   
																		   } else {
																			   notification = '';
																		   }
																	if(i == 0)
																	   {
																		html_1='<div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
													   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">ALERT NAME</span><p>'+data[i].alert_name+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">NOTIFICATION TYPE</span><p>'+notification+'</p></div><div class="col l1 s1 m1 gstaction"><a href="#" class="remove_service_alert_edit" data-alert_id="'+data[i].id+'" data-service_id="'+data[i].service_id+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a><a href="#" class="edit_alert_info" data-alert_id="'+data[i].id+'" data-service_id="'+data[i].service_id+'" data-alert_name="'+data[i].alert_name+'" data-alert_on="'+data[i].alert_on+'" data-set_condition="'+data[i].set_condition+'" data-alert_number="'+data[i].alert_number+'" data-alert_interval="'+data[i].alert_interval+'" data-notification_type="'+data[i].notification_type+'"><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div>';
																	   }
																	   else
																	   {
																		 html_1='<div class="alertmore"><div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
													   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">ALERT NAME</span><p>'+data[i].alert_name+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">NOTIFICATION TYPE</span><p>'+notification+'</p></div><div class="col l1 s1 m1 gstaction"><a href="#" class="remove_service_alert_edit" data-alert_id="'+data[i].id+'" data-service_id="'+data[i].service_id+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a><a href="#" class="edit_alert_info" data-alert_id="'+data[i].id+'" data-service_id="'+data[i].service_id+'" data-alert_name="'+data[i].alert_name+'" data-alert_on="'+data[i].alert_on+'" data-set_condition="'+data[i].set_condition+'" data-alert_number="'+data[i].alert_number+'" data-alert_interval="'+data[i].alert_interval+'" data-notification_type="'+data[i].notification_type+'"><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div>';
																	  
																	   }
																		 final_html = final_html + html_1;
																		 $("#alert_info_array_edit").html(final_html);
																		// $(".two-i").html(data.length);
																	}
																	Materialize.toast('An alert has been set!', 2000,'green rounded');
																}
																else
																{
																	html_1='<div class="gst-boxs add-gst">Add Alert Data</div>';
																	$("#alert_info_array_edit").html(html_1);
																	Materialize.toast('Add alert information!', 2000,'green rounded');
																}
															}
															else
															{
																html_1='<div class="gst-boxs add-gst">Add Alert Data</div>';
																$("#alert_info_array_edit").html(html_1);
																Materialize.toast('Error while processing!', 2000,'red rounded');
															}
															$("#edit_add_alert_details").find("input[type=text], textarea").val("");
															$("#edit_add-alert").modal('close');
													},
											});
										},
									});
								
								$(document).ready( function () {	
									$(document).on('click', '.remove_service_alert_session' ,function(){
										var alert_name = $(this).data('alert_name');
										var alert_no = $(this).data('alert_no');
										
										$('#remove_alert_data').modal('open');
										$('#remove_alert_data #remove_alert_name').val(alert_name);
										$('#remove_alert_data #remove_alert_no').val(alert_no);
									});
									
									$('.remove_alert_data_session').on('click',function(){
						
										var alert_name = $('#remove_alert_name').val();
										var alert_no = $('#remove_alert_no').val();

										$.ajax({
						
											url:base_url+'My_services/remove_alert_from_session',
						
											type:"post",
						
											data:{'csrf_test_name':csrf_hash,"alert_name":alert_name,"alert_no":alert_no},
						
											success:function(res){
												if(res!= false)
												{
													var data=JSON.parse(res);
													console.log(data);
													var html_1 ='';
													var final_html ='';
													if(data != '')
													{
														for(var i=0;i<data.length;i++){
															var notification = '';
															   if(data[i].notification_type == '1') {
																notification = 'Remainder';
															   }else if(data[i].notification_type == '2') {
																	notification = 'Message';   
															   } else if(data[i].notification_type == '3'){
																	notification = 'Email';   
															   } else {
																   notification = '';
															   }
															if(i == 0)
														   {
															html_1='<div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
										   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">ALERT NAME</span><p>'+data[i].alert_name+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">NOTIFICATION TYPE</span><p>'+notification+'</p></div><div class="col l1 s1 m1 gstaction"><a href="#" class="remove_service_alert_session" data-alert_name="'+data[i].alert_name+'" data-alert_no="'+data[i].alert_number+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a></div></div></div>';
														   }
														   else
														   {
															 html_1='<div class="alertmore"><div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
										   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">ALERT NAME</span><p>'+data[i].alert_name+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">NOTIFICATION TYPE</span><p>'+notification+'</p></div><div class="col l1 s1 m1 gstaction"><a href="#" class="remove_service_alert_session" data-alert_name="'+data[i].alert_name+'" data-alert_no="'+data[i].alert_number+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a></div></div></div>';
														  
														   }
															 final_html = final_html + html_1;
															 $("#alert_info_array").html(final_html);
															 //$(".two-i").html(data.length);
														}
														Materialize.toast('Alert information has been removed!', 2000,'green rounded');
													}
													else
													{
														html_1='<div class="gst-boxs add-gst">Add Alert Data</div>';
														$("#alert_info_array").html(html_1);
														Materialize.toast('Add alert information!', 2000,'green rounded');
													}
												}
												else
												{
													html_1='<div class="gst-boxs add-gst">Add Alert Data</div>';
													$("#alert_info_array").html(html_1);
													Materialize.toast('Error while processing!', 2000,'red rounded');
												}
												$("#remove_alert_data").modal('close');
												},
						
											});										
									});
								});	
									var bulk_invoices = [];
										$("input[id='invoices_bulk']").on("click",function(){
										if($(this).is(':checked',true)) {
											$(".invoice_bulk_action").prop('checked', true);
											$(".invoice_bulk_action:checked").each(function() {
													bulk_invoices.push($(this).val());
												});
												bulk_invoices = bulk_invoices.join(",");
												$('#deactive_multiple_invoices').attr('data-multi_invoices',bulk_invoices);
											}
											else {
												$(".invoice_bulk_action").prop('checked',false);
												bulk_invoices = [];
												$('#deactive_multiple_invoices').attr('data-multi_invoices',0);
											}
									});
									
									$("#deactive_multiple_invoices").on('click', function() {
										var invoice_values = $(this).data('multi_invoices');
										if(invoice_values == '' || invoice_values == '0')
										{
											Materialize.toast('Please select a record first', 2000,'red rounded');
										}
										else
										{
											$.ajax({
												url:base_url+'My_invoices/deactive_multiple',
												type:"POST",
												data:{'csrf_test_name':csrf_hash,"invoice_values":invoice_values,},
												success:function(res){
														location.reload();
													},
											});		
										}
									});
									
									 $("#create_invoice").validate({
										rules:{
											invoice_date:{
												required:true,
											},
											customer_id:{
												required:true,
											},
										},
										messages:{
											invoice_date:{
												required:"Invoice Date is required",
											},
											customer_id:{
												required:"Please Select Customer",
											},
										},
									});
									
									 $("#edit_invoice").validate({
										rules:{
											invoice_date:{
												required:true,
											},
											customer_id:{
												required:true,
											},
										},
										messages:{
											invoice_date:{
												required:"Invoice Date is required",
											},
											customer_id:{
												required:"Please Select Customer",
											},
										},
									});
									
									$('.mail_all_invoice').on('click',function(){
										$.ajax({
											url:base_url+'My_invoices/send_email_to_all_customers',
											type:"POST",
											data:{'csrf_test_name':csrf_hash,},
											success:function(res){
														Materialize.toast('Email sent to all clients', 2000,'green rounded');
												},
											});										
									});

										
								$("#invoice_new_service").submit(function(e){
									e.preventDefault();
								}).validate({
									rules:{
										invoice_service_name:{
											required:true,
										},
										invoice_hsn_sac_no:{
											required:true,
										},
									},
									messages:{
										invoice_service_name:{
											required:"Item / Service Name is required",
										},
										invoice_hsn_sac_no:{
											required:"HSN / SAC Number is required",
										},
									},
									submitHandler:function(form){
											var frm=$(form).serialize();
													$.ajax({
													url:base_url+'My_services/add_new_service_from_invoice',
													type:"POST",
													data:{'csrf_test_name':csrf_hash,"frm":frm,},
													success:function(res){
															if(res != false)
															{
																$("#invoice_new_service").find("input[type=text],textarea,option").val("");
																$('#invoice_add_new_service').modal('close');
																$("#selected_service").html(res);
																$("#selected_service").parents('.input-field').addClass('label-active');
																$('#selected_service').material_select();
																Materialize.toast('Services Details has been inserted successfully.', 2000,'green rounded');
															}										
														},
										});
									},
								});
								
								$('#document_type').on('change', function () {
									$.ajax({
										'url':base_url+'My_invoices/generate_invoice_no',
										type:"POST",
										data:{
											'csrf_test_name':csrf_hash,
											'doc_type':$(this).val(),
											'company_id':$('#company_name').val(),
										},
										success:function(res){
											$("#invoice_no").val(res);
										},
									});
										if($(this).val() == '5') {
											$(".show_sales_export").css('display','block');
										}
										else{
											$(".show_sales_export").css('display','none');
										}
										if($(this).val() == '4') {
											$(".show_sales_recurring").css('display','block');
										}
										else{
											$(".show_sales_recurring").css('display','none');
										}
								});
								
								$("#create_new_service_m").validate({
									rules:{
										service_name:{
											required:true,
										},
										han_sac_no:{
											required:true,
										},
										sku:{
											required:true,
										},
									},
									messages:{
										service_name:{
											required:"Item Name is required",
										},
										han_sac_no:{
											required:"HSN / SAC is required",
										},
										sku:{
											required:"SKU is required",
										},
									},
								});
								
			$("#add_other_tax_m").submit(function(e){
				e.preventDefault();
			}).validate({
				rules:{

					'tax_name[]':{
						required:true,
					},
					'tax_perc[]':{
						required:true,
					},
				},
				messages:{

					'tax_name[]':{
						required:"Tax Name is required",
					},
					'tax_perc[]':{
						required:"Tax Percentage is required",
					},
				},
				submitHandler:function(form){

						var frm=$(form).serialize();
								$.ajax({
								url:base_url+'My_services/add_other_tax',
								type:"POST",
								data:{'csrf_test_name':csrf_hash,"frm":frm,},
								success:function(res){
									if(res!= false)
									{
										var data=JSON.parse(res);
										var html_1 ='';
										var final_html ='';
										
										if(data != '')
										{
											for(var i=0;i<data.length;i++){
											if(i == 0)
											   {
												html_1='<div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">NAME OF TAX</span><p>'+data[i].tax_name+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">TAX PERCENTAGE</span><p>'+data[i].tax_percentage+'</p></div><div class="col l1 s1 m1 gstaction"><a href="#" class="remove_tax_m" data-tax_name="'+data[i].tax_name+'" data-tax_perc="'+data[i].tax_percentage+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a></div></div></div>';
											   }
											   else
											   {
												 html_1='<div class="gstmore"><div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">NAME OF TAX</span><p>'+data[i].tax_name+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">TAX PERCENTAGE</span><p>'+data[i].tax_percentage+'</p></div><div class="col l1 s1 m1 gstaction"><a href="#" class="remove_tax_m" data-tax_name="'+data[i].tax_name+'" data-tax_perc="'+data[i].tax_percentage+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a></div></div></div></div>';
											  
											   }
												 final_html = final_html + html_1;
												 $("#tax_info_array_m").html(final_html);
												 $(".tot_other_tax").html(data.length);
											}
											 Materialize.toast('Tax Information have been set', 2000,'green rounded');
										}
										else
										{
											html_1='<div class="gst-boxs add-gst">Add TAX Data</div>';
											$("#tax_info_array_m").html(html_1);
											$(".tot_other_tax").html(0);
										}
									}
									else
									{
										Materialize.toast('Error while processing. Please try again', 2000,'red rounded');
									}
									$("#add_other_tax_m").find("input[type=text], textarea").val("");
									$("#other-alert").modal('close');
									
						$(document).ready( function () {			
								$(document).on('click', '.remove_tax_m', function(){
									
									var tax_name = $(this).data('tax_name');
									var tax_perc = $(this).data('tax_perc');
									
									$('#remove_tax_data_m').modal('open');
									$('#remove_tax_data_m #remove_tax_name').val(tax_name);
									$('#remove_tax_data_m #remove_tax_perc').val(tax_perc);
									
								});
			
									$('.remove_tax_data_m').off().on('click',function(){
						
										var tax_name = $('#remove_tax_name').val();
										var tax_perc = $('#remove_tax_perc').val();
						
										$.ajax({
						
											url:base_url+'My_services/remove_tax_info',
						
											type:"post",
						
											data:{'csrf_test_name':csrf_hash,"tax_name":tax_name,"tax_perc":tax_perc},
						
											success:function(res){
													if(res!= false)
													{
														var data=JSON.parse(res);
														console.log(data);
														var html_1 ='';
														var final_html ='';
														if(data != '')
														{
															for(var i=0;i<data.length;i++){
															if(i == 0)
															   {
																html_1='<div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">NAME OF TAX</span><p>'+data[i].tax_name+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">TAX PERCENTAGE</span><p>'+data[i].tax_percentage+'</p></div><div class="col l1 s1 m1 gstaction"><a href="#" class="remove_tax_m" data-tax_name="'+data[i].tax_name+'" data-tax_perc="'+data[i].tax_percentage+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a></div></div></div>';
															   }
															   else
															   {
																html_1='<div class="gstmore"><div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">NAME OF TAX</span><p>'+data[i].tax_name+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">TAX PERCENTAGE</span><p>'+data[i].tax_percentage+'</p></div><div class="col l1 s1 m1 gstaction"><a href="#" class="remove_tax_m" data-tax_name="'+data[i].tax_name+'" data-tax_perc="'+data[i].tax_percentage+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a></div></div></div></div>';
															  
															   }
																 final_html = final_html + html_1;
																 $("#tax_info_array_m").html(final_html);
																 $(".tot_other_tax").html(data.length);
															}
															 Materialize.toast('Tax Information has been removed', 2000,'green rounded');
														}
														else
														{
															html_1='<div class="gst-boxs add-gst">Add TAX Data</div>';
															$("#tax_info_array_m").html(html_1);
															$(".tot_other_tax").html(0);
														}
													}
													else
													{
														html_1='<div class="gst-boxs add-gst">Add TAX Data</div>';
														$("#tax_info_array_m").html(html_1);
														$(".tot_other_tax").html(0);
													}
												},
						
											});										
									});
									
									});
							},
							
					});
				},
			});
			
			$("#add_stock").submit(function(e){
				e.preventDefault();
			}).validate({
				rules:{
					quantity:{
						required:true,
					},
					rate:{
						required:true,
					},
				},
				messages:{
					quantity:{
						required:"Quantity is required",
					},
					rate:{
						required:"Rate is required",
					},
				},
				submitHandler:function(form){
						var frm=$(form).serialize();
								$.ajax({
								url:base_url+'My_services/add_stock_info',
								type:"POST",
								data:{'csrf_test_name':csrf_hash,"frm":frm,},
								success:function(res){
										console.log(res);
										$("#add_stock").find("input[type=text], textarea").val("");
										$("#add-stock").modal('close');
										Materialize.toast('Stock Details have been set.', 2000,'green rounded');									
									},
					});
				},
			});
			
			$("#add_alert_details_m").submit(function(e){
				e.preventDefault();
			}).validate({
				rules:{
					alert_name:{
						required:true,
					},
				},
				messages:{
					alert_name:{
						required:"Alert Name is required",
					},
				},
				submitHandler:function(form){

						var frm=$(form).serialize();
								$.ajax({
								url:base_url+'My_services/add_alert_info',
								type:"post",
								data:{'csrf_test_name':csrf_hash,"frm":frm,},
								success:function(res){
									if(res!= false)
									{
										var data=JSON.parse(res);
										var html_1 ='';
										var final_html ='';
										if(data != '')
										{
											for(var i=0;i<data.length;i++){
												var notification = '';
												   if(data[i].notification_type == '1') {
													notification = 'Remainder';
												   }else if(data[i].notification_type == '2') {
														notification = 'Message';   
												   } else if(data[i].notification_type == '3'){
														notification = 'Email';   
												   } else {
													   notification = '';
												   }
												if(i == 0)
											   {
												html_1='<div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
							   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">ALERT NAME</span><p>'+data[i].alert_name+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">NOTIFICATION TYPE</span><p>'+notification+'</p></div><div class="col l1 s1 m1 gstaction"><a href="#" class="remove_service_alert_session" data-alert_name="'+data[i].alert_name+'" data-alert_no="'+data[i].alert_number+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a></div></div></div>';
											   }
											   else
											   {
												 html_1='<div class="alertmore"><div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
							   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">ALERT NAME</span><p>'+data[i].alert_name+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">NOTIFICATION TYPE</span><p>'+notification+'</p></div><div class="col l1 s1 m1 gstaction"><a href="#" class="remove_service_alert_session" data-alert_name="'+data[i].alert_name+'" data-alert_no="'+data[i].alert_number+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a></div></div></div>';
											  
											   }
												 final_html = final_html + html_1;
												 $("#alert_info_array_m").html(final_html);
												// $(".two-i").html(data.length);
											}
											Materialize.toast('An alert has been set!', 2000,'green rounded');
										}
										else
										{
											html_1='<div class="gst-boxs add-gst">Add Alert Data</div>';
											$("#alert_info_array_m").html(html_1);
											Materialize.toast('Add alert information!', 2000,'green rounded');
										}
									}
									else
									{
										html_1='<div class="gst-boxs add-gst">Add Alert Data</div>';
										$("#alert_info_array_m").html(html_1);
										Materialize.toast('Error while processing!', 2000,'red rounded');
									}
									$("#add_alert_details_m").find("input[type=text], textarea").val("");
									$('.notif_type img').removeClass('selected_notification');
									$("#add-alert").modal('close');
							},
					});
				},
			});
			
			$(document).ready( function() {	
					$(document).on('click', '.remove_service_alert_session' ,function(){
						var alert_name = $(this).data('alert_name');
						var alert_no = $(this).data('alert_no');
						
						$('#remove_alert_data_m').modal('open');
						$('#remove_alert_data_m #remove_alert_name').val(alert_name);
						$('#remove_alert_data_m #remove_alert_no').val(alert_no);
					});
					
					$('.remove_alert_data_session').off().on('click',function(){
		
						var alert_name = $('#remove_alert_name').val();
						var alert_no = $('#remove_alert_no').val();

						$.ajax({
		
							url:base_url+'My_services/remove_alert_from_session',
		
							type:"post",
		
							data:{'csrf_test_name':csrf_hash,"alert_name":alert_name,"alert_no":alert_no},
		
							success:function(res){
								if(res!= false)
								{
									var data=JSON.parse(res);
									console.log(data);
									var html_1 ='';
									var final_html ='';
									if(data != '')
									{
										for(var i=0;i<data.length;i++){
											var notification = '';
											   if(data[i].notification_type == '1') {
												notification = 'Remainder';
											   }else if(data[i].notification_type == '2') {
													notification = 'Message';   
											   } else if(data[i].notification_type == '3'){
													notification = 'Email';   
											   } else {
												   notification = '';
											   }
											if(i == 0)
										   {
											html_1='<div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
						   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">ALERT NAME</span><p>'+data[i].alert_name+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">NOTIFICATION TYPE</span><p>'+notification+'</p></div><div class="col l1 s1 m1 gstaction"><a href="#" class="remove_service_alert_session" data-alert_name="'+data[i].alert_name+'" data-alert_no="'+data[i].alert_number+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a></div></div></div>';
										   }
										   else
										   {
											 html_1='<div class="alertmore"><div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
						   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">ALERT NAME</span><p>'+data[i].alert_name+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">NOTIFICATION TYPE</span><p>'+notification+'</p></div><div class="col l1 s1 m1 gstaction"><a href="#" class="remove_service_alert_session" data-alert_name="'+data[i].alert_name+'" data-alert_no="'+data[i].alert_number+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a></div></div></div>';
										  
										   }
											 final_html = final_html + html_1;
											 $("#alert_info_array_m").html(final_html);
											 //$(".two-i").html(data.length);
										}
										Materialize.toast('Alert information has been removed!', 2000,'green rounded');
									}
									else
									{
										html_1='<div class="gst-boxs add-gst">Add Alert Data</div>';
										$("#alert_info_array_m").html(html_1);
										Materialize.toast('Add alert information!', 2000,'green rounded');
									}
								}
								else
								{
									html_1='<div class="gst-boxs add-gst">Add Alert Data</div>';
									$("#alert_info_array_m").html(html_1);
									Materialize.toast('Alert information has been removed!', 2000,'green rounded');
								}
								$("#remove_alert_data").modal('close');
								},
		
							});										
					});
			});
			
			$('.update_stock').on('click',function(){
				var service = $(this).data('service');
				var qty = $(this).data('qty');
				var rate = $(this).data('rate');
				var value = $(this).data('value');
				$('#update-stock').modal('open');
				$('#update-stock #edit_stock_service_id').val(service);
				$('#update-stock #quantity').val(qty);
				$('#update-stock #rate').val(rate);
				$('#update-stock #value').val(value);
			});
			
			$("#edit_stock").submit(function(e){
				e.preventDefault();
			}).validate({
				rules:{
					quantity:{
						required:true,
					},
					rate:{
						required:true,
					},
				},
				messages:{
					quantity:{
						required:"Quantity is required",
					},
					rate:{
						required:"Rate is required",
					},
				},
				submitHandler:function(form){
						var frm=$(form).serialize();
								$.ajax({
								url:base_url+'My_services/update_stock_data',
								type:"POST",
								data:{'csrf_test_name':csrf_hash,"frm":frm,},
								success:function(res){
									if(res != false) {
										$("#edit_stock").find("input[type=text], textarea").val("");
										$("#update-stock").modal('close');
										Materialize.toast('Stock Details has been updated successfully.', 2000,'green rounded');
										location.reload();
									} else {
										Materialize.toast('Error while processing!!', 2000,'red rounded');
									}
																			
									},
					});
				},
			});
			
			
			$(document).ready( function() {
				$(document).on('click', '.edit_service_tax_m' ,function(){
				$('#edit_tax_modal_m').modal('open');
				var tax_id = $(this).data('tax_id');
				var service_id = $(this).data('service_id');
				var tax_name = $(this).data('tax_name');
				var tax_percentage = $(this).data('tax_percentage');
				
				$('#edit_tax_modal_m #edit_tax_id').val(tax_id);
				$('#edit_tax_modal_m #edit_tax_service_id').val(service_id);
				$('#edit_tax_modal_m #edit_tax_name').val(tax_name);
				$('#edit_tax_modal_m #edit_tax_perc').val(tax_percentage);
			});
			
			$("#edit_tax_service_m").submit(function(e){
				e.preventDefault();
			}).validate({
				rules:{

					edit_tax_name:{
						required:true,
					},
					edit_tax_perc:{
						required:true,
					},
				},
				messages:{

					edit_tax_name:{
						required:"TAX Name is required",
					},
					edit_tax_perc:{
						required:"TAX Percentage is required",
					},
				},
				submitHandler:function(form){
						var frm=$(form).serialize();
								$.ajax({
								url:base_url+'My_services/edit_tax_info',
								type:"POST",
								data:{'csrf_test_name':csrf_hash,"frm":frm,},
								success:function(res){
									//console.log(res);
									if(res!= false)
									{
										var data=JSON.parse(res);
										var html_1 ='';
										var final_html ='';
										if(data != '')
										{
											for(var i=0;i<data.length;i++){
											if(i == 0)
											   {
												html_1='<div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">NAME OF TAX</span><p>'+data[i].tax_name+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">TAX PERCENTAGE</span><p>'+data[i].tax_percentage+'</p></div><div class="col l1 s1 m1 gstaction"><a href="#" class="remove_tax_edit_m" data-tax_id="'+data[i].id+'" data-service_id="'+data[i].service_id+'" data-tax_name="'+data[i].tax_name+'" data-tax_perc="'+data[i].tax_percentage+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a><a href="#" class="edit_service_tax_m" data-tax_id="'+data[i].id+'" data-service_id="'+data[i].service_id+'" data-tax_name="'+data[i].tax_name+'" data-tax_percentage="'+data[i].tax_percentage+'""><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div>';
											   }
											   else
											   {
												 html_1='<div class="gstmore"><div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">NAME OF TAX</span><p>'+data[i].tax_name+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">TAX PERCENTAGE</span><p>'+data[i].tax_percentage+'</p></div><div class="col l1 s1 m1 gstaction"><a href="#" class="remove_tax_edit_m" data-tax_id="'+data[i].id+'" data-service_id="'+data[i].service_id+'" data-tax_name="'+data[i].tax_name+'" data-tax_percentage="'+data[i].tax_percentage+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a><a href="#" class="edit_service_tax_m" data-tax_id="'+data[i].id+'" data-service_id="'+data[i].service_id+'" data-tax_name="'+data[i].tax_name+'" data-tax_percentage="'+data[i].tax_percentage+'""><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div></div>';
											  
											   }
												 final_html = final_html + html_1;
												 $("#tax_info_array_m").html(final_html);
												 $(".tot_other_tax").html(data.length);
											}
											 Materialize.toast('Tax information updated successfully !', 2000,'green rounded');
										}
										else
										{
											html_1='<div class="gst-boxs add-gst">Add TAX Data</div>';
											$("#tax_info_array_m").html(html_1);
											 Materialize.toast('ADD Tax DATA!', 2000,'green rounded');
										}
									}
									else
									{
										Materialize.toast('Error while processing. Please try again', 2000,'red rounded');
									}
									$("#edit_tax_service_m").find("input[type=text], textarea").val("");
									$("#edit_tax_modal_m").modal('close');
									
							},
							
					});
				},
				});
				
				$(document).on('click', '.remove_tax_edit_m' ,function(){
					
				var tax_id = $(this).data('tax_id');
				var service_id = $(this).data('service_id');
				
				$('#edit_remove_tax_data_m').modal('open');
				$('#edit_remove_tax_data_m #remove_tax_id').val(tax_id);
				$('#edit_remove_tax_data_m #remove_service_id').val(service_id);

				});
				
				$('.edit_remove_tax_m').on('click',function(){
						
										var tax_id = $('#remove_tax_id').val();
										var service_id = $('#remove_service_id').val();

										$.ajax({
						
											url:base_url+'My_services/delete_tax_info',
						
											type:"post",
						
											data:{'csrf_test_name':csrf_hash,"tax_id":tax_id,"service_id":service_id},
						
											success:function(res){
													if(res!= false)
													{
														var data=JSON.parse(res);
														var html_1 ='';
														var final_html ='';
														if(data != '')
														{
															for(var i=0;i<data.length;i++){
															if(i == 0)
															   {
																html_1='<div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">NAME OF TAX</span><p>'+data[i].tax_name+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">TAX PERCENTAGE</span><p>'+data[i].tax_percentage+'</p></div><div class="col l1 s1 m1 gstaction"><a href="#" class="remove_tax_edit_m" data-tax_id="'+data[i].id+'" data-service_id="'+data[i].service_id+'" data-tax_name="'+data[i].tax_name+'" data-tax_perc="'+data[i].tax_percentage+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a><a href="#" class="edit_service_tax_m" data-tax_id="'+data[i].id+'" data-service_id="'+data[i].service_id+'" data-tax_name="'+data[i].tax_name+'" data-tax_percentage="'+data[i].tax_percentage+'""><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div>';
															   }
															   else
															   {
																 html_1='<div class="gstmore"><div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">NAME OF TAX</span><p>'+data[i].tax_name+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">TAX PERCENTAGE</span><p>'+data[i].tax_percentage+'</p></div><div class="col l1 s1 m1 gstaction"><a href="#" class="remove_tax_edit_m" data-tax_id="'+data[i].id+'" data-service_id="'+data[i].service_id+'" data-tax_name="'+data[i].tax_name+'" data-tax_percentage="'+data[i].tax_percentage+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a><a href="#" class="edit_service_tax_m" data-tax_id="'+data[i].id+'" data-service_id="'+data[i].service_id+'" data-tax_name="'+data[i].tax_name+'" data-tax_percentage="'+data[i].tax_percentage+'""><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div></div>';
															  
															   }
																 final_html = final_html + html_1;
																 $("#tax_info_array_m").html(final_html);
																 $(".tot_other_tax").html(data.length);
															}
															Materialize.toast('TAX Info removed successfully !', 2000,'green rounded');
														}
														else
														{
															//html_1='<div class="gst-boxs add-gst">Add GSTIN Data</div>';
															//$("#tax_info_array_m").html(html_1);
															Materialize.toast('ADD TAX DATA!', 2000,'green rounded');
														}
													}
													else
													{
														//html_1='<div class="gst-boxs add-gst">Add GSTIN Data</div>';
														//$("#tax_info_array_m").html(html_1);
														Materialize.toast('Error while processing!', 2000,'red rounded');
													}
												},
						
											});										
									});
			});
			
			$('.open_add_tax_m').on('click',function(){
				var service = $(this).data('service_id');
				$('#edit_other-tax').modal('open');
				$('#edit_other-tax #editpage_tax_service_id').val(service);
			});
			
			$("#edit_add_other_tax_m").submit(function(e){
								e.preventDefault();
							}).validate({
								rules:{
									tax_name1: {
										required:true,	
									},
									'tax_name[]':{
										required:true,
									},
									'tax_perc[]':{
										required:true,
									},
								},
								messages:{
									tax_name1: {
										required:"Tax Name is required",
									},
									'tax_name[]':{
										required:"Tax Name is required",
									},
									'tax_perc[]':{
										required:"Tax Percentage is required",
									},
								},
								submitHandler:function(form){

										var frm=$(form).serialize();
												$.ajax({
												url:base_url+'My_services/add_tax_info_2',
												type:"POST",
												data:{'csrf_test_name':csrf_hash,"frm":frm,},
												success:function(res){
													if(res!= false)
													{
														var data=JSON.parse(res);
														var html_1 ='';
														var final_html ='';
														if(data != '')
														{
															for(var i=0;i<data.length;i++){
															if(i == 0)
															   {
																html_1='<div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">NAME OF TAX</span><p>'+data[i].tax_name+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">TAX PERCENTAGE</span><p>'+data[i].tax_percentage+'</p></div><div class="col l1 s1 m1 gstaction"><a href="#" class="remove_tax_edit_m" data-tax_id="'+data[i].id+'" data-service_id="'+data[i].service_id+'" data-tax_name="'+data[i].tax_name+'" data-tax_perc="'+data[i].tax_percentage+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a><a href="#" class="edit_service_tax_m" data-tax_id="'+data[i].id+'" data-service_id="'+data[i].service_id+'" data-tax_name="'+data[i].tax_name+'" data-tax_percentage="'+data[i].tax_percentage+'""><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div>';
															   }
															   else
															   {
																html_1='<div class="gstmore"><div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">NAME OF TAX</span><p>'+data[i].tax_name+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">TAX PERCENTAGE</span><p>'+data[i].tax_percentage+'</p></div><div class="col l1 s1 m1 gstaction"><a href="#" class="remove_tax_edit_m" data-tax_id="'+data[i].id+'" data-service_id="'+data[i].service_id+'" data-tax_name="'+data[i].tax_name+'" data-tax_percentage="'+data[i].tax_percentage+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a><a href="#" class="edit_service_tax_m" data-tax_id="'+data[i].id+'" data-service_id="'+data[i].service_id+'" data-tax_name="'+data[i].tax_name+'" data-tax_percentage="'+data[i].tax_percentage+'""><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div></div>';
															  
															   }
																 final_html = final_html + html_1;
																 $("#tax_info_array_m").html(final_html);
																 $(".tot_other_tax").html(data.length);
															}
															Materialize.toast('TAX Info inserted successfully!', 2000,'green rounded');
														}
														else
														{
															html_1='<div class="gst-boxs add-gst">Add TAX Data</div>';
															$("#tax_info_array_m").html(html_1);
															$(".tot_other_tax").html(0);
															Materialize.toast('ADD TAX DATA', 2000,'green rounded');
														}
													}
													else
													{
														Materialize.toast('Error while processing. Please try again', 2000,'red rounded');
													}
													$("#edit_add_other_tax_m").find("input[type=text], textarea").val("");
													$("#edit_other-tax").modal('close');
											},
									});
								},
							});
							
							$('.edit_add_alert_info_m').on('click' ,function(){
										var service_id = $(this).data('service_id');
										
										$('#edit_add-alert_2').modal('open');
										$('#edit_add-alert_2 #add_alert_for_service').val(service_id);
							});
									$("#edit_add_alert_details_m").submit(function(e){
										e.preventDefault();
									}).validate({
										rules:{
											alert_name:{
												required:true,
											},
										},
										messages:{
											alert_name:{
												required:"Alert Name is required",
											},
										},
										submitHandler:function(form){
						
												var frm=$(form).serialize();
														$.ajax({
														url:base_url+'My_services/add_alert_info_2',
														type:"POST",
														data:{'csrf_test_name':csrf_hash,"frm":frm,},
														success:function(res){
															if(res!= false)
															{
																var data=JSON.parse(res);
																var html_1 ='';
																var final_html ='';
																if(data != '')
																{
																	for(var i=0;i<data.length;i++){
																		var notification = '';
																		   if(data[i].notification_type == '1') {
																			notification = 'Remainder';
																		   }else if(data[i].notification_type == '2') {
																				notification = 'Message';   
																		   } else if(data[i].notification_type == '3'){
																				notification = 'Email';   
																		   } else {
																			   notification = '';
																		   }
																	if(i == 0)
																	   {
																		html_1='<div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
													   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">ALERT NAME</span><p>'+data[i].alert_name+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">NOTIFICATION TYPE</span><p>'+notification+'</p></div><div class="col l1 s1 m1 gstaction"><a href="#" class="remove_service_alert_edit_m" data-alert_id="'+data[i].id+'" data-service_id="'+data[i].service_id+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a><a href="#" class="edit_alert_info_m" data-alert_id="'+data[i].id+'" data-service_id="'+data[i].service_id+'" data-alert_name="'+data[i].alert_name+'" data-alert_on="'+data[i].alert_on+'" data-set_condition="'+data[i].set_condition+'" data-alert_number="'+data[i].alert_number+'" data-alert_interval="'+data[i].alert_interval+'" data-notification_type="'+data[i].notification_type+'"><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div>';
																	   }
																	   else
																	   {
																		 html_1='<div class="alertmore"><div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
													   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">ALERT NAME</span><p>'+data[i].alert_name+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">NOTIFICATION TYPE</span><p>'+notification+'</p></div><div class="col l1 s1 m1 gstaction"><a href="#" class="remove_service_alert_edit_m" data-alert_id="'+data[i].id+'" data-service_id="'+data[i].service_id+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a><a href="#" class="edit_alert_info_m" data-alert_id="'+data[i].id+'" data-service_id="'+data[i].service_id+'" data-alert_name="'+data[i].alert_name+'" data-alert_on="'+data[i].alert_on+'" data-set_condition="'+data[i].set_condition+'" data-alert_number="'+data[i].alert_number+'" data-alert_interval="'+data[i].alert_interval+'" data-notification_type="'+data[i].notification_type+'"><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div>';
																	  
																	   }
																		 final_html = final_html + html_1;
																		 $("#alert_info_array_m").html(final_html);
																		// $(".two-i").html(data.length);
																	}
																	Materialize.toast('An alert has been set!', 2000,'green rounded');
																}
																else
																{
																	html_1='<div class="gst-boxs add-gst">Add Alert Data</div>';
																	$("#alert_info_array_m").html(html_1);
																	Materialize.toast('Add alert information!', 2000,'green rounded');
																}
															}
															else
															{
																html_1='<div class="gst-boxs add-gst">Add Alert Data</div>';
																$("#alert_info_array_m").html(html_1);
																Materialize.toast('Error while processing!', 2000,'red rounded');
															}
															$("#edit_add_alert_details_m").find("input[type=text], textarea").val("");
															$('.notif_type img').removeClass('selected_notification');
															$("#edit_add-alert_2").modal('close');
													},
											});
										},
									});
						
						$(document).ready( function() {			
								$(document).on('click' ,'.remove_service_alert_edit_m', function(){
									var alert_id = $(this).data('alert_id');
									var service_id = $(this).data('service_id');
									
									$('#remove_alert_info_m').modal('open');
									$('#remove_alert_info_m #remove_alert_id').val(alert_id);
									$('#remove_alert_info_m #remove_alert_service_id').val(service_id);
								});
				
							$('.edit_remove_alert_m').on('click',function(){
						
										var alert_id = $('#remove_alert_id').val();
										var service_id = $('#remove_alert_service_id').val();

										$.ajax({
						
											url:base_url+'My_services/delete_alert_info',
						
											type:"post",
						
											data:{"alert_id":alert_id,"service_id":service_id},
						
											success:function(res){
													if(res!= false)
													{
														var data=JSON.parse(res);
														var html_1 ='';
														var final_html ='';
														if(data != '')
														{
															for(var i=0;i<data.length;i++){
																var notification = '';
																   if(data[i].notification_type == '1') {
																   	notification = 'Remainder';
																   }else if(data[i].notification_type == '2') {
																		notification = 'Message';   
																   } else if(data[i].notification_type == '3'){
																		notification = 'Email';   
																   } else {
																	   notification = '';
																   }
															if(i == 0)
															   {
																html_1='<div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">ALERT NAME</span><p>'+data[i].alert_name+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">NOTIFICATION TYPE</span><p>'+notification+'</p></div><div class="col l1 s1 m1 gstaction"><a href="#" class="remove_service_alert_edit_m" data-alert_id="'+data[i].id+'" data-service_id="'+data[i].service_id+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a><a href="#" class="edit_alert_info_m" data-alert_id="'+data[i].id+'" data-service_id="'+data[i].service_id+'" data-alert_name="'+data[i].alert_name+'" data-alert_on="'+data[i].alert_on+'" data-set_condition="'+data[i].set_condition+'" data-alert_number="'+data[i].alert_number+'" data-alert_interval="'+data[i].alert_interval+'" data-notification_type="'+data[i].notification_type+'"><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div>';
															   }
															   else
															   {
																 html_1='<div class="alertmore"><div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">ALERT NAME</span><p>'+data[i].alert_name+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">NOTIFICATION TYPE</span><p>'+notification+'</p></div><div class="col l1 s1 m1 gstaction"><a href="#" class="remove_service_alert_edit_m" data-alert_id="'+data[i].id+'" data-service_id="'+data[i].service_id+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a><a href="#" class="edit_alert_info_m" data-alert_id="'+data[i].id+'" data-service_id="'+data[i].service_id+'" data-alert_name="'+data[i].alert_name+'" data-alert_on="'+data[i].alert_on+'" data-set_condition="'+data[i].set_condition+'" data-alert_number="'+data[i].alert_number+'" data-alert_interval="'+data[i].alert_interval+'" data-notification_type="'+data[i].notification_type+'"><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div>';
															  
															   }
																 final_html = final_html + html_1;
																 $("#alert_info_array_m").html(final_html);
																// $(".two-i").html(data.length);
															}
															Materialize.toast('Alert Info removed successfully !', 2000,'green rounded');
														}
														else
														{
															html_1='<div class="gst-boxs add-gst">Add Alert Data</div>';
															$("#alert_info_array_m").html(html_1);
															Materialize.toast('Add alert information!', 2000,'green rounded');
														}
													}
													else
													{
														html_1='<div class="gst-boxs add-gst">Add Alert Data</div>';
														$("#alert_info_array_m").html(html_1);
														Materialize.toast('Error while processing!', 2000,'red rounded');
													}
												},
						
											});										
									});
								});	
								
								$(document).ready( function () { 
									$(document).on('click', '.edit_alert_info_m' ,function(){
										var alert_id = $(this).data('alert_id');
										var service_id = $(this).data('service_id');
										var alert_name = $(this).data('alert_name');
										var alert_on = $(this).data('alert_on');
										var condition = $(this).data('set_condition');
										var alert_number = $(this).data('alert_number');
										var alert_interval = $(this).data('alert_interval');
										var notification_type = $(this).data('notification_type');
										
										$('#edit_alert').modal('open');
										$('#edit_alert #edit_alert_id').val(alert_id);
										$('#edit_alert #edit_alert_name').val(alert_name);
										$('#edit_alert #edit_alert_service_id').val(service_id);
										$('#edit_alert #edit_set_alert_on option[value="'+alert_on+'"]').attr('selected','selected');
										$('#edit_alert #edit_set_alert_on').material_select();
										
										$('#edit_alert #edit_condition option[value="'+condition+'"]').attr('selected','selected');
										$('#edit_alert #edit_condition').material_select();
										
										$('#edit_alert #edit_alert_number').val(alert_number);
										
										$('#edit_alert #edit_alert_interval option[value="'+alert_interval+'"]').attr('selected','selected');
										$('#edit_alert #edit_alert_interval').material_select();
										
										$('#edit_alert #edit_notification_type').val(notification_type);
										$(".edit_notif_type").each(function() {
											var selecetd_type = ($(this).data('notif_type'));
											if(notification_type == selecetd_type){
												$(this).find('img').addClass('selected_notification');	
											}
										});
									});
									
									$("#edit_alert_details_m").submit(function(e){
											e.preventDefault();
										}).validate({
											rules:{
												edit_alert_name:{
													required:true,
												},
											},
											messages:{
												edit_alert_name:{
													required:"Alert Name is required",
												},
											},
											submitHandler:function(form){
							
													var frm=$(form).serialize();
															$.ajax({
															url:base_url+'My_services/update_alert_info',
															type:"POST",
															data:{'csrf_test_name':csrf_hash,"frm":frm,},
															success:function(res){
																if(res!= false)
																{
																	var data=JSON.parse(res);
																	var html_1 ='';
																	var final_html ='';
																	if(data != '')
																	{
																		for(var i=0;i<data.length;i++){
																			var notification = '';
																			   if(data[i].notification_type == '1') {
																				notification = 'Remainder';
																			   }else if(data[i].notification_type == '2') {
																					notification = 'Message';   
																			   } else if(data[i].notification_type == '3'){
																					notification = 'Email';   
																			   } else {
																				   notification = '';
																			   }
																		if(i == 0)
																		   {
																			html_1='<div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
														   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">ALERT NAME</span><p>'+data[i].alert_name+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">NOTIFICATION TYPE</span><p>'+notification+'</p></div><div class="col l1 s1 m1 gstaction"><a href="#" class="remove_service_alert_edit_m" data-alert_id="'+data[i].id+'" data-service_id="'+data[i].service_id+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a><a href="#" class="edit_alert_info_m" data-alert_id="'+data[i].id+'" data-service_id="'+data[i].service_id+'" data-alert_name="'+data[i].alert_name+'" data-alert_on="'+data[i].alert_on+'" data-set_condition="'+data[i].set_condition+'" data-alert_number="'+data[i].alert_number+'" data-alert_interval="'+data[i].alert_interval+'" data-notification_type="'+data[i].notification_type+'"><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div>';
																		   }
																		   else
																		   {
																			 html_1='<div class="alertmore"><div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+(i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">ALERT NAME</span><p>'+data[i].alert_name+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">NOTIFICATION TYPE</span><p>'+notification+'</p></div><div class="col l1 s1 m1 gstaction"><a href="#" class="remove_service_alert_edit_m" data-alert_id="'+data[i].id+'" data-service_id="'+data[i].service_id+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a><a href="#" class="edit_alert_info_m" data-alert_id="'+data[i].id+'" data-service_id="'+data[i].service_id+'" data-alert_name="'+data[i].alert_name+'" data-alert_on="'+data[i].alert_on+'" data-set_condition="'+data[i].set_condition+'" data-alert_number="'+data[i].alert_number+'" data-alert_interval="'+data[i].alert_interval+'" data-notification_type="'+data[i].notification_type+'"><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div></div></div>';
																		  
																		   }
																			 final_html = final_html + html_1;
																			 $("#alert_info_array_m").html(final_html);
																			// $(".two-i").html(data.length);
																		}
																		Materialize.toast('Alert Information updated successfully', 2000,'green rounded');
																	}
																	else
																	{
																		html_1='<div class="gst-boxs add-gst">Add Alert Data</div>';
																		$("#alert_info_array_m").html(html_1);
																		Materialize.toast('Add alert information!', 2000,'green rounded');
																	}
																}
																else
																{
																	html_1='<div class="gst-boxs add-gst">Add Alert Data</div>';
																	$("#alert_info_array_m").html(html_1);
																	Materialize.toast('Error while processing!', 2000,'red rounded');
																}
																$("#edit_alert_details_m").find("input[type=text], textarea").val("");
																$("#edit_alert").modal('close');
														},
												});
											},
										});
									});	
								$(document).off().on('click', '.print_service_2' ,function(){
									var service_id = $(this).data('service_id');
									var url = '&flag=' + 1;
									window.location.href = base_url+'My_services/view_service_2/' + service_id + '?' + url;
								});
								
								$(document).on('click', '.deactivate_service_2_m',function(){
										var service_id = $(this).data('service_id');
										$('#viewpage_deactive_service').modal('open');
										$('#viewpage_deactive_service #service_id').val(service_id);
								});
										
								$('.view_deactive_service_2').on('click',function(){
						
										var service = $('#service_id').val();
						
										$.ajax({
						
											url:base_url+'My_services/deactive_service_2',
						
											type:"POST",
						
											data:{'csrf_test_name':csrf_hash,"service":service,},
						
											success:function(res){
													window.location.href=base_url+'My_services/manage_services_for_manufacturing';
												},
											});										
								});
								
								$(document).on('click', '.activate_service_2_m',function(){
										var service_id = $(this).data('service_id');
										$('#viewpage_active_service').modal('open');
										$('#viewpage_active_service #activate_service_id').val(service_id);
								});
								$('.view_deactive_service_m').on('click',function(){
						
										var service = $('#activate_service_id').val();
						
										$.ajax({
						
											url:base_url+'My_services/activate_service_2',
						
											type:"POST",
						
											data:{'csrf_test_name':csrf_hash,"service":service,},
						
											success:function(res){
													window.location.href=base_url+'My_services/manage_services_for_manufacturing';
												},
											});										
								});
								
									
							
				

// ---------- Invoice Calculation ---------------






//-------------- Subscription & billing 

$(document).ready( function() {
	$(document).on('click','.select_plan', function() {
		var plan = $(this).data('plan');
		var cost = $(this).data('cost');
			$.ajax({
				url:base_url+'My_subscription/select_plan_session',
				type:"POST",
				data:{'csrf_test_name':csrf_hash,"plan":plan,"cost":cost},
				success:function(res){
						window.location.href = base_url+'My_subscription/add_space_to_plan';
				},
			});
	});
	$('input[name="size-radio"]').click(function(){
			var selected_space = $(this).val();
			var space_cost = $(this).data('space_cost');
			$('#space_cost').val(space_cost);
	});	

	$(document).on('click','.add_space', function() {
		if (!$("input[name='size-radio']:checked").val()) {
			Materialize.toast('Please Select Any one of these', 2000,'red rounded');
		} else {
			var selected_space = $('input[name=size-radio]:checked').val();
			var space_cost = $('#space_cost').val();
			$.ajax({
				url:base_url+'My_subscription/add_space_to_plan_session',
				type:"POST",
				data:{'csrf_test_name':csrf_hash,"selected_space":selected_space,"space_cost":space_cost},
				success:function(res){
						if(res == true) {
							window.location.href = base_url+'My_subscription/summary_section';
						} else {
							Materialize.toast('Error while processing. Please try again', 2000,'red rounded');
						}
				},
			});
		}
	});	
});

// ---------- Bulk Deactivation for my activity history -----

$(document).ready( function () {
	
	var bulk_activities = [];
				$("input[id='activity_bulk']").on("click",function(){
				if($(this).is(':checked',true)) {
					$(".activities_bulk_action").prop('checked', true);
					$(".activities_bulk_action:checked").each(function() {
							bulk_activities.push($(this).val());
						});
						bulk_activities = bulk_activities.join(",");
						$('#deactive_multiple_activities').attr('data-multi_activity',bulk_activities);
					}
					else {
						$(".activities_bulk_action").prop('checked',false);
						bulk_activities = [];
						$('#deactive_multiple_activities').attr('data-multi_activity',0);
					}
				});
				
$(document).on('click', "#deactive_multiple_activities", function(e) {
		var activity_values = $(this).data('multi_activity');
		if(activity_values == '' || activity_values == '0')
		{
			Materialize.toast('Please select a record first', 2000,'red rounded');
		}
		else
		{
			$.ajax({
				url:base_url+'My_account/deactive_multiple',
				type:"POST",
				data:{'csrf_test_name':csrf_hash,"activity_values":activity_values,},
				success:function(res){
					location.reload();
					},
			});		
		}
	});
	
	$(document).on('click', ".activate_records", function(e) {
		var module = $(this).data('module');
		$.ajax({
				url:base_url+'My_account/bulk_activate',
				type:"POST",
				data:{'csrf_test_name':csrf_hash,"module":module,},
				success:function(res){
					location.reload();
					},
			});	
	});	
});

$("#add_new_service_keywords").submit(function(e){
				e.preventDefault();
			}).validate({
				rules:{
					keyword_1:{
						required:true,
					},
				},
				messages:{
					keyword_1:{
						required:"Add At least one service keyword",
					},
				},
				submitHandler:function(form){
						var frm=$(form).serialize();
								 //$(form).ajaxSubmit();
								$.ajax({
								url:base_url+'Company_profile/add_keywords',
								type:"POST",
								data:{'csrf_test_name':csrf_hash,"frm":frm,},
								success:function(res){
										//console.log(res);
										if(res != false)
										{
											Materialize.toast('Service Info have been set', 2000,'green rounded');
											$("#add_new_service_keywords").find("input[type=text], textarea").val("");
											$("#add-new-service").modal('close');	
										} else {
											Materialize.toast('Error while processing...!!', 2000,'red rounded');	
										}
									},
					});
				},
			});
			
			$("#add_branch_info").submit(function(e){
				e.preventDefault();
			}).validate({
				rules:{
					branch_name:{
						required:true,
					},
					branch_city:{
						required:true,
					},
				},
				messages:{
					branch_name:{
						required:"Branch Name is required",
					},
					branch_city:{
						required:"Branch City is required",
					},
				},
				submitHandler:function(form){
						var frm=$(form).serialize();
								$.ajax({
								url:base_url+'Company_profile/add_branch_info',
								type:"POST",
								data:{'csrf_test_name':csrf_hash,"frm":frm,},
								success:function(res){
									console.log(res);
									if(res!= false)
									{
										var data=JSON.parse(res);
										var html_1 ='';
										var final_html_1 ='';
										if(data != '')
										{
											for(var i=0;i<data.length;i++){
											if(i == 0)
											   {
												html_1='<div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">BRANCH</span><p>'+data[i].branch_name+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">CITY</span><p>'+data[i].branch_city+'</p></div><div class="col l1 s1 m1 gstaction"><a class="remove_cmp_branch_model" data-branch="'+data[i].branch_name+'" data-city="'+data[i].branch_city+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a></div></div></div>';
											   }
											   else
											   {
												 html_1='<div class="gstmore"><div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">BRANCH</span><p>'+data[i].branch_name+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">CITY</span><p>'+data[i].branch_city+'</p></div><div class="col l1 s1 m1 gstaction"><a class="remove_cmp_branch_model" data-branch="'+data[i].branch_name+'" data-city="'+data[i].branch_city+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a></div></div></div></div>';
											  
											   }
												 final_html_1 = final_html_1 + html_1;
												 $("#company_branch_array_list").html(final_html_1);
												 $(".tot_branch").html(data.length);
											}
											Materialize.toast('Branch Details have been set.', 2000,'green rounded');
										}
										else
										{
											html_1='<div class="gst-boxs add-gst">Add Branch Data</div>';
											$("#company_branch_array_list").html(html_1);
										}
									}
									else
									{
										Materialize.toast('Error while processing. Please try again', 2000,'red rounded');
									}
									$("#add_branch_info").find("input[type=text],input[type=password], textarea").val("");
									$("#new-branch").modal('close');
									
							},
							complete:function(res){
								$(document).ready( function() {		
								$(document).on('click','.remove_cmp_branch_model',function(){				
									
									var branch = $(this).data('branch');
									var city = $(this).data('city');
									$('#remove_company_branch_data').modal('open');
									$('#remove_company_branch_data #remove_branch').val(branch);
									$('#remove_company_branch_data #remove_city').val(city);
								});
			
									$('.remove_company_branch_data').off().on('click',function(){
										var branch = $('#remove_branch').val();
										var city = $('#remove_city').val();
						
										$.ajax({
						
											url:base_url+'Company_profile/remove_branch_info',
						
											type:"post",
						
											data:{'csrf_test_name':csrf_hash,"branch":branch,"city":city},
						
											success:function(res){
												if(res!= false)
													{
														var data=JSON.parse(res);
														var html_1 ='';
														var final_html_1 ='';
														if(data != '')
														{
															$("#company_branch_array_list").html("");
															for(var i=0;i<data.length;i++){
															if(i == 0)
															   {
																html_1='<div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">BRANCH</span><p>'+data[i].branch_name+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">CITY</span><p>'+data[i].branch_city+'</p></div><div class="col l1 s1 m1 gstaction"><a class="remove_cmp_branch_model" data-branch="'+data[i].branch_name+'" data-city="'+data[i].branch_city+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a></div></div></div>';
															   }
															   else
															   {
																  html_1='<div class="gstmore"><div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">BRANCH</span><p>'+data[i].branch_name+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">CITY</span><p>'+data[i].branch_city+'</p></div><div class="col l1 s1 m1 gstaction"><a class="remove_cmp_branch_model" data-branch="'+data[i].branch_name+'" data-city="'+data[i].branch_city+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a></div></div></div></div>';
															  
															   }
															    final_html_1 = final_html_1 + html_1;
																 $("#company_branch_array_list").html(final_html_1);
																 $(".tot_branch").html(data.length);
															}
															Materialize.toast('Branch Details has been removed.', 2000,'green rounded');
														}
														else
														{
															html_1='<div class="gst-boxs add-gst">Add Branch Data</div>';
															$("#company_branch_array_list").html(html_1);
															$(".tot_branch").html(0);
														}
													}
													else
													{
														html_1='<div class="gst-boxs add-gst">Add Branch Data</div>';
														$("#company_branch_array_list").html(html_1);
														$(".tot_branch").html(0);
													}
												},
						
											});										
										});
									});
							},
							
					});
				},
			});
			