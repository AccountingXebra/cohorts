/*================================================================================

  Item Name: Materialize - Material Design Admin Template

  Version: 4.0

  Author: PIXINVENT

  Author URL: https://themeforest.net/user/pixinvent/portfolio

================================================================================*/



  /*Preloader*/

  /*$(window).on('load', function() {

    setTimeout(function() {

      $('body').addClass('loaded');

    }, 200);

  }); */


//var base_url="http://webtech-evolution.com/projects/EasyInvoices/";
  $(function() {



    "use strict";



    var window_width = $(window).width();

    var openIndex;



    // Collapsible navigation menu

    $('.nav-collapsible .navbar-toggler').click(function() {

      //set Index velue

      getCollapseIndex();

      // Toggle navigation expan and collapse on radio click

      if ($('#left-sidebar-nav').hasClass('nav-expanded') && !$('#left-sidebar-nav').hasClass('nav-lock')) {

        $('#left-sidebar-nav').toggleClass('nav-expanded');

        $('#main').toggleClass('main-full');

      } else {

        $('#left-sidebar-nav').toggleClass('nav-expanded nav-collapsed');

        $('#main').toggleClass('main-full');

      }

      // Set navigation lock / unlock with radio icon

      if ($('.menu-icon').hasClass('close')) {

        $('.menu-icon').removeClass('close');

        $('.menu-icon').addClass('open');

        $('#left-sidebar-nav').addClass('nav-lock');

        $('.header-search-wrapper').addClass('sideNav-lock');

        $('#header').removeClass('header-collapsed');

      } else {

        $('.menu-icon').removeClass('open');

        $('.menu-icon').addClass('close');

        $('#left-sidebar-nav').removeClass('nav-lock');

        $('.header-search-wrapper').removeClass('sideNav-lock');

        $('#header').addClass('header-collapsed');

      }



      setTimeout(function() {

        if (openIndex != null) {

          if ($('#left-sidebar-nav').hasClass('nav-collapsed')) {

            $('.collapsible').collapsible('close', (openIndex));

          }

        }

      }, 100);

    });

   $('.modal').perfectScrollbar({
  suppressScrolly: true,
  suppressScrollx: false
}); 

    $('.modal').modal();

	/*$('.model').modal({

			  dismissible: false,

			  //dismissible: true, // Modal can be dismissed by clicking outside of the modal

			  opacity: .5, // Opacity of modal background

			  inDuration: 300, // Transition in duration

			  outDuration: 200, // Transition out duration

			  startingTop: '4%', // Starting top style attribute

			  endingTop: '10%', // Ending top style attribute

			  ready: function(modal, trigger) { // Callback for Modal open. Modal and trigger parameters available.

				alert("Ready");

				console.log(modal, trigger);

			  },

			  complete: function() { alert('Closed'); } // Callback for Modal close

		  });*/



    function getCollapseIndex() {

      $("#slide-out > li > ul > li > a.collapsible-header").each(function(index) {

        if ($(this).parent().hasClass('active')) {

          openIndex = index;

        }

      });

    }



    // Search class for focus

    $('.header-search-input').focus(

      function() {

        $(this).parent('div').addClass('header-search-wrapper-focus');

      }).blur(

      function() {

        $(this).parent('div').removeClass('header-search-wrapper-focus');

      });



    // Check first if any of the task is checked

    $('#task-card input:checkbox').each(function() {

      checkbox_check(this);

    });



    // Task check box

    $('#task-card input:checkbox').change(function() {

      checkbox_check(this);

    });



    // Check Uncheck function

    function checkbox_check(el) {

      if (!$(el).is(':checked')) {

        $(el).next().css('text-decoration', 'none'); // or addClass

      } else {

        $(el).next().css('text-decoration', 'line-through'); //or addClass

      }

    }



    // Swipeable Tabs Demo Init

    if ($('#tabs-swipe-demo').length) {

      $('#tabs-swipe-demo').tabs({

        'swipeable': true

      });

    }



    // Plugin initialization



    $('select').material_select();

    // Set checkbox on forms.html to indeterminate

    var indeterminateCheckbox = document.getElementById('indeterminate-checkbox');

    if (indeterminateCheckbox !== null)

      indeterminateCheckbox.indeterminate = true;



    // Materialize Slider

    $('.slider').slider({

      full_width: true

    });



    // Commom, Translation & Horizontal Dropdown

    $('.dropdown-button, .translation-button, .dropdown-menu').dropdown({

      inDuration: 300,

      outDuration: 225,

      constrainWidth: false,

      hover: true,

      gutter: 0,

      belowOrigin: true,

      alignment: 'left',

      stopPropagation: false

    });

    // Notification, Profile & Settings Dropdown

    $('.notification-button, .dropdown-settings').dropdown({

      inDuration: 300,

      outDuration: 225,

      constrainWidth: false,

      hover: true,

      gutter: 0,

      belowOrigin: true,

      alignment: 'right',

      stopPropagation: false

    });



    // Materialize Tabs

    $('.tab-demo').show().tabs();

    $('.tab-demo-active').show().tabs();



    // Materialize Parallax

    $('.parallax').parallax();



    // Materialize scrollSpy

    $('.scrollspy').scrollSpy();



    // Materialize tooltip

    $('.tooltipped').tooltip({

      delay: 50

    });



    //Main Left Sidebar Menu

    $('.sidebar-collapse').sideNav({

      edge: 'left', // Choose the horizontal origin

    });



    // Overlay Menu (Full screen menu)

    $('.menu-sidebar-collapse').sideNav({

      menuWidth: 240,

      edge: 'left', // Choose the horizontal origin

      //closeOnClick:true, // Set if default menu open is true

      menuOut: false // Set if default menu open is true

    });



    //Main Left Sidebar Chat

    $('.chat-collapse').sideNav({

      menuWidth: 300,

      edge: 'right',

    });



    // Pikadate datepicker

    $('.datepicker').pickadate({

      selectMonths: true, // Creates a dropdown to control month

      selectYears: 15 // Creates a dropdown of 15 years to control year

    });



    // Perfect Scrollbar

    $('select').not('.disabled').material_select();

    var leftnav = $(".page-topbar").height();

    var leftnavHeight = window.innerHeight - leftnav;

    if (!$('#slide-out.leftside-navigation').hasClass('native-scroll')) {

      $('.leftside-navigation').perfectScrollbar({

        suppressScrollX: true

      });

    }

    var righttnav = $("#chat-out").height();

    $('.rightside-navigation').perfectScrollbar({

      suppressScrollX: true

    });
	$('#left-sidebar-tab-f').perfectScrollbar({
      suppressScroll: true,
    });


    // Fullscreen

    function toggleFullScreen() {

      if ((document.fullScreenElement && document.fullScreenElement !== null) ||

        (!document.mozFullScreen && !document.webkitIsFullScreen)) {

        if (document.documentElement.requestFullScreen) {

          document.documentElement.requestFullScreen();

        } else if (document.documentElement.mozRequestFullScreen) {

          document.documentElement.mozRequestFullScreen();

        } else if (document.documentElement.webkitRequestFullScreen) {

          document.documentElement.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);

        }

      } else {

        if (document.cancelFullScreen) {

          document.cancelFullScreen();

        } else if (document.mozCancelFullScreen) {

          document.mozCancelFullScreen();

        } else if (document.webkitCancelFullScreen) {

          document.webkitCancelFullScreen();

        }

      }

    }



    $('.toggle-fullscreen').click(function() {

      toggleFullScreen();

    });





    // Floating-Fixed table of contents (Materialize pushpin)

    if ($('nav').length) {

      $('.toc-wrapper').pushpin({

        top: $('nav').height()

      });

    } else if ($('#index-banner').length) {

      $('.toc-wrapper').pushpin({

        top: $('#index-banner').height()

      });

    } else {

      $('.toc-wrapper').pushpin({

        top: 0

      });

    }



    // Toggle Flow Text

    var toggleFlowTextButton = $('#flow-toggle')

    toggleFlowTextButton.click(function() {

      $('#flow-text-demo').children('p').each(function() {

        $(this).toggleClass('flow-text');

      })

    });



    //Alerts

    $("#card-alert .close").click(function() {

      $(this).closest('#card-alert').fadeOut('slow');

    });



    //Toggle Containers on page

    var toggleContainersButton = $('#container-toggle-button');

    toggleContainersButton.click(function() {

      $('body .browser-window .container, .had-container').each(function() {

        $(this).toggleClass('had-container');

        $(this).toggleClass('container');

        if ($(this).hasClass('container')) {

          toggleContainersButton.text("Turn off Containers");

        } else {

          toggleContainersButton.text("Turn on Containers");

        }

      });

    });



    // Detect touch screen and enable scrollbar if necessary

    function is_touch_device() {

      try {

        document.createEvent("TouchEvent");

        return true;

      } catch (e) {

        return false;

      }

    }

    if (is_touch_device()) {

      $('#nav-mobile').css({

        overflow: 'auto'

      })

    }



  $('ul.dropdown-content li').first().click(function(){    

    $('ul.dropdown-content li:not(:first-child)').each(function(index) {

      $(this).find("input[type=checkbox]").prop("checked", $('ul.dropdown-content li').first().find("input[type=checkbox]").prop("checked"));                       

    });

  });

     

  $('select.company_dropdown_select').material_select();



  $('.dropdown-button').dropdown({"hover": false})



  });

  function showSearch(){

    $('.header-search-wrapper').toggleClass('hide');

  }



  

//============ Sript for Changing fontsize on Choose Company - Top Bar ===



$(document).ready(function() {

  

  if($('.access-select input.select-dropdown').val()=='Access'){

    $('.access-select input.select-dropdown').attr('style','color: #b2b8ce;font-size:10px;text-transform: uppercase;');

  }

});



setInterval(function(){

$('.dropdown-content.select-dropdown').perfectScrollbar({
  suppressScrolly: true,
  suppressScrollx: false
});

//Setting Height for Box Shadow

$('.dropdown-content.active').each(function(){

  var totalHeight=0;

  $(".dropdown-content.active li").each(function() {

    totalHeight += $(this).outerHeight(true); // to include margins

  });



  $('.dropdown-content.active').css('height', totalHeight+'px');

});



if($('.access-select .dropdown-content.select-dropdown li').hasClass('selected')) {

    $ddval = $('.access-select .dropdown-content.select-dropdown li.selected span').html();

    if($ddval=='Access'){

      $('.access-select input.select-dropdown').attr('style','color: #b2b8ce;font-size:10px;text-transform: uppercase;');

    } else{

      $('.access-select input.select-dropdown').attr('style','');

    }

}



},100);



/*$("input[name='upload_img']").on('change',function(){

	//$('#upload_img').change(function() {

		console.log(this.files[0]);

		var img_info = (this.files[0]);

		console.log(img_info);

		$.ajax({

			url:base_url+'personal_profile/upload_profile_image',

			type:"POST",

			mimeType: "multipart/form-data",

			processData: false,

			contentType: false,

			data:{"img_info":img_info},

			success:function(res){

				console.log(res);

				var data=JSON.parse(res);

				console.log(res);

				//$("#item_add_modal").modal('show');

			},

		});

		

		$('#image_info').val(img_info);

	});*/

$('input[type="file"].hide-file').change(function() {
	var filesize = (this.files[0].size);
	var filename = (this.files[0].name);
	if(csrf_hash===""){
		csrf_hash=csrf_hash;
	}
	$.ajax({
      url:base_url+'profile/check_for_allowedsize',
      type:"POST",
      datatype: 'json',
      data:{
		  'csrf_test_name':csrf_hash,
		  "filesize":filesize,
          "filename":filename,
          "doctype":"image",
      },
      success:function(res){
        //alert(res);
        var res1 = JSON.parse(res);
		var myArray = res1.split("/");
		if(myArray[1]){
			csrf_hash=myArray[1];
		}
        if(myArray[0]=="sizeexc"){
          //Materialize.toast('Document size exceeds allowed limit (Allowed 2MB Size Only)', 7000,'red rounded');
           $('.hide-file').val("");
          return false;
        }
        else if(myArray[0] == 'error')
        {
          //Materialize.toast('Document format is incorrect', 7000,'red rounded');
           $('.hide-file').val("");
          return false;
        }
         else if(myArray[0] == 'subscription')
        {

          //Materialize.toast('You cannot use more than 1 GB space.Please Subscribe appropriate package', 2000,'red rounded');
              $(".hide-file").val("");
          return false;
        }
        else if(myArray[0] == 'subscription_1')
        {

          //Materialize.toast('You cannot use more than 1 GB space.Please Subscribe appropriate package', 2000,'red rounded');
              $(".hide-file").val("");
          return false;
        } else if(myArray[0] == 'subscription_2')
        {

          //Materialize.toast('You cannot use more than 5 GB space.Please Subscribe appropriate package', 2000,'red rounded');
              $(".hide-file").val("");
          return false;
        } else if(myArray[0] == 'subscription_3')
        {

          //Materialize.toast('You cannot use more than 10 GB space.Please Subscribe appropriate package', 2000,'red rounded');
               $(".hide-file").val("");
          return false;
        }
        else 
        {
  

    readURL(this);
  }

}
});

});

$('input[type="file"].hide-file-signature').change(function() {
    var filesize = (this.files[0].size);
  var filename = (this.files[0].name);
  $.ajax({
      url:base_url+'profile/check_for_allowedsize',
      type:"POST",
      datatype: 'json',
      data:{
		  'csrf_test_name':csrf_hash,
		  "filesize":filesize,
          "filename":filename,
           "doctype":"image",

      },
      success:function(res){
        var res1 = JSON.parse(res);
        
        if(res1=="sizeexc"){
          //Materialize.toast('Document size exceeds allowed limit (Allowed 2MB Size Only)', 7000,'red rounded');
          $('.hide-file-signature').val("");
          return false;
        }
        else if(res1 == 'error')
        {
          //Materialize.toast('Document format is incorrect', 7000,'red rounded');
           $('.hide-file-signature').val("");
          return false;
        }
        else if(res1 == 'subscription')
        {

          //Materialize.toast('You cannot use more than 1 GB space.Please Subscribe appropriate package', 2000,'red rounded');
              $(".hide-file-signature").val("");
          return false;
        }
        else if(res1 == 'subscription_1')
        {

          //Materialize.toast('You cannot use more than 1 GB space.Please Subscribe appropriate package', 2000,'red rounded');
              $(".hide-file-signature").val("");
          return false;
        } else if(res1 == 'subscription_2')
        {

          //Materialize.toast('You cannot use more than 5 GB space.Please Subscribe appropriate package', 2000,'red rounded');
              $(".hide-file-signature").val("");
          return false;
        } else if(res1 == 'subscription_3')
        {

          //Materialize.toast('You cannot use more than 10 GB space.Please Subscribe appropriate package', 2000,'red rounded');
               $(".hide-file-signature").val("");
          return false;
        }
        else 
        {
      readURLsign(this);
    }
    }
});
//readURLsign(this);
});

function readURL(input) {

  if (input.files && input.files[0]) {

	  	//var filename = input.files[0].name;

		//$('#image_info').val(filename);

      var reader = new FileReader();

      reader.onload = function (e) {
          $('.uploader-placeholder').css('background-image', 'url('+e.target.result+')');
      };

      reader.readAsDataURL(input.files[0]);

  }

}

function readURLsign(input) {

  if (input.files && input.files[0]) {

      //var filename = input.files[0].name;

    //$('#image_info').val(filename);

      var reader = new FileReader();

      reader.onload = function (e) {

          $('.uploader-placeholder-sign').css('background-image', 'url('+e.target.result+')');

      };

      reader.readAsDataURL(input.files[0]);

  }

}

$('.check-label').on('change', function(){

  if($(this).val()!=''){

    $(this).parents('.input-field').addClass('label-active');

  }else{

    $(this).parents('.input-field').removeClass('label-active');

  }

})



$('li.user-profile-fix').perfectScrollbar({

  suppressScrollX: true

});

$('#scrollbar-restable').perfectScrollbar({
  suppressScrolly: true,
  suppressScrollx: false
});



$('.country-dropdown ul').perfectScrollbar({

	suppressScrollX: true

});



$('.profile-button').dropdown({

	hover: false, 

	inDuration: 300,

	outDuration: 225,

	constrainWidth: false,

	gutter: 0,

	belowOrigin: true,

	alignment: 'right',

	stopPropagation: false

});


$('body').on('click', '.bdatepicker', function() {
  $(this).datepicker({
    autoclose: true,
    format: 'dd M',
	todayHighlight: true,
  }).focus();
});

$('body').on('click', '.bdatepicker_inco', function() {
  $(this).datepicker({
    autoclose: true,
    format: 'dd M',
    endDate: "today",
    maxDate: "today",
	todayHighlight: true,
  }).focus();
});

// if( $('.bdatepicker').length ){

//   $('.bdatepicker').datepicker({

//     autoclose: true

//   });

// }

$('body').on('focus',".idatepicker", function(){
  $(this).datepicker({autoclose: true});
});



$(document).ready(function() {
  $('.select-like-dropdown').hover(

    function() {

      if($(this).children('input.select-dropdown').hasClass('active')){

      }else{

        $(this).children('span.caret').html('&#9650;');

        $(this).children('input.select-dropdown').click();

      }

    },function(){

      $(this).children('span.caret').html('&#9660;');

      $(this).children('input.select-dropdown').removeClass('active');

      $(this).children('ul.dropdown-content.select-dropdown').css('opacity',0);

      $(this).children('ul.dropdown-content.select-dropdown').css('display','none');

    }

  );

})

$(document).ready(function(){
  $(".view").click(function(){
      $(".first-hide").toggleClass("show");
  });

	var x = 1;
  $(".add-icon").click(function(e){
	  e.preventDefault();
	  x++;
    var elemt='<div class="row gstrow"><div class="col l10 s10 m10 fieldset"><div class="row"><div class="col l6 s12 m6 fieldset"><div class="input-field"><label for="gstin'+x+'" class="full-bg-label">GSTIN</label><input type="hidden" name="gst_array[]" value="'+x+'" /><input id="gstin'+x+'" name="gstin'+x+'" class="full-bg adjust-width gstin" type="text"></div></div><div class="col l6 s12 m6 fieldset"><div class="input-field"><label for="location'+x+'" class="full-bg-label">PLACE OF SUPPLY</label><input id="location'+x+'" name="location'+x+'" class="full-bg adjust-width" type="text"></div></div></div></div><div class="col l2 s2 m2 fieldset"><div class="col l12 s12 m12 fieldset"><a class="add-remove-btn removegstrow"><i class="material-icons">remove</i></a></div></div></div>';
    $('#rep-element').append(elemt);
  });

  $('#rep-element').on('click', '.removegstrow', function(e) {
    e.preventDefault();
    $(this).parents('.gstrow.row').remove();
  });
  
  var c = 1;
  $(".add-icon").click(function(e){
	  e.preventDefault();
	  c++;
    var elemt='<div class="row gstrow"><div class="col l10 s10 m10 fieldset"><div class="row"><div class="col l6 s12 m6 fieldset"><div class="input-field"><label for="gstin1" class="full-bg-label">NAME OF THE TAX</label><input type="hidden" name="tax_array[]" value="'+c+'" /><input id="tax_name'+c+'" name="tax_name'+c+'" class="full-bg adjust-width " type="text"></div></div><div class="col l6 s12 m6 fieldset"><div class="input-field"><label for="location1" class="full-bg-label">PERCENTAGE OF THE TAX</label><input id="tax_perc'+y+'" name="tax_perc'+c+'" class="full-bg adjust-width" type="text"></div></div></div></div><div class="col l2 s2 m2 fieldset"><div class="col l12 s12 m12 fieldset"><a class="add-remove-btn removegstrow"><i class="material-icons rem-red">remove</i></a></div></div></div>';
    $('#rep-element1').append(elemt);
  });

  $('#rep-element1').on('click', '.removegstrow', function(e) {
    e.preventDefault();
    $(this).parents('.gstrow.row').remove();
  });
 
 var y = 1;
  $(".addtaxrow").click(function(e){
		e.preventDefault();
		y++;
    var elemt='<div class="row gstrow"><div class="col l10 s10 m10 fieldset"><div class="row"><div class="col l6 s12 m6 fieldset"><div class="input-field"><label for="gstin1" class="full-bg-label">NAME OF THE TAX</label><input type="hidden" name="tax_array[]" value="'+x+'" /><input id="tax_name'+y+'" name="tax_name'+y+'" class="full-bg adjust-width " type="text"></div></div><div class="col l6 s12 m6 fieldset"><div class="input-field"><label for="location1" class="full-bg-label">PERCENTAGE OF THE TAX</label><input id="tax_perc'+y+'" name="tax_perc'+y+'" class="full-bg adjust-width" type="text"></div></div></div></div><div class="col l2 s2 m2 fieldset"><div class="col l12 s12 m12 fieldset"><a class="add-remove-btn removegstrow"><i class="material-icons rem-red">remove</i></a></div></div></div>';
    $('#rep-element1').append(elemt);
  });

  $('#rep-element1').on('click', '.removegstrow', function(e) {
    e.preventDefault();
    $(this).parents('.gstrow.row').remove();
  });

 
    $('#Legaldocumentscover').on('click', '.dropdown-button', function(e) {
    e.preventDefault();
    var idval=$(this).attr('data-activates');
    $('#Legaldocumentscover #'+idval).toggle( function(){$(this).css({"opacity": "1"});});

  });
  
  $('#purchaseorderdoccover').on('click', '.reminderbtn', function(e) {
    e.preventDefault();
    var idval=$(this).attr('data-activates');
    $('#purchaseorderdoccover #'+idval).toggle( function(){$(this).css({"opacity": "1"});});

  });
  $('#Legaldocumentscover').on('click', '.dropdown-content li', function(e) {
    e.preventDefault();
	var selected_reminder = $(this).data('remainder');
	var selected_docno = $(this).data('docno');

	$("#legal_remainder"+selected_docno).find('option').removeAttr("selected");
	$("#legal_remainder"+selected_docno+" option[value="+selected_reminder+"]").attr('selected','selected');
    $('#Legaldocumentscover .dropdown-content').toggle();

  });
  $('#purchaseorderdoccover').on('click', '.dropdown-content li', function(e) {
    e.preventDefault();
	var selected_reminder = $(this).data('po_remainder');
	var selected_docno = $(this).data('po_docno');
	$("#purchase_remainder"+selected_docno).find('option').removeAttr("selected");
	$("#purchase_remainder"+selected_docno+" option[value="+selected_reminder+"]").attr('selected','selected');
    $('#purchaseorderdoccover .dropdown-content').toggle();
	
  });
  
});

function showMore(){
  $('#hidden-cover').toggleClass('active');
  if($('#hidden-cover').hasClass('active')){
    $('.changeicon').html('keyboard_arrow_up');
  }else{
    $('.changeicon').html('keyboard_arrow_down');
  }
}

function showMore_contact_info(){
  $('#hidden-cover_contact').toggleClass('active');
  if($('#hidden-cover_contact').hasClass('active')){
    $('.changeicon').html('keyboard_arrow_up');
  }else{
    $('.changeicon').html('keyboard_arrow_down');
  }
}

function showGstMore(){
  $('.gstmore').toggleClass('active');
  if($('.gstmore').hasClass('active')){
    $('#showgstboxs i').html('keyboard_arrow_up');
  }else{
    $('#showgstboxs i').html('keyboard_arrow_down');
    $('#company_gst_array_list_edit').find('dt:lt(2), dd:lt(2)').hide();
  }
}

function showContactMore(){
  $('.contactmore').toggleClass('active');
  if($('.contactmore').hasClass('active')){
    $('#showContactboxs i').html('keyboard_arrow_up');
  }else{
    $('#showContactboxs i').html('keyboard_arrow_down');
  }
}

function showOtherBox(){
  $('.step1btn').toggle();
  $('.othersection').toggle();
  $('.footer-pay').toggle();
}


var newImageObj = [];
var ImageNo = 0;
$("#Legaldocuments").change(function () {
   var filesize = (this.files[0].size);
   var filename = (this.files[0].name);
   $.ajax({
      url:base_url+'profile/check_for_allowedsize',
      type:"POST",
      datatype: 'json',
      data:{
		  'csrf_test_name':csrf_hash,
		  "filesize":filesize,
          "filename":filename,
           "doctype":"file",
      },
      success:function(res){
        //alert(res);
        var res1 = JSON.parse(res);
        if(res1=="sizeexc"){
          //Materialize.toast('Document size exceeds allowed limit (Allowed 2MB Size Only)', 2000,'red rounded');
          $("#Legaldocuments").val("");
          return false;
        }
        else if(res1 == 'error')
        {
          //Materialize.toast('Document format is incorrect', 2000,'red rounded');
          $("#Legaldocuments").val("");
          return false;
        }
         else if(res1 == 'subscription')
        {

          //Materialize.toast('You cannot use more than 1 GB space.Please Subscribe appropriate package', 2000,'red rounded');
              $("#Legaldocuments").val("");
          return false;
        }
        else if(res1 == 'subscription_1')
        {

          //Materialize.toast('You cannot use more than 1 GB space.Please Subscribe appropriate package', 2000,'red rounded');
              $("#Legaldocuments").val("");
          return false;
        } else if(res1 == 'subscription_2')
        {

          //Materialize.toast('You cannot use more than 5 GB space.Please Subscribe appropriate package', 2000,'red rounded');
              $("#Legaldocuments").val("");
          return false;
        } else if(res1 == 'subscription_3')
        {

          //Materialize.toast('You cannot use more than 10 GB space.Please Subscribe appropriate package', 2000,'red rounded');
               $("#Legaldocuments").val("");
          return false;
        }
        else if(res1 == 'true')
        {
          var fileUpload = document.getElementById("Legaldocuments");

  if (typeof (FileReader) != "undefined") {

      for (var i = 0; i < fileUpload.files.length; i++) {
          var j = 0;
          var file = fileUpload.files[i];
          var NewFile = fileUpload.files[i];
     // var filesize = this.files[0].size;
      var kbfilesize = Math.round(filesize / 1000);
          var reader = new FileReader();
          reader.onload = function (e) {

          var html ="<div class='docubox " + ImageNo + "_CClass'>";
          html +="<div class='col s4 m4 l4 divr-border'> <div class='pdf-file'> <div class='col l3 s3 m3'><img src='"+base_url+"/asset/images/pdf.png' alt='' class='pdf'></div> <div class='col l9 s9 m9'> <p id='uploadfilename' class='uploadfilename'>"+fileUpload.files[j].name+"</p><span id='filesize'>File Size "+kbfilesize+" kb</span></div> </div> </div> <div class='col s3 m3 l3 divr-border'> <input  type='text' placeholder='Doc No.' class='no-border-field' name='legal_po_num"+ImageNo+"' id='legal_po_num"+ImageNo+"'> </div> <div class='col s1 m1 l1 divr-border'> <div class='date-start setdate' data-position='top' data-delay='50' data-tooltip='Start Date'> <input type='text' id='legal_po_date"+ImageNo+"' name='legal_po_date"+ImageNo+"' class='idatepicker_doc hidendate'><img src='"+base_url+"/asset/css/img/icons/calendar-purple.png' alt=''></div><img src='"+base_url+"/asset/css/img/icons/tick.png' alt='' id='legal_po_date"+ImageNo+"_tick' class='tick-format'> </div> <div class='col s1 m1 l1 divr-border'> <div class='date-start setdate' data-position='top' data-delay='50' data-tooltip='Start Date'> <input type='text' id='start_date"+ImageNo+"' name='start_date"+ImageNo+"' class='idatepicker_doc hidendate'><img src='"+base_url+"/asset/css/img/icons/calendar-green.png' alt=''></div><img src='"+base_url+"/asset/css/img/icons/tick.png' alt='' id='start_date"+ImageNo+"_tick' class='tick-format'> </div> <div class='col s1 m1 l1 divr-border'> <div class='date-start setdate end-date' data-position='top' data-delay='50' data-tooltip='End Date'> <input type='text' class='idatepicker_doc hidendate' id='end_date"+ImageNo+"' name='end_date"+ImageNo+"'><img src='"+base_url+"/asset/css/img/icons/calendar-red.png' alt=''></div><img src='"+base_url+"/asset/css/img/icons/tick.png' alt='' id='end_date"+ImageNo+"_tick' class='tick-format'></div>";
          html +="<div class='col s1 m1 l1 alarom-col divr-border'> <div class='date-start end-date'> <a class='dropdown-button' data-activates='reminder"+ImageNo+"'><img src='"+base_url+"/asset/css/img/icons/alarmclock.png' alt=''> <input type='hidden' class='legal-rm-"+ImageNo+"'> </a> <ul id='reminder"+ImageNo+"' class='dropdown-content legal-rmul-"+ImageNo+" border-radius-0'> <li></li> <li data-remainder='1' data-docno="+ImageNo+"><a href='#!' class='grey-text text-darken-1'>One week before</a></li> <li data-remainder='2' data-docno="+ImageNo+"><a href='#!' class='grey-text text-darken-1'>Fortnight</a></li> <li data-remainder='3' data-docno="+ImageNo+"><a href='#!' class='grey-text text-darken-1'>One month before</a></li> <li data-remainder='4' data-docno="+ImageNo+"><a href='#!' class='grey-text text-darken-1'>Two months before</a></li> </ul> <select name='legal_remainder"+ImageNo+"' id='legal_remainder"+ImageNo+"' class='country-dropdown check-label alrm_rem remider_img'> <option value='1'>One week before</option> <option value='2'>Fortnight</option> <option value='3'>One month before</option> <option value='4'>Two months before</option> </select> </div><img src='"+base_url+"/asset/css/img/icons/tick.png' alt='' id='legal_remainder"+ImageNo+"_tick' class='tick-format'></div>";
          html +="<div class='col s1 m1 l1 delete-col'> <a href='javascript:void(0);' onclick='LegalRemoveImage(\"" + ImageNo + "_CClass\",\"" + fileUpload.files[j].name.toLowerCase() + "\")'> <img src='"+base_url+"/asset/images/delete.png' alt=''></a> </div>";
          html +="</div>";
        
      $("#Legaldocumentscover").empty(html);  
          $("#Legaldocumentscover").append(html);
           $(".tick-format").hide();

              //j = j + 1;

              //ImageNo = ImageNo + 1;
          }

          newImageObj.push(file);

          reader.readAsDataURL(file);
       $("legal_remainder"+ImageNo).material_select();
      }
  } else {
      alert("This browser does not support HTML5 FileReader.");
  }
          //Materialize.toast('Document has been uploaded', 2000,'green rounded');
        }
        else
        {
           return false;
        }
      },
    });
 
});

function LegalRemoveImage(objclass, ImageName) {

  $.each(newImageObj, function (e, element) {
      if ($(this)[0].name.toLowerCase().trim() == ImageName.trim()) {
          newImageObj.pop(this);
      }
  });

  $("#Legaldocumentscover").find("." + objclass + "").remove();
}


var newPurchaseObj = [];
var ImagePurchaseNo = 0;
$("#purchaseorderdoc").change(function () {

   var filesize = (this.files[0].size);
   var filename = (this.files[0].name);
   $.ajax({
      url:base_url+'profile/check_for_allowedsize',
      type:"POST",
      datatype: 'json',
      data:{
		  'csrf_test_name':csrf_hash,
		  "filesize":filesize,
          "filename":filename,
           "doctype":"file",
      },
      success:function(res){
        //alert(res);
        var res1 = JSON.parse(res);
        if(res1=="sizeexc"){
          //Materialize.toast('Document size exceeds allowed limit (Allowed 2MB Size Only)', 2000,'red rounded');
         $("#purchaseorderdoc").val("");
          return false;
        }
        else if(res1 == 'error')
        {
          //Materialize.toast('Document format is incorrect', 2000,'red rounded');
          $("#purchaseorderdoc").val("");
          return false;
        }
         else if(res1 == 'subscription')
        {

          //Materialize.toast('You cannot use more than 1 GB space.Please Subscribe appropriate package', 2000,'red rounded');
              $("#purchaseorderdoc").val("");
          return false;
        }
        else if(res1 == 'subscription_1')
        {

          //Materialize.toast('You cannot use more than 1 GB space.Please Subscribe appropriate package', 2000,'red rounded');
              $("#purchaseorderdoc").val("");
          return false;
        } else if(res1 == 'subscription_2')
        {

          //Materialize.toast('You cannot use more than 5 GB space.Please Subscribe appropriate package', 2000,'red rounded');
              $("#purchaseorderdoc").val("");
          return false;
        } else if(res1 == 'subscription_3')
        {

          //Materialize.toast('You cannot use more than 10 GB space.Please Subscribe appropriate package', 2000,'red rounded');
               $("#purchaseorderdoc").val("");
          return false;
        }
        else if(res1 == 'true')
        {
          var fileUpload = document.getElementById("purchaseorderdoc");

  if (typeof (FileReader) != "undefined") {
      for (var i = 0; i < fileUpload.files.length; i++) {
          var j = 0;
          var file = fileUpload.files[i];
          var NewFile = fileUpload.files[i];
      //var filesize = this.files[0].size;
      var kbfilesize = Math.round(filesize / 1000);
          var reader = new FileReader();
          reader.onload = function (e) {

          var html ="<div class='docubox " + ImagePurchaseNo + "_CClass'>";
          html +="<div class='col s4 m4 l4 divr-border'><div class='pdf-file'><div class='col l3 s3 m3'><img src='"+base_url+"/asset/images/pdf.png' alt='' class='pdf'></div><div class='col l9 s9 m9'><p id='uploadfilename' class='uploadfilename pur_file_name'>"+fileUpload.files[j].name+"</p><span id='filesize'>File Size "+kbfilesize +" kb</span></div></div></div><div class='col s3 m3 l3 divr-border'><input type='text' placeholder='Purchase Order' class='no-border-field' name='purchase_order"+ImagePurchaseNo+"' id='purchase_order"+ImagePurchaseNo+"'></div><div class='col s1 m1 l1 divr-border'><div class='date-start setdate' data-position='top' data-delay='50' data-tooltip='Purchase Order Date'><input type='text' class='idatepicker_doc hidendate' name='po_date"+ImagePurchaseNo+"' id='po_date"+ImagePurchaseNo+"'><img src='"+base_url+"/asset/css/img/icons/calendar-purple.png' alt=''></div><img src='"+base_url+"/asset/css/img/icons/tick.png' alt='' id='po_date"+ImagePurchaseNo+"_tick' class='po-tick-format'></div><div class='col s1 m1 l1 divr-border'><div class='date-start setdate' data-position='top' data-delay='50' data-tooltip='Start Date'><input type='text' class='idatepicker_doc hidendate' name='po_startdate"+ImagePurchaseNo+"' id='po_startdate"+ImagePurchaseNo+"'><img src='"+base_url+"/asset/css/img/icons/calendar-green.png' alt=''></div><img src='"+base_url+"/asset/css/img/icons/tick.png' alt='' id='po_startdate"+ImagePurchaseNo+"_tick' class='po-tick-format'></div><div class='col s1 m1 l1 divr-border'><div class='date-start setdate end-date' data-position='top' data-delay='50' data-tooltip='End Date'><input type='text' class='idatepicker_doc hidendate' id='po_enddate"+ImagePurchaseNo+"' name='po_enddate"+ImagePurchaseNo+"'><img src='"+base_url+"/asset/css/img/icons/calendar-red.png' alt=''></div><img src='"+base_url+"/asset/css/img/icons/tick.png' alt='' id='po_enddate"+ImagePurchaseNo+"_tick' class='po-tick-format'></div>";
          html +="<div class='col s1 m1 l1 alarom-col divr-border'><div class='date-start end-date'><a href='javascript:void(0);' class='dropdown-button reminderbtn' data-set='reminder"+ImagePurchaseNo+"' data-activates='reminder"+ImagePurchaseNo+"'><img src='"+base_url+"/asset/css/img/icons/alarmclock.png' alt=''><input type='hidden' class='legal-rm-"+ImagePurchaseNo+"' name=''></a><ul id='reminder"+ImagePurchaseNo+"' class='dropdown-content legal-rmul-"+ImagePurchaseNo+" border-radius-0'><li></li><li data-po_remainder='1' data-po_docno="+ImagePurchaseNo+"><a href='#!' class='grey-text text-darken-1'>One week before</a></li><li data-po_remainder='2' data-po_docno="+ImagePurchaseNo+"><a href='#!' class='grey-text text-darken-1'>Fortnight</a></li><li data-po_remainder='3' data-po_docno="+ImagePurchaseNo+"><a href='#!' class='grey-text text-darken-1'>One month before</a></li data-po_remainder='4' data-po_docno="+ImagePurchaseNo+"><li data-po_remainder='4' data-po_docno="+ImagePurchaseNo+"><a href='#!' class='grey-text text-darken-1'>Two months before</a></li></ul><select name='purchase_remainder"+ImagePurchaseNo+"' id='purchase_remainder"+ImagePurchaseNo+"' class='country-dropdown check-label'><option value='1'>One week before</option><option value='2'>Fortnight</option><option value='3'>One month before</option><option value='4'>Two months before</option></select></div><img src='"+base_url+"/asset/css/img/icons/tick.png' alt='' id='purchase_remainder"+ImageNo+"_tick' class='po-tick-format'></div>";
          html +="<div class='col s1 m1 l1 delete-col'><a href='javascript:void(0);' onclick='PurchaseReImage(\"" + ImagePurchaseNo + "_CClass\",\"" + fileUpload.files[j].name.toLowerCase() + "\")'> <img src='"+base_url+"/asset/images/delete.png' alt=''></a></div>";
          html +="</div>";
      
      $("#purchaseorderdoccover").empty(html);
          $("#purchaseorderdoccover").append(html);
          $(".po-tick-format").hide();

              //j = j + 1;

              //ImagePurchaseNo = ImagePurchaseNo + 1;
          }

          newPurchaseObj.push(file);

          reader.readAsDataURL(file);
      }
  } else {
      alert("This browser does not support HTML5 FileReader.");
  }
         // Materialize.toast('Document has been uploaded', 2000,'green rounded');
        }
        else
        {
           return false;
        }
      },
    });

  
});
function PurchaseRemoveImage(objclass, ImageName) {

  $.each(newPurchaseObj, function (e, element) {
      if ($(this)[0].name.toLowerCase().trim() == ImageName.trim()) {
        newPurchaseObj.pop(this);
      }
  });

  $("#purchaseorderdoccover").find("." + objclass + "").remove();
}


var newotherdocObj = [];
var ImageotherdocNo = 0;
$(".otherdoc").change(function () {
alert('hi');

   var filesize = (this.files[0].size);
   var filename = (this.files[0].name);
   $.ajax({
      url:base_url+'profile/check_for_allowedsize',
      type:"POST",
      datatype: 'json',
      data:{
		  'csrf_test_name':csrf_hash,
		  "filesize":filesize,
          "filename":filename,
           "doctype":"file",
      },
      success:function(res){
        //alert(res);
        var res1 = JSON.parse(res);
        if(res1=="sizeexc"){
          //Materialize.toast('Document size exceeds allowed limit (Allowed 2MB Size Only)', 2000,'red rounded');
           $("#otherdoc").val("");
          return false;
        }
        else if(res1 == 'error')
        {
          //Materialize.toast('Document format is incorrect', 2000,'red rounded');
           $("#otherdoc").val("");
          return false;
        }
        else if(res1 == 'subscription')
        {

          //Materialize.toast('You cannot use more than 1 GB space.Please Subscribe appropriate package', 2000,'red rounded');
              $("#otherdoc").val("");
          return false;
        }
        else if(res1 == 'subscription_1')
        {

          //Materialize.toast('You cannot use more than 1 GB space.Please Subscribe appropriate package', 2000,'red rounded');
              $("#otherdoc").val("");
          return false;
        } else if(res1 == 'subscription_2')
        {

          //Materialize.toast('You cannot use more than 5 GB space.Please Subscribe appropriate package', 2000,'red rounded');
              $("#otherdoc").val("");
          return false;
        } else if(res1 == 'subscription_3')
        {

          //Materialize.toast('You cannot use more than 10 GB space.Please Subscribe appropriate package', 2000,'red rounded');
               $("#otherdoc").val("");
          return false;
        }
        else if(res1 == 'true')
        {
          var fileUpload = document.getElementById("otherdoc");

  if (typeof (FileReader) != "undefined") {
      for (var i = 0; i < fileUpload.files.length; i++) {
          var j = 0;
          var file = fileUpload.files[i];
          var NewFile = fileUpload.files[i];
      //var filesize = this.files[0].size;
      var kbfilesize = Math.round(filesize / 1000);
          var reader = new FileReader();
          reader.onload = function (e) {

          var html ="<div class='docubox " + ImageotherdocNo + "_CClass'>";
          html +="<div class='col s6 m6 l6 divr-border'><div class='pdf-file'><div class='col l3 s3 m3'><img src='"+base_url+"/asset/images/pdf.png' alt='' class='pdf'></div><div class='col l9 s9 m9'><p class='uploadfilename'>"+fileUpload.files[j].name+"</p><span>File Size "+kbfilesize +" kb</span></div></div></div><div class='col s4 m4 l4 divr-border'><input type='text' placeholder='Document Name' class='no-border-field a' name='otherdoc_name"+ImageotherdocNo+"' id='otherdoc_name"+ImageotherdocNo+"'></div><div class='col s2 m2 l2'></div>";
          html +="<div class='col s2 m2 l2 alarom-col'></div>";
          html +="<div class='col s2 m2 l2 delete-col'><a href='javascript:void(0);' onclick='otherRemoveImage(\"" + ImageotherdocNo + "_CClass\",\"" + fileUpload.files[j].name.toLowerCase() + "\")'> <img src='"+base_url+"/asset/images/delete.png' alt=''></a></div>";
          html +="</div>";
          $("#otherdoccover").empty(html);
      $("#otherdoccover").append(html);

              //j = j + 1;

              //ImageotherdocNo = ImageotherdocNo + 1;
          }
       
      //$(".docubox").on("click", function(event) {
      //console.log("Hover done...!");
      //});
          
      newotherdocObj.push(file);

          reader.readAsDataURL(file);
      }
  } else {
      alert("This browser does not support HTML5 FileReader.");
  }
          //Materialize.toast('Document has been uploaded', 2000,'green rounded');
        }
        else
        {
           return false;
        }
      },
    });

  
});

$("#padvicedoc").change(function () {   
	
	alert("OKO");
	var filesize = (this.files[0].size);
	var filename = (this.files[0].name);
	var filesize = this.files[0].size;
   $.ajax({
      url:base_url+'profile/check_for_allowedsize',
      type:"POST",
      datatype: 'json',
      data:{
		  'csrf_test_name':csrf_hash,
		  "filesize":filesize,
          "filename":filename,
           "doctype":"file",
      },
      success:function(res){
        //alert(res);
        var res1 = JSON.parse(res);
        if(res1=="sizeexc"){
          //Materialize.toast('Document size exceeds allowed limit (Allowed 2MB Size Only)', 2000,'red rounded');
           $("#padvicedoc").val("");
          return false;
        }
        else if(res1 == 'error')
        {
          //Materialize.toast('Document format is incorrect', 2000,'red rounded');
          $("#padvicedoc").val("");
          return false;
        }
        else if(res1 == 'subscription')
        {

          //Materialize.toast('You cannot use more than 1 GB space.Please Subscribe appropriate package', 2000,'red rounded');
              $("#padvicedoc").val("");
          return false;
        }
        else if(res1 == 'subscription_1')
        {

          //Materialize.toast('You cannot use more than 1 GB space.Please Subscribe appropriate package', 2000,'red rounded');
              $("#padvicedoc").val("");
          return false;
        } else if(res1 == 'subscription_2')
        {

          //Materialize.toast('You cannot use more than 5 GB space.Please Subscribe appropriate package', 2000,'red rounded');
              $("#padvicedoc").val("");
          return false;
        } else if(res1 == 'subscription_3')
        {

          //Materialize.toast('You cannot use more than 10 GB space.Please Subscribe appropriate package', 2000,'red rounded');
               $("#padvicedoc").val("");
          return false;
        }
        else if(res1 == 'true')
        {
            var fileUpload = document.getElementById("padvicedoc");

    if (typeof (FileReader) != "undefined") {
    for (var i = 0; i < fileUpload.files.length; i++) {
          var j = 0;
          var file = fileUpload.files[i];
          var NewFile = fileUpload.files[i];
      //var filesize = this.files[0].size;
          var kbfilesize = Math.round(filesize / 1000);
          var reader = new FileReader();
          reader.onload = function (e) {
          var html ="<div class='docubox " + ImagepadvNo + "_pClass'>";
          html +="<div class='col s6 m6 l10 divr-border'><div class='pdf-file'><div class='col l1 s1 m1'><img src='"+base_url+"/asset/images/pdf.png' alt='' class='pdf'></div><div class='col l11 s11 m11'><p class='uploadfilename'>"+fileUpload.files[j].name+"</p><span>File Size "+kbfilesize +" kb</span></div></div></div>";
          html +="<div class='col s2 m2 l2 delete-col'><a href='javascript:void(0);' onclick='otherRemoveImage(\"" + ImagepadvNo + "_pClass\",\"" + fileUpload.files[j].name.toLowerCase() + "\")'> <img src='"+base_url+"/asset/images/delete.png' alt=''></a></div>";
          html +="</div>";
          $("#otherdoccover").append(html);

              j = j + 1;

              ImagepadvNo = ImagepadvNo + 1;
          }

          newpadvObj.push(file);

          reader.readAsDataURL(file);
      }
  } else {
      alert("This browser does not support HTML5 FileReader.");
  }
          //Materialize.toast('Document has been uploaded', 2000,'green rounded');
        }
        else
        {
           return false;
        }
      },
    });


});
function otherRemoveImage(objclass, ImageName) {

  $.each(newotherdocObj, function (e, element) {
      if ($(this)[0].name.toLowerCase().trim() == ImageName.trim()) {
        newotherdocObj.pop(this);
      }
  });

  $("#otherdoccover").find("." + objclass + "").remove();
}

var newImageObj = [];
var ImageNo = 0;
$("#memorandum").change(function () {

var filename = (this.files[0].name);
   var filesize = this.files[0].size;
   $.ajax({
      url:base_url+'profile/check_for_allowedsize',
      type:"POST",
      datatype: 'json',
      data:{
		  'csrf_test_name':csrf_hash,
		  "filesize":filesize,
          "filename":filename,
           "doctype":"file",
      },
      success:function(res){
        //alert(res);
        var res1 = JSON.parse(res);
        if(res1=="sizeexc"){
          //Materialize.toast('Document size exceeds allowed limit (Allowed 2MB Size Only)', 2000,'red rounded');
          $("#memorandum").val("");
          return false;
        }
        else if(res1 == 'error')
        {
          //Materialize.toast('Document format is incorrect', 2000,'red rounded');
         $("#memorandum").val("");
          return false;
        }
        else if(res1 == 'subscription')
        {

          //Materialize.toast('You cannot use more than 1 GB space.Please Subscribe appropriate package', 2000,'red rounded');
              $("#memorandum").val("");
          return false;
        }
        else if(res1 == 'subscription_1')
        {

          //Materialize.toast('You cannot use more than 1 GB space.Please Subscribe appropriate package', 2000,'red rounded');
              $("#memorandum").val("");
          return false;
        } else if(res1 == 'subscription_2')
        {

          //Materialize.toast('You cannot use more than 5 GB space.Please Subscribe appropriate package', 2000,'red rounded');
              $("#memorandum").val("");
          return false;
        } else if(res1 == 'subscription_3')
        {

          //Materialize.toast('You cannot use more than 10 GB space.Please Subscribe appropriate package', 2000,'red rounded');
               $("#memorandum").val("");
          return false;
        }
        else if(res1 == 'true')
        {
            var fileUpload = document.getElementById("memorandum");

  if (typeof (FileReader) != "undefined") {
    
      for (var i = 0; i < fileUpload.files.length; i++) {

          var j = 0;
          var file = fileUpload.files[i];
          var NewFile = fileUpload.files[i];
      //var filesize = this.files[0].size;
      var kbfilesize = Math.round(filesize / 1000);
          var reader = new FileReader();
          reader.onload = function (e) {

          var html123 ="<div class='docubox " + ImageNo1 + "_CClass'>";
          html123 +="<div class='col s6 m6 l6 divr-border'><div class='pdf-file'><div class='col l3 s3 m3'><img src='"+base_url+"/asset/images/pdf.png' alt='' class='pdf'></div><div class='col l9 s9 m9'><p class='uploadfilename'>"+fileUpload.files[j].name+"</p><span>File Size "+kbfilesize +" kb</span></div></div></div><div class='col s4 m4 l4 divr-border'><input type='text' placeholder='Document Name' class='no-border-field' name='doc_name"+ImageNo1+"' id='doc_name"+ImageNo1+"'></div><div class='col s2 m2 l2'></div>";
          html123 +="<div class='col s2 m2 l2 alarom-col'></div>";
          html123 +="<div class='col s2 m2 l2 delete-col'><a href='javascript:void(0);' onclick='MOARemoveImage(\"" + ImageNo1 + "_mClass\",\"" + fileUpload.files[j].name.toLowerCase() + "\")'> <img src='"+base_url+"/asset/images/delete.png' alt=''></a></div>";
          html123 +="</div>";
      
      $("#memorandumcover").empty(html123); 
          $("#memorandumcover").append(html123);

              //j = j + 1;

              //ImageNo1 = ImageNo1 + 1;
          }

          newImageObj.push(file);

          reader.readAsDataURL(file);
       $("legal_remainder"+ImageNo1).material_select();
      }
  } else {
      alert("This browser does not support HTML5 FileReader.");
  }
          //Materialize.toast('Document has been uploaded', 2000,'green rounded');
        }
        else
        {
           return false;
        }
      },
    });


  ///////////////////

 
});

function MOARemoveImage(objclass, ImageName) {

  $.each(newImageObj, function (e, element) {
      if ($(this)[0].name.toLowerCase().trim() == ImageName.trim()) {
          newImageObj.pop(this);
      }
  });

  $("#memorandumcover").find("." + objclass + "").remove();
}

/*function showStpe(showel,hideel){
  $(hideel).hide();
  $(showel).show();
}*/

$(function() {
  $('#accessport').change(function(){
    if($('#accessport').prop('checked')==true){
      $('#modal-popup').modal('open')
    } 
  });
});



function otherRemoveImages(objclass, ImageName) {

  $.each(newotherdocObjs, function (e, element) {
      if ($(this)[0].name.toLowerCase().trim() == ImageName.trim()) {
        newotherdocObjs.pop(this);
      }
  });

  $("#otherdoccover-sup").find("." + objclass + "").remove();
}



$(document).ready( function() {
	$(document).on('click', '.delete_row',  function() {
		var id=$(this).attr('id');
		//calculate_total(id);
		$(this).closest("tr").remove();
	});
});

$('.search-btn-field-show').click(function(){
  $('.btn-search').toggleClass('active');
  $('.search-hide-show').toggle();
  if($('.btn-search').hasClass('active')){
    $('input.search-hide-show').focus();
  }
});

$('.showmore-row i').click(function(){
  var id=$(this).attr('data-id');
  $('#'+id+' .popup-append-box').toggleClass('active');
  if($('#'+id+' .popup-append-box').hasClass('active')){
    $(this).html('keyboard_arrow_up');
  }else{
    $(this).html('keyboard_arrow_down');
  }
});