<?php
class Emailtemplate_model extends CI_Model{
		var $table;
	public function  __construct(){
		parent::__construct();
		$this->load->database();
		$this->table ='emailtemplate_master';
	}

		/*
	| -------------------------------------------------------------------
	| Insert data
	| -------------------------------------------------------------------
	|
	| general function to insert data in table
	|
	*/
	public function insertData($data)
	{
		
		$result = $this->db->insert('emailtemplate_master', $data);
		// echo "<pre>";
		// print_r($result);exit();
		if($result == 1){
			return $this->db->insert_id();
		}else{
			return false;
		}
	}
	
	
	/*
	| -------------------------------------------------------------------
	| Update data
	| -------------------------------------------------------------------
	|
	| general function to update data
	|
	*/
	public function updateData($data, $where)
	{
		$this->db->where($where);
		if($this->db->update('emailtemplate_master', $data)){
			return 1;
		}else{
			return 0;
		}
	}

	
		/*
	| -------------------------------------------------------------------
	| Delere data
	| -------------------------------------------------------------------
	|
	| general function to delete the records
	|
	*/
	public function deleteData($table, $data)
	{
		if($this->db->delete($table, $data)){
			return 1;
		}else{
			return 0;
		}
	}


	public function fetchTemplateData($column,$value)
	{
		$this->db->select('*');
		$this->db->from('emailtemplate_master');
		$this->db->where($column,$value);
		
		$query = $this->db->get();
		// echo "<pre>";
		// print_r($query);exit();
		return (array) $query->first_row();
	}


}
?>
