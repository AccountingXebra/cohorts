<?php
class Community_model extends CI_Model{

	public function  __construct() {
		parent::__construct();
		$this->load->database();
	
	}

	public function updateData($table, $data, $where) {

		$this->db->where($where);

		if($this->db->update($table, $data)){

			return 1;
		}else{
			return 0;
		}
	}

	public function updateData1($id) {

		$this->db->select('*');
        $this->db->from('events');
        $this->db->where('id',$id );
        
        $query = $this->db->get();
        return $result = $query->row_array();
	}

	public function deleteData($table, $data)
	{
		if($this->db->delete($table, $data)){

			return 1;
		}else{
			return 0;
		}
	}

	public function getSearchnew($data) {

		$this->db->select('bus.bus_id as code, bus.bus_id as bus_id, bus.reg_id as reg_id, bus.bus_company_name as company_name, bus.nature_of_bus as nature, bus.bus_billing_city as city, bus.bus_company_logo as company_logo, c.country_name, c.country_id, ci.city_id, ci.name, s.state_id, s.state_name');
		$this->db->from('businesslist as bus');
		$this->db->join('countries as c', 'c.country_id = bus.bus_billing_country' ,'left');
		$this->db->join('states as s', 's.state_id = bus.bus_billing_state','left');
		$this->db->join('cities as ci', 'ci.city_id = bus.bus_billing_city','left');
		if(isset($data['multi_loc']) && $data['multi_loc'][0] != "" ){
			$string = implode(",",$data['multi_loc']);
			$this->db->where("bus.bus_billing_city IN (".$string.")");
		}
		if(isset($data['nature_of_business']) && $data['nature_of_business'] != "" ) {
			$this->db->where("bus.nature_of_bus",$data['nature_of_business']);
		}
		$this->db->get();
		$query1 = $this->db->last_query();

		$this->db->select('ev.vendor_unique_code as code, ev.bus_id as bus_id, ev.reg_id as reg_id, ev.vendor_name as company_name, ev.nature_of_bus as nature, ev.vendor_billing_city as city, ev.vendor_shipping_address as company_logo, c.country_name, c.country_id, ci.city_id, ci.name, s.state_id, s.state_name');
		$this->db->from('expense_vendors as ev');
		$this->db->join('countries as c', 'c.country_id = ev.vendor_billing_country' ,'left');
		$this->db->join('states as s', 's.state_id = ev.vendor_billing_state','left');
		$this->db->join('cities as ci', 'ci.city_id = ev.vendor_billing_city','left');
		if(isset($data['multi_loc']) && $data['multi_loc'][0] != "" ){
			$string = implode(",",$data['multi_loc']);
			$this->db->where("ev.vendor_billing_city IN (".$string.")");
		}
		if(isset($data['nature_of_business']) && $data['nature_of_business'] != "" ) {
			$this->db->where("ev.nature_of_bus",$data['nature_of_business']);
		}
		$this->db->get();
		$query2 = $this->db->last_query();

		$this->db->select('sc.cust_unique_code as code, sc.bus_id as bus_id, sc.reg_id as reg_id, sc.cust_name as company_name, sc.nature_of_bus as nature, sc.cust_billing_city as city, sc.cust_shipping_address as company_logo, c.country_name, c.country_id, ci.city_id, ci.name, s.state_id, s.state_name');
		$this->db->from('sales_customers as sc');
		$this->db->join('countries as c', 'c.country_id = sc.cust_billing_country' ,'left');
		$this->db->join('states as s', 's.state_id = sc.cust_billing_state','left');
		$this->db->join('cities as ci', 'ci.city_id = sc.cust_billing_city','left');
		if(isset($data['multi_loc']) && $data['multi_loc'][0] != "" ){
			$string = implode(",",$data['multi_loc']);
			$this->db->where("sc.cust_billing_city IN (".$string.")");
		}
		if(isset($data['nature_of_business']) && $data['nature_of_business'] != "" ) {
			$this->db->where("sc.nature_of_bus",$data['nature_of_business']);
		}
		$this->db->get();
		$query3 = $this->db->last_query();

		$query = $this->db->query($query1." UNION ALL ".$query2 ." UNION ALL ".$query3);

		$result = $query->result_array();
		return $result;
	}

	public function getSearch($data) {

		//print_r($data); exit;
		$this->db->select("bus.*,c.country_name,c.country_id,ci.city_id,ci.name,s.state_id,s.state_name");

		$this->db->from('businesslist as bus');

		$this->db->join('countries as c', 'c.country_id = bus.bus_billing_country' ,'left');
		$this->db->join('states as s', 's.state_id = bus.bus_billing_state','left');
		$this->db->join('cities as ci', 'ci.city_id = bus.bus_billing_city','left');
		$this->db->join('rating as r', 'r.review_id = bus.bus_id', 'left');

		
		if(isset($data['multi_loc']) && $data['multi_loc'][0] != "" ){
			$string = implode(",",$data['multi_loc']);
			$this->db->where("bus.bus_billing_city IN (".$string.")");
		}

		if(isset($data['nature_of_business']) && $data['nature_of_business'] != "" ) {
			$this->db->where("bus.nature_of_bus",$data['nature_of_business']);
		}

		$this->db->order_by('r.review_id',"DESC");

		$query = $this->db->get();
		//print_r($this->db->last_query());exit;
		$result = $query->result_array();
		return $result;		

	}

	public function getKeyword($keyword) {

		$this->db->select('bus.bus_id as code, bus.bus_id as bus_id, bus.reg_id as reg_id, bus.bus_company_name as company_name, bus.nature_of_bus as nature, bus.bus_billing_city as city, bus.bus_company_logo as company_logo, c.country_name, c.country_id, ci.city_id, ci.name, s.state_id, s.state_name');
		$this->db->from('businesslist as bus');
		$this->db->join('countries as c', 'c.country_id = bus.bus_billing_country' ,'left');
		$this->db->join('states as s', 's.state_id = bus.bus_billing_state','left');
		$this->db->join('cities as ci', 'ci.city_id = bus.bus_billing_city','left');
		if(isset($data['multi_loc']) && $data['multi_loc'][0] != "" ){
			$string = implode(",",$data['multi_loc']);
			$this->db->where("bus.bus_billing_city IN (".$string.")");
		}
		if(isset($data['nature_of_business']) && $data['nature_of_business'] != "" ) {
			$this->db->where("bus.nature_of_bus",$data['nature_of_business']);
		}
		$this->db->like('bus.bus_company_name', $keyword);
		$this->db->or_like('bus.nature_of_bus', $keyword);
		$this->db->get();
		$query1 = $this->db->last_query();

		$this->db->select('ev.vendor_unique_code as code, ev.bus_id as bus_id, ev.reg_id as reg_id, ev.vendor_name as company_name, ev.nature_of_bus as nature, ev.vendor_billing_city as city, ev.vendor_shipping_address as company_logo, c.country_name, c.country_id, ci.city_id, ci.name, s.state_id, s.state_name');
		$this->db->from('expense_vendors as ev');
		$this->db->join('countries as c', 'c.country_id = ev.vendor_billing_country' ,'left');
		$this->db->join('states as s', 's.state_id = ev.vendor_billing_state','left');
		$this->db->join('cities as ci', 'ci.city_id = ev.vendor_billing_city','left');
		if(isset($data['multi_loc']) && $data['multi_loc'][0] != "" ){
			$string = implode(",",$data['multi_loc']);
			$this->db->where("ev.vendor_billing_city IN (".$string.")");
		}
		if(isset($data['nature_of_business']) && $data['nature_of_business'] != "" ) {
			$this->db->where("ev.nature_of_bus",$data['nature_of_business']);
		}
		$this->db->like('ev.vendor_name', $keyword);
		$this->db->or_like('ev.nature_of_bus', $keyword);
		$this->db->get();
		$query2 = $this->db->last_query();

		$this->db->select('sc.cust_unique_code as code, sc.bus_id as bus_id, sc.reg_id as reg_id, sc.cust_name as company_name, sc.nature_of_bus as nature, sc.cust_billing_city as city, sc.cust_shipping_address as company_logo, c.country_name, c.country_id, ci.city_id, ci.name, s.state_id, s.state_name');
		$this->db->from('sales_customers as sc');
		$this->db->join('countries as c', 'c.country_id = sc.cust_billing_country' ,'left');
		$this->db->join('states as s', 's.state_id = sc.cust_billing_state','left');
		$this->db->join('cities as ci', 'ci.city_id = sc.cust_billing_city','left');
		if(isset($data['multi_loc']) && $data['multi_loc'][0] != "" ){
			$string = implode(",",$data['multi_loc']);
			$this->db->where("sc.cust_billing_city IN (".$string.")");
		}
		if(isset($data['nature_of_business']) && $data['nature_of_business'] != "" ) {
			$this->db->where("sc.nature_of_bus",$data['nature_of_business']);
		}
		$this->db->like('sc.cust_name', $keyword);
		$this->db->or_like('sc.nature_of_bus', $keyword);
		$this->db->get();
		$query3 = $this->db->last_query();

		$query = $this->db->query($query1." UNION ALL ".$query2 ." UNION ALL ".$query3);
		//print_r($this->db->last_query());exit;
		$result = $query->result_array();
		return $result;
	} 

	public function Get_cities($data) {

		$this->db->select("ci.*");
		$this->db->from('cities as ci');
		$this->db->where("ci.city_id IN (".$data.")");
	
		$this->db->order_by("ci.name","ASC");
		$query = $this->db->get();
		//print_r($this->db->last_query());exit;
		$result= $query->result_array();
		return $result;		
	}

	public function Get_unique_cities() {

		$this->db->select("bus.bus_billing_city");
		$this->db->from('businesslist as bus');
		$this->db->get();
		$query1 = $this->db->last_query();

		$this->db->select("ev.vendor_billing_city");
		$this->db->from('expense_vendors as ev');
		$this->db->get();
		$query2 = $this->db->last_query();

		$this->db->select("sc.cust_billing_city");
		$this->db->from('sales_customers as sc');
		$this->db->get();
		$query3 = $this->db->last_query();
        $query = $this->db->query($query1);
		//$query = $this->db->query($query1." UNION ALL ".$query2 ." UNION ALL ".$query3);
		//print_r($this->db->last_query());exit;
		$result = $query->result_array();
		return $result;		

	}

	public function Get_rating($data) {

		$this->db->distinct();
		$this->db->from('rating as rat');
		$this->db->select("rat.rate");
		//$this->db->where("rat.bus_id",$data['id']);
		$query = $this->db->get();
		//print_r($this->db->last_query());exit;
		$result= $query->result_array();
		return $result;		

	}

	public function insertData($table, $data) {

		$result = $this->db->insert($table, $data);

		if($result == 1) {
			$id = $this->db->insert_id();
			return  $id;
		} else {
			return false;
		}
	}

	public function selectData($table, $fields='*', $where='', $order_by="", $order_type="", $group_by="", $limit="", $rows="", $type='') {

		$this->db->select($fields);
		$this->db->from($table);
		if ($where != "") {
			$this->db->where($where);
		}

		if ($order_by != '') {
			$this->db->order_by($order_by,$order_type);
		}

		if ($group_by != '') {
			$this->db->group_by($group_by);
		}

		if ($limit > 0 && $rows == "") {
			$this->db->limit($limit);
		}
		if ($rows > 0) {
			$this->db->limit($rows, $limit);
		}


		$query = $this->db->get();

		if ($type == "rowcount") {
			$data = $query->num_rows();
		}else{
			$data = $query->result();
		}

		#echo "<pre>"; print_r($this->db->queries); exit;
		$query->result();

		return $data;
	}

	public function getConnections() {

		//print_r($data); exit;
		$this->db->select("bus.*,c.country_name,c.country_id,ci.city_id,ci.name,s.state_id,s.state_name");
		$this->db->from('businesslist as bus');
		//$this->db->from('rating as r');
		$this->db->join('countries as c', 'c.country_id = bus.bus_billing_country' ,'left');
		$this->db->join('states as s', 's.state_id = bus.bus_billing_state','left');
		$this->db->join('cities as ci', 'ci.city_id = bus.bus_billing_city','left');
		$this->db->join('rating as r', 'r.review_id = bus.bus_id', 'left');

		//$this->db->where("bus.bus_company_name like ","%".$data['search']."%");
		//$this->db->or_where("bus.nature_of_bus like ","%".$data['search']."%");
		
		if(isset($data['multi_loc']) && $data['multi_loc'][0] != "" ){
			$string = implode(",",$data['multi_loc']);
			$this->db->where("bus.bus_billing_city IN (".$string.")");
		}

		if(isset($data['nature_of_business']) && $data['nature_of_business'] != "" ) {
			$this->db->where("bus.nature_of_bus",$data['nature_of_business']);
		}

		//$this->db->or_where("r.rating", 'rate');

		$this->db->order_by('r.review_id',"DESC");

		$query = $this->db->get();
		//print_r($this->db->last_query());exit;
		$result = $query->result_array();
		return $result;		

	}

	public function getConnectionRequest($bus_id) {

		$this->db->select("bus.bus_id, bus.reg_id, bus.bus_company_name, bus.bus_company_logo, bus.nature_of_bus, c.country_name, c.country_id, ci.city_id, ci.name, s.state_id, s.state_name, conn.conn_id");

		$this->db->from('businesslist as bus');

		$this->db->join('countries as c', 'c.country_id = bus.bus_billing_country', 'left');
		$this->db->join('states as s', 's.state_id = bus.bus_billing_state', 'left');
		$this->db->join('cities as ci', 'ci.city_id = bus.bus_billing_city', 'left');
		$this->db->join('connections as conn', 'bus.bus_id = conn.bus_id', 'left');

		$this->db->where('conn.accepted', '0');
		$this->db->where('conn.bus_id_connected_to', $bus_id);

		$query = $this->db->get();
		//print_r($this->db->last_query());exit;
		$result = $query->result_array();
		return $result;
	}

	public function getConnectionRequest12($bus_id) {

		$this->db->select("conn.*, bus.reg_id as reg_id, conn.bus_id as bus_id_connected_from, conn.bus_id_connected_to as code_connected_to, bus.bus_company_name as company_name, bus.bus_company_logo as company_logo, bus.nature_of_bus as nature, c.country_name, c.country_id, ci.city_id, ci.name, s.state_id, s.state_name");
		$this->db->from('connections as conn');
		$this->db->join('businesslist as bus', 'conn.bus_id = bus.bus_id', 'RIGHT');
		$this->db->join('countries as c', 'c.country_id = bus.bus_billing_country', 'left');
		$this->db->join('states as s', 's.state_id = bus.bus_billing_state', 'left');
		$this->db->join('cities as ci', 'ci.city_id = bus.bus_billing_city', 'left');
		$this->db->where('conn.accepted', '0');
		$this->db->where('conn.status', '1');
		$this->db->where('conn.bus_id_connected_to', $bus_id);
		$this->db->get();
		$query1 = $this->db->last_query();

		$this->db->select("conn.*, ev.reg_id as reg_id, conn.bus_id as bus_id_connected_from, conn.bus_id_connected_to as code_connected_to, ev.vendor_name as company_name, ev.vendor_shipping_address as company_logo, ev.nature_of_bus as nature, c.country_name, c.country_id, ci.city_id, ci.name, s.state_id, s.state_name");
		$this->db->from('connections as conn');
		$this->db->join('expense_vendors as ev', 'conn.bus_id = ev.vendor_unique_code', 'RIGHT');
		$this->db->join('countries as c', 'c.country_id = ev.vendor_billing_country', 'left');
		$this->db->join('states as s', 's.state_id = ev.vendor_billing_state', 'left');
		$this->db->join('cities as ci', 'ci.city_id = ev.vendor_billing_city', 'left');
		$this->db->where('conn.accepted', '0');
		$this->db->where('conn.status', '1');
		$this->db->where('conn.bus_id_connected_to', $bus_id);
		$this->db->get();
		$query2 = $this->db->last_query();

		$this->db->select("conn.*, sc.reg_id as reg_id, conn.bus_id as bus_id_connected_from, conn.bus_id_connected_to as code_connected_to, sc.cust_name as company_name, sc.cust_shipping_address as company_logo, sc.nature_of_bus as nature, c.country_name, c.country_id, ci.city_id, ci.name, s.state_id, s.state_name");
		$this->db->from('connections as conn');
		$this->db->join('sales_customers as sc', 'conn.bus_id = sc.cust_unique_code', 'RIGHT');
		$this->db->join('countries as c', 'c.country_id = sc.cust_billing_country', 'left');
		$this->db->join('states as s', 's.state_id = sc.cust_billing_state', 'left');
		$this->db->join('cities as ci', 'ci.city_id = sc.cust_billing_city', 'left');
		$this->db->where('conn.accepted', '0');
		$this->db->where('conn.status', '1');
		$this->db->where('conn.bus_id_connected_to', $bus_id);
		$this->db->get();
		$query3 = $this->db->last_query();

		$query 	= $this->db->query($query1." UNION ALL ".$query2 ." UNION ALL ".$query3);
		$result = $query->result_array();
		return $result;		
	}

	public function getConnectionRequest1($bus_id) {

		$this->db->select("bus.bus_id, bus.reg_id, bus.bus_company_name, bus.bus_company_logo, bus.nature_of_bus, c.country_name, c.country_id, ci.city_id, ci.name, s.state_id, s.state_name");

		$this->db->from('businesslist as bus');

		$this->db->join('countries as c', 'c.country_id = bus.bus_billing_country', 'left');
		$this->db->join('states as s', 's.state_id = bus.bus_billing_state', 'left');
		$this->db->join('cities as ci', 'ci.city_id = bus.bus_billing_city', 'left');
		$this->db->join('connections as conn', 'bus.bus_id = conn.bus_id', 'left');

		$this->db->where('conn.accepted', '1');
		$this->db->where('conn.bus_id_connected_to', "$bus_id");

		$query = $this->db->get();
		//print_r($this->db->last_query());exit;
		$result = $query->result_array();
		return $result;
	}

	public function getConnectionSendRequest($bus_id) {

		$this->db->select("conn.*, bus.reg_id as reg_id, conn.bus_id as bus_id_connected_from, conn.bus_id_connected_to as code_connected_to, bus.bus_company_name as company_name, bus.bus_company_logo as company_logo, bus.nature_of_bus as nature, c.country_name, c.country_id, ci.city_id, ci.name, s.state_id, s.state_name");
		$this->db->from('connections as conn');
		$this->db->join('businesslist as bus', 'conn.bus_id_connected_to = bus.bus_id', 'RIGHT');
		$this->db->join('countries as c', 'c.country_id = bus.bus_billing_country', 'left');
		$this->db->join('states as s', 's.state_id = bus.bus_billing_state', 'left');
		$this->db->join('cities as ci', 'ci.city_id = bus.bus_billing_city', 'left');
		$this->db->where('conn.accepted', '0');
		$this->db->where('conn.status', '1');
		$this->db->where('conn.bus_id', $bus_id);
		$this->db->get();
		$query1 = $this->db->last_query();

		$this->db->select("conn.*, ev.reg_id as reg_id, conn.bus_id as bus_id_connected_from, conn.bus_id_connected_to as code_connected_to, ev.vendor_name as company_name, ev.vendor_shipping_address as company_logo, ev.nature_of_bus as nature, c.country_name, c.country_id, ci.city_id, ci.name, s.state_id, s.state_name");
		$this->db->from('connections as conn');
		$this->db->join('expense_vendors as ev', 'conn.bus_id_connected_to = ev.vendor_unique_code', 'RIGHT');
		$this->db->join('countries as c', 'c.country_id = ev.vendor_billing_country', 'left');
		$this->db->join('states as s', 's.state_id = ev.vendor_billing_state', 'left');
		$this->db->join('cities as ci', 'ci.city_id = ev.vendor_billing_city', 'left');
		$this->db->where('conn.accepted', '0');
		$this->db->where('conn.status', '1');
		$this->db->where('conn.bus_id', $bus_id);
		$this->db->get();
		$query2 = $this->db->last_query();

		$this->db->select("conn.*, sc.reg_id as reg_id, conn.bus_id as bus_id_connected_from, conn.bus_id_connected_to as code_connected_to, sc.cust_name as company_name, sc.cust_shipping_address as company_logo, sc.nature_of_bus as nature, c.country_name, c.country_id, ci.city_id, ci.name, s.state_id, s.state_name");
		$this->db->from('connections as conn');
		$this->db->join('sales_customers as sc', 'conn.bus_id_connected_to = sc.cust_unique_code', 'RIGHT');
		$this->db->join('countries as c', 'c.country_id = sc.cust_billing_country', 'left');
		$this->db->join('states as s', 's.state_id = sc.cust_billing_state', 'left');
		$this->db->join('cities as ci', 'ci.city_id = sc.cust_billing_city', 'left');
		$this->db->where('conn.accepted', '0');
		$this->db->where('conn.status', '1');
		$this->db->where('conn.bus_id', $bus_id);
		$this->db->get();
		$query3 = $this->db->last_query();

		$query 	= $this->db->query($query1." UNION ALL ".$query2 ." UNION ALL ".$query3);
		$result = $query->result_array();
		return $result;		
	}

	public function getDetailsoffer($bus_id, $reg_id) {

		$this->db->select("bus.bus_id, reg.bus_id, bus.reg_id, reg.reg_id, bus.bus_company_name, reg.reg_username, reg.reg_email, reg.reg_mobile");

		$this->db->from('businesslist as bus');
		$this->db->join('registration as reg', 'reg.bus_id = bus.bus_id', 'left');

		$this->db->where('reg.reg_id', $reg_id);

		$query = $this->db->get();
		$result = $query->result_array();
		return $result;
	}

	public function getOffers($bus_id, $reg_id, $post) {

		$this->db->from('offers as off');
	
		
		if(!empty($post['offer_category'])) {
			$this->db->where("off.offer_category like ","%".$post['offer_category']."%");
		}

		if(!empty($post['offer_company'])) {
			$this->db->where("off.bus_id like ","%".$post['offer_company']."%");
		}

		if(!empty($post['c_start_date'])) {
			$this->db->where("off.start_date >=", date("Y-m-d", strtotime(str_replace('/', '-', $post['c_start_date']))));
		}

		if(!empty($post['c_end_date'])) {
			$this->db->where("off.end_date <=", date("Y-m-d", strtotime(str_replace('/', '-', $post['c_end_date']))));
		}

		if(!empty($post['search'])) {
			$this->db->where("off.offer_title like ","%".$post['search']."%");
			$this->db->or_where("off.offer_description like ","%".$post['search']."%");
			$this->db->or_where("off.contact_name like ","%".$post['search']."%");
			$this->db->or_where("off.company_name like ","%".$post['search']."%");
		}

		$this->db->where("off.status","1");
		$this->db->order_by("id" ,"DESC");


		$query = $this->db->get();
		
		$result = $query->result_array();
		return $result;
	}

	public function getOffersonprofile($data) {

		$this->db->from('offers');
		
		$this->db->where('bus_id', $data['id']);

		$query = $this->db->get();
		//print_r($this->db->last_query());exit;
		$result = $query->result_array();
		return $result;
	}

	public function getconnectiontype($data) {

		$this->db->from('connections');

		$this->db->where('bus_id_connected_to', $data['id']);
		//$this->db->where('bus_id', $bus_id);
		//$this->db->where('reg_id', $reg_id);
		//$this->db->where('status', '1');

		$query = $this->db->get();
		//print_r($this->db->last_query());exit;
		$result = $query->result_array();
		return $result;
	}

	public function getCompany(){

		$this->db->from('businesslist');

		$query = $this->db->get();
		//print_r($this->db->last_query());exit;
		$result = $query->result_array();
		return $result;
	}

	public function getEvents($bus_id, $reg_id, $post) {

		$this->db->from('events as off');
	

		if(!empty($post['nature'])) {
			$this->db->where("off.event_nature like ","%".$post['nature']."%");
		}

		if(!empty($post['location'])) {
			$this->db->where("off.event_city like ","%".$post['location']."%");
		}

		if(!empty($post['c_start_date'])) {
			$this->db->where("off.start_date >=", date("Y-m-d", strtotime(str_replace('/', '-', $post['c_start_date']))));
		}

		if(!empty($post['c_end_date'])) {
			$this->db->where("off.end_date <=", date("Y-m-d", strtotime(str_replace('/', '-', $post['c_end_date']))));
		}

		if(!empty($post['search'])) {
			$this->db->where("off.event_name like ","%".$post['search']."%");
			$this->db->or_where("off.event_description like ","%".$post['search']."%");
			$this->db->or_where("off.contact_name like ","%".$post['search']."%");
			$this->db->or_where("off.company_name like ","%".$post['search']."%");
		}
		$this->db->order_by("id" ,"DESC");

		$query = $this->db->get();
		
		$result = $query->result_array();
		return $result;
	}

	


	public function getCA($bus_id, $reg_id, $post) {

		$this->db->from('ca_registration as ca');
	

		

		if(!empty($post['location'])) {
			$this->db->where("ca.city like ","%".$post['location']."%");
		}

		$this->db->where("ca.status","Active");

		if(!empty($post['search'])) {
			$this->db->where("ca.city like ","%".$post['search']."%");
			
			$this->db->or_where("ca.reg_email like ","%".$post['search']."%");
			$this->db->or_where("ca.reg_username like ","%".$post['search']."%");
		}
		$this->db->order_by("ca.reg_id" ,"DESC");

		$query = $this->db->get();
		//print $this->db->last_query();
		$result = $query->result_array();
		return $result;
	}


	public function getChallange($post) {

		$this->db->from('challanges as ca');
	

		

		if(!empty($post['location'])) {
			$this->db->where("ca.location like ","%".$post['location']."%");
		}

		if(!empty($post['nature'])) {
			$this->db->where("ca.challange_nature like ","%".$post['nature']."%");
		}

		if(!empty($post['c_start_date'])) {
			$this->db->where("ca.challange_date >= ",date("Y-m-d", strtotime(str_replace('/', '-', $post['c_start_date']))));
		}


		if(!empty($post['c_end_date'])) {
			$this->db->where("ca.challange_date <= ",date("Y-m-d", strtotime(str_replace('/', '-', $post['c_end_date']))));
		}

		

		if(!empty($post['search'])) {
			$this->db->where("ca.location like ","%".$post['search']."%");
			
			$this->db->or_where("ca.challange_name like ","%".$post['search']."%");
			$this->db->or_where("ca.challange_nature like ","%".$post['search']."%");
		}
		$this->db->order_by("ca.id" ,"DESC");

		$query = $this->db->get();
		//print $this->db->last_query(); exit;
		$result = $query->result_array();
		return $result;
	}


	public function getLocation() {

		$this->db->select('distinct(ev.event_city)');
		$this->db->from('events as ev');

		$query = $this->db->get();
		//print_r($this->db->last_query());exit;
		$result = $query->result_array();
		return $result;
	}


	public function getNews() {

		$this->db->select('*');
		$this->db->from('newsletters as nw');

		$this->db->group_by("nw.newsletter_year,nw.newsletter_month");
		$this->db->order_by("nw.newsletter_year" ,"DESC");

		$query = $this->db->get();
	//	print_r($this->db->last_query());exit;
		$result = $query->result_array();
		return $result;
	}
	public function getnewss() {

		$this->db->select('*');
		$this->db->from('blogs as nw');

		$this->db->where("nw.category","news");

		$this->db->group_by("nw.newsletter_year,nw.newsletter_month");
		$this->db->order_by("nw.newsletter_year" ,"DESC");

		$query = $this->db->get();
	//	print_r($this->db->last_query());exit;
		$result = $query->result_array();
		return $result;
	}

	public function profileviewinfo($data)
	{
		$this->db->select('bus.bus_id as code, bus.bus_id as bus_id, bus.reg_id as reg_id, bus.bus_company_name as company_name, bus.nature_of_bus as nature, bus.bus_billing_city as city, bus.bus_company_logo as company_logo, bus.bus_facebook as facebook, bus.bus_linkedin as linkedin, bus.bus_twitter as twitter, bus.bus_googleplus as googleplus, bus.bus_incorporation_date as incorporation_date, bus.bus_company_size as company_size, bus.bus_company_revenue as company_revenue, bus.bus_billing_address as billing_address, bus.bus_billing_zipcode as billing_zipcode, bus.bus_services_keywords as services_keywords, bus.bus_website_url as website_url, bus.bus_case_study as case_study, c.country_name, c.country_id, ci.city_id, ci.name, s.state_id, s.state_name');
		$this->db->from('businesslist as bus');
		$this->db->join('countries as c', 'c.country_id = bus.bus_billing_country' ,'left');
		$this->db->join('states as s', 's.state_id = bus.bus_billing_state','left');
		$this->db->join('cities as ci', 'ci.city_id = bus.bus_billing_city','left');
		$this->db->where('bus.bus_id', $data['id']);
		$this->db->get();
		$query1 = $this->db->last_query();

		$this->db->select('ev.vendor_unique_code as code, ev.bus_id as bus_id, ev.reg_id as reg_id, ev.vendor_name as company_name, ev.nature_of_bus as nature, ev.vendor_billing_city as city, ev.vendor_shipping_address as company_logo, ev.vendor_shipping_address as facebook, ev.vendor_shipping_address as twitter, ev.vendor_shipping_address as linkedin, ev.vendor_shipping_address as googleplus, ev.vendor_shipping_address as incorporation_date, ev.vendor_shipping_address as company_size, ev.vendor_shipping_address as company_revenue, ev.vendor_billing_address as billing_address, ev.vendor_billing_pincode as billing_zipcode, ev.vendor_shipping_address as services_keywords, ev.vendor_shipping_address as website_url, ev.vendor_shipping_address as case_study, c.country_name, c.country_id, ci.city_id, ci.name, s.state_id, s.state_name');
		$this->db->from('expense_vendors as ev');
		$this->db->join('countries as c', 'c.country_id = ev.vendor_billing_country' ,'left');
		$this->db->join('states as s', 's.state_id = ev.vendor_billing_state','left');
		$this->db->join('cities as ci', 'ci.city_id = ev.vendor_billing_city','left');
		$this->db->where('ev.vendor_unique_code', $data['id']);
		$this->db->get();
		$query2 = $this->db->last_query();

		$this->db->select('sc.cust_unique_code as code, sc.bus_id as bus_id, sc.reg_id as reg_id, sc.cust_name as company_name, sc.nature_of_bus as nature, sc.cust_billing_city as city, sc.cust_shipping_address as company_logo, sc.cust_shipping_address as facebook, sc.cust_shipping_address as twitter, sc.cust_shipping_address as linkedin, sc.cust_shipping_address as googleplus, sc.cust_shipping_address as incorporation_date, sc.cust_shipping_address as company_size, sc.cust_shipping_address as company_revenue, sc.cust_billing_address as billing_address, sc.cust_billing_zipcode as billing_zipcode, sc.cust_shipping_address as services_keywords, sc.cust_shipping_address as website_url, sc.cust_shipping_address as case_study,c.country_name, c.country_id, ci.city_id, ci.name, s.state_id, s.state_name');
		$this->db->from('sales_customers as sc');
		$this->db->join('countries as c', 'c.country_id = sc.cust_billing_country' ,'left');
		$this->db->join('states as s', 's.state_id = sc.cust_billing_state','left');
		$this->db->join('cities as ci', 'ci.city_id = sc.cust_billing_city','left');
		$this->db->where('sc.cust_unique_code', $data['id']);
		$this->db->get();
		$query3 = $this->db->last_query();

		$this->db->select('conn.conn_id as code, conn.bus_id as bus_id, conn.reg_id as reg_id, conn.message as company_name, conn.message as nature, conn.message as city, conn.message as company_logo, conn.message as facebook, conn.message as twitter, conn.message as linkedin, conn.message as googleplus, conn.message as incorporation_date, conn.message as company_size, conn.message as company_revenue, conn.message as billing_address, conn.message as billing_zipcode, conn.message as services_keywords, conn.message as website_url, conn.message as case_study,c.country_name, c.country_id, ci.city_id, ci.name, s.state_id, s.state_name');
		$this->db->from('connections as conn');
		$this->db->join('countries as c', 'c.country_id = conn.message' ,'left');
		$this->db->join('states as s', 's.state_id = conn.message','left');
		$this->db->join('cities as ci', 'ci.city_id = conn.message','left');
		$this->db->where('conn.conn_id', $data['id']);
		$this->db->get();
		$query4 = $this->db->last_query();

		$query = $this->db->query($query1." UNION ALL ".$query2 ." UNION ALL ".$query3 ." UNION ALL".$query4);

		$result = $query->result_array();
		return $result;	

	}

	public function deleteEvent($id) {

		$this->db->where('id', $id);
		$this->db->delete('events');
	}
	
	public function getWorkspace($post) {

		$this->db->from('coworking_space as ws');
	

		if(!empty($post['name'])) {
			$this->db->where("ws.name like ","%".$post['name']."%");
		}

		if(!empty($post['country'])) {
			$this->db->where("ws.country ",$post['country']);
		}

		if(!empty($post['city'])) {
			$this->db->where("ws.city ",$post['city']);
		}

		

		
		$this->db->order_by("id" ,"DESC");

		$query = $this->db->get();
		
		$result = $query->result_array();
		return $result;
	}


	public function getIncubation($post) {

		$this->db->from('incubation as ic');
	

		if(!empty($post['name'])) {
			$this->db->where("ic.name like ","%".$post['name']."%");
		}

		if(!empty($post['country'])) {
			$this->db->where("ic.country ",$post['country']);
		}

		if(!empty($post['city'])) {
			$this->db->where("ic.city ",$post['city']);
		}

		

		
		$this->db->order_by("id" ,"DESC");

		$query = $this->db->get();
		
		$result = $query->result_array();
		return $result;
	}

}