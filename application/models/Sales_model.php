<?php
class Sales_model extends CI_Model{
		
	public function  __construct(){
		parent::__construct();
		$this->load->database();
	
	}


/*
	| -------------------------------------------------------------------
	| check unique fields
	| -------------------------------------------------------------------
	|
	*/
	public function isUnique($table, $field, $value,$id='')
	{
		$this->db->select('*');
		$this->db->from($table);
		$this->db->where($field,$value);
		if($id!='')
		{
			$this->db->where("id != ",$id);
		}
		$query = $this->db->get();
		
		$data = $query->num_rows();
		return ($data > 0)?FALSE:TRUE;
	}

		/*
	| -------------------------------------------------------------------
	| Insert data
	| -------------------------------------------------------------------
	|
	| general function to insert data in table
	|
	*/
	public function insertData($table, $data)
	{
		
		$result = $this->db->insert($table, $data);
		// echo "<pre>";
		// print_r($result);exit();
		if($result == 1){

			$id=$this->db->insert_id();
		
			return  $id;

		}else{
			return false;
		}
	}

	public function insertData_batch($table, $data)
	{
		
		$this->db->insert_batch($table, $data);
		return $this->db->insert_id();
	}
	
	
	/*
	| -------------------------------------------------------------------
	| Update data
	| -------------------------------------------------------------------
	|
	| general function to update data
	|
	*/
	public function updateData($table, $data, $where)
	{
		$this->db->where($where);
		if($this->db->update($table, $data)){

			return 1;
		}else{
			return 0;
		}
	}
	public function updateBatch($table, $data, $where)
	{
		
		if($this->db->update_batch($table, $data,$where)){
			return 1;
		}else{
			return 0;
		}
	}
	
	
	function get_single($table, $filter)
	{
		$data = $this->db->get_where($table, $filter);
		if($data->num_rows() > 0)
		{
			$data = $data->first_row("array");
			return $data;
		}
		else
		{
			return false;
		}
	}
	/*
	| -------------------------------------------------------------------
	| Select data
	| -------------------------------------------------------------------
	|
	| general function to get result by passing nesessary parameters
	|
	*/
	public function selectData($table, $fields='*', $where='', $order_by="", $order_type="", $group_by="", $limit="", $rows="", $type='')
	{
		$this->db->select($fields);
		$this->db->from($table);
		if ($where != "") {
			$this->db->where($where);
		}

		if ($order_by != '') {
			$this->db->order_by($order_by,$order_type);
		}

		if ($group_by != '') {
			$this->db->group_by($group_by);
		}

		if ($limit > 0 && $rows == "") {
			$this->db->limit($limit);
		}
		if ($rows > 0) {
			$this->db->limit($rows, $limit);
		}


		$query = $this->db->get();
        // echo $this->db->last_query();
		if ($type == "rowcount") {
			$data = $query->num_rows();
		}else{
			$data = $query->result();
		}

		#echo "<pre>"; print_r($this->db->queries); exit;
		$query->result();

		return $data;
	}

		/*
	| -------------------------------------------------------------------
	| Role List
	| -------------------------------------------------------------------
	|
	| general function to delete the records
	|
	*/
	
	
	public function selectRoleData($table){
		$query = $this->db->get($table);
		if($query!=NULL){
			return $query->result();
		}
		else{
			return false;
		}
	}
	public function selectcustData($table,$bus_id){
		
		$this->db->select('bill.*,cust.cust_id,cust.cust_name,reg.reg_username');
		$this->db->from($table.' as bill');
		$this->db->join('sales_customers as cust', 'cust.cust_id = bill.cust_id','left');
		$this->db->join('registration as reg', 'reg.reg_id = bill.reg_id','left');
		$this->db->where('bill.bus_id',$bus_id);
		
		$query = $this->db->get();
		return $query->result();

	}

	public function selectSer($table,$ser_id){
		
		$this->db->select('*');
		$this->db->from($table.' as ser');
		$this->db->join('services_other_taxes as other_tax', 'ser.service_id = other_tax.service_id','left');
		
		$this->db->where('ser.service_id',$ser_id);
		
		$query = $this->db->get();
		//print $this->db->last_query();
		return $query->result();

	}

	
		/*
	| -------------------------------------------------------------------
	| Delete data
	| -------------------------------------------------------------------
	|
	| general function to delete the records
	|
	*/
	public function deleteData($table, $data)
	{
		if($this->db->delete($table, $data)){

			return 1;
		}else{
			return 0;
		}
	}
	public function customerFilter($Data,$sort_field,$orderBy,$c)
	{
		$this->db->select("cus.*,c.country_id,c.country_name,s.state_id,s.state_name,con.cp_id,con.cp_name,con.cp_email,con.cp_mobile,con.cp_is_admin,con.type");
		$this->db->from('sales_customers as cus');

		if(!empty($Data['search']))
		{
			$this->db->group_start();
			$this->db->where("cus.cust_name like ","%".$Data['search']."%");
			$this->db->or_where("cus.cus_portal_access like ","%".$Data['search']."%");
			$this->db->or_where("cus.is_invited like ","%".$Data['search']."%");
			$this->db->or_where("c.country_name like ","%".$Data['search']."%");
			$this->db->or_where("s.state_name like ","%".$Data['search']."%");
			$this->db->or_where("con.cp_name like ","%".$Data['search']."%");
			$this->db->or_where("con.cp_email like ","%".$Data['search']."%");
			$this->db->or_where("con.cp_mobile like ","%".$Data['search']."%");
			$this->db->or_where("cus.cust_credit_period like ","%".$Data['search']."%");
			$this->db->group_end();
		}
		$this->db->join('countries as c', 'c.country_id = cus.cust_billing_country' ,'left');
		$this->db->join('states as s', 's.state_id = cus.cust_billing_state','left');
		$this->db->join('(SELECT * FROM sales_customer_contacts ORDER BY cp_is_admin ASC LIMIT 10000000000000000000) con', 'con.cust_id = cus.cust_id and con.type = "customer"','left');
		if($Data['country']!=''){
			$this->db->where("cus.cust_billing_country",$Data['country']);
		}
		if($Data['state']!=''){
			$this->db->where("cus.cust_billing_state",$Data['state']);
		}

		if($Data['status']!=''){
			$this->db->where("cus.status",$Data['status']);
		}
		//$this->db->where("cus.reg_id",$Data['reg_id']);
		$this->db->where("cus.bus_id",$Data['bus_id']);
		
		$this->db->group_by("cus.cust_id");
		$this->db->order_by("".$sort_field." ".$orderBy."");

		if( $c == 1)
		{
			if($Data['length']!= -1){		
			$this->db->limit($Data['length'],$Data['start']);
			}
			$query = $this->db->get();
			//print_r($this->db->last_query());exit;
			$result= $query->result_array();
			
			return $result;			
		}
		else
		{		
			
			$query = $this->db->get();
			$result['NumRecords']=$query->num_rows();		
			return $result;		
		}

	}

	public function downloadMultiple($array,$reg_id,$bus_id){
		$this->db->select('cus.*,c.*,s.*,con.*,ci.*,cur.*,sc.country_name as scountry,ss.state_name as sstate,sci.name as scity');
		$this->db->from('sales_customers as cus');
		$this->db->join('countries as c', 'c.country_id = cus.cust_billing_country');
		$this->db->join('countries as sc', 'sc.country_id = cus.cust_shipping_country','left');
		$this->db->join('states as ss', 'ss.state_id = cus.cust_shipping_state','left');
		$this->db->join('states as s', 's.state_id = cus.cust_billing_state');
		$this->db->join('cities as sci', 'sci.city_id = cus.cust_shipping_city','left');
		$this->db->join('cities as ci', 'ci.city_id = cus.cust_billing_city');
		$this->db->join('currency as cur', 'cur.currency_id = cus.cust_currency','left');
		$this->db->join('sales_customer_contacts as con', 'con.cust_id = cus.cust_id ','left');
		//$this->db->where('con.type','customer');
		$this->db->where_in('cus.cust_id',$array);
		$this->db->where('con.cp_is_admin',1);
		//$this->db->where('cus.reg_id',$reg_id);
		$this->db->where('cus.bus_id',$bus_id);
		$query = $this->db->get();
		return $query->result_array();
	}
	/*--------------EXPENSE LIST MODAL------------------*/


	public function expenseForm($Data,$sort_field,$orderBy,$c)
	{
		$this->db->select("eform.*,ecat.id,ecat.category_name,cur.currencycode");
		$this->db->from('expense_basic as eform');

		if(!empty($Data['search']['value']))
		{
			$this->db->where("eform.eb_exp_date like ","%".$Data['search']['value']."%");
			$this->db->or_where("eform.eb_exp_no like ","%".$Data['search']['value']."%");
			$this->db->or_where("eform.eb_exp_type like ","%".$Data['search']['value']."%");
			$this->db->or_where("eform.eb_exp_total like ","%".$Data['search']['value']."%");
			$this->db->or_where("eform.pay_status like ","%".$Data['search']['value']."%");
			$this->db->or_where("ecat.category_name like ","%".$Data['search']['value']."%");
		}
		$this->db->join('exp_category as ecat', 'ecat.id = eform.eb_exp_type' ,'left');
		$this->db->join('businesslist as bus', 'bus.bus_id = eform.bus_id' ,'left');
		$this->db->join('currency as cur', 'cur.currency_id = bus.bus_currency_format' ,'left');
		if($Data['expense_type']!=''){
			$this->db->or_where("eform.eb_exp_type",$Data['expense_type']);
		}
		if($Data['start_date']!=''){
			$this->db->where('eform.eb_exp_startdate >=',$Data['start_date']);
		}
		if($Data['end_date']!=''){
			$this->db->where('eform.eb_exp_enddate <=',$Data['start_date']);
		}
		if($Data['pay_status']!=''){
			$this->db->where("eform.pay_status",$Data['pay_status']);
		}

		$this->db->order_by("".$sort_field." ".$orderBy."");

		if( $c == 1)
		{
			if($Data['length']!= -1){		
			$this->db->limit($Data['length'],$Data['start']);
			}
			$query = $this->db->get();

			$result= $query->result_array();
			
			return $result;			
		}
		else
		{		
			
			$query = $this->db->get();
			$result['NumRecords']=$query->num_rows();		
			return $result;		
		}

	}
	public function joinCustomerDetails($id,$reg_id,$bus_id,$invited=NULL)
	{
	
	$this->db->select('cus.*,cus.status as custatus,c.*,s.*,con.*,con.cust_id as c_id,ci.*,cur.*,sc.country_name as scountry,ss.state_name as sstate,sci.name as scity');
	$this->db->from('sales_customers as cus');
	$this->db->join('countries as c', 'c.country_id = cus.cust_billing_country','left');
	$this->db->join('countries as sc', 'sc.country_id = cus.cust_shipping_country','left');
	$this->db->join('states as ss', 'ss.state_id = cus.cust_shipping_state','left');
	$this->db->join('states as s', 's.state_id = cus.cust_billing_state','left');
	$this->db->join('cities as sci', 'sci.city_id = cus.cust_shipping_city','left');
	$this->db->join('cities as ci', 'ci.city_id = cus.cust_billing_city','left');
	$this->db->join('currency as cur', 'cur.currency_id = cus.cust_currency','left');
	$this->db->join('sales_customer_contacts as con', 'cus.cust_id = con.cust_id  and con.type = "customer"','left');
	//$this->db->where('con.type','customer');
	/*if($invited!=NULL){
		$this->db->where_not_in('cus.is_invited',1);
	}*/
	if($id!=''){
	$this->db->where_in('cus.cust_id',$id);
	}
	//$this->db->where('cus.reg_id',$reg_id);
	
	$this->db->where('cus.bus_id',$bus_id);
	$query = $this->db->get();
	
	return $query->result();
	}

	public function joinGstDetails($id,$type)
	{
	
	$this->db->select('gst.*,c.*,s.*,ci.*');
	$this->db->from('gst_number as gst');
	$this->db->join('countries as c', 'c.country_id = gst.country','left');
	$this->db->join('states as s', 's.state_id = gst.state_code','left');
	$this->db->join('cities as ci', 'ci.city_id = gst.city','left');

	$this->db->where('gst.bus_id',$id);
	$this->db->where('gst.type',$type);
	//$this->db->where('gst.status','Active');
	$query = $this->db->get();
	//echo $this->db->last_query();
	return $query->result();
	}

	public function customerDetails($id,$reg_id,$bus_id)
	{
	
	$this->db->select('cus.*,c.*,s.*,ci.*,cur.*,sc.country_name as scountry,ss.state_name as sstate,sci.name as scity');
	$this->db->from('sales_customers as cus');
	$this->db->join('countries as c', 'c.country_id = cus.cust_billing_country');
	$this->db->join('countries as sc', 'sc.country_id = cus.cust_shipping_country','left');
	$this->db->join('states as ss', 'ss.state_id = cus.cust_shipping_state','left');
	$this->db->join('states as s', 's.state_id = cus.cust_billing_state');
	$this->db->join('cities as sci', 'sci.city_id = cus.cust_shipping_city','left');
	$this->db->join('cities as ci', 'ci.city_id = cus.cust_billing_city');
	$this->db->join('currency as cur', 'cur.currency_id = cus.cust_currency','left');
	$this->db->where('cus.cust_id',$id);
	//$this->db->where('cus.reg_id',$reg_id);
	$this->db->where('cus.bus_id',$bus_id);
	$query = $this->db->get();
	return $query->result();
	}


	public function expenseVoucher($Data,$sort_field,$orderBy,$c) {

		$this->user_session['reg_id'];

		$this->db->select("exp_voc.*,sc.cust_id,sc.cust_name,c.country_id,c.country_name,s.state_id,s.state_name,evl.evl_total_amt,evi.evi_id,count(evi.evi_ev_id) as ev_id_count");
		$this->db->from('sales_expense_voucher as exp_voc');
		//$this->db->from('sales_invoices_list as sil');

		if(!empty($Data['search']['value'])) {
			$this->db->where("exp_voc.ev_exp_vchr_no like ","%".$Data['search']['value']."%");
			$this->db->or_where("exp_voc.cust_id like ","%".$Data['search']['value']."%");
			$this->db->or_where("sc.cust_name like ","%".$Data['search']['value']."%");
			$this->db->or_where("c.country_name like ","%".$Data['search']['value']."%");
			$this->db->or_where("s.state_name like ","%".$Data['search']['value']."%");
			$this->db->or_where("evl.evl_total_amt like ","%".$Data['search']['value']."%");
			$this->db->or_where("evi.evi_ev_id like ","%".$Data['search']['value']."%");
		}

		$this->db->join('sales_expense_voucher_list as evl', 'evl.ev_id = exp_voc.ev_id' ,'left');
		$this->db->join('businesslist as bus', 'bus.bus_id = exp_voc.bus_id','left');
		$this->db->join('sales_customers as sc', 'sc.cust_id = exp_voc.cust_id','left');
		$this->db->join('sales_expense_voucher_image as evi', 'evi.evi_ev_id = exp_voc.ev_id' ,'left');
		//$this->db->join('sales_invoices as si', 'si.inv_id = sil.inv_id', 'left');
		$this->db->join('countries as c', 'c.country_id = sc.cust_billing_country' ,'left');
		$this->db->join('states as s', 's.state_id = sc.cust_billing_state','left');
        $this->db->where("exp_voc.bus_id",$Data['bus_id']);
		$this->db->where("exp_voc.status","Active");

        if($this->user_session['reg_admin_type']==5 || $this->session->userdata['user_session']['reg_admin_type'] == 2 || $this->session->userdata['user_session']['reg_admin_type'] == 4  ){

			$this->db->where("exp_voc.reg_id",$Data['reg_id']);

			if(count($Data['employee']) >0){
				$this->db->where("exp_voc.emp_code ", $Data['employee'][0]->emp_id);
				$this->db->or_where("exp_voc.approver_name like ", "%".$Data['employee'][0]->emp_id."%");
			}

		}

		if(@$Data['search_by_emp_name']!=''){
				$this->db->where("exp_voc.emp_code",$Data['search_by_emp_name']);
		}
	
		

 
			if(@$Data['search_by_client_name']!=''){
				$this->db->where("exp_voc.cust_id",$Data['search_by_client_name']);
				// print_r($Data['search_by_client_name']);

		}
		

			if(@$Data['inv_start_date']!=''){
			$this->db->where("exp_voc.exp_voi_date>=",date('Y-m-d',strtotime(str_replace('/', '-',$Data['inv_start_date']))));
		}

		    if(@$Data['inv_end_date']!=''){
			$this->db->where("exp_voc.exp_voi_date<=",date('Y-m-d',strtotime(str_replace('/', '-',$Data['inv_end_date']))));
		}

		if(@$sort_field!='' && @$orderBy!='' ){
		$this->db->group_by("exp_voc.ev_id");
		$this->db->order_by("".$sort_field." ".$orderBy."");
		//exit();
    }

		if( $c == 1)
		{
			if(@$Data['length']!= -1){		
			$this->db->limit($Data['length'],$Data['start']);
			}
			$query = $this->db->get();
			//print $this->db->last_query(); exit();
			$result= $query->result_array();
			return $result;			
		}
		else
		{		
			$query = $this->db->get();
			$result['NumRecords']=$query->num_rows();		
			return $result;		
		}

	}
	
	public function join_Expense_Voucher($id)
	{
	
	$this->db->select('evl.*,evi.*');
	$this->db->from('sales_expense_voucher_list as evl');
	$this->db->join('sales_expense_voucher_image as evi', 'evl.evl_id = evi.evl_id' ,'left');
	$this->db->where('evl.ev_id',$id);
	$this->db->group_by("evl.evl_id");
	$query = $this->db->get();
	//echo $this->db->last_query(); exit();
	return $query->result();
	}

	/*-- Credit Debit Motes */
	public function creditDebitFilter($Data,$sort_field,$orderBy,$c)
	{
		$this->db->select("cd.*,c.country_id,c.country_name,s.state_id,s.state_name,cus.cust_name,cus.cust_billing_country,cus.cust_billing_state,cus.cust_currency,cur.currency_id,cur.currencycode");
		$this->db->from('sales_credit_debit as cd');

		if(!empty($Data['search']))
		{
			$this->db->group_start();
			$this->db->where("cus.cust_name like ","%".$Data['search']."%");
			$this->db->or_where("cd.cd_note_date like ","%".$Data['search']."%");
			$this->db->or_where("cd.cd_note_no like ","%".$Data['search']."%");
			$this->db->or_where("cd.cd_invoice_no like ","%".$Data['search']."%");
			$this->db->or_where("cd.cd_grant_total like ","%".$Data['search']."%");
			$this->db->or_where("c.country_name like ","%".$Data['search']."%");
			$this->db->or_where("s.state_name like ","%".$Data['search']."%");
			$this->db->or_where("cur.currencycode like ","%".$Data['search']."%");
			$this->db->group_end();
		}
		$this->db->join('countries as c', 'c.country_id = cd.cd_billing_country' ,'left');
		$this->db->join('states as s', 's.state_id = cd.cd_billing_state','left');
		$this->db->join('sales_customers as cus', 'cus.cust_id = cd.cust_id','left');
		$this->db->join('currency as cur', 'cur.currency_id = cus.cust_currency','left');
		if($Data['search_by_customer']!=''){
			$this->db->where("cd.cust_id",$Data['search_by_customer']);
		}
		if($Data['search_by_type']!=''){
			$this->db->where("cd.cd_note_type",$Data['search_by_type']);
		}
		if($Data['cd_start_date']!=''){
			$this->db->where("cd.cd_note_date >=",date('Y-m-d',strtotime(str_replace('/', '-',$Data['cd_start_date']))));
		}
		if($Data['cd_end_date']!=''){
			$this->db->where("cd.cd_note_date <=",date('Y-m-d',strtotime(str_replace('/', '-',$Data['cd_end_date']))));
		}
		//$this->db->where("cd.reg_id",$Data['reg_id']);
		$this->db->where("cd.bus_id",$Data['bus_id']);
		//$this->db->where("cd.cd_note_type",$Data['note_type']);
		$this->db->where("cd.status","Active");
		
		$this->db->group_by("cd.cd_id");
		$this->db->order_by("".$sort_field." ".$orderBy."");

		if( $c == 1)
		{
			if($Data['length']!= -1){		
			if($Data['length']!= -1){		
			$this->db->limit($Data['length'],$Data['start']);
			}
			}
			$query = $this->db->get();
			//print_r($this->db->last_query());exit;
			$result= $query->result_array();
			
			return $result;			
		}
		else
		{		
			
			$query = $this->db->get();
			$result['NumRecords']=$query->num_rows();		
			return $result;		
		}

	}
	
	public function creditdebitDetails($id,$reg_id,$bus_id)
	{
	
	$this->db->select('cd.*,c.*,s.*,ci.*,cl.*,sc.country_name as scountry,ss.state_name as sstate,sci.name as scity,cus.cust_name');
	$this->db->from('sales_credit_debit as cd');
	$this->db->join('countries as c', 'c.country_id = cd.cd_billing_country');
	$this->db->join('countries as sc', 'sc.country_id = cd.cd_billing_country','left');
	$this->db->join('states as ss', 'ss.state_id = cd.cd_billing_state','left');
	$this->db->join('states as s', 's.state_id = cd.cd_billing_state');
	$this->db->join('cities as sci', 'sci.city_id = cd.cd_billing_city','left');
	$this->db->join('cities as ci', 'ci.city_id = cd.cd_billing_city');
	$this->db->join('sales_credit_debit_list as cl', 'cl.cd_id = cd.cd_id','left');
	$this->db->join('sales_customers as cus', 'cus.cust_id = cd.cust_id','left');
	$this->db->where('cd.cd_id',$id);
	//$this->db->where('cd.reg_id',$reg_id);
	$this->db->where('cd.bus_id',$bus_id);
	$query = $this->db->get();
	//echo $this->db->last_query(); exit();
	return $query->result();
	}
	public function salesReturn($Data,$sort_field,$orderBy,$c)
	{
		$this->db->select("sr.*,sc.cust_id,sc.cust_name,c.country_id,c.country_name,s.state_id,s.state_name");
		$this->db->from('sales_returns as sr');

		if(!empty($Data['search']['value']))
		{
			$this->db->where("sr.rtn_sr_no like ","%".$Data['search']['value']."%");
			$this->db->or_where("sr.rtn_sr_date like ","%".$Data['search']['value']."%");
			$this->db->where("sr.rtn_sr_invoice_no like ","%".$Data['search']['value']."%");
			$this->db->where("sr.rtn_grand_total like ","%".$Data['search']['value']."%");
			$this->db->or_where("sc.cust_name like ","%".$Data['search']['value']."%");
			$this->db->or_where("c.country_name like ","%".$Data['search']['value']."%");
			$this->db->or_where("s.state_name like ","%".$Data['search']['value']."%");

		}
		$this->db->join('businesslist as bus', 'bus.bus_id = sr.bus_id','left');
		$this->db->join('sales_customers as sc', 'sc.cust_id = sr.cust_id','left');
		$this->db->join('countries as c', 'c.country_id = sc.cust_billing_country' ,'left');
		$this->db->join('states as s', 's.state_id = sc.cust_billing_state','left');

		$this->db->where("sr.reg_id",$Data['reg_id']);
		$this->db->where("sr.bus_id",$Data['bus_id']);
		$this->db->where("sr.status","Active");
		if( $c == 1)
		{
			if($Data['length']!= -1){		
			$this->db->limit($Data['length'],$Data['start']);
			}
			$query = $this->db->get();
			$result= $query->result_array();
			return $result;			
		}
		else
		{		
			$query = $this->db->get();
			$result['NumRecords']=$query->num_rows();		
			return $result;		
		}

	}

	/* ...delivery challan... */

	public function deliveyChallanFilter($Data,$sort_field,$orderBy,$c)
	{
		$this->db->select("dc.*,c.country_id,c.country_name,s.state_id,s.state_name,cus.cust_name,cus.cust_billing_country,cus.cust_billing_state,cus.cust_currency,cur.currency_id,cur.currencycode");
		$this->db->from('sales_delivery_challan as dc');

		if(!empty($Data['search']['value']))
		{
			$this->db->where("cus.cust_name like ","%".$Data['search']['value']."%");
			//$this->db->where("dc.status","Active");
			$this->db->or_where("dc.dc_date like ","%".$Data['search']['value']."%");
			$this->db->or_where("dc.dc_no like ","%".$Data['search']['value']."%");
			$this->db->or_where("dc.dc_igst_total like ","%".$Data['search']['value']."%");
			$this->db->or_where("dc.dc_cgst_total like ","%".$Data['search']['value']."%");
			$this->db->or_where("dc.dc_sgst_total like ","%".$Data['search']['value']."%");
			$this->db->or_where("dc.dc_grand_total like ","%".$Data['search']['value']."%");
			$this->db->or_where("c.country_name like ","%".$Data['search']['value']."%");
			$this->db->or_where("s.state_name like ","%".$Data['search']['value']."%");

		}
		$this->db->join('countries as c', 'c.country_id = dc.dc_billing_country' ,'left');
		$this->db->join('states as s', 's.state_id = dc.dc_billing_state','left');
		$this->db->join('sales_customers as cus', 'cus.cust_id = dc.cust_id','left');
		$this->db->join('currency as cur', 'cur.currency_id = cus.cust_currency','left');
		if($Data['search_by_customer']!=''){
			$this->db->where("dc.cust_id",$Data['search_by_customer']);
		}
		
		//$this->db->where("dc.reg_id",$Data['reg_id']);
		$this->db->where("dc.bus_id",$Data['bus_id']);
		$this->db->where("dc.status","Active");
		
		$this->db->group_by("dc.dc_id");
		$this->db->order_by("".$sort_field." ".$orderBy."");

		if( $c == 1)
		{
			if($Data['length']!= -1){		
			$this->db->limit($Data['length'],$Data['start']);
			}
			$query = $this->db->get();
			//print_r($this->db->last_query());exit;
			$result= $query->result_array();
			
			return $result;			
		}
		else
		{		
			
			$query = $this->db->get();
			$result['NumRecords']=$query->num_rows();		
			return $result;		
		}

	}
	public function deliveryChallanDetails($id,$reg_id,$bus_id)
	{
	
	$this->db->select('dc.*,c.*,s.*,ci.*,dcl.*,sc.country_name as scountry,ss.state_name as sstate,sci.name as scity');
	$this->db->from('sales_delivery_challan as dc');
	$this->db->join('countries as c', 'c.country_id = dc.dc_billing_country');
	$this->db->join('countries as sc', 'sc.country_id = dc.dc_shipping_country','left');
	$this->db->join('states as ss', 'ss.state_id = dc.dc_shipping_state','left');
	$this->db->join('states as s', 's.state_id = dc.dc_billing_state');
	$this->db->join('cities as sci', 'sci.city_id = dc.dc_shipping_city','left');
	$this->db->join('cities as ci', 'ci.city_id = dc.dc_billing_city');
	$this->db->join('sales_delivery_challan_list as dcl', 'dcl.dc_id = dc.dc_id','left');
	$this->db->where('dc.dc_id',$id);
	//$this->db->where('dc.reg_id',$reg_id);
	$this->db->where('dc.bus_id',$bus_id);
	$query = $this->db->get();
	return $query->result();
	}

	/*-------START BILL OF SUPPLY---------*/
	public function billSupply($Data,$sort_field,$orderBy,$c)
	{
		$this->db->select("bos.*,sc.cust_id,sc.cust_name,c.country_id,c.country_name,s.state_id,s.state_name");
		$this->db->from('sales as bos');

		if(!empty($Data['search']['value']))
		{
			$this->db->where("bos.rtn_sr_no like ","%".$Data['search']['value']."%");
			$this->db->or_where("bos.rtn_sr_date like ","%".$Data['search']['value']."%");
			$this->db->where("bos.rtn_sr_invoice_no like ","%".$Data['search']['value']."%");
			$this->db->where("bos.rtn_grand_total like ","%".$Data['search']['value']."%");
			$this->db->or_where("sc.cust_name like ","%".$Data['search']['value']."%");
			$this->db->or_where("c.country_name like ","%".$Data['search']['value']."%");
			$this->db->or_where("s.state_name like ","%".$Data['search']['value']."%");

		}
		$this->db->join('businesslist as bus', 'bus.bus_id = bos.bus_id','left');
		$this->db->join('sales_customers as sc', 'sc.cust_id = bos.cust_id','left');
		$this->db->join('countries as c', 'c.country_id = sc.cust_billing_country' ,'left');
		$this->db->join('states as s', 's.state_id = sc.cust_billing_state','left');

		//$this->db->where("bos.reg_id",$Data['reg_id']);
		$this->db->where("bos.bus_id",$Data['bus_id']);
		$this->db->where("bos.status","Active");
		if( $c == 1)
		{
			if($Data['length']!= -1){		
			$this->db->limit($Data['length'],$Data['start']);
			}
			$query = $this->db->get();
			$result= $query->result_array();
			return $result;			
		}
		else
		{		
			$query = $this->db->get();
			$result['NumRecords']=$query->num_rows();		
			return $result;		
		}

	}
	public function billSupplyDetails($inv_id,$reg_id,$bus_id)
	{
	$this->db->select('inv.*,c.*,s.*,ci.*,bosl.*,sc.country_name as scountry,ss.state_name as sstate,sci.name as scity');
	$this->db->from('sales_invoices as inv');
	$this->db->join('countries as c', 'c.country_id = inv.inv_billing_country');
	$this->db->join('countries as sc', 'sc.country_id = inv.inv_shipping_country');
	$this->db->join('states as ss', 'ss.state_id = inv.inv_shipping_state');
	$this->db->join('states as s', 's.state_id = inv.inv_billing_state');
	$this->db->join('cities as sci', 'sci.city_id = inv.inv_shipping_city');
	$this->db->join('cities as ci', 'ci.city_id = inv.inv_billing_city');
	$this->db->join('sales_bill_supply_list as bosl', 'bosl.inv_id = inv.inv_id');
	$this->db->where('inv.inv_id',$inv_id);
	//$this->db->where('inv.reg_id',$reg_id);
	$this->db->where('inv.bus_id',$bus_id);
	$this->db->where('inv_document_type','Bill of Supply');
	$query = $this->db->get();
	return $query->result();
	}
	/*-------END BILL OF SUPPLY---------*/

	/*-------START SALES INVOICE---------*/
	public function salesInvoiceFilter($Data,$sort_field,$orderBy,$c,$e='')
	{
		$this->db->select("si.*,c.country_id,c.country_name,s.state_id,s.state_name,cus.cust_name,cus.cust_billing_country,cus.cust_billing_state,cus.cust_currency,cur.currency_id,cur.currencycode");
		$this->db->from('sales_invoices as si');
		if(!empty($Data['search']))
		{
			$this->db->group_start();
			
			$this->db->where("si.inv_invoice_no like ","%".$Data['search']."%");
			$this->db->or_where("si.inv_document_type like ","%".$Data['search']."%");
			$this->db->or_where("si.inv_invoice_date like ","%".$Data['search']."%");
			$this->db->or_where("si.inv_grant_total like ","%".$Data['search']."%");
			$this->db->or_where("si.inv_status like ","%".$Data['search']."%");
			
			$this->db->or_where("cur.currencycode like ","%".$Data['search']."%");
			if($e=='' || $e==NULL){
			$this->db->or_where("cus.cust_name like ","%".$Data['search']."%");
			$this->db->or_where("c.country_name like ","%".$Data['search']."%");
			$this->db->or_where("s.state_name like ","%".$Data['search']."%");
			$this->db->or_where("si.inv_attachment like ","%".$Data['search']."%");
			}else{
				$this->db->or_where("si.inv_equ_levy_amt like ","%".$Data['search']."%");
			}
			$this->db->group_end();
			//$this->db->or_where("doc.type like ","%".$Data['search']['value']."%");
		}
		$this->db->join('countries as c', 'c.country_id = si.inv_billing_country' ,'left');
		$this->db->join('states as s', 's.state_id = si.inv_billing_state','left');
		$this->db->join('sales_customers as cus', 'cus.cust_id = si.cust_id','left');
		$this->db->join('currency as cur', 'cur.currency_id = cus.cust_currency','left');
		//$this->db->join('document_type as doc', 'doc.id = si.inv_document_type');
		
		if($Data['search_by_document_type']!=''){
			$this->db->where("si.inv_document_type",$Data['search_by_document_type']);
		}
		if($Data['search_by_customer']!=''){
			$this->db->where("si.cust_id",$Data['search_by_customer']);
		}
		if($Data['search_by_status']!=''){
			 if($Data['search_by_status']=='CANCELLED'){
			 	$this->db->where("si.status","Inactive");
			 	$this->db->or_where("si.inv_status","Draft");
			 }else if($Data['search_by_status']=='EXCLUDING CANCELLED'){
               $this->db->where("si.status","Active");
               $this->db->where("si.inv_status !=","Draft");
			 }else{
			 	$this->db->where("si.inv_status",$Data['search_by_status']);
			 	$this->db->where("si.status","Active");
			 	
			}
			
		}else{
			 $this->db->where("si.status","Active");
			  $this->db->where("si.inv_status !=","Draft");
		}
		if($Data['inv_start_date']!=''){
			$this->db->where("si.inv_invoice_date>=",date('Y-m-d',strtotime(str_replace('/', '-',$Data['inv_start_date']))));
		}
		if($Data['inv_end_date']!=''){
			$this->db->where("si.inv_invoice_date<=",date('Y-m-d',strtotime(str_replace('/', '-',$Data['inv_end_date']))));
		}
			
			

		//$this->db->where("si.reg_id",$Data['reg_id']);
		$this->db->where("si.bus_id",$Data['bus_id']);
		if($Data['gst_id']!='0'){
		$this->db->where("si.gst_id",$Data['gst_id']);
		}
		//$this->db->where("si.status","Active");
		if($e!='' || $e!=NULL){
			$this->db->where("si.inv_equ_levy",1);
		}
		$this->db->group_by("si.inv_id");
		$this->db->order_by("".$sort_field." ".$orderBy."");

		if( $c == 1)
		{
			if($Data['length']!= -1){		
			$this->db->limit($Data['length'],$Data['start']);
			}

			$query = $this->db->get();
			//print_r($this->db->last_query());exit;
			$result= $query->result_array();
			
			return $result;			
		}
		else
		{		
			
			$query = $this->db->get();
			//print_r($this->db->last_query());exit;
			$result['NumRecords']=$query->num_rows();		
			return $result;		
		}

	}


	public function salesInvoiceFilter1($Data,$sort_field,$orderBy,$c,$e='') {

		//Sales Invoices
		$this->db->select("si.inv_id as inv_id, si.bus_id as bus_id, si.reg_id as reg_id, si.cust_id as cust_id, si.inv_document_type as inv_document_type, si.inv_invoice_no as inv_invoice_no, si.inv_invoice_date as inv_invoice_date, cust.cust_name as cust_name, si.inv_billing_state as state, si.inv_equ_levy as equa, si.inv_grant_total as inv_grant_total, si.inv_status as inv_status, si.status as status, si.inv_equ_levy_amt as inv_equ_levy_amt, si.inv_inr_value as inv_inr_value,cust.cust_code as code");

		$this->db->from('sales_invoices as si');
		$this->db->join('sales_customers as cust', 'si.cust_id = cust.cust_id');
		$this->db->join('currency as cur', 'cur.currency_id = cust.cust_currency','left');
		$this->db->where('si.bus_id', $Data['bus_id']);
		$this->db->where('si.gst_id', $Data['gst_id']);
		$this->db->where('si.status', 'Active');
		$this->db->where('si.inv_equ_levy', 1);
		$this->db->where('si.inv_equ_levy_amt >', 0);
		
		$this->db->where('si.inv_document_type', 'Sales Invoice');
		$this->db->group_by("si.inv_id");

		if(!empty($Data['search'])) {

			$this->db->group_start();
			
			$this->db->where("si.inv_invoice_no like ","%".$Data['search']."%");
			$this->db->or_where("si.inv_document_type like ","%".$Data['search']."%");
			$this->db->or_where("cust.cust_name like ","%".$Data['search']."%");
			$this->db->or_where("si.inv_invoice_date like ","%".$Data['search']."%");
			$this->db->or_where("si.inv_grant_total like ","%".$Data['search']."%");
			$this->db->or_where("si.inv_status like ","%".$Data['search']."%");
			
			$this->db->or_where("cur.currencycode like ","%".$Data['search']."%");

			if($e=='' || $e==NULL){
				$this->db->or_where("cust.cust_name like ","%".$Data['search']."%");
			}else{
				$this->db->or_where("si.inv_equ_levy_amt like ","%".$Data['search']."%");
			}
			$this->db->group_end();
		}
		
		if($Data['search_by_document_type']!=''){
			$this->db->where("si.inv_document_type",$Data['search_by_document_type']);
		}

		if($Data['search_by_customer']!=''){
			$this->db->where("cust.cust_code",$Data['search_by_customer']);
		}

		if($Data['search_by_status']!=''){
			if($Data['search_by_status']=='CANCELLED'){
			 	$this->db->where("si.status","Inactive");
			 	$this->db->or_where("si.inv_status","Draft");
			}else if($Data['search_by_status']=='EXCLUDING CANCELLED'){
               $this->db->where("si.status","Active");
               $this->db->where("si.inv_status !=","Draft");
			}else{
			 	$this->db->where("si.inv_status",$Data['search_by_status']);
			 	$this->db->where("si.status","Active");
			}
		} else {
			$this->db->where("si.status","Active");
			$this->db->where("si.inv_status !=","Draft");
		}

		if($Data['inv_start_date']!=''){
			$this->db->where("si.inv_invoice_date>=",date('Y-m-d',strtotime(str_replace('/', '-',$Data['inv_start_date']))));
		}

		if($Data['inv_end_date']!=''){
			$this->db->where("si.inv_invoice_date<=",date('Y-m-d',strtotime(str_replace('/', '-',$Data['inv_end_date']))));
		}

		$this->db->get();
    	$query1 = $this->db->last_query();

    	//Company Expense
    	$this->db->select("ce.exp_id as inv_id, ce.bus_id as bus_id, ce.reg_id as reg_id, ce.ce_vendorname as cus_id, ce.ce_category as inv_document_type, ce.ce_number as inv_invoice_no, ce.ce_date as inv_invoice_date, vend.vendor_name as cust_name, ce.ce_supplyplace as state, ce.equa_levy as equa, ce.ce_grandtotal as inv_grant_total, ce.exp_status inv_status, ce.status as status, ce.ce_eq_taxableamt as inv_equ_levy_amt, ce.ce_inr_value as inv_inr_value,vend.vendor_code as code");

		$this->db->from('company_expense as ce');
		$this->db->join('expense_vendors as vend', 'ce.ce_vendorname = vend.vendor_id');
		$this->db->where('ce.bus_id', $Data['bus_id']);
		$this->db->where('ce.gst_id', $Data['gst_id']);
		$this->db->where('ce.status', 'Active');
		$this->db->where('ce.equa_levy', 1);
		$this->db->where('ce.ce_eq_taxableamt >', 0);
		$this->db->group_by("ce.exp_id");

		if(!empty($Data['search'])) {

			$this->db->group_start();
			
			$this->db->where("ce.ce_number like ","%".$Data['search']."%");
			$this->db->or_where("ce.ce_category like ","%".$Data['search']."%");
			$this->db->or_where("vend.vendor_name like ","%".$Data['search']."%");
			$this->db->or_where("ce.ce_date like ","%".$Data['search']."%");
			$this->db->or_where("ce.ce_grandtotal like ","%".$Data['search']."%");
			$this->db->or_where("ce.exp_status like ","%".$Data['search']."%");
			
			$this->db->or_where("cur.currencycode like ","%".$Data['search']."%");
			if($e=='' || $e==NULL){
			$this->db->or_where("vend.vendor_name like ","%".$Data['search']."%");
			}else{
				$this->db->or_where("ce.ce_eq_taxableamt like ","%".$Data['search']."%");
			}
			$this->db->group_end();
		}

		$this->db->join('currency as cur', 'cur.currency_id = vend.vendor_currency','left');
		
		if($Data['search_by_document_type']!=''){
			$this->db->where("ce.ce_category",$Data['search_by_document_type']);
		}

		if($Data['search_by_customer']!=''){
			$this->db->where("vend.vendor_code",$Data['search_by_customer']);
		}
		

          if($Data['search_by_status']!=''){
			if($Data['search_by_status']=='FULL RECEIVED'){
			 	$this->db->where("ce.exp_status","Full Payment");
			 
			}else if($Data['search_by_status']=='PART RECEIVED'){
              $this->db->where("ce.exp_status","Part Payment");
			}else if($Data['search_by_status']=='Non Payable Amount'){
              $this->db->where("ce.exp_status","Part Payment");
			}else if($Data['search_by_status']=='CREDIT NOTE'){
              $this->db->where("ce.exp_status","Debit Note");
			}else{
			 	$this->db->where("ce.exp_status","Pending");
			 	
			}
		} 


		if($Data['inv_start_date']!=''){
			$this->db->where("ce.ce_date>=",date('Y-m-d',strtotime(str_replace('/', '-',$Data['inv_start_date']))));
		}

		if($Data['inv_end_date']!=''){
			$this->db->where("ce.ce_date<=",date('Y-m-d',strtotime(str_replace('/', '-',$Data['inv_end_date']))));
		}

		$this->db->get();
    	$query2 = $this->db->last_query();
             $limit = "";
    	if($c == 1) {
			if($Data['length'] != -1) {		
				$this->db->limit($Data['length'],$Data['start']);
				$limit = " LIMIT ".$Data['start'].",".$Data['length'];
			}
			else {
			$limit = "";
		}
		} 

    	$query = $this->db->query($query1." UNION ALL ".$query2 .' ORDER BY `inv_invoice_date` '. $orderBy . $limit);

    	if($c == 1) {
    		
			if($Data['length'] != -1) {		
				$this->db->limit($Data['length'],$Data['start']);
			}
			
			$result = $query->result_array();
			//print_r($this->db->last_query());exit;
			return $result;			
		} else {	
			$result= $query->result_array();
			//print_r($this->db->last_query());exit;
			$result['NumRecords'] = count($result);		
			return $result;
		}
	}

		public function getvendorORclient($bus_id) {

			$this->db->select('sc.cust_id as id, sc.bus_id as bus_id, sc.cust_name as name,sc.cust_code as code');
			$this->db->from('sales_customers as sc');
			$this->db->where('bus_id', $bus_id);
			$this->db->get();
    		$query1 = $this->db->last_query();

			$this->db->select('ev.vendor_id as id, ev.bus_id as bus_id, ev.vendor_name as name,ev.vendor_code as code');
			$this->db->from('expense_vendors as ev');
			$this->db->where('bus_id', $bus_id);
			$this->db->get();
    		$query2 = $this->db->last_query();

    		$orderBy = "ASC";

    		$query = $this->db->query($query1." UNION ALL ".$query2 .' ORDER BY `name` '. $orderBy);
    		$result= $query->result_array();
    		return $result;
		}

	/*-------START ADVANCE RECEIPTS---------*/	
	public function advanceReceipt($Data,$sort_field,$orderBy,$c)
	{
		$this->db->select("ar.*,c.country_id,c.country_name,s.state_id,s.state_name,cus.cust_name,cus.cust_billing_country,cus.cust_billing_state,cus.cust_currency,cur.currency_id,cur.currencycode");
		$this->db->from('sales_advance_receipts as ar');
		if(!empty($Data['search']['value']))
		{
			$this->db->where("ar.ar_invoice_no like ","%".$Data['search']['value']."%");
			$this->db->or_where("cus.cust_name like ","%".$Data['search']['value']."%");
			$this->db->or_where("c.country_name like ","%".$Data['search']['value']."%");
			$this->db->or_where("s.state_name like ","%".$Data['search']['value']."%");
			$this->db->or_where("ar.ar_grant_total like ","%".$Data['search']['value']."%");
			$this->db->or_where("ar.ar_status like ","%".$Data['search']['value']."%");
		}
		$this->db->join('countries as c', 'c.country_id = ar.ar_billing_country' ,'left');
		$this->db->join('states as s', 's.state_id = ar.ar_billing_state','left');
		$this->db->join('sales_customers as cus', 'cus.cust_id = ar.cust_id','left');
		$this->db->join('currency as cur', 'cur.currency_id = cus.cust_currency');
		
		/*if($Data['search_by_customer']!=''){
			$this->db->where("ar.cust_id",$Data['search_by_customer']);
		}
		if($Data['search_by_status']!=''){
			$this->db->where("ar.ar_status",$Data['search_by_status']);
		}*/

		//$this->db->where("ar.reg_id",$Data['reg_id']);
		$this->db->where("ar.bus_id",$Data['bus_id']);
		$this->db->where("ar.status","Active");
		
		$this->db->group_by("ar.ar_id");
		$this->db->order_by("".$sort_field." ".$orderBy."");

		if( $c == 1)
		{
			if($Data['length']!= -1){		
			$this->db->limit($Data['length'],$Data['start']);
			}
			$query = $this->db->get();
			//print_r($this->db->last_query());exit;
			$result= $query->result_array();
			
			return $result;			
		}
		else
		{		
			
			$query = $this->db->get();
			$result['NumRecords']=$query->num_rows();		
			return $result;		
		}
	}
	public function advance_ReceiptDetails($inv_id,$reg_id,$bus_id)
	{
	$this->db->select('inv.*,c.*,s.*,ci.*,arl.*,sc.country_name as scountry,ss.state_name as sstate,sci.name as scity');
	$this->db->from('sales_invoices as inv');
	$this->db->join('countries as c', 'c.country_id = inv.inv_billing_country');
	$this->db->join('countries as sc', 'sc.country_id = inv.inv_shipping_country');
	$this->db->join('states as ss', 'ss.state_id = inv.inv_shipping_state');
	$this->db->join('states as s', 's.state_id = inv.inv_billing_state');
	$this->db->join('cities as sci', 'sci.city_id = inv.inv_shipping_city');
	$this->db->join('cities as ci', 'ci.city_id = inv.inv_billing_city');
	$this->db->join('sales_advance_receipts_list as arl', 'arl.inv_id = inv.inv_id');
	$this->db->where('inv.inv_id',$inv_id);
	//$this->db->where('inv.reg_id',$reg_id);
	$this->db->where('inv.bus_id',$bus_id);
	$query = $this->db->get();
	return $query->result();
	}


	public function salesInvoiceDetails($sub_table,$id,$reg_id,$bus_id,$gst_id='',$cust_id='')
	{
	
	$this->db->select('inv.*,c.*,s.*,ci.*,sil.*,sc.country_name as scountry,ss.state_name as sstate,sci.name as scity,cus.cust_name,cbank.cbank_id,cbank.cbank_name,cbranch.cbank_id,cbranch.cbank_branch_name');
	$this->db->from('sales_invoices as inv');
	$this->db->join('countries as c', 'c.country_id = inv.inv_billing_country','left');
	$this->db->join('countries as sc', 'sc.country_id = inv.inv_shipping_country','left');
	$this->db->join('states as ss', 'ss.state_id = inv.inv_shipping_state','left');
	$this->db->join('states as s', 's.state_id = inv.inv_billing_state','left');
	$this->db->join('cities as sci', 'sci.city_id = inv.inv_shipping_city','left');
	$this->db->join('cities as ci', 'ci.city_id = inv.inv_billing_city','left');
	$this->db->join('sales_customers as cus', 'cus.cust_id = inv.cust_id','left');
	$this->db->join('company_bank as cbank', 'cbank.cbank_id = inv.inv_bank_name','left');
	$this->db->join('company_bank as cbranch', 'cbranch.cbank_id = inv.inv_bank_branch_no','left');
	//$this->db->join('document_type as doc', 'doc.id = inv.inv_document_type');
	$this->db->join($sub_table.' as sil', 'sil.inv_id = inv.inv_id','left');
	//$this->db->where('inv.inv_id',$id);
	if(is_array($id)){
		$this->db->where_in("inv.inv_id",$id);
		} else {
		$this->db->where("inv.inv_id",$id);	
		}
	//$this->db->where('inv.reg_id',$reg_id);
	$this->db->where('inv.bus_id',$bus_id);
	$this->db->where('inv.gst_id',$gst_id);

	


	//if($cust_id!=''){
	//$this->db->where('inv.cust_id',$cust_id);
	//}
	//print $this->db->last_query();
	$query = $this->db->get();

	return $query->result();
	}

	public function export_credit_note_all($array,$reg_id,$bus_id,$singal=''){

		$this->db->select("cd.*,c.country_id,c.country_name,s.state_id,s.state_name,cus.cust_name,cus.cust_billing_country,cus.cust_billing_state,cus.cust_currency,cur.currency_id,cur.currencycode");
		$this->db->from('sales_credit_debit as cd');
		$this->db->join('countries as c', 'c.country_id = cd.cd_billing_country' ,'left');
		$this->db->join('states as s', 's.state_id = cd.cd_billing_state','left');
		$this->db->join('sales_customers as cus', 'cus.cust_id = cd.cust_id','left');
		$this->db->join('currency as cur', 'cur.currency_id = cus.cust_currency','left');
		//$this->db->where("cd.reg_id",$reg_id);
		$this->db->where("cd.bus_id",$bus_id);
		$this->db->where("cd.status","Active");
		if($singal==1){
			$this->db->where("cd.cd_id",$array);
		}else {
			$this->db->where_in("cd.cd_id",$array);
		}
		$query = $this->db->get();
		$result= $query->result_array();
		return $result;	
	}

	public function export_all_invoice($array,$reg_id,$bus_id,$gst_id){
		$this->db->select("si.*,c.country_id,c.country_name,s.state_id,s.state_name,cus.cust_name,cus.cust_billing_country,cus.cust_billing_state,cus.cust_currency,cur.currency_id,cur.currencycode");
		$this->db->from('sales_invoices as si');
		$this->db->join('countries as c', 'c.country_id = si.inv_billing_country' ,'left');
		$this->db->join('states as s', 's.state_id = si.inv_billing_state','left');
		$this->db->join('sales_customers as cus', 'cus.cust_id = si.cust_id','left');
		$this->db->join('currency as cur', 'cur.currency_id = cus.cust_currency','left');
		//$this->db->where("si.reg_id",$reg_id);
		$this->db->where("si.bus_id",$bus_id);
		$this->db->where("si.gst_id",$gst_id);
		$this->db->where_in("si.inv_id",$array);
		$query = $this->db->get();
			//print_r($this->db->last_query());exit;
		$result= $query->result_array();
			
		return $result;			
	}

	public function download_equalisation_levy($id,$reg_id,$bus_id,$gst_id)
	{
		$this->db->select("si.*,c.country_id,c.country_name,s.state_id,s.state_name,cus.cust_name,cus.cust_billing_country,cus.cust_billing_state,cus.cust_currency,cur.currency_id,cur.currencycode,sl.invl_taxable_amt");
		$this->db->from('sales_invoices as si');
		$this->db->join('countries as c', 'c.country_id = si.inv_billing_country' ,'left');
		$this->db->join('states as s', 's.state_id = si.inv_billing_state','left');
		$this->db->join('sales_customers as cus', 'cus.cust_id = si.cust_id','left');
		$this->db->join('currency as cur', 'cur.currency_id = cus.cust_currency','left');
		$this->db->join('sales_invoices_list as sl', 'sl.inv_id = si.inv_id' ,'left');
		//$this->db->join('document_type as doc', 'doc.id = si.inv_document_type');
		//$this->db->where("si.reg_id",$reg_id);
		$this->db->where("sl.invl_service_type",3);
		$this->db->where("si.bus_id",$bus_id);
		$this->db->where("si.gst_id",$gst_id);
		$this->db->where("si.inv_id",$id);
		$this->db->where("si.status","Active");
		$this->db->where("si.inv_equ_levy",1);
		$this->db->group_by("si.inv_id");
		$query = $this->db->get();
		//print_r($this->db->last_query());exit;
		$result= $query->result_array();
		return $result;			
	}

	public function download_multiple_equalisation($array,$reg_id,$bus_id,$gst_id,$singal='')
	{
		$this->db->select("si.*,c.country_id,c.country_name,s.state_id,s.state_name,cus.cust_name,cus.cust_billing_country,cus.cust_billing_state,cus.cust_currency,cur.currency_id,cur.currencycode,sl.invl_taxable_amt");
		$this->db->from('sales_invoices as si');
		$this->db->join('countries as c', 'c.country_id = si.inv_billing_country' ,'left');
		$this->db->join('states as s', 's.state_id = si.inv_billing_state','left');
		$this->db->join('sales_customers as cus', 'cus.cust_id = si.cust_id','left');
		$this->db->join('currency as cur', 'cur.currency_id = cus.cust_currency','left');
		$this->db->join('sales_invoices_list as sl', 'sl.inv_id = si.inv_id' ,'left');
		//$this->db->join('document_type as doc', 'doc.id = si.inv_document_type');
		//$this->db->where("si.reg_id",$reg_id);
		// $this->db->where("si.bus_id",$bus_id);
		// $this->db->where("si.gst_id",$gst_id);
		$this->db->where("sl.invl_service_type",3);
		if($singal==1){
			$this->db->where("si.inv_id",$array);
		}else{
			$this->db->where_in("si.inv_id",$array);
		}
		$this->db->where("si.status","Active");
		$this->db->where("si.inv_equ_levy",1);
		$this->db->group_by("si.inv_id");
		$query = $this->db->get();
		//print_r($this->db->last_query());exit;
		$result= $query->result_array();
		return $result;			
	}

	public function getSalesReceiptAmt($inv_id){
   
         $this->db->select("*");
		$this->db->from('sales_receipts_list as srl');
		$this->db->join('sales_receipts as sr', 'srl.srec_id = sr.srec_id');
		
		$this->db->where("srl.srecl_inv_no",$inv_id);
		$query = $this->db->get();
		//print_r($this->db->last_query());exit;
		$result= $query->result_array();
		//$sr_amt=0;
		   //foreach($result as $key=>$value){
             // $sr_amt+=$value['srec_amount'];
		  // }
		   //print $sr_amt;exit;
		return $result;			
		
	}

	public function getSalesReceiptAttach($inv_id){
   
         $this->db->select("*");
		$this->db->from('sales_receipts_list as srl');
		$this->db->join('sales_receipts as sr', 'srl.srec_id = sr.srec_id');
		$this->db->join('srf_document as srf', 'sr.srec_id = srf.parent_id');
		
		$this->db->where("srl.srecl_inv_no",$inv_id);
		$query = $this->db->get();
		//print_r($this->db->last_query());exit;
		$result= $query->result_array();
		//$sr_amt=0;
		   //foreach($result as $key=>$value){
             // $sr_amt+=$value['srec_amount'];
		  // }
		   //print $sr_amt;exit;
		return $result;			
		
	}

	public function getCreditNoteAmt($inv_no,$cust_id){
   
         $this->db->select("*");
		$this->db->from('sales_credit_debit as cd');
		$this->db->join('sales_credit_debit_list as cdl', 'cd.cd_id = cdl.cd_id' ,'left');
		
		$this->db->where("cd.cd_invoice_no",$inv_no);
		$this->db->where("cd.cust_id",$cust_id);
		$this->db->where("cd.status",'Active');
		$query = $this->db->get();
		//print_r($this->db->last_query());
		$result= $query->result_array();
		$cd_amt=0;
		   foreach($result as $key=>$value){
              $cd_amt+=$value['cdl_amount']*$value['cdn_inr_value'];
		   }
		   //print $sr_amt;exit;
		return $cd_amt;			
		
	}

	public function getCreditNoteAmtSR($inv_no,$cust_id){
   
         $this->db->select("*");
		$this->db->from('sales_credit_debit as cd');
		$this->db->join('sales_credit_debit_list as cdl', 'cd.cd_id = cdl.cd_id' ,'left');
		
		$this->db->where("cd.cd_invoice_no",$inv_no);
		$this->db->where("cd.cust_id",$cust_id);
		$this->db->where("cd.status",'Active');
		$query = $this->db->get();
		//print_r($this->db->last_query());
		$result= $query->result_array();
		$note=array();
		if(count($result)>0){
		$cd_amt=0;
		
		   foreach($result as $key=>$value){
              $cd_amt+=$value['cdl_amount'];
              $cdn_value=0;
		   }
		   //print $sr_amt;exit;
		   $note['cr_amt']= $cd_amt;
		   $note['cr_inr_val']= $result[0]['cdn_inr_value'];
		}
		return $note;			
		
	}



	public function sales_expense_export_all($array,$bus_id){

		// $this->db->select("ev.*");
		// $this->db->from('sales_expense_voucher as ev');
		// $this->db->where_in("ev.ev_id",$array);
			


		$this->db->select("exp_voc.*,evl.*,sc.cust_id,sc.cust_name,c.country_id,c.country_name,s.state_id,s.state_name,evl.evl_total_amt,evi.evi_id,count(evi.evi_ev_id) as ev_id_count");
		$this->db->from('sales_expense_voucher as exp_voc');
		$this->db->join('sales_expense_voucher_list as evl', 'evl.ev_id = exp_voc.ev_id' ,'left');
		$this->db->join('businesslist as bus', 'bus.bus_id = exp_voc.bus_id','left');
		$this->db->join('sales_customers as sc', 'sc.cust_id = exp_voc.cust_id','left');
		$this->db->join('sales_expense_voucher_image as evi', 'evi.evi_ev_id = exp_voc.ev_id' ,'left');
		$this->db->join('countries as c', 'c.country_id = sc.cust_billing_country' ,'left');
		$this->db->join('states as s', 's.state_id = sc.cust_billing_state','left');

		//$this->db->where("exp_voc.reg_id",$Data['reg_id']);
		$this->db->where("exp_voc.bus_id",$bus_id);
		$this->db->where_in("exp_voc.ev_id",$array);
		$this->db->where("exp_voc.status","Active");
		$this->db->group_by("exp_voc.ev_id");
		$query = $this->db->get();	
		$result= $query->result_array();			
		return $result;		
	}


	public function fetch_invoices($exp_id) {

		$this->db->select('sil.inv_id, sil.invl_expense_voucher, si.inv_invoice_no_view, si.inv_status');

		$this->db->from('sales_invoices_list as sil');

		$this->db->join('sales_invoices as si', 'si.inv_id = sil.inv_id', 'left');

		$this->db->where('sil.invl_expense_voucher', $exp_id);

		$query = $this->db->get();
		//print $this->db->last_query(); exit();
		$result = $query->result_array();

		return $result;
	}

}
?>
