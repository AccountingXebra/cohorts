<?php
class Subscription_model extends CI_Model{
		
	public function  __construct(){
		parent::__construct();
		$this->load->database();
	
	}


	public function subscriptionFilter($Data,$sort_field,$orderBy,$c)
	{
		//print_r($Data);exit;
		$this->db->select("sub.*");
		$this->db->from('subscription as sub');

		if(!empty($Data['search']))
		{
			$this->db->group_start();
			$this->db->where("sub.name like ","%".$Data['search']."%");
			$this->db->or_where("sub.amount like ","%".$Data['search']."%");
			$this->db->or_where("sub.invoice_no like","%".$Data['search']."%");
			$this->db->or_where("sub.payment_status like ","%".$Data['search']."%");
			$this->db->or_where("sub.subscription_plan like ","%".$Data['search']."%");
			$this->db->or_where("sub.email like ","%".$Data['search']."%");
			$this->db->group_end();
		}
		//$this->db->join('services_other_taxes as tax', 'tax.service_id = ser.service_id' ,'left');
		if(@$Data['sub_start_date']!=''){
			$this->db->where("sub.createdat >= ",date('Y-m-d',strtotime(str_replace('/', '-',$Data['sub_start_date']))));
		}
		if(@$Data['sub_end_date']!=''){
			$this->db->where("CAST(sub.createdat as date) <= ",date('Y-m-d',strtotime(str_replace('/', '-',$Data['sub_end_date']))));
		}
		$this->db->where("sub.bus_id",$Data['bus_id']);
		
		$this->db->group_by("sub.subscription_id");
		$this->db->order_by("".$sort_field." ".$orderBy."");

		if( $c == 1)
		{
			if($Data['length']!= -1){		
			$this->db->limit($Data['length'],$Data['start']);
			}
			$query = $this->db->get();
			//print_r($this->db->last_query());exit;
			$result= $query->result_array();
			
			return $result;			
		}
		else
		{		
			
			$query = $this->db->get();
			$result['NumRecords']=$query->num_rows();		
			return $result;		
		}

	}


	public function GetSubscritionData($id,$bus_id)
	{
		$this->db->select('subscription.*,businesslist.bus_company_name');
	    $this->db->from('subscription');
	    $this->db->join('businesslist', 'subscription.bus_id = businesslist.bus_id'); 
	    $this->db->where('subscription.subscription_id',$id);
	    $this->db->where('subscription.bus_id',$bus_id);
	    $query = $this->db->get();
	    $userData = $query->result();
	    // print_r($userData);
	    // exit;
	    // $userData = json_decode($userData, true);
	    foreach ($userData as $key => $value) {

			$value->createdat = date('d M, Y',strtotime($value->createdat)); 
			$value->validity = date('d M, Y', strtotime("+".$value->validity." day", strtotime($value->createdat)));
			$value->userSpace= formatSizeUnits($this->usedSpace($value->bus_id));
			$value->storage = $value->storage."GB / ".$value->userSpace; 
			if($value->payment_status == "Completed")
			{
				$value->payment_status = "Paid";
			}else{
				$value->payment_status = "Unpaid";
			}
		}

		return $userData;
	}

	public function export_all($array){

		$this->db->select('subscription.*,businesslist.bus_company_name');
	    $this->db->from('subscription');
	    $this->db->join('businesslist', 'subscription.bus_id = businesslist.bus_id'); 
	    $this->db->where_in('subscription.subscription_id',$array);
	    $query = $this->db->get();
	    $userData = $query->result();

	    foreach ($userData as $key => $value) {

			$value->createdat = date('d M, Y',strtotime($value->createdat)); 
			$value->validity = date('d M, Y', strtotime("+".$value->validity." day", strtotime($value->createdat)));
			$value->userSpace= formatSizeUnits($this->usedSpace($value->bus_id));
			$value->storage = $value->storage."GB / ".$value->userSpace; 
			if($value->payment_status == "Completed")
			{
				$value->payment_status = "Paid";
			}else{
				$value->payment_status = "Unpaid";
			}
		}

		return $userData;

	}

	public function usedSpace($bus_id)
	{
		//return "30MB";
		$this->db->select("SUM(legal_document_size)");
		$this->db->from('legal_document');
		$this->db->where("bus_id",$bus_id);
		$query = $this->db->get();
		$result= $query->result_array();	

		$this->db->select("SUM(other_doc_size)");
		$this->db->from('other_document');
		$this->db->where("bus_id",$bus_id);
		$query = $this->db->get();
		$result1= $query->result_array();	

		return $result[0]['SUM(legal_document_size)'] + $result1[0]['SUM(other_doc_size)'];
	}


/*
	| -------------------------------------------------------------------
	| check unique fields
	| -------------------------------------------------------------------
	|
	*/
	public function isUnique($table, $field, $value,$id='')
	{
		$this->db->select('*');
		$this->db->from($table);
		$this->db->where($field,$value);
		if($id!='')
		{
			$this->db->where("id != ",$id);
		}
		$query = $this->db->get();
		// echo "<pre>";
		// print_r($query);exit();
		$data = $query->num_rows();
		return ($data > 0)?FALSE:TRUE;
	}

		/*
	| -------------------------------------------------------------------
	| Insert data
	| -------------------------------------------------------------------
	|
	| general function to insert data in table
	|
	*/
	public function insertData($table, $data)
	{
		
		$result = $this->db->insert($table, $data);

		if($result == 1){

			$id=$this->db->insert_id();
			
			return  $id;

		}else{
			return false;
		}
	}
	
	public function insertData_batch($table, $data)
	{
		
		$this->db->insert_batch($table, $data);
		return true;
	}
	
	
	/*
	| -------------------------------------------------------------------
	| Update data
	| -------------------------------------------------------------------
	|
	| general function to update data
	|
	*/

	public function updateData($table, $data, $where)
	{

		$this->db->where($where);
		if($this->db->update($table, $data)){

			return 1;
		}else{
			return 0;
		}
	}
	
	public function searchItemData($keyword,$where){
			$this->db->select('service_name');
			$this->db->from('services');
			$this->db->where($where);
			
			$this->db->like('service_name', $keyword, 'after');
			$query = $this->db->get();
			return $query->result();

	}
	
	/*
	| -------------------------------------------------------------------
	| Select data
	| -------------------------------------------------------------------
	|
	| general function to get result by passing nesessary parameters
	|
	*/
	public function selectData($table, $fields='*', $where='', $order_by="", $order_type="", $group_by="", $limit="", $rows="", $type='')
	{
		$this->db->select($fields);
		$this->db->from($table);
		if ($where != "") {
			$this->db->where($where);
		}

		if ($order_by != '') {
			$this->db->order_by($order_by,$order_type);
		}

		if ($group_by != '') {
			$this->db->group_by($group_by);
		}

		if ($limit > 0 && $rows == "") {
			$this->db->limit($limit);
		}
		if ($rows > 0) {
			$this->db->limit($rows, $limit);
		}


		$query = $this->db->get();

		if ($type == "rowcount") {
			$data = $query->num_rows();
		}else{
			$data = $query->result();
		}

		#echo "<pre>"; print_r($this->db->queries); exit;
		$query->result();

		return $data;
	}

		/*
	| -------------------------------------------------------------------
	| Role List
	| -------------------------------------------------------------------
	|
	| general function to delete the records
	|
	*/
	
	
	public function selectRoleData($table){
		$query = $this->db->get($table);
		if($query!=NULL){
			return $query->result();
		}
		else{
			return false;
		}
	}

	
		/*
	| -------------------------------------------------------------------
	| Delete data
	| -------------------------------------------------------------------
	|
	| general function to delete the records
	|
	*/
	
	public function deleteData($table, $data)
	{
		if($this->db->delete($table, $data)){

			return 1;
		}else{
			return 0;
		}
	}

	// public function servicesFilter($Data,$sort_field,$orderBy,$c)
	// {
	// 	$this->db->select("ser.*");
	// 	$this->db->from('services as ser');

	// 	if(!empty($Data['search']))
	// 	{
	// 		$this->db->group_start();
	// 		$this->db->where("ser.service_code like ","%".$Data['search']."%");
	// 		$this->db->or_where("ser.service_name like ","%".$Data['search']."%");
	// 		$this->db->or_where("ser.service_hsn_no like ","%".$Data['search']."%");
	// 		$this->db->or_where("ser.service_total_alert like ","%".$Data['search']."%");
	// 		$this->db->or_where("ser.service_gst like ","%".$Data['search']."%");
	// 		$this->db->group_end();
	// 		if($this->user_session['user_business']==2){
	// 		$this->db->or_where("ser.service_quantity like ","%".$Data['search']['value']."%");
	// 		$this->db->or_where("ser.purchase_qty like ","%".$Data['search']['value']."%");
	// 		$this->db->or_where("ser.sales_qty like ","%".$Data['search']['value']."%");
	// 		$this->db->or_where("ser.closing_qty like ","%".$Data['search']['value']."%");
	// 		$this->db->or_where("ser.service_value like ","%".$Data['search']['value']."%");
	// 		$this->db->or_where("ser.purchase_value like ","%".$Data['search']['value']."%");
	// 		$this->db->or_where("ser.sales_value like ","%".$Data['search']['value']."%");
	// 		$this->db->or_where("ser.closing_value like ","%".$Data['search']['value']."%");
	// 		}
	// 	}
	// 	//$this->db->join('services_other_taxes as tax', 'tax.service_id = ser.service_id' ,'left');
	// 	if($Data['ser_start_date']!=''){
	// 		$this->db->where("ser.service_date>=",date('Y-m-d',strtotime(str_replace('/', '-',$Data['ser_start_date']))));
	// 	}
	// 	if($Data['ser_end_date']!=''){
	// 		$this->db->where("ser.service_date<=",date('Y-m-d',strtotime(str_replace('/', '-',$Data['ser_end_date']))));
	// 	}
	// 	$this->db->where("ser.reg_id",$Data['reg_id']);
	// 	$this->db->where("ser.bus_id",$Data['bus_id']);
		
	// 	$this->db->group_by("ser.service_id");
	// 	$this->db->order_by("".$sort_field." ".$orderBy."");

	// 	if( $c == 1)
	// 	{
	// 		if($Data['length']!= -1){		
	// 		$this->db->limit($Data['length'],$Data['start']);
	// 		}
	// 		$query = $this->db->get();
	// 		//print_r($this->db->last_query());exit;
	// 		$result= $query->result_array();
			
	// 		return $result;			
	// 	}
	// 	else
	// 	{		
			
	// 		$query = $this->db->get();
	// 		$result['NumRecords']=$query->num_rows();		
	// 		return $result;		
	// 	}

	// }

	

	

}
?>