<?php
class Settings_model extends CI_Model{
		
	public function  __construct(){
		parent::__construct();
		$this->load->database();
	
	}


/*
	| -------------------------------------------------------------------
	| check unique fields
	| -------------------------------------------------------------------
	|
	*/
	public function isUnique($table, $field, $value,$id='')
	{
		$this->db->select('*');
		$this->db->from($table);
		$this->db->where($field,$value);
		if($id!='')
		{
			$this->db->where("id != ",$id);
		}
		$query = $this->db->get();
		// echo "<pre>";
		// print_r($query);exit();
		$data = $query->num_rows();
		return ($data > 0)?FALSE:TRUE;
	}

		/*
	| -------------------------------------------------------------------
	| Insert data
	| -------------------------------------------------------------------
	|
	| general function to insert data in table
	|
	*/

	public function insertData($table, $data)
	{
		
		$result = $this->db->insert($table, $data);

		if($result == 1){

			$id=$this->db->insert_id();
			
			return  $id;

		}else{
			return false;
		}
	}

	public function insertData_batch($table, $data)
	{
		
		$this->db->insert_batch($table, $data);
		return $this->db->insert_id();
	}
	
	
	/*
	| -------------------------------------------------------------------
	| Update data
	| -------------------------------------------------------------------
	|
	| general function to update data
	|
	*/
	
	public function updateData($table, $data, $where)
	{

		$this->db->where($where);
		if($this->db->update($table, $data)){

			return 1;
		}else{
			return 0;
		}
	}

	public function updateBatch($table, $data, $where)
	{
		
		if($this->db->update_batch($table, $data,$where)){
			return 1;
		}else{
			return 0;
		}
	}
	
	
	function get_single($table, $filter)
	{
		$data = $this->db->get_where($table, $filter);
		if($data->num_rows() > 0)
		{
			$data = $data->first_row("array");
			return $data;
		}
		else
		{
			return false;
		}
	}
	/*
	| -------------------------------------------------------------------
	| Select data
	| -------------------------------------------------------------------
	|
	| general function to get result by passing nesessary parameters
	|
	*/
	public function selectData($table, $fields='*', $where='', $order_by="", $order_type="", $group_by="", $limit="", $rows="", $type='')
	{
		$this->db->select($fields);
		$this->db->from($table);
		if ($where != "") {
			$this->db->where($where);
		}

		if ($order_by != '') {
			$this->db->order_by($order_by,$order_type);
		}

		if ($group_by != '') {
			$this->db->group_by($group_by);
		}

		if ($limit > 0 && $rows == "") {
			$this->db->limit($limit);
		}
		if ($rows > 0) {
			$this->db->limit($rows, $limit);
		}


		$query = $this->db->get();

		if ($type == "rowcount") {
			$data = $query->num_rows();
		}else{
			$data = $query->result();
		}

		#echo "<pre>"; print_r($this->db->queries); exit;
		$query->result();

		return $data;
	}

		/*
	| -------------------------------------------------------------------
	| Role List
	| -------------------------------------------------------------------
	|
	| general function to delete the records
	|
	*/

	public function deleteData($table, $data)
		{
			if($this->db->delete($table, $data)){

				return 1;
			}else{
				return 0;
			}
		}
	
	
	public function selectRoleData($table){
		$query = $this->db->get($table);
		if($query!=NULL){
			return $query->result();
		}
		else{
			return false;
		}
	}

	public function unioncusven($bus_id) {

		$this->db->select('sc.cust_id as id, sc.cust_code as code, sc.cust_name as name');
		$this->db->from('sales_customers as sc');
		$this->db->where('bus_id', $bus_id);
		$this->db->group_by('sc.cust_id');
		$this->db->get();
		$query1 = $this->db->last_query();

		$this->db->select('ev.vendor_id as id, ev.vendor_code as code, ev.vendor_name as name');
		$this->db->from('expense_vendors as ev');
		$this->db->where('bus_id', $bus_id);
		$this->db->group_by('ev.vendor_id');
		$this->db->get();
		$query2 = $this->db->last_query();

		$query = $this->db->query($query1." UNION ALL ".$query2);
		$result = $query->result_array();
		return $result;	
	}

	public function myAlertsFilter($Data,$sort_field,$orderBy,$c)
	{
		$this->db->select("alt.*");
		$this->db->from('my_alerts as alt');

		if(!empty($Data['search']))
		{
			$this->db->group_start();
			$this->db->where("alt.alert_number like ","%".$Data['search']."%");
			$this->db->or_where("alt.alert_date like ","%".$Data['search']."%");
			$this->db->or_where("alt.alert_reminder like ","%".$Data['search']."%");
			//$this->db->or_where("cus.cust_name like ","%".$Data['search']."%");
			$this->db->or_where("alt.alert_condition like ","%".$Data['search']."%");
			$this->db->or_where("alt.alert_target like ","%".$Data['search']."%");
			$this->db->group_end();

		}
		//echo '<pre>';print_r($Data);echo '</pre>'; exit();
		//$this->db->join('sales_customers as cus', 'cus.cust_id = alt.parent_id' ,'left');
		//if($Data['calerts_customer']!=''){
		//	$this->db->where("alt.parent_id",$Data['calerts_customer']);
		//}
		if($Data['calerts_start_date']!=''){
			$this->db->where("alt.alert_date>=",date('Y-m-d',strtotime(str_replace('/', '-',$Data['calerts_start_date']))));
		}
		if($Data['calerts_end_date']!=''){
			$this->db->where("alt.alert_date<=",date('Y-m-d',strtotime(str_replace('/', '-',$Data['calerts_end_date']))));
		}
		$this->db->where("alt.bus_id",$Data['bus_id']);
		$this->db->where_in("alt.alert_type",$Data['alert_type']);
		$this->db->where("alt.status","Active");

		$this->db->group_by("alt.id");
		$this->db->order_by("".$sort_field." ".$orderBy."");

		if( $c == 1)
		{
			if($Data['length']!= -1){		
			$this->db->limit($Data['length'],$Data['start']);
			}
			$query = $this->db->get();
			//print_r($this->db->last_query());exit;
			$result= $query->result_array();
			
			return $result;			
		}
		else
		{		
			
			$query = $this->db->get();
			$result['NumRecords']=$query->num_rows();		
			return $result;		
		}

	}
	public function join_Alrts($id,$bus_id,$alert_type,$sub_table,$field)  //$alert_type
	{
		if($sub_table==''){
		$this->db->select('alt.*,alt.status as alt_status');
		}else{
		$this->db->select('alt.*,alt.status as alt_status');
		}
		$this->db->from('my_alerts as alt');
		if($sub_table!=''){
		$this->db->join($sub_table.' as sub', 'sub.'.$field.' = alt.parent_id' ,'left');
		}
		$this->db->where('alt.id',$id);
		$this->db->where('alt.alert_type',$alert_type);
		$this->db->where('alt.bus_id',$bus_id);
		$this->db->where('alt.status',"Active");
		$this->db->group_by("alt.id");
		$query = $this->db->get();
	    
	return $query->result();
	}

	public function itemAlertsFilter($Data,$sort_field,$orderBy,$c)
	{
		$this->db->select("alt.*");
		$this->db->from('my_alerts as alt');

		if(!empty($Data['search']))
		{
			$this->db->group_start();
			$this->db->where("alt.alert_number like ","%".$Data['search']."%");
			$this->db->or_where("alt.alert_date like ","%".$Data['search']."%");
			$this->db->or_where("alt.alert_type like ","%".$Data['search']."%");
			$this->db->or_where("alt.alert_condition like ","%".$Data['search']."%");
			$this->db->or_where("alt.alert_target like ","%".$Data['search']."%");
			$this->db->or_where("alt.alert_reminder like ","%".$Data['search']."%");
			$this->db->group_end();
		}
		//$this->db->join('services as itm', 'itm.service_id = alt.parent_id' ,'left');
		if($Data['ialerts_customer']!=''){
			$this->db->where("alt.parent_id",$Data['ialerts_customer']);
		}
		if($Data['ialerts_start_date']!=''){
			$this->db->where("alt.alert_date>=",date('Y-m-d',strtotime(str_replace('/', '-',$Data['ialerts_start_date']))));
		}
		if($Data['ialerts_end_date']!=''){
			$this->db->where("alt.alert_date<=",date('Y-m-d',strtotime(str_replace('/', '-',$Data['ialerts_end_date']))));
		}
		$this->db->where("alt.bus_id",$Data['bus_id']);
		$this->db->where_in("alt.alert_type",$Data['alert_type']);
		$this->db->where("alt.status","Active");
		
		$this->db->group_by("alt.id");
		$this->db->order_by("".$sort_field." ".$orderBy."");

		if( $c == 1)
		{
			if($Data['length']!= -1){		
			$this->db->limit($Data['length'],$Data['start']);
			}
			$query = $this->db->get();
			//print_r($this->db->last_query());exit;
			$result= $query->result_array();
			
			return $result;			
		}
		else
		{		
			
			$query = $this->db->get();
			$result['NumRecords']=$query->num_rows();		
			return $result;		
		}

	}
	public function jvAlertsFilter($Data,$sort_field,$orderBy,$c)
	{
		$this->db->select("alt.*");
		$this->db->from('my_alerts as alt');

		if(!empty($Data['search']))
		{
			$this->db->group_start();
			$this->db->where("alt.alert_number like ","%".$Data['search']."%");
			$this->db->or_where("alt.alert_date like ","%".$Data['search']."%");
			$this->db->or_where("alt.alert_reminder like ","%".$Data['search']."%");
			$this->db->group_end();

		}
		if($Data['jalerts_customer']!=''){
			$this->db->where("alt.parent_id",$Data['jalerts_customer']);
		}
		if($Data['jalerts_start_date']!=''){
			$this->db->where("alt.alert_date>=",date('Y-m-d',strtotime(str_replace('/', '-',$Data['jalerts_start_date']))));
		}
		if($Data['jalerts_end_date']!=''){
			$this->db->where("alt.alert_date<=",date('Y-m-d',strtotime(str_replace('/', '-',$Data['jalerts_end_date']))));
		}
		$this->db->where("alt.bus_id",$Data['bus_id']);
		$this->db->where("alt.alert_type",$Data['alert_type']);
		$this->db->where("alt.status","Active");
		
		$this->db->group_by("alt.id");
		$this->db->order_by("".$sort_field." ".$orderBy."");

		if( $c == 1)
		{
			if($Data['length']!= -1){		
			$this->db->limit($Data['length'],$Data['start']);
			}
			$query = $this->db->get();
			//print_r($this->db->last_query());exit;
			$result= $query->result_array();
			
			return $result;			
		}
		else
		{		
			
			$query = $this->db->get();
			$result['NumRecords']=$query->num_rows();		
			return $result;		
		}

	}

	public function myexpAlertsFilter($Data,$sort_field,$orderBy,$c)
	{
		$this->db->select("alt.*,cus.exp_id,cus.exp_name");
		$this->db->from('my_alerts as alt');
		//$this->db->from('expense_exps as alt');

		if(!empty($Data['search']))
		{
			$this->db->group_start();
			$this->db->where("alt.alert_number like ","%".$Data['search']."%");
			$this->db->or_where("alt.alert_date like ","%".$Data['search']."%");
			//$this->db->or_where("alt.vendor_name like ","%".$Data['search']."%");
			$this->db->or_where("alt.alert_reminder like ","%".$Data['search']."%");
			$this->db->or_where("cus.cust_name like ","%".$Data['search']."%");
			$this->db->or_where("alt.alert_condition like ","%".$Data['search']."%");
			$this->db->or_where("alt.alert_target like ","%".$Data['search']."%");
			$this->db->group_end();

		}
		$this->db->join('expense_list as cus', 'cus.exp_id = alt.parent_id' ,'left');
		if($Data['ealerts_customer']!=''){
			$this->db->where("alt.parent_id",$Data['ealerts_customer']);
		}
		if($Data['ealerts_start_date']!=''){
			$this->db->where("alt.alert_date>=",date('Y-m-d',strtotime(str_replace('/', '-',$Data['ealerts_start_date']))));
		}
		if($Data['ealerts_end_date']!=''){
			$this->db->where("alt.alert_date<=",date('Y-m-d',strtotime(str_replace('/', '-',$Data['ealerts_end_date']))));
		}
		$this->db->where("alt.bus_id",$Data['bus_id']);
		$this->db->where("alt.alert_type","exp");
		$this->db->where("alt.status","Active");
		
		$this->db->group_by("alt.id");
		$this->db->order_by("".$sort_field." ".$orderBy."");

		if( $c == 1)
		{
			if($Data['length']!= -1){		
			$this->db->limit($Data['length'],$Data['start']);
			}
			$query = $this->db->get();
			//print_r($this->db->last_query());exit;
			$result= $query->result_array();
			
			return $result;			
		}
		else
		{		
			
			$query = $this->db->get();
			$result['NumRecords']=$query->num_rows();		
			return $result;		
		}

	}

	public function myvAlertsFilter($Data,$sort_field,$orderBy,$c)
	{
		$this->db->select("alt.*,cus.vendor_id,cus.vendor_name");
		$this->db->from('my_alerts as alt');
		//$this->db->from('expense_vendors as alt');

		if(!empty($Data['search']))
		{
			$this->db->group_start();
			$this->db->where("alt.alert_number like ","%".$Data['search']."%");
			$this->db->or_where("alt.alert_date like ","%".$Data['search']."%");
			//$this->db->or_where("alt.vendor_name like ","%".$Data['search']."%");
			$this->db->or_where("alt.alert_reminder like ","%".$Data['search']."%");
			$this->db->or_where("cus.cust_name like ","%".$Data['search']."%");
			$this->db->or_where("alt.alert_condition like ","%".$Data['search']."%");
			$this->db->or_where("alt.alert_target like ","%".$Data['search']."%");
			$this->db->group_end();

		}
		$this->db->join('expense_vendors as cus', 'cus.vendor_id = alt.parent_id' ,'left');
		if($Data['valerts_customer']!=''){
			$this->db->where("alt.parent_id",$Data['valerts_customer']);
		}
		if($Data['valerts_start_date']!=''){
			$this->db->where("alt.alert_date>=",date('Y-m-d',strtotime(str_replace('/', '-',$Data['valerts_start_date']))));
		}
		if($Data['valerts_end_date']!=''){
			$this->db->where("alt.alert_date<=",date('Y-m-d',strtotime(str_replace('/', '-',$Data['valerts_end_date']))));
		}
		$this->db->where("alt.bus_id",$Data['bus_id']);
		$this->db->where("alt.alert_type","Vendor");
		$this->db->where("alt.status","Active");
		
		$this->db->group_by("alt.id");
		$this->db->order_by("".$sort_field." ".$orderBy."");

		if( $c == 1)
		{
			if($Data['length']!= -1){		
			$this->db->limit($Data['length'],$Data['start']);
			}
			$query = $this->db->get();
			//print_r($this->db->last_query());exit;
			$result= $query->result_array();
			
			return $result;			
		}
		else
		{		
			
			$query = $this->db->get();
			$result['NumRecords']=$query->num_rows();		
			return $result;		
		}

	}


	public function chAlertsFilter($Data,$sort_field,$orderBy,$c)
	{
		$this->db->select("alt.*,cus.cust_id,cus.cust_name");
		$this->db->from('my_alerts as alt');

		if(!empty($Data['search']))
		{
			$this->db->group_start();
			$this->db->where("alt.alert_number like ","%".$Data['search']."%");
			$this->db->or_where("alt.alert_date like ","%".$Data['search']."%");
			$this->db->or_where("cus.cust_name like ","%".$Data['search']."%");
			$this->db->or_where("alt.alert_reminder like ","%".$Data['search']."%");
			$this->db->group_end();
			
		}
		$this->db->join('sales_customers as cus', 'cus.cust_id = alt.parent_id' ,'left');
		if($Data['halerts_customer']!=''){
			$this->db->where("alt.parent_id",$Data['halerts_customer']);
		}
		if($Data['halerts_start_date']!=''){
			$this->db->where("alt.alert_date>=",date('Y-m-d',strtotime(str_replace('/', '-',$Data['halerts_start_date']))));
		}
		if($Data['halerts_end_date']!=''){
			$this->db->where("alt.alert_date<=",date('Y-m-d',strtotime(str_replace('/', '-',$Data['halerts_end_date']))));
		}
		$this->db->where("alt.bus_id",$Data['bus_id']);
		$this->db->where("alt.alert_type",$Data['alert_type']);
		$this->db->where("alt.status","Active");
		
		$this->db->group_by("alt.id");
		$this->db->order_by("".$sort_field." ".$orderBy."");

		if( $c == 1)
		{
			if($Data['length']!= -1){		
			$this->db->limit($Data['length'],$Data['start']);
			}
			$query = $this->db->get();
			//print_r($this->db->last_query());exit;
			$result= $query->result_array();
			
			return $result;			
		}
		else
		{		
			
			$query = $this->db->get();
			//print $this->db->last_query(); exit();
			$result['NumRecords']=$query->num_rows();		
			return $result;		
		}

	}
	public function baAlertsFilter($Data,$sort_field,$orderBy,$c)
	{
		$this->db->select("alt.*");
		$this->db->from('my_alerts as alt');

		if(!empty($Data['search']))
		{
			$this->db->group_start();
			$this->db->where("alt.alert_number like ","%".$Data['search']."%");
			$this->db->or_where("alt.alert_date like ","%".$Data['search']."%");
			$this->db->or_where("alt.alert_name like ","%".$Data['search']."%");
			$this->db->or_where("alt.alert_occasion like ","%".$Data['search']."%");
			$this->db->or_where("alt.alert_reminder like ","%".$Data['search']."%");
			$this->db->group_end();
		}
		if($Data['balerts_occasion']!=''){
			$this->db->where("alt.alert_occasion",$Data['balerts_occasion']);
		}
		if($Data['balerts_customer']!=''){
			$this->db->where("alt.alert_stakeholder",$Data['balerts_customer']);
		}
		if($Data['balerts_start_date']!=''){
			$this->db->where("alt.alert_date>=",date('Y-m-d',strtotime(str_replace('/', '-',$Data['balerts_start_date']))));
		}
		if($Data['balerts_end_date']!=''){
			$this->db->where("alt.alert_date<=",date('Y-m-d',strtotime(str_replace('/', '-',$Data['balerts_end_date']))));
		}
		$this->db->where("alt.bus_id",$Data['bus_id']);
		$this->db->where("alt.alert_type",$Data['alert_type']);
		$this->db->where("alt.status","Active");
		
		$this->db->group_by("alt.id");
		$this->db->order_by("".$sort_field." ".$orderBy."");

		if( $c == 1)
		{
			if($Data['length']!= -1){		
			$this->db->limit($Data['length'],$Data['start']);
			}
			$query = $this->db->get();
			//print_r($this->db->last_query());exit;
			$result= $query->result_array();
			
			return $result;			
		}
		else
		{		
			
			$query = $this->db->get();
			$result['NumRecords']=$query->num_rows();		
			return $result;		
		}

	}
	
	public function print_multiple_alerts($array,$bus_id,$alert_type,$sub_table,$field,$singal='') {
       

		$this->db->select("alt.*,cus.cust_id,cus.cust_name");
		$this->db->from('my_alerts as alt');

		$this->db->join('sales_customers as cus', 'cus.cust_id = alt.parent_id' ,'left');

		if($singal == 1){
			$this->db->where('alt.id',$array);
		}else {
			$this->db->where_in('alt.id',$array);
		}

		$this->db->where('alt.bus_id', $bus_id);
			
		$this->db->where('alt.status', "Active");

		$query = $this->db->get();
		//print $this->db->last_query(); exit();

		$result = $query->result_array();
		return $result;	
	
	}

	public function print_multiple_alert($array,$bus_id,$alert_type,$sub_table,$field,$singal='') {

		if($sub_table==''){
		$this->db->select('alt.*,alt.status as alt_status');
		}else{
		$this->db->select('alt.*,alt.status as alt_status,sub.*');
		}
		$this->db->from('my_alerts as alt');
		if($sub_table!=''){
		$this->db->join($sub_table.' as sub', 'sub.'.$field.' = alt.parent_id' ,'left');
		}
		if($singal==1){
			$this->db->where('alt.id',$array);
		}else {
			$this->db->where_in('alt.id',$array);
		}	
		$this->db->where_in('alt.alert_type',array('services', 'expense'));
		$this->db->where('alt.bus_id',$bus_id);
		$this->db->where('alt.status',"Active");
		$this->db->group_by("alt.id");

		$query = $this->db->get();
		$result= $query->result_array();
		return $result;	
	
	}



	public function activityAlertsFilter($Data,$sort_field,$orderBy,$c) {

		$this->db->select("alt.*");
		$this->db->from('my_alerts as alt');

		/*if(!empty($Data['search'])) {
			$this->db->group_start();
			$this->db->where("alt.alert_number like ","%".$Data['search']."%");
			$this->db->or_where("alt.alert_date like ","%".$Data['search']."%");
			$this->db->or_where("alt.alert_name like ","%".$Data['search']."%");
			$this->db->or_where("alt.alert_date_time like ","%".$Data['search']."%");
			$this->db->or_where("alt.alert_type like ","%".$Data['search']."%");
			$this->db->or_where("alt.alert_reminder like ","%".$Data['search']."%");
			$this->db->group_end();
		}*/

		if($Data['aalerts_start_date']!=''){
			$this->db->where("alt.alert_date>=",date('Y-m-d',strtotime(str_replace('/', '-',$Data['aalerts_start_date']))));
		}
		if($Data['aalerts_end_date']!=''){
			$this->db->where("alt.alert_date<=",date('Y-m-d',strtotime(str_replace('/', '-',$Data['aalerts_end_date']))));
		}

		$this->db->where("alt.bus_id", $Data['bus_id']);
		$this->db->where_in("alt.alert_type", $Data['alert_type']);
		$this->db->where("alt.status", "Active");
		
		$this->db->group_by("alt.id");
		$this->db->order_by("".$sort_field." ".$orderBy."");

		if( $c == 1) {
			if($Data['length'] != -1){	
				$this->db->limit($Data['length'], $Data['start']);
			}

			$query = $this->db->get();
			//print $this->db->last_query();
			$result = $query->result_array();
			return $result;			
		} else {		
			$query = $this->db->get();
			$result['NumRecords'] = $query->num_rows();
			return $result;		
		}

	}

	public function print_multiple_activityalert($array,$bus_id,$alert_type,$sub_table,$field,$singal='') {

		if($sub_table==''){
			$this->db->select('alt.*,alt.status as alt_status');
		}else{
			$this->db->select('alt.*,alt.status as alt_status,sub.*');
		}

		$this->db->from('my_alerts as alt');
		
		if($sub_table != ''){
			$this->db->join($sub_table.' as sub', 'sub.'.$field.' = alt.parent_id' ,'left');
		}

		if($singal==1){
			$this->db->where('alt.id',$array);
		}else {
			$this->db->where_in('alt.id',$array);
		}

		$this->db->where_in('alt.alert_type',array('activity'));
		$this->db->where('alt.bus_id',$bus_id);
		$this->db->where('alt.status',"Active");
		$this->db->group_by("alt.id");

		$query = $this->db->get();
		//print $this->db->last_query(); exit();
		$result= $query->result_array();
		return $result;	
	
	}

	public function sss($bus_id, $id){

		$this->db->select('ma.bus_id, ma.id, ma.alert_date_time');
		$this->db->from('my_alerts as ma');

		$this->db->where('id', $id);
		$this->db->where('bus_id', $bus_id);

		$query = $this->db->get();
		$result = $query->result_array();
		return $result;
	}

	
}
?>