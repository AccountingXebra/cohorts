<?php
class User_model extends CI_Model{
		var $table;
	public function  __construct(){
		parent::__construct();
		$this->load->database();
		$this->table ='user_master';
	}


/*
	| -------------------------------------------------------------------
	| check unique fields
	| -------------------------------------------------------------------
	|
	*/
	public function isUnique($field, $value,$id='')
	{
		$this->db->select('*');
		$this->db->from('user_master');
		$this->db->where($field,$value);
		if($id!='')
		{
			$this->db->where("iUserId != ",$id);
		}
		$this->db->where("is_deleted",0);

		$query = $this->db->get();
		// echo "<pre>";
		// print_r($query);exit();
		$data = $query->num_rows();
		return ($data > 0)?FALSE:TRUE;
	}

	public function fetchUserData($column , $value)
	{
		$this->db->select('*');
		$this->db->from('user_master');
		$this->db->where($column,$value);
		$this->db->where('is_deleted',0);
		$this->db->where('tStatus',1);
		
		$query = $this->db->get();
		// echo "<pre>";
		// print_r($query);exit();
		$data = (array) $query->first_row();
		$dataArray = array();
		foreach ($data as $key => $value) {
			if($value == null)
				$data[$key] = '';
		}
		return $data;
	}

	public function fetchLastUserId()
	{
		$this->db->select('iUserId');
		$this->db->from('user_master');
		$this->db->order_by("iUserId","DESC");
		$query = $this->db->get();

		return (array) $query->first_row();
	}

	public function fetchUserAllData($column,$value)
	{
		$this->db->select('u.* ,d.vDeviceToken , d.tDeviceType ');
		$this->db->from('user_master as u');
 		$this->db->join("device_master as d","u.iUserId = d.iUserId","LEFT");
		$this->db->where($column,$value);
		$this->db->where('is_deleted',0);
		
		$query = $this->db->get();

		// echo "<pre>";
		// print_r($query);exit();
		$data = (array) $query->first_row();

		$dataArray = array();
		foreach ($data as $key => $value) {
			if($value == null)
				$data[$key] = '';
		}
		return $data;
	}

		/*
	| -------------------------------------------------------------------
	| Insert data
	| -------------------------------------------------------------------
	|
	| general function to insert data in table
	|
	*/
	public function businessData($type)
	{
		$this->db->select('u.* ,d.name');
		$this->db->from('businesslist as u');
 		$this->db->join("cities as d","u.bus_billing_city = d.city_id","LEFT");
		
		
		
		$query = $this->db->get();

		// echo "<pre>";
		// print_r($query);exit();
		

		$dataArray = array();
		if ($type == "rowcount") {
			$data = $query->num_rows();
		}else{
			$data = $query->result();
		}
		return $data;
	}
	public function insertData($table, $data)
	{
		
		$result = $this->db->insert($table, $data);

		if($result == 1){

			$id=$this->db->insert_id();
			
			return  $id;

		}else{
			return false;
		}
	}
	
	
	/*
	| -------------------------------------------------------------------
	| Update data
	| -------------------------------------------------------------------
	|
	| general function to update data
	|
	*/
	public function updateData($table, $data, $where)
	{

		$this->db->where($where);
		if($this->db->update($table, $data)){

			return 1;
		}else{
			return 0;
		}
	}
	
	
	
	/*
	| -------------------------------------------------------------------
	| Select data
	| -------------------------------------------------------------------
	|
	| general function to get result by passing nesessary parameters
	|
	*/
	public function selectData($table, $fields='*', $where='', $order_by="", $order_type="", $group_by="", $limit="", $rows="", $type='')
	{
		$this->db->select($fields);
		$this->db->from($table);
		if ($where != "") {
			$this->db->where($where);
		}

		if ($order_by != '') {
			$this->db->order_by($order_by,$order_type);
		}

		if ($group_by != '') {
			$this->db->group_by($group_by);
		}

		if ($limit > 0 && $rows == "") {
			$this->db->limit($limit);
		}
		if ($rows > 0) {
			$this->db->limit($rows, $limit);
		}


		$query = $this->db->get();

		if ($type == "rowcount") {
			$data = $query->num_rows();
		}else{
			$data = $query->result();
		}

		#echo "<pre>"; print_r($this->db->queries); exit;
		$query->result();

		return $data;
	}

	public function fetchUserFilter($Data,$sort_field,$orderBy,$c)
	{
		$this->db->select("us.*");
		$this->db->from('user_master as us');

		if(!empty($Data['search']['value']))
		{
			$this->db->where("vName like ","%".$Data['search']['value']."%");
			$this->db->or_where("vEmail like ","%".$Data['search']['value']."%");
			$this->db->or_where("vFacebookId like ","%".$Data['search']['value']."%");
			$this->db->or_where("tStatus like ","%".$Data['search']['value']."%");
			$this->db->or_where("tIsSubscribe like ","%".$Data['search']['value']."%");
			$this->db->or_where("vAddress like ","%".$Data['search']['value']."%");
			$this->db->or_where("vPostCode like ","%".$Data['search']['value']."%");
			$this->db->or_where("vCountry like ","%".$Data['search']['value']."%");
			$this->db->or_where("vContact like ","%".$Data['search']['value']."%");
		}
		$this->db->where("us.is_deleted",0);

		$this->db->order_by("".$sort_field." ".$orderBy."");

		if( $c == 1)
		{
			if($Data['length']!= -1){		
			$this->db->limit($Data['length'],$Data['start']);
			}
			$query = $this->db->get();

			$result= $query->result_array();	
			return $result;			
		}
		else
		{		
			
			$query = $this->db->get();
			$result['NumRecords']=$query->num_rows();		
			return $result;		
		}

	}

		/*
	| -------------------------------------------------------------------
	| Role List
	| -------------------------------------------------------------------
	|
	| general function to delete the records
	|
	*/

	public function selectRoleData($table){
		$query = $this->db->get($table);
		if($query!=NULL){
			return $query->result();
		}
		else{
			return false;
		}
	}

	
		/*
	| -------------------------------------------------------------------
	| Delere data
	| -------------------------------------------------------------------
	|
	| general function to delete the records
	|
	*/
	public function deleteData($table, $data)
	{
		if($this->db->delete($table, $data)){

			return 1;
		}else{
			return 0;
		}
	}

	public function totalActiveUserCount()
	{
		$this->db->select('*');
		$this->db->from('user_master');
		$this->db->where("is_deleted",0);
		$this->db->where("tStatus",1);

		$query = $this->db->get();
		// echo "<pre>";
		// print_r($query);exit();
		$data = $query->num_rows();
		return $data;
	}

		public function AuthAttributes($type,$data) {
        $arr = array();
        
        if ($type == 'signup') {
            $arr = array(
                "vEmail" => "vEmail must not be blank.",
                "vName" => "vName must not be blank.",
                "vPassword" => "vPassword must not be blank.",
                "RegisterType" => "RegisterType must not be blank.",
                "dlatitude" => "dlatitude must not be blank.",
                "dlongitude" => "dlongitude must not be blank.",
            );
        }else if ($type == 'fbsignup') {
            $arr = array(
                "vFacebookId" => "vFacebookId must not be blank.",
                "vName" => "vName must not be blank.",
                "RegisterType" => "RegisterType must not be blank.",
                "dlatitude" => "dlatitude must not be blank.",
                "dlongitude" => "dlongitude must not be blank.",
            );
        }else if ($type == 'guestsignup') {
            $arr = array(
                "RegisterType" => "RegisterType must not be blank.",
                "dlatitude" => "dlatitude must not be blank.",
                "dlongitude" => "dlongitude must not be blank.",
            );
        }else if ($type == 'simpleSignin') {
            $arr = array(
                "vEmail" => "vEmail must not be blank.",
                "vPassword" => "vPassword must not be blank.",
            );
        }else if ($type == 'FBSignin') {
            $arr = array(
                "vFacebookId" => "vFacebookId must not be blank.",
            );
        }else if ($type == 'forgotPassword') {
            $arr = array(
                "vEmail" => "vEmail must not be blank.",
            );
        }else if($type == 'editProfile'){
        	$arr = array(
        		"vName" => "vName must not be blank.",
        	);
        }else if($type == 'changePassword'){
        	$arr = array(
        		"oldPassword" => "oldPassword must not be blank.",
        		"newPassword" => "newPassword must not be blank.",
    		);
        }else if($type == 'userOrderAddress'){
        	$arr = array(
        		"vName" => "vName must not be blank.",
        		"vEmail" => "vEmail must not be blank.",
        		"vContact" => "vContact must not be blank.",
        		"vAddress" => "vAddress must not be blank.",
        		"vPostCode" => "vPostCode must not be blank.",
        		"vCountry" => "vCountry must not be blank.",
        	);
        }


        foreach ($arr as $key => $value) {

            if (empty($data[$key]) || $data[$key] == "" || $data[$key] == null) {
                $this->Common_model->code = 400;
                $this->Common_model->message = $value;
                $this->Common_model->data = (object) array();
                return false;
            }
        }
        return true;
    }
  
	public function documentLockerFilter($Data,$sort_field,$orderBy,$c) {
		
		//Legal Documents
		$this->db->select("legal.legal_id as 'dc_id', legal.bus_id, legal.bus_id as cust_id, legal.master_id, legal.master_id as parent_id, legal.legal_document as 'dc_name', legal.legal_type as 'type', CAST(legal.legal_document_size AS DECIMAL(18,2)), legal.legal_document_size as 'document_size', legal.createdat as 'created', legal.legal_po_date as 'po_date', legal.legal_start_date as 'start_date', legal.legal_end_date as 'end_date', legal.status as 'dstatus', legal.legal_doc_type as 'doc_type', legal.legal_reminder as 'reminder'");

		$this->db->from('legal_document as legal');
		
		if(!empty($Data['search'])) {

			$this->db->group_start();

			$this->db->where("legal.createdat like ","%".$Data['search']."%");
			$this->db->or_where("legal.legal_document like ","%".$Data['search']."%");
			
			if(strtolower($Data['search']) == "company" || strtolower($Data['search'])=="company profile"){
				$status="company";
				$this->db->or_where("legal.legal_type like ","%".$status."%");
				
			}else if(strtolower($Data['search'])=="client" || strtolower($Data['search'])=="my clients"){
				$status="customer";
				$this->db->or_where("legal.legal_type like ","%".$status."%");
				
			} else if(strtolower($Data['search'])=="vendor" || strtolower($Data['search'])=="my vendors"){
				$status="vendor";
				$this->db->or_where("legal.legal_type like ","%".$status."%");
			} else {
				$this->db->or_where("legal.legal_type like ","%".$Data['search']."%");
				
			}

			$this->db->or_where("legal.legal_document_size like ","%".$Data['search']."%");
			
			$this->db->group_end();
		}
		
		if($Data['docl_type']!=''){
			$this->db->where("legal.legal_type",$Data['docl_type']);
			
		}

		if($Data['docl_start_date']!=''){
			$this->db->where("DATE_FORMAT(legal.createdat, '%Y-%m-%d') >=",date('Y-m-d',strtotime(str_replace('/', '-',$Data['docl_start_date']))));
		}
		if($Data['docl_end_date']!=''){
			$this->db->where("DATE_FORMAT(legal.createdat, '%Y-%m-%d') <=",date('Y-m-d',strtotime(str_replace('/', '-',$Data['docl_end_date']))));
		}


		/*if($Data['docl_start_date']!=''){
			$this->db->where("legal.createdat >=",date('Y-m-d',strtotime($Data['docl_start_date'])));
			
		}

		if($Data['docl_end_date']!=''){
			$this->db->where("legal.createdat <=",date('Y-m-d',strtotime($Data['docl_end_date'])));
			
		}*/

		if(isset($Data['cust_id'])){
			$this->db->where("legal.bus_id", $Data['cust_id']);
			
		}

		$this->db->where("legal.master_id", $Data['bus_id']);
		$this->db->where("legal.status","Active");
		
		$this->db->group_by("legal.legal_id");
		
		$this->db->get(); 
    	$query1 = $this->db->last_query();

        $this->db->select("other.other_doc_id as 'dc_id',other.bus_id,other.bus_id as cust_id,other.master_id,other.master_id as parent_id,other.other_doc_name as 'dc_name',other.other_type as 'type',CAST(other.other_doc_size AS DECIMAL(18,2)),other.other_doc_size as 'document_size',other.createdat as 'created',other.createdat as 'po_date',other.other_start_date as 'start_date',other.other_end_date as 'end_date',other.status as 'dstatus',other.other_doc_type as 'doc_type',other.other_reminder as 'reminder'");

		$this->db->from('other_document as other');
		
		if(!empty($Data['search'])) {

			$this->db->group_start();

			$this->db->where("other.createdat like ","%".$Data['search']."%");
			$this->db->or_where("other.other_doc_name like ","%".$Data['search']."%");
			
			if(strtolower($Data['search'])=="company" || strtolower($Data['search'])=="company profile"){
				$status="company";
				$this->db->or_where("other.other_doc_type like ","%".$status."%");
				
			}else if(strtolower($Data['search'])=="client" || strtolower($Data['search'])=="my clients"){
				$status="customer";
				$this->db->or_where("other.other_doc_type like ","%".$status."%");
			} else if(strtolower($Data['search'])=="vendor" || strtolower($Data['search'])=="my vendors"){
				$status="vendor";
				$this->db->or_where("legal.legal_type like ","%".$status."%");
			}else{
				$this->db->or_where("other.other_doc_type like ","%".$Data['search']."%");
				
			}

			$this->db->or_where("other.other_doc_size like ","%".$Data['search']."%");
			
			$this->db->group_end();
		}
		
		if($Data['docl_type']!=''){
			$this->db->where("other.other_type",$Data['docl_type']);
			
		}

		if($Data['docl_start_date']!=''){
			$this->db->where("DATE_FORMAT(other.createdat, '%Y-%m-%d') >=",date('Y-m-d',strtotime(str_replace('/', '-',$Data['docl_start_date']))));
		}
		if($Data['docl_end_date']!=''){
			$this->db->where("DATE_FORMAT(other.createdat, '%Y-%m-%d') <=",date('Y-m-d',strtotime(str_replace('/', '-',$Data['docl_end_date']))));
		}

		/*if($Data['docl_start_date']!=''){
			$this->db->where("other.createdat >=",date('Y-m-d',strtotime($Data['docl_start_date'])));
			
		}

		if($Data['docl_end_date']!=''){
			$this->db->where("other.createdat <=",date('Y-m-d',strtotime($Data['docl_end_date'])));
			
		}*/

		if(isset($Data['cust_id'])){
			$this->db->where("other.bus_id",$Data['cust_id']);
			
		}

		$this->db->where("other.master_id",$Data['bus_id']);
		$this->db->where("other.status","Active");
		
		$this->db->group_by("other.other_doc_id");
		$field_pos=array("other_doc_id"=>'0',"created_at"=>'1',"other_doc_name"=>'2',"other_type"=>'3',"other_doc_size"=>'4');	
		
        $this->db->get(); 
    	$query2 = $this->db->last_query();

        $this->db->select("srf.srf_id as 'dc_id',srf.bus_id,srf.cust_id as cust_id,srf.bus_id,srf.parent_id as parent_id,srf.srf_document as 'dc_name',srf.srf_type as 'type',CAST(srf.srf_document_size AS DECIMAL(18,2)),srf.srf_document_size as 'document_size',srf.createdat as 'created',srf.createdat as 'po_date',srf.srf_start_date as 'start_date',srf.srf_end_date as 'end_date',srf.status as 'dstatus',srf.srf_type as 'doc_type',srf.srf_reminder as 'reminder'");

		$this->db->from('srf_document as srf');
		
		if(!empty($Data['search'])) {

			$this->db->group_start();

			$this->db->where("srf.createdat like ","%".$Data['search']."%");
			$this->db->or_where("srf.srf_document like ","%".$Data['search']."%");
			
			if(strtolower($Data['search'])=="expense" || strtolower($Data['search'])=="expense_voucher"){
		      $status="expense_voucher";
				$this->db->or_where("srf.srf_type like ","%".$status."%");
				
			}else if(strtolower($Data['search'])=="sales" || strtolower($Data['search'])=="sales_receipt"){
				$status="sales_receipt";
				$this->db->or_where("srf.srf_type like ","%".$status."%");
				
			}else{
				$this->db->or_where("srf.srf_type like ","%".$Data['search']."%");
				
			}

			$this->db->or_where("srf.srf_document_size like ","%".$Data['search']."%");
			
			$this->db->group_end();
		}
		
		if($Data['docl_type']!=''){
			$this->db->where("srf.srf_type",$Data['docl_type']);
			
		}

		if($Data['docl_start_date']!=''){
			$this->db->where("DATE_FORMAT(srf.createdat, '%Y-%m-%d') >=",date('Y-m-d',strtotime(str_replace('/', '-',$Data['docl_start_date']))));
		}
		if($Data['docl_end_date']!=''){
			$this->db->where("DATE_FORMAT(srf.createdat, '%Y-%m-%d') <=",date('Y-m-d',strtotime(str_replace('/', '-',$Data['docl_end_date']))));
		}

		/*if($Data['docl_start_date']!=''){
			$this->db->where("srf.createdat >=",date('Y-m-d',strtotime($Data['docl_start_date'])));
			
		}

		if($Data['docl_end_date']!=''){
			$this->db->where("srf.createdat <=",date('Y-m-d',strtotime($Data['docl_end_date'])));
			
		}*/

		if(isset($Data['cust_id'])){
			$this->db->where("srf.cust_id",$Data['cust_id']);
			
		}

		$this->db->where("srf.bus_id",$Data['bus_id']);
		$this->db->where("srf.status","Active");
		
		$this->db->group_by("srf.srf_id");
		$field_pos=array("srf_id"=>'0',"created_at"=>'1',"srf_document"=>'2',"srf_type"=>'3',"srf_document_size"=>'4');	
		
        $this->db->get(); 
    	$query3 = $this->db->last_query();
        $limit = "";
    	if($c == 1) {
			if($Data['length'] != -1) {		
				$this->db->limit($Data['length'],$Data['start']);
				$limit = " LIMIT ".$Data['start'].",".$Data['length'];
			}
			else {
			$limit = "";
		}
		} 

        $query = $this->db->query($query1." UNION ALL ".$query2 ." UNION ALL ".$query3 .' ORDER BY `dc_id` '. $orderBy . $limit);

		if( $c == 1) {

			if($Data['length']!= -1) {
				$this->db->limit($Data['length'],$Data['start']);
			}
			
			$result = $query->result_array();
			return $result;
		} else {
			$result= $query->result_array();
			//print $this->db->last_query(); exit;
			$result['NumRecords'] = count($result);
			return $result;
		}
	}

	public function download_multiple_documents_locker($array,$singal='') {
		$this->db->select('legal.*');
		$this->db->from('legal_document as legal');

		if($singal==1){
			$this->db->where('legal.legal_id',$array);
		}else {
			$this->db->where_in('legal.legal_id',$array);
		}	
		//$this->db->where('legal.bus_id',$bus_id);
		$this->db->where('legal.status',"Active");
		//$this->db->group_by("legal.lega_id");
		$query = $this->db->get();
		$result= $query->result_array();
		return $result;	
	
	}


}
?>
