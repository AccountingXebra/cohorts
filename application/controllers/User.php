<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require ("common/Index_Controller.php");
class Users extends Index_Controller {

	function __construct(){
		parent::__construct();
		
		is_login();
		is_trialExpire();
		 is_BusReg();
		$this->user_session = $this->session->userdata('user_session');
		$this->load->model('User_model');
	}

	public function index()
	{ 
		$data= array();
		$data['flash_msg'] = $this->msg_session;
		$this->load->view('user/index', $data);
		$this->session->set_userdata('msg_session','');
	}

	public function ajax_list()
	{
		$post = $this->input->post();

		$field_pos=array("vName"=>'0',"vEmail"=>'1',"vContact"=>'2',"vFacebookId"=>'3',"tGender"=>'4',"vAddress"=>'5','vPostCode'=>'6','vCountry'=>'7' , 'tIsSubscribe' => '8' , 'tStatus' => '9');	

		$sort_field=array_search($post['order'][0]['column'],$field_pos);

		if($post['order'][0]['dir']=='asc')
		{
	 		$orderBy="ASC";
		}
	 	else
		{
	 		$orderBy="DESC";
		}	

		$TotalRecord=$this->User_model->fetchUserFilter($post,$sort_field,$orderBy,0);	
	 	$userData = $this->User_model->fetchUserFilter($post,$sort_field,$orderBy,1);

	 	$iTotalRecords = $TotalRecord['NumRecords'];

	 	$records = array();
	  	$records["data"] = array();

	  	foreach ($userData as $key => $value) {
	  		$gender = '-';
	  		if($value['tGender'] == 1)
	  			$gender = 'Male';
	  		if($value['tGender'] == 2)
	  			$gender = 'Female';

	  		$status = '';
	  		if($value['tStatus'] == 1)
	  			$status = 'Active';
	  		else
	  			$status = 'Inactive';
	  		$btnColor = ($value['tStatus'] == 1)? "bg-green" : "bg-red" ;

	  		$statusBtn = '<button class="btn '. $btnColor.'" onclick="changeStatus('.$value['iUserId'].','.$value['tStatus'] .');" >'.$status.'</button>';

	  		$action = '';

	  		$action .= '<button class="btn bg-red" onclick="deleteUser('.$value['iUserId'].');" ><i class="fa fa-trash"></i></button>';


	  		$records["data"][] = array(
	  			$value['vName'],
	  			$value['vEmail'],
	  			$value['vContact'],
	  			$value['vFacebookId'],
	  			$gender,
	  			$value['vAddress'],
	  			$value['vPostCode'],
	  			$value['vCountry'],
	  			($value['tIsSubscribe'] == 0 )? "No" : "Yes",
	  			$statusBtn,
	  			$action,
	  			);
	  	}




	  	$records["draw"] = intval($post['draw']);
		$records["recordsTotal"] = $iTotalRecords;
		$records["recordsFiltered"] = $iTotalRecords;		
		echo json_encode($records);				  
		exit;
	}

	public function changeStatus()
	{
		$post = $this->input->post();

		if(!empty($post['userId']) && !empty($post['status']))
		{
			$newStatus = ($post['status'] == 1)? 2 : 1;

			$this->User_model->updateData(array("tStatus"=>$newStatus) , array("iUserId" => $post['userId']));
			echo "success";
		}
		else
		{
			echo "Error";

		}
		exit;
	}

	public function deleteUser()
	{
		$post = $this->input->post();
    	
		if(!empty($post['id']))
		{
			$this->User_model->updateData(array("is_deleted"=>1) , array("iUserId" => $post['id']));
			echo "success";
			// $flash_arr = array('flash_type' => 'success',
			// 									'flash_msg' => 'User deleted successfully.'
			// 								);
			// $this->session->set_userdata('msg_session',$flash_arr);
		}
		else
		{
			echo "Error";

		}
		exit;
	}
	public function testpdf()
	{
		$content=$this->load->view('',true);
		$this->pdf->create();
	}
	
}
?>