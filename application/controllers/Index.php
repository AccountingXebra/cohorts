<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require ("common/Index_Controller.php");
class Index extends Index_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct(){
		parent::__construct();
		$this->load->model('Adminmaster_model');
		$this->load->model('User_model');
		$this->load->library('session');

		//$this->load->model('Sales_model');
		$this->load->model('Subscription_model');
		$this->load->model('Profile_model');
		$this->load->model('Market_place_model');
		$this->load->model('Community_model');

		 //MY_Output's disable_cache() method
       // $this->output->disable_cache();



	}

	public function no_internet() {

		$this->load->view('errors/html/no-internet.php');
	}

	public function index()
	{
		$session = $this->session->userdata('user_session');

		if (isset($session['reg_id'])) {

			$redirectId ="community/";

			redirect($redirectId);
		}



		 if(@$_GET){
			if(@$_GET['signup']){
           $data['open_signup'] = 1;
           }
           else if(@$_GET['ne']){
           	$data['web_email'] = $_GET['ne'];
           $data['open_signup'] = 1;
           }

          
		}else{
			$data['web_email'] = '';
			$data['open_signup'] = "";
		}
		$this->load->view('index/index',$data);
	}



	function logoff(){

		$re='';
		if(@$this->session->userdata('client_session')){
			$re='client-portal';
		}elseif(!@$this->session->userdata('user_session')){
			$this->session->sess_destroy();

			redirect('https://'.$_SERVER['HTTP_HOST']);
		}else{
			date_default_timezone_set('Asia/Kolkata');
			$get_activity_data = $this->Adminmaster_model->selectData('login_record','*', array('login_id'=> $this->session->userdata['user_session']['reg_login_id']));
			// echo strtotime($get_activity_data[0]->created_at);
			// echo "<br>";
			// echo time();
			// echo "<br>";
			// echo $date = date('m/d/Y h:i:s a', time());
			// echo "<br>";
			$time = time() - strtotime($get_activity_data[0]->created_at);
			$final_time = round(abs($time) / 60);
			$update = array(
					'time_spend'	=>	$final_time
				);

				$update_status = $this->Adminmaster_model->updateData('login_record',$update,array('login_id'=> $this->session->userdata['user_session']['reg_login_id']));

		}

		$this->session->sess_destroy();

		//redirect('https://'.$_SERVER['HTTP_HOST']);
		//$this->load->view('index/logged-off');
		$this->load->view('index/login-back');
	}
	function success(){

		$session_data = $this->session->userdata('user_session');

		if($session_data['reg_admin_type'] && $session_data['reg_id'] && $session_data['bus_id'])
		{
			$redirectId = $this->GetRedirectURL($session_data['reg_admin_type'],$session_data['reg_id'],$session_data['bus_id']);

			redirect('community/');

		}else{

			redirect('community/');
		}


	}

	public function GetRedirectURL($type,$reg_id,$bus_id)
	{

		$redirectId = $this->findRedirectPath($type,$reg_id,$bus_id);
		if($redirectId =='1')
		{
			return 'sales/billing-documents';
		}
		else if($redirectId =='2')
		{
			return 'tax-summary';
		}
		else if($redirectId =='4')
		{
			return 'employee/manage_employee_master';
		}
		else if($redirectId =='5')
		{
			return 'sales/expense_voucher';
		}
		else if($redirectId =='31')
		{
			return 'profile/add-personal-profile';
		}
		else if($redirectId =='32')
		{
			return 'profile/add-company-profile';
		}
		else if($redirectId =='33')
		{
			return 'sales/billing-documents';
		}
		else
		{
			return 'sales/manage-company-profile';
		}

	}

	/*function emailmsg(){
		$this->load->view('index/email-change-password');
	}*/

	public function logout()
	{

		$re='';
		if(@$this->session->userdata('client_session')){
			$re='client-portal';
		}elseif(!@$this->session->userdata('user_session')){
			$this->session->sess_destroy();

			redirect('https://'.$_SERVER['HTTP_HOST']);
		}else{
			date_default_timezone_set('Asia/Kolkata');
			$get_activity_data = $this->Adminmaster_model->selectData('login_record','*', array('login_id'=> $this->session->userdata['user_session']['reg_login_id']));
			// echo strtotime($get_activity_data[0]->created_at);
			// echo "<br>";
			// echo time();
			// echo "<br>";
			// echo $date = date('m/d/Y h:i:s a', time());
			// echo "<br>";
			$time = time() - strtotime($get_activity_data[0]->created_at);
			$final_time = round(abs($time) / 60);
			$update = array(
					'time_spend'	=>	$final_time
				);

				$update_status = $this->Adminmaster_model->updateData('login_record',$update,array('login_id'=> $this->session->userdata['user_session']['reg_login_id']));

		}

		$this->session->sess_destroy();

		redirect('https://'.$_SERVER['HTTP_HOST']."/cohorts/");
	}

	public function logout_signup()
	{
		$re='';
		if($this->session->userdata('client_session')){
			$re='client-portal';
		}else{
			date_default_timezone_set('Asia/Kolkata');
			$get_activity_data = $this->Adminmaster_model->selectData('login_record','*', array('login_id'=> $this->session->userdata['user_session']['reg_login_id']));
			// echo strtotime($get_activity_data[0]->created_at);
			// echo "<br>";
			// echo time();
			// echo "<br>";
			// echo $date = date('m/d/Y h:i:s a', time());
			// echo "<br>";
			$time = time() - strtotime($get_activity_data[0]->created_at);
			$final_time = round(abs($time) / 60);
			$update = array(
					'time_spend'	=>	$final_time
				);

				$update_status = $this->Adminmaster_model->updateData('login_record',$update,array('login_id'=> $this->session->userdata['user_session']['reg_login_id']));

		}
		$this->session->sess_destroy();

		redirect(base_path().$re);
	}

	public function get_states(){
		$country_id=$this->input->post('country_id');
		$states=$this->Adminmaster_model->selectData('states','*', array('country_id'=>$country_id));
		$opt='';
		$opt.="<option value=''>Select State</option>";
		foreach($states as $st){
			$opt.="<option value=".$st->state_id." >".$st->state_name." - ".$st->state_gst_code."</option>";
		}
		echo $opt;
	}

	public function get_cities(){
		$state_id=$this->input->post('state_id');
		$cities=$this->Adminmaster_model->selectData('cities','*', array('state_id'=>$state_id));
		$opt='';
		$opt.="<option value=''>Select City</option>";
		foreach($cities as $st){
			$opt.="<option value=".$st->city_id." >".$st->name."</option>";
		}
		echo $opt;
	}

	/*public function resetPassword($token)
	{
		$data = array();
		if(!empty($token))
		{
			$userData = $this->User_model->fetchUserData("vToken" , $token);
			if(!empty($userData))
			{
				$data['status'] = "success";

				$post = $this->input->post();
				if ($post) {

					$updateUserData = array();
					$updateUserData['vPassword'] = md5($post['newpassword']);
					$updateUserData['vToken'] = md5($userData['vEmail'] . time());

					$this->User_model->updateData($updateUserData , array("iUserId" => $userData['iUserId']));

					$data['status'] = "confirm";
					$data['message'] = "Password Change Successfully.";

				}
			}
			else
			{
				$data['status'] = "error";
				$data['message'] = "Invalid link.";
			}
		}
		else
		{
			$data['status'] = "error";
			$data['message'] = "Invalid link.";
		}

		$this->load->view('index/resetPassword', $data);
	}
*/
	public function check_otp(){
		echo json_encode(true);
	}



	public function home_register()
	{
		$register_data = array();
		$ip_address='';
		if (!empty($_SERVER['HTTP_CLIENT_IP']))
		{
		 $ip_address = $_SERVER['HTTP_CLIENT_IP'];
		}
		elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
		{
		 $ip_address = $_SERVER['HTTP_X_FORWARDED_FOR'];
		}
		else
		{
		 $ip_address = $_SERVER['REMOTE_ADDR'];
		}

		 $json     = file_get_contents("http://ipinfo.io/$ip_address/geo");
$json     = json_decode($json, true);

$country  = $json['country'];
$region   = $json['region'];
$city     = $json['city'];

        $_POST=$this->security->xss_clean($_POST);
        	$csrf_token=$this->security->get_csrf_hash();
        
		
		parse_str($_POST['frm_signup'], $register_data);
		if(isset($_POST['g-recaptcha-response'])){
            $recaptcha = $_POST['g-recaptcha-response'];
			$res = $this->reCaptcha($recaptcha);
			if(!$res['success']){
				// Error
				echo "false"."/".$csrf_token;
						 exit();
			}
			unset($_POST['g-recaptcha-response']);
            }
		$email=$register_data['email'];

			if(@$register_data['subscribe'] == '1')
			{
				$exist_email_newsletter = $this->Adminmaster_model->isUnique('newsletter','email',$register_data['email']);
				if($exist_email_newsletter==TRUE)
				{
					$data =array(
						'email' => $register_data['email'],
					);
					$this->Adminmaster_model->insertData('newsletter',$data);
				}
			}
			else {
				unset($register_data['subscribe']);
			}

			$exist_email = $this->Adminmaster_model->isUnique('registration','reg_email',$email);


			if($exist_email==TRUE)
			{
					$array=array(

							'bus_company_name'=>'',
						);
					//$get_company_id = $this->Adminmaster_model->insertData('businesslist',$array);
					$reg_otp= mt_rand(100000,999999);
					$reg_mob_otp= mt_rand(100000,999999);
					$data_to_be_insert = array(
						//'bus_id'			=>  $get_company_id,
						//'reg_username'		=>	$register_data['email'],
						'reg_email'			=>	$register_data['email'],
						'reg_password' 		=>	md5($register_data['password_2']),
						//'reg_mobile'		=>	$register_data['contact'],
						//'reg_istrial'		=>	1,
						'reg_ipaddress'		=>	$ip_address,
						'reg_otp'			=>	$reg_otp,
						//'reg_mob_otp'			=>	$reg_mob_otp,
						'reg_otp_verify'	=>  'No',
						'reg_admin_type'	=>	3,
						'reg_token'			=>	"Cohorts",

						'reg_plan'          =>"Walk",
						'user_country'          =>$country,
						'user_state'          =>$region,
						'user_city'          =>$city,
						'status'			=>  "Active",
						);
					$get_admin_id = $this->Adminmaster_model->insertData('registration',$data_to_be_insert);
						if ($get_admin_id>0){

							  echo json_encode($get_admin_id);
                           exit;
                         /*   $name=explode(' ',$register_data['email']);
                            if(isset($name[1]) && $name[1]!=""){;
                            	$last_name	=$name[1];
                            }else{
                            	$last_name	="";
                            }
							
                  			$from="Xebra Cohorts";
							$dataa['user_name'] =  $register_data['email'];
							$dataa['user_start_date'] = date("d M y");
							$dataa['user_end_date'] = Date('d M y', strtotime("+14 days"));
							$htmldataa = $this->load->view('email_template/trial_period_begins',$dataa,true);
							$htmlWelcome = $this->load->view('email_template/welcome-user-email',$dataa,true);
							sendEmail($from,$register_data['email'],'Welcome to Xebra Cohorts',$htmlWelcome);
							//sendEmail($from,$register_data['email'],'Trial Period Begins',$htmldataa);


							$user = $this->Adminmaster_model->selectData('registration', '*', array("reg_id"=>$get_admin_id));
							$mob_otp=$user[0]->reg_mob_otp;
							$otp=$user[0]->reg_otp;
							$message="The one-time password to register on Xebra Cohorts is $mob_otp.";
							$msg="The one-time password to register on Xebra is $reg_otp.";
							//send_sms($user[0]->reg_mobile,$message);
							$from="Xebra Cohorts";
							$subject="Hi! Here’s your OTP.";
							$data['name'] = $register_data['email'];
							$data['otp'] = $otp;
							$content=$this->load->view('email_template/otp',$data,true);
							sendEmail($from,$register_data['email'],$subject,$content);
							echo json_encode($user);
							exit();
							*/
						}
						else
						{
						 echo "false"."/".$csrf_token;
						 exit();
						}
			}
			else{
				echo 'notExits'."/".$csrf_token;
				exit();
			}
		}

		public function send_otp(){
            
            $post = $this->input->post();
             $post=$this->security->xss_clean($post);
           $user = $this->Adminmaster_model->selectData('registration', '*', array("reg_id"=>$post['reg_id']));
                              
                                
													
							$mob_otp=$user[0]->reg_mob_otp;
							$otp=$user[0]->reg_otp;
							$tempId="1507161848200235239";
							//$message="The one-time password to register on Xebra is $mob_otp.";
							$message="The one-time OTP to register on Xebra is ".$mob_otp." XEBRA BizTech XEBRAB";
							$msg="The one-time password to register on Xebra is $otp.";
							send_sms($user[0]->reg_mobile,$message,$tempId);

							$from="Xebra Cohorts";
							$subject="Hi! Here’s your OTP.";
							$data['name'] = $user[0]->reg_username;
							$data['otp'] = $otp;
							$content=$this->load->view('email_template/otp',$data,true);
								sendEmail($from,$user[0]->reg_email,$subject,$content);
							
							    
                              exit;


		}

			public function send_login_otp(){
      

                               $post = $this->input->post();
                               $post=$this->security->xss_clean($post);
           $user = $this->Adminmaster_model->selectData('registration', '*', array("reg_id"=>$post['reg_id']));
                              
                                
													
							$reg_otp= mt_rand(100000,999999);
						$reg_mob_otp= mt_rand(100000,999999);
						$this->Adminmaster_model->updateData('registration',array("reg_otp"=>$reg_otp,"reg_mob_otp"=>$reg_mob_otp),array("reg_id"=>$user[0]->reg_id));

						//$message="The one-time password to register on Xebra is $reg_mob_otp.";
						$message="The one-time OTP to register on Xebra is ".$reg_mob_otp." XEBRA BizTech XEBRAB";
						$tempId="1507161848200235239";
						$from="Xebra Cohorts";
						$subject="Hi! Here’s your OTP.";
							$data['name'] = $user[0]->reg_username;
							$data['otp'] = $reg_otp;
							$content=$this->load->view('email_template/otp',$data,true);
							sendEmail($from,$user[0]->reg_email,$subject,$content);

							//send_sms($user[0]->reg_mobile,$message,$tempId);
							    
                              exit;


		}


		public function reactive_user()
		{
			$session_data = $this->session->userdata('temp_user_session');

			$this->session->set_userdata('user_session',$session_data);
			$status = $this->session->userdata('user_session');

			if($status)
			{
				$userid = $this->session->userdata['user_session']['reg_id'];

				$update = array(
					'status'	=>	'Active',
					'reg_otp'	=>	'',
				);

				$update_status = $this->Adminmaster_model->updateData('registration',$update,array("reg_id"=>$userid));

				// $get_client_list = $this->Adminmaster_model->selectData('sales_customers', 'cust_id', array("reg_id"=>$userid));
				// $get_client = json_decode( json_encode($get_client_list), true);
				// print_r($get_client);


				// $update = array(
				// 	'status'	=>	'InActive',
				// 	'cp_otp'	=>	'',
				// );

				// $update_status = $this->Adminmaster_model->updateData('sales_customer_contacts',$update,array("cust_id"=>$get_client));

				$update_clients_status = $this->Adminmaster_model->updateData('registration',$update,array("reg_createdby"=>$userid));

				if($update_status == true)
				{
						echo "success";
				}
				else {
					echo "cancel";
				}

			}else{
				echo "cancel";
			}

			exit();
		}

	function get_client_ip() {
	    $ipaddress = '';
	    if (getenv('HTTP_CLIENT_IP'))
	        $ipaddress = getenv('HTTP_CLIENT_IP');
	    else if(getenv('HTTP_X_FORWARDED_FOR'))
	        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
	    else if(getenv('HTTP_X_FORWARDED'))
	        $ipaddress = getenv('HTTP_X_FORWARDED');
	    else if(getenv('HTTP_FORWARDED_FOR'))
	        $ipaddress = getenv('HTTP_FORWARDED_FOR');
	    else if(getenv('HTTP_FORWARDED'))
	       $ipaddress = getenv('HTTP_FORWARDED');
	    else if(getenv('REMOTE_ADDR'))
	        $ipaddress = getenv('REMOTE_ADDR');
	    else
	        $ipaddress = 'UNKNOWN';
	    return $ipaddress;
	}

	public function login()
	{
		$where = array();
       $_POST=$this->security->xss_clean($_POST);
      

		parse_str($_POST['frm_login'], $where);
		$email = $where['email'];
		$password = $where['password'];

		 if(isset($_POST['g-recaptcha-response'])){
            $recaptcha = $_POST['g-recaptcha-response'];
			$res = $this->reCaptcha($recaptcha);
			if(!$res['success']){
				// Error
				echo "false";
						 exit();
			}
			unset($_POST['g-recaptcha-response']);
            }
	
  $remember=$where;
		$data = array();
		if ($where) {
			$error = array();
			$e_flag=0;
			if(trim($where['email']) == ''){
				$error['userid'] = 'Username cannot be blank.';
				$e_flag=1;
			}
			if(trim($where['password']) == ''){
				$error['password'] = 'Password cannot be blank.';
				$e_flag=1;
			}


			if ($e_flag == 0) {
				$where = array(
								'reg_email'		=>	$email,
								'reg_password'	=>	md5($password),
							);


				$chekUserEmail = $this->Adminmaster_model->selectData('registration', '*', array('reg_email'=>$email));
				if(!$chekUserEmail)
				{
					echo 'InvalidEmail';
					exit();
				}

				$user = $this->Adminmaster_model->selectData('registration', '*', $where);
				

				if (count($user) > 0) {

					$gst = $this->Adminmaster_model->selectData('gst_number', '*', array('bus_id'=>$user[0]->bus_id,'type'=>'business'),'gst_id','ASC');
					$gst_number='';
					if(count($gst)>0){
						$gst_number=$gst[0]->gst_id;
					}



					$array['reg_id'] = $user[0]->reg_id;
					$array['bus_id'] = $user[0]->bus_id;
					$array['access'] = $user[0]->reg_admin_type;
					$array['ip_address'] = $this->get_client_ip();

					$activity_login_id = $this->Adminmaster_model->insertData('login_record',$array);

					/*if($user[0]->reg_istrial==1){
						$now = time();
						$created_date = strtotime($user[0]->createdat);
						$datediff = $now - $created_date;
						if(round($datediff / (60 * 60 * 24))>15){

							$session_data = array(
										'reg_id' => $user[0]->reg_id,
										'bus_id' => $user[0]->bus_id,
										'email'	 => $user[0]->reg_email,
										'mobile'	 => $user[0]->reg_mobile,
										'ei_username' => $user[0]->reg_username,
										'ei_gst' => $gst_number,
										'user_photo'=>$user[0]->reg_profile_image,
										'createdat'	 => $user[0]->createdat,
										'reg_istrial'	 => $user[0]->reg_istrial,
										'reg_admin_type'	 => $user[0]->reg_admin_type,
										'reg_createdby'	 => $user[0]->reg_createdby,
										'reg_trial_expire' => '1',
										'reg_login_id'=> $activity_login_id,
										);
							$this->session->unset_userdata('user_session');
							$this->session->unset_userdata('client_session');
							$this->session->set_userdata('user_session',$session_data);

						echo 'trial_period_expired';
						exit();
						}
					}*/

					if($user[0]->reg_otp_verify == 'No')
					{

						echo json_encode($user[0]->reg_id);
						exit();
					}



					/*if($user[0]->status == 'Inactive')
					{
						$session_data = array(
										'reg_id' => $user[0]->reg_id,
										'bus_id' => $user[0]->bus_id,
										'email'	 => $user[0]->reg_email,
										'mobile'	 => $user[0]->reg_mobile,
										'ei_username' => $user[0]->reg_username,
										'ei_gst' => $gst_number,
										'user_photo'=>$user[0]->reg_profile_image,
										'createdat'	 => $user[0]->createdat,
										'reg_istrial'	 => $user[0]->reg_istrial,
										'reg_admin_type'	 => $user[0]->reg_admin_type,
										'reg_createdby'	 => $user[0]->reg_createdby,
										'reg_login_id'=> $activity_login_id,

										);
						$this->session->unset_userdata('user_session');
						$this->session->unset_userdata('client_session');
						$this->session->set_userdata('temp_user_session',$session_data);
						echo "error_account";
						exit();
					}*/

					$session_data = array(
										'reg_id' => $user[0]->reg_id,
										'bus_id' => $user[0]->bus_id,
										'email'	 => $user[0]->reg_email,
										'mobile'	 => $user[0]->reg_mobile,
										'ei_username' => $user[0]->reg_username,
										'ei_gst' => $gst_number,
										'user_photo'=>$user[0]->reg_profile_image,
										'createdat'	 => $user[0]->createdat,
										'reg_istrial'	 => $user[0]->reg_istrial,
										'reg_admin_type'	 => $user[0]->reg_admin_type,
										'reg_createdby'	 => $user[0]->reg_createdby,
										'reg_login_id'=> $activity_login_id,
										'reg_login_id'=> $activity_login_id,
										'status'=> $user[0]->status,

										);

					$this->session->unset_userdata('user_session');
					$this->session->unset_userdata('client_session');
					$this->session->unset_userdata('vendor_session');
					$this->session->set_userdata('user_session',$session_data);

					if(isset($remember['remember_me']) && $remember['remember_me']=="on"){

		$year = time() + 31536000;
		//setcookie('remember_me_u', $remember['email'], $year);
		//setcookie('remember_me_pass', $remember['password'], $year);

		$my_cookie= array(

       'name'   => 'remember_me_u',
       'value'  => $remember['email'],
       'expire' => '30000000',
       'secure' => TRUE

   );
		$this->input->set_cookie($my_cookie);

		$my_cookie1= array(

       'name'   => 'remember_me_pass',
       'value'  => '',// $remember['password'],
       'expire' => '30000000',
       'secure' => TRUE

   );
		$this->input->set_cookie($my_cookie1);
		}


					$redirectId ="My-Community";
					echo $redirectId;
					exit;
					 // echo "success";
					 // exit();


					}
						echo "error_password";
						exit();

				}
			}
		}


		function findRedirectPath($reg_admin_type,$reg_id,$bus_id)
	    {
	      if($reg_admin_type == 5)
	      {
	        return 5;
	      } elseif($reg_admin_type == 4)
	      {
	        return 4;
	      }
	      elseif($reg_admin_type == 3 || $reg_admin_type == -1)
	      {
	      	$user_data = $this->Adminmaster_model->selectData('registration','reg_username',array("reg_createdby"=>$reg_id));
	      	$bus_data = $this->Adminmaster_model->selectData('businesslist','bus_company_name',array("bus_id"=>$bus_id));
	      	if(!$user_data)
	      	{
	      		return 31;

	      	}elseif (!$bus_data) {

	      		return 32;

	      	}else{

	      		return 33;

	      	}
	      }elseif ($reg_admin_type == 2) {

	      	return 2;

	      }elseif ($reg_admin_type == 1) {

	      	return 1;
	      }
	      else
	      {
	        return base_url().'dashboard';
	      }


	    }


		public function new_index()
		{
			$this->load->view('index/new_index');
		}

		function deactive_my_account()
		{

			if($this->session->userdata('user_session')) {
			$bus_id = $this->session->userdata['user_session']['bus_id'];
			$userid = $this->session->userdata['user_session']['reg_id'];

			$otp=rand(100000,999999);
			$user_data = $this->Adminmaster_model->selectData('registration','*',array("reg_id"=>$userid));
			if(count($user_data)>0){
			if($this->input->post('mobile_no') ==1) {

				$message="Your OTP password for deactivate account in Xebra is:".$otp.". Please do not share with anyone.";
				send_sms($user_data[0]->reg_mobile,$message);

			}

			if($this->input->post('email_add') ==1) {
						//$msg="The one-time password to reset your MyEasyInvoices password is :".$otp;
						//$this->send_sms('91'.$userdata[0]->reg_mobile,$msg);

						//$this->email->set_mailtype("html");
						//$this->email->from('admin@windchimes.co.in', "EasyInvoices");
						//$this->email->to($userdata['emailid']);
						//$this->email->subject("Adieu invoicing nightmares!");
						//$from="admin@windchimes.co.in";
						/*$to=$user_data[0]->reg_email;
						$subject="Deactive Account - MyEasyInvoices";
						$message = "<html><body>";
						$message .= "<p style='text-align:center;'><img src='".base_url()."asset/images/logo.png' alt='Team Invoice Management'>       </p><h3 style='text-align:center;'>Password assistance</h3> ";
						$message .= "<p style='text-align:center;text-transform: uppercase;'>Do not reply to this email. it is auto generated.</p>";
						$message .= "<p>Here's your OTP</p>";
						$message .= "<h4>".$otp."</h4>";
						$message .= "<p>To deactive your account, we will need you to verify your account. It's really simple.</p>";
						$message .= "<p>Simply enter ".$otp." as the OTP in the My Easy Invoices page and you will have complete access to your account.</p>";
						$message .= "<h5>Happy Invoicing to you!</h5></body></html>";*/
						//$this->email->message($message);
						//$this->email->send();
						$from="Xebra cohorts";
						$to=$user_data[0]->reg_email;
						$msg="deactive your account";
						$data['name']=ucwords($user_data[0]->reg_username);
						$data['otp']=$otp;
						$data['msg']=$msg;
						$subject="Hi! Here’s your OTP.";
						$message=$this->load->view('email_template/otp',$data,true);
						sendEmail($from,$to,$subject,$message);


			}

			$update = array(
				'reg_otp'	=>	$otp,
			);
			/*$update = array(
				'status'	=>	'Inactive',
			);*/
			$update_status = $this->Adminmaster_model->updateData('registration',$update,array("reg_id"=>$userid,"bus_id"=>$bus_id));
			if($update_status != false)
			{
					//$sess_array = $this->session->sess_destroy();
					echo true;
			}
			else {
				echo false;
			}
		}else{
			echo false;
		}
		 }else{
			echo false;
		}
		}
		function match_acc_otp()
		{
			$otp_array = array();
			 $_POST=$this->security->xss_clean($_POST);
			parse_str($_POST['frm'],$otp_array);
			if($this->session->userdata('user_session')) {
				$bus_id = $this->session->userdata['user_session']['bus_id'];
				$userid = $this->session->userdata['user_session']['reg_id'];
				$user_data = $this->Adminmaster_model->selectData('registration','*',array("reg_id"=>$userid,"bus_id"=>$bus_id));

				$otp=intval(trim($otp_array['acc_otp'],' '));
				if($user_data[0]->reg_otp==$otp){
				$update = array(
					'status'	=>	'Inactive',
					'reg_otp'	=>	'',
				);

				$update_status = $this->Adminmaster_model->updateData('registration',$update,array("reg_id"=>$userid,"bus_id"=>$bus_id));

				$update_clients_status = $this->Adminmaster_model->updateData('registration',$update,array("reg_createdby"=>$userid));
				if($update_status != false)
				{
						$sess_array = $this->session->sess_destroy();
						echo json_encode(true);
				}
				else {
					echo json_encode(false);
				}
			 }else{
			 	echo json_encode(false);
			 }
			}else{
				echo json_encode(false);
			}

		}
		public function change_password()
		{
			$change_password = array();
			 $_POST=$this->security->xss_clean($_POST);
			parse_str($_POST['frm'], $change_password);
			$csrf_token=$this->security->get_csrf_hash();
			$old_password = $change_password['old_password'];
			$bus_id = $this->session->userdata['user_session']['bus_id'];
			$reg_id = $this->session->userdata['user_session']['reg_id'];

			$filter = array(
				'reg_id'		=>	$reg_id,
				'bus_id'		=>	$bus_id,
				'reg_password'	=>	md5($old_password),
			);
			$userdata = $this->Adminmaster_model->selectData('registration','*',$filter);

			if(count($userdata)>0)
			{
				$otp=rand(100000,999999);
				// $otp_array = array(
				// 	'reg_otp' => $otp,
				// );
				$array_password = array(
					'reg_id'		=>	$reg_id,
					'reg_otp'		=>	$otp,
					'reg_password'	=>	md5($change_password['new_password']),
					'reg_otp_verify'=> "No",
				);
				//$this->session->unset_userdata('change_password_info');
				//$this->session->set_userdata('change_password_info',$array_password);

				$insertinfo = $this->Adminmaster_model->updateData('registration', $array_password,array('reg_id'=>$reg_id));
				if ($insertinfo!=false) {
					//echo json_encode(true);
					echo "TRUE"."/".$csrf_token;
				}else{
					//echo json_encode(false);
					echo "FALSE"."/".$csrf_token;
				}


			}else
			{
				//echo json_encode(false);
				echo "FALSE"."/".$csrf_token;
			}
		}
		function sent_change_pwdotp()
		{

			if($this->session->userdata('user_session')) {
			$bus_id = $this->session->userdata['user_session']['bus_id'];
			$userid = $this->session->userdata['user_session']['reg_id'];

			$userdata = $this->Adminmaster_model->selectData('registration','*',array("reg_id"=>$userid,"bus_id"=>$bus_id));
			$otp = $this->session->userdata['change_password_info']['reg_otp'];

			if(count($userdata)>0){

			if($this->input->post('mobile_no') ==1) {
				$msg="The OTP password to reset your Xebra account password is :".$otp;
				send_sms($userdata[0]->reg_mobile,$msg);

						//$this->send_sms('91'.$userdata[0]->reg_mobile,$msg);
			}
			if($this->input->post('email_add') ==1) {
						//$msg="The one-time password to reset your MyEasyInvoices password is :".$otp;
						//$this->send_sms('91'.$userdata[0]->reg_mobile,$msg);

						//$this->email->set_mailtype("html");
						//$this->email->from('admin@windchimes.co.in', "EasyInvoices");
						//$this->email->to($userdata['emailid']);
						//$this->email->subject("Adieu invoicing nightmares!");
						//$from="admin@windchimes.co.in";
						/*$to=$userdata[0]->reg_email;
						$subject="Change Password - MyEasyInvoices";
						$message = "<html><body>";
						$message .= "<p style='text-align:center;'><img src='".base_url()."asset/images/logo.png' alt='Team Invoice Management'>       </p><h3 style='text-align:center;'>Password assistance</h3> ";
						$message .= "<p style='text-align:center;text-transform: uppercase;'>Do not reply to this email. it is auto generated.</p>";
						$message .= "<p>Here's your OTP</p>";
						$message .= "<h4>".$otp."</h4>";
						$message .= "<p>To change your password, we will need you to verify your account. It's really simple.</p>";
						$message .= "<p>Simply enter ".$otp." as the OTP in the My Easy Invoices page and you will have complete access to your account.</p>";
						$message .= "<h5>Happy Invoicing to you!</h5></body></html>";*/
						$to=$userdata[0]->reg_email;
						$from="Xebra cohorts";
						$msg="change your account password";
						$data['name']=ucwords($userdata[0]->reg_username);
						$data['otp']=$otp;
						$data['msg']=$msg;
						$subject="Hi! Here’s your OTP.";
						$message=$this->load->view('email_template/otp',$data,true);
						//$this->email->message($message);
						//$this->email->send();
						sendEmail($from,$to,$subject,$message);


			}
			/*$update_status = $this->Adminmaster_model->updateData('registration',$update,array("reg_id"=>$userid,"bus_id"=>$bus_id));
			if($update_status != false)
			{
					//$sess_array = $this->session->sess_destroy();
					echo true;
			}
			else {
				echo false;
			}*/
			echo json_encode(true);
		 }else{
			echo json_encode(false);
		}
		}else{
			echo json_encode(false);
		}
		}
		public function check_otp_for_password()
		{
			$otp_array = array();
			 $_POST=$this->security->xss_clean($_POST);
			parse_str($_POST['frm'], $otp_array);
			if($this->session->userdata('user_session')) {
				$bus_id = $this->session->userdata['user_session']['bus_id'];
				$userid = $this->session->userdata['user_session']['reg_id'];
				$ei_username = $this->session->userdata['user_session']['ei_username'];


				$otp=intval(trim($otp_array['acc_pwdotp'],' '));

				//$check = $otp_array['password_otp'];

				$id=$this->session->userdata['change_password_info']['reg_id'];
				$set_password = $this->session->userdata['change_password_info']['reg_password'];
				//$reg_otp = $this->session->userdata['change_password_info']['reg_otp'];
				$match = $this->Adminmaster_model->selectData('registration','*',array("reg_id"=>$id,"reg_otp"=>$otp));
				//$match = $this->Clientmodel->selectData('registration','*',array("reg_id"=>$userid,"bus_id"=>$bus_id));
				if(count($match)==1){
					$update_status = $this->Adminmaster_model->updateData('registration',array('reg_password'=>md5($set_password),'reg_otp'=>''),array("reg_id"=>$userid,"bus_id"=>$bus_id));
					$this->session->unset_userdata('change_password_info');
					$to=$this->session->userdata['user_session']['email'];
					$data['password']=$set_password;
					$data['name']=ucfirst($this->session->userdata['user_session']['ei_username']);
					$subject="Here’s your new password";
					$message=$this->load->view('email_template/change-password',$data,true);
					$from="Xebra cohorts";
					sendEmail($from,$to,$subject,$message);
					echo json_encode(true);
				}
				else
				{
					echo json_encode(false);
				}
			}else{
					echo json_encode(false);
				}
		}
		public function feedback()
		{
			$feedbck = array();
			 $_POST=$this->security->xss_clean($_POST);
			parse_str($_POST['frm'], $feedbck);
			$csrf_token=$this->security->get_csrf_hash();
			if($this->session->userdata('user_session')) {
				$bus_id = $this->session->userdata['user_session']['bus_id'];
				$reg_id = $this->session->userdata['user_session']['reg_id'];


				$data = array(
							'reg_id' => $reg_id,
							'bus_id' => $bus_id,
							'rate'	 => $feedbck['rate'],
							'feedback' => $feedbck['feedback'],
							);
				$update_status = $this->Adminmaster_model->insertData('feedback',$data);

				if($update_status!=false){
					$to=$this->session->userdata['user_session']['email'];
					$data['name']=ucfirst($this->session->userdata['user_session']['ei_username']);
					$subject="We have received your feedback";
					$message=$this->load->view('email_template/feedback',$data,true);
					$from="Xebra cohorts";
					sendEmail($from,$to,$subject,$message);
                 //////////////////////////////////////////////
                $tofb="feedback@xebra.in";
					$data['name']=ucfirst($this->session->userdata['user_session']['ei_username']);
					$subjectfb="Feedback from Xebra user"." ".ucfirst($this->session->userdata['user_session']['ei_username']);
					$messagefb=$this->load->view('email_template/feedback_client',$data,true);
					sendEmail($from,$tofb,$subjectfb,$messagefb);
				///////////////////////////////////////////////////////


					//echo json_encode(true);
					echo 'true'."@".$csrf_token;
				}
				else
				{
					//echo json_encode(false);
					echo 'false'."@".$csrf_token;
				}
			}else{
					//echo json_encode(false);
					echo 'false'."@".$csrf_token;
				}
		}

		function match_signup_otp()
		{
			$post = array();
			 $_POST=$this->security->xss_clean($_POST);

			 $csrf_token=$this->security->get_csrf_hash();

			parse_str($_POST['frm_otp'], $post);

			if($post){
				unset($post['submit']);
				$get_otp= $this->Adminmaster_model->selectData('registration','*',array("reg_id"=>$post['reg_id']));
				if($post['phone_otp'] == $get_otp[0]->reg_otp)
				{
					$this->Adminmaster_model->updateData('registration',array("reg_otp_verify"=>"Yes","status"=>"Active"),array("reg_id"=>$post['reg_id']));
					$gst = $this->Adminmaster_model->selectData('gst_number', '*', array('bus_id'=>$get_otp[0]->bus_id,'type'=>'business'),'gst_id','ASC');
					$gst_number='';
					if(count($gst)>0){
						$gst_number=$gst[0]->gst_id;
					}

					 $log = $this->Adminmaster_model->selectData('login_record', '*', array('bus_id'=>$get_otp[0]->bus_id,'reg_id'=>$post['reg_id']));

                 if(count($log) < 1){
					////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                       $get_admin_id= $get_otp[0]->reg_id;
                        $from="Xebra Cohorts";
							    $dataa['user_name'] =  $get_otp[0]->reg_username;
							$dataa['user_start_date'] = date("d M y");
							$dataa['user_end_date'] = Date('d M y', strtotime("+14 days"));
								//$htmldataa = $this->load->view('email_template/trial_period_begins',$dataa,true);
							$htmlWelcome = $this->load->view('email_template/welcome-user-email',$dataa,true);

								$htmlIntro = $this->load->view('email_template/welcome-user-email',$dataa,true);
						
						//	sendEmail($from,$get_otp[0]->reg_email,'Trial Period Begins',$htmldataa);
							sendEmail($from,$get_otp[0]->reg_email,'Welcome to Xebra Cohorts',$htmlWelcome);
								sendEmail($from,$get_otp[0]->reg_email,'Intro Of Xebra Cohorts',$htmlIntro);
							
							  //////////////////////////////////////////////////add subscription/////////////////////////////
              
						if($get_otp[0]->reg_admin_type==3 && $get_otp[0]->reg_createdby==0){

							if($get_otp[0]->reg_plan=="Walk"){


								      	if (date('m') > 3) {

								$year = date('Y')."-".(date('y') +1);





								}

								else {

								$year = (date('Y')-1)."-".date('y');



								}      

								$bus_info_xebra=$this->Subscription_model->selectData("registration","*", array('reg_email' =>"billdesk@xebra.in"));
								$reg_id = $bus_info_xebra[0]->reg_id;
								$bus_id = $bus_info_xebra[0]->bus_id;
								$inv_no=$this->Subscription_model->selectData('sales_invoices', '*', array('bus_id'=>$bus_id,'inv_document_type'=>'Sales Invoice','RIGHT(inv_invoice_no,7)'=>$year),'inv_id','DESC'); 


								if(count($inv_no)>0){
								$last_inv_no=$inv_no[0]->inv_invoice_no; 
								$whatIWant = substr($last_inv_no, strpos($last_inv_no, "/") + 1); 
								$invNo=str_replace('/'.$whatIWant,"",$last_inv_no);
								$temp=str_replace("INV","",$invNo);
								//if($inv_no[0]->document_type=='2'){
								$invno_ref='1'; 
								$temp=intval($temp) + 1;
								//$temp = substr($last_inv_no, 3, -7);
								//$temp= str_replace('INV', '', $last_inv_no); 
								$invno_ref=str_pad($temp, 0, 0, STR_PAD_LEFT);
								//}
								} 
								else
								{
								$invno_ref=str_pad(1, 0, 0, STR_PAD_LEFT);


								}



							 $plan_config = $this->Adminmaster_model->selectData('site_config', '*', array('plan_name'=>'Walk'));


						$name=explode(' ',$get_otp[0]->reg_username);
                            if(isset($name[1]) && $name[1]!=""){;
                            	$last_name	=$name[1];
                            }else{
                            	$last_name	="";
                            }		

       $user_info=array(
           'user_first_name'=>$name[0],
           'user_last_name'=>$last_name,
           'user_email_id'=> $get_otp[0]->reg_email,
           'user_mobile'=>$get_otp[0]->reg_mobile
       );

				$subscription['reg_id']= $get_admin_id;
            	$subscription['bus_id']= 0;
				$subscription['invoice_no'] = 'INV'.$invno_ref.'/'.$year;
				$subscription['subscription_type'] = "First Time";

			$subscription['billing_info'] = json_encode($user_info);
			$subscription['billing_country'] ='';
			$subscription['billing_state'] ='';


			//$data['invoice_no'] = 'INV001';
			$subscription['name'] = $get_otp[0]->reg_username;
			$subscription['email'] = $get_otp[0]->reg_email;
			$subscription['subscription_plan'] =$get_otp[0]->reg_plan;
			$subscription['plan_amount'] = 0;
			$subscription['amount'] = 0;
			$subscription['smscount'] = $plan_config[0]->sms;
			$subscription['storage'] =$plan_config[0]->doc;
				$subscription['client'] =$plan_config[0]->client;
					$subscription['vendor'] =$plan_config[0]->vendor;
						$subscription['employee'] =$plan_config[0]->employee;
							$subscription['gstin'] =$plan_config[0]->gstin;
								$subscription['plan_smscount'] =$plan_config[0]->sms;
									$subscription['plan_storage'] =$plan_config[0]->doc;
										$subscription['plan_client'] =$plan_config[0]->client;
											$subscription['plan_vendor'] =$plan_config[0]->vendor;
												$subscription['plan_employee'] =$plan_config[0]->employee;
													$subscription['plan_gstin'] =$plan_config[0]->gstin;
			
			
				$validity = 365;			
			$subscription['validity'] = $validity;
			$subscription['payment_mode'] = "Free";
			$subscription['payment_status'] = "Completed";
			$subscription['payment_id'] = "";
			$subscription['payment_details'] = "";
			$subscription['cgst'] = 0;
			$subscription['sgst'] =0;
			$subscription['igst'] = 0;
			$subscription['promocode'] = "";
			$subscription['referalcode'] ="";
			$subscription['status'] = 'Active';

			$insert_subscription = $this->Adminmaster_model->insertData('subscription', $subscription);
      }

               
		
				///////////////////////////////////////////////////////////////////////////////////////////////

                            $name=explode(' ',$get_otp[0]->reg_username);
                            if(isset($name[1]) && $name[1]!=""){;
                            	$last_name	=$name[1];
                            }else{
                            	$last_name	="";
                            }
							 $emp_data=array(


                  	'reg_id'=>$get_admin_id,
                  	//'bus_id'=>$register_data['bus_id'],
                  	'status'=>'Active',
                  	'first_name'=>$name[0],
                  	'last_name'=>$last_name,
                  	'employee_email_id'=>$get_otp[0]->reg_email,

                  	'employee_mobile_no'=>$get_otp[0]->reg_mobile,
                  //	'designation'=>$register_data['reg_designation'],
                  	'portal_access'=>1,
                  	'reg_admin_type'=>3,



                  );

                  $get_employee_id = $this->Adminmaster_model->insertData('employee_master',$emp_data);

                   }


               }
					/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

					// Activity Tracking

					$array['reg_id'] = $get_otp[0]->reg_id;
					$array['bus_id'] = $get_otp[0]->bus_id;
					$array['access'] = $get_otp[0]->reg_admin_type;
					$array['ip_address'] = $this->get_client_ip();

					$activity_login_id = $this->Adminmaster_model->insertData('login_record',$array);

					$session_data = array(
										'reg_id' => $get_otp[0]->reg_id,
										'bus_id' => $get_otp[0]->bus_id,
										'email'	 => $get_otp[0]->reg_email,
										'ei_username' => $get_otp[0]->reg_username,
										'ei_gst' => $gst_number,
										'user_photo'=>$get_otp[0]->reg_profile_image,
										'reg_admin_type'	 => $get_otp[0]->reg_admin_type,
										'createdat'	 => $get_otp[0]->createdat,
										'reg_istrial'	 => $get_otp[0]->reg_istrial,
										'reg_createdby'	 => $get_otp[0]->reg_createdby,
										'reg_login_id'=> $activity_login_id,

										);

					$this->session->unset_userdata('user_session');
					$this->session->unset_userdata('client_session');
					$this->session->set_userdata('user_session',$session_data);
				echo 'true'."@".$csrf_token;
				exit();
				}
				else
				{
					//echo false;
					echo 'false'."@".$csrf_token;
				exit();
				}
			}
			else
			{
				//echo false;
				echo 'false'."@".$csrf_token;
				exit();
			}


	}

	public function forgot_password()
	{

		$check_email=array();
      $_POST=$this->security->xss_clean($_POST);
		parse_str($_POST['frm'], $check_email);

		$emailid = $check_email['get_email'];

        $registration = $this->Adminmaster_model->selectData('registration','*',array("reg_email"=>$emailid));

        if(count($registration)>0)
        {
        	$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		    $charactersLength = strlen($characters);
		    $randomString = '';
		    for ($i = 0; $i < 12 ; $i++) {
		        $randomString .= $characters[rand(0, $charactersLength - 1)];
		    }

			$this->session->set_userdata('forgot_email',$emailid);
			// $get_web_token=rand(100000,999999);
			// $get_mob_token=rand(100000,999999);

				$reset_array = array(
					'forget_pwd_token' => $randomString,

				);

			$insertotp = $this->Adminmaster_model->updateData('registration',$reset_array,array('reg_email'=>$emailid));
			// $span = ucwords(@$registration[0]->reg_username);

	        if($insertotp!=false){



	           		$msg=base_url().'index/changemypassword?mytoken='. $randomString.'&email='.$emailid;
						//send_sms($registration[0]->reg_mobile,$msg);

	          			$to=$emailid;
	          			$data['name']=ucwords($registration[0]->reg_username);
						//$msg="Change your account password";
                       $from="Xebra Cohorts";
						$data['msg']=$msg;
						$subject="Trouble logging in? Reset your password";
						$message=$this->load->view('email_template/forget_link',$data,true);
						sendEmail($from,$to,$subject,$message);


	       }

            echo json_encode(TRUE);

		}

		else

		{

			echo json_encode(FALSE);

		}

	}

	public function changemypassword(){

		$this->session->userdata('forgot_email');
		if($_GET['email'])
		{
			if($_GET['mytoken'])
			{
				$flag=$this->Adminmaster_model->selectData('registration','*',array('reg_email'=>$_GET['email'],'forget_pwd_token'=>$_GET['mytoken']));
				if($flag)
				{
					// $("#reset_password_model").modal('show');

					$data['email_ei'] = $_GET['email'];
					$data['token_ei'] = $_GET['mytoken'];
					$this->load->view('index/email-change-password',$data);

				}else{

					redirect('');
				}
			}
		}

	}





	public function update_user_password()
	{
		echo "fsdf";
		exit;

		$get_data=array();
      $_POST=$this->security->xss_clean($_POST);
		parse_str($_POST['frm'], $get_data);

		$emailid = $get_data['email'];

		$token = $get_data['token'];

        $registration = $this->Adminmaster_model->selectData('registration','*',array("reg_email"=>$emailid,'forget_pwd_token'=>$token));

        if(count($registration)>0)
        {

				$reset_array = array(
					'reg_password' => md5($get_data['new_password']),

				);

			$insertotp = $this->Adminmaster_model->updateData('registration',$reset_array,array('reg_email'=>$emailid,'forget_pwd_token'=>$token));
			// $span = ucwords(@$registration[0]->reg_username);

	        if($insertotp!=false){

	       }
            echo json_encode(TRUE);
		}
		else
		{
			echo json_encode(FALSE);
		}

	}

	public function match_verification_code()
	{

			if($this->session->userdata('forgot_email')!='')
			{

				$check_code=array();
          $_POST=$this->security->xss_clean($_POST);
				parse_str($_POST['frm3'], $check_code);

				$var_code = $check_code['verification_code'];

				$mbl_code = $check_code['mbl_code'];


				$flag=$this->Adminmaster_model->selectData('registration','*',array('reg_email'=>$this->session->userdata('forgot_email'),'reg_otp'=>$var_code,'reg_mob_otp'=>$mbl_code));

				if(count($flag) == 1)

				{
					$flag=$this->Adminmaster_model->updateData('registration',array('reg_otp_verify'=>'Yes',"status"=>"Active"),array('reg_email'=>$this->session->userdata('forgot_email')));
					echo json_encode(TRUE);

				}

				else

				{

					echo json_encode(FALSE);

				}

			}

	}

	public function reset_password() {

	$check_email=array();

		
    $_POST=$this->security->xss_clean($_POST);   
    
		parse_str($_POST['frm4'], $check_email);

		if(isset($check_email['g-recaptcha-response'])){
            $recaptcha = $check_email['g-recaptcha-response'];
			$res = $this->reCaptcha($recaptcha);
			if(!$res['success']){
				// Error
				echo json_encode(FALSE);
						 exit();
			}
			unset($check_email['g-recaptcha-response']);
            }  

		$emailid = $check_email['email'];

		$token = $check_email['token'];
 
        $registration = $this->Adminmaster_model->selectData('registration','*',array("reg_email"=>$emailid,'forget_pwd_token'=>$token));



        if(count($registration)>0)
        {
        	$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		    $charactersLength = strlen($characters);
		    $randomString = '';
		    for ($i = 0; $i < 12 ; $i++) {
		        $randomString .= $characters[rand(0, $charactersLength - 1)];
		    }

			$this->session->set_userdata('forgot_email',$emailid);
			// $get_web_token=rand(100000,999999);
			// $get_mob_token=rand(100000,999999);

			$array_password = array(
					
					'reg_password'	=>	md5($check_email['new_password']),
					
				);

				$reset_array = array(
					'forget_pwd_token' => $randomString,

				);

			$insertotp = $this->Adminmaster_model->updateData('registration',$array_password,array('reg_email'=>$emailid));
			// $span = ucwords(@$registration[0]->reg_username);

	      /*  if($insertotp!=false){



	           		$msg=base_url().'index/changemypassword?mytoken='. $randomString.'&email='.$emailid;
						//send_sms($registration[0]->reg_mobile,$msg);

	          			$to=$emailid;
	          			$data['name']=ucwords($registration[0]->reg_username);

						//$msg="Change your account password";
                        $from="Xebra";
						$data['msg']=$msg;
						$subject="Trouble logging in? Reset your password";
						$message=$this->load->view('email_template/forget_link',$data,true);
						sendEmail($from,$to,$subject,$message);


	       }*/

           

            echo json_encode(TRUE);

		}

		else

		{

			echo json_encode(FALSE);

		}

	}

	public function view_gift(){
		$this->load->view('index/activate-gift');
	}

	public function signup(){
		$this->load->view('index/cohorts_signup');
	}

	public function resend_email_otp() {

	$post = $this->input->post();
	$post=$this->security->xss_clean($post);

		//$reg_email = $post['resend_email'];
		//$reg_password = $post['resend_password'];

		//$reg_email = 'vanlal@windchimes.co.in';
		//$reg_password = 'c8bdfd41d406005533290dff3b026c61';

		$user = $this->Adminmaster_model->selectData('registration', '*', array('reg_id'=>$post['reg_id_otp']));
		

		$array['reg_id'] = $user[0]->reg_id;
		$array['bus_id'] = $user[0]->bus_id;
		$array['access'] = $user[0]->reg_admin_type;
		$array['ip_address'] = $this->get_client_ip();

	//	$activity_login_id = $this->Adminmaster_model->insertData('login_record',$array);

		//if($user[0]->reg_istrial==1){
			$reg_otp= mt_rand(100000,999999);
			$reg_mob_otp= mt_rand(100000,999999);
			$this->Adminmaster_model->updateData('registration',array("reg_otp"=>$reg_otp),array("reg_id"=>$user[0]->reg_id));
			//print $this->db->last_query(); exit();

			$message="The one-time password to register on Xebra Cohorts is $reg_mob_otp.";
			$from="Xebra Cohorts";
			$subject="Hi! Here’s your OTP.";
			$data['name'] = $user[0]->reg_username;
			$data['otp'] = $reg_otp;
			$content=$this->load->view('email_template/otp',$data,true);
			sendEmail($from,$user[0]->reg_email,$subject,$content);

			echo json_encode($user);
			exit();
		//}
		
	}

	public function resend_mob_otp() {

		$post = $this->input->post();
		$post=$this->security->xss_clean($post);

		//$reg_email = $post['resend_email'];
		//$reg_password = $post['resend_password'];

		//$reg_email = 'vanlal@windchimes.co.in';
		//$reg_password = 'c8bdfd41d406005533290dff3b026c61';

		$user = $this->Adminmaster_model->selectData('registration', '*', array('reg_id'=>$post['reg_id_otp']));
		

		$array['reg_id'] = $user[0]->reg_id;
		$array['bus_id'] = $user[0]->bus_id;
		$array['access'] = $user[0]->reg_admin_type;
		$array['ip_address'] = $this->get_client_ip();

	//	$activity_login_id = $this->Adminmaster_model->insertData('login_record',$array);

		//if($user[0]->reg_istrial==1){
			$reg_otp= mt_rand(100000,999999);
			$reg_mob_otp= mt_rand(100000,999999);
			$this->Adminmaster_model->updateData('registration',array("reg_otp"=>$reg_otp),array("reg_id"=>$user[0]->reg_id));
			//print $this->db->last_query(); exit();

			$message="The one-time password to register on Xebra Cohorts is $reg_mob_otp.";
			$from="Xebra";
			$subject="Hi! Here’s your OTP.";
			$data['name'] = $user[0]->reg_username;
			$data['otp'] = $reg_otp;
			$content=$this->load->view('email_template/otp',$data,true);
			sendEmail($from,$user[0]->reg_email,$subject,$content);

			echo json_encode($user);
			exit();
		//}
	}

	public function send_mob_otp(){
		

		$post = $this->input->post();
		$post=$this->security->xss_clean($post);
        $userid= $this->user_session['reg_id'];
		//$reg_email = $post['resend_email_mobile'];
		//$reg_password = $post['resend_password_mobile'];

		//$reg_email = 'vanlal@windchimes.co.in';
		//$reg_password = 'c8bdfd41d406005533290dff3b026c61';

		$user = $this->Adminmaster_model->selectData('registration', '*', array('reg_id'=>$post['reg_id_otp']));
		
		/*$array['reg_id'] = $user[0]->reg_id;
		$array['bus_id'] = $user[0]->bus_id;
		$array['access'] = $user[0]->reg_admin_type;
		$array['ip_address'] = $this->get_client_ip();

		$activity_login_id = $this->Adminmaster_model->insertData('login_record',$array);*/

		//if($user[0]->reg_istrial==1){
		$reg_otp= mt_rand(100000,999999);
			$reg_mob_otp= mt_rand(100000,999999);
			$this->Adminmaster_model->updateData('registration',array("reg_mob_otp"=>$reg_mob_otp),array("reg_id"=>$user[0]->reg_id));
			$tempId="1507161848200235239";
			$message="The one-time password to register on Xebra Cohorts is $reg_mob_otp.";
			send_sms($user[0]->reg_mobile,$message,$tempId);
			
			echo json_encode($user);
			exit();
		//}
	}

	public function verify_mob_otp(){
		
		$post = $this->input->post();
		$post=$this->security->xss_clean($post);
		$mbl_code=$post['otp'];
         $userid= $this->user_session['reg_id'];
		$user=$this->Adminmaster_model->selectData('registration','*',array('reg_id'=>$userid,'reg_mob_otp'=>$mbl_code));

				if(count($user) > 0 )

				{

					$flag=$this->Adminmaster_model->updateData('registration',array('reg_otp_verify'=>'Yes','status'=>'Active'),array('reg_id'=>$userid));

					$gst = $this->Adminmaster_model->selectData('gst_number', '*', array('bus_id'=>$user[0]->bus_id,'type'=>'business'),'gst_id','ASC');
					$gst_number='';
					if(count($gst)>0){
						$gst_number=$gst[0]->gst_id;
					}

						$array['reg_id'] = $user[0]->reg_id;
					$array['bus_id'] = $user[0]->bus_id;
					$array['access'] = $user[0]->reg_admin_type;
					$array['ip_address'] = $this->get_client_ip();

					$activity_login_id = $this->Adminmaster_model->insertData('login_record',$array);


					$session_data = array(
										'reg_id' => $user[0]->reg_id,
										'bus_id' => $user[0]->bus_id,
										'email'	 => $user[0]->reg_email,
										'mobile'	 => $user[0]->reg_mobile,
										'ei_username' => $user[0]->reg_username,
										'ei_gst' => $gst_number,
										'user_photo'=>$user[0]->reg_profile_image,
										'createdat'	 => $user[0]->createdat,
										'reg_istrial'	 => $user[0]->reg_istrial,
										'reg_admin_type'	 => $user[0]->reg_admin_type,
										'reg_createdby'	 => $user[0]->reg_createdby,
										'reg_login_id'=> $activity_login_id,
										'reg_login_id'=> $activity_login_id,
										'status'=> $user[0]->status,

										);

					$this->session->unset_userdata('user_session');
									$this->session->set_userdata('user_session',$session_data);
					echo json_encode(TRUE);exit;

				}

				else

				{

					echo json_encode(FALSE);exit;

				}
	}

	public function verify_registration(){
		
		$post = $this->input->post();
		$post=$this->security->xss_clean($post);
		
         $userid= $this->user_session['reg_id'];

		$user=$this->Adminmaster_model->selectData('registration','*',"`reg_id`=".$userid." AND `status`='Active' AND( `reg_token`='Xebra Cohorts' OR  `reg_token`='Xebra' )");

				if(count($user) > 0 )

				{

					echo json_encode(TRUE);exit;

				}

				else

				{

					echo json_encode(FALSE);exit;

				}
	}
		public function insert_event(){
        
                 $data=$this->input->post();
                 $data=$this->security->xss_clean($data);
                 
              $get_event_id = $this->Adminmaster_model->insertData('events',$data);

                if($_FILES['upload']['name'] != '')
                {
                    $user_image=$this->Common_model->upload_org_filename('upload','event_image/'.$get_event_id ,$allowd="jpeg|jpg|png",array('width'=>200,'height'=>300));
                    if($user_image!=false){
                        
                        $get_update_id = $this->Adminmaster_model->updateData('events',array('event_image'=>$_FILES['upload']['name']),array('id'=>$get_event_id ));
                       
                     
                    }
                }

                exit;

	}

	public function insert_offer(){
        
                 $data=$this->input->post();
                 $data=$this->security->xss_clean($data);
                 
              $get_offer_id = $this->Adminmaster_model->insertData('offers',$data);

                if($_FILES['upload']['name'] != '')
                {
                    $user_image=$this->Common_model->upload_org_filename('upload','offer_image/'.$get_offer_id ,$allowd="jpeg|jpg|png",array('width'=>200,'height'=>300));
                    if($user_image!=false){
                        
                        $get_update_id = $this->Adminmaster_model->updateData('offers',array('offer_image'=>$_FILES['upload']['name']),array('id'=>$get_offer_id ));
                       
                     
                    }
                }

                exit;

	}


	public function company($url){
		$business=$this->Profile_model->selectData("businesslist","*",array('profile_url'=>$url));
		 
		  
		   if(count($business)>0){
			  if($business[0]->make_profile_public){
				     	$data['id'] 			= $business[0]->bus_id;
				$res['contact_person']	= $this->Market_place_model->selectData('registration', 'reg_username,reg_email,reg_mobile', array('bus_id'=>$data['id'],'reg_admin_type'=>'6'));
				$res['result']			= $this->Market_place_model->GetUserInfo($data);
				$res['name']			= $this->Community_model->getSearchnew($data);
				$res['branch']			= $this->Market_place_model->GetBranchInfo($data);
				$res['company'] 		= $this->Market_place_model->getCompanyname($data);
				$res['email'] 			= $this->Market_place_model->getRegemail($data);
				$res['contact'] 		= $this->Market_place_model->getRegcontact($data);
				$res['offers'] 			= $this->Community_model->getOffersonprofile($data);
				$res['connections'] 	= $this->Community_model->getconnectiontype($data);
					 
				  $this->load->view('my-community/manage-profile_clone',$res);
			  } else{
				 echo "This Profile is not public"; 
			  }
		   }else{
			   echo "There is no profile found";
		   }
		}

		public function blog_subscribe(){

		$data=$this->input->post();
		 $data=$this->security->xss_clean($data);


		if($data){

		   
		   $insert_subscribe=$this->Adminmaster_model->insertData("blog_subscribe",$data);

			$subject="Blog subscribe Request From ".$data['name'];

  //$to ="";
			
			$to ="info@xebra.in";




			$from =$data['name'];


			$message= '
            <h2>Blog Subscription Request Submitted</h2>
            <p><b>Name: </b>'.$data['name'].'</p>
            <p><b>Email: </b>'.$data['email'].'</p>
          
        ';
	
			sendEmail($from ,$to,$subject,$message);	
			$this->session->set_flashdata('success',"You have successfully subscribed to Xebra Newsletter");
			//redirect('demo');
		}
		
		redirect('blog');
		
	}

	function reCaptcha($recaptcha){
		//$secret = "6Ld_6L8ZAAAAAHFU42COviOd1xTky3IIPsdsMKvl";
		$secret = recaptcha_secretekey;
		$ip = $_SERVER['REMOTE_ADDR'];

		$postvars = array("secret"=>$secret, "response"=>$recaptcha, "remoteip"=>$ip);
		$url = "https://www.google.com/recaptcha/api/siteverify";
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_TIMEOUT, 10);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postvars);
		$data = curl_exec($ch);
		curl_close($ch);

		return json_decode($data, true);
	}
	
	


}
?>