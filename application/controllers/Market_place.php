<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require ("common/Index_Controller.php");
class Market_place extends Index_Controller {

	function __construct(){

		parent::__construct();

		$this->load->model('Market_place_model');
		//$this->load->model('Sales_model');	
		setlocale(LC_MONETARY, 'en_US.UTF-8'); 

		is_login();
		$this->user_session = $this->session->userdata('user_session');
		is_trialExpire(); 
		is_accessAvail(1);
		 is_BusReg();

	}

	public function index()
	{
		//redirects('business_analytics/customers-revenue');
		//$this->load->view('client_portal/download-soa-doc');
		$res_city_ids	=	$this->Market_place_model->Get_unique_cities();
		for($i=0;$i<count($res_city_ids);$i++)
		{
			$arr[$i] = $res_city_ids[$i]['bus_billing_city'];
		}
		$string = implode(",",$arr);

		$res['cities'] = $this->Market_place_model->Get_cities($string);
		$this->load->view('marketplace/market-search-company',$res);
	}
	
	public function search_company()
	{	
		//echo "Search Page"; 
		//$res_city_ids	=	$this->Market_place_model->selectData('countries', '*','','country_id','ASC');
		$res_city_ids	=	$this->Market_place_model->Get_unique_cities();
		for($i=0;$i<count($res_city_ids);$i++)
		{
			$arr[$i] = $res_city_ids[$i]['bus_billing_city'];
		}
		$string = implode(",",$arr);

		$res['cities'] = $this->Market_place_model->Get_cities($string);

		//print_r($res['cities']);exit;

		$this->load->view('marketplace/market-search-company',$res);
	}
	
	public function search_company_result()
	{	
		$data = $_GET;

		if($_GET['search'])
		{
			$data['search'] = $_GET['search'];
			//$res['countries']	=	$this->Market_place_model->selectData('countries', '*','','country_id','ASC');
			$res['result']=$this->Market_place_model->MarketFilter($data);
			$res['post_data']=$_GET;
			

			$res_city_ids	=	$this->Market_place_model->Get_unique_cities();
			for($i=0;$i<count($res_city_ids);$i++)
			{
				$arr[$i] = $res_city_ids[$i]['bus_billing_city'];
			}
			$string = implode(",",$arr);

			$res['cities'] = $this->Market_place_model->Get_cities($string);
			$res['rate'] = $this->Market_place_model->Get_rating($string);
			// print_r($rate['rate']);
			// exit();
		

			$str =  implode(",",$res['post_data']['multi_loc']);
			$res['selected_cities'] = $this->Market_place_model->Get_cities($str);
		
			//print_r($res['post_data']['multi_loc']);exit;
			$this->load->view('marketplace/company-search-result',$res);

		}else{

			$this->load->view('marketplace/market-search-company');
		}
		
	}
	
	public function view_company_details()
	{	
		//echo "View Details Of Company";
		if($_GET['token'])
		{
			$data['id'] = $_GET['token'];
			$res['contact_person']=$this->Market_place_model->selectData('registration', 'reg_username,reg_email,reg_mobile', array('bus_id'=>$data['id'],'reg_admin_type'=>6));
			$res['result']=$this->Market_place_model->GetUserInfo($data);
			$res['branch']=$this->Market_place_model->GetBranchInfo($data);
			$res['rate'] = $this->Market_place_model->Get_rating($data);
			//print_r($res['rate']);
			//exit();
			
			if($res){

				$this->load->view('marketplace/view-company-details',$res);

			}else{

				$this->load->view('marketplace/market-search-company');
			}
			

		}else{

			$this->load->view('marketplace/market-search-company');
		}
		
	}


	public function send_email()
	{
		$_POST=$this->security->xss_clean($_POST);
		$data = $_POST;
		
		$email = $this->Market_place_model->GetEmailIds($data);

		$subject = "Bussiness Lead from Xebra";
		$message = $data['message'];
		for($i=0;$i<count($email);$i++)
		{
			$status = sendEmail($email[$i]['reg_email'],$subject,$message);
		}
		
		if($status)
		{
			echo 1;
		}else{
			echo 0;
		}	
	}

	
	public function review()
		{
			$feedbck = array();
			$_POST=$this->security->xss_clean($_POST);
			parse_str($_POST['frm'], $feedbck);
			if($this->session->userdata('user_session')) {
				$bus_id = $this->session->userdata['user_session']['bus_id'];
				$reg_id = $this->session->userdata['user_session']['reg_id'];
				

				$data = array(	
							'reg_id' => $reg_id,
							'bus_id' => $feedbck['comp_id'],
							'rate'	 => $feedbck['rate'],
							'feedback' => $feedbck['feedback'],	
							'by_whom' => $bus_id,									
							);
				$update_status = $this->Market_place_model->insertData('rating',$data);
				
				if($update_status!=false){
					//$to="feedback@xebra.in";//$this->session->userdata['user_session']['email'];
					//$data['name']=ucfirst($this->session->userdata['user_session']['ei_username']);
					//$subject="We have received your feedback";
					//$message=$this->load->view('email_template/feedback',$data,true);
					//sendEmail($to,$subject,$message);
					echo json_encode(true);
				}
				else
				{
					echo json_encode(false);
				}
			}else{
					echo json_encode(false);
				}
		}


	public function get_cities()
	{
		  $_POST=$this->security->xss_clean($_POST);
		$data = $_POST;
		$states=$this->Market_place_model->Get_states($data);
		for($i=0;$i<count($states);$i++)
		{
			$arr[$i] = $states[$i]['state_id'];
		}
		$string = implode(",",$arr);

		$cities = $this->Market_place_model->Get_cities($string);

		$opt='';
		$opt.="<option value=''>SELECT CITY</option>";
		foreach($cities as $st){
			$opt.="<option value=".$st['city_id']." >".$st['name']."</option>";
		}

		echo $opt;

	}


	public function AjaxDataRefresh(){
       $_POST=$this->security->xss_clean($_POST);
		$data = $_POST;

		if($_POST)
		{
			$data['search'] = $_POST['search'];
			//$res['countries']	=	$this->Market_place_model->selectData('countries', '*','','country_id','ASC');
			$res['result']=$this->Market_place_model->MarketFilter($data);
			$res['post_data']=$_POST;

			$res_city_ids	=	$this->Market_place_model->Get_unique_cities();
			for($i=0;$i<count($res_city_ids);$i++)
			{
				$arr[$i] = $res_city_ids[$i]['bus_billing_city'];
			}
			$string = implode(",",$arr);

			$res['cities'] = $this->Market_place_model->Get_cities($string);

			if(@$res['post_data']['multi_loc'])
			{
				$str =  implode(",",$res['post_data']['multi_loc']);
				$res['selected_cities'] = $this->Market_place_model->Get_cities($str);
			}else{
				$res['selected_cities'] = array();
			}
			
		
			//print_r($res['post_data']['multi_loc']);exit;
			//$this->load->view('marketplace/company-search-result',$res);



			$result = $res['result'];
			if($result)
			{


			$output = '';
			for($i=0;$i<count($result);$i++){

				if($result[$i]["bus_company_logo"] != "")
				{
					$company_logo = '<img src="'.base_url().'public/upload/company_logos/'.$result[$i]['bus_id'].'/'.$result[$i]['bus_company_logo'].'" height="50px" class="logo_style_2" width="50px">';
				}
				else
				{
					$company_logo = "";	
				}

				$output .= '<div class="col l6 m6 s6 white-box" style="height:180px !important; width:48% !important; margin-right:5px; padding:10px !important; margin-top:10px !important;">
					<div class="row" style="margin-bottom:5px !important;">
						<div class="col s6 m6 l8" style="margin-top:5px !important;">
							<label class="comp_name"><b>'.$result[$i]["bus_company_name"].'</b></label><br>
							<label class="nature_buzz">'.$result[$i]["nature_of_bus"].'</label>
						</div>
						<div class="col s6 m6 l4" style="text-align:right">
							 '.$company_logo.'
						</div>	
					</div>
					<div class="row" style="margin-bottom:5px !important;">
						<div class="col s6 m6 l8">
							<div class="col l2 s1 m1" style="margin-left:-15px !important;"><i class="material-icons view-icons">location_on</i></div>
							<div class="col l2 s1 m1"><label>'.$result[$i]["name"].'</label></div>
						</div>
					</div>
					<div class="row" style="margin-bottom:20px !important;">
					
					</div>
					<div class="row" style="margin-bottom:5px !important;">
						<div class="col s6 m6 l6" style="font-size:14px;"> 	
							<a href="'.base_url().'market_place/view_company_details?token='.$result[$i]["bus_id"].'" style="color:#7965E9;" onclick=""><b>VIEW PROFILE</b></a>
						</div>
					</div>
				</div>';

			 }
			}else{
				$output = 'No Result Found';
			}

		}

		echo $output;
		exit;

	}

	public function AjaxDataRefresh_count(){
         $_POST=$this->security->xss_clean($_POST);
		$data = $_POST;
		$output = 0;
		if($_POST)
		{
			$data['search'] = $_POST['search'];
			//$res['countries']	=	$this->Market_place_model->selectData('countries', '*','','country_id','ASC');
			$res['result']=$this->Market_place_model->MarketFilter($data);
			$res['post_data']=$_POST;

			$res_city_ids	=	$this->Market_place_model->Get_unique_cities();
			for($i=0;$i<count($res_city_ids);$i++)
			{
				$arr[$i] = $res_city_ids[$i]['bus_billing_city'];
			}
			$string = implode(",",$arr);

			$res['cities'] = $this->Market_place_model->Get_cities($string);

			if(@$res['post_data']['multi_loc'])
			{
				$str =  implode(",",$res['post_data']['multi_loc']);
				$res['selected_cities'] = $this->Market_place_model->Get_cities($str);
			}else{
				$res['selected_cities'] = array();
			}

			$result = $res['result'];

			$output = count($result);
		}

		echo $output;
		exit;

	}

	
}