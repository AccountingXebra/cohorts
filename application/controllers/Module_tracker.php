<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require ("common/Index_Controller.php");
class Module_trackers extends Index_Controller {

	function __construct(){
		parent::__construct();

		$this->load->model('Module_Tracker_model');

		setlocale(LC_MONETARY, 'en_US.UTF-8'); 

		is_login();
		is_trialExpire();
		is_accessAvail(4);
		 is_BusReg();
		$this->user_session = $this->session->userdata('user_session');

	}
	
	public function index()
	{
		
		redirect('module_tracker/activity_log');
		
	}

	public function activity_log()
	{
		
		$reg_id = $this->user_session['reg_id']; 
		$bus_id = $this->user_session['bus_id'];

		$srec_arr= array('bus_id'=>$bus_id);

		$data['activity']=$this->Module_Tracker_model->selectData('login_record', '*', array('bus_id'=>$bus_id));
		$data['users']=$this->Module_Tracker_model->get_active_users($bus_id);

		if($data['activity']>0){
			
			$this->load->view('module_activity/activity_list',$data);

		}else{

			$this->load->view('module_activity/activity_empty');
		}
		
	}

	public function get_activity(){

	 	// Activity Tracker start
			 $tracking_info['module_name'] = 'My Account / My Activity History';
			 $tracking_info['action_taken'] = 'Viewed';
			 $tracking_info['reference'] = "Activity List";
			 $LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
		// Activity Tracker End

		$post = $this->input->post();
		$post['reg_id']=$this->user_session['reg_id'];
		$post['bus_id']=$this->user_session['bus_id'];
		
		$field_pos=array("created_at"=>'0',"reg_id"=>'1',"bus_id"=>'2',"log_in"=>'3',"log_out"=>'4',"module_name"=>'5',"ip_address"=>'6',"login_id"=>'7');	

		$sort_field=array_search($post['order'][0]['column'],$field_pos);

		if($post['order'][0]['dir']=='asc')
		{
	 		$orderBy="DESC";
		}
	 	else
		{
	 		$orderBy="DESC";
		}	

		$TotalRecord=$this->Module_Tracker_model->ActivityFilter($post,$sort_field,$orderBy,0);	
	 	$userData = $this->Module_Tracker_model->ActivityFilter($post,$sort_field,$orderBy,1);

	 	$iTotalRecords = $TotalRecord['NumRecords'];
	 	// pr($userData);
	 	// exit;
	 	$records = array();
	  	$records["data"] = array();
	  	$csrf_token=$this->security->get_csrf_hash();

	  	$records['csrf_hash']=$csrf_token;
		
      $servertime = ini_get('date.timezone');

	  	foreach ($userData as $key => $value) {

	  		$check='<input type="checkbox" id="filled-in-box'.$value['login_id'].'" name="filled-in-box'.$value['login_id'].'" class="filled-in purple activity_bulk_action" value="'.$value['login_id'].'"/><label for="filled-in-box'.$value['login_id'].'"></label>';

	  		$login_id = '<a href="'.base_url().'module_tracker/view-activity/'.$value['login_id'].'"></a>';
		
				$log_in = date('h:ia',strtotime('+5 hour +30 minutes',strtotime($value['created_at'])));

			if($value['created_at'] == $value['updated_at'])
			{
				$log_out = "--";
			}else{

				$log_out = date('h:ia',strtotime('+5 hour +30 minutes',strtotime($value['updated_at']))); 
			}
			//$module = "Work In process";
			//echo $value['login_id'];
			//echo $post['bus_id'];
            $module = $this->Module_Tracker_model->unique_modules($value['login_id'],$post['bus_id']);
            if($module){
            	$new_arr = (array) $module;
            	$string = '';
            	for($i=0; $i<count($new_arr); $i++)
            	{
            		$string .= $new_arr[$i]['module_name'].', ';
            	}

            	$module_data = rtrim($string,",");
            	$module_data = substr($string,0,30).'....';
            	//print_r($new_arr);
            }else{
            	$module_data = "--";
            }
            

			$reg_id = $value['reg_id'];
			$bus_id = $value['bus_id'];  

			$bus_name = $this->Module_Tracker_model->selectData('businesslist', 'bus_company_name', array('bus_id'=>$bus_id));
			$reg_cust_name = $this->Module_Tracker_model->selectData('registration', 'reg_username', array('reg_id'=>$reg_id));

			$name = "<a href=".base_url()."module_tracker/view-activity/".$value["login_id"].">".$reg_cust_name[0]->reg_username . "</a>";
			$date = date('d.m.Y',strtotime($value['created_at'])); 
			$access = $value['access'];
			$time_spend = $value['time_spend'];

			if($value['access'] == -1)
			{
				$access = 'Admin';

			}elseif ($value['access'] == 1) {

				$access = 'Accountant';

			}elseif ($value['access'] == 2) {
				
				$access = 'CA / Tax Specialist';

			}elseif ($value['access'] == 3) {
				
				$access = 'Admin';

			}elseif ($value['access'] == 4) {
				
				$access = 'Employee';

			}elseif ($value['access'] == 5) {
				
				$access = 'Client';

			}else{
				$access = 'NA';
			}

			  $action = '';$status='';$cls='';$cls1='';
			    $cls='active_record';
				$cls1='activate';

			$action  .= "<div class='dropdown-button action-more border-radius-3 btn-default default-width no-background ".$cls."' href='#' data-activates='dropdown".$value["login_id"]."'><i class='action-dot'></i></div>

			  <ul id='dropdown".$value["login_id"]."' class='dropdown-content'>
				 	<!--<li>
					<a href=".base_url()."module_tracker/view-activity/".$value["login_id"].'><i class="material-icons" style="color: #000;">mode_edit</i>View</a></li>-->
					
					<li><a href="javascript:void(0);" onclick="email_activity('.$value["login_id"].');"><i class="material-icons">email</i>Email</a></li>

                    <li><a href='.base_url().'module_tracker/download_activity/'.$value["login_id"].'><i class="dropdwon-icon icon download"></i>Download</a></li>
					
					<li><a href='.base_url().'module_tracker/print-activity/'.$value["login_id"].' target="_blank"><i class="material-icons">print</i>Print</a></li>';
		
			
			  $action  .= '</ul>';
							
	  		$records["data"][] = array(
				$check,
				$date,
				$name,
				$log_in,
	  			$log_out,
	  			$module_data,
	  			$value['ip_address'],
	  			$action,
	  			);
	  	}




	  	$records["draw"] = intval($post['draw']);
		$records["recordsTotal"] = $iTotalRecords;
		$records["recordsFiltered"] = $iTotalRecords;		
		echo json_encode($records);				  
		exit;
	}




	public function view_activity($id=""){

		if($id==''){
			redirect('module_tracker');
		}

		$reg_id=$this->user_session['reg_id'];
		$bus_id=$this->user_session['bus_id'];
		
		$data['activity'] = $this->Module_Tracker_model->selectData('activity_tracker','*',array("login_id"=>$id,"bus_id"=>$bus_id));
		
		$data['login_record'] = $this->Module_Tracker_model->selectData('login_record','*',array("login_id"=>$id,"bus_id"=>$bus_id));
		$data['bus_name'] = $this->Module_Tracker_model->selectData('businesslist','bus_company_name',array("bus_id"=>$bus_id));
		$data['user_Details'] = $this->Module_Tracker_model->selectData('registration','reg_username,reg_admin_type',array("reg_id"=>$data['login_record'][0]->reg_id));
		$data['access'] = $this->Module_Tracker_model->selectData('access_permissions','access',array("id"=>$data['user_Details'][0]->reg_admin_type));
		
		$login_date = date('d.m.Y',strtotime($data['login_record'][0]->created_at));
		$login_time = date('h:ia',strtotime($data['login_record'][0]->created_at));
		
		// Activity Tracker start
			 $tracking_info['module_name'] = 'My Account / My Activity History';
			 $tracking_info['action_taken'] = 'Viewed';
			 $tracking_info['reference'] = $login_date.' / '.$login_time;
			 $LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
		// Activity Tracker End
		
		$this->load->view('module_activity/view_my_activity',$data);
	
	}

	public function print_activity($id=""){

		if($id==''){
			redirect('module_tracker');
		}

		$reg_id=$this->user_session['reg_id'];
		$bus_id=$this->user_session['bus_id'];
		
		$data['activity'] = $this->Module_Tracker_model->selectData('activity_tracker','*',array("login_id"=>$id,"bus_id"=>$bus_id));
		$data['login_record'] = $this->Module_Tracker_model->selectData('login_record','*',array("login_id"=>$id,"bus_id"=>$bus_id));
		$data['bus_name'] = $this->Module_Tracker_model->selectData('businesslist','bus_company_name',array("bus_id"=>$bus_id));
		$data['user_Details'] = $this->Module_Tracker_model->selectData('registration','reg_username,reg_admin_type',array("reg_id"=>$data['login_record'][0]->reg_id));
		$data['access'] = $this->Module_Tracker_model->selectData('access_permissions','access',array("id"=>$data['user_Details'][0]->reg_admin_type));

		$login_date = date('d.m.Y',strtotime($data['login_record'][0]->created_at));
		$login_time = date('h:ia',strtotime($data['login_record'][0]->created_at));
		
		// Activity Tracker start
			 $tracking_info['module_name'] = 'My Account / My Activity History';
			 $tracking_info['action_taken'] = 'Printed';
			 $tracking_info['reference'] = $login_date.' / '.$login_time;
			 $LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
		// Activity Tracker End

		$this->load->view('module_activity/print_my_activity',$data);
	
	}


	public function download_activity($id=''){

		if($id==''){
			redirect('module_tracker');
		}

		$reg_id=$this->user_session['reg_id'];
		$bus_id=$this->user_session['bus_id'];

		$data['activity'] = $this->Module_Tracker_model->selectData('activity_tracker','*',array("login_id"=>$id,"bus_id"=>$bus_id));
		$data['login_record'] = $this->Module_Tracker_model->selectData('login_record','*',array("login_id"=>$id,"bus_id"=>$bus_id));
		$data['bus_name'] = $this->Module_Tracker_model->selectData('businesslist','bus_company_name',array("bus_id"=>$bus_id));
		$data['user_Details'] = $this->Module_Tracker_model->selectData('registration','reg_username,reg_admin_type',array("reg_id"=>$data['login_record'][0]->reg_id));
		$data['access'] = $this->Module_Tracker_model->selectData('access_permissions','access',array("id"=>$data['user_Details'][0]->reg_admin_type));

		$login_date = date('d.m.Y',strtotime($data['login_record'][0]->created_at));
		$login_time = date('h:ia',strtotime($data['login_record'][0]->created_at));
		
		// Activity Tracker start
			 $tracking_info['module_name'] = 'My Account / My Activity History';
			 $tracking_info['action_taken'] = 'Download';
			 $tracking_info['reference'] = $login_date.' / '.$login_time;
			 $LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
		// Activity Tracker End

		$name = "Activity";
		$html_data = $this->load->view('module_activity/activity_download',$data,true);
		$this->pdf->create($html_data,$name,'download');
	}



	public function download_multiple_activity(){
		
		$reg_id=$this->user_session['reg_id'];
		$bus_id=$this->user_session['bus_id'];
		
		$array=$this->input->get('ids');
		$array=explode(',', $array);

		foreach($array as $id)
		{
			$data['login_record'] = $this->Module_Tracker_model->selectData('login_record','*',array("login_id"=>$id,"bus_id"=>$bus_id));
			$login_date = date('d.m.Y',strtotime($data['login_record'][0]->created_at));
			$login_time = date('h:ia',strtotime($data['login_record'][0]->created_at));
			
			// Activity Tracker start
				 $tracking_info['module_name'] = 'My Account / My Activity History';
				 $tracking_info['action_taken'] = 'Download';
				 $tracking_info['reference'] = $login_date.' / '.$login_time;
				 $LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
			// Activity Tracker End
		}

		$userData['userdata'] = $this->Module_Tracker_model->print_multiple_activity($array,$bus_id);
	
		$this->load->view('module_activity/print_all_activitys',$userData);
		
		$name = "Activity";
		$html_data = $this->load->view('module_activity/print_all_activitys',$userData,true);
		$this->pdf->create($html_data,$name,'download');

	}

	public function print_multiple_activity(){

		$reg_id=$this->user_session['reg_id'];
		$bus_id=$this->user_session['bus_id'];

		$array=$this->input->get('ids');
		$array=explode(',', $array);

		foreach($array as $id)
		{
			$data['login_record'] = $this->Module_Tracker_model->selectData('login_record','*',array("login_id"=>$id,"bus_id"=>$bus_id));
			$login_date = date('d.m.Y',strtotime($data['login_record'][0]->created_at));
			$login_time = date('h:ia',strtotime($data['login_record'][0]->created_at));
			
			// Activity Tracker start
				 $tracking_info['module_name'] = 'My Account / My Activity History';
				 $tracking_info['action_taken'] = 'Printed';
				 $tracking_info['reference'] = $login_date.' / '.$login_time;
				 $LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
			// Activity Tracker End
		}

		

		$userData['userdata'] = $this->Module_Tracker_model->print_multiple_activity($array,$bus_id);
	
		$this->load->view('module_activity/print_all_activitys',$userData);
	}

	public function email_multiple_activity(){

		$reg_id=$this->user_session['reg_id'];
		$bus_id=$this->user_session['bus_id'];

		$data = $_POST;
		$ids = explode(',',$data['activity_id']);

		for($i = 0; $i < count($ids); $i++)
		{
			$data['login_record'] = $this->Module_Tracker_model->selectData('login_record','*',array("login_id"=>$ids[$i],"bus_id"=>$bus_id));
			$login_date = date('d.m.Y',strtotime($data['login_record'][0]->created_at));
			$login_time = date('h:ia',strtotime($data['login_record'][0]->created_at));
			
			// Activity Tracker start
				 $tracking_info['module_name'] = 'My Account / My Activity History';
				 $tracking_info['action_taken'] = 'Emailed';
				 $tracking_info['reference'] = $login_date.' / '.$login_time;
				 $LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
			// Activity Tracker End

			$data['activity'] = $this->Module_Tracker_model->selectData('activity_tracker','*',array("login_id"=>$ids[$i],"bus_id"=>$bus_id));
			$data['login_record'] = $this->Module_Tracker_model->selectData('login_record','*',array("login_id"=>$ids[$i],"bus_id"=>$bus_id));
			$data['bus_name'] = $this->Module_Tracker_model->selectData('businesslist','bus_company_name',array("bus_id"=>$bus_id));
			$data['user_Details'] = $this->Module_Tracker_model->selectData('registration','reg_username,reg_admin_type',array("reg_id"=>$data['login_record'][0]->reg_id));
			$data['access'] = $this->Module_Tracker_model->selectData('access_permissions','access',array("id"=>$data['user_Details'][0]->reg_admin_type));

			$htmldata = $this->load->view('module_activity/activity_download',$data,true);
			//$invoname ="Activity Data";
			//$Attach[$i]=$this->pdf->create($htmldata,$invoname.'.pdf','email');
			$fromemail = $this->Sales_model->selectData('businesslist','*',array('bus_id'=>$bus_id,'reg_id'=>$reg_id,'status'=>'Active'));
			$invoname=str_replace('/','-','Activity')."_".time();
			$downld=$this->pdf->create($htmldata,$invoname,'download_multi');
			$path=DOC_ROOT.'/public/pdfmail/'.$invoname.'.pdf';
			$Attach[$i]=$path;

		}

		$subject = $data['email_subject'];
		$all_emails =  $data['email_to'];
		$from = $fromemail[0]->bus_company_name;
		$output = preg_split("/(,|;)/", $all_emails);
		$cc = $data['email_cc'];
		$cc = preg_split("/(,|;)/", $cc);
		$message = $data['email_message'];


		for($i=0; $i < count($output);$i++)
		{
			$to = $output[$i];
			//$status = sendEmail($to,$subject,$message);
			$status = sendInvoiceEmailMultiple($from,$to,$subject,$message,$Attach,$invoname,$cc);
		}

		for($i=0; $i < count($Attach); $i++)
		{
			unlink($Attach[$i]);
		}

		if($status)
		{
			echo 1;
		}else{
			echo 0;
		}
	
		
	}



}
?>