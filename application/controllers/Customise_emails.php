<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require ("common/Index_Controller.php");
class Customises_emails extends Index_Controller {

	function __construct(){
		parent::__construct();

		setlocale(LC_MONETARY, 'en_US.UTF-8'); 
		is_login();
		is_trialExpire();
		 is_BusReg();
		$this->user_session = $this->session->userdata('user_session');
      
	}
	public function index()
	{    
		//session_start();
		$gst_id = $this->user_session['ei_gst'];
		if($gst_id==''){
			$this->session->set_flashdata('error',"Please enter your GSTIN First.");
			redirect('sales/billing-documents');
		}else{
			redirect('customise-emails/my-sales');
		}
	}
	public function my_sales()
	{

		$reg_id = $this->user_session['reg_id'];
		$bus_id = $this->user_session['bus_id'];
		$gst_id = $this->user_session['ei_gst'];
		// if($gst_id==''){
		// 	$this->session->set_flashdata('error',"Please set GSTIN Number First..!!");
		// 	redirect('sales/billing-documents');
		// }
		$post=$this->input->post();

		if($post){

			$ce_id=$post['ce_id'];
			unset($post['ce_id']);
			unset($post['submit']);
			
		if($reg_id != '')
		{
			$post['reg_id'] = $reg_id;
		}
		if($bus_id != '')
		{
			$post['bus_id'] = $bus_id;
		}
		if($gst_id != '')
		{
			$post['gst_id'] = $gst_id;
		}
		$flag=false;
		$customise_emails	=	$this->Adminmaster_model->selectData('customise_emails', '*',array('bus_id'=>$bus_id,'ce_type'=>$post['ce_type']));

		if(count($customise_emails) > 0){

			// Activity Tracker start
				 $tracking_info['module_name'] = 'Customise Email / My Sales';
				 $tracking_info['action_taken'] = 'Added';
				 $tracking_info['reference'] = $post['ce_type'];
				 $LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
			// Activity Tracker End
			
			$flag = $this->Adminmaster_model->updateData('customise_emails', $post, array('ce_id' =>$ce_id));
		}else{
				// Activity Tracker start
					 $tracking_info['module_name'] = 'Customise Email / My Sales';
					 $tracking_info['action_taken'] = 'Edited';
					 $tracking_info['reference'] = $post['ce_type'];
					 $LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
				// Activity Tracker End
			$flag = $this->Adminmaster_model->insertData('customise_emails', $post);
		}
		if($flag != false)
		{

			if($post['ce_type']=='creditnote'){
			  $post['ce_type']="credit note";
			}
			$this->session->set_flashdata('success',"You have successfully save ".$post['ce_type']." email format");
			redirect('customise-emails/my-sales');
		}
		}
		// Activity Tracker start
			 $tracking_info['module_name'] = 'Customise Email / My Sales';
			 $tracking_info['action_taken'] = 'Viewed';
			 $tracking_info['reference'] = "Customise Email";
			 $LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
		// Activity Tracker End

		$data['customise_emails']	=	$this->Adminmaster_model->selectData('customise_emails', '*',array('bus_id'=>$bus_id,'ce_type'=>"client"));
		
		$this->load->view('customise_emails/customise-sales',$data);			
	}

	public function my_tax() {

		$reg_id = $this->user_session['reg_id'];
		$bus_id = $this->user_session['bus_id'];
		$gst_id = $this->user_session['ei_gst'];
		if($gst_id==''){
			$this->session->set_flashdata('error',"Please enter your GSTIN First.");
			redirect('sales/billing-documents');
		}

		$post=$this->input->post();

		if($post){

			$ce_id=$post['ce_id'];
			unset($post['ce_id']);
			unset($post['submit']);
			
		if($reg_id != '')
		{
			$post['reg_id'] = $reg_id;
		}
		if($bus_id != '')
		{
			$post['bus_id'] = $bus_id;
		}
		if($gst_id != '')
		{
			$post['gst_id'] = $gst_id;
		}
		$flag=false;
		$customise_emails	=	$this->Adminmaster_model->selectData('customise_emails', '*',array('reg_id'=>$reg_id,'bus_id'=>$bus_id,'gst_id'=>$gst_id,'ce_type'=>$post['ce_type']));
		
		if(count($customise_emails) > 0){

			// Activity Tracker start
				 $tracking_info['module_name'] = 'Customise Email / My TAX';
				 $tracking_info['action_taken'] = 'Added';
				 $tracking_info['reference'] = $post['ce_type'];
				 $LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
			// Activity Tracker End
			
			$flag = $this->Adminmaster_model->updateData('customise_emails', $post, array('ce_id' =>$ce_id));
		}else{

			// Activity Tracker start
				 $tracking_info['module_name'] = 'Customise Email / My TAX';
				 $tracking_info['action_taken'] = 'Edited';
				 $tracking_info['reference'] = $post['ce_type'];
				 $LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
			// Activity Tracker End
			
			$flag = $this->Adminmaster_model->insertData('customise_emails', $post);
		}
		if($flag != false)
		{
			if($post['ce_type']=='equalisationlevy'){
			  $post['ce_type']="equalisation levy";
			}
			if($post['ce_type']=='tds'){
			  $post['ce_type']="TDS";
			}
			$this->session->set_flashdata('success',"You have successfully save ".$post['ce_type']." email format");
			redirect('customise-emails/my-tax');
		}
		}
		else{
			$post['ce_type'] = 'tds';
		}

		$data['customise_emails']	=	$this->Adminmaster_model->selectData('customise_emails', '*',array('reg_id'=>$reg_id,'bus_id'=>$bus_id,'gst_id'=>$gst_id,'ce_type'=>$post['ce_type']));
		
			// Activity Tracker start
				 $tracking_info['module_name'] = 'Customise Email / My TAX';
				 $tracking_info['action_taken'] = 'Viewed';
				 $tracking_info['reference'] = "Customise Email";
				 $LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
			// Activity Tracker End
			
		$this->load->view('customise_emails/customise-tax',$data);				
	}
	
	public function my_receipt()
	{
		$reg_id = $this->user_session['reg_id'];
		$bus_id = $this->user_session['bus_id'];
		$gst_id = $this->user_session['ei_gst'];
		if($gst_id==''){
			$this->session->set_flashdata('error',"Please enter your GSTIN First.");
			redirect('sales/billing-documents');
		}

		$post=$this->input->post();

		if($post){

			$ce_id=$post['ce_id'];
			unset($post['ce_id']);
			unset($post['submit']);
			
		if($reg_id != '')
		{
			$post['reg_id'] = $reg_id;
		}
		if($bus_id != '')
		{
			$post['bus_id'] = $bus_id;
		}
		if($gst_id != '')
		{
			$post['gst_id'] = $gst_id;
		}
		$flag=false;
		$customise_emails	=	$this->Adminmaster_model->selectData('customise_emails', '*',array('reg_id'=>$reg_id,'bus_id'=>$bus_id,'gst_id'=>$gst_id,'ce_type'=>$post['ce_type']));
		
		if(count($customise_emails) > 0){
			
			// Activity Tracker start
				 $tracking_info['module_name'] = 'Customise Email / My Receipts';
				 $tracking_info['action_taken'] = 'Added';
				 $tracking_info['reference'] = $post['ce_type'];
				 $LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
			// Activity Tracker End
			$flag = $this->Adminmaster_model->updateData('customise_emails', $post, array('ce_id' =>$ce_id));
		}else{
				
			// Activity Tracker start
				 $tracking_info['module_name'] = 'Customise Email / My Receipts';
				 $tracking_info['action_taken'] = 'Edited';
				 $tracking_info['reference'] = $post['ce_type'];
				 $LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
			// Activity Tracker End
			$flag = $this->Adminmaster_model->insertData('customise_emails', $post);
		}
		if($flag != false)
		{
			if($post['ce_type']=='sales_recipt'){
			  $post['ce_type']="Sales Recipt";
			}
			$this->session->set_flashdata('success',"You have successfully save ".$post['ce_type']." email format");
			//redirect('customise_emails/customise_receipt');
		}
		}

		$data['customise_emails']	=	$this->Adminmaster_model->selectData('customise_emails', '*',array('reg_id'=>$reg_id,'bus_id'=>$bus_id,'gst_id'=>$gst_id,'ce_type'=>"sales_recipt"));
		
		// Activity Tracker start
			 $tracking_info['module_name'] = 'Customise Email / My Receipts';
			 $tracking_info['action_taken'] = 'Viewed';
			 $tracking_info['reference'] = "Customise Email";
			 $LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
		// Activity Tracker End

		$this->load->view('customise_emails/customise-receipt',$data);				
	}
	
	public function my_company()
	{

		$reg_id = $this->user_session['reg_id'];
		$bus_id = $this->user_session['bus_id'];
		$gst_id = $this->user_session['ei_gst'];
		if($gst_id==''){
			$this->session->set_flashdata('error',"Please enter your GSTIN First.");
			redirect('sales/billing-documents');
		}

		$post=$this->input->post();

		if($post){

			$ce_id=$post['ce_id'];
			unset($post['ce_id']);
			unset($post['submit']);
			
		if($reg_id != '')
		{
			$post['reg_id'] = $reg_id;
		}
		if($bus_id != '')
		{
			$post['bus_id'] = $bus_id;
		}
		if($gst_id != '')
		{
			$post['gst_id'] = $gst_id;
		}
		$flag=false;
		$customise_emails	=	$this->Adminmaster_model->selectData('customise_emails', '*',array('reg_id'=>$reg_id,'bus_id'=>$bus_id,'gst_id'=>$gst_id,'ce_type'=>$post['ce_type']));
		
		if(count($customise_emails) > 0){
			
			// Activity Tracker start
				 $tracking_info['module_name'] = 'Customise Email / My Company';
				 $tracking_info['action_taken'] = 'Added';
				 $tracking_info['reference'] = $post['ce_type'];
				 $LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
			// Activity Tracker End

			$flag = $this->Adminmaster_model->updateData('customise_emails', $post, array('ce_id' =>$ce_id));
		}else{

			// Activity Tracker start
				 $tracking_info['module_name'] = 'Customise Email / My Company';
				 $tracking_info['action_taken'] = 'Edited';
				 $tracking_info['reference'] = $post['ce_type'];
				 $LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
			// Activity Tracker End
			
			$flag = $this->Adminmaster_model->insertData('customise_emails', $post);
		}
		if($flag != false)
		{
			if($post['ce_type']=='company'){
			  $post['ce_type']="Company";
			}
			$this->session->set_flashdata('success',"You have successfully save ".$post['ce_type']." email format");
			//redirect('customise_emails/customise_receipt');
		}
		}

		$data['customise_emails']	=	$this->Adminmaster_model->selectData('customise_emails', '*',array('reg_id'=>$reg_id,'bus_id'=>$bus_id,'gst_id'=>$gst_id,'ce_type'=>"company"));

		// Activity Tracker start
			 $tracking_info['module_name'] = 'Customise Email / My Company';
			 $tracking_info['action_taken'] = 'Viewed';
			 $tracking_info['reference'] = "Customise Email";
			 $LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
		// Activity Tracker End

		$this->load->view('customise_emails/customise-company',$data);				
	}
	
	public function document_locker()
	{
		$reg_id = $this->user_session['reg_id'];
		$bus_id = $this->user_session['bus_id'];
		$gst_id = $this->user_session['ei_gst'];
		if($gst_id==''){
			$this->session->set_flashdata('error',"Please enter your GSTIN First.");
			redirect('sales/billing-documents');
		}

		// Activity Tracker start
			 $tracking_info['module_name'] = 'Customise Email';
			 $tracking_info['action_taken'] = 'Viewed';
			 $tracking_info['reference'] = "My Recipt";
			 $LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
		// Activity Tracker End

		$post=$this->input->post();

		if($post){

			$ce_id=$post['ce_id'];
			unset($post['ce_id']);
			unset($post['submit']);
			
		if($reg_id != '')
		{
			$post['reg_id'] = $reg_id;
		}
		if($bus_id != '')
		{
			$post['bus_id'] = $bus_id;
		}
		if($gst_id != '')
		{
			$post['gst_id'] = $gst_id;
		}
		$flag=false;
		$customise_emails	=	$this->Adminmaster_model->selectData('customise_emails', '*',array('reg_id'=>$reg_id,'bus_id'=>$bus_id,'gst_id'=>$gst_id,'ce_type'=>$post['ce_type']));
		
		if(count($customise_emails) > 0){
				
			// Activity Tracker start
				 $tracking_info['module_name'] = 'Customise Email / My Document Locker';
				 $tracking_info['action_taken'] = 'Added';
				 $tracking_info['reference'] = $post['ce_type'];
				 $LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
			// Activity Tracker End
			$flag = $this->Adminmaster_model->updateData('customise_emails', $post, array('ce_id' =>$ce_id));
		}else{
			
			// Activity Tracker start
				 $tracking_info['module_name'] = 'Customise Email / My Document Locker';
				 $tracking_info['action_taken'] = 'Edited';
				 $tracking_info['reference'] = $post['ce_type'];
				 $LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
			// Activity Tracker End
			$flag = $this->Adminmaster_model->insertData('customise_emails', $post);
		}
		if($flag != false)
		{
			if($post['ce_type']=='doc_locker'){
			  $post['ce_type']="Document Locker";
			}
			$this->session->set_flashdata('success',"You have successfully save ".$post['ce_type']." email format");
			//redirect('customise_emails/customise_receipt');
		}
		}

		$data['customise_emails']	=	$this->Adminmaster_model->selectData('customise_emails', '*',array('reg_id'=>$reg_id,'bus_id'=>$bus_id,'gst_id'=>$gst_id,'ce_type'=>"doc_locker"));

		// Activity Tracker start
			 $tracking_info['module_name'] = 'Customise Email / Document Locker';
			 $tracking_info['action_taken'] = 'Viewed';
			 $tracking_info['reference'] = "Customise Email";
			 $LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
		// Activity Tracker End

		$this->load->view('customise_emails/customise-document-locker',$data);				
	}
	
	public function subscription_billing()
	{
		$reg_id = $this->user_session['reg_id'];
		$bus_id = $this->user_session['bus_id'];
		$gst_id = $this->user_session['ei_gst'];
		if($gst_id==''){
			$this->session->set_flashdata('error',"Please enter your GSTIN First.");
			redirect('sales/billing-documents');
		}

		// Activity Tracker start
			 $tracking_info['module_name'] = 'Customise Email';
			 $tracking_info['action_taken'] = 'Viewed';
			 $tracking_info['reference'] = "My Recipt";
			 $LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
		// Activity Tracker End

		$post=$this->input->post();

		if($post){

			$ce_id=$post['ce_id'];
			unset($post['ce_id']);
			unset($post['submit']);
			
		if($reg_id != '')
		{
			$post['reg_id'] = $reg_id;
		}
		if($bus_id != '')
		{
			$post['bus_id'] = $bus_id;
		}
		if($gst_id != '')
		{
			$post['gst_id'] = $gst_id;
		}
		$flag=false;
		$customise_emails	=	$this->Adminmaster_model->selectData('customise_emails', '*',array('reg_id'=>$reg_id,'bus_id'=>$bus_id,'gst_id'=>$gst_id,'ce_type'=>$post['ce_type']));
		
		if(count($customise_emails) > 0){
			
			// Activity Tracker start
				 $tracking_info['module_name'] = 'Customise Email / My Subscription & Billing';
				 $tracking_info['action_taken'] = 'Added';
				 $tracking_info['reference'] = $post['ce_type'];
				 $LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
			// Activity Tracker End

			$flag = $this->Adminmaster_model->updateData('customise_emails', $post, array('ce_id' =>$ce_id));
		}else{
			
			// Activity Tracker start
				 $tracking_info['module_name'] = 'Customise Email / My Subscription & Billing';
				 $tracking_info['action_taken'] = 'Edited';
				 $tracking_info['reference'] = $post['ce_type'];
				 $LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
			// Activity Tracker End
			$flag = $this->Adminmaster_model->insertData('customise_emails', $post);
		}
		if($flag != false)
		{
			if($post['ce_type']=='subscription'){
			  $post['ce_type']="Subscription";
			}
			$this->session->set_flashdata('success',"You have successfully save ".$post['ce_type']." email format");
			//redirect('customise_emails/customise_receipt');
		}
		}

		$data['customise_emails']	=	$this->Adminmaster_model->selectData('customise_emails', '*',array('reg_id'=>$reg_id,'bus_id'=>$bus_id,'gst_id'=>$gst_id,'ce_type'=>"subscription"));

		// Activity Tracker start
			 $tracking_info['module_name'] = 'Customise Email / My Subscription & Billing';
			 $tracking_info['action_taken'] = 'Viewed';
			 $tracking_info['reference'] = "Customise Email";
			 $LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
		// Activity Tracker End

		$this->load->view('customise_emails/customise-subscription',$data);				
	}
	
	public function my_alert()
	{
		$reg_id = $this->user_session['reg_id'];
		$bus_id = $this->user_session['bus_id'];
		$gst_id = $this->user_session['ei_gst'];
		if($gst_id==''){
			$this->session->set_flashdata('error',"Please enter your GSTIN First.");
			redirect('sales/billing-documents');
		}

		// Activity Tracker start
			 $tracking_info['module_name'] = 'Customise Email';
			 $tracking_info['action_taken'] = 'Viewed';
			 $tracking_info['reference'] = "My Recipt";
			 $LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
		// Activity Tracker End

		$post=$this->input->post();

		if($post){

			$ce_id=$post['ce_id'];
			unset($post['ce_id']);
			unset($post['submit']);
			
		if($reg_id != '')
		{
			$post['reg_id'] = $reg_id;
		}
		if($bus_id != '')
		{
			$post['bus_id'] = $bus_id;
		}
		if($gst_id != '')
		{
			$post['gst_id'] = $gst_id;
		}
		$flag=false;
		$customise_emails	=	$this->Adminmaster_model->selectData('customise_emails', '*',array('reg_id'=>$reg_id,'bus_id'=>$bus_id,'gst_id'=>$gst_id,'ce_type'=>$post['ce_type']));
		
		if(count($customise_emails) > 0){

			// Activity Tracker start
				 $tracking_info['module_name'] = 'Customise Email / My Alert';
				 $tracking_info['action_taken'] = 'Added';
				 $tracking_info['reference'] = $post['ce_type'];
				 $LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
			// Activity Tracker End
			$flag = $this->Adminmaster_model->updateData('customise_emails', $post, array('ce_id' =>$ce_id));
		}else{
			// Activity Tracker start
				 $tracking_info['module_name'] = 'Customise Email / My Alert';
				 $tracking_info['action_taken'] = 'Edited';
				 $tracking_info['reference'] = $post['ce_type'];
				 $LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
			// Activity Tracker End
			$flag = $this->Adminmaster_model->insertData('customise_emails', $post);
		}
		if($flag != false)
		{
			if($post['ce_type']=='client_alert'){
			  $post['ce_type_d']="Client Alert";
			}
			if($post['ce_type']=='item_alert'){
			  $post['ce_type_d']="Item Alert";
			}
			if($post['ce_type']=='credit_period'){
			  $post['ce_type_d']="Credit Period";
			}
			if($post['ce_type']=='client_ba'){
			  $post['ce_type_d']="Birthday and Anniversary";
			}
			$this->session->set_flashdata('success',"You have successfully save ".$post['ce_type_d']." email format");
			//redirect('customise_emails/customise_receipt');
		}
		}else{
			$post['ce_type'] = 'client_alert';
		}

		$data['customise_emails']	=	$this->Adminmaster_model->selectData('customise_emails', '*',array('reg_id'=>$reg_id,'bus_id'=>$bus_id,'gst_id'=>$gst_id,'ce_type'=>$post['ce_type']));

		// Activity Tracker start
			 $tracking_info['module_name'] = 'Customise Email / My Alert';
			 $tracking_info['action_taken'] = 'Viewed';
			 $tracking_info['reference'] = "Customise Email";
			 $LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
		// Activity Tracker End
		$this->load->view('customise_emails/customise-alert',$data);				
	}
	
	public function get_customise_email()
	{
		$reg_id = $this->user_session['reg_id'];
		$bus_id = $this->user_session['bus_id'];
		$gst_id = $this->user_session['ei_gst'];
		$ce_type=$this->input->post('ce_type');
		$customise_emails	=	$this->Adminmaster_model->selectData('customise_emails', '*',array('bus_id'=>$bus_id,'ce_type'=>$ce_type));
		$csrf_token=$this->security->get_csrf_hash();
	  	$customise_emails['csrf_hash']=$csrf_token;
		echo json_encode($customise_emails);
	}
}

	