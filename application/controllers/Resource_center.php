<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require ("common/Index_Controller.php");
class Resource_center extends Index_Controller {

	function __construct(){
		parent::__construct();

		$this->load->model('Resource_model');
		
		setlocale(LC_MONETARY, 'en_US.UTF-8'); 

		is_login();
		//is_trialExpire();
		 //is_BusReg();
		$this->user_session = $this->session->userdata('user_session');

	}
	public function index()
	{

		redirect('resource_center/faqs');
		
	}

	public function faqs()
	{
		$tracking_info['module_name'] = 'My Resource Center / FAQs';
		 $tracking_info['action_taken'] = 'Viewed';
		 $tracking_info['reference'] = "FAQs";
		 $LogRec = $this->Adminmaster_model->insertActivity($tracking_info);

		$this->load->view('resource_center/faq');
		
	}
	public function accounting_terms(){

		$tracking_info['module_name'] = 'My Resource Center / Accounting Terms';
			 $tracking_info['action_taken'] = 'Viewed';
			 $tracking_info['reference'] = "Accounting Terms";
			 $LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
		$this->load->view('resource_center/accounting-terms');

	}
	
	public function financial_ratios(){

		$this->load->view('resource_center/financial-ratios');

	}
	
	public function govt_scheme(){

		$this->load->view('resource_center/govt_schemes');

	}
}
	
