<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require ("common/Index_Controller.php");

class Refer_earn extends Index_Controller {

	function __construct(){

		parent::__construct();
		$this->load->model('Refer_earn_model');
		$this->load->model('User_model');
		$this->load->model('Profile_model');
		$this->load->library('session');


		is_login();

        is_accessAvail(1);
		is_trialExpire();
		// is_BusReg();


		$this->user_session = $this->session->userdata('user_session');

	}

	public function index(){
		$reg_id = $this->user_session['reg_id'];
		$entry = $this->Refer_earn_model->selectData('refer_earn', '*', array('reg_no'=>$reg_id));
		if(count($entry) > 0) {
			// already created ... list view
			$data['refer_code'] = $entry;
			$post['reg_id'] = $reg_id;
			$TotalRecord=$this->Refer_earn_model->referalListFilter($post,'','',0,0);
		 	$data['count'] = $TotalRecord['NumRecords'];
			$data['refered'] = $this->Refer_earn_model->selectData('refer_earn_list', '*', array('reg_no'=>$reg_id));
			$this->load->view('refer_earn/referal_list_view', $data);
		} else {
			// create
			$data['personal_profile'] = $this->Profile_model->selectData('registration', '*', "(reg_createdby=".$reg_id." OR reg_id=".$reg_id.")");
			$data['countries']	=	$this->Profile_model->selectData('countries', '*','','country_id','ASC');
			// print_r($data['personal_profile'][0]->reg_username);

			$temp = $data['personal_profile'][0]->reg_username;
			$arr = explode(' ',trim($temp));
			$temp = $arr[0];
			$last_refer_id = $this->Refer_earn_model->selectData('refer_earn', '*', '','refer_id','DESC');
			if(count($last_refer_id)>0){
				$last_refer_code=intval($last_refer_id[0]->refer_id) + 1;
				$ref_code=str_pad($last_refer_code, 0, 0, STR_PAD_LEFT);
			}
			else
			{
				$ref_code=str_pad(1, 0, 0, STR_PAD_LEFT);
			}
			$temp= strtolower($temp).$ref_code;
			$data['refer_code']=$temp;
			$this->load->view('refer_earn/referal_create', $data);
		}

	}
	public function blank(){
		index();
	}


	public function create_your_referal_code(){

		$reg_id = $this->user_session['reg_id'];
		$data['personal_profile'] = $this->Profile_model->selectData('registration', '*', "(reg_createdby=".$reg_id." OR reg_id=".$reg_id.")");
		// print_r($data['personal_profile'][0]->reg_username);

		$temp = $data['personal_profile'][0]->reg_username;
		$arr = explode(' ',trim($temp));
		$temp = $arr[0];
		$last_refer_id = $this->Refer_earn_model->selectData('refer_earn', '*', '','refer_id','DESC');
		if(count($last_refer_id)>0){
			$last_refer_code=intval($last_refer_id[0]->refer_id) + 1;
			$ref_code=str_pad($last_refer_code, 0, 0, STR_PAD_LEFT);

		}
		else
		{
			$ref_code=str_pad(1, 0, 0, STR_PAD_LEFT);
		}
		$temp= strtolower($temp).$ref_code;
		$post = $this->input->post();
		if($post) {
			$post['reg_no'] = $reg_id;
			unset($post['referal_code']);
			$post['refer_code'] = $temp;
			unset($post['contact_name']);
			$post['refer_name'] = $data['personal_profile'][0]->reg_username;
			$post['refer_email'] = $post['email_id'];
			unset($post['email_id']);
			$post['refer_cell_no'] = $post['cell_no'];
			unset($post['cell_no']);
			$post['refer_billing_address'] = $post['billing_address'];
			unset($post['billing_address']);
			$post['refer_pan_no'] = $post['pan_no'];
			unset($post['pan_no']);
			//$post['refer_bank_no'] = $post['bank_account_details'];
			//unset($post['bank_account_details']);
			if($this->session->userdata('add_bank_info') != '') {

				$bank_session = $this->session->userdata('add_bank_info');
				
				$post['ac_number'] = $bank_session[0]['cbank_account_no'];
				$post['bank_name'] = $bank_session[0]['cbank_name'];
				$post['acc_type'] = $bank_session[0]['cbank_account_type'];
				$post['bank_branch_name'] = $bank_session[0]['cbank_branch_name'];
				$post['bus_bank_country'] = $bank_session[0]['cbank_country_code'];
				$post['ifsc_code'] = $bank_session[0]['cbank_ifsc_code'];
				$post['bank_cur_check'] = $bank_session[0]['cbank_currency_code'];
				$post['swift_code'] = $bank_session[0]['cbank_swift_code'];
					
			}

			$refer_done = $this->Refer_earn_model->insertData('refer_earn', $post);
              $this->session->unset_userdata('add_bank_infow`');
			if($refer_done != 0) {
				$this->session->set_flashdata('success',"Your Referal Code has been generated !");
			} else {
				$this->session->set_flashdata('error',"Your Referal Code failed to be generated. Try again !");
			}
			redirect('refer_earn');
		} else {

		}

	}

	public function edit_your_referal_code($refer_id = ''){

		// if($refer_id==''){
		// 	redirect('refer_earn');
		// }

		$reg_id = $this->user_session['reg_id'];
		$data['personal_profile'] = $this->Profile_model->selectData('registration', '*', "(reg_createdby=".$reg_id." OR reg_id=".$reg_id.")");
		$data['countries']	=	$this->Profile_model->selectData('countries', '*','','country_id','ASC');
		$data['currency']	=	$this->Profile_model->selectData('currency', '*','','currency_id','ASC');
		// print_r($data['personal_profile'][0]->reg_username);

		$temp = $data['personal_profile'][0]->reg_username;
		$arr = explode(' ',trim($temp));
		$temp = $arr[0];

		$refer_id_found = $this->Refer_earn_model->selectData('refer_earn', '*', array('reg_no'=>$reg_id),'refer_id','DESC');
		$refer_id = $refer_id_found[0]->refer_id;


		$ref_code = $refer_id_found[0]->refer_code;

		$post = $this->input->post();
		if($post) {
			$post['reg_no'] = $reg_id;
			unset($post['referal_code']);
			$post['refer_code'] = $ref_code;
			unset($post['contact_name']);
			$post['refer_name'] = $data['personal_profile'][0]->reg_username;
			$post['refer_email'] = $post['email_id'];
			unset($post['email_id']);
			$post['refer_cell_no'] = $post['cell_no'];
			unset($post['cell_no']);
			$post['refer_billing_address'] = $post['billing_address'];
			unset($post['billing_address']);
			$post['refer_pan_no'] = $post['pan_no'];
			unset($post['pan_no']);
			//$post['refer_bank_no'] = $post['bank_account_details'];
			//unset($post['bank_account_details']);
			if($this->session->userdata('add_bank_info') != '') {

				$bank_session = $this->session->userdata('add_bank_info');

				
				
				$post['ac_number'] = $bank_session[0]['cbank_account_no'];
				$post['bank_name'] = $bank_session[0]['cbank_name'];
				$post['acc_type'] = $bank_session[0]['cbank_account_type'];
				$post['bank_branch_name'] = $bank_session[0]['cbank_branch_name'];
				$post['bus_bank_country'] = $bank_session[0]['cbank_country_code'];
				$post['ifsc_code'] = $bank_session[0]['cbank_ifsc_code'];
				$post['bank_cur_check'] = $bank_session[0]['cbank_currency_code'];
				$post['swift_code'] = $bank_session[0]['cbank_swift_code'];
					
			}

			
			$refer_done = $this->Refer_earn_model->updateData('refer_earn', $post, array('refer_id'=>$refer_id, 'reg_no'=>$reg_id));
			 $this->session->unset_userdata('add_bank_info');

			if($refer_done != 0) {
				$this->session->set_flashdata('success',"Your Referal Code Details has been updated !");
			} else {
				$this->session->set_flashdata('error',"Your Referal Code Details failed to be updated. Try again !");
			}
			redirect('refer_earn');
		}

		$data['refer_data'] = $this->Refer_earn_model->selectData('refer_earn', '*', array('refer_id'=>$refer_id));

		$this->load->view('refer_earn/referal_edit', $data);

	}

	public function get_referal_details(){

		// Activity Tracker start
			 $tracking_info['module_name'] = 'My REFERALS';
			 $tracking_info['action_taken'] = 'Listied';
			 $tracking_info['reference'] = "My REFERALS List";
			 $LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
		// Activity Tracker End

			$post = $this->input->post();
		$post['reg_id']=$this->user_session['reg_id'];

		$sort_field = '';
		$orderBy ='';


		$TotalRecord=$this->Refer_earn_model->referalListFilter($post,$sort_field,$orderBy,0,0);
	 	$userData = $this->Refer_earn_model->referalListFilter($post,$sort_field,$orderBy,1,0);
        //
	 	$iTotalRecords = $TotalRecord['NumRecords'];
		$counter = 0;
		$records = array();
		$records["data"] = array();
		foreach ($userData as $key => $value) {

			$counter = $value['refer_list_id'];
			$condition_arr= array('reg_email'=>$value['refer_list_email']);
			$data['personal_profile'] = $this->Profile_model->selectData('registration', '*', $condition_arr);

			$check='<input type="checkbox" id="filled-in-box-'.$counter.'" name="filled-in-box-'.$counter.'" class="filled-in purple referal_bulk_action" value=""/> <label for="filled-in-box-'.$counter.'"></label>';
			$invitedate = date('d-m-Y',strtotime($value['last_updated']));
            if(count($data['personal_profile']) > 0) {

				$signup = date('d-m-Y',strtotime($data['personal_profile'][0]->createdat));
				$email = $data['personal_profile'][0]->reg_email;//$value['refer_list_email'];
				$clientName = $data['personal_profile'][0]->reg_username;
				$status = $data['personal_profile'][0]->reg_istrial;
				$status = intval($status);
				if($status == 1)
					$status = 'Trial';
				else
					$status = 'Paid';
				$phone =$data['personal_profile'][0]->reg_mobile;
				$signupdate=date("d-m-Y",strtotime($data['personal_profile'][0]->createdat));
			} else {
				$signup = 'Not Yet Registered';
				$status = 'Not Yet Registered';
				$email = $value['refer_list_email'];
				$phone = $value['refer_list_mobile'];
				$clientName = '';
				$signupdate="NA";
			}

			$condition_arr= array('email'=>$value['refer_list_email']);
			$data['subscription'] = $this->Refer_earn_model->selectData('subscription', '*', $condition_arr);
			if(count($data['subscription']) >0) {
				$subscription = $data['subscription'][0]->subscription_plan;
			} else {
				$subscription = 'NA';
			}
			$fee = 0;
			$paid="Pending";
			if($subscription == "Gallop") {
				$fee = 5000;
			} else  {
				$fee = 0;
				$paid="NA";
			}
			
			//print_r($clientName);exit();
			if(empty($clientName)){
				$cl_name = $email.'<br>'.$phone;
			}else{
				$cl_name = $clientName.'<br>'.$email.'<br>'.$phone;
			}
			
			$records["data"][] = array(
					$check,
					$invitedate,
		  			$cl_name,
		  			$signupdate,
					$status,
		  			$subscription,
		  			$fee,
					$paid,
		  			);
		}

		$records["recordsTotal"] = $iTotalRecords;
		$records["recordsFiltered"] = $iTotalRecords;
		echo json_encode($records);
		exit();
	}

	//
	public function invite_by_email() {
		$reg_id = $this->user_session['reg_id'];
		$fromemail = $this->Sales_model->selectData('businesslist','*',array('bus_id'=>$bus_id,'reg_id'=>$reg_id,'status'=>'Active'));
		$post = $this->input->post();
		$csrf_token=$this->security->get_csrf_hash();
		if($post['email'] != '' && $post['refer_id'] != '') {
			$post['reg_no'] = $reg_id;
			$post['refer_list_email'] = $post['email'];
			unset($post['email']);
			$post['last_updated'] = date('Y-m-d');

			$data['your_refer_code'] = $this->Refer_earn_model->selectData('refer_earn', '*', array('refer_id'=>$post['refer_id']));
			$to = $post['refer_list_email'];
			$from = $fromemail[0]->bus_company_name;
			$subject = 'Invite to Xebra';
			$message=$this->load->view('email_template/email_coupon',$data,true);
			//$message = 'I recently read about the Xebra application that studies your own business data and gives you a clearer picture. Also, you can carry out invoicing, record expenses, asset and even payroll. You can subscribe to the Full Throttle pack with this code '.$your_refer_code[0]->refer_code.' and avail a discount of Rs. 5,000. https://www.xebra.in/xebra/?signup=1';
			//$message = 'I recently read about the Xebra application and felt this could be of value to you. You can subscribe to it with this code '.$your_refer_code[0]->refer_code.' and get a discount of Rs. 2500. https://www.xebra.in/xebra/?signup=1';
			$status = sendEmail($from,$to,$subject,$message);

			$check_if_already = $this->Refer_earn_model->selectData('refer_earn_list', '*', array('reg_no'=>$reg_id,'refer_id'=>$post['refer_id'], 'refer_list_email'=>$post['refer_list_email']));
			
			if(count($check_if_already) > 0){


			}
			else{
				$refer_list = $this->Refer_earn_model->insertData('refer_earn_list', $post);
			
			
			if($refer_list != 0) {
				$this->session->set_flashdata('success',"Your Referal Code has been sent for invite !");
			} else {
				$this->session->set_flashdata('error',"Your Referal Code failed to be sent for invite. Try again !");
			}
		}
		} else {
			redirect('refer_earn');
		}
	}

	//
	public function invite_by_sms() {
		$reg_id = $this->user_session['reg_id'];
		$post = $this->input->post();
		if($post['phone_no'] != '' && $post['refer_id'] != '') {
			$post['reg_no'] = $reg_id;
			$post['refer_list_mobile'] = $post['phone_no'];
			unset($post['phone_no']);
			$post['last_updated'] = date('Y-m-d');

			
			$your_refer_code = $this->Refer_earn_model->selectData('refer_earn', '*', array('refer_id'=>$post['refer_id']));
			$to = $post['refer_list_mobile'];
			$subject = 'Invite to Xebra';
			$message = 'Hi, do try out Xebra as you can then carry out Invoicing, Expenses & Asset tracking, Payroll, Tax Calculation, Banking & Accounting - all in one';
			//$message = 'Hi, I am using Xebra app that merges invoicing, expenses, payroll, banking, tax calculation & accounting. Use code '.$your_refer_code[0]->refer_code.' to get Rs 5000 off on Gallop plan https://www.xebra.in/pricing';
			//$message = 'I liked the business Intelligence Application Xebra and felt this could be of value to you. You can subscribe to it with this code '.$your_refer_code[0]->refer_code.' and get a discount of Rs. 2500. https://www.xebra.in/xebra/?signup=1';
			//Hey, I recently read about the Xebra application that helps you understand your business growth and profitability better.It shows you how your company is faring so you can take timely business decisions. It combines invoicing, expense,asset tracking, payroll & automated accounting. You can Subscribe to the Full Throttle pack with this code '.$your_refer_code[0]->refer_code.' and avail a discount of Rs. 5,000.https://www.xebra.in/xebra/?signup=1'
			$status = send_sms($to,$message);

			$check_if_already = $this->Refer_earn_model->selectData('refer_earn_list', '*', array('reg_no'=>$reg_id,'refer_id'=>$post['refer_id'], 'refer_list_mobile'=>$post['refer_list_mobile']));
			
			if(count($check_if_already) > 0){

			}
			else{
				$refer_list = $this->Refer_earn_model->insertData('refer_earn_list', $post);
			
			
			if($refer_list != 0) {
				$this->session->set_flashdata('success',"Your Referal Code has been sent for invite !");
			} else {
				$this->session->set_flashdata('error',"Your Referal Code failed to be sent for invite. Try again !");
			}
		}
		} else {
			redirect('refer_earn');
		}
	}

	public function download_multiple_referearn() {
		$array=$this->input->get('id');
		$reg_id = $this->user_session['reg_id'];
		$array=explode(',', $array);
		foreach ($array as $id) {
			//track Here

		}
		// $condition_arr= array('refer_id'=>$value['refer_lst_email']);
		// $data['userData'] = $this->Refer_earn_model->selectData('refer_earn_list', '*', );
		$data['result'] = $this->Refer_earn_model->download_multiple_referearn_list($array,$reg_id);

		$this->load->view('refer_earn/download_all_list', $data);
		// print_r($data['result']);
	}

}
