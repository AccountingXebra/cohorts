<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require ("common/Index_Controller.php");
class Errors extends Index_Controller {

	function __construct(){

		parent::__construct();
		$this->load->model('Adminmaster_model');
		$this->load->model('User_model');
		$this->load->library('session');
		setlocale(LC_MONETARY, 'en_US.UTF-8'); 
		$this->user_session = $this->session->userdata('user_session');
	}
	
	public function index(){	
		
		$this->load->view('errors\html\error_exception');
	
	}
}
