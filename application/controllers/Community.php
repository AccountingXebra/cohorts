<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	require ("common/Index_Controller.php");

	class Community extends Index_Controller {

		function __construct() {

			parent::__construct();

			$this->load->model('Community_model');
			//$this->load->model('Sales_model');
			$this->load->model('Market_place_model');
			$this->load->model('User_model');

			setlocale(LC_MONETARY, 'en_US.UTF-8'); 

			is_login();

			$this->user_session = $this->session->userdata('user_session');

			//is_trialExpire(); 
			//is_accessAvail(1);
			//is_BusReg();
		}

		/**** My Marketplace start****/
		
		

		public function index() {

			$res_city_ids = $this->Community_model->Get_unique_cities();
			for($i=0;$i<count($res_city_ids);$i++) {
				$arr[$i] = $res_city_ids[$i]['bus_billing_city'];
			}

			$string = implode(",",$arr);

			$res['cities'] = $this->Community_model->Get_cities($string);

			// Activity Tracker start
			$tracking_info['module_name'] = 'Community / Index';
			$tracking_info['action_taken'] = 'Viewed';
			$tracking_info['reference'] = "Community";
			$LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
			// Activity Tracker End

			
			$this->load->view('my-community/my-welcome');
			//$this->load->view('template/marketplace-sidebar');
			
		
		}
		
		public function get_cities(){
			$state_id=$this->input->post('state_id');
			$cities=$this->Community_model->selectData('cities','*', array('state_id'=>$state_id));
			$opt='';
			$opt.="<option value=''>CITY *</option>";
			foreach($cities as $st){
				$opt.="<option value=".$st->city_id." >".strtoupper($st->name)."</option>";
			}
			echo $opt;
		}
		
		public function temp(){
			$this->load->view('email_template/welcome-user-email');
		}
		
		public function my_gift(){
			$this->load->view('index/activate-gift');
		}
		
		public function my_welcome(){
			$this->load->view('my-community/my-welcome');
		}

		public function my_profile(){

			$user_id = $this->session->userdata['user_session']['reg_id'];
		$bus_id = $this->session->userdata['user_session']['bus_id'];
        

		$data['comp_profile']=$this->Community_model->selectData('businesslist', '*',array('bus_id'=>$bus_id));
		if(count($data['comp_profile'])>0){
            $data['comp_profile']=(array)$data['comp_profile'][0];
		}else{
			$data['comp_profile']=array();
		}
		$data['per_profile']=$this->Community_model->selectData('registration', '*',array('reg_id'=>$user_id));
		$data['countries']=$this->Community_model->selectData('countries', '*',array());
		$data['states']="";
		$data['cities']="";
		$data['company_type'] = $this->Community_model->selectData('company_type', '*','','id','ASC');
			$this->load->view('my-community/add_cohorts_profile',$data);
		}
		
		public function my_marketplace() {

			$res_city_ids =	$this->Community_model->Get_unique_cities();
			for($i=0;$i<count($res_city_ids);$i++) {
				$arr[$i] = $res_city_ids[$i]['bus_billing_city'];
			}

			$string = implode(",",$arr);

			$res['cities'] = $this->Community_model->Get_cities($string);
			//print_r($res['cities']);exit;

			// Activity Tracker start
			$tracking_info['module_name'] = 'Community / My Marketplace';
			$tracking_info['action_taken'] = 'Viewed';
			$tracking_info['reference'] = "My Marketplace";
			$LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
			// Activity Tracker End

			$this->load->view('my-community/Cohorts-header', $res);
			//$this->load->view('template/marketplace-sidebar');
			$this->load->view('my-community/my-marketplace');
			$this->load->view('template/footer');
		}

		public function search_company() {

			$data = $_GET;
			$res['all_data'] = $_GET;
  
			if(isset($_GET['submit'])) {

				$bus_id = $this->user_session['bus_id'];
				$reg_id = $this->user_session['reg_id'];

				$data['submit'] = $_GET['submit'];

				//$res['connections'] = $this->Community_model->getConnectionRequest($bus_id);
				//print $this->db->last_query(); exit();
				$res['result'] = $this->Community_model->getSearchnew($data);
				//echo "<pre>"; print_r(count($res['offers'])); echo "</pre>"; exit();
				//print $this->db->last_query(); exit();
				$res['post_data'] = $_GET;

				$res_city_ids =	$this->Community_model->Get_unique_cities();

				if(count($res_city_ids) > 0) {
					for($i=0; $i<count($res_city_ids); $i++) {
					$arr[$i] = $res_city_ids[$i]['bus_billing_city'];
				}

				$string = implode(",",$arr);

				$res['cities'] = $this->Community_model->Get_cities($string);
				} else {
					$res['cities'] = array();
				}

				
				if(isset($res['post_data']['multi_loc'])) {
					$str =  implode(",",$res['post_data']['multi_loc']);
					$res['selected_cities'] = $this->Community_model->Get_cities($str);
				} else {
					$res['selected_cities'] = array();
				}

				// Activity Tracker start
				$tracking_info['module_name'] = 'Community / My Marketplace / Search Company';
				$tracking_info['action_taken'] = 'Search Company';
				$tracking_info['reference'] = "My Marketplace";
				$LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
				// Activity Tracker End

				$this->load->view('my-community/Cohorts-header', $res);
				//$this->load->view('template/marketplace-sidebar', $res);
				$this->load->view('my-community/company-search-results', $res);
				$this->load->view('template/footer');

			} else {

				$res_city_ids =	$this->Community_model->Get_unique_cities();

				for($i=0;$i<count($res_city_ids);$i++) {
					$arr[$i] = $res_city_ids[$i]['bus_billing_city'];
				}

				$string = implode(",",$arr);
				$res['post_data']=$_GET;
				$res['cities'] = $this->Community_model->Get_cities($string);

				// Activity Tracker start
				$tracking_info['module_name'] = 'My Marketplace';
				$tracking_info['action_taken'] = 'Viewed';
				$tracking_info['reference'] = "My Marketplace";
				$LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
				// Activity Tracker End

				$this->load->view('my-community/Cohorts-header', $res);
			//	$this->load->view('template/marketplace-sidebar', $res);
				$this->load->view('my-community/my-marketplace', $res);
				$this->load->view('template/footer');
			}
		}

		public function company_search() {

			$res['all_data']=$_GET;

			if($this->input->post('submit')){

				$res_city_ids = $this->Community_model->Get_unique_cities();
				for($i=0;$i<count($res_city_ids);$i++) {
					$arr[$i] = $res_city_ids[$i]['bus_billing_city'];
				}

				$string = implode(",",$arr);

				$res['cities'] = $this->Community_model->Get_cities($string); //cities

				$keyword = $this->input->post('search');
				$res['result'] = $this->Community_model->getKeyword($keyword);
				//echo "<pre>"; print_r($res); echo "</pre>"; exit();

				if(isset($res['post_data']['multi_loc'])) {
					$str =  implode(",",$res['post_data']['multi_loc']);
					$res['selected_cities'] = $this->Community_model->Get_cities($str);
				} else {
					$res['selected_cities'] = array();
				}

				// Activity Tracker start
				$tracking_info['module_name'] = 'Community / My Marketplace / Search Company';
				$tracking_info['action_taken'] = 'Search';
				$tracking_info['reference'] = "My Marketplace";
				$LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
				// Activity Tracker End

				$this->load->view('my-community/Cohorts-header', $res);
				//$this->load->view('template/marketplace-sidebar', $res);
				$this->load->view('my-community/company-search-results', $res);
				$this->load->view('template/footer');
			}
		}
		
		public function company_search_connect() {

			$res['all_data']=$_GET;

			if($this->input->post('submit')){

				$res_city_ids = $this->Community_model->Get_unique_cities();
				for($i=0;$i<count($res_city_ids);$i++) {
					$arr[$i] = $res_city_ids[$i]['bus_billing_city'];
				}

				$string = implode(",",$arr);

				$res['cities'] = $this->Community_model->Get_cities($string); //cities

				$keyword = $this->input->post('search');
				$res['result'] = $this->Community_model->getKeyword($keyword);
				//echo "<pre>"; print_r($res); echo "</pre>"; exit();

				if(isset($res['post_data']['multi_loc'])) {
					$str =  implode(",",$res['post_data']['multi_loc']);
					$res['selected_cities'] = $this->Community_model->Get_cities($str);
				} else {
					$res['selected_cities'] = array();
				}

				// Activity Tracker start
				$tracking_info['module_name'] = 'Community / My Marketplace / Search Company';
				$tracking_info['action_taken'] = 'Search';
				$tracking_info['reference'] = "My Marketplace";
				$LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
				// Activity Tracker End

				$this->load->view('my-community/Cohorts-header', $res);
				//$this->load->view('template/marketplace-sidebar', $res);
				$this->load->view('my-community/company-search-connect-results', $res);
				$this->load->view('template/footer');
			}
		}

		public function view_company_details() {

			$reg_id = $this->user_session['reg_id'];
			$bus_id = $this->user_session['bus_id'];

			if($_GET['token']) {
				$data['id'] 			= $_GET['token'];
				$res['contact_person']	= $this->Market_place_model->selectData('registration', 'reg_username,reg_email,reg_mobile', array('bus_id'=>$data['id'],'reg_admin_type'=>'6'));
				$res['result']			= $this->Market_place_model->GetUserInfo($data);
				$res['name']			= $this->Community_model->getSearchnew($data);
				$res['branch']			= $this->Market_place_model->GetBranchInfo($data);
				$res['company'] 		= $this->Market_place_model->getCompanyname($data);
				$res['email'] 			= $this->Market_place_model->getRegemail($data);
				$res['contact'] 		= $this->Market_place_model->getRegcontact($data);
				$res['offers'] 			= $this->Community_model->getOffersonprofile($data);
				$res['connections'] 	= $this->Community_model->getconnectiontype($data);
				//echo "<pre>"; print_r($res['result']); echo "</pre>"; exit();
				//$this->db->last_query($res['result']); exit();
				
				if($res) {

					// Activity Tracker start
					$tracking_info['module_name'] = 'Community / My Marketplace / View Company Profile';
					$tracking_info['action_taken'] = 'Viewed';
					$tracking_info['reference'] = "My Marketplace";
					$LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
					// Activity Tracker End

					$this->load->view('my-community/view-profile-details',$res);
				} else {
					$this->load->view('my-community/my-marketplace');
				}
			} else {
				$this->load->view('my-community/my-marketplace');
			}
		}

		public function company_deals() {

			$reg_id = $this->user_session['reg_id'];
			$bus_id = $this->user_session['bus_id'];
			$data['bus_id']=$bus_id;

			if($_GET['token']) {
				$data['id'] = $_GET['token'];
				$res['contact_person']=$this->Market_place_model->selectData('registration', 'reg_username,reg_email,reg_mobile', array('bus_id'=>$data['id'],'reg_admin_type'=>'6'));
				$res['result']=$this->Market_place_model->GetUserInfo($data);
				//print $this->db->last_query(); exit();
				$res['name']=$this->Community_model->getSearchnew($data);
				$res['branch']=$this->Market_place_model->GetBranchInfo($data);
				$res['company'] = $this->Market_place_model->getCompanyname($data);
				$res['email'] = $this->Market_place_model->getRegemail($data);
				$res['contact'] = $this->Market_place_model->getRegcontact($data);
				$res['offers'] = $this->Community_model->getOffersonprofile($data);
				//echo "<pre>"; print_r($res['result']); echo "</pre>"; exit();
				//$this->db->last_query($res['company']); exit();
				
				if($res) {

					// Activity Tracker start
					$tracking_info['module_name'] = 'My Marketplace / View Company Deals';
					$tracking_info['action_taken'] = 'View Company Deals';
					$tracking_info['reference'] = "My Marketplace";
					$LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
					// Activity Tracker End

					$this->load->view('my-community/particular-company-deals',$res);
				} else {
					$this->load->view('my-community/my-marketplace');
				}
			} else {
				$this->load->view('my-community/my-marketplace');
			}
		}

		public function review_company() {

			if($_GET['token']) {
				$data['id'] = $_GET['token'];
				$res['contact_person']=$this->Market_place_model->selectData('registration', 'reg_username,reg_email,reg_mobile', array('bus_id'=>$data['id'],'reg_admin_type'=>6));
				$res['result']=$this->Market_place_model->GetUserInfo($data);
				$res['name']=$this->Community_model->getSearch($data);
				$res['branch']=$this->Market_place_model->GetBranchInfo($data);
				$res['rate'] = $this->Market_place_model->Get_rating($data);
				$res['company'] = $this->Market_place_model->getCompanyname($data);

				//echo "<pre>"; print_r($res['company']); echo "</pre>"; exit();
				//$this->db->last_query($res['company']); exit();
				
				if($res) {

					// Activity Tracker start
					$tracking_info['module_name'] = 'Community / My Marketplace / Review Company';
					$tracking_info['action_taken'] = 'Viewed';
					$tracking_info['reference'] = "My Marketplace";
					$LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
					// Activity Tracker End

					$this->load->view('my-community/Cohorts-header', $res);
					$this->load->view('my-community/review-company');
					$this->load->view('template/footer');

				} else {

					// Activity Tracker start
					$tracking_info['module_name'] = 'Community / My Marketplace';
					$tracking_info['action_taken'] = 'Viewed';
					$tracking_info['reference'] = "My Marketplace";
					$LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
					// Activity Tracker End

					$this->load->view('my-community/my-marketplace');
				}
				

			} else {

				// Activity Tracker start
				$tracking_info['module_name'] = 'Community / My Marketplace';
				$tracking_info['action_taken'] = 'Viewed';
				$tracking_info['reference'] = "My Marketplace";
				$LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
				// Activity Tracker End

				$this->load->view('my-community/my-marketplace');
			}
		}

		public function email_company() {
          $_POST=$this->security->xss_clean($_POST);
		  $csrf_token=$this->security->get_csrf_hash();
			$postdata = $_POST;
			$array = $postdata['bus_id'];

			$reg_id = $this->user_session['reg_id'];
	        $bus_id = $this->user_session['bus_id'];
	        $gst_id = $this->user_session['ei_gst'];

			$ids = explode(',',$postdata['bus_id']);

	        $count = 0;
	        //$fromemail = $this->Sales_model->selectData('registration','*',array('bus_id'=>$bus_id,'reg_id'=>$reg_id,'status'=>/'Active'));
			$fromemail = $this->Community_model->selectData('registration','*',array('bus_id'=>$postdata['bus_id']));
			$all_emails = $postdata['email_to'];
			$to = preg_split("/(,|;)/", $all_emails);
			$from = $fromemail[0]->reg_username;
			$cc = $postdata['email_cc'];
			$cc = preg_split("/(,|;)/", $cc);
			$message = $postdata['email_message'];
			$subject = $postdata['email_subject'];
			$Attach = array();
			$invoname = "";
			$status = sendInvoiceEmailMultiple($from,$to,$subject,$message,$Attach,$invoname,$cc);

			if($status) {
				//echo 1;
				echo '1'."@".$csrf_token;
			}else{
				//echo 0;
				echo '0'."@".$csrf_token;
			}
		}

		// add_connection_info
		public function add_connection_info() {

			$reg_id = $this->user_session['reg_id'];
			$bus_id = $this->user_session['bus_id'];
			$gst_id = $this->user_session['ei_gst'];
            $company=$this->Community_model->selectData('businesslist', '*', array('bus_id'=>$bus_id));
			$post = $this->input->post();
			$post=$this->security->xss_clean($post);
			$csrf_token=$this->security->get_csrf_hash();
			$post['bus_id'] = $bus_id;
			$post['reg_id'] = $reg_id;
			$post['gst_id'] = $gst_id;
			$post['status'] = '1';
			$post['conn_date'] = date("Y-m-d");
			$post['date_created'] = date('Y-m-d H:i:s');
			$post['date_updated'] = date('Y-m-d H:i:s');

			$flag = $this->Community_model->insertData('connections', $post);
           
             $send_data= $this->Community_model->selectData('registration', '*', array('bus_id'=>$post['bus_id_connected_to']));
			$notification=array(
				'bus_id'=>$send_data[0]->bus_id,
				'reg_id'=>$send_data[0]->reg_id,
				'status'=>'Active',
				'noti_read_status'=>0,
				'noti_type'=>'connection_request_recd',
				'noti_message'=>"Connections Request Received from".$company[0]->bus_company_name,
				'noti_url'=>"community/connection-requests",
				'ref_id'=>$bus_id,


			);

			$noti = $this->Community_model->insertData('notifications', $notification);
			//print $this->db->last_query(); exit();
			if($flag != false) {

				// Activity Tracker start
				$tracking_info['module_name'] = 'Community / My Connections / Connection Sent';
				$tracking_info['action_taken'] = 'Connection Sent';
				$tracking_info['reference'] = "My Connections";
				$LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
				// Activity Tracker End

			}

			$data['connections'] = $this->Community_model->selectData('connections', '*', array('bus_id'=>$bus_id,'status'=>1));
			$data['csrf_hash'] = $csrf_token;
			echo json_encode($data);
			exit();

		}

		/**** My Marketplace end****/


		/**** My Connections start****/

		public function my_connections() {

			$reg_id = $this->user_session['reg_id'];
			$bus_id = $this->user_session['bus_id'];
			$gst_id = $this->user_session['ei_gst'];

			$data['conn'] =  $this->User_model->businessData('');
			$data['result'] = $this->Community_model->getConnectionRequest($bus_id);
			$data['result1'] = $this->Community_model->getConnectionRequest1($bus_id);

			//echo "<pre>"; print_r($data['result']); echo "</pre>"; exit();

			// Activity Tracker start
			$tracking_info['module_name'] = 'Community / My Connections';
			$tracking_info['action_taken'] = 'Viewed';
			$tracking_info['reference'] = "My Connections";
			$LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
			// Activity Tracker End

			$this->load->view('my-community/Cohorts-header');
			//$this->load->view('template/sidebar');
			$this->load->view('my-community/my-connections',$data);
			$this->load->view('template/footer');
		}

		public function change_accepted() {

			$reg_id = $this->user_session['reg_id'];
			$bus_id = $this->user_session['bus_id'];
			$gst_id = $this->user_session['ei_gst'];
			$csrf_token=$this->security->get_csrf_hash();
			$company=$this->Community_model->selectData('businesslist', '*', array('bus_id'=>$bus_id));
			$post = $this->input->post();
			 $post=$this->security->xss_clean($post);

			$post['date_updated'] = date('Y-m-d H:i:s');
			$post['accepted'] = '1';

			$flag = $this->Community_model->updateData('connections', $post, "`bus_id_connected_to` = ".$bus_id." and `conn_id` = ".$post['conn_id']);

			 $send_data= $this->Community_model->selectData('connections', '*', array('bus_id_connected_to'=>$bus_id));
			 $reg=$this->Community_model->selectData('registration', '*', array('bus_id'=>$send_data[0]->bus_id));
			$notification=array(
				'bus_id'=>$send_data[0]->bus_id,
				'reg_id'=>$reg[0]->reg_id,
				'status'=>'Active',
				'noti_read_status'=>0,
				'noti_type'=>'connection_request_accept',
				'noti_message'=>"Connections Request Accepted from".$company[0]->bus_company_name,
				//'noti_url'=>"community/my-connections",
				'noti_url'=>"sales/add-client?con_id=".$post['conn_id'],
				'ref_id'=>$post['conn_id'],


			);

			$noti = $this->Community_model->insertData('notifications', $notification);
			//print $this->db->last_query(); exit();

			if($flag != false) {

				// Activity Tracker start
				$tracking_info['module_name'] = 'Community / My Connections / Request Accepted';
				$tracking_info['action_taken'] = 'Request Accepted';
				$tracking_info['reference'] = "My Connections";
				$LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
				// Activity Tracker End

				$data['connections'] = $this->Community_model->selectData('connections', '*', array('bus_id'=>$bus_id,'status'=>'1'));
				$data['status'] = true;
				$data['csrf_hash'] = $csrf_token;
				echo json_encode($data);

				//redirect('community/my-connections');
			} else {
				echo false;
			}
		}

		public function delete_request() {

			$reg_id = $this->user_session['reg_id'];
			$bus_id = $this->user_session['bus_id'];
			$gst_id = $this->user_session['ei_gst'];
			$csrf_token=$this->security->get_csrf_hash();
			$post = $this->input->post();
			 $post=$this->security->xss_clean($post);

			$flag = $this->Community_model->deleteData('connections', "`bus_id_connected_to` = '$bus_id' and `conn_id` = ".$post['conn_id']);
			//print $this->db->last_query(); exit();

			if($flag != false) {

				// Activity Tracker start
				$tracking_info['module_name'] = 'Community / My Connections / Request Deleted';
				$tracking_info['action_taken'] = 'Request Deleted';
				$tracking_info['reference'] = "My Connections";
				$LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
				// Activity Tracker End

				$data['connections'] = $this->Community_model->selectData('connections', '*', array('bus_id'=>$bus_id,'status'=>'1'));
				$data['status'] = true;
				$data['csrf_hash'] = $csrf_token;
				echo json_encode($data);

				//redirect('community/my-connections');
			} else {
				echo false;
			}
		}

		public function withdraw_sent_request() {

			$reg_id = $this->user_session['reg_id'];
			$bus_id = $this->user_session['bus_id'];
			$gst_id = $this->user_session['ei_gst'];
			$post = $this->input->post();
			$post=$this->security->xss_clean($post);
			$csrf_token=$this->security->get_csrf_hash();
			//print_r($post); exit();

			$flag = $this->Community_model->deleteData('connections', "`bus_id` = '$bus_id' and `conn_id` = ".$post['conn_id']);
			//print $this->db->last_query(); exit();

			if($flag != false) {

				// Activity Tracker start
				$tracking_info['module_name'] = 'Community / My Connections / Request Withdrawn';
				$tracking_info['action_taken'] = 'Request Withdrawn';
				$tracking_info['reference'] = "My Connections";
				$LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
				// Activity Tracker End

				$data['connections'] = $this->Community_model->selectData('connections', '*', array('bus_id'=>$bus_id,'status'=>'1'));
				$data['status'] = true;
				$data['csrf_hash'] = $csrf_token;
				echo json_encode($data);

				//redirect('community/my-connections');
			} else {
				echo false;
			}
		}

		public function connection_requests() {

			$reg_id = $this->user_session['reg_id'];
			$bus_id = $this->user_session['bus_id'];
			$gst_id = $this->user_session['ei_gst'];

			$data['result'] = $this->Community_model->getConnectionRequest12($bus_id);
			$data['result_count'] = count($data['result']);
			$data['result1'] = $this->Community_model->getConnectionSendRequest($bus_id);
			$data['result1_count'] = count($data['result1']);
			//echo "<pre>";print_r($data['result1']);echo "</pre>";exit();

			// Activity Tracker start
			$tracking_info['module_name'] = 'Community / My Connections / Connection Requests';
			$tracking_info['action_taken'] = 'Viewed';
			$tracking_info['reference'] = "My Connections";
			$LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
			// Activity Tracker End

			$this->load->view('my-community/Cohorts-header', $data);
			//$this->load->view('template/sidebar');
			$this->load->view('my-community/sent-receive-connections');
			$this->load->view('template/footer');
		}

		public function connection_sent() {

			$this->load->view('my-community/Cohorts-header', $res);
			//$this->load->view('template/sidebar');
			$this->load->view('my-community/connection-sent');
			$this->load->view('template/footer');
		}
		/**** My Connections end****/


		/**** Offers ****/

		public function deals() {

			$reg_id = $this->user_session['reg_id'];
			$bus_id = $this->user_session['bus_id'];
			
			$post = $this->input->post();
			 $post=$this->security->xss_clean($post);
			$data['offers'] = $this->Community_model->getOffers($bus_id, $reg_id, $post);
			//print_r($data['offers']); exit();
			$data['search'] = $post;
			$data['company'] = $this->Community_model->getCompany($bus_id);
			$data['email'] = $this->Community_model->getDetailsoffer($bus_id, $reg_id);
			//echo "<pre>"; print_r($data['offers']); echo "</pre>"; exit();

			// Activity Tracker start
			$tracking_info['module_name'] = 'Community / Offers';
			$tracking_info['action_taken'] = 'Viewed';
			$tracking_info['reference'] = "Offers";
			$LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
			// Activity Tracker End

			$this->load->view('my-community/offers-list', $data);
		}

		public function add_offer() {

			$reg_id = $this->user_session['reg_id'];
			$bus_id = $this->user_session['bus_id'];
			$gst_id = $this->user_session['ei_gst'];

			$data['company'] = $this->Community_model->getDetailsoffer($bus_id, $reg_id);

			$data['offers'] = $this->Community_model->selectData('offers', '*', array("bus_id" => $bus_id), 'id','DESC');

			if(count($data['offers']) > 0) {
				$last_offer_no = $data['offers'][0]->offer_number;
				$temp = str_replace("DLN", "", $last_offer_no);
				$temp = intval($temp) + 1;
				$alno_ref = str_pad($temp, 0, 0, STR_PAD_LEFT);
			} else {
				$alno_ref = str_pad(1, 0, 0, STR_PAD_LEFT);
			}

			// Activity Tracker start
			$tracking_info['module_name'] = 'Community / Offers / Add Offers';
			$tracking_info['action_taken'] = 'Add';
			$tracking_info['reference'] = "Offers";
			$LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
			// Activity Tracker End

			$data['alertNo']='DLN'.$alno_ref;
			//print_r($data['alertNo']); exit();

			$this->load->view('my-community/add-offer', $data);
		}

		public function add_new_offer() {

			$reg_id = $this->user_session['reg_id'];
			$bus_id = $this->user_session['bus_id'];



			$post = $this->input->post();
			 $post=$this->security->xss_clean($post);

			 $csrf_token=$this->security->get_csrf_hash();
			//print_r($post); exit();

			$post['bus_id'] 		= $bus_id;
			$post['reg_id'] 		= $reg_id;
			$post['status'] 		= '1';
			$post['offer_date'] 	= date("Y-m-d");
			$post['start_date'] 	= date("Y-m-d", strtotime(str_replace('/', '-',$post['start_date'])));
			$post['end_date'] 		= date("Y-m-d", strtotime(str_replace('/', '-',$post['end_date'])));
			$post['date_created'] 	= date('Y-m-d H:i:s');
			$post['date_updated'] 	= date('Y-m-d H:i:s');
			
			if(isset($_FILES['offer_image']) && $_FILES['offer_image']['name']!= "") {

				if($_FILES['offer_image']['name'] != NULL) {
					$post['offer_image'] = $_FILES['offer_image']['name'];
				}
			}

			$flag = $this->Community_model->insertData('offers', $post);
			//print $this->db->last_query(); exit();

			if($flag != false) {

				$image_info = array();
				$image_info = $_FILES;

				if(isset($image_info['offer_image']) && $image_info['offer_image']['name']!= "") {

					if($_FILES['offer_image']['name'] != NULL) {

						$files = $_FILES['offer_image'];

						$_FILES['offer_image']['name']      = $files['name'];
						$_FILES['offer_image']['type']      = $files['type'];
						$_FILES['offer_image']['tmp_name']  = $files['tmp_name'];
						$_FILES['offer_image']['error']     = $files['error'];
						$_FILES['offer_image']['size']      = $files['size'];

						$docs = $this->Common_model->upload_org_filename('offer_image','offer_image/'.$flag, $allowd="jpg|jpeg|png", array('width' => 100,'height' => 100));
					}
				}


				////////////////////////////////////Bvcohorts///////////////////////
                          
                         if($this->user_session['email']=="cohortmanager01@gmail.com"){
						     // SERVER A - UPLOAD FILE VIA CURL POST
						// (A) SETTINGS
						$urls = array();
						//array of cURL handles


						//set the urls
						$urls[] =  'https://bharatvaani.in/index/insert_offer';
						//$urls[]="http://localhost/bvcohorts/index/insert_incubators"; // Where to upload file to
						//$urls[] =  "http://localhost/bvcohorts/index/insert_incubators"; // Where to upload file to
						$urls[] =  'https://onecohort.in/index/insert_offer';
						//$url = "http://localhost/bvcohorts/index/insert_incubators"; // Where to upload file to
						$file = DOC_ROOT."/upload/offer_image/".$flag."/".$_FILES['offer_image']['name']; // File to upload
						$upname = $_FILES['offer_image']['name'] ; // File name to be uploaded as

							

						// (B) NEW CURL FILE
						$cf = new CURLFile($file, mime_content_type($file), $upname);
						
						$data=$post;

						$data['upload']=$cf;
						
						// (C) CURL INIT
						foreach ($urls as $key => $url) {
						//print $url;
						$ch = curl_init();
						curl_setopt($ch, CURLOPT_URL, $url);
						curl_setopt($ch, CURLOPT_POST, true);
						curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
						curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

						// (D) CURL RUN
						// (D1) GO!
						$result = curl_exec($ch);

						// (D2) CURL ERROR
						if (curl_errno($ch)) {
						echo "CURL ERROR - " . curl_error($ch);
						}

						// (D3) CURL OK - DO YOUR "POST UPLOAD" HERE
						else {
						// $info = curl_getinfo($ch);
						// print_r($info);
						echo $result;
						}

						// (D4) DONE
						curl_close($ch);

						}   

					}

						////////////////////////////////////Bvcohorts///////////////////////

				$this->session->set_flashdata('success',"You have successfully added a deals");
				
				redirect('community/deals');

			}

			

		}

		public function edit_offer($id) {

			$reg_id = $this->user_session['reg_id'];
			$bus_id = $this->user_session['bus_id'];

			$offerpost = $this->input->post();
			 $offerpost=$this->security->xss_clean($offerpost);
			 $csrf_token=$this->security->get_csrf_hash();
			//print_r($offerpost); exit();

			if($offerpost) {

				$offerpost['offer_date'] 	= date("Y-m-d");
				$offerpost['start_date'] 	= date("Y-m-d", strtotime(str_replace('/', '-',$offerpost['start_date'])));
				$offerpost['end_date'] 		= date("Y-m-d", strtotime(str_replace('/', '-',$offerpost['end_date'])));
				$offerpost['date_updated'] 	= date('Y-m-d H:i:s');
                 unset($offerpost['image_info_deal']);
               unset($offerpost['image_id_deal']);
				if(isset($_FILES['offer_image']) && $_FILES['offer_image']['name']!= "") {
                
				if($_FILES['offer_image']['name'] != NULL) {
					$offerpost['offer_image'] = $_FILES['offer_image']['name'];
				}
			}

				$aflag = $this->Community_model->updateData('offers', $offerpost, array('id'=>$id));
				//print $this->db->last_query(); exit();
				if($aflag != false) {

					$image_info = array();
				$image_info = $_FILES;

				if(isset($image_info['offer_image']) && $image_info['offer_image']['name']!= "") {

					if($_FILES['offer_image']['name'] != NULL) {

						$files = $_FILES['offer_image'];

						$_FILES['offer_image']['name']      = $files['name'];
						$_FILES['offer_image']['type']      = $files['type'];
						$_FILES['offer_image']['tmp_name']  = $files['tmp_name'];
						$_FILES['offer_image']['error']     = $files['error'];
						$_FILES['offer_image']['size']      = $files['size'];

						$docs = $this->Common_model->upload_org_filename('offer_image','offer_image/'.$id, $allowd="jpg|jpeg|png", array('width' => 100,'height' => 100));
					}
				}
			
				// Activity Tracker start
				$rec_info = $this->Community_model->selectData('offers', '*',array('id'=>$id));
				$tracking_info['module_name'] = 'Community / Edit Offers / Offers';
				$tracking_info['action_taken'] = 'Edit';
				$tracking_info['reference'] = 'Offers';
				$LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
				// Activity Tracker End

				$this->session->set_flashdata('success',"You have successfully update a deals");
				} else {
					$this->session->set_flashdata('error',"Error while processing...!!");
				}
				redirect('community/deals');
			}

			$data['results'] = $this->Community_model->selectData('offers', '*', " `bus_id` = ".$bus_id. " and `status` = '1' and `id` = ".$id."") ;
			//echo "<pre>"; print_r($data['results']); echo "</pre>"; exit();

			$this->load->view('my-community/edit-deals', $data);
		}

		public function delete_deal(){
			$reg_id = $this->user_session['reg_id'];
			$bus_id = $this->user_session['bus_id'];
			$id = $this->input->post('deal_id');
			$csrf_token=$this->security->get_csrf_hash();
			$flag = $this->Community_model->deleteData("offers", array('id' => $id));
			//print $this->db->last_query(); exit();
			if($flag != 0)
			{
				//echo true; 
				echo 'true'."@".$csrf_token;
				exit();
			}
		}

		/**** Offers end ****/
		
		/**** Events start ****/
		public function events(){

			$reg_id = $this->user_session['reg_id'];
			$bus_id = $this->user_session['bus_id'];

			$post = $this->input->post();
			 $post=$this->security->xss_clean($post);
			//$searchpost = $this->input->post('search');
			//print_r($searchpost); exit();
            $data['search'] = $post;
			$data['events'] = $this->Community_model->getEvents($bus_id, $reg_id, $post);
			$data['company'] = $this->Community_model->getCompany($bus_id);
			$data['email'] = $this->Community_model->getDetailsoffer($bus_id, $reg_id);
			$data['location'] = $this->Community_model->getLocation();
			//echo "<pre>"; print_r($data['location']); echo "</pre>"; exit();

			// Activity Tracker start
			$tracking_info['module_name'] = 'Community / Events';
			$tracking_info['action_taken'] = 'Viewed';
			$tracking_info['reference'] = "Events";
			$LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
			// Activity Tracker End

			$this->load->view('my-community/list-event', $data);
		}
		public function add_events(){

			$reg_id = $this->user_session['reg_id'];
			$bus_id = $this->user_session['bus_id'];
			$gst_id = $this->user_session['ei_gst'];

			$data['company'] = $this->Community_model->getDetailsoffer($bus_id, $reg_id);
			if(count($data['company'])>0){
            $data['company']=$data['company'][0];
		}else{
			$data['company']=array();
		}

			$data['events'] = $this->Community_model->selectData('events', '*', array("bus_id" => $bus_id), 'id','DESC');

			if(count($data['events']) > 0) {
				$last_event_no = $data['events'][0]->event_number;
				$temp = str_replace("EV", "", $last_event_no);
				$temp = intval($temp) + 1;
				$alno_ref = str_pad($temp, 0, 0, STR_PAD_LEFT);
			} else {
				$alno_ref = str_pad(1, 0, 0, STR_PAD_LEFT);
			}

			// Activity Tracker start
			$tracking_info['module_name'] = 'Community / Events / Add Events';
			$tracking_info['action_taken'] = 'Add';
			$tracking_info['reference'] = "Events";
			$LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
			// Activity Tracker End

			$data['alertNo']='EV'.$alno_ref;
			//print_r($data['alertNo']); exit();

			$this->load->view('my-community/add-events', $data);
		}

		public function add_new_event() {

			$reg_id = $this->user_session['reg_id'];
			$bus_id = $this->user_session['bus_id'];

			$post = $this->input->post();
			 $post=$this->security->xss_clean($post);
			 $csrf_token=$this->security->get_csrf_hash();


			$post['bus_id'] 		= $bus_id;
			$post['reg_id'] 		= $reg_id;
			$post['status'] 		= '1';
			$post['event_date'] 	= date("Y-m-d");

			$post['start_date'] 	= date("Y-m-d", strtotime(str_replace('/', '-',$post['start_date'])));
			$post['end_date'] 		= date("Y-m-d", strtotime(str_replace('/', '-',$post['end_date'])));
			
			$post['date_created'] 	= date('Y-m-d H:i:s');
			$post['date_updated'] 	= date('Y-m-d H:i:s');

			if(isset($_FILES['event_image']) && $_FILES['event_image']['name']!= "") {

				if($_FILES['event_image']['name'] != NULL) {
					$post['event_image'] = $_FILES['event_image']['name'];
				}
			}
			
			$flag = $this->Community_model->insertData('events', $post);
			//print $this->db->last_query(); exit();

			if($flag != false) {

				$image_info = array();
				$image_info = $_FILES;

				if(isset($image_info['event_image']) && $image_info['event_image']['name']!= "") {

					if($_FILES['event_image']['name'] != NULL) {

						$files = $_FILES['event_image'];

						$_FILES['event_image']['name']      = $files['name'];
						$_FILES['event_image']['type']      = $files['type'];
						$_FILES['event_image']['tmp_name']  = $files['tmp_name'];
						$_FILES['event_image']['error']     = $files['error'];
						$_FILES['event_image']['size']      = $files['size'];

						$docs = $this->Common_model->upload_org_filename('event_image','event_image/'.$flag, $allowd="jpg|jpeg|png", array('width' => 100,'height' => 100));
					}
				}


					////////////////////////////////////Bvcohorts///////////////////////
                        
                         if($this->user_session['email']=="cohortmanager01@gmail.com"){
						     // SERVER A - UPLOAD FILE VIA CURL POST
						// (A) SETTINGS
						$urls = array();
						//array of cURL handles


						//set the urls
						$urls[] =  'https://bharatvaani.in/index/insert_event';
						//$urls[]="http://localhost/bvcohorts/index/insert_incubators"; // Where to upload file to
						//$urls[] =  "http://localhost/bvcohorts/index/insert_incubators"; // Where to upload file to
						$urls[] =  'https://onecohort.in/index/insert_event';
						//$url = "http://localhost/bvcohorts/index/insert_incubators"; // Where to upload file to
						$file = DOC_ROOT."/upload/event_image/".$flag."/".$_FILES['event_image']['name']; // File to upload
						$upname = $_FILES['event_image']['name'] ; // File name to be uploaded as

							

						// (B) NEW CURL FILE
						$cf = new CURLFile($file, mime_content_type($file), $upname);
						
						$data=$post;

						$data['upload']=$cf;
						
						// (C) CURL INIT
						foreach ($urls as $key => $url) {
						//print $url;
						$ch = curl_init();
						curl_setopt($ch, CURLOPT_URL, $url);
						curl_setopt($ch, CURLOPT_POST, true);
						curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
						curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

						// (D) CURL RUN
						// (D1) GO!
						$result = curl_exec($ch);

						// (D2) CURL ERROR
						if (curl_errno($ch)) {
						echo "CURL ERROR - " . curl_error($ch);
						}

						// (D3) CURL OK - DO YOUR "POST UPLOAD" HERE
						else {
						// $info = curl_getinfo($ch);
						// print_r($info);
						echo $result;
						}

						// (D4) DONE
						curl_close($ch);

						}

					}

						////////////////////////////////////Bvcohorts///////////////////////


				$this->session->set_flashdata('success',"Your Event has been added successfully");
				
				redirect('community/events');

			}

			//$data['connections'] = $this->Community_model->selectData('events', '*', array('bus_id'=>$bus_id,'status'=>1));

			

		}

		public function edit_event($id) {

			$reg_id = $this->user_session['reg_id'];
			$bus_id = $this->user_session['bus_id'];

			$eventpost = $this->input->post();
			 $eventpost=$this->security->xss_clean($eventpost);

			 $csrf_token=$this->security->get_csrf_hash();

			if($eventpost) {

				$eventpost['event_date'] 	= date("Y-m-d");
				$eventpost['start_date'] 	= date("Y-m-d", strtotime(str_replace('/', '-',$eventpost['start_date'])));
				$eventpost['end_date'] 		= date("Y-m-d", strtotime(str_replace('/', '-',$eventpost['end_date'])));
				$eventpost['date_updated'] 	= date('Y-m-d H:i:s');
               unset($eventpost['image_info_event']);
               unset($eventpost['image_id_event']);
				if(isset($_FILES['event_image']) && $_FILES['event_image']['name']!= "") {

					if($_FILES['event_image']['name'] != NULL) {
						$eventpost['event_image'] = $_FILES['event_image']['name'];
					}
				}

				$aflag = $this->Community_model->updateData('events', $eventpost, array('id'=>$id));
				//print $this->db->last_query(); exit();

				if($aflag != false) {

				$image_info = array();
				$image_info = $_FILES;

				if(isset($image_info['event_image']) && $image_info['event_image']['name']!= "") {

					if($_FILES['event_image']['name'] != NULL) {

						$files = $_FILES['event_image'];

						$_FILES['event_image']['name']      = $files['name'];
						$_FILES['event_image']['type']      = $files['type'];
						$_FILES['event_image']['tmp_name']  = $files['tmp_name'];
						$_FILES['event_image']['error']     = $files['error'];
						$_FILES['event_image']['size']      = $files['size'];

						$docs = $this->Common_model->upload_org_filename('event_image','event_image/'.$id, $allowd="jpg|jpeg|png", array('width' => 100,'height' => 100));
					}
				}
			
				// Activity Tracker start
				$rec_info = $this->Community_model->selectData('events', '*',array('id'=>$id));
				$tracking_info['module_name'] = 'Community / Edit Event / Events';
				$tracking_info['action_taken'] = 'Edited';
				$tracking_info['reference'] = 'Events';
				$LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
				// Activity Tracker End

				$this->session->set_flashdata('success',"Your Event has been updated successfully");
				} else {
					$this->session->set_flashdata('error',"Error while processing...!!");
				}
				redirect('community/events');
			}

			$data['results'] = $this->Community_model->selectData('events', '*', " `bus_id` = ".$bus_id. " and `status` = '1' and `id` = ".$id."") ;
			//echo "<pre>"; print_r($data['results']); echo "</pre>"; exit();

			$this->load->view('my-community/edit-event', $data);
		}

		public function delete_event() {
			$id = $this->input->post('event_id');
			$bus_id = $this->user_session['bus_id'];
			$csrf_token=$this->security->get_csrf_hash();
			$filter = array(
				'status' =>$this->input->post('status'),
			);
        	
			$flag = $this->Community_model->deleteData('events', array('id'=>$id));
			//print $this->db->last_query(); exit();
			if($flag != false) {

				// Activity Tracker start
				$rec_info = $this->Community_model->selectData('events', '*',array('id'=>$id));
				$tracking_info['module_name'] = 'Community / Event';
				$tracking_info['action_taken'] = 'Delete';
				$tracking_info['reference'] = 'Delete Event';
				$LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
				// Activity Tracker End

				//echo true;
				echo 'true'."@".$csrf_token;
			}
			else
			{
				//echo false;
				echo 'false'."@".$csrf_token;
			}
		}

		/**** Events ends ****/

		/*** Manage Profile **/

		public function manage_profile() {

			  $reg_id=$this->session->userdata['user_session']['reg_id'];
        $bus_id=$this->session->userdata['user_session']['bus_id'];



			if($_GET['token']) {
				$data['id'] = $_GET['token'];

                if($data['id']!=$bus_id) { 

			 	$this->session->set_flashdata('error',"You are not authorized to view this profile.!!");
             redirect('community');
			 }


				$res['contact_person']	= $this->Market_place_model->selectData('registration', 'reg_username,reg_email,reg_mobile', array('bus_id'=>$data['id'],'reg_admin_type'=>'3'));
				$res['result']			= $this->Market_place_model->GetUserInfo($data);
				$res['name']			= $this->Community_model->getSearchnew($data);
				$res['branch']			= $this->Market_place_model->GetBranchInfo($data);
				$res['company'] 		= $this->Market_place_model->getCompanyname($data);
				$res['email'] 			= $this->Market_place_model->getRegemail($data);
				$res['contact'] 		= $this->Market_place_model->getRegcontact($data);
				$res['offers'] 			= $this->Community_model->getOffersonprofile($data);
				$res['connections'] 	= $this->Community_model->getconnectiontype($data);
				//echo "<pre>"; print_r($res['result']); echo "</pre>"; exit();
				//$this->db->last_query($res['result']); exit();
				
				if($res) {

					// Activity Tracker start
					$tracking_info['module_name'] = 'Community / Manage Profile';
					$tracking_info['action_taken'] = 'Viewed';
					$tracking_info['reference'] = "Manage Profile";
					$LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
					// Activity Tracker End
					$res['comp_profile']=$this->Community_model->selectData('businesslist', '*',array('reg_id'=>$reg_id));
					if(count($res['comp_profile'])>0){
						$res['comp_profile']=(array)$res['comp_profile'][0];
					}else{
						$res['comp_profile']=array();
					}      
					
					$this->load->view('my-community/manage-profile',$res);
				} else {
					$this->load->view('my-community/my-marketplace');
				}
			} else {
				$this->load->view('my-community/my-marketplace');
			}
		}

		public function insert_company_profile(){

			$user_id = $this->session->userdata['user_session']['reg_id'];
		$bus_id = $this->session->userdata['user_session']['bus_id'];
          $post=$this->input->post();
          $post=$this->security->xss_clean($post);
          $csrf_token=$this->security->get_csrf_hash();

		$comp_profile=$this->Community_model->selectData('businesslist', '*',array('reg_id'=>$user_id));
		$per_profile=$this->Community_model->selectData('registration', '*',array('reg_id'=>$user_id));
        if($post){

        	$dataCompany=array();
        	$datapersonal=array();

        	$dataCompany['bus_company_name']=$post['bus_company_name'];
        	$dataCompany['bus_billing_address']=$post['bus_billing_address'];
        	$dataCompany['bus_billing_country']=$post['bus_billing_country'];
        	$dataCompany['bus_billing_state']=$post['bus_billing_state'];
        	$dataCompany['bus_billing_city']=$post['bus_billing_city'];
        		$dataCompany['bus_billing_zipcode']=$post['bus_billing_zipcode'];
        			$dataCompany['bus_company_type']=$post['bus_company_type'];
        			
                         if($post['bus_incorporation_date'] != ''){
			$dataCompany['bus_incorporation_date'] = date("Y-m-d", strtotime(str_replace('/', '-',($post['bus_incorporation_date']))));
		}	
		else {
			$dataCompany['bus_incorporation_date'] ='';
		}
        				$dataCompany['bus_company_size']=$post['bus_company_size'];
        		$dataCompany['bus_company_revenue']=$post['bus_company_revenue'];
        			$dataCompany['bus_website_url']=$post['bus_website_url'];
        				$dataCompany['bus_loc']=$post['bus_loc'];
        					$dataCompany['bus_case_study']=$post['bus_case_study'];
        		$dataCompany['bus_facebook']=$post['bus_facebook'];
        			$dataCompany['bus_twitter']=$post['bus_twitter'];
        				$dataCompany['bus_linkedin']=$post['bus_linkedin'];

        					$dataCompany['bus_instagram']=$post['bus_instagram'];
        			$dataCompany['bus_youtube']=$post['bus_youtube'];
        				$dataCompany['bus_googleplus']=$post['bus_googleplus'];
						$dataCompany['bus_user_on_ca']=$post['bus_user_on_ca'];
						$service_info = $this->session->userdata('company_services_keywords');

						if ($service_info != '') {
							$dataCompany['bus_services_keywords'] = ltrim(implode('|@|', $service_info), '|@|');
						}




//////////////////////////////////////////////////////////////////////////////////////////////////////////////
        	$datapersonal['reg_username']=$post['first_name']." ".$post['last_name'];
        	$datapersonal['reg_email']=$post['reg_email'];
        	$datapersonal['reg_mobile']=$post['reg_mobile'];
        	$datapersonal['reg_designation']=$post['reg_designation'];
       
        	
		if(count($comp_profile)>0){
           
            if($_FILES['bus_company_logo']['name'] !="")
		{
			$image_info1 = array();
			$image_info1 = $_FILES;
			
			if($image_info1['bus_company_logo']['name'] != '')
			{
				$profile_image=$this->Common_model->upload_org_filename('bus_company_logo','company_logos/'.$bus_id,$allowd="jpg|jpeg|png",array('width'=>200,'height'=>300));
	            if($profile_image!=false){
	                $dataCompany['bus_company_logo']=$profile_image['file_name'];

	               
	            }
			}	
		}

          $get_comp_update_id = $this->Community_model->updateData('businesslist',$dataCompany,array('bus_id'=>$bus_id));	
         $comp_id=$bus_id;
		}else{
           	  if($_FILES['bus_company_logo']['name'] !="")
		{
			  $dataCompany['bus_company_logo']=$_FILES['bus_company_logo']['name'];
		}
		   $dataCompany['reg_id']=$user_id;
           $insert_comp_profile = $this->Community_model->insertData('businesslist',$dataCompany);	

           if($_FILES['bus_company_logo']['name'] !="")
		{
			$image_info1 = array();
			$image_info1 = $_FILES;
			
			if($image_info1['bus_company_logo']['name'] != '')
			{
				$profile_image=$this->Common_model->upload_org_filename('bus_company_logo','company_logos/'.$insert_comp_profile,$allowd="jpg|jpeg|png",array('width'=>200,'height'=>300));
	           
			}	
		}

		$this->Community_model->updateData('registration',array('bus_id'=>$insert_comp_profile),array('reg_id'=>$user_id));
		$comp_id=$insert_comp_profile;
		}
   ////////////////////////////////////////////////////////////////////////////////////////

		if(count($per_profile)>0){
			     
         if($_FILES['reg_profile_image']['name'] !="")
		{
			$image_info1 = array();
			$image_info1 = $_FILES;
			  // print_r($image_info1['reg_profile_image']['name']);exit;
			if($image_info1['reg_profile_image']['name'] != '')
			{
				$profile_image=$this->Common_model->upload_org_filename('reg_profile_image','personal_images/'.$user_id,$allowd="jpg|jpeg|png",array('width'=>200,'height'=>300));
	            if($profile_image!=false){
	                $datapersonal['reg_profile_image']=$profile_image['file_name'];

	                if($this->session->userdata['user_session']['reg_id'] == $user_id)
	                {
	                	$this->session->userdata['user_session']['user_photo'] = $datapersonal['reg_profile_image'];
	                }
	            }
			}	
		}

          $get_update_id = $this->Community_model->updateData('registration',$datapersonal,array('reg_id'=>$user_id));		

			
           
		} else{
			  if($_FILES['reg_profile_image']['name'] !="")
		{
			 $datapersonal['reg_profile_image']=$_FILES['reg_profile_image']['name'];
		}
           $insert_profile = $this->Community_model->insertData('registration',$datapersonal);	

           if($_FILES['reg_profile_image']['name'] !="")
		{
			$image_info1 = array();
			$image_info1 = $_FILES;
			
			if($image_info1['reg_profile_image']['name'] != '')
			{
				$profile_image=$this->Common_model->upload_org_filename('reg_profile_image','personal_images/'.$insert_profile,$allowd="jpg|jpeg|png",array('width'=>200,'height'=>300));
	           
			}	
		}
		}
	

	   

	}

        $this->session->set_flashdata('success',"Data has been saved successfully");	
         redirect('community/manage_profile?token='.$comp_id);
		}

		public function update_profile($id="") {

			$this->load->model('Profile_model');

		//$id = str_replace(array('-', '_', '~'), array('+', '/', '='), $id);
		//$org_id = $this->encrypt->decode($id);
		$user_id = $this->session->userdata['user_session']['reg_id'];
		$id = $this->session->userdata['user_session']['bus_id'];

		$data['company_profile'] = $this->Profile_model->selectData('businesslist', '*', array('bus_id'=>$id));
		 
		//$this->data['services'] = $this->Profile_model->get_by_condition('EI_services',array('company_id'=>$org_id));
		$data['countries']	=	$this->Profile_model->selectData('countries', '*','','country_id','ASC');
		$data['currency']	=	$this->Profile_model->selectData('currency', '*','','currency_id','ASC');
		$data['company_type'] = $this->Profile_model->selectData('company_type', '*','','id','ASC');
		$data['legal_documents'] = $this->Profile_model->selectData('legal_document', '*',array('bus_id'=>$id,'legal_type'=>'company'));
		$data['other_documents'] = $this->Profile_model->selectData('other_document', '*',array('bus_id'=>$id,'other_type'=>'company'));
		//$data['gst_details'] = $this->Profile_model->selectData('gst_number','*', array('bus_id'=>$id,'status'=>'Active'));
		$data['gst_details'] = $this->Profile_model->selectData('gst_number','*', array('bus_id'=>$id,'type'=>'business'));
		$data['branch_details'] = $this->Profile_model->selectData('company_branch', '*', array('bus_id'=>$id));
		$data['bank_details'] = $this->Profile_model->get_company_bank_details($id);

		$data['card_details'] = $this->Profile_model->selectData('company_cards','*', array('bus_id'=>$id));


		$cmp_profile = $data['company_profile'][0];
		if($cmp_profile->bus_billing_country != '')
		{
			$data['state']=$this->Profile_model->selectData('states', '*',array('country_id'=>$cmp_profile->bus_billing_country),'state_id','ASC');	
		}
		if($cmp_profile->bus_shipping_country != '')
		{
			$data['shipping_state']=$this->Profile_model->selectData('states', '*',array('country_id'=>$cmp_profile->bus_shipping_country),'state_id','ASC');	
		}
		if($cmp_profile->bus_billing_state!= '')
		{
			$data['cities']=$this->Profile_model->selectData('cities', '*',array('state_id'=>$cmp_profile->bus_billing_state),'city_id','ASC');	
		}
		if($cmp_profile->bus_shipping_state != '')
		{
			$data['shipping_cities']=$this->Profile_model->selectData('cities', '*',array('state_id'=>$cmp_profile->bus_shipping_state),'city_id','ASC');	
		}

		// Activity Tracker start
		$tracking_info['module_name'] = 'Community / Manage Profile / Update Profile';
		$tracking_info['action_taken'] = 'Update';
		$tracking_info['reference'] = "Manage Profile";
		$LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
		// Activity Tracker End

		$this->load->view('my-community/update-profile',$data);
	}
	
	public function get_event_details() {
		$post=$this->input->post();
		 $post=$this->security->xss_clean($post);
		$post['length']=-1;
           $reg_id = $this->user_session['reg_id'];
			$bus_id = $this->user_session['bus_id'];
		   $events= $this->Community_model->getEvents($bus_id, $reg_id, $post);
			$company = $this->Community_model->getCompany($bus_id);
			$email = $this->Community_model->getDetailsoffer($bus_id, $reg_id);
			$location= $this->Community_model->getLocation();

         $records = array();
         $records["data"] = array();
         $csrf_token=$this->security->get_csrf_hash();

	  	$records['csrf_hash']=$csrf_token;
		    foreach ($events as $key => $value) {
					if($value['event_image'] != '') {
						$image = '<img src="/xebra/public/upload/event_image/'.$value['id'].'/'.$value['event_image'].'" height="130px" class="logo_style_2" width="130px">';
					} else {
						$image = '<p style="text-align:center;">No Image</p>';
						//$image = '<img src="'.base_url().'public/upload/event_image/'.$value['id'].'/'.$value['event_image'].'" height="130px" class="logo_style" width="130px">';
						
					}
		  
			//$image = '<div class="img-offer"><img height="130px" width="130" src="'.base_url().'asset/images/users.png"></div>';			
			$middle_part = '<div class="row">
						<div class="col s12 m12 l9">
							<h5 style="font-size:20px; color:#7864e9 !important;"><strong>'. strtoupper($value['event_name']).'</strong></h5>
						</div>
						<div class="col s12 m12 l3" style="margin: -40px 0px 4px 73%;">
							<a style="color:#fff;" class="btn btn-theme btn-large right set_remin" href="'. base_url().'settings/add-event-activity-alert/'.$value['id'].'" target="_blank"><img src="'.base_url().'public/images/clock.png" style="width: 20px; margin: 0px 0px -6px 0px;">&nbsp; SET REMINDER</a>
						</div>
						</div>
						<div class="row">
						<div class="col s12 m12 l12">
						<div class="col s12 m12 l4" style="margin-left:-12px;">
							<p class="offer_p"><i class="fas fa-calendar-alt"></i> <strong>EVENT DATE: </strong>'.strtoupper(date("jS M, y", strtotime($value["start_date"])).' to '.date("jS M, y", strtotime($value["end_date"]))).'</p>
						</div>
						<div class="col s12 m12 l4" style="margin-left: -10px;">
							<p class="offer_p" style="white-space: initial !important;"><i class="fas fa-globe"></i> <strong>WEBSITE: </strong>'.strtoupper(wordwrap($value["website_link"],30,"<br />\n")).'</p>
						</div>
						<div class="col s12 m12 l4" style="margin-left: -10px;">
							<p class="offer_p" style="margin-left: -12px !important;"><i class="fas fa-map-marker-alt"></i> <strong>LOCATION: </strong>'.strtoupper(wordwrap($value["event_address"].', '.$value["event_city"],38,"<br>\n")).'</p>
						</div>
						</div>
						</div>
						<!--div class="col s12 m12 l12" style="margin-top: -15px;">
							<p class="offer_p" style="margin-left: -12px !important;"><strong>LOCATION:</strong>'.$value["event_address"].','.$value["event_city"].'</p>
						</div-->
						<div class="row offer-time" id="morelist'.$value["id"].'" hidden>
							<div class="col s12 m12 l12 offer_description" style="margin: -10px 0px -5px 0 !important;">
								<p style="text-align: justify;"><i class="fas fa-info-circle"></i> '.strtoupper(wordwrap($value["event_description"],140,"<br>\n")).'</p>
							</div>
							<div class="col s12 m12 l12" style="margin-left:7px; background-color:#50E3C2; border-top-left-radius:5px; border-top-right-radius:5px; width:97%;">
								<div class="col s12 m12 l4" style="margin-left:-1.6%;">
									<p class="offer_p"><i class="fas fa-building"></i> <strong>COMPANY NAME: </strong> '.strtoupper(wordwrap($value["company_name"],24,"<br>\n")).'</p>
								</div>
								<div class="col s12 m12 l4" style="margin-left:-12px;">
									<p class="offer_p"><i class="far fa-id-card"></i> <strong>CONTACT NAME: </strong>'.strtoupper($value["contact_name"]).'</p>
								</div>
								<div class="col s12 m12 l4" style="margin-left: -20px;">
									<p class="offer_p"><i class="fas fa-coins"></i> <strong>ENTRY FEE: </strong>'.$value["entry_fee"].'</p>
								</div>
							</div>
						</div>
						<div class="row offer-time" id="morelist1'.$value["id"].'" hidden>
							<div class="col s12 m12 l12" style="margin:-12px 0 0 7px; background-color:#50E3C2; border-bottom-left-radius:5px; border-bottom-right-radius:5px; width:97%;">
								<div class="col s12 m12 l4" style="margin-left:-1.6%;">
									<p class="offer_p"><i class="fas fa-mobile-alt"></i> <strong>MOBILE NUMBER: </strong>'.$value["mobile_no"].'</p>
								</div>
								<div class="col s12 m12 l4" style="margin-left:-12px;">
									<p class="offer_p eve-email"><a href="javascript:void(0);" onclick="email_event(01);"><i class="fas fa-envelope"></i> <strong>EMAIL: </strong>'.strtoupper($value["email"]).'</a></p>
								</div>
							</div>
						</div>

						<div class="row" style="text-align: center; margin: 10px 0 0 -200px;">
							<div class="col l12 s12 m12">
								<a href="javascript:void(0);" id="showmorelist'.$value["id"].'" class="more showmore active" title="Show More" onclick="showmorelist('.$value["id"].')">SHOW MORE</a>
								<a href="javascript:void(0);" id="less'.$value["id"].'" class="less showmore" title="Show More" onclick="showmorelist('.$value["id"].')" hidden>SHOW LESS</a>
							</div>
						</div>';
			$vid=$value['id'];	
			if ($value['bus_id'] == $bus_id) {			
			$action  = "<div style='top:20px;' class='dropdown-button action-more border-radius-3 btn-default default-width no-background' href='#' data-activates='dropdown$vid'><i class='action-dot'></i></div>
			 	<ul id='dropdown01' class='dropdown-content'>
				<li><a href=".base_url()."community/edit-event/".$value["id"].'><i class="material-icons" style="color: #000;">mode_edit</i>EDIT</a></li>
				<li><a href="javascript:void(0);" class="deactive_event" data-cd_id="'.$value['id'].'"><i class="material-icons">delete</i>DELETE</a></li>';
			
			$action  .= '</ul>


			  <div class="status-action">

				<i class="action-status-icon"></i>

			  </div>';		
			  }else{
			  	$action="";
			  }	
			
	  		$records["data"][] = array(
				$image,
				$middle_part,
				$action,
	  		);
         }
	  	$records["draw"] = intval($post['draw']);
		$records["recordsTotal"] = count($events);
		$records["recordsFiltered"] = count($events);	

		echo json_encode($records);			  
		exit; 	
	}
	
	public function get_deal_details() {

              $post=$this->input->post();
               $post=$this->security->xss_clean($post);
		$post['length']=-1;
           $reg_id = $this->user_session['reg_id'];
			$bus_id = $this->user_session['bus_id'];
		  $offers = $this->Community_model->getOffers($bus_id, $reg_id, $post);
			//print_r($data['offers']); exit();
			$company = $this->Community_model->getCompany($bus_id);
			$email= $this->Community_model->getDetailsoffer($bus_id, $reg_id);

       $records = array();
         $records["data"] = array();
         $csrf_token=$this->security->get_csrf_hash();

	  	$records['csrf_hash']=$csrf_token;
                  foreach($offers as $off){
                  	if($off['offer_image'] != '') {
										$image = '<img src="/xebra/public/upload/offer_image/'.$off['id'].'/'.$off['offer_image'].'" height="130px" class="logo_style_2" width="130px">';
									} else {
										$image = '<p style="text-align:center;">No Image</p>';
										//$image = '<img src="'.base_url().'public/upload/offer_image/'.$off['id'].'/'.$off['offer_image'].'" height="130px" class="logo_style" width="130px">';
									}
						
			$middle_part = '<div class="row">
						<div class="col s12 m12 l9">
							<h5 style="font-size:20px; color:#7864e9 !important;"><strong>'.strtoupper($off['offer_title']).'</strong></h5>
						</div>
						<div class="col s12 m12 l3" style="margin: -40px 0px 4px 73%;">
							<a style="color:#fff;" class="btn btn-theme btn-large right set_remin" href="'.base_url().'settings/add-offer-activity-alert/'.$off['id'].'" target="_blank"><img src="'.base_url().'public/images/clock.png" style="width: 20px; margin: 0px 0px -6px 0px;">&nbsp; SET REMINDER</a>
						</div>
						</div>
						<div class="row">
						<div class="col s12 m12 l12">
						<div class="col s12 m12 l5" style="margin-left:-12px;">
							<p class="offer_p"><i class="fas fa-calendar-alt"></i> <strong>VALIDITY DATE:</strong> '.strtoupper(date("jS M, y", strtotime($off["start_date"])).' to '. date("jS M, y", strtotime($off["end_date"]))).'</p>
						</div>

						<div class="col s12 m6 l5">
							<p class="offer_p"><strong>VALIDITY TIME:</strong> '.strtoupper($off["start_time"].' to '. $off["end_time"] ?? '').'</p>
						</div>
						</div>
						</div>
						<div class="col s12 m12 l12" style="margin: -15px 0 0 0px;">
							<p class="offer_p" style="margin-left: -12px !important; white-space: initial !important;"><i class="fas fa-globe"></i> <strong>WEBSITE:</strong> '. strtoupper(wordwrap($off["website_link"],100,"<br>\n")?? '').'</p>
						</div>
						<div class="row offer-time" id="morelist'.$off["id"].'" hidden>
							<div class="col s12 m12 l12 offer_description" style="margin: -10px 0px 0px 0 !important;">
								<p style="text-align: justify;"><i class="fas fa-info-circle"></i> '. strtoupper(wordwrap($off["offer_description"],140,"<br>\n")?? '').'</p>
							</div>
							<div class="col s12 m12 l12" style="margin-left:7px; background-color:#50E3C2; border-radius:5px; width:97%;">
								<div class="col s12 m12 l5" style="margin: 0 12px 0 -1.7%;">
									<p class="offer_p"><i class="fas fa-building"></i> <strong>COMPANY NAME:</strong> '. strtoupper(wordwrap($off["company_name"],24,"<br>\n") ?? '').'</p>
								</div>
								<div class="col s12 m12 l6" style="margin-left:-12px;">
									<p class="offer_p"><i class="far fa-id-card"></i> <strong>CONTACT NAME: </strong>'. strtoupper($off["contact_name"] ?? '').'</p>
								</div>
							</div>
						</div>
						<div class="row offer-time" id="morelist1<?php //echo $off[id];?>" hidden>
							<div class="col s12 m12 l12" style="margin:-12px 0 0 7px; background-color:#50E3C2; border-bottom-left-radius:5px; border-bottom-right-radius:5px; width:97%;">
								<div class="col s12 m12 l5" style="margin:0px 12px 0 0px;">
									<p class="offer_p"><a href="javascript:void(0);" onclick="email_deals(01);"><i class="fas fa-envelope"></i> <strong>EMAIL:</strong> '. strtoupper($off["email"] ?? '').'</a></p>
								</div>
								<div class="col s12 m12 l6" style="margin: 0px 0 0 -1.7%;">
									<p class="offer_p"><i class="fas fa-mobile-alt"></i> <strong>MOBILE NUMBER:</strong> '.$off["mobile_no"].'</p>
								</div>
							</div>
						</div>

						<div class="row" style="text-align: center; margin: 10px 0 0 -200px;">
							<div class="col l12 s12 m12">
								<a href="javascript:void(0);" id="showmorelist'.$off["id"].'" class="more showmore active" title="Show More" onclick="showmorelist('.$off["id"].')">SHOW MORE</a>
								<a href="javascript:void(0);" id="less'. $off["id"].'" class="less showmore" title="Show More" onclick="showmorelist('.$off["id"].')" hidden>SHOW LESS</a>
							</div>
						</div>';
			$vid=$off["id"];		
			if ($off['bus_id'] == $bus_id) {			
			$action  = "<div style='top:8px;' class='dropdown-button action-more border-radius-3 btn-default default-width no-background' href='#' data-activates='dropdown$vid'><i class='action-dot'></i></div>
			 	<ul id='dropdown$vid' class='dropdown-content'>
				<li><a href=".base_url()."community/edit-offer/".$off["id"].'><i class="material-icons" style="color: #000;">mode_edit</i>EDIT</a></li>
				<li><a href="javascript:void(0);" class="deactive_deal" data-deal_id='.$off["id"].'><i class="material-icons">delete</i>DELETE</a></li>';
			
			$action  .= '</ul>

			  <div class="status-action">

				<i class="action-status-icon"></i>

			  </div>';
			  }else{
			  	$action="";
			  }			
			
	  		$records["data"][] = array(
				$image,
				$middle_part,
				$action,
	  		);
	  	}

	  	$records["draw"] = intval($post['draw']);
		$records["recordsTotal"] = count($offers);
		$records["recordsFiltered"] = count($offers);	

		echo json_encode($records);			  
		exit; 	
	}
	
	public function cohorts_faqs() {
		$this->load->view('my-community/cohorts-faqs');
	}
	
	public function cohorts_blog() {
		$this->load->view('my-community/cohorts-blog');
	}
	
	public function cohorts_blog_first() {
		$this->load->view('my-community/cohorts_blog/co_article_1');
	}
	
	public function coworking_list(){

		$data['name']=$this->Community_model->selectData('coworking_space',"name");
		$data['country']=$this->Community_model->selectData('coworking_space',"DISTINCT(country)");
		$data['city']=$this->Community_model->selectData('coworking_space',"DISTINCT(city)");


		
		
		$this->load->view('my-community/coworking_list',$data);
		
	}
	
	public function get_coworking_details() {
         $post['length']=-1;
          

         $records = array();
         $records["data"] = array();
         $csrf_token=$this->security->get_csrf_hash();

	  	$records['csrf_hash']=$csrf_token;

          $post=$this->input->post();
           $post=$this->security->xss_clean($post);
		 $working_space= $this->Community_model->getWorkspace($post);
		 foreach ($working_space as $key => $value) {
		if($value['image'] != '') {
			$image = '<img src='.DOC_ROOT_DOWNLOAD_PATH_CO.'co_working_image/'.$value['id'].'/'.$value['image'].' height="auto" class="logo_style_2" width="130px">';
		} else {
			$image = '<p style="text-align:center;">No Image</p>';				
		}


		$country=$this->Community_model->selectData('countries',"*",array('country_id'=>$value['country']));

		$cities=$this->Community_model->selectData('cities',"*",array('city_id'=>$value['city']));

		if(count($country)>0){
			$ctry=$country[0]->country_name;
		}else{
			$ctry="";
		}


		if(count($cities)>0){
    $city=$cities[0]->name;
		}else{
           $city="";
		}
		 
		 
		$middle_part = '<div class="row">
						<div class="col s12 m12 l12">
							<h5 style="font-size:20px; color:#7864e9 !important;"><strong>'.$value['name'].'</strong></h5>
						</div>
						</div>
						<div class="row">
						<div class="col s12 m12 l12">
						<div class="col s12 m12 l7" style="margin-left:-12px;">
							<p class="offer_p"><i class="fas fa-calendar-alt"></i> <strong>ADDRESS: '.wordwrap($value['address'],75,"<br>\n").' </strong></p>
						</div>
						<div class="col s12 m12 l3" style="margin-left: -10px;">
							<p class="offer_p"><i class="fas fa-globe"></i> <strong>COUNTRY: '.$ctry.' </strong></p>
						</div>
						<div class="col s12 m12 l2 city-row">
							<p class="offer_p" style="margin-left: -12px !important;"><i class="fas fa-map-marker-alt"></i> <strong>CITY: '.$city.' </strong></p>
						</div>
						</div>
						</div>
						
						<div class="row offer-time" id="morelist'.$value['id'].'" hidden>
							<div class="col s12 m12 l12" style="margin:3px 0 0 7px; background-color:#50E3C2; border-radius:5px; width:97%;">
								<div class="col s12 m12 l7" style="margin-left:-15px;">
									<p class="offer_p eve-email"><a href="javascript:void(0);" onclick="email_event(01);"><i class="fas fa-envelope"></i> <strong>EMAIL: '.$value['email_id'].' </strong></a></p>
								</div>
								<div class="col s12 m12 l4" style="margin-left:0.6%;">
									<p class="offer_p"><i class="fas fa-mobile-alt"></i> <strong>CONTACT NUMBER: '.$value['contact_no'].' </strong></p>
								</div>
							</div>
						</div>

						<div class="row" style="text-align: center; margin: 10px 0 0 -200px;">
							<div class="col l12 s12 m12">
								<a href="javascript:void(0);" id="showmorelist'.$value["id"].'" class="more showmore active" title="Show More" onclick="showmorelist('.$value['id'].')">SHOW MORE</a>
								<a href="javascript:void(0);" id="less'.$value["id"].'" class="less showmore" title="Show More" onclick="showmorelist('.$value['id'].')" hidden>SHOW LESS</a>
							</div>
						</div>';
						
		/*	$action  = "<div style='top:20px;' class='dropdown-button action-more border-radius-3 btn-default default-width no-background' href='#' data-activates='dropdown01'><i class='action-dot'></i></div>
			 	<ul id='dropdown01' class='dropdown-content'>
				<li><a href=".base_url()."admin_dashboard/edit_coworking_space/".$value['id'].'><i class="material-icons" style="color: #000;">mode_edit</i>EDIT</a></li>
				<li><a href="javascript:void(0);" class="deactive_space" data-cd_id="'.$value['id'].'"><i class="material-icons">delete</i>DELETE</a></li>';
			
			$action .= '</ul>


			  <div class="status-action">

				<i class="action-status-icon"></i>

			  </div>';	*/	
			
	  		$records["data"][] = array(
				$image,
				$middle_part,
				"",
	  		);
	  	 }
	  	$records["draw"] = intval($post['draw']);
		$records["recordsTotal"] = count($working_space);
		$records["recordsFiltered"] = count($working_space);	

		echo json_encode($records);			  
		exit; 	
	}

	
	public function incubator_list(){
			$data['name']=$this->Community_model->selectData(' incubation',"name");
		$data['country']=$this->Community_model->selectData(' incubation',"DISTINCT(country)");
		$data['city']=$this->Community_model->selectData(' incubation',"DISTINCT(city)");
		
		$this->load->view('my-community/incubator_list',$data);
		
	}
	
	public function get_incubator_details() {
		 $post['length']=-1;

         $records = array();
         $records["data"] = array();
         $csrf_token=$this->security->get_csrf_hash();

	  	$records['csrf_hash']=$csrf_token;

          $post=$this->input->post();
           $post=$this->security->xss_clean($post);
		 $incube= $this->Community_model->getIncubation($post);
		 foreach ($incube as $key => $value) {
		if($value['image'] != '') {
			$image = '<img src="'.DOC_ROOT_DOWNLOAD_PATH_CO.'incubator_image/'.$value['id'].'/'.$value['image'].'" height="auto" class="logo_style_2" width="130px">';
		} else {
			$image = '<p style="text-align:center;">No Image</p>';				
		}


		$country=$this->Community_model->selectData('countries',"*",array('country_id'=>$value['country']));

		$cities=$this->Community_model->selectData('cities',"*",array('city_id'=>$value['city']));

		if(count($country)>0){
    $ctry=$country[0]->country_name;
		}else{
           $ctry="";
		}


		if(count($cities)>0){
    $city=$cities[0]->name;
		}else{
           $city="";
		}
		 
		 
		$middle_part = '<div class="row">
						<div class="col s12 m12 l12">
							<h5 style="font-size:20px; color:#7864e9 !important;"><strong>'.$value['name'].'</strong></h5>
						</div>
						</div>
						<div class="row" style="margin-top:-20px;">
						<div class="col s12 m12 l12">
						<div class="col s12 m12 l7" style="margin-left:-12px;">
							<p class="offer_p"><i class="fas fa-calendar-alt"></i> <strong>ADDRESS: '.wordwrap($value['address'],75,"<br>\n").' </strong></p>
						</div>
						<div class="col s12 m12 l3" style="margin-left: -10px;">
							<p class="offer_p"><i class="fas fa-globe"></i> <strong>COUNTRY: '.$ctry.' </strong></p>
						</div>
						<div class="col s12 m12 l2" style="margin-left: -10px;">
							<p class="offer_p" style="margin-left: -12px !important;"><i class="fas fa-map-marker-alt"></i> <strong>CITY: '.$city.' </strong></p>
						</div>
						</div>
						</div>
						
						<div class="row offer-time" id="morelist'.$value['id'].'" hidden>
							<div class="col s12 m12 l12" style="margin:-10px 0 0 7px; background-color:#50E3C2; border-radius:5px; width:97%; padding-bottom:10px;">
								<div class="col s12 m12 l7" style="margin-left:-15px;">
									<p class="offer_p eve-email"><a href="javascript:void(0);" onclick="email_event(01);"><i class="fas fa-envelope"></i> <strong>EMAIL: '.$value['email_id'].' </strong></a></p>
								</div>
								<div class="col s12 m12 l4" style="margin-left:0.6%;">
									<p class="offer_p"><i class="fas fa-mobile-alt"></i> <strong>CONTACT NUMBER: '.$value['contact_no'].' </strong></p>
								</div>
							</div>
						</div>

						<div class="row" style="text-align: center; margin: 10px 0 0 -200px;">
							<div class="col l12 s12 m12">
								<a href="javascript:void(0);" id="showmorelist'.$value['id'].'" class="more showmore active" title="Show More" onclick="showmorelist('.$value['id'].')">SHOW MORE</a>
								<a href="javascript:void(0);" id="less'.$value['id'].'" class="less showmore" title="Show More" onclick="showmorelist('.$value['id'].')" hidden>SHOW LESS</a>
							</div>
						</div>';
						
			/*$action  = "<div style='top:20px;' class='dropdown-button action-more border-radius-3 btn-default default-width no-background' href='#' data-activates='dropdown01'><i class='action-dot'></i></div>
			 	<ul id='dropdown01' class='dropdown-content'>
				<li><a href=".base_url()."admin_dashboard/edit_incubator/".$value['id'].'><i class="material-icons" style="color: #000;">mode_edit</i>EDIT</a></li>
				<li><a href="javascript:void(0);" class="deactive_incub" data-cd_id="'.$value['id'].'"><i class="material-icons">delete</i>DELETE</a></li>';
			
			$action .= '</ul>


			  <div class="status-action">

				<i class="action-status-icon"></i>

			  </div>';	*/	
			
	  		$records["data"][] = array(
				$image,
				$middle_part,
				"",
	  		);
	  	 }
	  	$records["draw"] = intval($post['draw']);
		$records["recordsTotal"] = count($incube);
		$records["recordsFiltered"] = count($incube);	

		echo json_encode($records);			  
		exit; 	
	}
	
	public function faqs(){
		$this->load->view('my-community/faqs');
	}
	/*** End Manage Profile **/
	
	public function info_list(){

		$reg_id = $this->user_session['reg_id'];
		$bus_id = $this->user_session['bus_id'];

		$post = $this->input->post();
		 $post=$this->security->xss_clean($post);
		$data['search'] = $post;
		$data['CA'] = $this->Community_model->getCA($bus_id, $reg_id, $post);
		$data['company'] = $this->Community_model->getCompany($bus_id);
		$data['email'] = $this->Community_model->getDetailsoffer($bus_id, $reg_id);
		$data['city']=$this->Community_model->selectData('ca_registration',"DISTINCT(city)");
		//$tracking_info['module_name'] = 'Community / Events';
		//$tracking_info['action_taken'] = 'Viewed';
		//$tracking_info['reference'] = "Events";
		//$LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
		// Activity Tracker End
           
		$this->load->view('my-community/infolist', $data);
	}
	
	public function news(){

		$reg_id = $this->user_session['reg_id'];
        $bus_id = $this->user_session['bus_id'];

        $post = $this->input->post();
        $data['search'] = $post;
        //$data['CA'] = $this->Community_model->getCA($bus_id, $reg_id, $post);
        $data['company'] = $this->Community_model->getCompany($bus_id);
        $data['email'] = $this->Community_model->getDetailsoffer($bus_id, $reg_id);
        /* $data['city']=$this->Community_model->selectData('ca_registration',"DISTINCT(city)"); */
        $data['newsletter'] = $this->Community_model->getNews();
        $data['newsletters'] = $this->Community_model->getnewss();
        if(isset($post["location"]) && $post["location"]!="" && $post["location"]!="newsall" ){
			$date_archive=explode(" ",$post['location']);
			$month=$date_archive[0];
			$year=$date_archive[1];
            $data['select_month'] = $month;
            $data['select_year'] = $year;
            $data['blogs']=$this->Community_model->selectData('blogs', '*',array('newsletter_year'=>$year,'newsletter_month'=>$month), 'created_at', 'DESC');
		}else{
            $data['blogs'] = $this->Adminmaster_model->selectData('blogs', '*', '', 'created_at', 'DESC');
            $data['select_month'] = "";
            $data['select_year'] = "";
        }

		//$data['newsletters']=$this->Adminmaster_model->selectData('newsletters', '*', '', 'createdat', 'DESC');
		//$data['newsletters']=$this->Adminmaster_model->selectData('newsletters', '*', '', 'createdat', 'DESC');
		$data['blogscount']=$this->Adminmaster_model->selectData('blogs', '*',array('category'=>'news'));
		$this->load->view('my-community/newsletter', $data);
		
		/*$reg_id = $this->user_session['reg_id'];
		$bus_id = $this->user_session['bus_id'];

		$post = $this->input->post();
		$data['search'] = $post;
		//$data['CA'] = $this->Community_model->getCA($bus_id, $reg_id, $post);
		$data['company'] = $this->Community_model->getCompany($bus_id);
		$data['email'] = $this->Community_model->getDetailsoffer($bus_id, $reg_id);
		$data['city']=$this->Community_model->selectData('ca_registration',"DISTINCT(city)");

		$data['newsletter']=$this->Community_model->getNews();
		$data['newsletters']=$this->Community_model->getnewss();

		//$data['newsletters']=$this->Adminmaster_model->selectData('newsletters', '*', '', 'createdat', 'DESC');
		//$data['newsletters']=$this->Adminmaster_model->selectData('newsletters', '*', '', 'createdat', 'DESC');
		$data['blogs']=$this->Adminmaster_model->selectData('blogs', '*', '','created_at','DESC');
		$this->load->view('my-community/newsletter', $data);*/
	}

	public function blog($url=""){

		$data=$this->input->post();
        if($url!=""){
			$title=urldecode($url);
			$data['blog']=$this->Adminmaster_model->selectData('blogs', '*',array('url like'=> "%".$title."%"));
			$data['blogs']=$this->Adminmaster_model->selectData('blogs', '*', '', 'created_at', 'DESC');
			$data['newsletters']=$this->Adminmaster_model->selectData('newsletters', '*', '', 'createdat', 'DESC');
			$data['blog_comment']=$this->Adminmaster_model->selectData('blog_comment', '*',array('blog_id'=>$data['blog'][0]->id));
			$data['blogss']=$this->Adminmaster_model->getblogs();
			$this->load->view('my-community/post_page',$data); 
		}else{

		if(isset($data["archive"]) && $data["archive"]!=""){

			$date_archive=explode(" ",$data['archive']);

			$month=$date_archive[0];

			$year=$date_archive[1];

		}else{
           $month="";

			$year="";
		}
		
		if(isset($data["search"]) && $data["search"]!="" && isset($data["archive"]) && $data["archive"]!="" ){
			$data['blogs']=$this->Adminmaster_model->selectData('blogs', '*',array('post_name like'=>"%".$data["search"]."%",'newsletter_year'=>$year,'newsletter_month'=>$month));

			$data['newsletters']=$this->Adminmaster_model->selectData('newsletters', '*',array('newsletter_name like'=>"%".$data["search"]."%",'newsletter_year'=>$year,'newsletter_month'=>$month));
		}else if(isset($data["archive"]) && $data["archive"]=="" && isset($data["search"]) && $data["search"]!=""){

			$data['blogs']=$this->Adminmaster_model->selectData('blogs', '*',array('post_name like'=>"%".$data["search"]."%"));
			
			$data['newsletters']=$this->Adminmaster_model->selectData('newsletters', '*',array('newsletter_name like'=>"%".$data["search"]."%"));

		}

		else if(isset($data["archive"]) && $data["archive"]!="" && isset($data["search"]) && $data["search"]==""){

			$data['blogs']=$this->Adminmaster_model->selectData('blogs', '*',array('newsletter_year'=>$year,'newsletter_month'=>$month));
			
			$data['newsletters']=$this->Adminmaster_model->selectData('newsletters', '*',array('newsletter_year'=>$year,'newsletter_month'=>$month));

		}else{
			$data['blogs']=$this->Adminmaster_model->selectData('blogs', '*', '', 'created_at', 'DESC');
			
			$data['newsletters']=$this->Adminmaster_model->selectData('newsletters', '*','', 'createdat', 'DESC');
		}
			$data['newsletter']=$this->Adminmaster_model->getnews();
			$data['blogss']=$this->Adminmaster_model->getblogs();
			$this->load->view('my-community/newsletter',$data);

	}
	}
	
	public function challenge(){

		$reg_id = $this->user_session['reg_id'];
		$bus_id = $this->user_session['bus_id'];

		$post = $this->input->post();
		 $post=$this->security->xss_clean($post);
		$data['search'] = $post;
		//$data['CA'] = $this->Community_model->getCA($bus_id, $reg_id, $post);
		$data['company'] = $this->Community_model->getCompany($bus_id);
		$data['email'] = $this->Community_model->getDetailsoffer($bus_id, $reg_id);
		$data['city']=$this->Community_model->selectData('ca_registration',"DISTINCT(city)");
	   // $data['challanges'] = $this->Community_model->selectData("challanges","*",'','id','DESC');
         
         $data['location'] = $this->Community_model->selectData('challanges',"DISTINCT(location)");
		$data['challanges'] = $this->Community_model->getChallange($post);
		   
		$this->load->view('my-community/challange', $data);
	}
	
	public function interviews($url=""){	
		$data['title'] = "Interviews";
    	$data['description'] = "Interviews";
		$data['keywords'] = "Interviews";
		$input=$this->input->post();	

	
		if($url!=""){

				$data['video']=$this->Adminmaster_model->selectData('interviews', '*',array('url'=>$url));
				$data['interviews']=$this->Adminmaster_model->selectData('interviews', '*');

		$data['comments']=$this->Adminmaster_model->selectData('interview_comments', '*', array('interview_id'=>$data['video'][0]->id),'id','DESC');

		   $data['likes']=$this->Adminmaster_model->selectData('interview_likes', '*', array('interview_id'=>$data['video'][0]->id,'interview_like'=>1),'id','DESC');
           $ip_address = $this->get_client_ip();
            $insert=array(
             'interview_id'=>$data['video'][0]->id,
              'parent_id'=>$data['video'][0]->profile_id,
              'ip'=>$ip_address,
            );

            $this->Adminmaster_model->insertData('interview_views', $insert);


             $data['views']=$this->Adminmaster_model->selectData('interview_views', '*', array('interview_id'=>$data['video'][0]->id),'id','DESC');
		if(isset($this->session->userdata['user_session']['reg_id'])){	
			$reg_id=$this->session->userdata['user_session']['reg_id'];
			$data['user']=$this->Adminmaster_model->selectData('registration', '*', array('reg_id'=>$reg_id));
			$this->load->view('my-community/view_interviews', $data);
        }else{
        	$this->load->view('my-community/interviews', $data);
		}	

		}else{
			if(isset($this->session->userdata['user_session']) && $this->session->userdata['user_session']['reg_id']!=""){	
				$reg_id=$this->session->userdata['user_session']['reg_id'];
				$data['user']=$this->Adminmaster_model->selectData('registration', '*', array('reg_id'=>$reg_id));
				
				if(isset($input['innovo_name']) && $input['innovo_name']!=""){
					//$data['video']=$this->Adminmaster_model->selectData('video', '*',array('category'=>$input['video_cat']));
					$data['interviews']=$this->Adminmaster_model->selectData('interviews', '*',array('name'=>$input['innovo_name']));
					$data['innovo_name']=$input['innovo_name'];
				}else{
					$data['interviews']=$this->Adminmaster_model->selectData('interviews', '*');
				}
				
				$this->load->view('my-community/interviews', $data);
			}else{
				if(isset($input['innovo_name']) && $input['innovo_name']!=""){
					//$data['video']=$this->Adminmaster_model->selectData('video', '*',array('category'=>$input['video_cat']));
					$data['interviews']=$this->Adminmaster_model->selectData('interviews', '*',array('name'=>$input['innovo_name']));
					$data['innovo_name']=$input['innovo_name'];
				}else{
					$data['interviews']=$this->Adminmaster_model->selectData('interviews', '*');
				}
				$this->load->view('my-community/interviews', $data);
			}
		}
		
	}	

	public function interview_comment(){
	
		if (!isset($this->session->userdata['user_session']['reg_id'])) {
        $data['msg']="login";
        
      }else{
		$post=$this->input->post();
		$post=$this->security->xss_clean($post);
		$csrf_token=$this->security->get_csrf_hash();
        $reg_id=$this->session->userdata['user_session']['reg_id'];
         
        $insert=array(
          
          'interview_id'=>$post['interview_id'],
          'parent_id'=>$post['profile_id'],
          'user_id'=> $reg_id,
           'name'=>$this->session->userdata['user_session']['ei_username'],
           'email'=>$this->session->userdata['user_session']['email'],
           'comment'=>$post['comment'],
         

        );

        
        	 $subid=$this->Adminmaster_model->insertData('interview_comments', $insert);
        

      $data['comment']=$this->Adminmaster_model->selectData('interview_comments', '*', array('interview_id'=>$post['interview_id']),'id','DESC');
	  $data['csrf_hash'] = $csrf_token;
      }

		
 echo json_encode($data);exit;
	}
	
		public function video($url){	

		$data['title'] = "Bharat Vaani";	//header data
		$data['description'] = "Bharat Vaani";
		$data['keywords'] = "";
		$data['video']=$this->Adminmaster_model->selectData('video', '*', array('url'=>$url));
		
		$data['video1']=$this->Adminmaster_model->selectData('video', '*', array('category'=>@$data['video'][0]->category,'language'=>'English'));

		 if(isset($this->session->userdata['user_session']['reg_id'])){
           //$data['subscription_user']=$this->Adminmaster_model->selectData('subscription', '*', array('profile_id'=>$data['video'][0]->reg_id,'subscriber_id'=>$this->session->userdata['user_session']['reg_id']));
		  }else{
		  	  //$data['subscription_user']=array();
		  }
		   $data['comments']=$this->Adminmaster_model->selectData('comments', '*', array('video_id'=>@$data['video'][0]->id),'id','DESC');

		   $data['likes']=$this->Adminmaster_model->selectData('likes', '*', array('video_id'=>@$data['video'][0]->id,'vid_like'=>1),'id','DESC');
           $ip_address = $this->get_client_ip();
            $insert=array(
             'video_id'=>$data['video'][0]->id,
              'parent_id'=>$data['video'][0]->reg_id,
              'ip'=>$ip_address,
            );

            $this->Adminmaster_model->insertData('views', $insert);


             $data['views']=$this->Adminmaster_model->selectData('views', '*', array('video_id'=>$data['video'][0]->id),'id','DESC');


		      
          
        $reg_id=$this->session->userdata['user_session']['reg_id'];
		$data['user']=$this->Adminmaster_model->selectData('registration', '*', array('reg_id'=>$reg_id));
		
		
		$this->load->view('my-community/view_videos', $data);
		
	}


		function get_client_ip() {
	    $ipaddress = '';
	    if (getenv('HTTP_CLIENT_IP'))
	        $ipaddress = getenv('HTTP_CLIENT_IP');
	    else if(getenv('HTTP_X_FORWARDED_FOR'))
	        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
	    else if(getenv('HTTP_X_FORWARDED'))
	        $ipaddress = getenv('HTTP_X_FORWARDED');
	    else if(getenv('HTTP_FORWARDED_FOR'))
	        $ipaddress = getenv('HTTP_FORWARDED_FOR');
	    else if(getenv('HTTP_FORWARDED'))
	       $ipaddress = getenv('HTTP_FORWARDED');
	    else if(getenv('REMOTE_ADDR'))
	        $ipaddress = getenv('REMOTE_ADDR');
	    else
	        $ipaddress = 'UNKNOWN';
	    return $ipaddress;
	}


	public function videos(){	


		$input=$this->input->post();
		 $input=$this->security->xss_clean($input);
        
		
		if(isset($input['video_cat']) && $input['video_cat']!=""){
              $data['video']=$this->Adminmaster_model->selectData('video', '*',array('category'=>$input['video_cat']));
		}else{
			$data['video']=$this->Adminmaster_model->selectData('video', '*');
		}

		
		//$data['video1']=$this->Adminmaster_model->selectData('video', '*', array('category'=>$data['video'][0]->category,'language'=>'English'));

		
		
		
		$this->load->view('my-community/videos', $data);
		
	}

	
	public function subscribe(){
		if (!isset($this->session->userdata['user_session']['reg_id'])) {
        $data['msg']="login";
        
      }else{
     $post=$this->input->post();
      $post=$this->security->xss_clean($post);
	  $csrf_token=$this->security->get_csrf_hash();
         $reg_id=$this->session->userdata['user_session']['reg_id'];
         $subscription=$this->Adminmaster_model->selectData('subscription', '*', array('profile_id'=>$post['id'],'subscriber_id'=>$reg_id));
        $insert=array(

          'profile_id'=>$post['id'],
           'subscriber_id'=>$reg_id,
           'status'=>$post['status'],
           'updatedat'=>date('Y-m-d H:i:s'),

        );

        if(count($subscription)>0){
            $subid=$this->Adminmaster_model->updateData('subscription', $insert, array('profile_id'=>$post['id'],'subscriber_id'=>$reg_id));
        }else{
        	 $subid=$this->Adminmaster_model->insertData('subscription', $insert);
        }

      $data['subscription']=$this->Adminmaster_model->selectData('subscription', '*', array('profile_id'=>$post['id'],'status'=>1));
      }

		$data['csrf_hash'] = $csrf_token;		
		echo json_encode($data);exit;
	}

	public function video_comment(){

		if (!isset($this->session->userdata['user_session']['reg_id'])) {
        $data['msg']="login";
        
      }else{
		$post=$this->input->post();
		$post=$this->security->xss_clean($post);
		$csrf_token=$this->security->get_csrf_hash();
        $reg_id=$this->session->userdata['user_session']['reg_id'];
         
        $insert=array(
          
          'video_id'=>$post['vid_id'],
          'parent_id'=>$post['profile_id'],
          'user_id'=> $reg_id,
           'name'=>$this->session->userdata['user_session']['ei_username'],
           'email'=>$this->session->userdata['user_session']['email'],
           'comment'=>$post['comment'],
         

        );

        
        	 $subid=$this->Adminmaster_model->insertData('comments', $insert);
        

      $data['comment']=$this->Adminmaster_model->selectData('comments', '*', array('video_id'=>$post['vid_id']),'id','DESC');
	  $data['csrf_hash'] = $csrf_token;
      }

		
 echo json_encode($data);exit;
	}



	public function video_share(){
		$csrf_token=$this->security->get_csrf_hash();
		if (!isset($this->session->userdata['user_session']['reg_id'])) {
			$data['msg']="login";
		}else{
			$data['msg']="share";
		}
		$data['csrf_hash'] = $csrf_token;
		echo json_encode($data);exit;
	}

	public function video_like(){
		if (!isset($this->session->userdata['user_session']['reg_id'])) {
        $data['msg']="login";
        
      }else{
     $post=$this->input->post();
      $post=$this->security->xss_clean($post);
	  $csrf_token=$this->security->get_csrf_hash();
         $reg_id=$this->session->userdata['user_session']['reg_id'];
          $likes=$this->Adminmaster_model->selectData('likes', '*', array('parent_id'=>$post['profile_id'],'user_id'=>$reg_id));
         
        $insert=array(
          
          'video_id'=>$post['vid_id'],
          'parent_id'=>$post['profile_id'],
          'user_id'=> $reg_id,
           'name'=>$this->session->userdata['user_session']['ei_username'],
           'email'=>$this->session->userdata['user_session']['email'],
           'vid_like'=>$post['like'],
         

        );

        
        	 

        	   if(count($likes)>0){
            $subid=$this->Adminmaster_model->updateData('likes', $insert, array('parent_id'=>$post['profile_id'],'user_id'=>$reg_id));
        }else{
        	 $subid=$this->Adminmaster_model->insertData('likes', $insert);
        }
        $data['likes']=$this->Adminmaster_model->selectData('likes', '*', array('video_id'=>$post['vid_id'],'vid_like'=>1),'id','DESC');
		}
		$data['carf_hash'] = $csrf_token;
		echo json_encode($data);exit;
	}
	
	public function new_news(){

		$reg_id = $this->user_session['reg_id'];
		$bus_id = $this->user_session['bus_id'];

		$post = $this->input->post();
		 $post=$this->security->xss_clean($post);
		$data['search'] = $post;
		$data['company'] = $this->Community_model->getCompany($bus_id);
		   
		$this->load->view('my-community/show_news', $data);
	}
	
	public function interview_like(){
		$csrf_token=$this->security->get_csrf_hash();	
		if (!isset($this->session->userdata['user_session']['reg_id'])) {
			$data['msg']="login";
		}else{
			$post=$this->input->post();
			
			$reg_id=$this->session->userdata['user_session']['reg_id'];
			$likes=$this->Community_model->selectData('interview_likes', '*', array('parent_id'=>$post['profile_id'],'user_id'=>$reg_id));
			$insert=array(
				'interview_id'=>$post['interview_id'],
				'parent_id'=>$post['profile_id'],
				'user_id'=> $reg_id,
				//'name'=>$this->session->userdata['user_session']['reg_username'],
				//'email'=>$this->session->userdata['user_session']['email'],
				'interview_like'=>$post['like'],
			);
			if(count($likes)>0){
				$subid=$this->Community_model->updateData('interview_likes', $insert, array('parent_id'=>$post['profile_id'],'user_id'=>$reg_id));
			}else{
				$subid=$this->Community_model->insertData('interview_likes', $insert);
			}
			$data['likes']=$this->Community_model->selectData('interview_likes', '*', array('interview_id'=>$post['interview_id'],'interview_like'=>1),'id','DESC');
		}
		$data['csrf_hash'] = $csrf_token;
		echo json_encode($data);exit;
	}

	
	}