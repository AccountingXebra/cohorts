<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require ("common/Index_Controller.php");
class Public_prof extends Index_Controller 
{
	function __construct(){
		//is_trialExpire();
		parent::__construct();
		$this->load->model('Sales_model');
		$this->load->model('User_model');
		$this->load->model('Profile_model');
		$this->load->model('Market_place_model');
		$this->load->model('Community_model');
		

	}
	
	public function index($url)
	{   
	
	      $business=$this->Profile_model->selectData("businesslist","*",array('profile_url'=>$url));
		 
		  
		   if(count($business)>0){
			  if($business[0]->make_profile_public){
				     	$data['id'] 			= $business[0]->bus_id;
				$res['contact_person']	= $this->Market_place_model->selectData('registration', 'reg_username,reg_email,reg_mobile', array('bus_id'=>$data['id'],'reg_admin_type'=>'6'));
				$res['result']			= $this->Market_place_model->GetUserInfo($data);
				$res['name']			= $this->Community_model->getSearchnew($data);
				$res['branch']			= $this->Market_place_model->GetBranchInfo($data);
				$res['company'] 		= $this->Market_place_model->getCompanyname($data);
				$res['email'] 			= $this->Market_place_model->getRegemail($data);
				$res['contact'] 		= $this->Market_place_model->getRegcontact($data);
				$res['offers'] 			= $this->Community_model->getOffersonprofile($data);
				$res['connections'] 	= $this->Community_model->getconnectiontype($data);
					 
				  $this->load->view('my-community/manage-profile_clone',$res);
			  } else{
				 echo "This Profile is not public"; 
			  }
		   }else{
			   echo "There is no profile found";
		   }
		}


}