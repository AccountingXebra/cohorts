<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require ("common/Index_Controller.php");
class Profile extends Index_Controller 
{

	function __construct(){
		parent::__construct();
		global $flag;
		$this->load->model('Profile_model');
		

		is_login();
		is_trialExpire();
		//is_accessAvail(5,6);
		//is_BusReg();
		$this->user_session = $this->session->userdata('user_session');

	}
	
	public function index()
	{ 
		//$this->load->view('email_template/client-portal-access-mail');
		// print_r($this->session->userdata['user_session']);
		// echo $this->session->userdata['user_session']['reg_admin_type'];
		// exit;
		//redirect('profile/company-profile');
		
	$this->load->view('test');
	}

	public function company_profile()
	{
		is_accessAvail(6);
		$bus_id = $this->session->userdata['user_session']['bus_id'];
		$user_id = $this->session->userdata['user_session']['reg_id'];
		$data['company_profiles'] = $this->Profile_model->selectData('businesslist', '*', array('bus_id'=>$bus_id));
		
		if(count($data['company_profiles'])>0){
			redirect('profile/manage-company-profile');
		}else{

			// Activity Tracker start
			
			 $tracking_info['module_name'] = 'My Company Profile';
			 $tracking_info['action_taken'] = 'Viewed';
			 $tracking_info['reference'] = 'Company Profile';
			 $LogRec = $this->Adminmaster_model->insertActivity($tracking_info);

		 	// Activity Tracker End

			$data['select_service'] = $this->Profile_model->selectData('registration', '*', array('reg_id'=>$user_id,'reg_admin_type'=>'3','reg_bus_type'=>''));

			if(count($data['select_service'])>0){
				$this->load->view('profile/company_blank',$data);
			}else{
				$this->load->view('profile/company_blank');
			} 
		}

	}

	public function manage_company_profile()
	{
		is_accessAvail(6);
		$bus_id = $this->session->userdata['user_session']['bus_id'];	
		$data['company_profiles'] = $this->Profile_model->selectData('businesslist', '*', array('bus_id'=>$bus_id));
		$data['countries']	=	$this->Profile_model->selectData('countries', '*','','country_id','ASC');
		$data['currency']	=	$this->Profile_model->selectData('currency', '*','','currency_id','ASC');

		$this->load->view('profile/company_list',$data);
		
	}

	public function get_company_profiles()
	{
		// Activity Tracker start
		
		 $tracking_info['module_name'] = 'My Company Profile';
		 $tracking_info['action_taken'] = 'Viewed';
		 $tracking_info['reference'] = 'Company Profile List';
		 $LogRec = $this->Adminmaster_model->insertActivity($tracking_info);

	 	// Activity Tracker End

		$post = $this->input->post();
		$post=$this->security->xss_clean($post);
		$post=$this->security->xss_clean($post);
		$post['bus_id']=$this->session->userdata['user_session']['bus_id'];
		$field_pos=array("bus_company_name"=>'0',"bus_company_logo"=>'1',"bus_billing_state"=>'2',"bus_billing_country"=>'3',"bus_company_type"=>'4',"gst_no"=>'5',"place"=>'6',"cbank_name"=>'7',"cbank_account_no"=>'8','status'=>'9');	

		$sort_field=array_search($post['order'][0]['column'],$field_pos);

		if($post['order'][0]['dir']=='asc')
		{
	 		$orderBy="ASC";
		}
	 	else
		{
	 		$orderBy="DESC";
		}	

		$TotalRecord=$this->Profile_model->fetchUserFilter($post,$sort_field,$orderBy,0);	
	 	$userData = $this->Profile_model->fetchUserFilter($post,$sort_field,$orderBy,1);

	 	$iTotalRecords = $TotalRecord['NumRecords'];

	 	$records = array();
	  	$records["data"] = array();
	  	$csrf_token=$this->security->get_csrf_hash();

	  	$records['csrf_hash']=$csrf_token;
		
	  	foreach ($userData as $key => $value) {
			$check='<input type="checkbox" id="filled-in-box'.$value['bus_id'].'" name="filled-in-box'.$value['bus_id'].'" class="filled-in purple company_profile_bulk_action" value="'.$value['bus_id'].'"/> <label for="filled-in-box'.$value['bus_id'].'"></label>';
	  		$company_logo='';
			
			if($value['bus_company_logo'] != '')
			{
				$company_logo = '<img src="'.base_url().'public/upload/company_logos/'.$value['bus_id'].'/'.$value['bus_company_logo'].'" height="35px" width="35px">';
			}
			else
			{
				$company_logo = '';	
			}
					
			$bus_company_name=$value['bus_company_name'];
			if($value['country_name'] != '' &&  $value['state_name'] != ''){
					$bus_company_name .= '<div class="row"><div class="col s12 m12 l12"><small class="text-gray">'.$value['state_name'].', '.$value['country_name'].'</small></div></div>';
					}
			$bus_company_name='<a href="'.base_url().'profile/view-company-profile/'.$value['bus_id'].'" >'.$bus_company_name.'</a>';
			$gst_no='';
			if($value['gst_no'] != '')
			{
				$gst_no = $value['gst_no'];
				if($value['place'] != '' ) {
					$gst_no .= '<div class="small-text"><small class="text-gray">'.$value['place'].'</small></div>';
				}
			}
			$cbank_name='';
			if($value['cbank_name'] != '')
			{
				$cbank_name =$value['cbank_name'];
				if($value['cbank_account_no'] != '' ) {
					$cbank_name .= '<div class="small-text"><small class="text-gray">'.substr($value['cbank_account_no'],0,2).'XXX XXX'.substr($value['cbank_account_no'],-4).' </small></div>';
				}
			}
							
	  		/* $status = '';
	  		if($value['tStatus'] == 1)
	  			$status = 'Active';
	  		else
	  			$status = 'Inactive';
				$btnColor = ($value['tStatus'] == 1)? "bg-green" : "bg-red" ;
 */
	  		//$statusBtn = '<button class="btn '. $btnColor.'" onclick="changeStatus('.$value['bus_id'].','.$value['status'] .');" >'.$value['status'].'</button>';

	  		$action = '';$status='';$cls='';$cls1='';
			if($value['status']!="Active"){
				$status='<li class="activation"><a href="javascript:void(0);" class="active_company_profile" data-profileid='.$value["bus_id"].'><i class="dropdwon-icon icon activate"></i>Re-Activate</a></li>';
				$cls='deactive_record';
				$cls1='deactivate';
			}else{
				
				$status='<li class="deactivation"><a href="javascript:void(0);" class="deactive_company_profile" data-profileid="'.$value["bus_id"].'"><i class="dropdwon-icon icon deactivate"></i>Deactivate</a></li>';
				$cls='active_record';
				$cls1='activate';
				
			}
			$action  = "<div class='dropdown-button action-more border-radius-3 btn-default default-width no-background ".$cls."' href='javascript:void(0);' data-activates='dropdown".$value["bus_id"]."'><i class='action-dot'></i></div>

			  <ul id='dropdown".$value["bus_id"]."' class='dropdown-content'>";
				
			$li="<li><a href=".base_url()."profile/edit-company-profile/".$value["bus_id"].'><i class="material-icons" style="color: #000;">mode_edit</i>Edit</a></li>
				
				 <li><a href="javascript:void(0);" onclick="email_company_profile('.$value["bus_id"].');"><i class="material-icons">email</i>Email</a></li>
			
				<li><a href='.base_url().'profile/download_single_xls/'.$value["bus_id"].'><img class="icon-img" src="'.base_url().'public/icons/export.png" style="width: 21px;height: 20px;"><p class="ex">Export</p></a></li>
				<li><a href='.base_url().'profile/print-company-profile/'.$value["bus_id"].' target="_blank"><i class="material-icons dp48">print</i>Print</a></li>';
				if($value['status']=="Active"){	
					$action .= $li;
				}
			
			$action  .= $status;
			
			  $action  .= '</ul>

			  <div class="status-action '.$cls1.'">

				<i class="action-status-icon"></i>

			  </div>';
			
			$disable='';	
			if($value['api_key'] == null and $value['auth_token'] == null)
			{
				$insta_integration = "<a href='#instamojo_modal' data-toggle='modal' class='pay-dis btn btn-theme-border modal-trigger modal-close'>INTEGRATE</a>";
			}else{
				$disable='disabled="disabled"';
				$insta_integration = "<a href='#' data-toggle='modal' class='pay-dis btn btn-theme-border modal-trigger modal-close' ".$disable.">INTEGRATED</a>";
			}		
			
	  		

	  		$records["data"][] = array(
				//$check,
				$company_logo,
	  			$bus_company_name,
	  			$value['company_type'],
				$gst_no,
	  			$cbank_name,
	  			$insta_integration,
	  			$action,
	  			);
	  	}




	  	$records["draw"] = intval($post['draw']);
		$records["recordsTotal"] = $iTotalRecords;
		$records["recordsFiltered"] = $iTotalRecords;		
		echo json_encode($records);				  
		exit;
	}

	public function get_states(){
		$country_id=$this->input->post('country_id');
		$csrf_token=$this->security->get_csrf_hash();
		$states=$this->Profile_model->selectData('states','*', array('country_id'=>$country_id));
		$opt='';
		$opt.="<option value=''>STATE *</option>";
		foreach($states as $st){
			if($country_id==101){
			$opt.="<option value=".$st->state_id." >".strtoupper($st->state_name)." - ".$st->state_gst_code."</option>";
		    }else{
		    	$opt.="<option value=".$st->state_id." >".strtoupper($st->state_name)."</option>";
		    }
		}
		//echo $opt;
		echo $opt."@".$csrf_token;
	}

	public function timezone(){
		$country_id=$this->input->post('country_id');
		$csrf_token=$this->security->get_csrf_hash();
		$timezone=$this->Profile_model->selectData('tbl_countries_currency','timezone', array('country_id'=>$country_id));
		if($timezone){
			$new_arr = (array) $timezone[0];
		}else{

			$new_arr = array();
		}

		$opt='';
		if(count($new_arr) > 0)
		{			
			$opt.='<option value="'.$new_arr['timezone'].'">'.$new_arr['timezone'].'</option>';
		}else{
			$opt.='<option value="">Select Timezone</option>';
		}
			$opt.='<option value="GMT-12:00">GMT-12:00</option>
		                              <option value="GMT-11:00">GMT-11:00</option>
		                              <option value="GMT-10:00">GMT-10:00</option>
		                              <option value="GMT-09:00">GMT-09:00</option>
		                              <option value="GMT-08:00">GMT-08:00</option>
		                              <option value="GMT-07:00">GMT-07:00</option>
		                              <option value="GMT-06:00">GMT-06:00</option>
		                              <option value="GMT-05:00">GMT-05:00</option>
		                              <option value="GMT-04:00">GMT-04:00</option>
		                              <option value="GMT-03:30">GMT-03:30</option>
		                              <option value="GMT-03:00">GMT-03:00</option>
		                              <option value="GMT-02:00">GMT-02:00</option>
		                              <option value="GMT-01:00">GMT-01:00</option>
		                              <option value="GMT+00:00">GMT+00:00</option>
		                              <option value="GMT+01:00">GMT+01:00</option>
		                              <option value="GMT+02:00">GMT+02:00</option>
		                              <option value="GMT+03:00">GMT+03:00</option>
		                              <option value="GMT+03:30">GMT+03:30</option>
		                              <option value="GMT+04:00">GMT+04:00</option>
		                              <option value="GMT+04:30">GMT+04:30</option>
		                              <option value="GMT+05:00">GMT+05:00</option>
		                              <option value="GMT+05:30">GMT+05:30</option>
		                              <option value="GMT+05:45">GMT+05:45</option>
		                              <option value="GMT+06:00">GMT+06:00</option>
		                              <option value="GMT+06:30">GMT+06:30</option>
		                              <option value="GMT+07:00">GMT+07:00</option>
		                              <option value="GMT+08:00">GMT+08:00</option>
		                              <option value="GMT+09:00">GMT+09:00</option>
		                              <option value="GMT+10:00">GMT+10:00</option>
		                              <option value="GMT+11:00">GMT+11:00</option>
		                              <option value="GMT+12:00">GMT+12:00</option>
		                              <option value="GMT+13:00">GMT+13:00</option>';


		//echo $opt;
		echo $opt."@".$csrf_token;
	}

	public function date_format(){
		$country_id=$this->input->post('country_id');
		$csrf_token=$this->security->get_csrf_hash();
		$date=$this->Profile_model->selectData('tbl_countries_currency','country_date_format', array('country_id'=>$country_id));

		if($date){
			$new_arr = (array) $date[0];
		}else{

			$new_arr = array();
		}
		
		$opt='';
		if(count($new_arr) > 0)
		{
			if($new_arr['country_date_format'] == 'd/m/Y')
			{
				$view_format = "dd/mm/yyyy";
			}elseif ($new_arr['country_date_format'] == 'm-d-Y') {
				$view_format = "mm-dd-yyyy";
			}elseif ($new_arr['country_date_format'] == 'Y-m-d') {
				$view_format = "yyyy-mm-dd";
			}else{
				$view_format = '';
			}

		
		$opt.='<option value="'.$new_arr['country_date_format'].'"selected>'.$view_format.'</option>';
		
        }else{

			$opt.='<option value="">Select Date Format</option>';
        }
        $opt.='<option value="d/m/Y">dd/mm/yyyy</option>
				<option value="m-d-Y">mm-dd-yyyy</option>
                <option value="Y-m-d">yyyy-mm-dd</option>';

		//echo $opt;
		echo $opt."@".$csrf_token;
	}

	public function bank_swift_check(){

		$country_id=$this->input->post('country_id');
		$csrf_token=$this->security->get_csrf_hash();
		$currency=$this->Profile_model->selectData('tbl_countries_currency','currency_id', array('country_id'=>$country_id));
		$get_active_currency = array();
		if($currency)
		{
			$new_arr = (array) $currency[0];
			$get_active_currency=$this->Profile_model->selectData('currency','*', array('currency_id'=>$new_arr['currency_id']));
			$get_active_currency = (array) $get_active_currency[0];
		}

		$all_currency=$this->Profile_model->selectData('currency','*', '');

		if(count($get_active_currency) > 0)
		{
			$opt='';
			$opt.="<option value='".$get_active_currency['currency_id']."'>".$get_active_currency['currency']." (".$get_active_currency['currencycode'].")</option>";
			foreach($all_currency as $cr){
				$opt.="<option value=".$cr->currency_id." >".$cr->currency." (".$cr->currencycode.") </option>";
			}

		}else{
			$opt='';
			$opt.="<option value=''>Select Currency</option>";
			foreach($all_currency as $cr){
				$opt.="<option value=".$cr->currency_id." >".$cr->currency." (".$cr->currencycode.") </option>";
			}
		}

		
		//echo $opt;
		echo $opt."@".$csrf_token;
	}

	public function get_all_states(){
		$country_id=$this->input->post('country_id');
		$states=$this->Profile_model->selectData('states','*', array('country_id'=>$country_id));
		$opt='';
		$opt.="<option value=''>STATE</option>";
		$opt.="<option value=''>All</option>";
		foreach($states as $st){
			$opt.="<option value=".$st->state_id." >".strtoupper($st->state_name)." ".$st->state_gst_code."</option>";
		}
		echo $opt;
	}
	public function get_cities(){
		$state_id=$this->input->post('state_id');
		$csrf_token=$this->security->get_csrf_hash();
		$cities=$this->Profile_model->selectData('cities','*', array('state_id'=>$state_id));
		$opt='';
		$opt.="<option value=''>CITY *</option>";
		foreach($cities as $st){
			$opt.="<option value=".$st->city_id." >".strtoupper($st->name)."</option>";
		}
		//echo $opt;
		echo $opt."@".$csrf_token;
	}
	public function get_all_cities(){
		$state_id=$this->input->post('state_id');
		$cities=$this->Profile_model->selectData('cities','*', array('state_id'=>$state_id));
		$opt='';
		$opt.="<option value=''>CITY</option>";
		$opt.="<option value=''>All</option>";
		foreach($cities as $st){
			$opt.="<option value=".$st->city_id." >".$st->name."</option>";
		}
		echo $opt;
	}

	public function check_for_Bus()
	{
		$company_id = $this->session->userdata['user_session']['bus_id'];
		if($company_id != 0 )
		{
			echo "exist";
		}else{
			
			echo "notexist";
		}
	}
	
	public function add_company_profile()
	{
		
		is_accessAvail(5);
		$company_id = $this->session->userdata['user_session']['bus_id'];
		$reg_id = $this->session->userdata['user_session']['reg_id'];
		// print_r($this->session->userdata('user_session'));
		// exit;
		$subscription_data	=	$this->Profile_model->selectData('subscription', '*',array('bus_id'=>0,'reg_id'=>$reg_id),'','ASC');
		
		if($company_id == 0 || count($subscription_data) > 0)
		{
			//redirect(base_url().'Home/billing_and_upgrade');
			$data['subscription_id'] = 0;
			if($subscription_data)
			{
				$subscription_data = (array) $subscription_data[0];
				$data['subscription_id'] = $subscription_data['subscription_id'];
			}

			$data['countries']	=	$this->Profile_model->selectData('countries', '*','','country_id','ASC');
			//$data['cities']	=	$this->Profile_model->selectData('cities', '*',array('state_id>='=>'1','state_id<='=>'41'),'city_id','ASC');
			$data['currency']	=	$this->Profile_model->selectData('currency', '*','','currency_id','ASC');
			$data['company_type'] = $this->Profile_model->selectData('company_type', '*','','id','ASC');
			$this->session->unset_userdata('company_branch_session');
			$this->session->unset_userdata('company_gst_info_session');
			$this->session->unset_userdata('add_bank_info'); 
			$this->session->unset_userdata('company_services_keywords');
			$this->load->view('profile/add_company',$data);
		}
		else
		{

			redirect(base_url('subscription/my_subscription'));
		}
	
	}

	public function check_for_allowedsize()
	{   
		$reg_id=$this->session->userdata['user_session']['reg_id'];
		$bus_id=$this->session->userdata['user_session']['bus_id'];
		$csrf_token=$this->security->get_csrf_hash();
		$legal_document_size=$this->Profile_model->selectData('legal_document', "SUM(legal_document_size) as legal_doc_size", array('bus_id'=>$bus_id));
		
		$other_document_size=$this->Profile_model->selectData('other_document', "SUM(other_doc_size) as other_doc_size", array('bus_id'=>$bus_id));
		$srf_document_size=$this->Profile_model->selectData('srf_document', "SUM(srf_document_size) as srf_doc_size", array('bus_id'=>$bus_id));

		$docsize=($legal_document_size[0]->legal_doc_size+$other_document_size[0]->other_doc_size+$srf_document_size[0]->srf_doc_size)/1024;

			$business=$this->Profile_model->selectData('registration', '*', array('reg_id'=>$reg_id,'bus_id'=>$bus_id));
			
			$subscription_bus=$this->Profile_model->selectData('businesslist', '*', array('bus_id'=>$bus_id));
			$plan= $this->Profile_model->selectData('subscription', '*',array('bus_id'=>$bus_id ,'subscription_id'=>$subscription_bus[0]->subscription_id));
      


		$filesize = $this->input->post('filesize');
		$filename = $this->input->post('filename');
		$filetype = $this->input->post('doctype');
		// convert uploaded file size in kb
		$kbfilesize = (int)$filesize;
		$allowedsize = (int)25000*1024;
      
		
		if($kbfilesize < $allowedsize)
		{
           if($filetype=="image"){
             $allowed =  array('png','jpg','jpeg','PNG','JPG','JPEG');
           }else{
           	$allowed =  array('pdf','PDF','png','PNG');
           }
			
			// $allowed =  array('pdf','PDF');

			$ext = pathinfo($filename, PATHINFO_EXTENSION); 

			if(!in_array($ext,$allowed) )
			{
				//echo json_encode('error');exit;
				echo json_encode('error'."/".$csrf_token);exit;	
			}
			else
			{
				//echo json_encode('true');exit;
				echo json_encode('true'."/".$csrf_token);exit;	
			}
		}
		else
		{
			///echo json_encode("sizeexc");exit;
			echo json_encode('sizeexc'."/".$csrf_token);exit;	
		}
	}

	public function add_bank_info()
	{
			$info = array();
			$_POST=$this->security->xss_clean($_POST);
			$csrf_token=$this->security->get_csrf_hash();
			parse_str($_POST['frm'],$info);
			  if(isset($info['opening_bank_balance'])){
			  	$opening_bal=$info['opening_bank_balance'];
			  }else{
			  	$opening_bal="";
			  }

			   if(isset($info['opening_balance_date'])){
			  	$opening_bal_date=date("Y-m-d", strtotime(str_replace('/', '-',($info['opening_balance_date']))));
			  }else{
			  	$opening_bal_date="";
			  }
			 
				$bank_info	=	array(		
					'cbank_account_no' 	=>  $info['ac_number'],
					'cbank_name'		=>	$info['bank_name'],
					'cbank_branch_name'		=>	$info['bank_branch_name'],
					'cbank_account_type'	=>	$info['acc_type'],
					'cbank_ifsc_code'		=>	@$info['ifsc_code'],
					'cbank_swift_code'		=>	@$info['swift_code'],
					'cbank_country_code'		=>	@$info['bus_bank_country'],
					'cbank_currency_code'		=>	$info['bank_cur_check'],
                    'opening_bank_balance'		=>	$opening_bal,
                    'opening_balance_date'		=>	$opening_bal_date,
					'csrf_hash' => $csrf_token,
			);
			if($this->session->userdata('add_bank_info') != '') 
			{
				$update_session = $this->session->userdata('add_bank_info');
				$update_session[] = $bank_info;
				$this->session->set_userdata('add_bank_info', $update_session);
			}
			else
			{
				$new_session[] = $bank_info;
				$this->session->set_userdata('add_bank_info', $new_session);
			}
			$session_data=array_values($this->session->userdata('add_bank_info'));
			echo json_encode($session_data);	
			/*$branch_multi[]=$bank_info;
			$this->session->set_userdata('add_bank_info',$branch_multi);
		    echo true;*/
	}

	public function add_card_info()
	{     


			$info = array();
			$_POST=$this->security->xss_clean($_POST);
			parse_str($_POST['frm'],$info);
                
             	  
              $i=0;
			  foreach($info['card'] as $card){

			  	if($card!=""){
			 
				$bank_info	=	array(		
					'card_type' 	=>  $card,
					'card_number'		=>	"",
					'bank_name'		=>	$info['iss_bank'][$i],					
					

			);
			if($this->session->userdata('add_card_info') != '') 
			{
				$update_session = $this->session->userdata('add_card_info');
				$update_session[] = $bank_info;
				$this->session->set_userdata('add_card_info', $update_session);
			}
			else
			{
				$new_session[] = $bank_info;
				$this->session->set_userdata('add_card_info', $new_session);
			}
			$i++;
		}
		}
			$session_data=array_values($this->session->userdata('add_card_info'));
			echo json_encode($session_data);	
			/*$branch_multi[]=$bank_info;
			$this->session->set_userdata('add_bank_info',$branch_multi);
		    echo true;*/
	}


	public function remove_bank_details()
	{
		
		$ac_no = $this->input->post('ac_no');
		$cbank = $this->input->post('cbank');
		$branch = $this->input->post('branch');
		$acc_type = $this->input->post('acc_type');
		$ifsc = $this->input->post('ifsc');
		if($this->session->userdata('add_bank_info') != '') 
		{
			$bank_session = $this->session->userdata('add_bank_info');
			foreach($bank_session as $key => $value)
			{
				if(in_array($ac_no,$value) && in_array($cbank,$value) && in_array($branch,$value) && in_array($acc_type,$value) && in_array($ifsc,$value))
				{
					 unset($bank_session[$key]);	
				}
			}
			if(count($bank_session) > 0)
			{
				$this->session->unset_userdata('add_bank_info');
				$this->session->set_userdata('add_bank_info',$bank_session);
				$session_data=array_values($this->session->userdata('add_bank_info'));
				echo json_encode($session_data);
			}
			else{
				$this->session->unset_userdata('add_bank_info');
				echo false;
			}
		}
	}

	public function add_branch_info()
	{
		$branch_info = array();
		$_POST=$this->security->xss_clean($_POST);
		parse_str($_POST['frm'],$branch_info);
		$branch_info_session = array(
			'cbranch_name'	=>	$branch_info['branch_name'],
			'cbranch_city'	=>	$branch_info['branch_city'],
		);
		if($this->session->userdata('company_branch_session') != '') 
		{
			$update_session = $this->session->userdata('company_branch_session');
			$update_session[] = $branch_info_session;
			$this->session->set_userdata('company_branch_session', $update_session);
		}
		else
		{
			$new_session[] = $branch_info_session;
			$this->session->set_userdata('company_branch_session', $new_session);
		}
		$session_data=array_values($this->session->userdata('company_branch_session'));
		echo json_encode($session_data);
	}

	public function remove_branch_info()
	{
		$branch = $this->input->post('branch');
		$city = $this->input->post('city');
		if($this->session->userdata('company_branch_session') != '') 
		{
			$branch_session = $this->session->userdata('company_branch_session');
			foreach($branch_session as $key => $value)
			{
				if(in_array($branch,$value) && in_array($city,$value))
				{
					 unset($branch_session[$key]);	
				}
			}
			if(count($branch_session) > 0)
			{
				$this->session->unset_userdata('company_branch_session');
				$this->session->set_userdata('company_branch_session',$branch_session);
				$session_data=array_values($this->session->userdata('company_branch_session'));
				echo json_encode($session_data);
			}
			else{
				$this->session->unset_userdata('company_branch_session');
				echo false;
			}
		}
	}

	public function add_keywords()
	{
		$keywords = array();
		$words = array();
		$keyword_str = '';
		parse_str($_POST['frm'],$keywords);	
		$csrf_token=$this->security->get_csrf_hash();
		for($i=1;$i<=10;$i++)
		{
			if($keywords['keyword_'.$i] != '' && $keywords['keyword_'.$i] != NULL) {
				$words[] =  $keywords['keyword_'.$i];	
			}
		}
		if(count($words) != 0) {
			$keyword_str = implode("|@|",$words);
			$service_session = array(
				'bus_services_keywords'=>$keyword_str,
			);
			$this->session->set_userdata('company_services_keywords',$service_session);
			$service=$this->session->userdata('company_services_keywords');
			echo json_encode($service);
		} else {
			echo json_encode(FALSE);	
		}
	}

	public function remove_keywords()
	{
		$service_id = $this->input->post('service_id');
		if($this->session->userdata('company_services_keywords') != '') 
		{
			$service=$this->session->userdata('company_services_keywords');
			$this->session->unset_userdata('company_services_keywords');
			$keyword_str = explode("|@|",$service['bus_services_keywords']);
			foreach($keyword_str as $key=>$value){
				if($value==$service_id){
					unset($keyword_str[$key]);
				}
			}
			$keyword_str = implode("|@|",$keyword_str);
			$service_session = array(
				'bus_services_keywords'=>$keyword_str,
			);
			$this->session->set_userdata('company_services_keywords',$service_session);
			$service=$this->session->userdata('company_services_keywords');
			echo json_encode($service);
		} else {
			echo json_encode(FALSE);	
		}
	}

	public function add_gstin_info()
	{
		 if (!$this->session->userdata('user_session')) {
            redirect(base_url());
        }
		else
		{
			$gst_info = array();
			$_POST=$this->security->xss_clean($_POST);
			parse_str($_POST['frm'],$gst_info);
			$gst_array = $gst_info['gst_array'];
			/*$company_id = $this->session->userdata['aduserData']['aduser_cmpid'];
			$user_id = $this->session->userdata['aduserData']['aduser_id'];
			$cmpidns = $this->session->userdata('aduserData');

			$gstin	=	array(		
					'company_id' 	=>  $company_id,	
					'user_id'		=>	$user_id,
					'branch_name'	=>	$info['branch_name'],
					'gst_number'	=>	$info['gst_no'],
					'gst_percent'	=>	$info['gst_perc'],
					'status'		=>	'0',
			);
			$this->session->set_userdata('add_gstin_info',$gstin);
		    echo true;*/
		}
		
		$company_id = $this->session->userdata['user_session']['bus_id'];
		$userid = $this->session->userdata['user_session']['reg_id'];
		
		//$this->session->unset_userdata('gst_info_session');
		//while (list($key, $value) = each($gst_array)) {
		  foreach($gst_array as $key=>$value){
			if($gst_info['gstin'.$value]!='' && $gst_info['location'.$value] != '')
			{
				$gst_info_session = array(
					
					'gst_no'			=>	strtoupper($gst_info['gstin'.$value]),
					'place'	=>	$gst_info['location'.$value],
					'country'	=>	$gst_info['bus_billing_country'.$value],
					'state_code'	=>	$gst_info['bus_billing_state'.$value],
					'address'	=>	$gst_info['bus_billing_address'.$value],
					'city'	=>	$gst_info['bus_billing_city'.$value],
					'zipcode'	=>	$gst_info['bus_billing_zipcode'.$value],
					//'email'	=>	$gst_info['bus_email_id'.$value],
					//'contact'	=>	$gst_info['bus_billing_contact'.$value],
				);
				if($this->session->userdata('company_gst_info_session') != '') 
				{
					$update_session = $this->session->userdata('company_gst_info_session');
					$update_session[] = $gst_info_session;
					$this->session->set_userdata('company_gst_info_session', $update_session);
				}
				else
				{
					$new_session[] = $gst_info_session;
					$this->session->set_userdata('company_gst_info_session', $new_session);
				}
			}
			else {
				echo  false;	
			}
		}
		echo json_encode($this->session->userdata('company_gst_info_session'));
	}

	public function remove_company_gst_info()
	{
		$gst_no = $this->input->post('gst_no');
		$location = $this->input->post('location');
		if($this->session->userdata('company_gst_info_session') != '') 
		{
			$gst_session = $this->session->userdata('company_gst_info_session');
			foreach($gst_session as $key => $value)
			{
				if(in_array($gst_no,$value) && in_array($location,$value))
				{
					 unset($gst_session[$key]);	
				}
			}
			if(count($gst_session) != 0)
			{
				$this->session->unset_userdata('company_gst_info_session');
				$this->session->set_userdata('company_gst_info_session',$gst_session);
				echo json_encode(array_values($this->session->userdata('company_gst_info_session')));
			}
			else{
				$this->session->unset_userdata('company_gst_info_session');
				echo json_encode(false);
			}
		}
		else {
		}
	}

	public function insert_company_profile()
	{

		$user_id = $this->session->userdata['user_session']['reg_id'];
		$company_id = $this->session->userdata['user_session']['bus_id'];
		$gst_id = $this->session->userdata['user_session']['ei_gst'];
		
		$post = $this->input->post();
		$post=$this->security->xss_clean($post);
		if($post['api_key'] == '' || $post['api_key'] == null || $post['auth_token'] == '' || $post['auth_token'] == null)
		{
			unset($post['api_key']);
			unset($post['auth_token']);
		}
      unset($post['search_gstin']);
		unset($post['same-bill']);
		$i=0;
		$l=count($_FILES['Legaldocuments']);
		$p=count($_FILES['purchaseorderdoc']);
		$o=count($_FILES['otherdoc']);
		$m=count($_FILES['memorandum']);
	
		for($j=0;$j<$l;$j++){
			unset($post['start_date'.$j]);
			unset($post['end_date'.$j]);
			unset($post['legal_remainder'.$j]);
			unset($post['legal_po_num'.$j]);
			unset($post['legal_po_date'.$j]);
		}
		for($j=0;$j<$p;$j++){
			unset($post['purchase_order'.$j]);
			unset($post['po_date'.$j]);
			unset($post['po_startdate'.$j]);
			unset($post['po_enddate'.$j]);
			unset($post['purchase_remainder'.$j]);
		}
		for($j=0;$j<$o;$j++){
			unset($post['otherdoc_name'.$j]);

		}
		unset($post['otherdoc_name']);
		for($j=0;$j<$m;$j++){
			unset($post['doc_name'.$j]);
		}
		
		$image_info = array();
		$image_info = $_FILES;
		$profile_img ='';
		
		
		if($post['bus_fy_startdate'] != ''){
			$post['bus_fy_startdate'] = date("Y-m-d", strtotime(str_replace('/', '-',($post['bus_fy_startdate']))));
		}	
		else {
			$post['bus_fy_startdate'] ='';
		}
		if(isset($post['cash_balance_date']) && $post['cash_balance_date'] != ''){
			$post['cash_balance_date'] = date("Y-m-d", strtotime(str_replace('/', '-',($post['cash_balance_date']))));
		}	
		else {
			$post['cash_balance_date'] ='';
		}
		if(isset($post['opening_balance_date']) && $post['opening_balance_date'] != ''){
			$post['opening_balance_date'] = date("Y-m-d", strtotime(str_replace('/', '-',($post['opening_balance_date']))));
		}	
		else {
			$post['opening_balance_date'] ='';
		}
		if($post['bus_fy_enddate'] != ''){
			$post['bus_fy_enddate'] = date("Y-m-d", strtotime(str_replace('/', '-',($post['bus_fy_enddate']))));
		}	
		else {
			$post['bus_fy_enddate'] ='';
		}
		if($post['bus_incorporation_date'] != ''){
			$post['bus_incorporation_date'] = date("Y-m-d", strtotime($post['bus_incorporation_date']));
		}	
		else {
			$post['bus_incorporation_date'] ='';
		}
		$service_info = $this->session->userdata('company_services_keywords');
		
		if($service_info != '')
		{
			$post['bus_services_keywords'] = ltrim(implode('|@|',$service_info),'|@|');
		}
		if($user_id != '')
		{
			$post['reg_id'] = $user_id;
			$post['gst_id'] = $gst_id;
		}
		$flag = $this->Profile_model->insertData('businesslist', $post);
		$logo_id = $flag;
		if($flag)
		{

			// Activity Tracker start
		
				 $tracking_info['module_name'] = 'My Company Profile';
				 $tracking_info['action_taken'] = 'Added';
				 $tracking_info['reference'] = $post['bus_company_name'];
				 $tracking_info['bus_id'] = $post['bus_company_name'];
				 $LogRec = $this->Adminmaster_model->insertActivity($tracking_info);

			// Activity Tracker End


			if($post['subscription_id'] != 0)
			{
				$update_subscription =$this->Profile_model->updateData('subscription',array('bus_id'=>$flag),array('subscription_id'=>$post['subscription_id']));
			}
			
			if($company_id == 0)
			{
				$user_info = $this->Profile_model->selectData("registration",'*',array("reg_id"=>$user_id));
				if($user_info[0]->reg_createdby)
				{
					$update_reg =$this->Profile_model->updateData('registration',array('bus_id'=>$flag),array('reg_id'=>$user_info[0]->reg_createdby));
					$update_reg2 =$this->Profile_model->updateData('registration',array('bus_id'=>$flag),array('reg_createdby'=>$user_info[0]->reg_createdby));
				}else{

					$update_reg =$this->Profile_model->updateData('registration',array('bus_id'=>$flag),array('reg_id'=>$user_id));
					$update_reg2 =$this->Profile_model->updateData('registration',array('bus_id'=>$flag),array('reg_createdby'=>$user_id));
				}
				

				$user_session = $this->session->userdata['user_session'];
				$user_session['bus_id'] = $flag;
				$this->session->set_userdata('user_session',$user_session);
			}
			
		}
		if($image_info['bus_company_logo']['name'] != '')
		{
			$profile_image=$this->Common_model->upload_org_filename('bus_company_logo','company_logos/'.$logo_id,$allowd="jpg|jpeg|png",array('width'=>200,'height'=>300));
            if($profile_image!=false){
                $profile_img=$profile_image['file_name'];
                 $updat =$this->Profile_model->updateData('businesslist',array('bus_company_logo'=>$profile_img),array('bus_id'=>$flag));
            }
           
			
		}

		if($flag != false)
		{
			$this->session->unset_userdata('company_services_keywords');
			// Add Multiple Bank Info //
			error_reporting(E_ALL ^ E_WARNING); 
			 
			$bank_info = $this->session->userdata('add_bank_info');
			if(is_array($bank_info)){
			$bank_info = array_map(function($arr) use($flag){
			   return $arr + ['bus_id' => $flag];

			},$bank_info);
			
			$bank = $this->Profile_model->insertData_batch('company_bank', $bank_info);
			
			
			 $this->session->unset_userdata('add_bank_info'); 
			}

			$card_info = $this->session->userdata('add_card_info');
			if(is_array($card_info)){
			$card_info = array_map(function($arr) use($flag){
			   return $arr + ['bus_id' => $flag,'status'=>'Active'];

			},$card_info);
			
			$card = $this->Profile_model->insertData_batch('company_cards', $card_info);
			
			
			 $this->session->unset_userdata('add_card_info'); 
			}
			
			// Add GST Info //
			if($this->session->userdata('company_gst_info_session') != '') 
			{
				$gst_sess = $this->session->userdata('company_gst_info_session');
				if(is_array($gst_sess)){
				$gst_sess = array_map(function($arr) use($flag){
				   return $arr + ['bus_id' => $flag,'type'=>'business'];

				}, $gst_sess);
				
					$flag_2 = $this->Profile_model->insertData_batch('gst_number',$gst_sess);
					$this->session->unset_userdata('company_gst_info_session');
					
				}
				
			}
			// Add Branch Info //
			if($this->session->userdata('company_branch_session') != '') 
			{
				$branch_sess = $this->session->userdata('company_branch_session');
				if(is_array($branch_sess)){
				$branch_sess = array_map(function($arr) use($flag,$user_id){
				   return $arr + ['bus_id' => $flag,'reg_id'=>$user_id];

				}, $branch_sess);
				
					$branch = $this->Profile_model->insertData_batch('company_branch',$branch_sess);
					$this->session->unset_userdata('company_branch_session');
				}
				
			}
			// Upload Legal and Purchase Order Document //	
			if($image_info['Legaldocuments'] != '' || $image_info['Legaldocuments'] != NULL)
			{
				if($_FILES['Legaldocuments']['name'][0] != NULL)
				{	
						$files = $_FILES['Legaldocuments'];
						
						
						for($i=0;$i<count($files['name']);$i++)
						{
						  $_FILES['Legaldocuments']['name']      = $files['name'][$i];
						  $_FILES['Legaldocuments']['type']      = $files['type'][$i];
						  $_FILES['Legaldocuments']['tmp_name']  = $files['tmp_name'][$i];
						  $_FILES['Legaldocuments']['error']     = $files['error'][$i];
						  $_FILES['Legaldocuments']['size']      = $files['size'][$i];
				
						  $docs=$this->Common_model->upload_org_filename('Legaldocuments','company_legal_documents',$allowd="jpg|jpeg|png|doc|pdf",array('width'=>200,'height'=>300));	

							if($docs != false)
							{
								
								$data1['image'] = $docs['file_name'];
								$data1['doc_size']	= round($docs['file_size']);
										if($this->input->post('legal_po_date'.$i) != '')
										{
											$po_date = date("Y-m-d", strtotime(str_replace('/', '-',$this->input->post('legal_po_date'.$i))));
										}
										else {
											$po_date = '';
										}
										if($this->input->post('start_date'.$i) != '')
										{
											$start_date = date("Y-m-d", strtotime(str_replace('/', '-',$this->input->post('start_date'.$i))));
										}
										else {
											$start_date = '';
										}
										if($this->input->post('end_date'.$i) != '')
										{
											$end_date = date("Y-m-d", strtotime(str_replace('/', '-',$this->input->post('end_date'.$i))));
										}
										else
										{
											$end_date = '';
										}

										$legal_doc_array = array(
											'bus_id'			=>	$flag,
											'legal_document'	=>	$data1['image'],
											'legal_document_size'	=>	$data1['doc_size'],
											'legal_start_date'		=>	$start_date,
											'legal_end_date'		=>	$end_date,
											'legal_reminder'		=>	$this->input->post('legal_remainder'.$i),
											'legal_po_date'      =>	$po_date,
											'legal_po_num'      =>	$this->input->post('legal_po_num'.$i),
											'legal_doc_type'      =>	'legal',
											'legal_type'      =>	'company',
											'master_id'      =>	$flag
											
										);										
										$flag_legal = $this->Profile_model->insertData('legal_document',$legal_doc_array);
							}
						}
				}	
			}
			// Upload Purchase Order Document //	
			if($image_info['purchaseorderdoc'] != '' || $image_info['purchaseorderdoc'] != NULL)
			{
				if($_FILES['purchaseorderdoc']['name'][0] != NULL)
				{	
						$files = $_FILES['purchaseorderdoc'];
						
						for($i=0;$i<count($files['name']);$i++)
						{
						  $_FILES['purchaseorderdoc']['name']      = $files['name'][$i];
						  $_FILES['purchaseorderdoc']['type']      = $files['type'][$i];
						  $_FILES['purchaseorderdoc']['tmp_name']  = $files['tmp_name'][$i];
						  $_FILES['purchaseorderdoc']['error']     = $files['error'][$i];
						  $_FILES['purchaseorderdoc']['size']      = $files['size'][$i];
				
						  $po_docs=$this->Common_model->upload_org_filename('purchaseorderdoc','company_po_documents',$allowd="jpg|jpeg|png|doc|pdf",array('width'=>200,'height'=>300));		
							if($po_docs != false)
							{								
								$data_2['image'] = $po_docs['file_name'];
								$data_2['doc_size'] = $po_docs['file_size'];
									if($this->input->post('po_date'.$i) != '')
										{
											$po_date = date("Y-m-d", strtotime(str_replace('/', '-',$this->input->post('po_date'.$i))));
										}
										else {
											$po_date = '';
										}
										if($this->input->post('po_startdate'.$i) != '')
										{
											$start_date = date("Y-m-d", strtotime(str_replace('/', '-',$this->input->post('po_startdate'.$i))));
										}
										else {
											$start_date = '';
										}
										if($this->input->post('po_enddate'.$i) != '')
										{
											$end_date = date("Y-m-d", strtotime(str_replace('/', '-',$this->input->post('po_enddate'.$i))));
										}
										else
										{
											$end_date = '';
										}

										$po_doc_array = array(
											'bus_id'			=>	$flag,
											'legal_document'	=>	$data_2['image'],
											'legal_document_size'	=>	$data_2['doc_size'],
											'legal_start_date'		=>	$start_date,
											'legal_end_date'		=>	$end_date,
											'legal_reminder'		=>	$this->input->post('purchase_remainder'.$i),
											'legal_po_date'      =>	$po_date,
											'legal_po_num'      =>	$this->input->post('purchase_order'.$i),
											'legal_doc_type'    => 'purchase_order',
											'legal_type'      =>	'company',
											'master_id'      =>	$flag
										);										
										$flag_po = $this->Profile_model->insertData('legal_document',$po_doc_array);
							}
						}
				}	
			}
			// Upload Other Document //	
			if($image_info['otherdoc'] != '' || $image_info['otherdoc'] != NULL)
			{
				if($_FILES['otherdoc']['name'][0] != NULL)
				{	
						$files = $_FILES['otherdoc'];
						
						for($i=0;$i<count($files['name']);$i++)
						{
						  $_FILES['otherdoc']['name']      = $files['name'][$i];
						  $_FILES['otherdoc']['type']      = $files['type'][$i];
						  $_FILES['otherdoc']['tmp_name']  = $files['tmp_name'][$i];
						  $_FILES['otherdoc']['error']     = $files['error'][$i];
						  $_FILES['otherdoc']['size']      = $files['size'][$i];
				
						  $other_docs=$this->Common_model->upload_org_filename('otherdoc','company_other_documents',$allowd="jpg|jpeg|png|doc|pdf",array('width'=>200,'height'=>300));		
							if($other_docs != false)
							{								
								$data_3['image'] = $other_docs['file_name'];
								$data_3['doc_size'] = $other_docs['file_size'];
										$other_doc_array = array(
											'bus_id'	=>	$flag,
											'other_doc_name'		=>	$data_3['image'],
											'other_doc_size'	=>	$data_3['doc_size'],
											'other_document'	=>	$this->input->post('otherdoc_name')[$i],
											'other_doc_type'		=>	'other',
											'other_type'		=>	'company',
											'master_id'      =>	$flag
										);										
								$flag_po = $this->Profile_model->insertData('other_document',$other_doc_array);
							}
						}
				}	
			}
			// Upload Memorandum Document //	
			if($image_info['memorandum'] != '' || $image_info['memorandum'] != NULL)
			{
				if($_FILES['memorandum']['name'][0] != NULL)
				{	
						$files = $_FILES['memorandum'];
						
						for($i=0;$i<count($files['name']);$i++)
						{
						  $_FILES['memorandum']['name']      = $files['name'][$i];
						  $_FILES['memorandum']['type']      = $files['type'][$i];
						  $_FILES['memorandum']['tmp_name']  = $files['tmp_name'][$i];
						  $_FILES['memorandum']['error']     = $files['error'][$i];
						  $_FILES['memorandum']['size']      = $files['size'][$i];
				
						  $other_docs_2=$this->Common_model->upload_org_filename('memorandum','company_moa_documents',$allowd="jpg|jpeg|png|doc|pdf",array('width'=>200,'height'=>300));		
							if($other_docs_2 != false)
							{								
								$data_3['image'] = $other_docs_2['file_name'];
								$data_3['doc_size'] = $other_docs_2['file_size'];
										$other_doc_array = array(
											
											'bus_id'	=>	$flag,
											'other_doc_name'		=>	$data_3['image'],
											'other_doc_size'	=>	$data_3['doc_size'],
											'other_document'	=>	$this->input->post('doc_name'.$i),
											'other_doc_type'		=>	'Memorandum',
											'other_type'		=>	'company',
											'master_id'      =>	$flag
										);										
										$flag_po = $this->Profile_model->insertData('other_document',$other_doc_array);
							}
						}
				}	
			}
			
				$this->session->set_flashdata('success',"Your company profile has been successfully created");
		}
		else
		{
			$this->session->set_flashdata('error',"Error While Processing!!");;	
		}
		$this->session->unset_userdata('company_branch_session');
		$this->session->unset_userdata('company_gst_info_session');
		$this->session->unset_userdata('add_bank_info'); 
		$this->session->unset_userdata('company_services_keywords');
		redirect('profile/manage-company-profile');
	}

	public function update_company_profile()
	{
		$user_id = $this->session->userdata['user_session']['reg_id'];
		$company_id = $this->session->userdata['user_session']['bus_id'];
		$id = $this->input->post('id');
		
		$post = $this->input->post();
		$post=$this->security->xss_clean($post);
		unset($post['same-bill']);
		unset($post['id']);
		unset($post['userid']);
		unset($post['image_info']);
		unset($post['img_folder']);

		unset($post['legal_counter']);
			unset($post['po_counter']);
			unset($post['o_counter']);
			// Unset upload document...........
			unset($post['start_date']);
			unset($post['end_date']);
			unset($post['legal_remainder']);
			
			unset($post['po_date']);
			unset($post['po_startdate']);
			unset($post['po_enddate']);
			unset($post['purchase_remainder']);
			//unset($post['otherdoc_name']);
			unset($post['legal_po_date']);
		$image_info = array();
		$image_info = $_FILES;
		$profile_img ='';
		
		$company_profile = $this->Profile_model->selectData("businesslist",'*',array("bus_id"=>$id));

		if($image_info['bus_company_logo']['name'] != '' && $image_info['bus_company_logo']['name'] != $company_profile[0]->bus_company_logo)
			{
				$profile_image=$this->Common_model->upload_org_filename('bus_company_logo','company_logos/'.$id,$allowd="jpg|jpeg|png",array('width'=>200,'height'=>300));
				if($profile_image!=false){
					$profile_img=$profile_image['file_name'];
				}
			}
			else {
				$profile_img = $company_profile[0]->bus_company_logo;
			}
		/*$i=0;
		$l=$post['legal_counter'];
		$p=$post['po_counter'];
		$o=$post['o_counter'];
		$m=$post['m_counter'];
		unset($post['legal_counter']);
		unset($post['po_counter']);
		unset($post['o_counter']);
		unset($post['m_counter']);
		for($j=0;$j<$l;$j++){
			unset($post['start_date'.$j]);
			unset($post['end_date'.$j]);
			unset($post['legal_remainder'.$j]);
			unset($post['legal_po_num'.$j]);
			unset($post['legal_po_date'.$j]);

		}
		for($j=0;$j<$p;$j++){
			unset($post['purchase_order'.$j]);
			unset($post['po_date'.$j]);
			unset($post['po_startdate'.$j]);
			unset($post['po_enddate'.$j]);
			unset($post['purchase_remainder'.$j]);
		}
		for($j=0;$j<$o;$j++){
			unset($post['otherdoc_name'.$j]);
		}
		for($j=0;$j<$m;$j++){
			unset($post['doc_name'.$j]);
		}*/
  $legaldata=array();

			if(isset($post['legal_po_date100']) ){
				if($post['legal_po_date100']!=""){

				$legaldata['legal_po_date'] = date("Y-m-d", strtotime(str_replace('/', '-',($post['legal_po_date100']))));
			}else{
				$legaldata['legal_po_date'] = date("0000-00-00");
			}
				unset($post['legal_po_date100']);
			}
			
 
			if(isset($post['start_date100'])){
				if($post['start_date100']!=""){
				$legaldata['legal_start_date'] = date("Y-m-d", strtotime(str_replace('/', '-',($post['start_date100']))));
			}else{
				$legaldata['legal_start_date'] = date("0000-00-00");
			}
				unset($post['start_date100']);
			}
			


			if(isset($post['end_date100'])){
				if($post['end_date100']!=""){
				$legaldata['legal_end_date'] = date("Y-m-d", strtotime(str_replace('/', '-',($post['end_date100']))));
			}else{
				$legaldata['legal_end_date'] = date("0000-00-00");
			}
				unset($post['end_date100']);
			}
			

			if(isset($post['legal_remainder100'])){
				$legaldata['legal_reminder'] = $post['legal_remainder100'];
				unset($post['legal_remainder100']);
			}


			if(isset($post['legal_po_num100'])){
				$legaldata['legal_po_num'] = $post['legal_po_num100'];
				unset($post['legal_po_num100']);
			}

			

			if(isset($post['legal_id'])){
				$legalId = $post['legal_id'];
				unset($post['legal_id']);
			}

            

			$podata=array();
			if(isset($post['po_date100'])){
				if($post['po_date100']!=""){
					$podata['legal_po_date'] = date("Y-m-d", strtotime(str_replace('/', '-',($post['po_date100']))));
				}else{
					$podata['legal_po_date'] = date("0000-00-00");
				}
				
				unset($post['po_date100']);
			}

			if(isset($post['po_startdate100'])){
				if($post['po_startdate100']!=""){
					$podata['legal_start_date'] = date("Y-m-d", strtotime(str_replace('/', '-',($post['po_startdate100']))));
				}else{
					$podata['legal_start_date'] = date("0000-00-00");
				}
			
				unset($post['po_startdate100']);
			}


			if(isset($post['po_enddate100'])){
				if($post['po_enddate100']!=""){
					$podata['legal_end_date'] = date("Y-m-d", strtotime(str_replace('/', '-',($post['po_enddate100']))));
				}else{
					$podata['legal_end_date'] = date("0000-00-00");
				}
				
				unset($post['po_enddate100']);
			}

			if(isset($post['purchase_remainder100'])){
				$podata['legal_reminder'] = $post['purchase_remainder100'];
				unset($post['purchase_remainder100']);
			}

			if(isset($post['purchase_order100'])){
				$podata['legal_po_num'] = $post['purchase_order100'];
				unset($post['purchase_order100']);
			}
            
			if(isset($post['po_id'])){
				$poId = $post['po_id'];
				unset($post['po_id']);
			}


        $i=0;
		$l=count($_FILES['Legaldocuments']);
		$p=count($_FILES['purchaseorderdoc']);
		$o=count($_FILES['otherdoc']);
		$m=count($_FILES['memorandum']);
		
		unset($post['legal_counter']);
		unset($post['po_counter']);
		unset($post['o_counter']);
		unset($post['m_counter']);
	
		for($j=0;$j<$l;$j++){
			unset($post['start_date'.$j]);
			unset($post['end_date'.$j]);
			unset($post['legal_remainder'.$j]);
			unset($post['legal_po_num'.$j]);
			unset($post['legal_po_date'.$j]);
		}
		for($j=0;$j<$p;$j++){
			unset($post['purchase_order'.$j]);
			unset($post['po_date'.$j]);
			unset($post['po_startdate'.$j]);
			unset($post['po_enddate'.$j]);
			unset($post['purchase_remainder'.$j]);
		}
		for($j=0;$j<$o;$j++){
			unset($post['otherdoc_name'.$j]);

		}
		for($j=0;$j<$m;$j++){
			unset($post['doc_name'.$j]);
		}
		
		if(isset($post['cash_balance_date']) && $post['cash_balance_date'] != ''){
			$post['cash_balance_date'] = date("Y-m-d", strtotime(str_replace('/', '-',($post['cash_balance_date']))));
		}	
		else {
			$post['cash_balance_date'] ='';
		}
		if(isset($post['opening_balance_date']) && $post['opening_balance_date'] != '' ){
			$post['opening_balance_date'] = date("Y-m-d", strtotime(str_replace('/', '-',($post['opening_balance_date']))));
		}	
		else {
			$post['opening_balance_date'] ='';
		}

		if(isset($post['petty_balance_date']) && $post['petty_balance_date'] != '' ){
			$post['petty_balance_date'] = date("Y-m-d", strtotime(str_replace('/', '-',($post['petty_balance_date']))));
		}	
		else {
			$post['opening_balance_date'] ='';
		}
		if($post['bus_fy_startdate'] != ''){
			$post['bus_fy_startdate'] = date("Y-m-d", strtotime(str_replace('/', '-',($post['bus_fy_startdate']))));
		}	
		else {
			$post['bus_fy_startdate'] ='';
		}
		if($post['bus_fy_enddate'] != ''){
			$post['bus_fy_enddate'] = date("Y-m-d", strtotime(str_replace('/', '-',($post['bus_fy_enddate']))));
		}	
		else {
			$post['bus_fy_enddate'] ='';
		}
		if($post['bus_incorporation_date'] != ''){
			$post['bus_incorporation_date'] = date("Y-m-d", strtotime($post['bus_incorporation_date']));
		}	
		else {
			$post['bus_incorporation_date'] ='';
		}
		
		if($profile_img != '')
		{
			$post['bus_company_logo'] = $profile_img;
		}
		if($company_profile[0]->bus_pancard!=$post['bus_pancard']){
				$fromemail = $this->Sales_model->selectData('businesslist','*',array('bus_id'=>$bus_id,'reg_id'=>$reg_id,'status'=>'Active'));
				$data['name']=  "PAN";
				$data['customer_id']=  "profile/view-company-profile/".$id;
				$to =  $this->user_session['email'];
				$from = $fromemail[0]->bus_company_name;
				$subject = 'Your PAN details have just been updated!';
				$message=$this->load->view('email_template/change-pancard',$data,true);
				sendEmail($from,$to,$subject,$message);
			
		}

		if(isset($post['other_id'])){
              foreach ($post['other_id'] as $key => $value) {
                	$otherId=$value;
                	$otherdata['other_document']=$post['otherdoc_name'][$key];

                	 $flag_other=$this->Profile_model->updateData('other_document',$otherdata,array('other_doc_id'=>$otherId));
                	//print $this->db->last_query();

                	unset($post['other_id'][$key]);
                	unset($post['otherdoc_name'][$key]);
                }  

                unset($post['other_id']);
                	unset($post['otherdoc_name']);

			}
             
             unset($post['otherdoc_name']);

		$flag = $this->Profile_model->updateData('businesslist',$post,array('bus_id'=>$id));

		
		if($flag != false)
		{

			// Activity Tracker start
		
			 $tracking_info['module_name'] = 'My Company Profile';
			 $tracking_info['action_taken'] = 'Edited';
			 $tracking_info['reference'] = $post['bus_company_name'];
			 $LogRec = $this->Adminmaster_model->insertActivity($tracking_info);

			// Activity Tracker End
             if(isset($legalId)){
              $flag_legal=$this->Profile_model->updateData('legal_document',$legaldata,array('legal_id'=>$legalId,'bus_id'=>$id,'legal_type'=>'company'));
           }

            if(isset($poId)){
               $flag_po=$this->Profile_model->updateData('legal_document',$podata,array('legal_id'=>$poId,'bus_id'=>$id,'legal_type'=>'company'));
           }
       

			if($image_info['Legaldocuments'] != '' || $image_info['Legaldocuments'] != NULL)
			{
				if($_FILES['Legaldocuments']['name'][0] != NULL)
				{	
						$files = $_FILES['Legaldocuments'];
						for($i=0;$i<count($files['name']);$i++)
						{
						  $_FILES['Legaldocuments']['name']      = $files['name'][$i];
						  $_FILES['Legaldocuments']['type']      = $files['type'][$i];
						  $_FILES['Legaldocuments']['tmp_name']  = $files['tmp_name'][$i];
						  $_FILES['Legaldocuments']['error']     = $files['error'][$i];
						  $_FILES['Legaldocuments']['size']      = $files['size'][$i];
				
						  $docs=$this->Common_model->upload_org_filename('Legaldocuments','company_legal_documents',$allowd="jpg|jpeg|png|doc|pdf",array('width'=>200,'height'=>300));		
							if($docs != false)
							{
								
								$data1['image'] = $docs['file_name'];
								$data1['doc_size']	= round($docs['file_size']);
										if($this->input->post('legal_po_date'.$i) != '')
										{
											$po_date = date("Y-m-d", strtotime(str_replace('/', '-',$this->input->post('legal_po_date'.$i))));
										}
										else {
											$po_date = '';
										}
										if($this->input->post('start_date'.$i) != '')
										{
											$start_date = date("Y-m-d", strtotime(str_replace('/', '-',$this->input->post('start_date'.$i))));
										}
										else {
											$start_date = '';
										}
										if($this->input->post('end_date'.$i) != '')
										{
											$end_date = date("Y-m-d", strtotime(str_replace('/', '-',$this->input->post('end_date'.$i))));
										}
										else
										{
											$end_date = '';
										}

										$legal_doc_array = array(
											'bus_id'				=>	$id,
											'legal_document'		=>	$data1['image'],
											'legal_document_size'	=>	$data1['doc_size'],
											'legal_start_date'		=>	$start_date,
											'legal_end_date'		=>	$end_date,
											'legal_reminder'		=>	$this->input->post('legal_remainder'.$i),
											'legal_po_date'    		=>	$po_date,
											'legal_po_num'      	=>	$this->input->post('legal_po_num'.$i),
											'legal_doc_type'      	=>	'legal',
											'legal_type'      		=>	'company',
											'master_id'      		=>	$id
											
										);		

										$flag_legal = $this->Profile_model->insertData('legal_document',$legal_doc_array);

									}
							}
				}	
			}
			if($image_info['purchaseorderdoc'] != '' || $image_info['purchaseorderdoc'] != NULL)
			{
				if($_FILES['purchaseorderdoc']['name'][0] != NULL)
				{	
						$files = $_FILES['purchaseorderdoc'];
						
						for($i=0;$i<count($files['name']);$i++)
						{
						  $_FILES['purchaseorderdoc']['name']      = $files['name'][$i];
						  $_FILES['purchaseorderdoc']['type']      = $files['type'][$i];
						  $_FILES['purchaseorderdoc']['tmp_name']  = $files['tmp_name'][$i];
						  $_FILES['purchaseorderdoc']['error']     = $files['error'][$i];
						  $_FILES['purchaseorderdoc']['size']      = $files['size'][$i];
				
						  $po_docs=$this->Common_model->upload_org_filename('purchaseorderdoc','company_po_documents',$allowd="jpg|jpeg|png|doc|pdf",array('width'=>200,'height'=>300));		
							if($po_docs != false)
							{								
								$data_2['image'] = $po_docs['file_name'];
								$data_2['doc_size'] = $po_docs['file_size'];
									if($this->input->post('po_date'.$i) != '')
										{
											$po_date = date("Y-m-d", strtotime(str_replace('/', '-',$this->input->post('po_date'.$i))));
										}
										else {
											$po_date = '';
										}
										if($this->input->post('po_startdate'.$i) != '')
										{
											$start_date = date("Y-m-d", strtotime(str_replace('/', '-',$this->input->post('po_startdate'.$i))));
										}
										else {
											$start_date = '';
										}
										if($this->input->post('po_enddate'.$i) != '')
										{
											$end_date = date("Y-m-d", strtotime(str_replace('/', '-',$this->input->post('po_enddate'.$i))));
										}
										else
										{
											$end_date = '';
										}

										$po_doc_array = array(
											'bus_id'			=>	$id,
											'legal_document'	=>	$data_2['image'],
											'legal_document_size'	=>	$data_2['doc_size'],
											'legal_start_date'		=>	$start_date,
											'legal_end_date'		=>	$end_date,
											'legal_reminder'		=>	$this->input->post('purchase_remainder'.$i),
											'legal_po_date'      =>	$po_date,
											'legal_po_num'      =>	$this->input->post('purchase_order'.$i),
											'legal_doc_type'    => 'purchase_order',
											'legal_type'      =>	'company',
											'master_id'      =>	$id
										);										
										$flag_po = $this->Profile_model->insertData('legal_document',$po_doc_array);
							}
						}
				}	
			}
			if($image_info['otherdoc'] != '' || $image_info['otherdoc'] != NULL)
			{
				if($_FILES['otherdoc']['name'][0] != NULL)
				{	
						$files = $_FILES['otherdoc'];
						
						for($i=0;$i<count($files['name']);$i++)
						{
						  $_FILES['otherdoc']['name']      = $files['name'][$i];
						  $_FILES['otherdoc']['type']      = $files['type'][$i];
						  $_FILES['otherdoc']['tmp_name']  = $files['tmp_name'][$i];
						  $_FILES['otherdoc']['error']     = $files['error'][$i];
						  $_FILES['otherdoc']['size']      = $files['size'][$i];
				
						  $other_docs=$this->Common_model->upload_org_filename('otherdoc','company_other_documents',$allowd="jpg|jpeg|png|doc|pdf",array('width'=>200,'height'=>300));		
							if($other_docs != false)
							{								
								$data_3['image'] = $other_docs['file_name'];
								$data_3['doc_size'] = $other_docs['file_size'];
										$other_doc_array = array(
											'bus_id'	=>	$id,
											'other_doc_name'		=>	$data_3['image'],
											'other_doc_size'	=>	$data_3['doc_size'],
											'other_document'	=>	$this->input->post('otherdoc_name'.$i),
											'other_doc_type'		=>	'other',
											'other_type'		=>	'company',
											'master_id'      =>	$id,
										);										
								$flag_po = $this->Profile_model->insertData('other_document',$other_doc_array);
							}
						}
				}	
			}
			
			if($image_info['memorandum'] != '' || $image_info['memorandum'] != NULL)
			{
				if($_FILES['memorandum']['name'][0] != NULL)
				{	
						$files = $_FILES['memorandum'];
						
						for($i=0;$i<count($files['name']);$i++)
						{
						  $_FILES['memorandum']['name']      = $files['name'][$i];
						  $_FILES['memorandum']['type']      = $files['type'][$i];
						  $_FILES['memorandum']['tmp_name']  = $files['tmp_name'][$i];
						  $_FILES['memorandum']['error']     = $files['error'][$i];
						  $_FILES['memorandum']['size']      = $files['size'][$i];
				
						  $other_docs_2=$this->Common_model->upload_org_filename('memorandum','company_moa_documents',$allowd="jpg|jpeg|png|doc|pdf",array('width'=>200,'height'=>300));		
							if($other_docs_2 != false)
							{								
								$data_3['image'] = $other_docs_2['file_name'];
								$data_3['doc_size'] = $other_docs_2['file_size'];
										$other_doc_array = array(
											
											'bus_id'	=>	$id,
											'other_doc_name'		=>	$data_3['image'],
											'other_doc_size'	=>	$data_3['doc_size'],
											'other_document'	=>	$this->input->post('doc_name'.$i),
											'other_doc_type'		=>	'Memorandum',
											'other_type'		=>	'company',
											'master_id'      =>	$id,
										);										
										$flag_po = $this->Profile_model->insertData('other_document',$other_doc_array);
							}
						}
				}	
			}
			
			$this->session->set_flashdata('success',"Company Profile has been updated successfully");
		}
		else
		{
			$this->session->set_flashdata('error',"Error while processing!!");
		}
		if($company_id==$id){

			if($this->session->userdata['user_session']['ei_gst']==''){
			$new_gst_data = $this->Profile_model->selectData('gst_number','*',array('bus_id'=>$id,'type'=>'business','status'=>'Active'),'gst_id','ASC','',1);
			$this->session->userdata['user_session']['ei_gst']=$new_gst_data[0]->gst_id;
			}
			//print_r($new_gst_data);exit();
		}
		
		redirect('profile/company-profile');
	}

	public function edit_company_profile($id="")
	{
		//$id = str_replace(array('-', '_', '~'), array('+', '/', '='), $id);
		//$org_id = $this->encrypt->decode($id);
		$user_id = $this->session->userdata['user_session']['reg_id'];

		$data['company_profile'] = $this->Profile_model->selectData('businesslist', '*', array('bus_id'=>$id));
		 
		//$this->data['services'] = $this->Profile_model->get_by_condition('EI_services',array('company_id'=>$org_id));
		$data['countries']	=	$this->Profile_model->selectData('countries', '*','','country_id','ASC');
		$data['currency']	=	$this->Profile_model->selectData('currency', '*','','currency_id','ASC');
		$data['company_type'] = $this->Profile_model->selectData('company_type', '*','','id','ASC');
		$data['legal_documents'] = $this->Profile_model->selectData('legal_document', '*',array('bus_id'=>$id,'legal_type'=>'company'));
		$data['other_documents'] = $this->Profile_model->selectData('other_document', '*',array('bus_id'=>$id,'other_type'=>'company'));
		//$data['gst_details'] = $this->Profile_model->selectData('gst_number','*', array('bus_id'=>$id,'status'=>'Active'));
		$data['gst_details'] = $this->Profile_model->selectData('gst_number','*', array('bus_id'=>$id,'type'=>'business'));
		$data['branch_details'] = $this->Profile_model->selectData('company_branch', '*', array('bus_id'=>$id));
		$data['bank_details'] = $this->Profile_model->get_company_bank_details($id);

		$data['card_details'] = $this->Profile_model->selectData('company_cards','*', array('bus_id'=>$id));


		$cmp_profile = $data['company_profile'][0];
		if($cmp_profile->bus_billing_country != '')
		{
			$data['state']=$this->Profile_model->selectData('states', '*',array('country_id'=>$cmp_profile->bus_billing_country),'state_id','ASC');	
		}
		if($cmp_profile->bus_shipping_country != '')
		{
			$data['shipping_state']=$this->Profile_model->selectData('states', '*',array('country_id'=>$cmp_profile->bus_shipping_country),'state_id','ASC');	
		}
		if($cmp_profile->bus_billing_state!= '')
		{
			$data['cities']=$this->Profile_model->selectData('cities', '*',array('state_id'=>$cmp_profile->bus_billing_state),'city_id','ASC');	
		}
		if($cmp_profile->bus_shipping_state != '')
		{
			$data['shipping_cities']=$this->Profile_model->selectData('cities', '*',array('state_id'=>$cmp_profile->bus_shipping_state),'city_id','ASC');	
		}
		$this->load->view('profile/edit_company_profile',$data);
	}
	public function add_service_keywords_2()
	{
		$keywords = array();
		$_POST=$this->security->xss_clean($_POST);
		parse_str($_POST['frm'],$keywords);
		
		$company_id = $keywords['add_for_company_id'];
		$company_info = $this->Profile_model->selectData('businesslist','*',array('bus_id'=>$company_id));
		$old_service_keywords = $company_info[0]->bus_services_keywords;
		$old_service = explode("|@|",$old_service_keywords);
		for($i=1;$i<=10;$i++)
		{
			if($keywords['keyword_'.$i] != '' && $keywords['keyword_'.$i] != NULL) {
				$words[] =  $keywords['keyword_'.$i];	
			}
		}
		if(count($words) != 0) {
			$output = array_unique(array_merge($old_service, $words));
			$keyword_str = implode("|@|",$output);
			$filter = array(
				'bus_services_keywords'	=>	$keyword_str
			);
			$flag =$this->Profile_model->updateData('businesslist',$filter,array('bus_id'=>$company_id));
			
			if($flag){
			$service = $this->Profile_model->selectData('businesslist','*',array('bus_id'=>$company_id));	
			echo json_encode($service);
			}
			else
			{
				echo false;
			}
		} else {
			echo false;	
		}
	}
	
	public function add_branch_info_2()
	{
		$branch_info = array();
		$_POST=$this->security->xss_clean($_POST);
		parse_str(($_POST['frm']),$branch_info);	
		$insert_branch = array(
			'bus_id'	=>	$branch_info['add_branch_for_cmp'],
			'reg_id'	=>	$branch_info['add_branch_for_user'],
			'cbranch_name'	=>	$branch_info['branch_name'],
			'cbranch_city'	=>	$branch_info['branch_city']
		);
		$flag=$this->Profile_model->insertData('company_branch',$insert_branch);
		if($flag != false)
		{
			$branch_data = $this->Profile_model->selectData('company_branch','*',array('bus_id'=>$branch_info['add_branch_for_cmp']));
			echo json_encode($branch_data);
			
		}
		else
		{
			echo false;
		}
	}

    public function add_new_card()
	{
		$bus_id = $this->user_session['bus_id'];//$this->input->post('bus_id');
		$card_info = array();
		$_POST=$this->security->xss_clean($_POST);
		parse_str(($_POST['frm']),$card_info);	
		$i=0;
			  foreach($card_info['card'] as $card){

			  	if($card!=""){
		$insert_card = array(
			'bus_id'	=>	$bus_id,
			'card_number' 	=>  "",
			'card_type'		=>	$card,
			'bank_name'		=>	$card_info['iss_bank'][$i],
			
		);
		$flag=$this->Profile_model->insertData('company_cards',$insert_card);
	}

}

	$card_data = $this->Profile_model->selectData('company_cards','*',array('bus_id'=>$bus_id));
			echo json_encode($card_data);
		/*if($flag != false)
		{
			$card_data = $this->Profile_model->selectData('company_card','*',array('bus_id'=>$bus_id));
			echo json_encode($card_data);
		}
		else
		{
			echo false;
		}*/
	}
	public function add_new_bank()
	{
		$bus_id = $this->user_session['bus_id'];//$this->input->post('bus_id');
		$bank_info = array();
		$_POST=$this->security->xss_clean($_POST);
		parse_str(($_POST['frm']),$bank_info);	
		$insert_bank = array(
			'bus_id'	=>	$bus_id,
			'cbank_account_no' 	=>  $bank_info['ac_number'],
			'cbank_name'		=>	$bank_info['bank_name'],
			'cbank_branch_name'		=>	$bank_info['bank_branch_name'],
			'cbank_account_type'	=>	$bank_info['acc_type'],
			'cbank_ifsc_code'		=>	@$bank_info['ifsc_code'],
			'cbank_swift_code'		=>	@$bank_info['swift_code'],
			'cbank_country_code'		=>	@$bank_info['bus_bank_country'],
			'cbank_currency_code'		=>	$bank_info['bank_cur_check'],
			'opening_bank_balance'		=>	$bank_info['opening_bank_balance'],
                    'opening_balance_date'		=>	date("Y-m-d", strtotime(str_replace('/', '-',($bank_info['opening_balance_date'])))),
		);
		$flag=$this->Profile_model->insertData('company_bank',$insert_bank);
		if($flag != false)
		{
			$bank_data = $this->Profile_model->selectData('company_bank','*',array('bus_id'=>$bus_id));
			echo json_encode($bank_data);
		}
		else
		{
			echo false;
		}
	}

	public function edit_card_info()
	{  
     $bus_id = $this->user_session['bus_id'];
		$info = array();
		$_POST=$this->security->xss_clean($_POST);
		parse_str($_POST['frm'],$info);
		$card_info	=	array(		
				'card_type' 	=>  $info['card'],
				'bank_name'		=>	$info['iss_bank'],
				'status'		=>	$info['status'],
				

		);
		$data['name']= $this->user_session['ei_username'];
		
	

		$flag=$this->Profile_model->updateData('company_cards',$card_info,array('bus_id'=>$bus_id,'ccard_id'=>$info['ccard_id']));
		if($flag != false)
		{
			$card_data = $this->Profile_model->selectData('company_cards','*',array('bus_id'=>$bus_id));
			echo json_encode($card_data);
			exit;
		}
		else
		{
			echo false;
		}

	}

	public function edit_card_new_info()
	{  
     $bus_id = $this->user_session['bus_id'];
		$info = array();
		$_POST=$this->security->xss_clean($_POST);
		parse_str($_POST['frm'],$info);
		$card_info	=	array(	
		'bus_id'=>$bus_id,
				'card_type' 	=>  $info['card'],
				'bank_name'		=>	$info['iss_bank'],
				'status'		=>	$info['status'],
				

		);
		$data['name']= $this->user_session['ei_username'];
		
	

		$flag=$this->Profile_model->insertData('company_cards',$card_info);
		if($flag != false)
		{
			$card_data = $this->Profile_model->selectData('company_cards','*',array('bus_id'=>$bus_id));
			echo json_encode($card_data);
			exit;
		}
		else
		{
			echo false;
		}

	}

	public function edit_bank_info()
	{
		$info = array();
		$_POST=$this->security->xss_clean($_POST);
		parse_str($_POST['frm'],$info);
		$bank_info	=	array(		
				'cbank_account_no' 	=>  $info['edit_ac_number'],
				'cbank_name'		=>	$info['edit_bank_name'],
				'cbank_branch_name'		=>	$info['edit_bank_branch_name'],
				'cbank_account_type'	=>	$info['edit_acc_type'],
				'cbank_ifsc_code'		=>	@$info['ifsc_code'],
				'cbank_swift_code'		=>	@$info['swift_code'],
				'cbank_country_code'		=>	@$info['bus_bank_country'],
				'cbank_currency_code'		=>	$info['bank_cur_check'],
				'opening_bank_balance'		=>	$info['opening_bank_balance'],
                    'opening_balance_date'		=>	date("Y-m-d", strtotime(str_replace('/', '-',($info['opening_balance_date'])))),

                    'status'		=>	$info['status'],

		);
		$fromemail = $this->Sales_model->selectData('businesslist','*',array('bus_id'=>$bus_id,'reg_id'=>$reg_id,'status'=>'Active'));
		$data['name']= $this->user_session['ei_username'];
		$data['cmp_id']=  $info['edit_cmpid'];
		$to = $this->user_session['email'];
		$from = $fromemail[0]->bus_company_name;
		$subject = 'Your bank details have been updated!';
		$message=$this->load->view('email_template/change-bank',$data,true);
		sendEmail($from,$to,$subject,$message);
	

		$flag=$this->Profile_model->updateData('company_bank',$bank_info,array('cbank_id'=>$info['edit_cbank_id']));
		if($flag != false)
		{
			$bank_data =$this->Profile_model->get_company_bank_details($info['edit_cmpid']);
			//$bank_data = $this->Profile_model->selectData('company_bank','*',array('bus_id'=>$info['edit_cmpid']));
			echo json_encode($bank_data);
			exit;
		}
		else
		{
			echo false;
		}
	}

	public function add_another_gstin_info()
	{
		
		$gst_info = array();
		$_POST=$this->security->xss_clean($_POST);
		parse_str($_POST['frm'], $gst_info);
		$gst_array = $gst_info['gst_array'];
		//while (list($key, $value) = each($gst_array)) {
		foreach($gst_array as $key=>$value){
			if($gst_info['gstin'.$value]!='' && $gst_info['location'.$value] != '')
			{
				$new_gst_info = array(
					
					'bus_id'	=>	$gst_info['company_id_gst_2'],
					'gst_no'	=>	strtoupper($gst_info['gstin'.$value]),
					'place'	=>	$gst_info['location'.$value],
					'country'	=>	$gst_info['bus_billing_country'.$value],
					'state_code'	=>	$gst_info['bus_billing_state'.$value],
					'address'	=>	$gst_info['bus_billing_address'.$value],
					'city'	=>	$gst_info['bus_billing_city'.$value],
					'zipcode'	=>	$gst_info['bus_billing_zipcode'.$value],
					'email'	=>	$gst_info['bus_email_id'.$value],
					'contact'	=>	$gst_info['bus_billing_contact'.$value],
					'type'	=>	"business"
				);
				$flag = $this->Profile_model->insertData('gst_number', $new_gst_info);
			}
			else {
				echo  false;	
			}
		}
		$new_gst_data = $this->Profile_model->selectData('gst_number','*',array('bus_id'=>$gst_info['company_id_gst_2'],'type'=>'business'));
		echo json_encode($new_gst_data);
	}
	public function edit_gstin_info()
	{
	
		$gst_info = array();
		$_POST=$this->security->xss_clean($_POST);
		parse_str(($_POST['frm']),$gst_info);
		$old_gst_info = $this->Profile_model->selectData('gst_number','*',array('gst_id'=>$gst_info['cmp_gst_id']));
		if($old_gst_info != '')
		{
			$update_array = array(
				'gst_no'	=>	strtoupper($gst_info['gst_no_edit']),
				'place'	=>	$gst_info['gst_edit_location'],
				'country'	=>	$gst_info['bus_billing_country1'],
				'state_code'	=>	$gst_info['bus_billing_state1'],
				'address'	=>	$gst_info['bus_billing_address1'],
				'city'	=>	$gst_info['bus_billing_city1'],
				'zipcode'	=>	$gst_info['bus_billing_zipcode1'],
				'email'	=>	$gst_info['bus_email_id1'],
				'contact'	=>	$gst_info['bus_billing_contact1'],
				'status'	=>	$gst_info['bus_billing_status1'],
				'type'	=>	"business"
			);	
			$flag = $this->Profile_model->updateData('gst_number',$update_array,array('gst_id'=>$gst_info['cmp_gst_id']));
			if($flag != false)
			{
				if($old_gst_info[0]->gst_no!=$gst_info['gst_no_edit']){
					$fromemail = $this->Sales_model->selectData('businesslist','*',array('bus_id'=>$bus_id,'reg_id'=>$reg_id,'status'=>'Active'));
					$data['name']=  "GST";
					$data['customer_id']=  "profile/view-company-profile/".$old_gst_info[0]->bus_id;
					$to =  $this->user_session['email'];
					$from = $fromemail[0]->bus_company_name;
					$subject = 'Your GST details have just been updated!';
					$message=$this->load->view('email_template/change-pancard',$data,true);
					sendEmail($from,$to,$subject,$message);
				}
				$new_gst_data = $this->Profile_model->selectData('gst_number','*',array('bus_id'=>$gst_info['cmp_id_edit_gst'],'type'=>'business'));	
				echo json_encode($new_gst_data);
			}
		}
		else
		{
			echo false;	
		}
	}
	public function delete_gst_info()
	{
		$gst_id = $this->input->post('gst_id');
		$bus_id = $this->input->post('companyid');
		$update_array = array(
				'status'	=>	"Inactive",
			);	
		$flag = $this->Profile_model->updateData('gst_number',$update_array,array('gst_id'=>$gst_id,'bus_id'=>$bus_id,'type'=>'business'));
		//$flag=$this->Profile_model->deleteData("gst_number",array('gst_id'=>$gst_id,"bus_id"=>$bus_id));
		if($flag){
			
			//$gst = $this->Profile_model->selectData('gst_number','*',array('bus_id'=>$bus_id,'status'=>'Active'));	
			$gst = $this->Profile_model->selectData('gst_number','*',array('bus_id'=>$bus_id,'type'=>'business'));	
			echo json_encode($gst);
		}
		else
		{
			echo false;
		}
	}
	public function delete_service_info()
	{
		$service = $this->input->post('service_id');
		$company_id = $this->input->post('company_id');
		$company_data = $this->Profile_model->selectData('businesslist','*',array("bus_id"=>$company_id));
		$keywords = explode("|@|",$company_data[0]->bus_services_keywords);

		foreach ($keywords as $key => $value) {
			if ($service === $value) {
				unset($keywords[$key]);
			}
		}
		$new_keywords = implode("|@|",$keywords);
		$filter = array(
			'bus_services_keywords'	=>	$new_keywords,
		);
		$flag = $this->Profile_model->updateData('businesslist',$filter,array('bus_id'=>$company_id));
		if($flag){
			
			$service = $this->Profile_model->selectData('businesslist','*',array('bus_id'=>$company_id));	
			echo json_encode($service);
		}
		else
		{
			echo false;
		}
	}
	public function delete_branch_info()
	 {
		$branch_id = $this->input->post('branch_id');
		$bus_id = $this->input->post('company_id');

		$flag=$this->Profile_model->deleteData("company_branch",array('cbranch_id'=>$branch_id,'bus_id'=>$bus_id));
		if($flag){			
			$branchs = $this->Profile_model->selectData('company_branch','*',array('bus_id'=>$bus_id));
			echo json_encode($branchs);
			
		}
		else
		{
			echo false;
		}
	}

	public function delete_bank_info()
	 {
		$cbank_id = $this->input->post('cbank_id');
		$bus_id = $this->user_session['bus_id'];//$this->input->post('bus_id');
        $update_array = array(
				'status'	=>	"Inactive",
			);	
		$flag = $this->Profile_model->updateData('company_bank',$update_array,array('cbank_id'=>$cbank_id,'bus_id'=>$bus_id));
		//$flag=$this->Profile_model->deleteData("company_bank",array('cbank_id'=>$cbank_id,'bus_id'=>$bus_id));
		if($flag){			
			$banks = $this->Profile_model->selectData('company_bank','*',array('bus_id'=>$bus_id));
			echo json_encode($banks);
			
		}
		else
		{
			echo false;
		}
	}

	public function delete_card_info()
	 {
		$ccard_id = $this->input->post('ccard_id');
		$bus_id = $this->user_session['bus_id'];//$this->input->post('bus_id');
        $update_array = array(
				'status'	=>	"Inactive",
			);	
		$flag = $this->Profile_model->updateData('company_cards',$update_array,array('ccard_id'=>$ccard_id,'bus_id'=>$bus_id));
		
		//$flag=$this->Profile_model->deleteData("company_bank",array('cbank_id'=>$cbank_id,'bus_id'=>$bus_id));
		if($flag){			
			$cards = $this->Profile_model->selectData('company_cards','*',array('bus_id'=>$bus_id));
			echo json_encode($cards);
			
		}
		else
		{
			echo false;
		}
	}

	function email_company_profile(){

		// print_r($_POST);
		// exit();
		$data = $_POST;
		$_POST=$this->security->xss_clean($_POST);
		$id = $data['company_id'];

		$user_id = $this->session->userdata['user_session']['reg_id'];
		$bus_id = $this->session->userdata['user_session']['bus_id'];

		$data['company_profile'] = $this->Profile_model->selectDataxls($id);
		$data['legal_documents'] = $this->Profile_model->selectData('legal_document', '*',array('bus_id'=>$id,'legal_type'=>'company'));
		$data['other_documents'] = $this->Profile_model->selectData('other_document', '*',array('bus_id'=>$id,'other_type'=>'company'));
		$data['gst_details'] = $this->Profile_model->selectData('gst_number','*', array('bus_id'=>$id,'status'=>'Active'));
		$data['branch_info'] = $this->Profile_model->selectData('company_branch', '*', array('bus_id'=>$id));
		$data['bank_details'] = $this->Profile_model->selectData('company_bank', '*', array('bus_id'=>$id));

		$html_data = $this->load->view('profile/email-company-profile',$data,true);
		//$attachment = $this->pdf->create($html_data,'attachment.pdf','email');
		//$attachment_name = "company_profile";
		$fromemail = $this->Sales_model->selectData('businesslist','*',array('bus_id'=>$bus_id,'reg_id'=>$reg_id,'status'=>'Active'));
		$invoname=str_replace('/','-','Company Profile')."_".time();
		$downld=$this->pdf->create($html_data,$invoname,'download_multi');
		$path=DOC_ROOT.'/public/pdfmail/'.$invoname.'.pdf';
		$attachment[0]=$path;

		$cc = $data['email_cc'];
		$cc = preg_split("/(,|;)/", $cc);
		

		//$to =  $data['email_to'];
		$from = $fromemail[0]->bus_company_name;
		$subject = $data['email_subject'];
		$message = $data['email_message'];
		//$message=$this->load->view('email_template/subscription_detail',$data,true);
		$all_emails =  $data['email_to'];
		$to = preg_split("/(,|;)/", $all_emails);
		
		//$status = sendEmail($to,$subject,$content);
		$status = sendInvoiceEmailMultiple($from,$to,$subject,$message,$attachment,$invoname,$cc);
		for($i=0; $i < count($attachment); $i++)
		{
			unlink($attachment[$i]);
		}

		if($status)
		{
			// Activity Tracker start
		
				 $tracking_info['module_name'] = 'My Company Profile';
				 $tracking_info['action_taken'] = 'Emailed';
				 $tracking_info['reference'] = $data['company_profile'][0]->bus_company_name;
				 $LogRec = $this->Adminmaster_model->insertActivity($tracking_info);

			// Activity Tracker End

			echo 1;

		}else{
			echo 0;
		}

		//sendEmail($this->session->userdata['user_session']['email'],'Company Profile',$content);
		//echo json_encode(true);

	}

	
	function print_company_profile($id){

		$user_id = $this->session->userdata['user_session']['reg_id'];
		$bus_id = $this->session->userdata['user_session']['bus_id'];

		$data['company_profile'] = $this->Profile_model->selectDataxls($id);
		$data['legal_documents'] = $this->Profile_model->selectData('legal_document', '*',array('bus_id'=>$id,'legal_type'=>'company'));
		$data['other_documents'] = $this->Profile_model->selectData('other_document', '*',array('bus_id'=>$id,'other_type'=>'company'));
		$data['gst_details'] = $this->Profile_model->selectData('gst_number','*', array('bus_id'=>$id,'status'=>'Active'));
		$data['branch_info'] = $this->Profile_model->selectData('company_branch', '*', array('bus_id'=>$id));
		$data['bank_details'] = $this->Profile_model->selectData('company_bank', '*', array('bus_id'=>$id));

		if(count($data['company_profile'])!=0){

			// Activity Tracker start
		
				 $tracking_info['module_name'] = 'My Company Profile';
				 $tracking_info['action_taken'] = 'Printed';
				 $tracking_info['reference'] = $data['company_profile'][0]->bus_company_name;
				 $LogRec = $this->Adminmaster_model->insertActivity($tracking_info);

			// Activity Tracker End

			//print_r($data);exit();
			$this->load->view('profile/print-company-profile',$data);
		}
		else{
			redirect('sales/company-profile');
		}
		
	}

	function download_multiple_profiles(){
		$array=$this->input->get('ids');
		$array=explode(',', $array);
		$data['result']=$this->Profile_model->downloadMultiple($array,$this->session->userdata['user_session']['reg_id']);
		//echo "<pre>"; print_r($data); exit();
		$this->load->view('profile/export_com_all',$data);
	}
	function temp_unset(){
		$this->session->sess_destroy();
	}
	public function	email_multiple_cmp_profiles() {
		$array=$this->input->post('profiles_values');
		$array=explode(',', $array);
		$reg_id=$this->user_session['reg_id'];
		$bus_id=$this->user_session['bus_id'];
		$data['result']=$this->Profile_model->downloadMultiple($array,$this->session->userdata['user_session']['reg_id']);
		$htmldata = $this->load->view('profile/print_company_profiles_all',$data,true);
		$invoname ="Company Profile";
		$msg ="Hey ".ucwords($this->user_session['ei_username']).", Congratulations! You have successfully create Company Profile.";
		$output=$this->pdf->create($htmldata,$invoname.'.pdf','email');
		sendInvoiceEmail($this->user_session['email'],'Your Company Profile has been created',$msg,$output,$invoname); 
	}
	public function print_multiple_profiles() {

		$array=$this->input->get('ids');
		$array=explode(',', $array);
		$reg_id = $this->user_session['reg_id'];
		$bus_id = $this->user_session['bus_id'];
		$data['result']=$this->Profile_model->downloadMultiple($array,$this->session->userdata['user_session']['reg_id']);
		$this->load->view('profile/print_company_profiles_all',$data);
	}
 
						/*-------------PERSONAL PROFILE----------------*/



	public function personal_profile()
	{
		is_accessAvail(5);
		$reg_id=$this->session->userdata['user_session']['reg_id'];
		$bus_id=$this->session->userdata['user_session']['bus_id'];
		$data['personal_profile'] = $this->Profile_model->selectData('registration', '*', array('bus_id'=>$bus_id));
		if(count($data['personal_profile'])>0){
			redirect('profile/manage-personal-profile');
		}else{

			// Activity Tracker start
		
				 $tracking_info['module_name'] = 'My Personal Profile';
				 $tracking_info['action_taken'] = 'Viewed';
				 $tracking_info['reference'] = 'Personal Profile';
				 $LogRec = $this->Adminmaster_model->insertActivity($tracking_info);

			// Activity Tracker End

			$this->load->view('profile/personal_profile_blank',$data);
		} 
		
	}

	public function add_personal_profile()
	{
		is_accessAvail(5);
         $reg_id=$this->session->userdata['user_session']['reg_id'];
		$bus_id=$this->session->userdata['user_session']['bus_id'];
		$data['access_list'] = $this->Profile_model->selectData('access_permissions', '*','','access','ASC');



		$profile_data=$this->Profile_model->selectData('registration', '*', array('bus_id'=>$bus_id));
		$emp_data=$this->Profile_model->selectData('employee_master', '*', array('bus_id'=>$bus_id,'portal_access'=>0,'status'=>'Active'));

			$business=$this->Profile_model->selectData('registration', '*', array('reg_id'=>$reg_id,'bus_id'=>$bus_id));
			
			$subscription_bus=$this->Profile_model->selectData('businesslist', '*', array('bus_id'=>$bus_id));
			$plan= $this->Profile_model->selectData('subscription', '*',array('bus_id'=>$bus_id ,'subscription_id'=>@$subscription_bus[0]->subscription_id));
			$totalProfile=count($profile_data)+count($emp_data);
    
	/*	if($business[0]->reg_istrial)
		{
			if($totalProfile>=25){
				$this->session->set_flashdata('error',"You can’t add more than 25 users in the trial period. Do subscribe to an appropriate pack to add more profiles");

						redirect('profile/personal_profile');	
			}//
		}else{   
			     if(count($plan)>0){
                 if($plan[0]->subscription_plan=="Trot" && $totalProfile>=250){
                 	$this->session->set_flashdata('error',"You can’t add more than 250 users. Do upgrade to Canter or Full Throttle pack to add more profiles");

						redirect('profile/personal_profile');	
                 }else if($plan[0]->subscription_plan=="Canter" && $totalProfile>=1000){
                    $this->session->set_flashdata('error',"You can’t add more than 1000 users. Do upgrade to Gallop pack to add more profiles");

						redirect('profile/personal_profile');
                 }else if($plan[0]->subscription_plan=="Walk" && $totalProfile>=25 ){
                 	 $this->session->set_flashdata('error',"You can’t add more than 25 users.");
                 }else{
                 	//return true;
                 }


             }else{
                  $this->session->set_flashdata('error',"Please subscribe appropriate pack.");

						redirect('subscription/my_subscription'); 
             }


			}*/


		$this->load->view('profile/add_personal_profile',$data);
	}
	public function manage_personal_profile()
	{
		$reg_id=$this->session->userdata['user_session']['reg_id'];
		$bus_id=$this->session->userdata['user_session']['bus_id'];
		$data['countries']	= $this->Profile_model->selectData('countries', '*','','country_id','ASC');
		if($bus_id){

			$data['personal_profile'] = $this->Profile_model->selectData('registration', '*', "(reg_createdby=".$reg_id." OR bus_id=".$bus_id.")");
			$data['inactivate_personal_profile'] = $this->Profile_model->selectData('registration', '*', "(status='Inactive' AND bus_id=".$bus_id.")");
		}else{

			$data['personal_profile'] = $this->Profile_model->selectData('registration', '*', "(reg_createdby=".$reg_id." OR reg_id=".$reg_id.")");
			$data['inactivate_personal_profile'] = $this->Profile_model->selectData('registration', '*', "(status='Inactive' AND  bus_id=".$bus_id.")");
		}
		
		// print_r($data['personal_profile']);
		// exit;
		$this->load->view('profile/personal_profile_list',$data);
		
	}
	
	public function insert_personal_profile()
	{
		$reg_id = $this->session->userdata['user_session']['reg_id'];
		$bus_id = $this->session->userdata['user_session']['bus_id'];

		$ip_address='';
		if (!empty($_SERVER['HTTP_CLIENT_IP']))   
		{
		 $ip_address = $_SERVER['HTTP_CLIENT_IP'];
		}
		elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))  
		{
		 $ip_address = $_SERVER['HTTP_X_FORWARDED_FOR'];
		}
		else
		{
		 $ip_address = $_SERVER['REMOTE_ADDR'];
		}

		$register_data = array();
		$_POST=$this->security->xss_clean($_POST);
		$register_data = $_POST;	
		$register_data['bus_id'] = $bus_id;
       $pass=$register_data['reg_password'];
		$register_data['reg_password']=md5($register_data['reg_password']);
		unset($register_data['contact_confirm_password']);	
		unset($register_data['image_info']);	
		unset($register_data['image_signature_info']);
		unset($register_data['confirmpassword']);	
		$register_data['reg_ipaddress']= $ip_address;
		$register_data['reg_istrial']= 0;	
		$register_data['reg_createdby']= $reg_id;	

		// if($register_data['reg_admin_type'] == 3)
		// {
		// 		$register_data['reg_admin_type'] = '-1';
		// }

		$exist_email = $this->Profile_model->isUnique('registration','reg_email',$register_data['reg_email']);
		$exist_email_client = $this->Profile_model->isUnique('sales_customer_contacts','cp_email',$register_data['reg_email']);
		$exist_email_vendor = $this->Profile_model->isUnique('expense_vendor_contacts','cp_email',$register_data['reg_email']);

		// Activity Tracker start
		
			 $tracking_info['module_name'] = 'My Personal Profile';
			 $tracking_info['action_taken'] = 'Added';
			 $tracking_info['reference'] = $register_data['reg_username'];
			 $LogRec = $this->Adminmaster_model->insertActivity($tracking_info);

		// Activity Tracker End
		
		if($exist_email==TRUE && $exist_email_client==TRUE && $exist_email_vendor==TRUE)
			{
				/*$array=array(
						'bus_company_name'=>'',
					);
				$get_company_id = $this->Profile_model->insertData('businesslist',$array);*/
				//unset($register_data['bus_id']);
				
				$get_admin_id = $this->Profile_model->insertData('registration',$register_data);
			
                 $name=explode(' ',$register_data['reg_username']);
				if($get_admin_id){
                  if($register_data['reg_admin_type']==1 || $register_data['reg_admin_type']==4 || $register_data['reg_admin_type']==5){
				  $emp_data=array(
                  	'reg_id'=>$register_data['reg_createdby'],
                  	'bus_id'=>$register_data['bus_id'],
                  	'status'=>'Inactive',
                  	'first_name'=>$name[0],
                  	'last_name'=>$name[1],
                  	'employee_email_id'=>$register_data['reg_email'],
                  	'employee_mobile_no'=>$register_data['reg_mobile'],
                  	'designation'=>$register_data['reg_designation'],
                  	'portal_access'=>1,



                  );
					
                  $get_employee_id = $this->Profile_model->insertData('employee_master',$emp_data);
					$image_info = array();
				$image_info = $_FILES;

				$employee_img =array();
				if($image_info['reg_profile_image']['name'] != '')
				{
					$employee_image=$this->Common_model->upload_org_filename('reg_profile_image','employee_images/'. $get_employee_id,$allowd="jpg|jpeg|png",array('width'=>200,'height'=>300));
		            if($employee_image!=false){
		               $employee_img['reg_profile_image']=$employee_image['file_name'];
		                 $get_update_emp_id = $this->Profile_model->updateData('employee_master',$employee_img,array('emp_id'=>$get_employee_id));

		            }
				}	

                  }

				}		

				$image_info = array();
				$image_info = $_FILES;

				$profile_img =array();
				if($image_info['reg_profile_image']['name'] != '')
				{
					$profile_image=$this->Common_model->upload_org_filename('reg_profile_image','personal_images/'.$get_admin_id,$allowd="jpg|jpeg|png",array('width'=>200,'height'=>300));
		            if($profile_image!=false){
		                $profile_img['reg_profile_image']=$profile_image['file_name'];
		                $get_update_id = $this->Profile_model->updateData('registration',$profile_img,array('reg_id'=>$get_admin_id));
		                $emp_img['emp_profile_img']=$profile_image['file_name'];
		                // $get_update_emp_id = $this->Profile_model->updateData('employee_master',$emp_img,array('emp_id'=>$get_admin_id));

		            }
				}	

				$signature_image = array();
				if($image_info['reg_degital_signature']['name'] != '')
				{
					$signature_image=$this->Common_model->upload_org_filename('reg_degital_signature','degital_signature/'.$get_admin_id,$allowd="jpg|jpeg|png",array('width'=>200,'height'=>300));
		            if($signature_image!=false){
		                $signature_img['reg_degital_signature']=$signature_image['file_name'];
		                $get_update_id = $this->Profile_model->updateData('registration',$signature_img,array('reg_id'=>$get_admin_id));
		            }
				}

				 $data['business'] = $this->Profile_model->selectData('businesslist','*',array('bus_id'=>$bus_id));
                   $data['username'] = $register_data['reg_email'];
				 $data['newpass'] = $pass;
				 $fromemail = $this->Sales_model->selectData('businesslist','*',array('bus_id'=>$bus_id,'reg_id'=>$reg_id,'status'=>'Active'));
                    //print_r($data['Cust_name']);exit;
                    $content=$this->load->view('employee_master/employee_portal_invite',$data,true);
                    $subject = "Portal Credentials";
                    $to =$data['username'];
                    $from = $fromemail[0]->bus_company_name;

                    $status = sendEmail($from,$to,$subject,$content);

					if (count($get_admin_id)>0){		
								redirect('profile/manage_personal_profile');	
					}
					else
					{
						$this->session->set_flashdata('error',"A profile with this email id already exists. Please use another email id");
						redirect('profile/add_personal_profile');	
					}
			}
			else
			{
				//Seems like this email address is already registered
				$this->session->set_flashdata('error',"A profile with this email id already exists. Please use another email id");
				redirect('profile/add_personal_profile');	
			}
	}

	public function get_personal_profiles()
	{
		// Activity Tracker start
		
				 $tracking_info['module_name'] = 'My Personal Profile';
				 $tracking_info['action_taken'] = 'Viewed';
				 $tracking_info['reference'] = 'Personal Profile List';
				 $LogRec = $this->Adminmaster_model->insertActivity($tracking_info);

			// Activity Tracker End

		$post = $this->input->post();
		$post=$this->security->xss_clean($post);
		$post['reg_id']=$this->session->userdata['user_session']['reg_id'];
		$post['bus_id']=$this->session->userdata['user_session']['bus_id'];
		$field_pos=array("reg_profile_image"=>'0',"reg_username"=>'1',"reg_email"=>'2',"reg_mobile"=>'3',"reg_admin_type"=>'4');	

		$sort_field=array_search($post['order'][0]['column'],$field_pos);

		if($post['order'][0]['dir']=='asc')
		{
	 		$orderBy="ASC";
		}
	 	else
		{
	 		$orderBy="DESC";
		}	

		$TotalRecord=$this->Profile_model->fetchUserFilter1($post,$sort_field,$orderBy,0);	
	 	$userData = $this->Profile_model->fetchUserFilter1($post,$sort_field,$orderBy,1);
	 	
	 	$iTotalRecords = $TotalRecord['NumRecords'];

	 	$records = array();
	  	$records["data"] = array();
	  	$csrf_token=$this->security->get_csrf_hash();

	  	$records['csrf_hash']=$csrf_token;
		
	  	foreach ($userData as $key => $value) {

			$check='<input type="checkbox" id="filled-in-box'.$value['reg_id'].'" name="filled-in-box'.$value['reg_id'].'" class="filled-in purple personal_profile_bulk_action" value="'.$value['reg_id'].'"/> <label for="filled-in-box'.$value['reg_id'].'"></label>';	
	  		
	  		$reg_profile_image='';
	  		if($value['reg_profile_image'] != ''){
			$reg_profile_image='<img style="width:50px" src="'.base_url().'public/upload/personal_images/'.$value['reg_id'].'/'.$value['reg_profile_image'].'">';
			}	
	  		$reg_username='';
	  		if($value['reg_username'] != ''){
			$reg_username='<a href="'.base_url().'profile/view_personal_profile/'.$value['reg_id'].'" >'.$value['reg_username'].'</a>';
			}		
			$reg_email = $value['reg_email'];

			$reg_mobile = $value['reg_mobile'];	
			$reg_admin_type='';
	  		if($value['access']!=''){
				$reg_admin_type = $value['access'];	
			}else{
				$reg_admin_type = 'Admin';	
			}
			$action = '';$status='';$cls='';$cls1='';
			if($value['status']!="Active"){
				$cls='deactive_record';
				$cls1='deactivate';

				$action  = "<div class='dropdown-button action-more border-radius-3 btn-default default-width no-background ".$cls."' href='#' data-activates='dropdown".$value["reg_id"]."'><i class='action-dot'></i></div>
			 	<ul id='dropdown".$value["reg_id"]."' class='dropdown-content'>";
				$status='<li class="activation"><a href="#" class="active_personal_profile" data-profileid='.$value["reg_id"].'><i class="dropdwon-icon icon activate"></i>Re-Activate</a></li>';
				
			}else{

				$cls='active_record';
				$cls1='activate';
				$action  = "<div class='dropdown-button action-more border-radius-3 btn-default default-width no-background ".$cls."' href='#' data-activates='dropdown".$value["reg_id"]."'><i class='action-dot'></i></div>
			 	<ul id='dropdown".$value["reg_id"]."' class='dropdown-content'>
				<li><a href=".base_url()."profile/edit_personal_profile/".$value["reg_id"].'><i class="material-icons" style="color: #000;">mode_edit</i>Edit</a></li>
				<li><a href='.base_url().'profile/export_personal_profile/'.$value["reg_id"].'><img class="icon-img" src="'.base_url().'public/icons/export.png" style="width: 21px;height: 20px;"><p class="ex">Export</p></a></li>
				<li><a href='.base_url().'profile/print-personal-profile?ids='.$value["reg_id"].' target="_blank"><i class="material-icons dp48">print</i>Print</a></li>';
				$status='<li class="deactivation"><a href="#" class="deactive_personal_profile" data-profileid="'.$value["reg_id"].'"><i class="dropdwon-icon icon deactivate"></i>Deactivate</a></li>';
				
			}
			
				
			
			$action  .= $status;
			
			  $action  .= '</ul>

			  <div class="status-action '.$cls1.'">

				<i class="action-status-icon"></i>

			  </div>';
							
		
	  		

	  		$records["data"][] = array(
				$check,
				$reg_profile_image,
	  			$reg_username,
	  			$reg_email,
	  			$reg_mobile,
				$reg_admin_type,
	  			$action,
	  			);
	  	}




	  	$records["draw"] = intval($post['draw']);
		$records["recordsTotal"] = $iTotalRecords;
		$records["recordsFiltered"] = $iTotalRecords;		
		echo json_encode($records);				  
		exit;
	  	

	}

	public function view_personal_profile($id)
	{	
		$reg_id = $this->user_session['reg_id'];
		$bus_id = $this->user_session['bus_id'];
		$profile_data['company_name']= $this->Profile_model->selectData('businesslist', '*', array('bus_id'=>$bus_id));
		$profile_data['profile_data']=$this->Profile_model->selectData('registration', '*', array('reg_id'=>$id));
		$profile_data['access_data']=$this->Profile_model->selectData('access_permissions', '*',  array('id'=>$profile_data['profile_data'][0]->reg_admin_type));

		// Activity Tracker start
		
			 $tracking_info['module_name'] = 'My Personal Profile';
			 $tracking_info['action_taken'] = 'Viewed';
			 $tracking_info['reference'] = $profile_data['profile_data'][0]->reg_username;
			 $LogRec = $this->Adminmaster_model->insertActivity($tracking_info);

		// Activity Tracker End

		/*echo "<pre>";
		print_r($profile_data['access_data']);
		exit();*/
		$this->load->view('profile/view_personal_profile',$profile_data);
	}

	public function edit_personal_profile($id)
	{
		$profile_data['profile_data']=$this->Profile_model->selectData('registration', '*', array('reg_id'=>$id));
		$profile_data['access_list'] = $this->Profile_model->selectData('access_permissions', '*');		
		//print_r($profile_data['profile_data']);exit;
		$this->load->view('profile/edit_personal_profile',$profile_data);
	}

	public function update_personal_profile($id)
	{
		$profile_data['profile_data']=$this->Profile_model->selectData('registration', '*', array('reg_id'=>$id));

		if($profile_data['profile_data'][0]->reg_profile_image == NULL){
			$profile_data['profile_data'][0]->reg_profile_image='';
		}

		if($profile_data['profile_data'][0]->reg_degital_signature == NULL){
			$profile_data['profile_data'][0]->reg_degital_signature='';
		}

		$update_data = array();
		$_POST=$this->security->xss_clean($_POST);
		$update_data = $_POST;
		 

		if($profile_data['profile_data'][0]->reg_profile_image != $_FILES['reg_profile_image']['name'])
		{
			$image_info1 = array();
			$image_info1 = $_FILES;
			
			if($image_info1['reg_profile_image']['name'] != '')
			{
				$profile_image=$this->Common_model->upload_org_filename('reg_profile_image','personal_images/'.$id,$allowd="jpg|jpeg|png",array('width'=>200,'height'=>300));
	            if($profile_image!=false){
	                $update_data['reg_profile_image']=$profile_image['file_name'];

	                if($this->session->userdata['user_session']['reg_id'] == $id)
	                {
	                	$this->session->userdata['user_session']['user_photo'] = $update_data['reg_profile_image'];
	                }
	            }
			}	
		}
		else{
			unset($update_data['reg_profile_image']);	
		}

		if($profile_data['profile_data'][0]->reg_degital_signature != $_FILES['reg_degital_signature']['name'])
		{
			$image_info2 = array();
			$image_info2 = $_FILES;
			
			if($image_info2['reg_degital_signature']['name'] != '')
			{
				$signature_image=$this->Common_model->upload_org_filename('reg_degital_signature','degital_signature/'.$id,$allowd="jpg|jpeg|png",array('width'=>200,'height'=>300));
	            if($signature_image!=false){
	                $update_data['reg_degital_signature']=$signature_image['file_name'];

	                if($this->session->userdata['user_session']['reg_id'] == $id)
	                {
	                	$this->session->userdata['user_session']['user_sign'] = $update_data['reg_degital_signature'];
	                }
	            }
			}	
		}
		else{
			unset($update_data['reg_degital_signature']);	
		}

		unset($update_data['image_info']);
		unset($update_data['img_folder']);

		unset($update_data['client-name']);

		unset($update_data['image_signature_info']);
		unset($update_data['sign_img_folder']);


		// if($update_data['reg_admin_type'] == 3)
		// {
		// 		$update_data['reg_admin_type'] = '-1';
		// }
		if($profile_data['profile_data'][0]->reg_email != $update_data['reg_email']){

				$exist_email = $this->Profile_model->isUnique('registration','reg_email',$update_data['reg_email']);
				
		
		}else {
			$exist_email=TRUE;
			
		}	
		if($exist_email==TRUE)
		{
			$get_update_id = $this->Profile_model->updateData('registration',$update_data,array('reg_id'=>$id));

			// Activity Tracker start
		
				 $tracking_info['module_name'] = 'My Personal Profile';
				 $tracking_info['action_taken'] = 'Edited';
				 $tracking_info['reference'] = $update_data['reg_username'];
				 $LogRec = $this->Adminmaster_model->insertActivity($tracking_info);

			// Activity Tracker End

			if ($get_update_id==1){	
				if($profile_data['profile_data'][0]->reg_email != $update_data['reg_email']){
					if($exist_email==TRUE){
						$fromemail = $this->Sales_model->selectData('businesslist','*',array('bus_id'=>$bus_id,'reg_id'=>$reg_id,'status'=>'Active'));
						$data['name']=$profile_data['profile_data'][0]->reg_username;
						$to = $profile_data['profile_data'][0]->reg_email;
						$from = $fromemail[0]->bus_company_name;
						$subject = 'Your email address has been updated!';
						$message=$this->load->view('email_template/change-email',$data,true);
						sendEmail($from,$to,$subject,$message);	
					}
				}
				if($profile_data['profile_data'][0]->reg_mobile != $update_data['reg_mobile']){
					$fromemail = $this->Sales_model->selectData('businesslist','*',array('bus_id'=>$bus_id,'reg_id'=>$reg_id,'status'=>'Active'));
						$data['name']=$profile_data['profile_data'][0]->reg_username;
						$to = $profile_data['profile_data'][0]->reg_email;
						$from = $fromemail[0]->bus_company_name;
						$subject = 'Your mobile number has been updated!';
						$message=$this->load->view('email_template/change-mobile',$data,true);
						sendEmail($from,$to,$subject,$message);	
					
				}
				$this->session->set_flashdata('success',"Profile has been updated successfully");	
				redirect('profile/personal-profile');	
			}
			else{
				$this->session->set_flashdata('error',"Error while processing!");
				redirect('profile/personal-profile');	
			}
		}else{
			    $this->session->set_flashdata('error',"A profile with this email id already exists. Please use another email id");
				redirect('profile/personal-profile');	
				//echo "no2";
			}	


	}
	public function deactive_personal_profile($id)
	{	
       $bus_id = $this->session->userdata['user_session']['bus_id'];

        $profile=$this->Profile_model->selectData('registration', '*', array('bus_id'=>$bus_id,'reg_id'=>$id));


        if($profile[0]->reg_admin_type==3){

       $profileInactive=$this->Profile_model->selectData('registration', '*', array('bus_id'=>$bus_id,'reg_admin_type'=>3,'status'=>'Inactive'));

    $profileAct=$this->Profile_model->selectData('registration', '*',array('bus_id'=>$bus_id,'reg_admin_type'=>3));

    

   $proActive=count($profileAct)-1;
     $proInactive=count($profileInactive);
    
    if( $proActive == $proInactive){
         //$this->session->set_flashdata('error',"There should be at least 1 active admin profile");
       echo 'lastadmin';exit;
    }else{
    	 $deactive = array(
					'status'	=> "Inactive"
					);
		$get_deactive_id = $this->Profile_model->updateData('registration',$deactive,array('reg_id'=>$id));

		if ($get_deactive_id==1){	

				// Activity Tracker start
					$rec_info = $this->Profile_model->selectData('registration', '*',array('reg_id'=>$id));
					 $tracking_info['module_name'] = 'My Personal Profile';
					 $tracking_info['action_taken'] = 'Deactivated';
					 $tracking_info['reference'] = $rec_info[0]->reg_username;
					 $LogRec = $this->Adminmaster_model->insertActivity($tracking_info);

				// Activity Tracker End
				echo true;
		}else{
				echo false;
		}
    }

}else{


	 $deactive = array(
					'status'	=> "Inactive"
					);
		$get_deactive_id = $this->Profile_model->updateData('registration',$deactive,array('reg_id'=>$id));

		if ($get_deactive_id==1){	

				// Activity Tracker start
					$rec_info = $this->Profile_model->selectData('registration', '*',array('reg_id'=>$id));
					 $tracking_info['module_name'] = 'My Personal Profile';
					 $tracking_info['action_taken'] = 'Deactivated';
					 $tracking_info['reference'] = $rec_info[0]->reg_username;
					 $LogRec = $this->Adminmaster_model->insertActivity($tracking_info);

				// Activity Tracker End
				echo true;
		}else{
				echo false;
		}
}

		
	}
	public function activate_personal_profile($id)
	{	
		$active = array(
							'status'	=> "Active"
							);
		$get_active_id = $this->Profile_model->updateData('registration',$active,array('reg_id'=>$id));
		if ($get_active_id==1){	

				// Activity Tracker start
					$rec_info = $this->Profile_model->selectData('registration', '*',array('reg_id'=>$id));
					 $tracking_info['module_name'] = 'My Personal Profile';
					 $tracking_info['action_taken'] = 'Activated';
					 $tracking_info['reference'] = $rec_info[0]->reg_username;
					 $LogRec = $this->Adminmaster_model->insertActivity($tracking_info);

				// Activity Tracker End

				echo true;
		}else{
				echo false;
		}
	}

	function deactive_multiple_profile()
	{
		$profiles = $this->input->post('profiles_values');
		$status = $this->input->post('profiles_status');	
		$profile_id_array = explode(",",$profiles);
		foreach($profile_id_array as $id)
		{    
			if($status=="reactivate"){
				$deactive = array(
						'status'	=> "Active"
						);
			}else{
				$deactive = array(
						'status'	=> "Inactive"
						);
			}
			

			// Activity Tracker start
				$rec_info = $this->Profile_model->selectData('registration', '*',array('reg_id'=>$id));
				 $tracking_info['module_name'] = 'My Personal Profile';
				 $tracking_info['action_taken'] = 'Deactivated';
				 $tracking_info['reference'] = $rec_info[0]->reg_username;
				 $LogRec = $this->Adminmaster_model->insertActivity($tracking_info);

			// Activity Tracker End

			$get_deactive_id = $this->Profile_model->updateData('registration',$deactive,array('reg_id'=>$id));
			//print $this->db->last_query();
		}
		if ($get_deactive_id==1){	
			echo true;
		}else{
			echo false;
		}
		
	}

	function print_personal_profile(){
		
		$array=$this->input->get('ids');
		$array=explode(',', $array);
		$user_id = $this->session->userdata['user_session']['reg_id'];
		$bus_id = $this->session->userdata['user_session']['bus_id'];
		foreach($array as $id)
		{
			$data['company_profile'] = $this->Profile_model->selectData('businesslist', '*',array('bus_id'=>$bus_id));
			$data['register_data'] = $this->Profile_model->selectData('registration', '*',array('reg_id'=>$id));
			$data['access_data']=$this->Profile_model->selectData('access_permissions', '*',  array('id'=>$data['register_data'][0]->reg_admin_type));		
			// Activity Tracker start
			 $tracking_info['module_name'] = 'My Personal Profile';
			 $tracking_info['action_taken'] = 'Printed';
			 $tracking_info['reference'] = $data['register_data'][0]->reg_username;
			 $LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
			// Activity Tracker End

			//print_r($data[register_data]);
			//exit();
			$this->load->view('profile/print-personal-profile',$data);
		}
		
		
		/*$user_id = $this->session->userdata['user_session']['reg_id'];
		$bus_id = $this->session->userdata['user_session']['bus_id'];

		$data['register_data'] = $this->Profile_model->selectData('registration', '*',array('reg_id'=>$id));
		$data['company_profile'] = $this->Profile_model->selectData('businesslist', '*',array('bus_id'=>$bus_id));
		$data['access_data']=$this->Profile_model->selectData('access_permissions', '*',  array('id'=>$data['register_data'][0]->reg_admin_type));


		// Activity Tracker start
			 $tracking_info['module_name'] = 'My Personal Profile';
			 $tracking_info['action_taken'] = 'Printed';
			 $tracking_info['reference'] = $data['register_data'][0]->reg_username;
			 $LogRec = $this->Adminmaster_model->insertActivity($tracking_info);

		// Activity Tracker End

		$this->load->view('profile/print-personal-profile',$data);*/
	}

	function download_multi_personal_profiles(){

		$array=$this->input->get('ids');
		$array=explode(',', $array);

		foreach($array as $id)
		{
			// Activity Tracker start
				$rec_info = $this->Profile_model->selectData('registration', '*',array('reg_id'=>$id));
				 $tracking_info['module_name'] = 'My Personal Profile';
				 $tracking_info['action_taken'] = 'Downloaded';
				 $tracking_info['reference'] = $rec_info[0]->reg_username;
				 $LogRec = $this->Adminmaster_model->insertActivity($tracking_info);

			// Activity Tracker End
		}

		/*print_r($array);
		exit();*/
		$data['result']=$this->Profile_model->downloadMultiPerosnalprofile($array,$this->session->userdata['user_session']['reg_id']);
		//echo "<pre>"; print_r($data); exit();
		$this->load->view('profile/export_per_all',$data);
	}

	public function view_company_profile($id)
	{
		
		
		$user_id = $this->session->userdata['user_session']['reg_id'];
		$data['company_profile'] = $this->Profile_model->selectData('businesslist', '*', array('bus_id'=>$id));
		
		//$this->data['services'] = $this->Profile_model->get_by_condition('EI_services',array('company_id'=>$org_id));
		$data['countries']	=	$this->Profile_model->selectData('countries', '*','','country_id','ASC');
		$data['currency']	=	$this->Profile_model->selectData('currency', '*','','currency_id','ASC');
		$data['company_type'] = $this->Profile_model->selectData('company_type', '*','','id','ASC');
		$data['legal_documents'] = $this->Profile_model->selectData('legal_document', '*',array('bus_id'=>$id,'legal_type'=>'company'));
		$data['other_documents'] = $this->Profile_model->selectData('other_document', '*',array('bus_id'=>$id,'other_type'=>'company'));
		$data['gst_details'] = $this->Profile_model->selectData('gst_number','*', array('bus_id'=>$id,'status'=>'Active'));
		$data['branch_info'] = $this->Profile_model->selectData('company_branch', '*', array('bus_id'=>$id));
		$data['bank_details'] = $this->Profile_model->selectData('company_bank', '*', array('bus_id'=>$id));


		// Activity Tracker start
				
			 $tracking_info['module_name'] = 'My Company Profile';
			 $tracking_info['action_taken'] = 'Viewed';
			 $tracking_info['reference'] = $data['company_profile'][0]->bus_company_name;
			 $LogRec = $this->Adminmaster_model->insertActivity($tracking_info);

		// Activity Tracker End
		//print_r($data);exit();
		$this->load->view('profile/view_company_profile',$data);
	}

	public function active_deactive_company()
	{
		$id = $this->input->post('profile_id');
		
		$filter = array(
			'status'	=>	$this->input->post('status'),
		);
		$flag=$this->Profile_model->updateData('businesslist',$filter,array('bus_id'=>$id));

		if($flag != false)
		{
			$rec_info = $this->Profile_model->selectData('businesslist', '*',array('bus_id'=>$id));

			if($this->input->post('status') == 'Active')
			{
				// Activity Tracker start
					
					 $tracking_info['module_name'] = 'My Company Profile';
					 $tracking_info['action_taken'] = 'Activated';
					 $tracking_info['reference'] = $rec_info[0]->bus_company_name;
					 $LogRec = $this->Adminmaster_model->insertActivity($tracking_info);

				// Activity Tracker End
			}else{
				// Activity Tracker start
		
					 $tracking_info['module_name'] = 'My Company Profile';
					 $tracking_info['action_taken'] = 'Deactivated';
					 $tracking_info['reference'] = $rec_info[0]->bus_company_name;
					 $LogRec = $this->Adminmaster_model->insertActivity($tracking_info);

				// Activity Tracker End
			}

			echo true;
		}
		else
		{
			echo false;
		}
	}

	function deactive_multiple()
	{
		$profiles = $this->input->post('profiles_values');	
		$profile_id_array = explode(",",$profiles);
		foreach($profile_id_array as $id)
		{
			$filter = array(
				'status'	=>	'Inactive',
			);
			$flag=$this->Profile_model->updateData('businesslist',$filter,array('bus_id'=>$id));	
		}
		$this->session->set_flashdata('success',"Company Profiles has been successfully deactivated");
		echo true;
	}

	function searchCity()
	{
	    $test='';
		if(!empty($_POST["keyword"])) {
		$cities	=	$this->Profile_model->searchData($_POST["keyword"]);
		
		if(!empty($cities)) {
			 ?>
			<ul id="state-list" class="select-wrapper tabl-select border-radius-6 btn-dropdown-select select-like-dropdown create-bil-suyp sales-in-drop ul-autocomplete initialized ">
			<?php
			foreach ($cities as $value) {
			?>
			<li onClick="selectCountry('<?php echo $value->state_name; ?>','<?php echo $_POST["type_id"]; ?>');" class="li-autocomplete"><?php echo $value->state_name; ?></li>
			<?php } ?>
			</ul>
		<?php } }
		
		

	}
	function get_state_from_code(){
	 $state_code = $this->input->post('state_code');	
	 $states = $this->Profile_model->selectData('states', '*', array('state_gst_code'=>$state_code,'country_id'=>101));
	 echo json_encode($states);
	}

	public function export_personal_profile($id){
		$data['exp_data']=$this->Profile_model->selectData('registration','*',array('reg_id'=>$id));
		$data['access_data']=$this->Profile_model->selectData('access_permissions', '*',  array('id'=>$data['exp_data'][0]->reg_admin_type));
		
		 $tracking_info['module_name'] = 'My Personal Profile';
		 $tracking_info['action_taken'] = 'Exported';
		 $tracking_info['reference'] = $data['exp_data'][0]->reg_username;
		 $LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
		$this->load->view('profile/export_per',$data);
	}

	public function sent_mail(){
		$reg_id = $this->user_session['reg_id'];
        $bus_id = $this->user_session['bus_id'];
        $gst_id = $this->user_session['ei_gst'];
		$fromemail = $this->Sales_model->selectData('businesslist','*',array('bus_id'=>$bus_id,'reg_id'=>$reg_id,'status'=>'Active'));
		$to =  'gopal@hmmbiz.com';
		//$to = 'programmer5.techybirds@gmail.com';
		$from = $fromemail[0]->bus_company_name;
		$subject = 'Test Mail';
		$message = 'Testing Email';
		sendEmail($from,$to,$subject,$message);	
	}

	public function download_single_xls($id){
		$user_id = $this->session->userdata['user_session']['reg_id'];
		$data['company_profile'] = $this->Profile_model->selectDataxls($id);
		$data['legal_documents'] = $this->Profile_model->selectData('legal_document', '*',array('bus_id'=>$id,'legal_type'=>'company'));
		$data['other_documents'] = $this->Profile_model->selectData('other_document', '*',array('bus_id'=>$id,'other_type'=>'company'));
		$data['gst_details'] = $this->Profile_model->selectData('gst_number','*', array('bus_id'=>$id,'status'=>'Active'));
		$data['branch_info'] = $this->Profile_model->selectData('company_branch', '*', array('bus_id'=>$id));
		$data['bank_details'] = $this->Profile_model->selectData('company_bank', '*', array('bus_id'=>$id));

		// Activity Tracker start
		
			 $tracking_info['module_name'] = 'My Company Profile';
			 $tracking_info['action_taken'] = 'Downloaded';
			 $tracking_info['reference'] = $data['company_profile'][0]->bus_company_name;
			 $LogRec = $this->Adminmaster_model->insertActivity($tracking_info);

		// Activity Tracker End
		$this->load->view('profile/export_com',$data);
	}	
	
	public function insert_instamojo_token()
	{
		$bus_id = $this->session->userdata['user_session']['bus_id'];
		$_POST=$this->security->xss_clean($_POST);
		parse_str($_POST['frm'],$post);
		$api_key = $post['api_key'];
		$auth_token = $post['auth_token'];

		if(!empty($api_key) and !empty($auth_token) and !empty($bus_id))
		{
			$filter = array(
				'api_key'	=>	$api_key,
				'auth_token' => $auth_token,
			);
			$flag =$this->Profile_model->updateData('businesslist',$filter,array('bus_id'=>$bus_id));

			if($flag)
			{
				 $rec_info = $this->Profile_model->selectData('businesslist', '*',array('bus_id'=>$bus_id));
				 $tracking_info['module_name'] = 'My Company Profile';
				 $tracking_info['action_taken'] = 'Integrated Instamojo';
				 $tracking_info['reference'] = $rec_info[0]->bus_company_name;
				 $LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
				echo true;
				exit;
				//$this->session->set_flashdata('success',"Your Instamojo Integrated Successfully");
			}else{
				echo false;
				exit;
				//$this->session->set_flashdata('success',"Your Instamojo Integrated Failed");
			}

		}else{
			echo false;
			exit;
			//$this->session->set_flashdata('success',"Your Instamojo Integrated Failed");
		}

		redirect('profile/manage-company-profile');
	}

	public function edit_instamojo()
	{
		$bus_id = $this->session->userdata['user_session']['bus_id'];
		$_POST=$this->security->xss_clean($_POST);
		parse_str($_POST['frm'],$post);
		$api_key = $post['api_key'];
		$auth_token = $post['auth_token'];

		if(!empty($api_key) and !empty($auth_token) and !empty($bus_id))
		{
			$filter = array(
				'api_key'	=>	$api_key,
				'auth_token' => $auth_token,
			);
			$flag =$this->Profile_model->updateData('businesslist',$filter,array('bus_id'=>$bus_id));

			if($flag)
			{
				 $rec_info = $this->Profile_model->selectData('businesslist', '*',array('bus_id'=>$bus_id));
				 $tracking_info['module_name'] = 'My Company Profile';
				 $tracking_info['action_taken'] = 'Integrated Instamojo';
				 $tracking_info['reference'] = $rec_info[0]->bus_company_name;
				 $LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
				echo true;
				exit;
			}else{
				echo false;
			}

		}else{

			echo false;
		}

	}

	
}





