<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require ("common/Index_Controller.php");
class Notifications extends Index_Controller 
{
	function __construct(){
		//is_trialExpire();
		parent::__construct();
		$this->load->model('Sales_model');
		$this->load->model('User_model');

	}
	
	public function index()
	{   
		//$businesses=$this->Sales_model->selectData('businesslist', '*', array('status'=>'active'));
		//foreach($businesses as $bkey => $bvalue) {
			$customers=$this->Sales_model->selectData('sales_customers','*', array('status'=>'active'));//'reg_id'=>$bvalue->reg_id,'bus_id'=>$bvalue->bus_id,
			foreach($customers as $key => $value) {
				$cust_id=$value->cust_id;
				$cust_name=ucfirst($value->cust_name);
				$bus_id=$value->bus_id;
				$reg_id=$value->reg_id;
				$gst_id=0;
				/*--- Start Contact Persons Birthday/Anniversary Reminder---*/
				$contact_persons = $this->Sales_model->selectData('sales_customer_contacts','*',array('cust_id'=>$cust_id,'type' => 'customer'));
			//	$contact_persons = $this->Sales_model->selectalert($cust_id,$bus_id);
			
				if(count($contact_persons)>0){
					foreach($contact_persons as $ckey => $cvalue) {	
						$cp_name=ucfirst($cvalue->cp_name);
						$cp_id=$cvalue->cp_id;
						$cp_email=$cvalue->cp_email;
						if($cvalue->cp_birth_date!='0000-00-00'){
							$dateTime = date("d-m-Y",  strtotime($cvalue->cp_birth_date));
							$birth_date = str_replace("-","/",$dateTime);
							$noti_message='The birthday of '.$cp_name.' is coming up on '.$birth_date;
							$notification=array(
								'bus_id'=>$bus_id,
								'reg_id'=>$reg_id,
								'gst_id'=>$gst_id,
								'cust_id'=>$cust_id,
								'ref_id'=>$cp_id,
								'noti_type'=>'cust_birthday',
								'noti_message'=>$noti_message,
								'noti_url'=>'sales/view-client/'.$cust_id,
							);
							if($cvalue->cp_birth_reminder==1){
								if($cvalue->cp_birth_date==date('Y-m-d')){
								$this->birthday_reminder($cust_name,$cp_name,$birth_date,$cp_email);
								
								$send=$this->Sales_model->insertData("notifications",$notification);
								}
							}else if($cvalue->cp_birth_reminder==3){
								if($cvalue->cp_birth_date==date('Y-m-d',strtotime('+3 days'))){	
								$this->birthday_reminder($cust_name,$cp_name,$birth_date,$cp_email);
								
								$send=$this->Sales_model->insertData("notifications",$notification);
								}
							}else if($cvalue->cp_birth_reminder==7){
								if($cvalue->cp_birth_date==date('Y-m-d',strtotime('+7 days'))){
								$this->birthday_reminder($cust_name,$cp_name,$birth_date,$cp_email);
								
								$send=$this->Sales_model->insertData("notifications",$notification);
								}
							}
						}	
						if($cvalue->cp_anniversary_date!='0000-00-00'){
							$dateTime = date("d-m-Y",  strtotime($cvalue->cp_anniversary_date));
							$anniversary_date = str_replace("-","/",$dateTime);
						    $anoti_message='The anniversary  of '.$cp_name.' is coming up on '.$anniversary_date;
							$anni_noti=array(
								'bus_id'=>$bus_id,
								'reg_id'=>$reg_id,
								'gst_id'=>$gst_id,
								'cust_id'=>$cust_id,
								'ref_id'=>$cp_id,
								'noti_type'=>'cust_aniversary',
								'noti_message'=>$anoti_message,
								'noti_url'=>'sales/view-client/'.$cust_id,
							);
							if($cvalue->cp_anniversary_reminder==1){
								if($cvalue->cp_anniversary_date==date('Y-m-d')){
								$this->anniversary_reminder($cust_name,$cp_name,$anniversary_date,$cp_email);
								
								$send=$this->Sales_model->insertData("notifications",$anni_noti);
								}
							}else if($cvalue->cp_anniversary_reminder==3){
								if($cvalue->cp_anniversary_date==date('Y-m-d',strtotime('+3 days'))){	
								$this->anniversary_reminder($cust_name,$cp_name,$anniversary_date,$cp_email);
								
								$send=$this->Sales_model->insertData("notifications",$anni_noti);
								
								}
							}else if($cvalue->cp_anniversary_reminder==7){
								if($cvalue->cp_anniversary_date==date('Y-m-d',strtotime('+7 days'))){
								$this->anniversary_reminder($cust_name,$cp_name,$anniversary_date,$cp_email);

								$send=$this->Sales_model->insertData("notifications",$anni_noti);
								
								}
							}
						}
					}
				}

				/*--- Start Document Reminder ---*/
				$legal_documents = $this->Sales_model->selectData('legal_document','*',array("bus_id"=>$cust_id,"legal_type"=>'customer'));
				$other_documents = $this->Sales_model->selectData('other_document','*',array("bus_id"=>$cust_id,"other_type"=>'customer'));
				$contact_data=$this->Sales_model->selectData('sales_customer_contacts','cust_id,cp_email,cp_mobile',array("cust_id"=>$cust_id,'cp_is_admin'=>'1','type'=>'customer'));
				if(count($contact_data)>0){
					$cp_email=$contact_data[0]->cp_email;
					$cp_mobile=$contact_data[0]->cp_mobile;

					foreach($legal_documents as $lkey => $lvalue) {
					   
						if($lvalue->legal_end_date!='0000-00-00'){
						    
							$dateTime = date("d-m-Y",  strtotime($lvalue->legal_end_date));
							$doc_date = str_replace("-","/",$dateTime);
							$legal_doc	= $lvalue->legal_document;
							$legal_id	= $lvalue->legal_id;
							$noti_message='The renewal period for '.$legal_doc.' is coming up on '.$doc_date;
							$notification=array(
								'bus_id'=>$bus_id,
								'reg_id'=>$reg_id,
								'gst_id'=>$gst_id,
								'cust_id'=>$cust_id,
								'ref_id'=>$legal_id,
								'noti_type'=>'legal_document',
								'noti_message'=>$noti_message,
								'noti_url'=>'sales/view-client/'.$cust_id,
							);	
							
							if($lvalue->legal_reminder=='1') {
								if(date('Y-m-d',strtotime($lvalue->legal_end_date.'-7 days'))==date('Y-m-d')){
									$this->legal_reminder($cust_name,$legal_doc,$doc_date,$cp_email);
									$send=$this->Sales_model->insertData("notifications",$notification);
									send_sms($cp_mobile,$noti_message);
									// SMS Tracker start
									$tracking_info['bus_id'] = $bus_id;
									$tracking_info['module_name'] = 'Alert Reminder';
									$tracking_info['action_taken'] = "legal reminder message sent";
									$tracking_info['reference'] = 'legal document reminder';
									$LogRec = $this->Adminmaster_model->insertSmsActivity($tracking_info);
							 		// SMS Tracker End	
								}
							} else if($lvalue->legal_reminder=='2') {
								if(date('Y-m-d',strtotime($lvalue->legal_end_date.'-14 days'))==date('Y-m-d')){
									$this->legal_reminder($cust_name,$legal_doc,$doc_date,$cp_email);
									$send=$this->Sales_model->insertData("notifications",$notification);
									send_sms($cp_mobile,$noti_message);
									// SMS Tracker start
									$tracking_info['bus_id'] = $bus_id;
									$tracking_info['module_name'] = 'Alert Reminder';
									$tracking_info['action_taken'] = "legal reminder message sent";
									$tracking_info['reference'] = 'legal document reminder';
									$LogRec = $this->Adminmaster_model->insertSmsActivity($tracking_info);
								 	// SMS Tracker End	
								}
							}
							else if($lvalue->legal_reminder=='3'){
							if(date('Y-m-d',strtotime($lvalue->legal_end_date.'-1 month'))==date('Y-m-d')){
								$this->legal_reminder($cust_name,$legal_doc,$doc_date,$cp_email);
								$send=$this->Sales_model->insertData("notifications",$notification);
								send_sms($cp_mobile,$noti_message);
											// SMS Tracker start
			$tracking_info['bus_id'] = $bus_id;
			 $tracking_info['module_name'] = 'Alert Reminder';
			 $tracking_info['action_taken'] = "legal reminder message sent";
			 $tracking_info['reference'] = 'legal document reminder';
			 $LogRec = $this->Adminmaster_model->insertSmsActivity($tracking_info);

		 	// SMS Tracker End	
								}
							}
							else if($lvalue->legal_reminder=='4'){
								if(date('Y-m-d',strtotime($lvalue->legal_end_date.'-2 month'))==date('Y-m-d')){
								$this->legal_reminder($cust_name,$legal_doc,$doc_date,$cp_email);
								$send=$this->Sales_model->insertData("notifications",$notification);
								send_sms($cp_mobile,$noti_message);

									// SMS Tracker start
			$tracking_info['bus_id'] = $bus_id;
			 $tracking_info['module_name'] = 'Alert Reminder';
			 $tracking_info['action_taken'] = "legal reminder message sent";
			 $tracking_info['reference'] = 'legal document reminder';
			 $LogRec = $this->Adminmaster_model->insertSmsActivity($tracking_info);

		 	// SMS Tracker End	

								}
							}
						}
					}
					//////////////////other document//////////////////

					foreach($other_documents as $okey => $ovalue) {
					   
						if($ovalue->other_end_date!='0000-00-00'){
						    
							$dateTime = date("d-m-Y",  strtotime($ovalue->other_end_date));
							$doc_date = str_replace("-","/",$dateTime);
							$other_doc	= $ovalue->other_doc_name;
							$other_id	= $ovalue->other_doc_id;
							$noti_message='The renewal period for '.$other_doc.' is coming up on '.$doc_date;
							$notification=array(
								'bus_id'=>$bus_id,
								'reg_id'=>$reg_id,
								'gst_id'=>$gst_id,
								'cust_id'=>$cust_id,
								'ref_id'=>$other_id,
								'noti_type'=>'other_document',
								'noti_message'=>$noti_message,
								'noti_url'=>'sales/view-client/'.$cust_id,
							);	
							
							if($ovalue->other_reminder=='1'){
							   
							    
								if(date('Y-m-d',strtotime($ovalue->other_end_date.'-7 days'))==date('Y-m-d')){
								$this->legal_reminder($cust_name,$other_doc,$doc_date,$cp_email);
								$send=$this->Sales_model->insertData("notifications",$notification);
								send_sms($cp_mobile,$noti_message);
									// SMS Tracker start
			$tracking_info['bus_id'] = $bus_id;
			 $tracking_info['module_name'] = 'Alert Reminder';
			 $tracking_info['action_taken'] = "legal reminder message sent";
			 $tracking_info['reference'] = 'legal document reminder';
			 $LogRec = $this->Adminmaster_model->insertSmsActivity($tracking_info);

		 	// SMS Tracker End	
								}
							}else if($ovalue->other_reminder=='2'){
								if(date('Y-m-d',strtotime($ovalue->other_end_date.'-14 days'))==date('Y-m-d')){
								$this->legal_reminder($cust_name,$other_doc,$doc_date,$cp_email);
								$send=$this->Sales_model->insertData("notifications",$notification);
								send_sms($cp_mobile,$noti_message);
									// SMS Tracker start
			$tracking_info['bus_id'] = $bus_id;
			 $tracking_info['module_name'] = 'Alert Reminder';
			 $tracking_info['action_taken'] = "legal reminder message sent";
			 $tracking_info['reference'] = 'legal document reminder';
			 $LogRec = $this->Adminmaster_model->insertSmsActivity($tracking_info);

		 	// SMS Tracker End	
								}
							}
							else if($ovalue->other_reminder=='3'){
							if(date('Y-m-d',strtotime($ovalue->other_end_date.'-1 month'))==date('Y-m-d')){
								$this->legal_reminder($cust_name,$other_doc,$doc_date,$cp_email);
								$send=$this->Sales_model->insertData("notifications",$notification);
								send_sms($cp_mobile,$noti_message);
									// SMS Tracker start
			$tracking_info['bus_id'] = $bus_id;
			 $tracking_info['module_name'] = 'Alert Reminder';
			 $tracking_info['action_taken'] = "legal reminder message sent";
			 $tracking_info['reference'] = 'legal document reminder';
			 $LogRec = $this->Adminmaster_model->insertSmsActivity($tracking_info);

		 	// SMS Tracker End	
								}
							}
							else if($ovalue->other_reminder=='4'){
								if(date('Y-m-d',strtotime($ovalue->other_end_date.'-2 month'))==date('Y-m-d')){
								$this->legal_reminder($cust_name,$other_doc,$doc_date,$cp_email);
								$send=$this->Sales_model->insertData("notifications",$notification);
								send_sms($cp_mobile,$noti_message);
									// SMS Tracker start
			$tracking_info['bus_id'] = $bus_id;
			 $tracking_info['module_name'] = 'Alert Reminder';
			 $tracking_info['action_taken'] = "legal reminder message sent";
			 $tracking_info['reference'] = 'legal document reminder';
			 $LogRec = $this->Adminmaster_model->insertSmsActivity($tracking_info);

		 	// SMS Tracker End	
								}
							}
						}
					}


					/////////////////other document//////////////////
				}
			}
		//}
		//--------------Start Alerts Section -----------------------//
		$alerts = $this->Sales_model->selectData('my_alerts','*',array('status'=>'Active'));
		print_r($alerts); exit();
		if(count($alerts)>0) {
			foreach($alerts as $altkey => $altvalue) {
				
				if($altvalue->alert_reminder==date('Y-m-d')){
				  
				$bus_id=$altvalue->bus_id;
				$business = $this->Sales_model->selectData('businesslist','reg_id', array("bus_id"=>$bus_id));
				$alert_id=$altvalue->id;
				$reg_id=$business[0]->reg_id;
				$parent_id=$altvalue->parent_id;
				$alert_name=$altvalue->alert_name;
				$alert_type=$altvalue->alert_type;
				$alert_notification=$altvalue->alert_notification;
				$alert_msg=$altvalue->alert_msg;
				$alert_mail=$altvalue->alert_mail;
				$alert_reminder=date("d M",strtotime($altvalue->alert_reminder));
				$alert_occasion=$altvalue->alert_occasion;
				$alert_occasion_date=$altvalue->occasion_date;
				
				if($alert_type=="customers"){
					$customers=$this->Sales_model->selectData('sales_customers','*', array('cust_id'=>$parent_id));

						/*
                          if($rev_tar < $rev){
	                       $alert_name="Congratulations! You have exceeded your set revenue target of Rs" .$rev_tar."for ."$customers[0]->cust_name ";
                          }else{
	                         $alert_name="Alert! Your revenue is below your set target for".$customers[0]->cust_name;
                          }
						*/

						$noti_message=$alert_name;

						$alert_rem =array(
							'bus_id'=>$bus_id,
							'reg_id'=>$reg_id,
							'cust_id'=>$parent_id,
							'ref_id'=>$alert_id,
							'noti_type'=>'customer_alert',
							'noti_message'=>$noti_message,
							'noti_url'=>'settings/view-client-alert/'.$alert_id,
						);

						if($alert_notification==1){

							$send=$this->Sales_model->insertData("notifications",$alert_rem);	
						}

						if($alert_msg==1){
							$message="The one-time password to register on Xebra is";
							send_sms($altvalue->alert_mobile,$message);	
							// SMS Tracker start
							$tracking_info['bus_id'] = $bus_id;
							$tracking_info['module_name'] = 'Alert Reminder';
							$tracking_info['action_taken'] = "message sent";
							$tracking_info['reference'] = 'legal document reminder';
							$LogRec = $this->Adminmaster_model->insertSmsActivity($tracking_info);
			 				// SMS Tracker End	
						}

						if($alert_mail==1){
							$message="The one-time password to register on Xebra is ";
							$this->alerts_email(ucwords($customers[0]->cust_name),$message,$alert_reminder,$altvalue->alert_email);		
						}
					}

					if($alert_type=="services"){
						$services=$this->Sales_model->selectData('services','*', array('service_id'=>$parent_id));
						 /*if($rev_tar < $rev){
	                       $alert_name="Congratulations! You have exceeded your set revenue target of Rs" .$rev_tar."for ."$services[0]->service ;
                          }else{
	                         $alert_name="Alert! Your revenue is below your set target for".$services[0]->service;
                          }*/

						$noti_message=$alert_name;
						$alert_rem =array(
							'bus_id'=>$bus_id,
							'reg_id'=>$reg_id,
							'cust_id'=>$parent_id,
							'ref_id'=>$alert_id,
							'noti_type'=>'customer_alert',
							'noti_message'=>$noti_message,
							'noti_url'=>'settings/view-client-alert/'.$alert_id,
						);
						if($alert_notification==1){

							$send=$this->Sales_model->insertData("notifications",$alert_rem);	
						}
						if($alert_msg==1){
						$message="The one-time password to register on Xebra is";
						send_sms($altvalue->alert_mobile,$message);	
							// SMS Tracker start
			$tracking_info['bus_id'] = $bus_id;
			 $tracking_info['module_name'] = 'Alert Reminder';
			 $tracking_info['action_taken'] = "reminder message sent";
			 $tracking_info['reference'] = 'Alert reminder';
			 $LogRec = $this->Adminmaster_model->insertSmsActivity($tracking_info);

		 	// SMS Tracker End	
						}
						if($alert_mail==1){
							$message="The one-time password to register on Xebra is ";
							$this->alerts_email(ucwords($customers[0]->cust_name),$message,$alert_reminder,$altvalue->alert_email);		
						}
					}

///////////////////////////////////////////////////////////////////

					if($alert_type=="services"){
						$services=$this->Sales_model->selectData('services','*', array('service_id'=>$parent_id));
						 /*if($rev_tar < $rev){
	                       $alert_name="Congratulations! You have exceeded your set revenue target of Rs" .$rev_tar."for ."$services[0]->service ;
                          }else{
	                         $alert_name="Alert! Your revenue is below your set target for".$services[0]->service;
                          }*/

						$noti_message=$alert_name;
						$alert_rem =array(
							'bus_id'=>$bus_id,
							'reg_id'=>$reg_id,
							'cust_id'=>$parent_id,
							'ref_id'=>$alert_id,
							'noti_type'=>'customer_alert',
							'noti_message'=>$noti_message,
							'noti_url'=>'settings/view-client-alert/'.$alert_id,
						);
						if($alert_notification==1){

							$send=$this->Sales_model->insertData("notifications",$alert_rem);	
						}
						if($alert_msg==1){
						$message="The one-time password to register on Xebra is";
						send_sms($altvalue->alert_mobile,$message);	
							// SMS Tracker start
			$tracking_info['bus_id'] = $bus_id;
			 $tracking_info['module_name'] = 'Alert Reminder';
			 $tracking_info['action_taken'] = "reminder message sent";
			 $tracking_info['reference'] = 'Alert reminder';
			 $LogRec = $this->Adminmaster_model->insertSmsActivity($tracking_info);

		 	// SMS Tracker End	
						}
						if($alert_mail==1){
							$message="The one-time password to register on Xebra is ";
							$this->alerts_email(ucwords($customers[0]->cust_name),$message,$alert_reminder,$altvalue->alert_email);		
						}
					}


///////////////////////////////////////////////////////////////////



					if($alert_type=="birthday_anniversary"){
						
						$customers=$this->Sales_model->selectData('sales_customers','*', array('cust_id'=>$parent_id));
				$cp=$this->Sales_model->selectData('sales_customer_contacts','*', array('cust_id'=>$parent_id));
						$noti_message="Hey ".ucwords($customers[0]->cust_name).", We care about your relationships. The ".$alert_occasion." of ".$cp[0]->cp_name." is coming up on ".$alert_occasion_date;
						$alert_rem =array(
							'bus_id'=>$bus_id,
							'reg_id'=>$reg_id,
							'cust_id'=>$parent_id,
							'ref_id'=>$alert_id,
							'noti_type'=>'birthday_anniversary_alert',
							'noti_message'=>$noti_message,
							'noti_url'=>'settings/view-client-alert/'.$alert_id,
						);
						if($alert_notification==1){

							$send=$this->Sales_model->insertData("notifications",$alert_rem);	
						}
						if($alert_msg==1){
					$message="It's ".$cp[0]->cp_name." ".$alert_occasion."today.Send Flowers";
						send_sms($altvalue->alert_mobile,$message);	
						// SMS Tracker start
			$tracking_info['bus_id'] = $bus_id;
			 $tracking_info['module_name'] = 'Alert Reminder';
			 $tracking_info['action_taken'] = "reminder message sent";
			 $tracking_info['reference'] = 'Alert reminder';
			 $LogRec = $this->Adminmaster_model->insertSmsActivity($tracking_info);

		 	// SMS Tracker End	
						}
						if($alert_mail==1){
							$message="It's ".$cp[0]->cp_name." ".$alert_occasion."today. Send Flowers";
							if($alert_occasion=="Birthday"){
							$this->birthday_reminder(ucwords("Admin"),$cp[0]->cp_name,$alert_occasion_date,$altvalue->alert_email);	
							}
							
							if($alert_occasion=="Anniversary"){
							$this->anniversary_reminder(ucwords("Admin"),$cp[0]->cp_name,$alert_occasion_date,$altvalue->alert_email);	
							}
							
						}
					}
				}
			}
		}
		//--------------End Alerts Section -----------------------//

	}

	public function read_notification()
	{
		$noti_id = $_POST['noti_id'];
		$updat =$this->Sales_model->updateData('notifications',array('noti_read_status'=>1),array('noti_id'=>$noti_id));
		if($updat)
		{
			echo "Done";
		}else{
			echo "Fail";
		}
		
	}

	function birthday_reminder($cust_name,$cp_name,$bdate,$email){
		$reg_id = $this->user_session['reg_id'];
        $bus_id = $this->user_session['bus_id'];
        $gst_id = $this->user_session['ei_gst'];

        $fromemail = $this->Sales_model->selectData('businesslist','*',array('bus_id'=>$bus_id,'reg_id'=>$reg_id,'status'=>'Active'));
		$data['client_name']= $cust_name;
		$data['name']= $cp_name;
		$data['bdate']= $bdate;
		$to =  $email;
		$from = $fromemail[0]->bus_company_name;
		$subject = 'There’s a birthday coming up';
		$message=$this->load->view('email_template/birthday-reminder',$data,true);
		sendEmail($from,$to,$subject,$message);
	}
	function anniversary_reminder($cust_name,$cp_name,$bdate,$email){
		$reg_id = $this->user_session['reg_id'];
        $bus_id = $this->user_session['bus_id'];
        $gst_id = $this->user_session['ei_gst'];

        $fromemail = $this->Sales_model->selectData('businesslist','*',array('bus_id'=>$bus_id,'reg_id'=>$reg_id,'status'=>'Active'));
		$data['client_name']= $cust_name;
		$data['name']= $cp_name;
		$data['bdate']= $bdate;
		$from = $fromemail[0]->bus_company_name;
		$to =  $email;
		$subject = 'There’s an anniversary coming up';
		$message=$this->load->view('email_template/anniversary-reminder',$data,true);
		sendEmail($from,$to,$subject,$message);
	}
	function legal_reminder($cust_name,$legal_doc,$doc_date,$email){
		$reg_id = $this->user_session['reg_id'];
        $bus_id = $this->user_session['bus_id'];
        $gst_id = $this->user_session['ei_gst'];

        $fromemail = $this->Sales_model->selectData('businesslist','*',array('bus_id'=>$bus_id,'reg_id'=>$reg_id,'status'=>'Active'));
		$data['client_name']= $cust_name;
		$data['name']= $legal_doc;
		$data['doc_date']= $doc_date;
		$to =  $email;
		$from = $fromemail[0]->bus_company_name;
		$subject = 'Document period is coming to a close';
		$message=$this->load->view('email_template/document-reminder',$data,true);
		sendEmail($from,$to,$subject,$message);
	}
	function alerts_email($cust_name,$message,$alert_reminder,$email){
		$reg_id = $this->user_session['reg_id'];
        $bus_id = $this->user_session['bus_id'];
        $gst_id = $this->user_session['ei_gst'];

        $fromemail = $this->Sales_model->selectData('businesslist','*',array('bus_id'=>$bus_id,'reg_id'=>$reg_id,'status'=>'Active'));
		$data['client_name']= $cust_name;
		$data['name']= $message;
		$data['date']= $alert_reminder;
		$to =  $email;
		$subject = '';


		//$message=$this->load->view('email_template/document-reminder',$data,true);
		//sendEmail($to,$subject,$message);
	}


	function subscription_reminder(){

		$chekUser= $this->User_model->selectData('registration', '*', array('reg_admin_type'=>3,'reg_istrial'=>0));

     foreach ($chekUser as $key => $value) {
     	$reg_id = $this->user_session['reg_id'];
        $bus_id = $this->user_session['bus_id'];
        $gst_id = $this->user_session['ei_gst'];

        $fromemail = $this->Sales_model->selectData('businesslist','*',array('bus_id'=>$bus_id,'reg_id'=>$reg_id,'status'=>'Active'));
       
     	$subscription= $this->User_model->selectData('subscription', '*', array('reg_id'=>$value->reg_id));
     	   if(count($subscription)>0){
           foreach ($subscription as $keyCheck => $valueCheck) {
         
            $createdDate=date('Y-m-d',strtotime($valueCheck->createdat."+365 days"));


        if($valueCheck->subscription_plan=="Booster"){

        	if(date("Y-m-d")==date('Y-m-d',strtotime($createdDate."+7 days"))){

          $data['cust_name']=$value->reg_username;
              $to =  $value->reg_email;
              $from = $fromemail[0]->bus_company_name;
			$subject = 'Your subscription has been end';
			$message=$this->load->view('email_template/subscription-ends-1-week-after',$data,true);
			sendEmail($from,$to,$subject,$message);

			$msg="Your subscription has been end";
		    send_sms($value->reg_mobile,$msg);	
        	}else if(date("Y-m-d")==date('Y-m-d',strtotime($createdDate."+15 days"))){
        		$data['cust_name']=$value->reg_username;
              $to =  $value->reg_email;
              $from = $fromemail[0]->bus_company_name;
			$subject = 'Your subscription nearly comes to  end';
			$message=$this->load->view('email_template/subscription-ends-2-week-after',$data,true);
			sendEmail($from,$to,$subject,$message);

			$msg="Your subscription nearly comes to nearly end";
		    send_sms($value->reg_mobile,$msg);	

        	}else if(date("Y-m-d")==date('Y-m-d',strtotime($createdDate."-7 days"))){
        		$data['cust_name']=$value->reg_username;
              $to =  $value->reg_email;
              $from = $fromemail[0]->bus_company_name;
			$subject = 'Your subscription nearly comes to  end';
			$message=$this->load->view('email_template/subscription-ends-1-week-before',$data,true);
			sendEmail($from,$to,$subject,$message);

			$msg="Your subscription nearly comes to nearly end";
		    send_sms($value->reg_mobile,$msg);	

        	}else{
        		//
        	}

        	$smscount=$valueCheck->smscount;
        	$userSms=4754;

        	 if($userSms==400){
              $data['cust_name']=$value->reg_username;
              $to =  $value->reg_email;
              $from = $fromemail[0]->bus_company_name;
			$subject = 'Your subscription nearly comes to  end';
			$message=$this->load->view('email_template/used-all-allocated-space',$data,true);
			sendEmail($from,$to,$subject,$message);

			$msg="Your subscription nearly comes to nearly end";
		    send_sms($value->reg_mobile,$msg);	
        	 }else if($userSms==450){
              $data['cust_name']=$value->reg_username;
              $to =  $value->reg_email;
              $from = $fromemail[0]->bus_company_name;
			$subject = 'Your subscription nearly comes to  end';
			$message=$this->load->view('email_template/used-all-allocated-space',$data,true);
			sendEmail($from,$to,$subject,$message);

			$msg="Your subscription nearly comes to nearly end";
		    send_sms($value->reg_mobile,$msg);	
        	 }else if($userSms==475){
                 $data['cust_name']=$value->reg_username;
              $to =  $value->reg_email;
              $from = $fromemail[0]->bus_company_name;
			$subject = 'Your subscription nearly comes to  end';
			$message=$this->load->view('email_template/used-all-allocated-space',$data,true);
			sendEmail($from,$to,$subject,$message);

			$msg="Your subscription nearly comes to nearly end";
		    send_sms($value->reg_mobile,$msg);	
        	 }else if($userSms==400){
               $data['cust_name']=$value->reg_username;
              $to =  $value->reg_email;
              $from = $fromemail[0]->bus_company_name;
			$subject = 'Your subscription nearly comes to  end';
			$message=$this->load->view('email_template/used-all-allocated-space',$data,true);
			sendEmail($from,$to,$subject,$message);

			$msg="Your subscription nearly comes to nearly end";
		    send_sms($value->reg_mobile,$msg);	
        	 }else{
        	 	//
        	 }

        	 $storage=$valueCheck->storage;
        	$userStorage=1*10244;

        	 if($userStorage==750){
                   $data['cust_name']=$value->reg_username;
              $to =  $value->reg_email;
              $from = $fromemail[0]->bus_company_name;
			$subject = 'Your subscription nearly comes to  end';
			$message=$this->load->view('email_template/used-all-allocated-space',$data,true);
			sendEmail($from,$to,$subject,$message);

			$msg="Your subscription nearly comes to nearly end";
		    send_sms($value->reg_mobile,$msg);	
        	 }else if($userStorage==850){
                $data['cust_name']=$value->reg_username;
              $to =  $value->reg_email;
              $from = $fromemail[0]->bus_company_name;
			$subject = 'Your subscription nearly comes to  end';
			$message=$this->load->view('email_template/used-all-allocated-space',$data,true);
			sendEmail($from,$to,$subject,$message);

			$msg="Your subscription nearly comes to nearly end";
		    send_sms($value->reg_mobile,$msg);	
        	 }else if($userStorage==900){
               $data['cust_name']=$value->reg_username;
              $to =  $value->reg_email;
              $from = $fromemail[0]->bus_company_name;
			$subject = 'Your subscription nearly comes to  end';
			$message=$this->load->view('email_template/used-all-allocated-space',$data,true);
			sendEmail($from,$to,$subject,$message);

			$msg="Your subscription nearly comes to nearly end";
		    send_sms($value->reg_mobile,$msg);	
        	 }else if($userStorage==1024){
                 $data['cust_name']=$value->reg_username;
              $to =  $value->reg_email;
              $from = $fromemail[0]->bus_company_name;
			$subject = 'Your subscription nearly comes to  end';
			$message=$this->load->view('email_template/used-all-allocated-space',$data,true);
			sendEmail($from,$to,$subject,$message);

			$msg="Your subscription nearly comes to nearly end";
		    send_sms($value->reg_mobile,$msg);	
        	 }else{
        	 	//
        	 }


            }

           if($valueCheck->subscription_plan=="Turbo"){

        	if(date("Y-m-d")==date('Y-m-d',strtotime($createdDate."+7 days"))){

              $data['cust_name']=$value->reg_username;
              $to =  $value->reg_email;
              $from = $fromemail[0]->bus_company_name;
			$subject = 'Your subscription has been end';
			$message=$this->load->view('email_template/subscription-ends-1-week-after',$data,true);
			sendEmail($from,$to,$subject,$message);

			$msg="Your subscription has been end";
		    send_sms($value->reg_mobile,$msg);	
        	}else if(date("Y-m-d")==date('Y-m-d',strtotime($createdDate."+15 days"))){
                 $data['cust_name']=$value->reg_username;
              $to =  $value->reg_email;
              $from = $fromemail[0]->bus_company_name;
			$subject = 'Your subscription has been  end';
			$message=$this->load->view('email_template/subscription-ends-2-week-after',$data,true);
			sendEmail($from,$to,$subject,$message);

			$msg="Your subscription has been end";
		    send_sms($value->reg_mobile,$msg);	
        	}else if(date("Y-m-d")==date('Y-m-d',strtotime($createdDate."-7 days"))){
              $data['cust_name']=$value->reg_username;
              $to =  $value->reg_email;
              $from = $fromemail[0]->bus_company_name;
			$subject = 'Your subscription nearly comes to  end';
			$message=$this->load->view('email_template/subscription-ends-1-week-before',$data,true);
			sendEmail($from,$to,$subject,$message);

			$msg="Your subscription nearly comes to nearly end";
		    send_sms($value->reg_mobile,$msg);	
        	}else{
        		//
        	}

        	$smscount=$valueCheck->smscount;
        	$userSms=475;

        	 if($userSms==400){
               $data['cust_name']=$value->reg_username;
              $to =  $value->reg_email;
              $from = $fromemail[0]->bus_company_name;
			$subject = 'Your subscription nearly comes to  end';
			$message=$this->load->view('email_template/used-all-allocated-space',$data,true);
			sendEmail($from,$to,$subject,$message);

			$msg="Your subscription nearly comes to nearly end";
		    send_sms($value->reg_mobile,$msg);	
        	 }else if($userSms==450){
              $data['cust_name']=$value->reg_username;
              $to =  $value->reg_email;
              $from = $fromemail[0]->bus_company_name;
			$subject = 'Your subscription nearly comes to  end';
			$message=$this->load->view('email_template/used-all-allocated-space',$data,true);
			sendEmail($from,$to,$subject,$message);

			$msg="Your subscription nearly comes to nearly end";
		    send_sms($value->reg_mobile,$msg);	
        	 }else if($userSms==475){
              $data['cust_name']=$value->reg_username;
              $to =  $value->reg_email;
              $from = $fromemail[0]->bus_company_name;
			$subject = 'Your subscription nearly comes to  end';
			$message=$this->load->view('email_template/used-all-allocated-space',$data,true);
			sendEmail($from,$to,$subject,$message);

			$msg="Your subscription nearly comes to nearly end";
		    send_sms($value->reg_mobile,$msg);	
        	 }else if($userSms==400){
                $data['cust_name']=$value->reg_username;
              $to =  $value->reg_email;
              $from = $fromemail[0]->bus_company_name;
			$subject = 'Your subscription nearly comes to  end';
			$message=$this->load->view('email_template/used-all-allocated-space',$data,true);
			sendEmail($from,$to,$subject,$message);

			$msg="Your subscription nearly comes to nearly end";
		    send_sms($value->reg_mobile,$msg);	
        	 }else{
        	 	//
        	 }

        	  $storage=$valueCheck->storage;
        	$userStorage=1*1024;

        	 if($userStorage==750){
               $data['cust_name']=$value->reg_username;
              $to =  $value->reg_email;
              $from = $fromemail[0]->bus_company_name;
			$subject = 'Your subscription nearly comes to  end';
			$message=$this->load->view('email_template/used-all-allocated-space',$data,true);
			sendEmail($from,$to,$subject,$message);

			$msg="Your subscription nearly comes to nearly end";
		    send_sms($value->reg_mobile,$msg);	
        	 }else if($userStorage==850){
              $data['cust_name']=$value->reg_username;
              $to =  $value->reg_email;
              $from = $fromemail[0]->bus_company_name;
			$subject = 'Your subscription nearly comes to  end';
			$message=$this->load->view('email_template/used-all-allocated-space',$data,true);
			sendEmail($from,$to,$subject,$message);

			$msg="Your subscription nearly comes to nearly end";
		    send_sms($value->reg_mobile,$msg);	
        	 }else if($userStorage==900){
             $data['cust_name']=$value->reg_username;
              $to =  $value->reg_email;
              $from = $fromemail[0]->bus_company_name;
			$subject = 'Your subscription nearly comes to  end';
			$message=$this->load->view('email_template/used-all-allocated-space',$data,true);
			sendEmail($from,$to,$subject,$message);

			$msg="Your subscription nearly comes to nearly end";
		    send_sms($value->reg_mobile,$msg);	
        	 }else if($userStorage==1024){
               $data['cust_name']=$value->reg_username;
              $to =  $value->reg_email;
              $from = $fromemail[0]->bus_company_name;
			$subject = 'Your subscription nearly comes to  end';
			$message=$this->load->view('email_template/used-all-allocated-space',$data,true);
			sendEmail($from,$to,$subject,$message);

			$msg="Your subscription nearly comes to nearly end";
		    send_sms($value->reg_mobile,$msg);	
        	 }else{
        	 	//
        	 }


            }


        if($valueCheck->subscription_plan=="Full Throttle"){

        	if(date("Y-m-d")==date('Y-m-d',strtotime($createdDate."+7 days"))){
          $data['cust_name']=$value->reg_username;
              $to =  $value->reg_email;
              $from = $fromemail[0]->bus_company_name;
			$subject = 'Your subscription has been end';
			$message=$this->load->view('email_template/subscription-ends-1-week-after',$data,true);
			sendEmail($from,$to,$subject,$message);

			$msg="Your subscription has been end";
		    send_sms($value->reg_mobile,$msg);	
        	}else if(date("Y-m-d")==date('Y-m-d',strtotime($createdDate."+15 days"))){
        		$data['cust_name']=$value->reg_username;
              $to =  $value->reg_email;
              $from = $fromemail[0]->bus_company_name;
			$subject = 'Your subscription nearly comes to  end';
			$message=$this->load->view('email_template/subscription-ends-2-week-after',$data,true);
			sendEmail($from,$to,$subject,$message);

			$msg="Your subscription nearly comes to nearly end";
		    send_sms($value->reg_mobile,$msg);	

        	}else if(date("Y-m-d")==date('Y-m-d',strtotime($createdDate."-7 days"))){
        		$data['cust_name']=$value->reg_username;
              $to =  $value->reg_email;
              $from = $fromemail[0]->bus_company_name;
			$subject = 'Your subscription nearly comes to  end';
			$message=$this->load->view('email_template/subscription-ends-1-week-before',$data,true);
			sendEmail($from,$to,$subject,$message);

			$msg="Your subscription nearly comes to nearly end";
		    send_sms($value->reg_mobile,$msg);	

        	}else{
        		//
        	}

        	$smscount=$valueCheck->smscount;
        	$userSms=475;

        	 if($userSms==400){
              $data['cust_name']=$value->reg_username;
              $to =  $value->reg_email;
              $from = $fromemail[0]->bus_company_name;
			$subject = 'Your subscription nearly comes to  end';
			$message=$this->load->view('email_template/used-all-allocated-space',$data,true);
			sendEmail($from,$to,$subject,$message);

			$msg="Your subscription nearly comes to nearly end";
		    send_sms($value->reg_mobile,$msg);	
        	 }else if($userSms==450){
              $data['cust_name']=$value->reg_username;
              $to =  $value->reg_email;
              $from = $fromemail[0]->bus_company_name;
			$subject = 'Your subscription nearly comes to  end';
			$message=$this->load->view('email_template/used-all-allocated-space',$data,true);
			sendEmail($from,$to,$subject,$message);

			$msg="Your subscription nearly comes to nearly end";
		    send_sms($value->reg_mobile,$msg);	
        	 }else if($userSms==475){
                 $data['cust_name']=$value->reg_username;
              $to =  $value->reg_email;
              $from = $fromemail[0]->bus_company_name;
			$subject = 'Your subscription nearly comes to  end';
			$message=$this->load->view('email_template/used-all-allocated-space',$data,true);
			sendEmail($from,$to,$subject,$message);

			$msg="Your subscription nearly comes to nearly end";
		    send_sms($value->reg_mobile,$msg);	
        	 }else if($userSms==400){
               $data['cust_name']=$value->reg_username;
              $to =  $value->reg_email;
              $from = $fromemail[0]->bus_company_name;
			$subject = 'Your subscription nearly comes to  end';
			$message=$this->load->view('email_template/used-all-allocated-space',$data,true);
			sendEmail($from,$to,$subject,$message);

			$msg="Your subscription nearly comes to nearly end";
		    send_sms($value->reg_mobile,$msg);	
        	 }else{
        	 	//
        	 }

        	 $storage=$valueCheck->storage;
        	$userStorage=1*1024;

        	 if($userStorage==750){
                   $data['cust_name']=$value->reg_username;
              $to =  $value->reg_email;
              $from = $fromemail[0]->bus_company_name;
			$subject = 'Your subscription nearly comes to  end';
			$message=$this->load->view('email_template/used-all-allocated-space',$data,true);
			sendEmail($from,$to,$subject,$message);

			$msg="Your subscription nearly comes to nearly end";
		    send_sms($value->reg_mobile,$msg);	
        	 }else if($userStorage==850){
                $data['cust_name']=$value->reg_username;
              $to =  $value->reg_email;
              $from = $fromemail[0]->bus_company_name;
			$subject = 'Your subscription nearly comes to  end';
			$message=$this->load->view('email_template/used-all-allocated-space',$data,true);
			sendEmail($from,$to,$subject,$message);

			$msg="Your subscription nearly comes to nearly end";
		    send_sms($value->reg_mobile,$msg);	
        	 }else if($userStorage==900){
               $data['cust_name']=$value->reg_username;
              $to =  $value->reg_email;
              $from = $fromemail[0]->bus_company_name;
			$subject = 'Your subscription nearly comes to  end';
			$message=$this->load->view('email_template/used-all-allocated-space',$data,true);
			sendEmail($from,$to,$subject,$message);

			$msg="Your subscription nearly comes to nearly end";
		    send_sms($value->reg_mobile,$msg);	
        	 }else if($userStorage==1024){
                 $data['cust_name']=$value->reg_username;
              $to =  $value->reg_email;
              $from = $fromemail[0]->bus_company_name;
			$subject = 'Your subscription nearly comes to  end';
			$message=$this->load->view('email_template/used-all-allocated-space',$data,true);
			sendEmail($from,$to,$subject,$message);

			$msg="Your subscription nearly comes to nearly end";
		    send_sms($value->reg_mobile,$msg);	
        	 }else{
        	 	//
        	 }


            }
         
         }     

         }   

     }


	}



	public function change_status() {

		$post = $this->input->post();

		

			
			$data = array(
				'clear_noti' => 1,
			);
	

		$flag = $this->User_model->updateData('notifications',$data,array());
		
		
		echo json_encode($flag);
		exit();
	}




	function trial_period_expire(){
		$reg_id = $this->user_session['reg_id'];
        $bus_id = $this->user_session['bus_id'];
        $gst_id = $this->user_session['ei_gst'];

        $fromemail = $this->Sales_model->selectData('businesslist','*',array('bus_id'=>$bus_id,'reg_id'=>$reg_id,'status'=>'Active'));

       $chekUser= $this->User_model->selectData('registration', '*', array('reg_admin_type'=>3,'reg_istrial'=>1));

     foreach ($chekUser as $key => $value) {
         $createdDate=date('Y-m-d',strtotime($value->createdat));
     	
     	$trial_end=date('Y-m-d',strtotime($createdDate."+14days"));

     	$data['cust_name']= $value->reg_username;
			
     	
     	if(date("Y-m-d")==date('Y-m-d',strtotime($trial_end."+1day"))){

			$data['cust_name']= $value->reg_username;
			$from = $fromemail[0]->bus_company_name;
			$to =  $value->reg_email;
			$subject = 'Your trial period expired';
			$message=$this->load->view('email_template/trial-period-ends',$data,true);
			sendEmail($from,$to,$subject,$message);

			$msg="Your trial period expired";
		    send_sms($value->reg_mobile,$msg);	
         
     	}

     	if(date("Y-m-d")==date('Y-m-d',strtotime($trial_end."-7 days"))){

			$data['cust_name']= $value->reg_username;
			$from = $fromemail[0]->bus_company_name;
			$to =  $value->reg_email;
			$subject = 'Just one week left your trial period expires';
			$message=$this->load->view('email_template/trial-period-ends-1-week-before',$data,true);
			sendEmail($from,$to,$subject,$message);
			$msg="Just one week left your trial period expires";
		    send_sms($value->reg_mobile,$msg);
		    // SMS Tracker start
			
			 $tracking_info['module_name'] = 'Alert Notification';
			 $tracking_info['action_taken'] = $message;
			 $tracking_info['reference'] = 'Trial Period end alert sms';
			 $LogRec = $this->Adminmaster_model->insertSmsActivity($tracking_info);

		 	// SMS Tracker End		
         
     	}

     	if(date("Y-m-d")==date('Y-m-d',strtotime($trial_end."+7 days"))){

			$data['cust_name']= $value->reg_username;
			$from = $fromemail[0]->bus_company_name;
			$to =  $value->reg_email;
			$subject = 'We’re missing you already';
			$message=$this->load->view('email_template/trial-period-ends-1-week-after',$data,true);
			sendEmail($from,$to,$subject,$message);
			$msg="We are missing you already.Your trial period expired one week ago.";
		    send_sms($value->reg_mobile,$msg);	
		    
         
     	}

     	if(date("Y-m-d")==date('Y-m-d',strtotime($trial_end."+14 days"))){

			$data['cust_name']= $value->reg_username;
			$from = $fromemail[0]->bus_company_name;
			$to =  $value->reg_email;
			$subject = 'We’re missing you already';
			$message=$this->load->view('email_template/trial-period-ends-1-week-after',$data,true);
			sendEmail($from,$to,$subject,$message);
			$msg="We are missing you already.Your trial period expired 15 days ago.";
		    send_sms($value->reg_mobile,$msg);	
		    
         
     	}

     	if(date("Y-m-d")==date('Y-m-d',strtotime($trial_end."+21 days"))){

			$data['cust_name']= $value->reg_username;
			
			$to =  $value->reg_email;
			$from = $fromemail[0]->bus_company_name;
			$subject = 'We’re missing you already';
			$message=$this->load->view('email_template/trial-period-ends-1-week-after',$data,true);
			sendEmail($from,$to,$subject,$message);
			$msg="We are missing you already.Your trial period expired 21 days ago.";
		    send_sms($value->reg_mobile,$msg);
		   
         
     	}

     	if(date("Y-m-d")==date('Y-m-d',strtotime($trial_end."+1 month"))){

			$data['cust_name']= $value->reg_username;
			
			$to =  $value->reg_email;
			$from = $fromemail[0]->bus_company_name;
			$subject = 'We’re missing you already';
			$message=$this->load->view('email_template/trial-period-ends-1-week-after',$data,true);
			sendEmail($from,$to,$subject,$message);
			$msg="We are missing you already.Your trial period expired 1 month ago.";
		    send_sms($value->reg_mobile,$msg);
		    
         
     	}
     }


	}
function reccurring_to_invoice(){
        
       
		$invoices = $this->Sales_model->selectData('sales_invoices', '*', array('inv_nature_type'=>'recurring','inv_nooftime_recurring >'=>0));
       
		foreach ($invoices as $key => $value) {
		    
              $inv_details = $this->Sales_model->selectData('sales_invoices', '*', array('inv_id'=>$value->inv_id,'inv_nature_type'=>'recurring'));
            $myArray = (array) $inv_details[0];
           
        $custm_inv_details = $this->Sales_model->selectData('customise_invoices', '*', array('inv_id'=>$value->inv_id));
        $custInvArray = (array) $custm_inv_details[0];
        //unset($myArray['inv_id']);
       // unset($myArray['inv_invoice_no']);
        //unset($myArray['inv_document_type']);
		
         if (date('m') >= 4) {

	$year = date('Y')."-".(date('y') +1);

	}

	else {
	$year = (date('Y')-1)."-".date('y');

	}
		
		$inv_no=$this->Sales_model->selectData('sales_invoices', '*', array('bus_id'=>$value->bus_id,'inv_document_type'=>'Sales Invoice','RIGHT(inv_invoice_no,7)'=>$year),'inv_id','DESC'); 
		
		if(count($inv_no)>0){
			$last_inv_no=$inv_no[0]->inv_invoice_no; 
			 $whatIWant = substr($last_inv_no, strpos($last_inv_no, "/") + 1); 
              $invNo=str_replace('/'.$whatIWant,"",$last_inv_no);
              $temp=str_replace("INV","",$invNo);
			//if($inv_no[0]->document_type=='2'){
				$invno_ref='1'; 
				$temp=intval($temp) + 1;
				//$temp = substr($last_inv_no, 3, -7);
				//$temp= str_replace('INV', '', $last_inv_no); 
				$invno_ref=str_pad($temp, 6, 0, STR_PAD_LEFT);
			//}
		} 
		else
		{
				$invno_ref=str_pad(1, 6, 0, STR_PAD_LEFT);
			
			
		}
		if (date('m') < 4) {//Upto June 2014-2015
    $financial_year = (date('Y')-1) . '-' . date('y');
} else {//After June 2015-2016
    $financial_year = date('Y') . '-' . (date('y') + 1);
}       

      
        //$myArray['inv_po_no']=$myArray['inv_invoice_no'];
		//$myArray['inv_po_date']=$myArray['inv_invoice_date'];
		$myArray['inv_invoice_no']="INV".$invno_ref."/".$financial_year;
		$myArray['inv_document_type']='Sales Invoice';
		$myArray['inv_nature_type']='recurring';
		$myArray['inv_status']='Pending';
		
		
		 
		 for($j=0;$j<$myArray['inv_nooftime_recurring'];$j++){
	
		
		if($myArray['inv_recurring_frequency']=='Weekly'){
		    
		    $dateOfNextRec=date('Y-m-d',strtotime($myArray['inv_recurring_date']."+7 days"));
		  
		}

		if($myArray['inv_recurring_frequency']=='Fortnightly'){
		    
		    $dateOfNextRec=date('Y-m-d',strtotime($myArray['inv_recurring_date']."+15 days"));
		    
		}
		
		
		if($myArray['inv_recurring_frequency']=='Monthly'){
		    
		    $dateOfNextRec=date('Y-m-d',strtotime($myArray['inv_recurring_date']."+1 Month"));
		    
		}
		
		if($myArray['inv_recurring_frequency']=='Yearly'){
		    
		    $dateOfNextRec=date('Y-m-d',strtotime($myArray['inv_recurring_date']."+1 Year"));
		    
		}
		
	
		    
		    $myArray['inv_po_date']=$dateOfNextRec;
		    $myArray['inv_invoice_date']=$dateOfNextRec;
		     $myArray['inv_recurring_date']=$dateOfNextRec;
		     $myArray['inv_recurring_date']=$dateOfNextRec;
		$myArray['inv_nooftime_recurring']=$myArray['inv_nooftime_recurring']-1;
			$myArray['createdat']=date("Y-m-d H:i:s");
				$myArray['updatedat']=date("Y-m-d H:i:s");
		    
		    //
		     $filterSet = array(

			

                  				  

                  				   "inv_nooftime_recurring"=>0,	



		);

         

		$flagSet=$this->Sales_model->updateData('sales_invoices',$filterSet,array('inv_id'=>$myArray['inv_id']));
		    unset($myArray['inv_id']); 
		    
		if(date('Y-m-d')==$dateOfNextRec){
		    
		   
         $inv_id1=$this->Sales_model->insertData('sales_invoices',$myArray);
         
         $inv_list_details = $this->Sales_model->selectData('sales_invoices_list', '*', array('inv_id'=>$value->inv_id));
          $myArray1 = (array) $inv_list_details[0];
         
          

        unset($myArray1['invl_id']);

        unset($myArray1['inv_id']);                           

    
        if($inv_id1){


                    
                    unset($custInvArray['cust_inv_id']);
                    unset($custInvArray['inv_id']);
                    $custInvArray['inv_id']=$inv_id1;
        	         $myArray1['inv_id']=$inv_id1;
        	        
        	         $estl_id=$this->Sales_model->insertData('sales_invoices_list',$myArray1);
        	        
    
        	         $flag = $this->Sales_model->insertData('customise_invoices', $custInvArray);

        }
        
		}
        
		 }
			
		}
     

		//print_r($inv_id);exit;

	}

	function reccurring_to_expense(){
        
       
		$expenses = $this->Sales_model->selectData('company_expense', '*', array('ce_nature'=>'recurring','ce_no_of_recur >'=>0));
       
		foreach ($invoices as $key => $value) {
		    
              $inv_details = $this->Sales_model->selectData('sales_invoices', '*', array('inv_id'=>$value->inv_id,'inv_nature_type'=>'recurring'));
            $myArray = (array) $inv_details[0];
           
        $custm_inv_details = $this->Sales_model->selectData('customise_invoices', '*', array('inv_id'=>$value->inv_id));
        $custInvArray = (array) $custm_inv_details[0];
        //unset($myArray['inv_id']);
       // unset($myArray['inv_invoice_no']);
        //unset($myArray['inv_document_type']);
		
         if (date('m') >= 4) {

	$year = date('Y')."-".(date('y') +1);

	}

	else {
	$year = (date('Y')-1)."-".date('y');

	}
		
		$inv_no=$this->Sales_model->selectData('sales_invoices', '*', array('bus_id'=>$value->bus_id,'inv_document_type'=>'Sales Invoice','RIGHT(inv_invoice_no,7)'=>$year),'inv_id','DESC'); 
		
		if(count($inv_no)>0){
			$last_inv_no=$inv_no[0]->inv_invoice_no; 
			 $whatIWant = substr($last_inv_no, strpos($last_inv_no, "/") + 1); 
              $invNo=str_replace('/'.$whatIWant,"",$last_inv_no);
              $temp=str_replace("INV","",$invNo);
			//if($inv_no[0]->document_type=='2'){
				$invno_ref='1'; 
				$temp=intval($temp) + 1;
				//$temp = substr($last_inv_no, 3, -7);
				//$temp= str_replace('INV', '', $last_inv_no); 
				$invno_ref=str_pad($temp, 6, 0, STR_PAD_LEFT);
			//}
		} 
		else
		{
				$invno_ref=str_pad(1, 6, 0, STR_PAD_LEFT);
			
			
		}
		if (date('m') < 4) {//Upto June 2014-2015
    $financial_year = (date('Y')-1) . '-' . date('y');
} else {//After June 2015-2016
    $financial_year = date('Y') . '-' . (date('y') + 1);
}       

      
        //$myArray['inv_po_no']=$myArray['inv_invoice_no'];
		//$myArray['inv_po_date']=$myArray['inv_invoice_date'];
		$myArray['inv_invoice_no']="INV".$invno_ref."/".$financial_year;
		$myArray['inv_document_type']='Sales Invoice';
		$myArray['inv_nature_type']='recurring';
		$myArray['inv_status']='Pending';
		
		
		 
		 for($j=0;$j<$myArray['inv_nooftime_recurring'];$j++){
	
		
		if($myArray['inv_recurring_frequency']=='Weekly'){
		    
		    $dateOfNextRec=date('Y-m-d',strtotime($myArray['inv_recurring_date']."+7 days"));
		  
		}

		if($myArray['inv_recurring_frequency']=='Fortnightly'){
		    
		    $dateOfNextRec=date('Y-m-d',strtotime($myArray['inv_recurring_date']."+15 days"));
		    
		}
		
		
		if($myArray['inv_recurring_frequency']=='Monthly'){
		    
		    $dateOfNextRec=date('Y-m-d',strtotime($myArray['inv_recurring_date']."+1 Month"));
		    
		}
		
		if($myArray['inv_recurring_frequency']=='Yearly'){
		    
		    $dateOfNextRec=date('Y-m-d',strtotime($myArray['inv_recurring_date']."+1 Year"));
		    
		}
		
	
		    
		    $myArray['inv_po_date']=$dateOfNextRec;
		    $myArray['inv_invoice_date']=$dateOfNextRec;
		     $myArray['inv_recurring_date']=$dateOfNextRec;
		     $myArray['inv_recurring_date']=$dateOfNextRec;
		$myArray['inv_nooftime_recurring']=$myArray['inv_nooftime_recurring']-1;
			$myArray['createdat']=date("Y-m-d H:i:s");
				$myArray['updatedat']=date("Y-m-d H:i:s");
		    
		    //
		     $filterSet = array(

			

                  				  

                  				   "inv_nooftime_recurring"=>0,	



		);

         

		$flagSet=$this->Sales_model->updateData('sales_invoices',$filterSet,array('inv_id'=>$myArray['inv_id']));
		    unset($myArray['inv_id']); 
		    
		if(date('Y-m-d')==$dateOfNextRec){
		    
		   
         $inv_id1=$this->Sales_model->insertData('sales_invoices',$myArray);
         
         $inv_list_details = $this->Sales_model->selectData('sales_invoices_list', '*', array('inv_id'=>$value->inv_id));
          $myArray1 = (array) $inv_list_details[0];
         
          

        unset($myArray1['invl_id']);

        unset($myArray1['inv_id']);                           

    
        if($inv_id1){


                    
                    unset($custInvArray['cust_inv_id']);
                    unset($custInvArray['inv_id']);
                    $custInvArray['inv_id']=$inv_id1;
        	         $myArray1['inv_id']=$inv_id1;
        	        
        	         $estl_id=$this->Sales_model->insertData('sales_invoices_list',$myArray1);
        	        
    
        	         $flag = $this->Sales_model->insertData('customise_invoices', $custInvArray);

        }
        
		}
        
		 }
			
		}
     

		//print_r($inv_id);exit;

	}

}