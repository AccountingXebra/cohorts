<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require ("common/Index_Controller.php");
class Dashboards extends Index_Controller {

	function __construct(){

		parent::__construct();
		$this->load->model('User_model');
		$this->load->model('Profile_model');
	}

	public function index()
	{ 	
		$reg_id = $this->session->userdata['user_session']['reg_id'];
		$data['select_service'] = $this->Profile_model->selectData('registration', '*', array('reg_createdby'=>$reg_id,'reg_bus_type'=>''));
		if(count($data)>0){
		redirect('profile/company_profile',$data);
		}else{
			redirect('profile/company_profile');
		}
	}
	public function get_branch_gst_info()
	{
		$csrf_token=$this->security->get_csrf_hash();
		$bus_id = $this->input->post('bus_id');
		$reg_id = $this->session->userdata['user_session']['reg_id'];
		$branch_gst = $this->Profile_model->selectData('gst_number', '*', array('bus_id'=>$bus_id,'type'=>'business'),'gst_id','ASC');
		$this->session->userdata['user_session']['bus_id']=$bus_id;
		$this->session->userdata['user_session']['ei_gst']='';
			
		if(count($branch_gst)>0){

		$option="";
		foreach($branch_gst as $k=>$row){
			if($k==0){
				$this->session->userdata['user_session']['ei_gst']=$row->gst_id;
			}
			/*$result = mb_substr($row->gst_no, 0, 7).'...';*/
			$option.="<option value=".$row->gst_id.">GSTIN: ".$row->gst_no." - ".$row->place."</option>";
		}
		$option.='<option value="Combine">Combine</option>';
			echo json_encode($option);
		}else{
			echo json_encode(false);
		}

	}
	public function change_branch_gst_info()
	{
		$gst_id = $this->input->post('gst_id');
		$reg_id = $this->session->userdata['user_session']['reg_id'];
		$bus_id = $this->session->userdata['user_session']['bus_id'];
		$this->session->userdata['user_session']['ei_gst']=$gst_id;
		echo json_encode(true);
		

	}


	public function document_locker() {
		
		$reg_id = $this->session->userdata['user_session']['reg_id'];
		$bus_id = $this->session->userdata['user_session']['bus_id'];
		$data['legal'] = $this->Profile_model->selectData('legal_document', '*',array('master_id' => $bus_id,'status' => 'Active'));
		$data['other'] = $this->Profile_model->selectData('other_document', '*',array('master_id' => $bus_id));
		$data['clients'] = $this->Profile_model->selectData('sales_customers', '*', array('bus_id' => $bus_id));
	
		if(count($data['legal']) > 0 || count($data['other']) > 0) {
		
			$this->load->view('dashboard/document_locker/documents-list',$data);

		} else {

			$this->load->view('dashboard/document_locker/documents-list',$data);
		} 
	}

	public function get_documents_locker(){

		// Activity Tracker start
		$tracking_info['module_name'] = 'Document Locker';
		$tracking_info['action_taken'] = 'Viewed';
		$tracking_info['reference'] = "Document Locker List";
		$LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
		// Activity Tracker End

		$post = $this->input->post();
		$post['reg_id'] = $this->user_session['reg_id'];
		$post['bus_id'] = $this->user_session['bus_id'];
		
		$field_pos = array("legal_id" => '0',"created_at" => '1',"legal_document" => '2',"legal_type" => '3',"legal_document_size" => '4');	

		$sort_field = array_search($post['order'][0]['column'],$field_pos);

		if($post['order'][0]['dir'] == 'asc') {
	 		$orderBy = "ASC";
		} else {
	 		$orderBy = "DESC";
		}	

		$TotalRecord = $this->User_model->documentLockerFilter($post,$sort_field,$orderBy,0);
	 	$userData = $this->User_model->documentLockerFilter($post,$sort_field,$orderBy,1);

	 	$iTotalRecords = $TotalRecord['NumRecords'];

	 	$records = array();
	  	$records["data"] = array();
       $business=$this->Profile_model->selectData('businesslist', '*', array('bus_id'=>$this->user_session['bus_id']));
       if(count($business) >0){
       $clientName=$business[0]->bus_company_name;
       }else{
       	$clientName="";
       }
				
	  	foreach ($userData as $key => $value) {

	  		$path="";

			if($value['doc_type'] == "legal" and $value['type'] == "customer") {
             	$path = base_url().'public/upload/sales/customer/customer_legal_documents/'.$value['bus_id'].'/'.$value['dc_name'];
             	$doctype = 1;
			}

			if($value['doc_type'] == "legal" and $value['type']=="company") {
				$path=base_url().'public/upload/company_legal_documents/'.$value['dc_name'];
             	$doctype=1;
			}

			if($value['doc_type']=="legal" and $value['type']=="vendor") {
				$path=base_url().'public/upload/expense/vendor/vendor_legal_documents/'.$value['dc_name'];
             	$doctype=1;
			}

			if($value['doc_type']=="purchase_order" and $value['type']=="customer") {
              	$path=base_url().'public/upload/sales/customer/customer_po_documents/'.$value['bus_id'].'/'.$value['dc_name'];
              	$doctype=2;
			}

			if($value['doc_type']=="purchase_order" and $value['type']=="company") {
			  	$path=base_url().'public/upload/company_po_documents/'.$value['dc_name'];
              	$doctype=2;
			}

			if($value['doc_type']=="purchase_order" and $value['type']=="vendor") {
			  	$path=base_url().'public/upload/expense/vendor/vendor_po_documents/'.$value['dc_name'];
              	$doctype=2;
			}

			if($value['doc_type']=="other" and $value['type']=="customer") {
				$path=base_url().'public/upload/sales/customer/customer_other_documents/'.$value['bus_id'].'/'.$value['dc_name'];
				$doctype=3;
			}
			if($value['doc_type']=="other" and $value['type']=="company") {
				$path=base_url().'public/upload/company_other_documents/'.$value['dc_name'];
				$doctype=3;
			}

			if($value['doc_type']=="other" and $value['type']=="vendor") {
				$path=base_url().'public/upload/expense/vendor/vendor_other_documents/'.$value['bus_id'].'/'.$value['dc_name'];
				$doctype=3;
			}

			if($value['doc_type']=="Memorandum" and $value['type']=="company") {
				$path=base_url().'public/upload/company_moa_documents/'.$value['dc_name'];
				$doctype=4;
			}

			if($value['doc_type']=="Memorandum" and $value['type']=="customer") {
				$path=base_url().'public/sales/customer/company_moa_documents/'.$value['dc_name'];
				$doctype=4;
			}

			if($value['doc_type']=="Memorandum" and $value['type']=="vendor") {
				$path=base_url().'public/expense/vendor/vendor_moa_documents/'.$value['dc_name'];
				$doctype=4;
			}

			if($value['doc_type']=="sales_receipt") {
				$path=base_url().'public/upload/receipts_payments/sales_receipt/'.$value['parent_id'].'/'.$value['dc_name'];
				$doctype=5;
			}

			if($value['doc_type'] == "expense_voucher") {
				$path=base_url().'public/upload/expense_voucher_images/'.$value['parent_id'].'/'.$value['dc_name'];
				$doctype=6;
			}

			if($value['doc_type'] == "asset_purchase") {
				$path=base_url().'public/upload/asset_purchase/'.$value['parent_id'].'/'.$value['dc_name'];
				$doctype=7;
			}

			if($value['doc_type'] == "asset_tracker") {
				$path=base_url().'public/upload/asset_tracker/'.$value['parent_id'].'/'.$value['dc_name'];
				$doctype=8;
			}

			if($value['doc_type'] == "asset_sales") {
				$path=base_url().'public/upload/asset_sales/'.$value['parent_id'].'/'.$value['dc_name'];
				$doctype=9;
			}

			if($value['doc_type'] == "company_expense") {
				$path=base_url().'public/upload/company_expense/'.$value['parent_id'].'/'.$value['dc_name'];
				$doctype=10;
			}

			if($value['doc_type'] == "gst_challan") {
				$path=base_url().'public/upload/gst_challan_img/'.$value['parent_id'].'/'.$value['dc_name'];
				$doctype=11;
			}

			if($value['doc_type'] == "prof_tax") {
				$path=base_url().'public/upload/prof_tax/'.$value['parent_id'].'/'.$value['dc_name'];
				$doctype=12;
			}

			if($value['doc_type'] == "tds_emp") {
				$path=base_url().'public/upload/tds_emp/'.$value['parent_id'].'/'.$value['dc_name'];
				$doctype=13;
			}

			if($value['doc_type']=="asset_register") {
				$path=base_url().'public/upload/asset_register/'.$value['parent_id'].'/'.$value['dc_name'];
				
				$doctype=14;
			}
           
           if($value['doc_type']=="equilisation_payment"){
				$path=base_url().'public/upload/equi_levy_payment/'.$value['parent_id'].'/'.$value['dc_name'];
				$doctype=15;
			}

			 if($value['doc_type']=="expense_payments"){
				$path=base_url().'public/upload/receipts_payments/expense_payments/'.$value['parent_id'].'/'.$value['dc_name'];
				$doctype=16;
			}


			
	  		$check='<input type="checkbox" id="filled-in-box'.$value['dc_id'].'" class="filled-in purple docl_bulk_action" value="'.$value['dc_id'].'-'.$doctype.'"/> <label for="filled-in-box'.$value['dc_id'].'"></label>';
	  		
	  		$d_name=preg_replace('/\\.[^.\\s]{3,4}$/', '', $value['dc_name']);
	  		
	  		//truncate string
	  		$dc_name=$d_name;
	  		if (strlen($d_name) > 25) {
   		    $stringCut = substr($d_name, 0, 25);
            $endPoint = strrpos($stringCut, ' ');
            $dc_name = $endPoint? substr($stringCut, 0, $endPoint):substr($stringCut, 0);
   			$dc_name .= '...';
   			}
	  		$type='';
	  		if($value['type']=="customer" ){
	  			$type="Clients";

	  			$href=base_url().'sales/edit-client/'.$value['bus_id'];
	  			
	  		}else if($value['type']=="vendor" ){
	  			$type="Vendor";

	  			$href=base_url().'expense/edit-vendor/'.$value['bus_id'];
	  			
	  		}else if($value['type']=="sales_receipt"){
             $type="Sales Receipt";
              $href="#";

	  		}else if($value['type']=="expense_voucher"){
             $type="Expense Voucher";
             $href="#";
	  			
	  		}else if($value['type']=="asset_register"){
             $type="Assets Register";
             $href="#";
	  			
	  		}else if( $value['type']=="asset_tracker" ){
             $type="Assets Tracker";
             $href="#";
	  			
	  		}
	  		else if( $value['type']=="asset_purchase" ){
             $type="Assets Purchase";
             $href="#";
	  			
	  		}
	  		else if( $value['type']=="asset_sales" ){
             $type="Assets Sales";
             $href="#";
	  			
	  		}
	  		else if( $value['type']=="gst_challan" ){
             $type="Tax Payment / GST";
             $href="#";
	  			
	  		}
	  		else if( $value['type']=="prof_tax" ){
             $type="Professional Tax";
             $href="#";
	  			
	  		}
	  		else if($value['type']=="tds_emp"){
             	$type="TDS Employee";
             	$href="#";
	  			
	  		}
	  		else if($value['type']=="equilisation_payment"){
             	$type="EQUILISATION Payment";
             	$href="#";
	  		}
	  		else if($value['type']=="expense_payments"){
             	$type="Expense Payment";
             	$href="#";
	  		}
	  		else{
	  			$type="My Company Profile";
	  			$href=base_url().'profile/edit-company-profile/'.$value['master_id'];
	  		}

	  		$document_size=number_format((float)$value['document_size'], 2, '.', ''); 
	  		$created_date='';
				if($value['created']!='')
				{
					
					$dateTime = date("d-m-Y",  strtotime($value['created']));
					if($value['created']=='0000-00-00'){
						$dateTime='';
					}
					$created_date = str_replace("-",".",$dateTime);
				}
			$po_date='';$s_date='';$e_date='';$rem_type='';
            $dateTime = date("d-m-Y",strtotime($value['po_date']));
            $po_date = str_replace("-",".",$dateTime);

            $dateTime1 = date("d-m-Y",  strtotime($value['start_date']));
            				$s_date = str_replace("-",".",$dateTime1);
            				$dateTime2 = date("d-m-Y",  strtotime($value['end_date']));
            				$e_date = str_replace("-",".",$dateTime2);

			if($value['doc_type']=="legal" && $value['doc_type']=="purchase_order"){

			if($value['po_date']!='')
			{
				
				$dateTime = date("d-m-Y",strtotime($value['po_date']));
				if($value['po_date']=='0000-00-00'){
					$dateTime='';
				}
				$po_date = str_replace("-",".",$dateTime);
			}
			if($value['start_date']!='')
			{
				
				$dateTime = date("d-m-Y",  strtotime($value['start_date']));
				if($value['start_date']=='0000-00-00'){
					$dateTime='';
				}
				$s_date = str_replace("-",".",$dateTime);
			}
			if($value['end_date']!='')
			{
				
				$dateTime = date("d-m-Y",  strtotime($value['end_date']));
				if($value['end_date']=='0000-00-00'){
					$dateTime='';
				}
				$e_date = str_replace("-",".",$dateTime);
			}
		}
			if($value['reminder']=="1"){
				$rem_type="One week before";
			}else if($value['reminder']=="2"){
				$rem_type="Fortnight";
			}else if($value['reminder']=="3"){
				$rem_type="One month before";
			}else if($value['reminder']=="4"){
				$rem_type="Two months before";
			}

			$reminder_data ="";

			if($value['type']!="sales_receipt" && $value['type']!="expense_voucher"){
				$reminder_data .='<a href="#"><img src="'.base_url().'asset/css/img/icons/calendar-purple.png" class="deletes" title=""><img src="'.base_url().'asset/css/img/icons/calendar-green.png" class="deletes" title=""><img src="'.base_url().'asset/css/img/icons/calendar-red.png" class="deletes" title="""><img src="'.base_url().'asset/css/img/icons/alarmclock.png" class="deletes" title=""></a>';
            }

			$contact_person1="";
			
			$action = '';$status='';$cls='';$cls1='';

			if($value['dstatus']!="Active"){
				$status='<li class="activation"><a href="javascript:void(0);" class="activate_credit_debit" data-customer_id='.$value['dc_id'].'><i class="dropdwon-icon icon activate"></i>Activate</a></li>';
				$cls='deactive_record';
				$cls1='deactivate';
			}else{
				$status='<li class="deactivation"><a href="javascript:void(0);" class="deactive_credit_debit" data-customer_id="'.$value['dc_id'].'"><i class="dropdwon-icon icon deactivate"></i>Deactivate</a></li>';
				$cls='active_record';
				$cls1='activate';
			}

			$action  = "<div class='dropdown-button action-more border-radius-3 btn-default default-width no-background ".$cls."' href='#' data-activates='dropdown".$value['dc_id']."'><i class='action-dot'></i></div>
           
			  <ul id='dropdown".$value['dc_id']."' class='dropdown-content'>
					".'<li><a href="javascript:void(0);" onclick="email_document_locker('.$value['dc_id'].','.$doctype.');"><i class="material-icons">email</i>Email</a></li>
				

                    <!--<li><a href='.base_url().'dashboard/download-document-locker/'.$value['dc_id'].' style="display: inline-flex;"><img class="icon-img" src="'.base_url().'public/icons/export.png" style="width: 21px;height: 20px;">Download</a></li>-->


                    <li><a href='.$path.' style="display: inline-flex;" download><img class="icon-img" src="'.base_url().'public/icons/export.png" style="width: 21px; height: 20px;">Download</a></li>
					
					<!--<li><a href='.base_url().'dashboard/print-document-locker/'.$value['dc_id'].' target="_blank"><i class="material-icons">print</i>Print</a></li>-->
					<li style="display:none;"><a href="javascript:void(0);" class="deactive_dcl" data-dcl_id="'.$value['dc_id'].'"><i class="material-icons">delete</i>Delete</a></li></ul>';
					


                if($value['type']=="customer" ) {
					$client_name=$this->Profile_model->selectData('sales_customers', '*', array('cust_id'=>$value['cust_id']));

					if(count($client_name)>0){
						$client= wordwrap($client_name[0]->cust_name,13,"<br>\n");
					}else{
						$client="-";
					}
				}else if ($value['type']=="vendor" ){
					$vendor_name=$this->Profile_model->selectData('expense_vendors', '*', array('vendor_id'=>$value['cust_id']));

					if(count($vendor_name)>0){
						$client= wordwrap($vendor_name[0]->vendor_name,10,"<br>\n");
					}else{
						$client="-";
					}
				}else{

					$client=$clientName;
				}
							
	  		$records["data"][] = array(
				$check,
				$created_date,
				$dc_name,
				$client,
	  			$type,
	  			$document_size,
				$reminder_data,
	  			$action,
	  		);
	  	}

	  	$records["draw"] = intval($post['draw']);
		$records["recordsTotal"] = $iTotalRecords;
		$records["recordsFiltered"] = $iTotalRecords;

		echo json_encode($records);				  
		exit;
	}

	/*---START CUSTOMET PORTAL ACCESS LOGIN---*/
	public function client_login()
	{
		
		$post=$this->input->post();
		if($this->session->userdata('client_session')){
			redirect('client-portal/my-invoices');
		}

		if($post){			
			
			$result=$this->Profile_model->selectData('sales_customers', '*', array('cust_unique_code'=>trim($post['cust_name'])));

	//echo $this->db->last_query();
			if(count($result)>0){

				$cust_id=$result[0]->cust_id;
				$contact=$this->Profile_model->selectData('sales_customer_contacts', '*', array('cp_cust_code'=>$post['cust_name'],'cp_is_admin'=>1,'cp_email'=>$post['cp_email'],'cp_password'=>md5($post['password'])));

				

				if(count($contact)>0){
				//	$this->session->unset_userdata('user_session');
					$this->session->unset_userdata('client_session');
					if($result[0]->cus_portal_access==1){
						$business=$this->Profile_model->selectData('businesslist', '*', array('bus_id'=>$result[0]->bus_id));
		
						$session_data = array(	
							'reg_id' => $result[0]->reg_id,
							'bus_id' => $result[0]->bus_id,
							'gst_id' =>$business[0]->gst_id,
							'cust_id' => $result[0]->cust_id,
							'client_email'	 => $contact[0]->cp_email,
							'client_username' => $contact[0]->cp_name,					
							);	
						
						$this->session->set_userdata('client_session',$session_data);

					if(isset($post['remeber_me']) && $post['remeber_me']=="on"){
						
							$year = time() + 31536000;
							//setcookie('remember_me_u', $post['cp_email'], $year);
							//setcookie('remember_me_pass', $post['password'], $year);
							
							$my_cookie= array(

							   'name'   => 'cust_name',
							   'value'  => $post['cust_name'],                            
							   'expire' => '30000000',                                                                                   
							   'secure' => TRUE

						   );
								$this->input->set_cookie($my_cookie);
								
								$my_cookie1= array(

							   'name'   => 'cp_name',
							   'value'  => $post['cp_name'],                            
							   'expire' => '30000000',                                                                                   
							   'secure' => TRUE

						   );
								$this->input->set_cookie($my_cookie1);
								
								
								
								$my_cookie2= array(

							   'name'   => 'cp_email',
							   'value'  => $post['cp_email'],                            
							   'expire' => '30000000',                                                                                   
							   'secure' => TRUE

						   );
								$this->input->set_cookie($my_cookie2);
								
								$my_cookie3= array(

							   'name'   => 'cp_password',
							   'value'  => $post['password'],                            
							   'expire' => '30000000',                                                                                   
							   'secure' => TRUE

						   );
								$this->input->set_cookie($my_cookie3);

								
						}
							
						//$this->session->set_flashdata('success',"Login Successfully");

						redirect('client-portal/my-invoices');
						}else{
					//echo "Customer has not allow you to access";
					$this->session->set_flashdata('error',"Client has not allow you to access Client Portal");
					}
				}else{
					$this->session->set_flashdata('error',"Your Information is Incorrect..!!");
					//echo "not Primary Contact";
				}

				
			}else{
				//echo "Error";
				$this->session->set_flashdata('error',"Error while processing!");
			}
			
		}
		
		$this->load->view('client_portal/client-login');
		
	}
	/*---END CUSTOMET PORTAL ACCESS LOGIN---*/

	/*---START CUSTOMET PORTAL ACCESS LOGIN---*/
public function vendor_login()
{

	$post=$this->input->post();
	if($this->session->userdata('vendor_session')){
	redirect('vendor-portal/invoices');
	}

		if($post){                                             

		$result=$this->Profile_model->selectData('expense_vendors', '*', array('vendor_unique_code'=>trim($post['vendor_name'])));
		if(count($result)>0){


		$cust_id=$result[0]->vendor_id;
		$contact=$this->Profile_model->selectData('expense_vendor_contacts', '*', array('cp_vendor_code'=>$post['vendor_name'],'cp_is_admin'=>1,'cp_email'=>$post['cp_email'],'cp_password'=>md5($post['password'])));
      


		if(count($contact)>0){
		//$this->session->unset_userdata('user_session');
		//$this->session->unset_userdata('client_session');

		$this->session->unset_userdata('vendor_session');
		if($result[0]->vendor_portal_access==1){


		$business=$this->Profile_model->selectData('businesslist', '*', array('bus_id'=>$result[0]->bus_id));

		$session_data = array(  
		'reg_id' => $result[0]->reg_id,
		'bus_id' => $result[0]->bus_id,
		'gst_id' =>$business[0]->gst_id,

		'vendor_id' => $result[0]->vendor_id,
		'vendor_email'  => $contact[0]->cp_email,
		'vendor_username' => $contact[0]->cp_name,                                                                 
		);             
		$this->session->set_userdata('vendor_session',$session_data);


		if(array_key_exists("remeber_me",$post))
		{
	$year = time() + 31536000;
							//setcookie('remember_me_u', $post['cp_email'], $year);
							//setcookie('remember_me_pass', $post['password'], $year);
							
							$my_cookie= array(

							   'name'   => 'vend_name',
							   'value'  => $post['vendor_name'],                            
							   'expire' => '30000000',                                                                                   
							   'secure' => TRUE

						   );
								$this->input->set_cookie($my_cookie);
								
								$my_cookie1= array(

							   'name'   => 'vend_cp_name',
							   'value'  => $post['cp_name'],                            
							   'expire' => '30000000',                                                                                   
							   'secure' => TRUE

						   );
								$this->input->set_cookie($my_cookie1);
								
								
								$my_cookie2= array(

							   'name'   => 'vend_cp_email',
							   'value'  => $post['cp_email'],                            
							   'expire' => '30000000',                                                                                   
							   'secure' => TRUE

						   );
								$this->input->set_cookie($my_cookie2);
								
								$my_cookie3= array(

							   'name'   => 'vend_password',
							   'value'  => $post['password'],                            
							   'expire' => '30000000',                                                                                   
							   'secure' => TRUE

						   );
								$this->input->set_cookie($my_cookie3);
		}


		//
		$this->session->set_flashdata('success',"Login Successfully");

		redirect('vendor_portal/invoices');
		}else{
		//echo "Customer has not allow you to access";
		$this->session->set_flashdata('error',"Vendor has not allow you to access Vendor Portal");
		}
		}else{
		$this->session->set_flashdata('error',"Your Information is Incorrect..!!");
		//echo "not Primary Contact";
		}


		}else{
		//echo "Error";
		$this->session->set_flashdata('error',"Error while processing!");
		}

		}
		$this->load->view('vendor_portal/vendor_login');

}
 
	/*---END CUSTOMET PORTAL ACCESS LOGIN---*/

	/*~~~~~~~~~~DOCUMENT LOCKER DOWNLOAD / PRINT / DEACTIVE~~~~~~~~~~~~~~~~~~~*/

	public function download_multiple_documents_locker(){
		$array=$this->input->get('ids');
		
		$ids=explode(',', $array);
		
		$i=0;
		foreach($ids as $idl)
		{

			 $path="";
			$doc_type= explode('-',$idl);
			
			 $type = $doc_type[1];
			 $id=$doc_type[0];

			if($type==1){
				$doc_name=$this->Profile_model->selectData('legal_document', '*', array('legal_id'=>$id));
				
				if($doc_name[0]->legal_type == 'company')
				{
            	 	$path=DOC_ROOT.'/public/upload/company_legal_documents/'.$doc_name[0]->legal_document;
				}
				if($doc_name[0]->legal_type == 'customer')
				{
					$path=DOC_ROOT.'/public/upload/sales/customer/customer_legal_documents/'.$doc_name[0]->bus_id.'/'.$doc_name[0]->legal_document;
				}

				if($doc_name[0]->legal_type == 'vendor')
				{
					$path=DOC_ROOT.'/public/upload/expense/vendor/vendor_legal_documents/'.$doc_name[0]->bus_id.'/'.$doc_name[0]->legal_document;
				}

				$rec_info = $this->Profile_model->selectData('legal_document', '*',array('legal_id'=>$id));
				 if($rec_info)
				 {
				 	    $tracking_info['module_name'] = 'Document Locker';
				 		$tracking_info['action_taken'] = 'Downloaded';
				 		$tracking_info['reference'] = $rec_info[0]->legal_document;
				 		$LogRec = $this->Adminmaster_model->insertActivity($tracking_info);

				 }
			}

			if($type==2){

				$doc_name=$this->Profile_model->selectData('legal_document', '*', array('legal_id'=>$id));
			  	if($doc_name[0]->legal_type == 'company')
				{
              		$path=DOC_ROOT.'/public/upload/company_po_documents/'.$doc_name[0]->legal_document;
              	}
              	if($doc_name[0]->legal_type == 'customer')
				{
					$path=DOC_ROOT.'/public/upload/sales/customer/customer_po_documents/'.$doc_name[0]->bus_id.'/'.$doc_name[0]->legal_document;
				}

				if($doc_name[0]->legal_type == 'vendor')
				{
					$path=DOC_ROOT.'/public/upload/expense/vendor/vendor_po_documents/'.$doc_name[0]->bus_id.'/'.$doc_name[0]->legal_document;
				}

				$rec_info = $this->Profile_model->selectData('legal_document', '*',array('legal_id'=>$id));
				 if($rec_info)
				 {
				 	    $tracking_info['module_name'] = 'Document Locker';
				 		$tracking_info['action_taken'] = 'Downloaded';
				 		$tracking_info['reference'] = $rec_info[0]->legal_document;
				 		$LogRec = $this->Adminmaster_model->insertActivity($tracking_info);

				 }

			}

			if($type==3){
				$doc_name=$this->Profile_model->selectData('other_document', '*', array('other_doc_id'=>$id));
				if($doc_name[0]->other_type == 'company')
				{
					$path=DOC_ROOT.'/public/upload/company_other_documents/'.$doc_name[0]->other_doc_name;
				}
				if($doc_name[0]->other_type == 'customer')
				{
					$path=DOC_ROOT.'/public/upload/sales/customer/customer_other_documents/'.$doc_name[0]->bus_id.'/'.$doc_name[0]->other_doc_name;
				}

				if($doc_name[0]->other_type == 'vendor')
				{
					$path=DOC_ROOT.'/public/upload/expense/vendor/vendor_other_documents/'.$doc_name[0]->bus_id.'/'.$doc_name[0]->other_doc_name;
				}
				$rec_info = $this->Profile_model->selectData('other_document', '*',array('other_doc_id'=>$id));
			 	if($rec_info)
			 	{
			 		$tracking_info['module_name'] = 'Document Locker';
			 		$tracking_info['action_taken'] = 'Downloaded';
			 		$tracking_info['reference'] = $rec_info[0]->other_doc_name;
			 		$LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
			 	}
			}

			if($type==4){

				$doc_name=$this->Profile_model->selectData('other_document', '*', array('other_doc_id'=>$id));
				if($doc_name[0]->other_type == 'company')
				{
					$path=DOC_ROOT.'/public/upload/company_moa_documents/'.$doc_name[0]->other_doc_name;
				}
				if($doc_name[0]->other_type == 'customer')
				{
					$path=DOC_ROOT.'/public/upload/sales/customer/customer_other_documents/'.$doc_name[0]->bus_id.'/'.$doc_name[0]->other_doc_name;
				}

				if($doc_name[0]->other_type == 'vendor')
				{
					$path=DOC_ROOT.'/public/upload/expense/vendor/vendor_other_documents/'.$doc_name[0]->bus_id.'/'.$doc_name[0]->other_doc_name;
				}


				$rec_info = $this->Profile_model->selectData('other_document', '*',array('other_doc_id'=>$id));
			 	if($rec_info)
			 	{
			 		$tracking_info['module_name'] = 'Document Locker';
			 		$tracking_info['action_taken'] = 'Downloaded';
			 		$tracking_info['reference'] = $rec_info[0]->other_doc_name;
			 		$LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
			 	}
				
			}

			if($type==5){

				$doc_name=$this->Profile_model->selectData('srf_document', '*', array('srf_id'=>$id));
				if($doc_name[0]->srf_type == 'sales_receipt')
				{
					$path=DOC_ROOT.'/public/upload/receipts_payments/sales_receipt/'.$doc_name[0]->parent_id.'/'.$doc_name[0]->srf_document;
					
				}
				
				$rec_info = $this->Profile_model->selectData('srf_document', '*',array('srf_id'=>$id));
			 	if($rec_info)
			 	{
			 		$tracking_info['module_name'] = 'Document Locker';
			 		$tracking_info['action_taken'] = 'Downloaded';
			 		$tracking_info['reference'] = $rec_info[0]->srf_document;
			 		$LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
			 	}
				
			}

			if($type==6){

				$doc_name=$this->Profile_model->selectData('srf_document', '*', array('srf_id'=>$id));
				if($doc_name[0]->srf_type == 'expense_voucher')
				{
					$path=DOC_ROOT.'/public/upload/expense_voucher_images/'.$doc_name[0]->parent_id.'/'.$doc_name[0]->srf_document;
					
				}
				
				$rec_info = $this->Profile_model->selectData('srf_document', '*',array('srf_id'=>$id));
			 	if($rec_info)
			 	{
			 		$tracking_info['module_name'] = 'Document Locker';
			 		$tracking_info['action_taken'] = 'Downloaded';
			 		$tracking_info['reference'] = $rec_info[0]->srf_document;
			 		$LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
			 	}
				
			}

			if($type==7){

				$doc_name=$this->Profile_model->selectData('srf_document', '*', array('srf_id'=>$id));
				if($doc_name[0]->srf_type == 'asset_purchase')
				{
					$path=DOC_ROOT.'/public/upload/asset_purchase/'.$doc_name[0]->parent_id.'/'.$doc_name[0]->srf_document;
					
				}
				
				$rec_info = $this->Profile_model->selectData('srf_document', '*',array('srf_id'=>$id));
			 	if($rec_info)
			 	{
			 		$tracking_info['module_name'] = 'Document Locker';
			 		$tracking_info['action_taken'] = 'Downloaded';
			 		$tracking_info['reference'] = $rec_info[0]->srf_document;
			 		$LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
			 	}
				
			}

			if($type==8){

				$doc_name=$this->Profile_model->selectData('srf_document', '*', array('srf_id'=>$id));
				if($doc_name[0]->srf_type == 'asset_tracker')
				{
					$path=DOC_ROOT.'/public/upload/asset_tracker/'.$doc_name[0]->parent_id.'/'.$doc_name[0]->srf_document;
					
				}
				
				$rec_info = $this->Profile_model->selectData('srf_document', '*',array('srf_id'=>$id));
			 	if($rec_info)
			 	{
			 		$tracking_info['module_name'] = 'Document Locker';
			 		$tracking_info['action_taken'] = 'Downloaded';
			 		$tracking_info['reference'] = $rec_info[0]->srf_document;
			 		$LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
			 	}
				
			}

			if($type==9){

				$doc_name=$this->Profile_model->selectData('srf_document', '*', array('srf_id'=>$id));
				if($doc_name[0]->srf_type == 'asset_sales')
				{
					$path=DOC_ROOT.'/public/upload/asset_sales/'.$doc_name[0]->parent_id.'/'.$doc_name[0]->srf_document;
					
				}
				
				$rec_info = $this->Profile_model->selectData('srf_document', '*',array('srf_id'=>$id));
			 	if($rec_info)
			 	{
			 		$tracking_info['module_name'] = 'Document Locker';
			 		$tracking_info['action_taken'] = 'Downloaded';
			 		$tracking_info['reference'] = $rec_info[0]->srf_document;
			 		$LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
			 	}
				
			}

			if($type==10){

				$doc_name=$this->Profile_model->selectData('srf_document', '*', array('srf_id'=>$id));
				if($doc_name[0]->srf_type == 'company_expense')
				{
					$path=DOC_ROOT.'/public/upload/company_expense/'.$doc_name[0]->parent_id.'/'.$doc_name[0]->srf_document;
					
				}
				$rec_info = $this->Profile_model->selectData('srf_document', '*',array('srf_id'=>$id));
			 	if($rec_info)
			 	{
			 		$tracking_info['module_name'] = 'Document Locker';
			 		$tracking_info['action_taken'] = 'Downloaded';
			 		$tracking_info['reference'] = $rec_info[0]->srf_document;
			 		$LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
			 	}
				
			}

				if($type==11){

				$doc_name=$this->Profile_model->selectData('srf_document', '*', array('srf_id'=>$id));
				if($doc_name[0]->srf_type == 'gst_challan')
				{
					$path=DOC_ROOT.'/public/upload/gst_challan_img/'.$doc_name[0]->parent_id.'/'.$doc_name[0]->srf_document;
					
				}
				$rec_info = $this->Profile_model->selectData('srf_document', '*',array('srf_id'=>$id));
			 	if($rec_info)
			 	{
			 		$tracking_info['module_name'] = 'Document Locker';
			 		$tracking_info['action_taken'] = 'Downloaded';
			 		$tracking_info['reference'] = $rec_info[0]->srf_document;
			 		$LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
			 	}
				
			}

				if($type==12){

				$doc_name=$this->Profile_model->selectData('srf_document', '*', array('srf_id'=>$id));
				if($doc_name[0]->srf_type == 'prof_tax')
				{
					$path=DOC_ROOT.'/public/upload/prof_tax/'.$doc_name[0]->parent_id.'/'.$doc_name[0]->srf_document;
					
				}

				$rec_info = $this->Profile_model->selectData('srf_document', '*',array('srf_id'=>$id));
			 	if($rec_info)
			 	{
			 		$tracking_info['module_name'] = 'Document Locker';
			 		$tracking_info['action_taken'] = 'Downloaded';
			 		$tracking_info['reference'] = $rec_info[0]->srf_document;
			 		$LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
			 	}
				
			}



				if($type==13){

				$doc_name=$this->Profile_model->selectData('srf_document', '*', array('srf_id'=>$id));
				if($doc_name[0]->srf_type == 'tds_emp')
				{
					$path=DOC_ROOT.'/public/upload/tds_emp/'.$doc_name[0]->parent_id.'/'.$doc_name[0]->srf_document;
					
				}
				
				$rec_info = $this->Profile_model->selectData('srf_document', '*',array('srf_id'=>$id));
			 	if($rec_info)
			 	{
			 		$tracking_info['module_name'] = 'Document Locker';
			 		$tracking_info['action_taken'] = 'Downloaded';
			 		$tracking_info['reference'] = $rec_info[0]->srf_document;
			 		$LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
			 	}
				
			}

            if($type==14){

				$doc_name=$this->Profile_model->selectData('srf_document', '*', array('srf_id'=>$id));
				if($doc_name[0]->srf_type == 'asset_register')
				{
					$path=DOC_ROOT.'/public/upload/asset_register/'.$doc_name[0]->parent_id.'/'.$doc_name[0]->srf_document;
					
				}
				
				$rec_info = $this->Profile_model->selectData('srf_document', '*',array('srf_id'=>$id));
			 	if($rec_info)
			 	{
			 		$tracking_info['module_name'] = 'Document Locker';
			 		$tracking_info['action_taken'] = 'Downloaded';
			 		$tracking_info['reference'] = $rec_info[0]->srf_document;
			 		$LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
			 	}
				
			}

			 if($type==15){

				$doc_name=$this->Profile_model->selectData('srf_document', '*', array('srf_id'=>$id));
				if($doc_name[0]->srf_type == 'equilisation_payment
')
				{
					$path=DOC_ROOT.'/public/upload/equi_levy_payment/'.$doc_name[0]->parent_id.'/'.$doc_name[0]->srf_document;
					
				}
				
			$rec_info = $this->Profile_model->selectData('srf_document', '*',array('srf_id'=>$id));
			 	if($rec_info)
			 	{
			 		$tracking_info['module_name'] = 'Document Locker';
			 		$tracking_info['action_taken'] = 'Downloaded';
			 		$tracking_info['reference'] = $rec_info[0]->srf_document;
			 		$LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
			 	}
				
			}

            
			if($type==16){

     $doc_name=$this->Profile_model->selectData('srf_document', '*', array('srf_id'=>$id));
				if($doc_name[0]->srf_type == 'expense_payments')
				{
					$path=DOC_ROOT.'/public/upload/receipts_payments/expense_payments/'.$doc_name[0]->parent_id.'/'.$doc_name[0]->srf_document;
					
				}
			
			$rec_info = $this->Profile_model->selectData('srf_document', '*',array('srf_id'=>$id));
			 	if($rec_info)
			 	{
			 		$tracking_info['module_name'] = 'Document Locker';
			 		$tracking_info['action_taken'] = 'Downloaded';
			 		$tracking_info['reference'] = $rec_info[0]->srf_document;
			 		$LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
			 	}
				
			}



			

			$invoname ="Document Locker";
			//$Attach[$i] = $path;

			$this->zip->read_file($path); 

			 $i++;

		}
     
		$doc_zip = "Documents_".time().".zip";
		$this->zip->download($doc_zip);

	}

	public function print_multiple_documents_locker(){
		$array=$this->input->get('ids');
		$array=explode(',', $array);
		foreach($array as $id)
		{
			// Activity Tracker start
			 $rec_info = $this->Profile_model->selectData('legal_document', '*',array('legal_id'=>$id));
			 if($rec_info)
			 {
			 	    $tracking_info['module_name'] = 'Document Locker';
			 		$tracking_info['action_taken'] = 'Printed';
			 		$tracking_info['reference'] = $rec_info[0]->legal_document;
			 		$LogRec = $this->Adminmaster_model->insertActivity($tracking_info);

			 }else{
			 	$rec_info = $this->Profile_model->selectData('other_document', '*',array('other_doc_id'=>$id));
			 	if($rec_info)
			 	{
			 		$tracking_info['module_name'] = 'Document Locker';
			 		$tracking_info['action_taken'] = 'Printed';
			 		$tracking_info['reference'] = $rec_info[0]->other_doc_name;
			 		$LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
			 	}

			 }
			 
			// Activity Tracker End
		}

		$reg_id = $this->user_session['reg_id'];
		$bus_id = $this->user_session['bus_id'];
		$data['result'] = $this->User_model->download_multiple_documents_locker($array);
		$this->load->view('dashboard/document_locker/print_all_document_locker',$data);
	}
	public function download_document_locker($array) {
		$reg_id = $this->user_session['reg_id'];
		$bus_id = $this->user_session['bus_id'];
			// Activity Tracker start
			 $rec_info = $this->Profile_model->selectData('legal_document', '*',array('legal_id'=>$array));
			 if($rec_info)
			 {
			 	    $tracking_info['module_name'] = 'Document Locker';
			 		$tracking_info['action_taken'] = 'Downloaded';
			 		$tracking_info['reference'] = $rec_info[0]->legal_document;
			 		$LogRec = $this->Adminmaster_model->insertActivity($tracking_info);

			 }else{
			 	$rec_info = $this->Profile_model->selectData('other_document', '*',array('other_doc_id'=>$array));
			 	if($rec_info)
			 	{
			 		$tracking_info['module_name'] = 'Document Locker';
			 		$tracking_info['action_taken'] = 'Downloaded';
			 		$tracking_info['reference'] = $rec_info[0]->other_doc_name;
			 		$LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
			 	}

			 }
			 
			// Activity Tracker End
		$data['result'] = $this->User_model->download_multiple_documents_locker($array,'1');
		$this->load->view('dashboard/document_locker/download_document_locker',$data);
	}
	public function print_document_locker($array){
		$reg_id = $this->user_session['reg_id'];
		$bus_id = $this->user_session['bus_id'];
			// Activity Tracker start
			 $rec_info = $this->Profile_model->selectData('legal_document', '*',array('legal_id'=>$array));
			 if($rec_info)
			 {
			 	    $tracking_info['module_name'] = 'Document Locker';
			 		$tracking_info['action_taken'] = 'Printed';
			 		$tracking_info['reference'] = $rec_info[0]->legal_document;
			 		$LogRec = $this->Adminmaster_model->insertActivity($tracking_info);

			 }else{
			 	$rec_info = $this->Profile_model->selectData('other_document', '*',array('other_doc_id'=>$array));
			 	if($rec_info)
			 	{
			 		$tracking_info['module_name'] = 'Document Locker';
			 		$tracking_info['action_taken'] = 'Printed';
			 		$tracking_info['reference'] = $rec_info[0]->other_doc_name;
			 		$LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
			 	}

			 }
			 
			// Activity Tracker End
		$data['result'] = $this->User_model->download_multiple_documents_locker($array,'1');
		$this->load->view('dashboard/document_locker/print_all_document_locker',$data);
	}
	public function email_multiple_locker(){
		$array=$this->input->post('alerts_values');
		$array=explode(',', $array);
		foreach($array as $id)
		{
			// Activity Tracker start
			 $rec_info = $this->Profile_model->selectData('legal_document', '*',array('legal_id'=>$id));
			 if($rec_info)
			 {
			 	    $tracking_info['module_name'] = 'Document Locker';
			 		$tracking_info['action_taken'] = 'Emailed';
			 		$tracking_info['reference'] = $rec_info[0]->legal_document;
			 		$LogRec = $this->Adminmaster_model->insertActivity($tracking_info);

			 }else{
			 	$rec_info = $this->Profile_model->selectData('other_document', '*',array('other_doc_id'=>$id));
			 	if($rec_info)
			 	{
			 		$tracking_info['module_name'] = 'Document Locker';
			 		$tracking_info['action_taken'] = 'Emailed';
			 		$tracking_info['reference'] = $rec_info[0]->other_doc_name;
			 		$LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
			 	}

			 }
			 
			// Activity Tracker End
		}
		$reg_id=$this->user_session['reg_id'];
		$bus_id=$this->user_session['bus_id'];
		$data['result'] = $this->User_model->download_multiple_documents_locker($array);
		$htmldata = $this->load->view('dashboard/document_locker/print_all_document_locker',$data,true);
		$invoname ="Document Locker";
		$msg ="Hey ".ucwords($this->user_session['ei_username']).", Congratulations! You have successfully create Document Locker.";
		$output=$this->pdf->create($htmldata,$invoname.'.pdf','email');
		sendInvoiceEmail($this->user_session['email'],'Your Document Locker has been created',$msg,$output,$invoname); 
	} 
	public function email_document_locker() {
		
		// exit();
		$post_data = $_POST;
		//$id = $post_data['doc_id'];

		$reg_id = $this->user_session['reg_id'];
		$bus_id = $this->user_session['bus_id'];

		$ids = explode(',',$post_data['doc_id']);

		$i=0;
		foreach($ids as $idl)
		{

			 $path="";
			$doc_type= explode('-',$idl);
			 $type = $doc_type[1];
			 $id=$doc_type[0];

			if($type==1){
				$doc_name=$this->Profile_model->selectData('legal_document', '*', array('legal_id'=>$ids[$i]));
				
				if($doc_name[0]->legal_type == 'company')
				{
            	 	$path=DOC_ROOT.'/public/upload/company_legal_documents/'.$doc_name[0]->legal_document;
				}
				if($doc_name[0]->legal_type == 'customer')
				{
					$path=DOC_ROOT.'/public/upload/sales/customer/customer_legal_documents/'.$doc_name[0]->bus_id.'/'.$doc_name[0]->legal_document;
				}

				if($doc_name[0]->legal_type == 'vendor')
				{
					$path=DOC_ROOT.'/public/upload/expense/vendor/vendor_legal_documents/'.$doc_name[0]->bus_id.'/'.$doc_name[0]->legal_document;
				}
			}

			if($type==2){

				$doc_name=$this->Profile_model->selectData('legal_document', '*', array('legal_id'=>$ids[$i]));
			  	if($doc_name[0]->legal_type == 'company')
				{
              		$path=DOC_ROOT.'/public/upload/company_po_documents/'.$doc_name[0]->legal_document;
              	}
              	if($doc_name[0]->legal_type == 'customer')
				{
					$path=DOC_ROOT.'/public/upload/sales/customer/customer_po_documents/'.$doc_name[0]->bus_id.'/'.$doc_name[0]->legal_document;
				}
				if($doc_name[0]->legal_type == 'vendor')
				{
					$path=DOC_ROOT.'/public/upload/expense/vendor/vendor_po_documents/'.$doc_name[0]->bus_id.'/'.$doc_name[0]->legal_document;
				}

			}

			if($type==3){
				$doc_name=$this->Profile_model->selectData('other_document', '*', array('other_doc_id'=>$ids[$i]));
				if($doc_name[0]->other_type == 'company')
				{
					$path=DOC_ROOT.'/public/upload/company_other_documents/'.$doc_name[0]->other_doc_name;
				}
				if($doc_name[0]->other_type == 'customer')
				{
					$path=DOC_ROOT.'/public/upload/sales/customer/customer_other_documents/'.$doc_name[0]->bus_id.'/'.$doc_name[0]->other_doc_name;
				}
				if($doc_name[0]->legal_type == 'vendor')
				{
					$path=DOC_ROOT.'/public/upload/expense/vendor/vendor_other_documents/'.$doc_name[0]->bus_id.'/'.$doc_name[0]->legal_document;
				}
			}

			if($type==4){

				$doc_name=$this->Profile_model->selectData('other_document', '*', array('other_doc_id'=>$ids[$i]));
				if($doc_name[0]->other_type == 'company')
				{
					$path=DOC_ROOT.'/public/upload/company_moa_documents/'.$doc_name[0]->other_doc_name;
				}
				if($doc_name[0]->other_type == 'customer')
				{
					$path=DOC_ROOT.'/public/upload/sales/customer/customer_other_documents/'.$doc_name[0]->bus_id.'/'.$doc_name[0]->other_doc_name;
				}
				
			}

			if($type==5){

				$doc_name=$this->Profile_model->selectData('srf_document', '*', array('srf_id'=>$ids[$i]));
				if($doc_name[0]->srf_type == 'sales_receipt')
				{
					$path=DOC_ROOT.'/public/upload/receipts_payments/sales_receipt/'.$doc_name[0]->parent_id.'/'.$doc_name[0]->srf_document;
					
				}
				
				$rec_info = $this->Profile_model->selectData('srf_document', '*',array('srf_id'=>$ids[$i]));
			 	
				
			}

			if($type==6){

				$doc_name=$this->Profile_model->selectData('srf_document', '*', array('srf_id'=>$ids[$i]));
				if($doc_name[0]->srf_type == 'expense_voucher')
				{
					$path=DOC_ROOT.'/public/upload/expense_voucher_images/'.$doc_name[0]->parent_id.'/'.$doc_name[0]->srf_document;
					
				}
				
				
				
			}

			if($type==7){

				$doc_name=$this->Profile_model->selectData('srf_document', '*', array('srf_id'=>$ids[$i]));
				if($doc_name[0]->srf_type == 'asset_purchase')
				{
					$path=DOC_ROOT.'/public/upload/asset_purchase/'.$doc_name[0]->parent_id.'/'.$doc_name[0]->srf_document;
					
				}
				
				$rec_info = $this->Profile_model->selectData('srf_document', '*',array('srf_id'=>$ids[$i]));
			 	if($rec_info)
			 	{
			 		$tracking_info['module_name'] = 'Document Locker';
			 		$tracking_info['action_taken'] = 'Emailed';
			 		$tracking_info['reference'] = $rec_info[0]->srf_document;
			 		$LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
			 	}
				
			}

			if($type==8){

				$doc_name=$this->Profile_model->selectData('srf_document', '*', array('srf_id'=>$ids[$i]));
				if($doc_name[0]->srf_type == 'asset_tracker')
				{
					$path=DOC_ROOT.'/public/upload/asset_tracker/'.$doc_name[0]->parent_id.'/'.$doc_name[0]->srf_document;
					
				}
				
				$rec_info = $this->Profile_model->selectData('srf_document', '*',array('srf_id'=>$ids[$i]));
			 	if($rec_info)
			 	{
			 		$tracking_info['module_name'] = 'Document Locker';
			 		$tracking_info['action_taken'] = 'Emailed';
			 		$tracking_info['reference'] = $rec_info[0]->srf_document;
			 		$LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
			 	}
				
			}

			if($type==9){

				$doc_name=$this->Profile_model->selectData('srf_document', '*', array('srf_id'=>$ids[$i]));
				if($doc_name[0]->srf_type == 'asset_sales')
				{
					$path=DOC_ROOT.'/public/upload/asset_sales/'.$doc_name[0]->parent_id.'/'.$doc_name[0]->srf_document;
					
				}
				
				$rec_info = $this->Profile_model->selectData('srf_document', '*',array('srf_id'=>$ids[$i]));
			 	if($rec_info)
			 	{
			 		$tracking_info['module_name'] = 'Document Locker';
			 		$tracking_info['action_taken'] = 'Emailed';
			 		$tracking_info['reference'] = $rec_info[0]->srf_document;
			 		$LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
			 	}
				
			}

			if($type==10){

				$doc_name=$this->Profile_model->selectData('srf_document', '*', array('srf_id'=>$ids[$i]));
				if($doc_name[0]->srf_type == 'company_expense')
				{
					$path=DOC_ROOT.'/public/upload/company_expense/'.$doc_name[0]->parent_id.'/'.$doc_name[0]->srf_document;
					
				}
				
				$rec_info = $this->Profile_model->selectData('srf_document', '*',array('srf_id'=>$ids[$i]));
			 	if($rec_info)
			 	{
			 		$tracking_info['module_name'] = 'Document Locker';
			 		$tracking_info['action_taken'] = 'Emailed';
			 		$tracking_info['reference'] = $rec_info[0]->srf_document;
			 		$LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
			 	}
				
			}

			if($type==11){

				$doc_name=$this->Profile_model->selectData('srf_document', '*', array('srf_id'=>$ids[$i]));
				if($doc_name[0]->srf_type == 'gst_challan')
				{
					$path=DOC_ROOT.'/public/upload/gst_challan_img/'.$doc_name[0]->parent_id.'/'.$doc_name[0]->srf_document;
					
				}
				
				$rec_info = $this->Profile_model->selectData('srf_document', '*',array('srf_id'=>$ids[$i]));
			 	if($rec_info)
			 	{
			 		$tracking_info['module_name'] = 'Document Locker';
			 		$tracking_info['action_taken'] = 'Emailed';
			 		$tracking_info['reference'] = $rec_info[0]->srf_document;
			 		$LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
			 	}
				
			}

			if($type==12){

				$doc_name=$this->Profile_model->selectData('srf_document', '*', array('srf_id'=>$ids[$i]));
				if($doc_name[0]->srf_type == 'prof_tax')
				{
					$path=DOC_ROOT.'/public/upload/prof_tax/'.$doc_name[0]->parent_id.'/'.$doc_name[0]->srf_document;
					
				}
				
				$rec_info = $this->Profile_model->selectData('srf_document', '*',array('srf_id'=>$ids[$i]));
			 	if($rec_info)
			 	{
			 		$tracking_info['module_name'] = 'Document Locker';
			 		$tracking_info['action_taken'] = 'Emailed';
			 		$tracking_info['reference'] = $rec_info[0]->srf_document;
			 		$LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
			 	}
				
			}

			if($type==13){

				$doc_name=$this->Profile_model->selectData('srf_document', '*', array('srf_id'=>$ids[$i]));
				if($doc_name[0]->srf_type == 'tds_emp')
				{
					$path=DOC_ROOT.'/public/upload/tds_emp/'.$doc_name[0]->parent_id.'/'.$doc_name[0]->srf_document;
					
				}
				
				$rec_info = $this->Profile_model->selectData('srf_document', '*',array('srf_id'=>$ids[$i]));
			 	if($rec_info)
			 	{
			 		$tracking_info['module_name'] = 'Document Locker';
			 		$tracking_info['action_taken'] = 'Emailed';
			 		$tracking_info['reference'] = $rec_info[0]->srf_document;
			 		$LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
			 	}
				
			}

			if($type==14){

				$doc_name=$this->Profile_model->selectData('srf_document', '*', array('srf_id'=>$ids[$i]));
				if($doc_name[0]->srf_type == 'asset_register')
				{
					$path=DOC_ROOT.'/public/upload/asset_register/'.$doc_name[0]->parent_id.'/'.$doc_name[0]->srf_document;
					
				}
				
				$rec_info = $this->Profile_model->selectData('srf_document', '*',array('srf_id'=>$ids[$i]));
			 	if($rec_info)
			 	{
			 		$tracking_info['module_name'] = 'Document Locker';
			 		$tracking_info['action_taken'] = 'Emailed';
			 		$tracking_info['reference'] = $rec_info[0]->srf_document;
			 		$LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
			 	}
				
			}

			if($type==15){

				$doc_name=$this->Profile_model->selectData('srf_document', '*', array('srf_id'=>$ids[$i]));
				if($doc_name[0]->srf_type == 'equilisation_payment
')
				{
					$path=DOC_ROOT.'/public/upload/equi_levy_payment/'.$doc_name[0]->parent_id.'/'.$doc_name[0]->srf_document;
					
				}
				
				$rec_info = $this->Profile_model->selectData('srf_document', '*',array('srf_id'=>$ids[$i]));
			 	if($rec_info)
			 	{
			 		$tracking_info['module_name'] = 'Document Locker';
			 		$tracking_info['action_taken'] = 'Emailed';
			 		$tracking_info['reference'] = $rec_info[0]->srf_document;
			 		$LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
			 	}
				
			}


			if($type==16){

				$doc_name=$this->Profile_model->selectData('srf_document', '*', array('srf_id'=>$ids[$i]));
				if($doc_name[0]->srf_type == 'expense_payments')
				{
					$path=DOC_ROOT.'/public/upload/receipts_payments/expense_payments/'.$doc_name[0]->parent_id.'/'.$doc_name[0]->srf_document;
					
				}
				
				$rec_info = $this->Profile_model->selectData('srf_document', '*',array('srf_id'=>$ids[$i]));
			 	if($rec_info)
			 	{
			 		$tracking_info['module_name'] = 'Document Locker';
			 		$tracking_info['action_taken'] = 'Emailed';
			 		$tracking_info['reference'] = $rec_info[0]->srf_document;
			 		$LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
			 	}
				
			}
			//echo $path;
			$fromemail = $this->Sales_model->selectData('businesslist','*',array('bus_id'=>$bus_id,'reg_id'=>$reg_id,'status'=>'Active'));
			$invoname ="Document Locker";
			$Attach[$i] = $path;
			$i++;

		}

		
		$cc = $post_data['email_cc'];
		$cc = preg_split("/(,|;)/", $cc);
		$message = $post_data['email_message'];
		
		$all_emails =  $post_data['email_to'];
		$to = preg_split("/(,|;)/", $all_emails);
		$from = $fromemail[0]->bus_company_name;
		$subject = $post_data['email_subject'];
		
		$status = sendInvoiceEmailDoclocker($from,$to,$subject,$message,$Attach,$invoname,$cc);

		if($status)
		{
			echo 1;
		}
		else{

			echo 0;
		}
		//sendInvoiceEmail($this->user_session['email'],'Your Document Locker has been created',$msg,$output,$invoname); 

	}

	public function deactive_document_locker()
	{
		$id = $this->input->post('dcl_id');
		
		// Activity Tracker start
			 $rec_info = $this->Profile_model->selectData('legal_document', '*',array('legal_id'=>$id));
			 if($rec_info)
			 {
			 	    $tracking_info['module_name'] = 'Document Locker';
			 		$tracking_info['action_taken'] = 'Deleted';
			 		$tracking_info['reference'] = $rec_info[0]->legal_document;
			 		$LogRec = $this->Adminmaster_model->insertActivity($tracking_info);

			 }else{
			 	$rec_info = $this->Profile_model->selectData('other_document', '*',array('other_doc_id'=>$id));
			 	if($rec_info)
			 	{
			 		$tracking_info['module_name'] = 'Document Locker';
			 		$tracking_info['action_taken'] = 'Deleted';
			 		$tracking_info['reference'] = $rec_info[0]->other_doc_name;
			 		$LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
			 	}

			 }
			 
			// Activity Tracker End
		$filter = array(
			'status'	=>	$this->input->post('status'),
		);
		$this->load->model('Sales_model');
		$flag=$this->Sales_model->updateData('legal_document',$filter,array('legal_id'=>$id));

		if($flag != false)
		{
			echo true;
		}
		else
		{
			echo false;
		}
	}
	function deactive_multiple_docl()
	{
		$dcl_values = $this->input->post('docl_values');	
		$dcl_values_array = explode(",",$dcl_values);
		$this->load->model('Sales_model');
		foreach($dcl_values_array as $id)
		{
			// Activity Tracker start
			 $rec_info = $this->Profile_model->selectData('legal_document', '*',array('legal_id'=>$id));
			 if($rec_info)
			 {
			 	    $tracking_info['module_name'] = 'Document Locker';
			 		$tracking_info['action_taken'] = 'Deleted';
			 		$tracking_info['reference'] = $rec_info[0]->legal_document;
			 		$LogRec = $this->Adminmaster_model->insertActivity($tracking_info);

			 }else{
			 	$rec_info = $this->Profile_model->selectData('other_document', '*',array('other_doc_id'=>$id));
			 	if($rec_info)
			 	{
			 		$tracking_info['module_name'] = 'Document Locker';
			 		$tracking_info['action_taken'] = 'Deleted';
			 		$tracking_info['reference'] = $rec_info[0]->other_doc_name;
			 		$LogRec = $this->Adminmaster_model->insertActivity($tracking_info);
			 	}

			 }
			 
			// Activity Tracker End
		}
		foreach($dcl_values_array as $id)
		{
			
			$filter = array(
				'status'	=>	$this->input->post('status'),
			);
			$flag=$this->Sales_model->updateData('legal_document',$filter,array('legal_id'=>$id));	
		}	
		
		echo true;
	}
	/*~~~~~~~~~~DOCUMENT LOCKER ALERTS DOWNLOAD / PRINT / DEACTIVE~~~~~~~~~~~~~~~~~~~*/



	
}

?>