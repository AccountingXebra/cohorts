<?php 
    
    function public_path($type="www")
    {
        return base_url()."public/";
    }

    function base_path($type="www")
    {
      return base_url();
    }

    function index_path($type="www")
    {
      return base_url()."index/";
    }

    function admin_path($type="www")
    {
      return base_url()."admin/";
    }

    function profile_img_path($type="www")
    {
        return base_url()."public/uploads/profile_images/";
    }
    function upload_path($type="www")
    {
        return base_url()."public/uploads/";
    }

    function translate($text)
    {
      $CI =& get_instance();
      $line=$CI->lang->line($text);
      if(empty($line))
      {
        $line=$text;
      }
      
      return $line;
    }

    function is_login()
    {

      $CI =& get_instance();
$referrer_value = current_url().($_SERVER['QUERY_STRING']!=""?"?".$_SERVER['QUERY_STRING']:"");
$CI->session->set_userdata('login_referrer', $referrer_value);


     
      $session = $CI->session->userdata('user_session');
     // print_r($session);
      if (!isset($session['reg_id'])) {
        redirect(base_url().'login');
      }
     
     
    }

    function is_login_admin_panel()
    {

      $CI =& get_instance();
      $session = $CI->session->userdata('admin_session');
      if (!isset($session['admin_id'])) {
        redirect(base_url().'admin-dashboard');
      }
    }

    function is_trialExpire()
    {

      $CI =& get_instance();
      $session = $CI->session->userdata('user_session');
      if (isset($session['reg_trial_expire'])) {
        redirect(base_url().'subscription/my-subscription');
      }else if(isset($session['bus_id']) && $session['bus_id']==0){
      // redirect(base_url().'profile/add_company_profile');
      }else{
       //
      }
    }

     function is_Busreg()
    {

      $CI =& get_instance();
      $session = $CI->session->userdata('user_session');

      if (isset($session['bus_id']) && $session['bus_id']==0 && @!$session['reg_trial_expire']) {
        //$this->session->set_flashdata('error',"Please set Company profile and GSTIN Number First..!!");
        redirect(base_url().'profile/add_company_profile');
      }
    }

    function is_accessAvail($module)
    {

      $CI =& get_instance();
      $session = $CI->session->userdata('user_session');

      $user_type = $session['reg_admin_type'];

      $owner = array(1,2,3,4,5,6,7,8,9,13);
      $accountant = array(1,2,3,4,5,6,8,9,13);
      $hr = array(1,2,3,4,5,6,8,9,13);
      $ca = array(1,4,5,9);
      $admin = array(1,2,3,4,5,6,8,9,13);
      $employee = array(1,5,13);
      $client = array(4,8,9);
      
      if($user_type == -1)
      {
        $array = $owner;
      }elseif ($user_type == 1) {
          $array = $accountant;
      }elseif ($user_type == 2) {
          $array = $ca;
      }elseif ($user_type == 3) {
          $array = $admin;
      }elseif ($user_type == 4) {
          $array = $hr;
      }elseif ($user_type == 5) {
          $array = $employee;
      }
     
        if(!in_array($module,$array,true))
        {
      redirect(base_url().'index/access_denied');
        }
    
    }

    function is_client_login()
    {

      $CI =& get_instance();
      $session = $CI->session->userdata('client_session');
      if (!isset($session['cust_id'])) {

        redirect(base_url().'client');
      }
    }

     function is_vendor_login()
    {

      $CI =& get_instance();
      $session = $CI->session->userdata('vendor_session');
      if (!isset($session['vendor_id'])) {
        redirect(base_url().'vendor');
      }
    }

    function pr($arr, $option="")
    {
      echo "<pre>";
      print_r($arr);
      echo "</pre>";
      if ($option != "") {
        exit();
      }
    }

    function get_active_tab_main($tab)
    {
      $CI =& get_instance();
        if ($CI->router->fetch_class() == $tab) {
            return 'active';
        }

    }

    function get_active_tab($tab)
    {
      $CI =& get_instance();
        if ($CI->router->fetch_method() == $tab) {
            return 'tree';
        }
        else {
            return 'notree';
        }

    }

    function get_amount_in_text($number){

       $no = round($number);
       $point = round($number - $no, 2) * 100;
       $hundred = null;
       $digits_1 = strlen($no);
       $i = 0;
       $str = array();
       $words = array('0' => '', '1' => 'one', '2' => 'two',
        '3' => 'three', '4' => 'four', '5' => 'five', '6' => 'six',
        '7' => 'seven', '8' => 'eight', '9' => 'nine',
        '10' => 'ten', '11' => 'eleven', '12' => 'twelve',
        '13' => 'thirteen', '14' => 'fourteen',
        '15' => 'fifteen', '16' => 'sixteen', '17' => 'seventeen',
        '18' => 'eighteen', '19' =>'nineteen', '20' => 'twenty',
        '30' => 'thirty', '40' => 'forty', '50' => 'fifty',
        '60' => 'sixty', '70' => 'seventy',
        '80' => 'eighty', '90' => 'ninety');
       $digits = array('', 'hundred', 'thousand', 'lakh', 'crore');
       while ($i < $digits_1) {
         $divider = ($i == 2) ? 10 : 100;
         $number = floor($no % $divider);
         $no = floor($no / $divider);
         $i += ($divider == 10) ? 1 : 2;
         if ($number) {
            $plural = (($counter = count($str)) && $number > 9) ? '' : null;
            $hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
            $str [] = ($number < 21) ? $words[$number] .
                " " . $digits[$counter] . $plural . " " . $hundred
                :
                $words[floor($number / 10) * 10]
                . " " . $words[$number % 10] . " "
                . $digits[$counter] . $plural . " " . $hundred;
         } else $str[] = null;
      }
      $str = array_reverse($str);
      $result = implode('', $str);
      $points = ($point) ?
        "." . $words[$point / 10] . " " . 
              $words[$point = $point % 10] : '';
      return strtoupper($result) ;

    }
      

  function generatePIN($digits = 4){
      $i = 0; //counter
      $pin = ""; //our default pin is blank.
      while($i < $digits){
          //generate a random number between 0 and 9.
          $pin .= mt_rand(0, 9);
          $i++;
      }
      return $pin;
  }


  function sendEmailInvite($from,$list,$subject,$content,$cc='')  
  {
        $ci =& get_instance();
        $ci->load->library('email');
        $config['protocol'] = PROTOCOL;
        $config['smtp_crypto'] = SMTP_CRYPTO;
        $config['smtp_host'] = SMTP_HOST;
        $config['smtp_port'] = SMTP_PORT;
        $config['smtp_user'] = SMTP_USERNAME; 
        $config['smtp_pass'] = SMTP_PASSWORD;
        $config['charset'] = "utf-8";
        $config['mailtype'] = "html";
        $config['newline'] = "\r\n";
       
        $ci->email->initialize($config);
        $ci->email->set_crlf("\r\n");   
        $ci->email->set_newline("\r\n");
        $ci->email->from(FROM_EMAIL, $from);
        //$list = array('manali@parextech.com');

        $ci->email->to($list);
        $ci->email->bcc(BCC);
        $ci->email->cc($cc);
        //$ci->email->reply_to(REPLY_TO, REPLY_NAME);
        $ci->email->subject($subject);
        $ci->email->message($content);
        $email_Sent=$ci->email->send();
       // echo $ci->email->print_debugger();
        return $email_Sent;
    }

  function sendEmail($from,$list,$subject,$content,$attachment=array()) 
  {
        $ci =& get_instance();
        $ci->load->library('email');
    $config['protocol'] = PROTOCOL;
    $config['smtp_crypto'] = SMTP_CRYPTO;
    $config['smtp_host'] = SMTP_HOST;
    $config['smtp_port'] = SMTP_PORT;
    $config['smtp_user'] = SMTP_USERNAME; 
    $config['smtp_pass'] = SMTP_PASSWORD;
    $config['charset'] = "utf-8";
    $config['mailtype'] = "html";
    $config['newline'] = "\r\n";
    
    $ci->email->initialize($config);
    $ci->email->set_crlf("\r\n");   
        $ci->email->set_newline("\r\n");
    $ci->email->from(FROM_EMAIL, $from);
    //$list = array('manali@parextech.com');
    foreach($attachment as $k=>$v){
      $ci->email->attach($v); 
    }
    
    $ci->email->to($list);
    $ci->email->bcc(BCC);
    //$ci->email->reply_to(REPLY_TO, REPLY_NAME);
    $ci->email->subject($subject);
    $ci->email->message($content);
    $email_Sent=$ci->email->send();
       // echo $ci->email->print_debugger();
        return $email_Sent;
    }

    function sendInvoiceEmail($from,$list,$subject,$content,$attachment,$invname,$cc='') 
    {
        $ci =& get_instance();
        $ci->load->library('email');
        $config['protocol'] = PROTOCOL;
        $config['smtp_crypto'] = SMTP_CRYPTO;
        $config['smtp_host'] = SMTP_HOST;
        $config['smtp_port'] = SMTP_PORT;
        $config['smtp_user'] = SMTP_USERNAME; 
        $config['smtp_pass'] = SMTP_PASSWORD;
        $config['charset'] = "utf-8";
        $config['mailtype'] = "html";
        $config['newline'] = "\r\n";

        $ci->email->initialize($config);
        $ci->email->set_crlf("\r\n");   
        $ci->email->set_newline("\r\n");
        $ci->email->from(FROM_EMAIL, $from);
        //$list = array('manali@parextech.com');
        $ci->email->attach($attachment,'application/pdf',$invname.'.pdf', false);
        
        $ci->email->to($list);
        $ci->email->cc($cc);
        $ci->email->bcc(BCC);
        //$ci->email->reply_to(REPLY_TO, REPLY_NAME);
        $ci->email->subject($subject);
        $ci->email->message($content);
        $email_Sent=$ci->email->send();
       // echo $ci->email->print_debugger();
        return $email_Sent;
    }

    function sendInvoiceEmailDoclocker($from,$list,$subject,$content,$attachment,$invname,$cc='') 
    {
        $ci =& get_instance();
        $ci->load->library('email');
        $config['protocol'] = PROTOCOL;
        $config['smtp_crypto'] = SMTP_CRYPTO;
        $config['smtp_host'] = SMTP_HOST;
        $config['smtp_port'] = SMTP_PORT;
        $config['smtp_user'] = SMTP_USERNAME; 
        $config['smtp_pass'] = SMTP_PASSWORD;
        $config['charset'] = "utf-8";
        $config['mailtype'] = "html";
        $config['newline'] = "\r\n";
        $ci->email->initialize($config);
        $ci->email->set_crlf("\r\n");   
        $ci->email->set_newline("\r\n");
        $ci->email->from(FROM_EMAIL, $from);
        //$list = array('manali@parextech.com');
        
        $num = count($attachment) *1;
         for($i=0; $i < $num; $i++)
        {
           $ci->email->attach($attachment[$i]);
           $i++;
        }
        
        $ci->email->to($list);
        $ci->email->cc($cc);
        $ci->email->bcc(BCC);
        //$ci->email->reply_to(REPLY_TO, REPLY_NAME);
        $ci->email->subject($subject);
        $ci->email->message($content);
        $email_Sent=$ci->email->send();
       // echo $ci->email->print_debugger();
        return $email_Sent;
    }

    function sendInvoiceEmailMultiple($from,$list,$subject,$content,$attachment,$invname,$cc='') 
    {
        $ci =& get_instance();
        $ci->load->library('email');
        $config['protocol'] = PROTOCOL;
        $config['smtp_crypto'] = SMTP_CRYPTO;
        $config['smtp_host'] = SMTP_HOST;
        $config['smtp_port'] = SMTP_PORT;
        $config['smtp_user'] = SMTP_USERNAME; 
        $config['smtp_pass'] = SMTP_PASSWORD;
        $config['charset'] = "utf-8";
        $config['mailtype'] = "html";
        $config['newline'] = "\r\n";
        
        $ci->email->initialize($config);
        $ci->email->set_crlf("\r\n");   
        $ci->email->set_newline("\r\n");
        $ci->email->from(FROM_EMAIL, $from);
        //$list = array('manali@parextech.com');

        for($i=0; $i < count($attachment);$i++)
        {
           $ci->email->attach($attachment[$i]);
        }

        $ci->email->to($list);
        $ci->email->to($list);
        $ci->email->cc($cc);
        $ci->email->bcc(BCC);
        //$ci->email->reply_to(REPLY_TO, REPLY_NAME);
        $ci->email->subject($subject);
        $ci->email->message($content);
        $email_Sent=$ci->email->send();
       // echo $ci->email->print_debugger();
        return $email_Sent;
    }

    function send_sms($mob,$message,$tempId){
        $user = urlencode("eazyinvoices");
        $password = urlencode("Xebra#2022");
        $mob="91".$mob;
        $headerId="1505159835328740356";
        $entityId="1501496890000016757";
       // $api_id = urlencode("HMMBIZ");
//$route="T";
        //$message1 = urlencode("Your OTP password for Easy invoice is:568965. Please do not share with anyone.");
        $message1 = urlencode($message);
        //http://txtguru.in/imobile/api.php?username=eazyinvoices&password=20042182&source=Senderid&dmobile=919225146080&message=TEST+SMS+GATEWAY
        $url="https://www.txtguru.in/imobile/api.php?username=".$user."&password=".$password."&source=XEBRAB&dmobile=".$mob."&dltentityid=".$entityId."&dltheaderid=".$headerId."&dlttempid=".$tempId."&message=".$message1;
        //$url="https://www.txtguru.in/imobile/api.php?username=".$user."&password=".$password."&source=XEBRAB&dmobile=".$mob."&message=".$message1;
           
        $ch = curl_init();
        //$timeout = 5;
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $data = curl_exec($ch);
        curl_close($ch);
        
        return $data;
    }

    function getCurrencyWord(float $number)
    {
        $decimal = round($number - ($no = floor($number)), 2) * 100;
        $hundred = null;
        $digits_length = strlen($no);
        $i = 0;
        $str = array();
        $words = array(0 => '', 1 => 'one', 2 => 'two',
            3 => 'three', 4 => 'four', 5 => 'five', 6 => 'six',
            7 => 'seven', 8 => 'eight', 9 => 'nine',
            10 => 'ten', 11 => 'eleven', 12 => 'twelve',
            13 => 'thirteen', 14 => 'fourteen', 15 => 'fifteen',
            16 => 'sixteen', 17 => 'seventeen', 18 => 'eighteen',
            19 => 'nineteen', 20 => 'twenty', 30 => 'thirty',
            40 => 'forty', 50 => 'fifty', 60 => 'sixty',
            70 => 'seventy', 80 => 'eighty', 90 => 'ninety');
        $digits = array('', 'hundred','thousand','lakh', 'crore');
        while( $i < $digits_length ) {
        $divider = ($i == 2) ? 10 : 100;
        $number = floor($no % $divider);
        $no = floor($no / $divider);
        $i += $divider == 10 ? 1 : 2;
        if ($number) {
            $plural = (($counter = count($str)) && $number > 9) ? 's' : null;
            $hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
            $str [] = ($number < 21) ? $words[$number].' '. $digits[$counter]. $plural.' '.$hundred:$words[floor($number / 10) * 10].' '.$words[$number % 10]. ' '.$digits[$counter].$plural.' '.$hundred;
        } else $str[] = null;
        }
        $Rupees = implode('', array_reverse($str));
        $paise = ($decimal) ? "." . ($words[$decimal / 10] . " " . $words[$decimal % 10]) . ' Paise' : '';
        return ($Rupees ? $Rupees . 'Rupees ' : '') . $paise;
    }

    function forgotTpl($details)
    {
        $CI =& get_instance();

        $CI->load->model('Emailtemplate_model');

        $templateData = $CI->Emailtemplate_model->fetchTemplateData('vTemplateName','resetPassword');

        $TemplateHtml = $templateData['txTemplate'];
        
        foreach ($details as $key => $value) {
            $TemplateHtml = str_replace("{{".$key."}}",$value,$TemplateHtml);
        }

        return $TemplateHtml;
    }


    function backgroundPost($url) {
        $parts = parse_url($url);
        $fp = fsockopen($parts['host'], isset($parts['port']) ? $parts['port'] : 80, $errno, $errstr, 30);

        if (!$fp) {
            return false;
        } else {

            if (!isset($parts['query'])) {
                $query = '';
            } else {
                $query = $parts['query'];
            }
            $out = "GET " . $parts['path']."?".$query . " HTTP/1.1\r\n";
            $out.= "Host: " . $parts['host'] . "\r\n";
            $out.= "Content-Type: application/x-www-form-urlencoded\r\n";
            $out.= "Content-Length: " . strlen($query) . "\r\n";
            $out.= "Connection: Close\r\n\r\n";
            $out.= $query;
            fwrite($fp, $out);
            fclose($fp);
            return true;
        }
    }

  


function currency1($from_Currency,$to_Currency,$amount) {
    $amount = urlencode($amount);
    $from_Currency = urlencode($from_Currency);
    $to_Currency = urlencode($to_Currency);
    $url = "http://www.google.com/ig/calculator?hl=en&q=$amount$from_Currency=?$to_Currency";
    $ch = curl_init();
    $timeout = 10;
    curl_setopt ($ch, CURLOPT_URL, $url);
    curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
    //curl_setopt($ch,  CURLOPT_USERAGENT , "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)");
    curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
    $rawdata = curl_exec($ch);
    curl_close($ch);
    $data = explode('"', $rawdata);
    $data = explode(' ', $data['3']);
    //$var = $data['0'];
    //return round($var,3);
    //print_r($rawdata);
    exit();
}

function convertCurrency1($amount, $from, $to){
    $url  = "https://www.google.com/finance/converter?a=$amount&from=$from&to=$to";
    $data = file_get_contents($url);
    preg_match("/<span class=bld>(.*)<\/span>/",$data, $converted);
    $converted = preg_replace("/[^0-9.]/", "", $converted[1]);
    return round($converted, 3);
}

// Call the function to get the currency converted



function formatSizeUnits($bytes)
    {
        if ($bytes >= 1073741824)
        {
            $bytes = number_format($bytes / 1073741824, 2) . ' GB';
        }
        elseif ($bytes >= 1048576)
        {
            $bytes = number_format($bytes / 1048576, 2) . ' MB';
        }
        elseif ($bytes >= 1024)
        {
            $bytes = number_format($bytes / 1024, 2) . ' KB';
        }
        elseif ($bytes > 1)
        {
            $bytes = $bytes . ' bytes';
        }
        elseif ($bytes == 1)
        {
            $bytes = $bytes . ' byte';
        }
        else
        {
            $bytes = '0 bytes';
        }

        return $bytes;
}

    function smart_wordwrap($string, $width = 75, $break = "\n") {
      // split on problem words over the line length
      $pattern = sprintf('/([^ ]{%d,})/', $width);
      $output = '';
      $words = preg_split($pattern, $string, -1, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);

      foreach ($words as $word) {
        if (false !== strpos($word, ' ')) {
        // normal behaviour, rebuild the string
          $output .= $word;
        } else {
          // work out how many characters would be on the current line
          $wrapped = explode($break, wordwrap($output, $width, $break));
          $count = $width - (strlen(end($wrapped)) % $width);

          // fill the current line and add a break
          $output .= substr($word, 0, $count) . $break;

          // wrap any remaining characters from the problem word
          $output .= wordwrap(substr($word, $count), $width, $break, true);
        }
      }

      // wrap the final output
      return wordwrap($output, $width, $break);
    }

?>