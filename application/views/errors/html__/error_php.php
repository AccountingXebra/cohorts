<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div style="border:1px solid #990000;padding-left:20px;margin:0 0 10px 0;" hidden>

<h4>A PHP Error was encountered</h4>

<p>Severity: <?php echo $severity; ?></p>
<p>Message:  <?php echo $message; ?></p>
<p>Filename: <?php echo $filepath; ?></p>
<p>Line Number: <?php echo $line; ?></p>

<?php if (defined('SHOW_DEBUG_BACKTRACE') && SHOW_DEBUG_BACKTRACE === TRUE): ?>

	<p>Backtrace:</p>
	<?php foreach (debug_backtrace() as $error): ?>

		<?php if (isset($error['file']) && strpos($error['file'], realpath(BASEPATH)) !== 0): ?>

			<p style="margin-left:10px">
			File: <?php echo $error['file'] ?><br />
			Line: <?php echo $error['line'] ?><br />
			Function: <?php echo $error['function'] ?>
			</p>

		<?php endif ?>

	<?php endforeach ?>

<?php endif ?>

</div>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Php Error</title>
<meta name="description" content="Page not found">
<style type="text/css">
::selection { background-color: #E13300; color: white; }
::-moz-selection { background-color: #E13300; color: white; }
body { background-color: #fff; margin: 40px; font: 13px/20px normal Helvetica, Arial, sans-serif; color: #4F5155; }
a { font-size:28px !important; color: #003399; background-color: transparent; font-weight: normal; }
h1 { color: #444; background-color: transparent; font-size: 19px; font-weight: normal; margin: 0 0 14px 0; padding: 14px 15px 10px 15px; }
code { font-family: Consolas, Monaco, Courier New, Courier, monospace; font-size: 12px; background-color: #f9f9f9; border: 1px solid #D0D0D0; color: #002166; display: block; margin: 14px 0 14px 0; padding: 12px 10px 5px 10px; }
#container { text-align: center; }
.hlink{ text-decoration: underline; }
a { text-decoration: none; color: #7864e9; font-size: 22px; }
p { margin: 5px 15px 5px 15px; color: #7864e9; font-size: 25px; }
.error-head{ padding: 40px 0 15px 0; color: #7864e9; font-size: 40px; }
</style>
</head>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.3/jquery.min.js" integrity="sha512-STof4xm1wgkfm7heWqFJVn58Hm3EtS31XFaagaa8VMReCXAkQnJZ+jEy8PCC/iT18dFy95WcExNHFTqLyp72eQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<body style="overflow-y:hidden;">
	<div id="container" style="margin:8% 0 25% 0;">
		<img src="../../asset/css/img/icons/error.png">
		
		<h1 class="error-head"><b>Oops,</b></h1>
		<p style="margin:25px 0 25px 0;"><a>Something went wrong</a></p>

		<p><a class="hlink goback" href="#">Go back</a></p>
	</div>
	<script>
		window.location.href = "../errors";
		$('.goback').click(function(){
			window.history.go(-1);
		});
	</script>
</body>
</html>