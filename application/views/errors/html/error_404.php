<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>404 Page Not Found</title>
<style type="text/css">

::selection { background-color: #E13300; color: white; }
::-moz-selection { background-color: #E13300; color: white; }

body {
	background-color: #fff;
	margin: 40px;
	font: 13px/20px normal Helvetica, Arial, sans-serif;
	color: #4F5155;
}

a {
	color: #003399;
	background-color: transparent;
	font-weight: normal;
}

h1 {
	color: #444;
	background-color: transparent;
	border-bottom: 1px solid #D0D0D0;
	font-size: 19px;
	font-weight: normal;
	margin: 0 0 14px 0;
	padding: 14px 15px 10px 15px;
}

code {
	font-family: Consolas, Monaco, Courier New, Courier, monospace;
	font-size: 12px;
	background-color: #f9f9f9;
	border: 1px solid #D0D0D0;
	color: #002166;
	display: block;
	margin: 14px 0 14px 0;
	padding: 12px 10px 5px 10px;
}

#container {
	text-align: center;
}

a {
	text-decoration: none;
	color: #7864e9;
	font-size: 22px;
}

p {
	margin: 5px 15px 5px 15px;
	color: #7864e9;
	font-size: 25px;
}

.error-head{
	padding: 15px 0;
}
</style>
</head>
<body>
	<div id="container">
		<img src="http://localhost//xebra/asset/css/img/icons/404page.jpg">
		
		<p class="error-head"><b>Oops,</b></p>
		<p class="error-head"><a>It appears the Xebras are grazing elsewhere.</a></p>

		<p><a href="javascript:window.history.go(-1);">Go back</a></p>
	</div>
</body>
</html>