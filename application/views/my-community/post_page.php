<!DOCTYPE html>
<html lang="en">
	<?php $this->load->view('my-community/Cohorts-header'); ?>
    <head>
		<link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,400;0,500;0,600;0,700;0,800;1,400;1,700&display=swap" rel="stylesheet">	
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" defer>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="msapplication-tap-highlight" content="no">
		<meta name="keywords" content="">
        <title><?=$blog[0]->post_name?></title>
		<meta name="description" content="<?php echo $blog[0]->post_name; ?>">	
    </head>
	<style>
	#commentform [type="checkbox"] + label:before, [type="checkbox"]:not(.filled-in) + label:after {
		width: 18px;
		height: 18px;
		margin: 3px 0 0 2px;
		border: 2px solid #B0B7CA;
	}
	.postSub #digest_form #contact_submit {
		margin: 0 0 0 1px;
		border-radius: 0;
        padding: 0px 10px;
		width: 40%;
		font-size: 14px;
	}
	.xbForm #contact_submit {
		background-color: #644dd5;
		width: 30%;
		color: #fff;
		border-radius: 10px;
		padding: 10px 0;
		box-shadow: 0 0 10px rgb(0 0 0 / 65%);
	}
	#commentform #submit {
		background-color: #7863e8;
		padding: 5px 12px;
		color: #fff;
		border: 1px solid #7863e8;
		font-size: 15px;
		border-radius: 3px;
	}
	#commentform label{ font-size:15px; color:#595959; }	
	.post_content a {
		color: #7965ea;
		font-weight: 500;
	}
	.color_purple{ color: #7965ea; }
	.xebraTweet {
		height: 508px;
		overflow-y: scroll;
		margin-top:40px;
	}
	.mi-share{ margin-left:6px; }
	.postSearch input[type=text] {
		padding: 6px;
		margin-top: 8px;
		font-size: 17px;
		border: 1px solid #ccc;
		border-top-left-radius: 3px;
		border-bottom-left-radius: 3px;
		width: 94%;
		height:26px;
	}
	.postSearch select {
		border: 1px solid #ccc;
		width: 100%;
		padding: 5px 5px;
		margin: 10px 0;
		border-radius: 3px;
	}
	.postSearch .searchsubmit {
		border: 1px solid #ccc;
		height: 40px;
		line-height: 36px;
		padding: 1px 10px;
		margin: 0 0 0 -4px;
		border-top-right-radius: 3px;
		border-bottom-right-radius: 3px;
	}
	.postSearch p {
		font-weight: 600;
		font-size: 1.077em;
	}
	#commentform textarea.form-field {
		width: 102% !important;
		border: 1px solid #ccc !important;
		padding: 5px 10px;
		border-radius: 3px !important;
		height: 150px !important;
	}
	#commentform input.form-field {
		width: 100%;
		border: 1px solid #ccc;
		padding: 5px 10px;
		border-radius: 3px;
		height:30px;
		margin-bottom:0px;
	}
	.related_post .postInfo {
		text-align: justify;
		font-size: 15px;
		display: -webkit-box;
		-webkit-line-clamp: 3;
		-webkit-box-orient: vertical;
		overflow: hidden;
		text-overflow: ellipsis;
		height: 80px;
		padding-top: 12px;
	}
		.coverPic img { object-fit: contain; }
		.postContent h1 { font-size: 24px; margin-left:8px; }	
		.post_content p { font-size: 16px; text-align: justify; line-height: 25px; padding: 12px 0; }
		.eachPost{ margin-top:2%; }
		
		.media_container .tab button {
			/*width: auto;*/
		}
		.embed-240p{ height: 400px; padding: 3% 0 0 0; }
		.embed-240p iframe{
			left: 20% !important;
			width: 60% !important;
			height: 65% !important;
			border-radius: 10px;
		}
	.pagination .current{
		background-color: #ff7c9b;
		border-radius: 5px;
		color:#fff;
	}
	.pagination .prev, .pagination .next{
		font-size:25px;
	}
	.pagination .page:hover{
		cursor:pointer;
	}
	.pagination{ 
		margin: 3% 0 3% 0;
	}
	.pagination a{
		padding: 10px 15px;
		color:#313131;
	}	
	.mediaPost img {
		border-radius: 10px;
		object-fit: cover;
		width: 68%;
	}
	@media only screen and (max-width: 600px) {
		.mediaPost img {
			border-radius: 10px;
			object-fit: cover;
			width: 100%;
		}	
		.media_container .tab {
			margin-left:35px;
		}
		.marleft4{
			margin-left:-13% !important;
		}
		.letterul .letterDiv {
			height: 175px;
			width: 185px;
			margin: 20px 25px 20% 50px;
			float: left;
			text-align: center;
		}
		.modal {
			width: 94% !important;
			left: 3% !important;
		}	
		.firstTable .secondImg{ width:0% !important; }
	}
	.marleft4{
		margin:0 0 5% 1%;
		/*margin-left:1%;*/
	}
	.media_container .tabcontent {
		padding: 0px 1%;
	}
	.tabnews{ margin:0 -5%; }
	.letterDiv{ height:175px; width: 185px; margin: 20px 25px 12% 20px; float:left; text-align:center; }
	.letterDiv img{ border:1px solid #ccc; object-fit:cover; }
	.newsletter_title{ color: #000; text-decoration: none; text-align: left; margin: 0 -5px 0 0px; font-size: 13px; }
	.newsletter-container .tab { padding: 5px 10px; border: none; margin:auto; width:auto; display:table; }
	.newsletter-container .tab button { border-left:1px solid #ccc; }
	.tab button.tablinks1.active { padding: 8px 16px; border-radius: 1px; color: #7965ea; background-color:transparent; font-weight:600; }
	.tab button.tablinks1:hover { padding: 8px 16px; color: #7965ea; border-radius: 1px; background-color:transparent; font-weight:600;}
	.letterul{ list-style-type: none; }
	.mar40{ margin-top:40px; }
	.openewsletter{ cursor:pointer; }
	.allactive{ margin:0 -1% 0 0; }
	.tab button{ padding:8px 0; }
	.tab{ width:560px; }
	.success-div {
		background-color: #50e3c2;
		padding-bottom: 10px;
		width: 40%;
		color: #fff;
		font-size: 18px;
		font-weight: 500;
		left: 39% !important;
		position: fixed !important;
		transform: translate(-50%, 0px) !important;
		margin-top: 0.5%;
	}
	
	.error-div{
		background-color: #ff7c9b;
		padding-bottom: 10px;
		width: 40%;
		color: #fff;
		font-size: 18px;
		font-weight: 500;
		left: 39% !important;
		position: fixed !important;
		transform: translate(-50%, 0px) !important;
		margin-top:0.5%;
	}
	@supports (-webkit-touch-callout: none) {
		/* CSS specific to iOS devices */ 
	}
	
	@supports (-webkit-font-smoothing: auto) {

	#archive::after, #categories::after {
       position: absolute;
       z-index: 2;
       top: calc(45% - 5px);
       right: .5rem;
       width: 10px;
       height: 10px;
	}

	#archive select, #categories select {
      padding: .375rem 0;
      width: calc(100% - 4px);
      transform: translateX(15px); 
	}
 
	}
	.tab .newsletterbtn{ padding: 8px 8px; }
	.newsletterbtn.active, .newsletterbtn:hover{ width:115px !important; }
	.onlyblog{ margin:0% 0 0 0; }
	.postSearch{ margin-top:14%; }
	.breadcrumbs{ margin:0 0 3% 1%;  font-size: 14px; color: #000; }
	.homelink, .breadcrumbs i{ margin:0 10px 0 0; } 
	.breadcrumbs a{ color:#7965ea; }
	.catWord{ background-color:#7965ea; padding:1% 2%; font-size:22px; color: #fff; margin: 0 15px 0 0; }
	.catinfo{ margin:-10px 0 3% 8px; }
	</style>
    <body class="insidePages">
		<div class="be-each-section section eachPost">
			<div class="section">
				<div class="container postContent media_container">
					<div class="row">
						<div class="col s12 m12 l9">
							<div class="onlyblog">
							<p class="text-left breadcrumbs"><span class="homelink"><a class="gotohome" href="<?php echo base_url(); ?>news">Home</a></span><i class="fa fa-angle-right" aria-hidden="true"></i>  <?=$blog[0]->post_name?></p>
							
							<!-- Post Name Here -->
							<h1 class="post_head"><?=$blog[0]->post_name?></h1>
							
							<!-- Post Cover Picture -->
							<div class="row">
								<div class="col s12 m12 l12 text-center coverPic">
									<?php if($blog[0]->id == 37 ){ ?>
										<img width="430" height="300" src="https://xebra.in/public/upload/blog_image/36/pd1_cover.png" alt=""/>
									<?php } else { ?>
										<img width="430" height="300" src="<?=$blog[0]->image?>/<?=$blog[0]->filename?>" alt=""/>
									<?php } ?>	
								</div>	
							</div>
							
							<!-- Post Content -->
							<div class="row">
								<div class="col s12 m12 l12 text-center post_content">
									<?=$blog[0]->blog_content;?>
								</div>
							</div>

							<!-- Post Like -->
							<!--<div class="row">
								<div class="col s12 m12 l12 text-center post_like">
									<p class="text-left"><strong>Share This</strong></p>
									<hr>
									<div class="row">	
										<div class="col s12 m12 l12 text-left">
											<div class="tweet">
												<a href="https://twitter.com/login" target="_blank" class="p-share s-tweet"><i class="fa fa-twitter" aria-hidden="true"></i> Tweet</a><a href="https://www.facebook.com/" target="_blank" class="p-share s-face"><i class="fa fa-facebook" aria-hidden="true"></i> Facebook</a><a href="https://www.linkedin.com/login" target="_blank" class="p-share s-link"><i class="fa fa-linkedin" aria-hidden="true"></i> Linkedin</a><a href="mailto:" target="_blank" class="p-share s-email"><i class="fa fa-envelope" aria-hidden="true"></i> Email</a>
											</div>
										</div>
									</div>
								</div>
							</div>-->		
							
							<!-- Post Author -->
							<div class="row mar25">
								<div class="col s12 m12 l12 post_author">
									<div class="row">	
										<div class="col s12 m12 l12 text-left">
											<?php 
                                             $author=$this->Adminmaster_model->selectData('admin_profile', '*', array('id' =>$blog[0]->reg_id));
											?>
											<div class="author">
												<?php if(isset($author[0]->reg_profile_image)){ ?>
												<?php if($author[0]->reg_profile_image == " " || $author[0]->reg_profile_image == null){ ?>
													<a href="#" target="_blank"><img width="315" height="315" src="<?php echo base_url(); ?>asset/css/img/home_images/authorPic.png" alt=""></a>
												<?php }else{ ?>	
													<a href="#" target="_blank"><img width="315" height="315" src="<?php echo base_url(); ?>public/upload/admin_image/<?=$author[0]->id?>/<?=$author[0]->reg_profile_image?>" alt=""></a>
												<?php } ?>	
												<a href="#" target="_blank" class="author_name"><?=$author[0]->reg_username?></a>
												<?php } ?>
											</div>
										</div>
									</div>
								</div>
							</div>

							<!-- Related Post -->
							<div class="row mar25">
								<div class="col s12 m12 l12 related_post">
									<p class="text-left"><strong>Related Posts</strong></p>
									<hr>
									<div class="row">	
										<?php 
											$blogs=$this->Adminmaster_model->selectData('blogs', '*', '', 'created_at', 'DESC');
											$i = 0;
											foreach($blogs as $blogg){
											if($blogg->category=="news" && $blogg->view_for=="Other"){
											if($i < 3){
										?>
										<div class="col s12 m12 l4 text-left">
											<div class="text-center">
												<img width="200" height="175" src="<?=$blogg->image?>/<?=$blogg->filename?>" alt="" class="">
											</div>
											<p><a class="color_purple postName" href="<?php echo base_url(); ?>blog/<?=$blogg->url?>" target="_blank"><?=$blogg->post_name?></a></p>
											<p class="postInfo"><?=$blogg->excerpt?></p>
										</div>
											<?php } }$i++; } ?>
									</div>
								</div>
							</div>

							<!-- Post Comment -->
							<div class="row mar25" hidden>
								<div class="col s12 m12 l12 post_comment">
									<p class="text-left"><strong>This Post Has <span class="comment_count"><?=count($blog_comment)?></span> Comments</strong></p>
									<hr>
									<!-- Comment List From Here -->
									<div id="comment">
									<?php foreach($blog_comment as $comment){ ?>
									<div class="row">
										<div class="col s12 m12 l12">
											<div class="commentBox">
												<p class="text-left"><span class="comment_user"><?=$comment->name?></span> Says:</p>
												<p class="text-left comment_date"><?=date('d-m-y',strtotime($comment->created_at))?></p>
												<p class="text-left"><?=$comment->comment?>
												</p>
											</div>	
										</div>
									</div>	
									<?php  } ?>
								</div>
									<div class="row mar25">
										<div class="col s12 m12 l12">
											<p class="text-left">Leave a Reply</p>
										</div>	
										<div class="col s12 m12 l12 text-left mar10">
											<span class="text-left emailadd">Your email address will not be published. Required fields are marked *</span>
										</div>	
										<form action="" method="post" name="commentform" id="commentform" class="comment-form" novalidate="">
											<div class="col s12 m12 l12">
												<div class="row mar25">
													<div class="col s12 m12 l6 text-left">
														<label>Name <span class="redAsh">*</span></label></br>
														<input type="text" name="name" class="form-field"/>
														<input type="hidden" name="blog_id" id="blog_id" value="<?=$blog[0]->id?>" class="form-field"/>
													</div>
													<div class="col s12 m12 l6 text-left">
														<label>Email <span class="redAsh">*</span></label></br>
														<input type="text" name="email" class="form-field"/>
													</div>
												</div>
												<div class="row mar25">
													<div class="col s12 m12 l12 text-left">
														<label>Website</label></br>
														<input type="text" name="website" class="form-field"/>
													</div>
												</div>
												<div class="row mar25">
													<div class="col s12 m12 l12 text-left savefornext">
														<input class="form-check-input" type="checkbox" value="" id="savefornext" name="savefornext">
														<label class="form-check-label" for="savefornext">
															Save my name, email, and website in this browser for the next time I comment.
														</label>
													</div>
												</div>
												<div class="row mar25">
													<div class="col s12 m12 l12 text-left">
														<label>Comment</label><span class="redAsh">*</span></label></br>
														<textarea type="text" name="comment" class="form-field"></textarea>
													</div>
												</div>
												<div class="row mar25">
													<div class="col s12 m12 l12 text-left">
														<input name="submit" type="submit" id="submit" class="submit" value="Post Comment">
													</div>
												</div>
											</div>	
										</form>	
									</div>
								</div>			
							</div>
							</div>
						</div>
	                      
						<div class="col s12 m12 l3">
							<div class="search-container postSearch">
								<form action="<?php echo base_url(); ?>news" method="post" id="searchpostform" class="searchpostform" novalidate="">
										<?php $csrf = array(
					'name' => $this->security->get_csrf_token_name(),
					'hash' => $this->security->get_csrf_hash()
				);
				?>
						<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
									<div class="col s12 m12 l12">
										<p class="text-left">SEARCH</p>
										<input type="text" placeholder="Search.." name="search" style="width:81%;">
										<button type="submit" name="searchsubmit" class="searchsubmit"><i class="fa fa-search"></i></button>
									</div>	
									<!--<div class="col s12 m12 l12 mar25">
										<p class="text-left">CATEGORIES</p>
										<select class="" name="categories" id="categories" onchange='getElementById("searchpostform").submit();'>
											<option value="">Select Categories</option>
											<option value="all" <?php if($blog[0]->category=="all"){echo "selected";}?>>All</option>
											<option value="news_page" <?php if($blog[0]->category=="news_page"){echo "selected";}?>>News</option>
											<option value="media" <?php if($blog[0]->category=="media"){echo "selected";}?>>Media</option>
											<option value="blog" <?php if($blog[0]->category=="blog"){echo "selected";}?>>Product</option>
											<option value="video" <?php if($blog[0]->category=="video"){echo "selected";}?>>Video</option>
											<option value="news" <?php if($blog[0]->category==" "){echo "selected";}?>>Newsletters</option>
										</select>
									</div>-->	
									<div class="col s12 m12 l12 mar25">
										<p class="text-left">FOLLOW US ON</p>
										<div class="tweet mar25">
											<a href="https://twitter.com/xebradotin" target="_blank" class="mi-share s-tweet"><img src="<?php echo base_url(); ?>asset/images/sm_icons/icon_twitter - 27.png" width="27" height="27" alt="Twitter" title="Twitter"></a>
											<a href="https://www.facebook.com/Xebra.in/" target="_blank" class="mi-share s-face"><img src="<?php echo base_url(); ?>asset/images/sm_icons/icon_facebook - 27.png" width="27" height="27" alt="Facebook" title="Facebook"></a>
											<a href="https://www.linkedin.com/company/xebrabiztech/" target="_blank" class="mi-share s-link"><img src="<?php echo base_url(); ?>asset/images/sm_icons/icon_linkedin - 27.png" width="27" height="27" alt="Linkedin" title="Linkedin"></a>
											<a href="https://www.instagram.com/xebradotin/" target="_blank" class="mi-share s-insta"><img src="<?php echo base_url(); ?>asset/images/sm_icons/icon_instagram - 27.png" width="27" height="27" alt="Instagram" title="Instagram"></a>
											<a href="https://www.youtube.com/channel/UCddSZ6gFMbpIdXeSeucNfTQ?view_as=subscriber" target="_blank" class="mi-share s-youtube"><img src="<?php echo base_url(); ?>asset/images/sm_icons/icon_youtube - 27.png" width="27" height="27" alt="Youtube" title="Youtube"></a>
											<a href="https://www.kooapp.com/profile/xebra" rel="noopener" target="_blank" class="mi-share s-koo"><img src="<?php echo base_url(); ?>asset/images/sm_icons/koo.png" width="28" height="28" alt="Koo" title="Koo"></a>
										</div>
									</div>
									<div class="col s12 m12 l12 mar50 xebraTweet scrollbar">
										<a class="twitter-timeline" href="https://twitter.com/xebradotin?ref_src=twsrc%5Etfw">Tweets by xebradotin</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
									</div>									
									<div class="col s12 m12 l12 mar25">
										<?php $month= date('m'); ?>
										<p class="text-left">ARCHIVE</p>
										<select class="" id="archive" name="archive" onchange='getElementById("searchpostform").submit();'>
										<option value="">Select Month</option>
											
										<?php foreach($blogss as $news){ 
                       if($news['newsletter_month']==1){
                       	$month= "January";
                       }
                       if($news['newsletter_month']==2){
                       	$month= "February";
                       }
                       if($news['newsletter_month']==3){
                       	$month= "March";
                       }
                       if($news['newsletter_month']==4){
                       	$month= "April";
                       }
                       if($news['newsletter_month']==5){
                       	$month= "May";
                       }
                       if($news['newsletter_month']==6){
                       	$month= "June";
                       }
                       if($news['newsletter_month']==7){
                       	$month= "July";
                       }
                       if($news['newsletter_month']==8){
                       	$month= "August";
                       }
                       if($news['newsletter_month']==9){
                       	$month= "September";
                       }
                       if($news['newsletter_month']==10){
                       	$month= "October";
                       }
                       if($news['newsletter_month']==11){
                       	$month= "November";
                       }
                       if($news['newsletter_month']==12){
                       	$month= "December";
                       }

									  ?>	
									<option value="<?=$news['newsletter_month']." ".$news['newsletter_year']?>"><?=$month." ".$news['newsletter_year']?></option>
									<?php } ?>

									</select>

									</div>	
								</form>
								<div class="col s12 m12 l12 mar25 postSub">
									<form class="xbForm text-center" id="digest_form" name="digest_form" action="<?php echo 'https://'.$_SERVER['HTTP_HOST']; ?>/index/blog_subscribe" method="post">
										<?php $csrf = array(
					'name' => $this->security->get_csrf_token_name(),
					'hash' => $this->security->get_csrf_hash()
				);
				?>
				<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
										<p>SUBSCRIBE TO XEBRA NEWSLETTERS</p>
										<div class="input-container mar25">
											<input class="input-field" type="text" placeholder="EMAIL" name="email" id="email">
										</div>
										<div class="input-container mar10">
											<input class="input-field" type="text" placeholder="NAME" name="name" id="name"><!--span class="colored">*</span-->
										</div>
										<div class="mar25">
											<input id="contact_submit" type="submit" value="SUBSCRIBE" class="btn"/>
										</div>	
									</form>
								</div>
							</div>
						</div>
					</div>	
				</div>
			</div>
		</div>		
	</body>
	<script>
		jQuery(document).ready(function() {
			$('.nav-link.loginxb').css('color','rgba(0,0,0,.5) !important');
			$('.post_content a').addClass('otherBlog');
			$('.otherBlog').attr('target', '_blank');
			
			<?php 
            if(isset($categories)){
            	if($categories=="media"){ ?>
                   window.location.href=base_url+"media"
				<?php }else if($categories=="blog"){ ?>
                     //openModule(event, 'blog');
                     window.location.href=base_url+"product"
				<?php }else if($categories=="news"){ ?>
                    // openModule(event, 'newsletters');
                     window.location.href=base_url+"newsletters"
				<?php }else if($categories=="news_page"){ ?>
					window.location.href=base_url+"news"
				<?php }else if($categories=="video"){ ?>
					window.location.href=base_url+"video"	
            	<?php }else{ ?>
					//openModule(event, 'all');
					window.location.href=base_url+"blog"
				<?php	}
            }
			?>
		});	
	</script>
	<script>
	jQuery(document).ready(function() {
		
		$('.allblog').hide();
		$('.gotohome').click(function(){
			$('.onlyblog').hide();
			$('.allblog').show();
		});
        
		$('.newsletterbtn').click(function(){
			$('.newsallactive').click();
			setTimeout(function(){ 
				$('.newsallactive').addClass('active');
			}, 100);
		});
		$('#location').change(function(){
			var id=$(this).val();
			
			openNewsletters(event, id);
			
		});
		$('.openewsletter').click(function(){
			var id=$(this).data('id');
			var file=$(this).data('file');
			console.log(file);
			$('.loadnews').load(file);
			$('#show_newsletter').modal('show');
		});
		
		$(".close-pop").click(function () {
			$("#show_newsletter").modal("hide");
		});
		
		$(function () {
			$("a[class='modal-trigger']").click(function () {
				$("#show_newsletter").modal("show");
				return false;
			});
		});
		
		<?php 
            if(isset($categories)){
            	if($categories=="media"){ ?>
                   window.location.href=base_url+"media"
				<?php }else if($categories=="blog"){ ?>
                     //openModule(event, 'blog');
                     window.location.href=base_url+"product"
				<?php }else if($categories=="news"){ ?>
                    // openModule(event, 'newsletters');
                     window.location.href=base_url+"newsletters"
				<?php }else if($categories=="news_page"){ ?>
					window.location.href=base_url+"news"
				<?php }else if($categories=="video"){ ?>
					window.location.href=base_url+"video"	
            	<?php }else{ ?>
					//openModule(event, 'all');
					//window.location.href=base_url+"blog"
				<?php	}
            }
        ?>
		
		$('.nav-link.loginxb').css('color','rgba(0,0,0,.5) !important');
			
		(function($) {
		var pagify = {
			items: {},
			container: null,
			totalPages: 1,
			perPage: 3,
			currentPage: 0,
			createNavigation: function() {
				this.totalPages = Math.ceil(this.items.length / this.perPage);
				$('.pagination', this.container.parent()).remove();
				var pagination = $('<div class="pagination"></div>').append('<a class="nav prev disabled" data-next="false"><</a>');
				for (var i = 0; i < this.totalPages; i++) {
					var pageElClass = "page";
					if (!i)
						pageElClass = "page current";
					var pageEl = '<a class="' + pageElClass + '" data-page="' + (
						i + 1) + '">' + (
						i + 1) + "</a>";
					pagination.append(pageEl);
				}
				pagination.append('<a class="nav next" data-next="true">></a>');
				this.container.after(pagination);
				var that = this;
				$("body").off("click", ".nav");
					this.navigator = $("body").on("click", ".nav", function() {
					var el = $(this);
					that.navigate(el.data("next"));
				});
				$("body").off("click", ".page");
				this.pageNavigator = $("body").on("click", ".page", function() {
					var el = $(this);
					that.goToPage(el.data("page"));
				});
			},
			navigate: function(next) {
				// default perPage to 5
				if (isNaN(next) || next === undefined) {
					next = true;
				}
				$(".pagination .nav").removeClass("disabled");
				if (next) {
					this.currentPage++;
					if (this.currentPage > (this.totalPages - 1))
						this.currentPage = (this.totalPages - 1);
					if (this.currentPage == (this.totalPages - 1))
						$(".pagination .nav.next").addClass("disabled");
					}
				else {
					this.currentPage--;
					if (this.currentPage < 0)
						this.currentPage = 0;
					if (this.currentPage == 0)
					$(".pagination .nav.prev").addClass("disabled");
				}
				this.showItems();
			},
			updateNavigation: function() {
				var pages = $(".pagination .page");
				pages.removeClass("current");
				$('.pagination .page[data-page="' + (
				this.currentPage + 1) + '"]').addClass("current");
			},
			goToPage: function(page) {
				this.currentPage = page - 1;
				$(".pagination .nav").removeClass("disabled");
				if (this.currentPage == (this.totalPages - 1))
				$(".pagination .nav.next").addClass("disabled");
				if (this.currentPage == 0)
				$(".pagination .nav.prev").addClass("disabled");
				this.showItems();
			},
			showItems: function() {
				this.items.hide();
				var base = this.perPage * this.currentPage;
				this.items.slice(base, base + this.perPage).show();
				this.updateNavigation();
			},
			init: function(container, items, perPage) {
				this.container = container;
				this.currentPage = 0;
				this.totalPages = 1;
				this.perPage = perPage;
				this.items = items;
				this.createNavigation();
				this.showItems();
			}
		};
		// stuff it all into a jQuery method!
		$.fn.pagify = function(perPage, itemSelector) {
			var el = $(this);
			var items = $(itemSelector, el);
			// default perPage to 5
			if (isNaN(perPage) || perPage === undefined) {
				perPage = 3;
			}
			// don't fire if fewer items than perPage
			if (items.length <= perPage) {
				return true;
			}
			pagify.init(el, items, perPage);
		};
		})(jQuery);	
	
		$(".allPost").pagify(6, ".mediaPost");
		//$(".allBlog").pagify(6, ".mediaPost");
	
	});
	
		//For Media 
		//document.getElementById('Insights').style.display = "block";
		function openModule(evt, moduleName) {
			$('.onlyblog').hide();
			$('.allblog').show();
			if(moduleName == "all"){
				$(".allPost").pagify(6, ".mediaPost");
			}else if(moduleName == "blog"){
				$(".allproduct").pagify(6, ".mediaPost");	
			}else if(moduleName == "media"){
				$(".allMedia").pagify(6, ".mediaPost");
			}else if(moduleName == "news"){
				$(".allNews").pagify(6, ".mediaPost");
			}else if(moduleName == "video"){
				$(".allVideo").pagify(6, ".mediaPost");
			}
			var i, tabcontent, tablinks;
			tabcontent = document.getElementsByClassName("tabcontent");
			for (i = 0; i < tabcontent.length; i++) {
				tabcontent[i].style.display = "none";
			}
			tablinks = document.getElementsByClassName("tablinks");
			for (i = 0; i < tablinks.length; i++) {
				tablinks[i].className = tablinks[i].className.replace(" active", "");
			}
			document.getElementById(moduleName).style.display = "block";
			evt.currentTarget.className += " active";
		}
		
		//For Newsletters
		function openNewsletters(evt, moduleName) {
			var i, tabcontent, tablinks1;
			tabcontent = document.getElementsByClassName("tabnews");
			for (i = 0; i < tabcontent.length; i++) {
				tabcontent[i].style.display = "none";
			}
			tablinks1 = document.getElementsByClassName("tablinks1");
			for (i = 0; i < tablinks1.length; i++) {
				tablinks1[i].className = tablinks1[i].className.replace(" active", "");
			}
			document.getElementById(moduleName).style.display = "block";
			evt.currentTarget.className += " active";
		}
	</script>
	<?php $this->load->view('template/footer'); ?>
</html>	