<meta name="viewport" content="width=device-width, initial-scale=1">

<style type="text/css">

	.dataTables_scrollBody{
	overflow:hidden !important;
	height:100% !important;
	}

	.dataTables_scrollHead{
	margin-bottom:-24px !important;
	}

	table.dataTable thead .sorting {
	background-position: 110px 15px !important;
	}
	table.dataTable thead .sorting_asc {
	background-position: 110px 15px !important;
	}
	table.dataTable thead .sorting_desc {
	background-position: 110px 15px !important;
	}

	.select-dropdown{
	max-height: 350px !important;
	}
	.btn-theme-disabled{
	cursor: not-allowed;
	}
	.icon-img {
	margin: 0 30px 0 2px;
	}

	/*----------START SEARCH DROPDOWN CSS--------*/
	.select2-container--default .select2-selection--single {
	border:none;
	}

	input[type="search"]:not(.browser-default) {
	height: 30px;
	font-size: 12px;
	margin: 0;
	border-radius: 5px;

	}
	.select2-container--default .select2-selection--single .select2-selection__rendered {
	font-size: 13px !important;
	line-height: 30px;
	color: #000 !important;
	font-weight: 400 !important;

	}
	.select2-container--default .select2-selection--single .select2-selection__arrow {
	height: 40px;
	}
	.select2-search--dropdown {
	padding: 0;
	}
	input[type="search"]:not(.browser-default):focus:not([readonly]) {
	border-bottom: 1px solid #bbb;
	box-shadow: none;
	}
	.select2-container--default .select2-selection--single:focus {
	outline: none;
	}
	.select2-container--default .select2-results__option--highlighted[aria-selected] {
	background: #fffaef;
	color: #666;
	}
	.select2-container--default .select2-results > .select2-results__options {
	font-size: 12px;
	border-radius: 5px;
	box-shadow: 0px 2px 6px #B0B7CA;
	}
	.select2-dropdown {
	border: none;
	border-radius: 5px;
	}
	.select2-container .select2-selection--single {
	height: 40px;
	padding: 6px;
	border: 1px solid #d4d8e4;
	background: #f8f9fd;
	border-radius: 5px;
	}
	.select2-results__option[aria-selected] {
	border-bottom: 1px solid #f2f7f9;
	padding: 14px 16px;
	}
	.select2-container--default .select2-search--dropdown .select2-search__field {
	border: 1px solid #d0d0d0;
	padding: 0 0 0 15px !important;
	width: 90%;
	max-width: 100%;
	}
	.select2-search__field::placeholder {
	content: "Search Here";
	}
	.select2-container--open .select2-dropdown--below {
	margin-top: 0;
	}
	.select2-container {
	width: 170px !important;
	}
	/*----------END SEARCH DROPDOWN CSS--------*/

	@media only screen and (min-width: 993px){
		.row .col.l6 {
		    margin-bottom: 0px !important;
		}
	}

	.dataTables_length {

	margin-left: 500px;
	}
	#my-customers_length{
	border:1px solid #B0B7CA;
	height:38px;
	border-radius:4px;
	width:90px;
	margin-top:5px;
	margin-left:52%;
	}

	#my-customers_length .select-wrapper input.select-dropdown {
	margin-top:-3px !important;
	margin-left:10px !important;
	}

	#my-customers_length .dropdown-content {
	min-width: 90px;
	margin-top:-50% !important;
	}

	.customer_profile_bulk_action.filled-in:not(:checked) + label:after {
	top:5px !important;
	}

	#my-customers_length .select-wrapper span.caret {
	margin: 17px 5px 0 0;
	}

	.sticky {
	position: fixed;
	top: 70px;
	width: 76.2%;
	z-index:999;
	background: white;
	color: black;
	}

	.sticky + .scrollbody {
	padding-top: 102px;
	}

	.tipxebra{
	margin-top:-10px;
	}

	a.filter-search.btn-search.btn.active {
	width:32% !important;
	margin-right: -60px !important;
	}

	#c_start_date, #c_end_date{
	max-width:80px !important;
	height:35px !important;
	padding-top:2px !important;
	margin-left:0px !important;
	}

	span.tabl {
	margin: -4px 0 0 21px !important;
	}

	#c_start_date::placeholder{
	color:#000;
	font-size:12px;
	}
	#c_end_date::placeholder{
	color:#000;
	font-size:12px;
	}

	.ex{
	margin:-28px 0 0 48px !important;
	}

	.btn-stated {
	background-position: 95px center !important;
	}

	.offer-1{
	border-radius:7px;
	padding:0 2%;
	}

	.offer-1:hover{
	border-left:4px solid #ff7d9a;
	}

	.img-offer{
	padding: 1% 0 !important;
	}

	.offer-info{
	padding: 2% 0 !important;
	}

	.set_remin{
	background-color: #ff7d9a !important;
	}

	a.showmore{
	color: #ff7d9a;
	font-size: 11px;
	text-decoration: underline;
	text-align:center;
	font-size: 12px;
	padding-top: 15px;
	}

	.offer-info h6{
	color:#7864e9 !important;
	}

	.offer_p, .offer_description{
	font-size:13px;
	}

	.btn-theme {
    font-size: 13px;
    font-weight: 500;
    padding: 0px 10px;
    border-radius: 5px;
    line-height: 50px;
    height: 50px;
    letter-spacing: 0.20px !important;
    background-color: #7864e9;
}

</style>

<!-- Company Offers Start --->
<div class="row deals-row">
	<div class="col s12 m12 l12">
		<div class="col l12 m12 s12 view-box white-box">
			<div class="row">

				<div class="col l12 m12 s12">
					<h6 id="deals" class="white-box-title" style="padding: 11px 34px !important">Deals</h6>
				</div>

				<div class="container">
			<div class="row">
				<div class="col s12 m12 l12" style="padding-top:3%;">
				<div class="offers_list">
					<div class="row">
						<?php if (count($offers) > 0){ foreach($offers as $off) {?>
						<div class="col s12 m12 l12">
						<div class="offer-1 box-wrapper bg-white shadow" style="margin: 0 0 10px 0px;">
							<div class="col s12 m12 l3 img-offer">
								<?php
									if($off['offer_image'] != '') {
										$offer_image = '<img src="'.DOC_ROOT_DOWNLOAD_PATH.'offer_image/'.$off['id'].'/'.$off['offer_image'].'" height="205px" class="logo_style_2" width="205px">';
									} else {
										$offer_image = '<img src="'.DOC_ROOT_DOWNLOAD_PATH.'offer_image/'.$off['id'].'/'.$off['offer_image'].'" height="205px" class="logo_style" width="205px">';
									}
								?>
								<div class="col s6 m6 l4" style="text-align: right">
									<?php echo $offer_image; ?>
								</div>
							</div>
							<div class="col s12 m12 l9 offer-info">
								<div class="row">
									<div class="col s12 m12 l9">

										<h6><strong><?php echo strtoupper($off['offer_title']);?></strong></h6>

										
										<?php if($off['bus_id']==$this->user_session['bus_id']){?>

										<div class="col s12 m12 l4" style="margin: -35px 0px 0px 127%;">
											<a href="javascript:void(0);" class="waves-effect waves-block waves-light event-button" data-activates="event-dropdown"><i class="material-icons">edit</i></a>
											<ul id="event-dropdown" class="dropdown-content user-profile-down" style="left: 1187.33px !important; top: 358.484px !important;">

						              <li class="user-profile-fix">
						                <ul>
															<li>
						                    <a href="<?php echo base_url();?>community/edit-offer/<?php echo $off['id'];?>"><i class="material-icons" style="color: #000;">mode_edit</i>Edit</a>
						                  </li>
															<li>
						                    <a href="javascript:void(0);" class="deactive_deal" data-deal_id="3"><i class="material-icons">delete</i>Delete</a>
						                  </li>
														</ul>
													</li>
												</ul>
										</div>
                                       <?php } ?>

										<div class="col s12 m12 l7" style="margin-left:-12px;">
											<p class="offer_p"><strong>VALIDITY DATE:</strong> <?php echo date("d-m-Y", strtotime($off['start_date'])); ?> - <?php echo date("d-m-Y", strtotime($off['end_date'])); ?></p>
										</div>

										<div class="col s12 m6 l5">
											<p class="offer_p"><strong>VALIDITY TIME:</strong> <?php echo $off['start_time'];?> - <?php echo $off['end_time'];?></p>
										</div>

										<div class="col s12 m12 l12" style="margin-top: -23px;">
											<p class="offer_p" style="margin-left: -12px;"><strong>WEBSITE:</strong> <?php echo $off['website_link'];?></p>
										</div>

									</div>

									
								</div>

								<div class="row">
									<div class="col s12 m12 l12 offer_description" style="margin: -15px 0px -10px 0;">
										<p style="text-align: justify;"><?php echo $off['offer_description'];?></p>
									</div>
								</div>

								<div class="row offer-time">
									<div class="col s12 m12 l12">
										<div class="col s12 m12 l6" style="margin: 10px 0px 0px -12px;">
											<p class="offer_p"><strong>COMPANY NAME:</strong> <?php echo $off['company_name'];?></p>
										</div>
										<div class="col s12 m12 l6">
											<p class="offer_p" style="margin: 22px 0px -10px -13px;"><strong>CONTACT NAME:</strong> <?php echo $off['contact_name'];?></p>
										</div>
										<div class="col s12 m12 l6" style="margin-left:-12px;">
											<p class="offer_p"><strong>EMAIL:</strong> <?php echo $off['email'];?></p>
										</div>
										<div class="col s12 m12 l6">
											<p class="offer_p" style="margin: -33px 10px -30px -12px;"><strong>MOBILE NUMBER:</strong> <?php echo $off['mobile_no'];?></p>
										</div>
									</div>
								</div>

								<div class="row" style="text-align: center; margin: 40px 0 0 -200px;" hidden>
									<div class="col l12 s12 m12">
										<a href="javascript:void(0);" class="more showmore" title="Show More">Show More</a>
										<a href="javascript:void(0);" class="less showmore" title="Show More" hidden>Show Less</a>
									</div>
								</div>	
							</div>
						</div>
						</div>
						
					<?php }} else{

						echo "<br>";

						echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";

						echo "There are no offers right now!";

					}?>
				
					</div>	
				</div>
				</div>
				<div class="row"><div class="col s12 m12 l12"><p></p></div></div>
				<div class="row"><div class="col s12 m12 l12"><p></p></div></div>
				<div class="row"><div class="col s12 m12 l12"><p></p></div></div>
            </div>
        </div>
			</div>
		</div>
	</div>
</div>
<!-- Company Offers End --->