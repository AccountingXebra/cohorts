<?php $this->load->view('my-community/Cohorts-header'); ?>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style type="text/css">
	.dataTables_scrollBody{
		overflow:hidden !important;
		height:100% !important;
	}

	.dataTables_scrollHead{
		margin-bottom:-24px !important;
	}

	table.dataTable thead .sorting {
		background-position: 110px 15px !important;
	}
	table.dataTable thead .sorting_asc {
		background-position: 110px 15px !important;
	}
	table.dataTable thead .sorting_desc {
		background-position: 110px 15px !important;
	}
	
  .select-dropdown{
    max-height: 350px !important;
  }
  .btn-theme-disabled{
     cursor: not-allowed;
  }
  .icon-img {
    margin: 0 30px 0 2px;
  }

/*----------START SEARCH DROPDOWN CSS--------*/
.select2-container--default .select2-selection--single {
  border:none;
}

input[type="search"]:not(.browser-default) {
  height: 30px;
  font-size: 12px;
  margin: 0;
  border-radius: 5px;

}
.select2-container--default .select2-selection--single .select2-selection__rendered {
  font-size: 13px !important;
  line-height: 30px;
  color: #000 !important;
  font-weight: 400 !important;

}
.select2-container--default .select2-selection--single .select2-selection__arrow {
  height: 40px;
}
.select2-search--dropdown {
  padding: 0;
}
input[type="search"]:not(.browser-default):focus:not([readonly]) {
  border-bottom: 1px solid #bbb;
  box-shadow: none;
}
.select2-container--default .select2-selection--single:focus {
    outline: none;
}
.select2-container--default .select2-results__option--highlighted[aria-selected] {
    background: #fffaef;
  color: #666;
}
.select2-container--default .select2-results > .select2-results__options {
  font-size: 12px;
  border-radius: 5px;
  box-shadow: 0px 2px 6px #B0B7CA;
}
.select2-dropdown {
  border: none;
  border-radius: 5px;
}
.select2-container .select2-selection--single {
  height: 40px;
    padding: 6px;
    border: 1px solid #d4d8e4;
    background: #f8f9fd;
    border-radius: 5px;
}
.select2-results__option[aria-selected] {
  border-bottom: 1px solid #f2f7f9;
  padding: 14px 16px;
}
.select2-container--default .select2-search--dropdown .select2-search__field {
    border: 1px solid #d0d0d0;
    padding: 0 0 0 15px !important;
    width: 90%;
    max-width: 100%;
}
.select2-search__field::placeholder {
  content: "Search Here";
}
.select2-container--open .select2-dropdown--below {
  margin-top: 0;
}
.select2-container {
  width: 200px !important;
}
/*----------END SEARCH DROPDOWN CSS--------*/

  .dataTables_length {

    margin-left: 500px;
}
	#my-customers_length{
		border:1px solid #B0B7CA;
		height:38px;
		border-radius:4px;
		width:90px;
		margin-top:5px;
		margin-left:52%;
	}

	#my-customers_length .select-wrapper input.select-dropdown {
		margin-top:-3px !important;
		margin-left:10px !important;
	}

	#my-customers_length .dropdown-content {
		min-width: 90px;
		margin-top:-50% !important;
	}

	.customer_profile_bulk_action.filled-in:not(:checked) + label:after {
		top:5px !important;
	}

	#my-customers_length .select-wrapper span.caret {
		margin: 17px 5px 0 0;
	}
	
	.sticky {
		position: fixed;
		top: 70px;
		width: 76.2%;
		z-index:999;
		background: white;
		color: black;
	}

	.sticky + .scrollbody {
		padding-top: 102px;
	}
	
	.tipxebra{
		margin-top:-10px;
	}
	
	a.filter-search.btn-search.btn.active {
		width:32% !important;
		margin-right: -60px !important;
	}
	
	#c_start_date, #c_end_date{
		max-width:80px !important;
		height:35px !important;
		padding-top:2px !important;
		margin-left:0px !important;
	}
	
	span.tabl {
		margin: -4px 0 0 21px !important;
	}
	
	#c_start_date::placeholder{
		color:#000;
		font-size:12px;
	}
	#c_end_date::placeholder{
		color:#000;
		font-size:12px;
	}
	
	.ex{
		margin:-28px 0 0 48px !important;
	}
	
	.btn-stated {
		background-position: 95px center !important;
	}
	
	.offer-1{
		border-radius:7px;
		padding:0 1%;
	}
	
	.offer-1:hover{
		border-left:4px solid #ff7d9a;
	}
	
	.img-offer{
		padding: 1% 0 !important;
	}
	
	.offer-info{
		padding: 2% 0 !important;
	}
	
	.set_remin{
		background-color: #ff7d9a !important;
	}
	
	a.showmore{
		color: #ff7d9a;
		font-size: 11px;
		text-decoration: underline;
		text-align:center;
		font-size: 12px;
		padding-top: 15px;
	}
	
	.offer-info h6{
		color:#7864e9 !important;
	}
	
	.offer_p, .offer_description{
		font-size:13px;
	}
</style>
    <div id="main">
      <div class="wrapper">
        <?php $this->load->view('template/sidebar'); ?>
        <section id="content" class="bg-cp customer-search">
          <div id="breadcrumbs-wrapper">
            <div class="container">
               <div class="row">
               <div id="plan_msg"></div>
             </div>
              <div class="row">
                <div class="col s10 m6 l6">
                  <h5 class="breadcrumbs-title">Events<small class="grey-text">(Total)</small></h5>

                  <ol class="breadcrumbs">
                    <li><a>My Community</a>
                    </li>
                    <li class="active">Events</li>
                  </ol>
                </div>
                <div class="col s10 m6 l6">
				  <a class="btn btn-theme btn-large right add-new cus-new" href="<?= base_url(); ?>community/add_events">CREATE NEW EVENT</a>
                </div>
              </div>
            </div>
          </div>

          <div id="bulk-action-wrapper">
            <div class="container">
              <div class="row">
				<div class="col l12 s12 m12" style="margin-top:-20px;">
                 <a href="javascript:void(0);" class="addmorelink right" onclick="reset_clientfilter();" title="Reset all">Reset</a>
                </div>
                <div class="col l4 s12 m12">
                  <a class='dropdown-button border-radius-6 btn-dropdown-action btn-bulk btn-default uppercase no-background' href='#' data-activates='dropdown04'>Bulk Actions <i class="arrow-icon"></i></a>
                    <ul id='dropdown04' class='dropdown-content'>
                      <li><a id="email_multiple_customers"><i class="dropdwon-icon icon email"></i>Email</a></li>
                      <li><a id="download_multiple_customers" data-multi_download="0" style="display: flex;"><img class="icon-img" src="<?php echo base_url(); ?>public/icons/export.png" alt="export" style="width: 15px;height: 22px;">Export</a></li>
                      <li><a id="print_multiple_customers"><i class="material-icons">print</i>Print</a></li>
                    </ul>
                    <a class="filter-search btn-search btn">
                    <input type="text" name="search" id="search_cli" class="search-hide-show" style="display:none" />
                    <i class="material-icons ser search-btn-field-show">search</i>
					</a>
                </div>

                 <div class="col l8 s12 m12">
					<div class="action-btn-wapper right">
                    <div class="bulk location-drop">
                         <select class="select-stat js-example-basic-single" name="nature_buzz" id="nature_buzz">
							<option value="">NATURE BUSINESS</option>
							<option value="">ALL</option>
						  </select>
						  <select class="js-example-basic-single" name="event_location" id="event_location">
							<option value="">LOCATION</option>
							<option value="">ALL</option> 
                          </select>
                          <input type="text" placeholder="START DATE" class="btn-date icon-calendar-green rangedatepicker_list date-cng btn-stated out-line" id="c_start_date" name="c_start_date" readonly="readonly">
						  <input type="text" placeholder="END DATE" class="btn-date icon-calendar-red rangedatepicker_list date-cng btn-stated out-line" id="c_end_date" name="c_end_date" readonly="readonly">
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

		<div class="container">
			<div class="row">
				<div class="col s12 m12 l12" style="padding-top:3%;">
				<div class="offers_list">
					<div class="row">
						<div class="col s12 m12 l12">
						<div class="offer-1 box-wrapper bg-white shadow">
							<div class="col s12 m12 l3 img-offer">
								<img width="200" height="200" src="<?php echo base_url(); ?>public/images/img_bg_2.jpg" alt="offer-img" class="text-hide-collapsible logo_style_2"/>
							</div>
							<div class="col s12 m12 l9 offer-info">
								<div class="row">
									<div class="col s12 m12 l9">
										<h6><strong>LOREM IPSUM LOREM IPSUM LOREM IPSUM LOREM IPSUM LOREM IPSUM</strong></h6>
										<div class="col s12 m12 l6" style="margin-left:-12px;">
											<p class="offer_p"><strong>DATE:</strong> 26TH OCTOBER 2020</p>
										</div>
										<div class="col s12 m12 l6">
											<p class="offer_p"><strong>LOCATION:</strong> NEW DELHI</p>
										</div>
									</div>
									<div class="col s12 m12 l3">
										<!--img style="margin:0 0 0 0;" width="20" height="20" src="<?php //echo base_url(); ?>public/images/clock.png" alt="event-img" class="text-hide-collapsible logo_style_2"/-->
										<a class="btn btn-theme btn-large right set_remin" href="#">SET REMINDER</a>
									</div>
								</div>	
								<div class="row">
									<div class="col s12 m12 l12 offer_description">
										<p>Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum </p>
									</div>
								</div>	
								<div class="row offer-time" hidden>
									<div class="col s12 m12 l12">
										<p class="offer_p"><strong>ADDRESS:</strong> 12 DR. APJ ABDUL KALAM RD, MOTILAL NEHARU AREA, NEW DELHI, MUMBAI-110003</p>
										<p class="offer_p"><strong>TIME:</strong> 7:00 AM - 10:00 AM</p>
									</div>
								</div>
								<div class="row" style="text-align:center;">
									<div class="col l12 s12 m12">
										<a href="javascript:void(0);" class="more showmore" title="Show More">Show More</a>
										<a href="javascript:void(0);" class="less showmore" title="Show More" hidden>Show Less</a>
									</div>
								</div>	
							</div>
						</div>
						</div>
					</div>	
				</div>
				</div>
            </div>
        </div>
		</section>
	</div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
		$('.js-example-basic-single').select2();
	});
</script>
<script type="text/javascript">
    $(document).ready(function() {
		$(".showmore").click(function(){
			if($(this).hasClass('active')){
				$(".offer-time").hide();
				$(".showmore").removeClass('active');
				$('.less').hide();
				$('.more').show();				
			}else{
				$(".offer-time").show();
				$(".showmore").addClass('active');	
				$('.less').show();
				$('.more').hide();				
			}
		});
	});

	function reset_clientfilter() {
		$('.action-btn-wapper').find('select').prop('selectedIndex',0);
		$('.js-example-basic-single').trigger('change.select2');
		$('.btn-date, .search-hide-show').val('');
		customerDatatable(base_path()+'sales/get_customers_profiles/','my-customers');
		$('select').material_select();
	}
</script>
<?php $this->load->view('template/footer'); ?>
