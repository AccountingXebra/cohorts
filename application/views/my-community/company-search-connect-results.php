<style>
	@media only screen and (max-width: 600px) {
		.leftSide .white-box{ padding-bottom:50px !important; width:100% !important; }
		.rightSide{ margin-top:204% !important; height:90px; }
		.rightSide .page-header{ margin:-781px 0 0 2% !important; }
		.rightSide .page-content{ margin:-865px 0 0 2% !important; }
		.searchmark{ margin-top: 50px; margin-left: 10px; }
		.upD #search_update .white-box.boxx{ max-width: 95% !important; min-width: 95%; }
		.decon .connectdeal{  width: 158% !important; margin: 12px 0 0 -190px !important; }
		.page-content.result-content{ margin-left:-1% !important; }
		.serachBy{ width:100% !important; margin-left:-68% !important; }
		.serachBy #search_comp{ margin-right:20px !important; }
	}
	.result-content{ /*(when ratings is added) margin: -970px 0 0 155px !important;*/margin: -810px 0 0 14% !important; width: 87% !important }
	.page-header{ margin: -763px 0 0 260px !important; }
	.white-box{ margin-left: -10px !important; padding-bottom: 450px !important; width: 85% !important; }
	.rightSide{ margin-top:-1.3%; }
	.upD{ margin-left:13% !important; }
	.connectdeal{ text-align: right; width: 63%; margin-top: -4.8%; margin-left: 160px !important; }
	.searchmark{ margin-top: 50px; margin-left: 140px; }
	.select-wrapper label.error:not(.active) {
	margin: -30px 0 0 -11px;
	}


	/*----------START SEARCH DROPDOWN CSS--------*/
	.select2-container {
	width: 100% !important;
	}

	[type="radio"]:not(:checked) + label, [type="radio"]:checked + label {
    position: relative;
    padding-left: 20px;
    cursor: pointer;
    display: inline-block;
    height: 25px;
    line-height: 25px;
    font-size: 1rem;
    -webkit-transition: .28s ease;
    transition: .28s ease;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
}

[type="radio"] + label:before, [type="radio"] + label:after {
    content: '';
    position: absolute;
    left: 0;
    top: 0;
    margin: 4px 0 0 0;
    width: 16px;
    height: 16px;
    z-index: 0;
    -webkit-transition: .28s ease;
    transition: .28s ease;
}

body.easy-tab1 [type="radio"] + label:before, [type="radio"] + label:after {
    content: '';
    position: absolute;
    left: 0;
    top: 0;
    margin: 3px 0 0 -2px;
    width: 20px;
    height: 20px;
    z-index: 0;
    -webkit-transition: .28s ease;
    transition: .28s ease;
}

	.select2-container--default .select2-selection--multiple {
	border:1px solid #afb8c9 !important;
	border-radius:5px !important;
	}

	input[type="search"]:not(.browser-default) {
	height: 30px;
	font-size: 14px;
	margin: 0;
	margin-top:8px !important;
	border-radius: 5px;
	}

	.select2-container--default .select2-selection--multiple .select2-selection__rendered {
	font-size: 13px;
	line-height: 35px;
	color: #666;
	font-weight: 400;
	}

	.select2-container--default .select2-selection--multiple .select2-selection__arrow {
	height: 32px;
	}

	.select2-search--dropdown {
	padding: 0;
	margin-top:-10px !important;
	}

	input[type="search"]:not(.browser-default):focus:not([readonly]) {
	border-bottom: 1px solid #bbb;
	box-shadow: none;
	}

	.select2-container--default .select2-selection--multiple:focus {
	outline: none;
	}

	.select2-container--default .select2-results__option--highlighted[aria-selected] {
	background: #fffaef;
	color: #666;
	}

	.select2-container--default .select2-results > .select2-results__options {
	font-size: 14px;
	border-radius: 5px;
	box-shadow: 0px 2px 6px #B0B7CA;
	}

	.select2-dropdown {
	border: none;
	border-radius: 5px;
	}

	.select2-container .select2-selection--multiple {
	height: 48px;
	}

	.select2-container .select2-selection--single {
	height: 48px;
	}

	.select2-results__option[aria-selected] {
	border-bottom: 1px solid #f2f7f9;
	padding: 14px 16px;
	border-top-left-radius: 0;
	}

	.select2-container--default .select2-search--dropdown .select2-search__field {
	/* border: none;*/
	border: 1px solid #d0d0d0;
	padding: 0 0 0 15px !important;
	width: 93.5%;
	max-width: 100%;
	background: #fff;
	border-radius: 4px;
	border-top-left-radius: 4px;
	border-top-right-radius: 4px;
	}

	input[type="radio"]+span:before, input[type="radio"]+span:after {
		left: -15px !important;
    	top: 4px !important;
	}

	.select2-container--open .select2-dropdown--below {
	margin-top: 0px;
	/*border-top: 1px solid #aaa;
	border-radius: 5px !important;*/
	}

	ul#select2-currency-results {
	width: 97%;
	}

	/*---Dropdown error message format---*/
	.select-wrapper + label.error{
	margin: 18px 0 0 -10px;
	} 

	select.error + label.error:not(.active){
	margin: -20px 0 0 -10px; 
	}

	select + label.error.active{
	margin-left: -10px;
	}

	body {
	/*overflow-y:hidden;*/
	}

	#search_comp[type=text] {
	width: 55% !important;
	box-sizing: border-box !important;
	border: 1px solid #ccc !important;
	border-radius:5px !important;
	font-size: 16px !important;
	background-color: none !important;
	background-image: url('<?php echo base_url(); ?>asset/css/img/icons/search.png') !important;
	background-position: 10px 10px !important; 
	background-repeat: no-repeat !important;
	padding: 12px 20px 12px 40px !important;
	outline: none !important;
	}

	#select2-market_country-container{
	margin-top:10px !important;
	}

	.select2-container--default .select2-selection--single .select2-selection__arrow {
	top:10px !important;
	}

	/* Select Box css */

	.multi-loc input[type=text]:not(.browser-default) {
	font-size: 13px !important;
	padding-left:10px !important;
	color: #9e9e9e !important;
	}

	.multi-loc .dropdown-content.select-dropdown {
	margin-top:47px !important;
	overflow-y: scroll !important;
	}

	.loc_select .select2{
	width:200px !important;
	}

	#nature input[type=text]:not(.browser-default) {
	font-size: 13px !important;
	padding-left:10px !important;
	color: #9e9e9e !important;
	}

	#nature .dropdown-content.select-dropdown {
	margin-top:47spx!important;
	overflow-y: scroll !important;
	}

	.loc_select{
		width:105% !important;
		border:1px solid #e8ebf4;
		height:50px;
		border-radius:5px;
		padding-left:0px;
		margin-bottom:20px;
	}

	.loc_select input[type=text] {
	margin-left: 10px;
	padding-top: 3px;
	}

	.nat_select{
	width:240px !important;
	margin-left:23px;
	border:1px solid #e8ebf4;
	height:50px;
	border-radius:5px;
	padding-left:15px;
	margin-bottom:20px;
	}
	/* Select Box css end */

	.page-link{
	font-size:13px;
	}

	.multi-loc .select-dropdown {
	border:1px solid #afb8c9 !important;
	border-radius:5px !important;
	}

	.loc_title{
	font-size:14px !important;
	}

	.select_loc{
	padding-left:10px; 
	padding-right:10px;
	padding-top:3px;
	padding-bottom:3px;		   
	text-align:center; 
	width:15px !important; 
	height:15px !important;
	color:#ffff; 
	background-color:#B0B7CA !important; 
	border-radius:15px; 
	font-size:14px;
	}
	
	.city-market .dropdown-content.select-dropdown{
		overflow-y: scroll !important;
		height:300px !important;
		margin-top:50px !important;
	}
	
	.natbuzz .dropdown-content.select-dropdown{
		overflow-y: scroll !important;
		height:300px !important;
		margin-top:50px !important;
	}
	
	.city-market .select-wrapper span.caret {
		margin: 20px 12px 0 0 !important;
	}
	
	.natbuzz .select-wrapper span.caret {
		margin: 20px 12px 0 0 !important;
	}

	.remove_loc{
	padding-left:5px !important;
	color: white !important;
	}

	.comp_name{
	font-size:14px;
	color:#000000;
	}

	.rate-star{
	padding:10px 0  10px 10px !important;
	margin-top:-5px;
	}

	#five_star + label {
	padding:0 0 0 25px !important;
	}

	#four_star + label {
	padding:0 0 0 25px !important;
	}

	#three_star + label {
	padding:0 0 0 25px !important;
	}

	#two_star + label {
	padding:0 0 0 25px !important;
	}

	#one_star + label {
	padding:0 0 0 25px !important;
	}

	[type="checkbox"] + label:before, [type="checkbox"]:not(.filled-in) + label:after {
		margin: 4px 0 0 0 !important;		
	}

	.rate-btn{
		background-color:#7965E9 !important;
		color: white !important;
		border:1px solid #7965E9;
		border-radius:5px;
	}

	.modalheader {
	    padding: 20px;
	}
	
	.select2-container--default .select2-selection--multiple .select2-selection__choice {
		background-color: transparent;
		border: none ;
	}
	
	input[type="search"]:not(.browser-default):focus:not([readonly]) {
		border-bottom: none;
    }
	
	.select2-container--default .select2-selection--multiple{
		background-color:transparent !important;
	}
	
	.city-market .select2-container--default .select2-search--inline .select2-search__field{
		padding-left:5px !important;
	}
	
	::placeholder{
		color:#000 !important;
		font-weight:500;
	}

</style>

<!-- START MAIN -->
<div id="main" style="padding:10px 0 0 0px !important;">
	<!-- START WRAPPER -->
	<div class="wrapper">
		<!-- START CONTENT -->
		<section id="content">

			<!--Form-->
			<form action="<?php echo base_url();?>community/search-company" method="GET" id="search-rslt-form">
				<?php $csrf = array(
					'name' => $this->security->get_csrf_token_name(),
					'hash' => $this->security->get_csrf_hash()
				);
				?>
				<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
				<!--Row-->
				<div class="row">

					<!--Col-->
					<div class="col s12 m12 l3 leftSide" style="margin-right:-10px !important;">

						<!--Filter by-->
						<div class="col l12 m12 s12 white-box">
							<div class="row border-bottom" style="margin-left:-12px !important; margin-bottom:12px;">
								<h6 class="white-box-title" style="padding: 11px 25px !important">Filter by</h6>
							</div>

							<!--Nature of Business-->
							<div class="row form-group" style="margin-top: -25px !important;">
								<div class="col s12 m12 l12">
									<div class="col s12 m12 l12">
										<!--label>NATURE OF BUSINESS</label-->
									</div>								
									<div class="col s12 m12 l12 natbuzz" style="margin-top: 20px !important; margin-left: -15px !important;">
										<select  id="nature_of_business" name="nature_of_business" class="loc_select js-example-basic-multiple form-control"  data-live-search="true">
											<?php if($all_data['nature_of_business']!="") { ?>
												<option value="<?php echo $all_data['nature_of_business'];?>" selected><?php echo strtoupper($all_data['nature_of_business']);?></option>
											<?php } ?>
											<option value="">BUSINESS CATEGORY</option>
											<option value="Accounting & Taxation">ACCOUNTING & TAXATION</option>
											<option value="Advertising">ADVERTISING</option>
											<option value="Animation Studio">ANIMATION STUDIO</option>
											<option value="Architecture">ARCHITECTURE</option>
											<option value="Arts & Crafts">ARTS & CRAFTS</option>
											<option value="Audit & Tax">AUDIT & TAX</option>
											<option value="Brand Consulting">BRAND CONSULTING</option>
											<option value="Celebrity Management">CELEBRITY MANAGEMENT</option>
											<option value="Consultant">CONSULTANT</option>
											<option value="Content Studio">CONTENT STUDIO</option>
											<option value="Cyber Security">CYBER SECURITY</option>
											<option value="Data Analytics">DATA ANALYTICS</option>
											<option value="Digital Influencer">DIGITAL INFLUENCER</option>
											<option value="Digital & Social Media">DIGITAL & SOCIAL MEDIA</option>
											<option value="Direct Marketing">DIRECT MARKETING</option>
											<option value="Entertainment">ENTERTAINMENT</option>
											<option value="Event Planning">EVENT PLANNING</option>
											<option value="Florist">FLORIST</option>
											<option value="Foreign Exchange">FOREIGN EXCHANGE</option>
											<option value="Financial and Banking">FINANCIAL & BANKING</option>
											<option value="Gaming Studio">GAMING STUDIO</option>
											<option value="DESIGN & UI/UX">DESIGN & UI/UX</option>
											<option value="Hardware Servicing">HARDWARE SERVICING</option>
											<option value="Industry Bodies">INDUSTRY BODIES</option>
											<option value="Insurance">INSURANCE</option>
											<option value="Interior Designing ">INTERIOR DESIGNING</option>
											<option value="Legal Firm">LEGAL FIRM</option>
											<option value="Media Planning & Buying">MEDIA PLANNING & BUYING</option>
											<option value="Mobile Services">MOBILE SERVICES</option>
											<option value="Music">MUSIC</option>
											<option value="Non-Profit">NON-PROFIT</option>
											<option value="Outdoor / Hoarding">OUTDOOR / HOARDING</option>
											<option value="Photography">PHOTOGRAPHY</option>
											<option value="Printing">PRINTING</option>
											<option value="Production Studio">PRODUCTION STUDIO</option>
											<option value="PR / Image Management">PR / IMAGE MANAGEMENT</option>
											<option value="Publishing">PUBLISHING</option>
											<option value="Real Estate">REAL ESTATE</option>
											<option value="Recording Studio">RECORDING STUDIO</option>
											<option value="Research">RESEARCH</option>
											<option value="Sales Promotion">SALES PROMOTION</option>
											<option value="Staffing & Recruitment">STAFFING & RECRUITMENT</option>
											<option value="Stock & Shares">STOCK & SHARES</option>
											<option value="Technology (AI, AR, VR)">TECHNOLOGY (AI, AR, VR)</option>
											<option value="Tours & Travel">TOURS & TRAVELS</option>
											<option value="Training & Coaching">TRAINING & COACHING</option>
											<option value="Translation & Voice Over">TRANSLATION & VOICE OVER</option>
											<option value="Therapists">THERAPISTS</option>
											<option value="Visual Effects / VFX">VISUAL EFFECTS / VFX</option>
											<option value="Web Development">WEB DEVELOPMENT</option>
										</select>
									</div>
								</div>
							</div> <!--Nature of Business-->

							<!--Location-->
							<div class="row form-group">
								<div class="col s12 m12 l12" style="margin-top: -40px;">
									<div class="col s12 m12 l12">
										<!--label>LOCATION</label-->
									</div>	

									<div class="col s12 m12 l12 city-market" style="margin-top: 20px !important; margin-left: -15px !important; width:104%;">
										<select id="multi_loc" name="multi_loc[]" class="loc_select js-example-basic-multiple form-control"  multiple="multiple" data-live-search="true">
											<option disabled value="">SELECT CITY</option>
											<?php if(count($cities)>0) { 
												for($i=0;$i<count($cities);$i++) {
												//if(count($post_data['multi_loc'])>0) {?>
												<!--<option value="<?php //echo $cities[$i]['city_id']; ?>"
												<?php //echo in_array($cities[$i]['city_id'], $post_data['multi_loc'])?'selected':''; ?>><?php //echo strtoupper($cities[$i]['name']); ?></option> 	<?php //} else { ?>-->
												<option value="<?php echo $cities[$i]['city_id']; ?>"><?php echo strtoupper($cities[$i]['name']); ?></option><?php } }?>
										</select>	
									</div>
								</div>
							</div> <!--Location-->
							
							<div class="row form-group">
								<div class="col s12 m12 l12" style="margin-top: -10px; text-align:right;">
									<div class="col s12 m12 l12">
										<!--label>LOCATION</label-->
									</div>		

									<div class="col s12 m12 l12" style="margin-left: -15px !important;">
										<input type="submit" style="width: 30%;" id="submit" name="submit" class="btn btn-welcome" value="APPLY" hidden>
									</div>
								</div>
							</div> <!--Location-->

							<!--Rating-->
							<div class="row" style="margin-top: -20px;">
								<div class="col s12 m12 l12">
									<!--label>RATING</label-->
								</div>	

								<!--div class="col s12 m12 l12 rate-star" style="margin-left: -8px !important">

									<div style="float: left; width: 30%;">
										<input type="checkbox" id="five_star"  name="five_star"/>
										<label class="label-star" for="five_star">5 Star</label>
									</div>

									<div style="float:left; width:70%; text-align:right; margin-top:-5px;">
										<span style="font-size:150%; color:#50e3c2;">&starf;</span>
										<span style="font-size:150%; color:#50e3c2;">&starf;</span>
										<span style="font-size:150%;color:#50e3c2;">&starf;</span>
										<span style="font-size:150%;color:#50e3c2;">&starf;</span>
										<span style="font-size:150%;color:#50e3c2;">&starf;</span>
									</div>

									<div style="float:left; width:30%;">
										<input type="checkbox" id="four_star"  name="four_star"/><label class="label-star" for="four_star">4 Star</label>
									</div>

									<div style="float:left; width:70%; text-align:right;margin-top:-5px;">
										<span style="font-size:150%; color:#50e3c2;">&starf;</span>
										<span style="font-size:150%; color:#50e3c2;">&starf;</span>
										<span style="font-size:150%;color:#50e3c2;">&starf;</span>
										<span style="font-size:150%;color:#50e3c2;">&starf;</span>
									</div>

									<div style="float:left; width:30%;">
										<input type="checkbox" id="three_star"  name="three_star"/><label class="label-star" for="three_star">3 Star</label>
									</div>

									<div style="float:left; width:70%; text-align:right; margin-top:-5px;">
										<span style="font-size:150%; color:#50e3c2;">&starf;</span>
										<span style="font-size:150%; color:#50e3c2;">&starf;</span>
										<span style="font-size:150%;color:#50e3c2;">&starf;</span>
									</div>

									<div style="float:left; width:30%;">
										<input type="checkbox" id="two_star"  name="two_star"/><label class="label-star" for="two_star">2 Star</label>
									</div>

									<div style="float:left; width:70%; text-align:right; margin-top:-5px;">
										<span style="font-size:150%; color:#50e3c2;">&starf;</span>
										<span style="font-size:150%; color:#50e3c2;">&starf;</span>
									</div>

									<div style="float:left; width:30%;">
										<input type="checkbox" id="one_star"  name="one_star"/><label class="label-star" for="one_star">1 Star</label>
									</div>

									<div style="float:left; width:70%; text-align:right; margin-top:-5px;">
										<span style="font-size:150%; color:#50e3c2;">&starf;</span>
									</div>
								</div-->
							</div> <!--Rating-->

						</div> <!--Filter by-->

					</div><!--Col-->
				</div> <!--Row-->

				<!--Apply-->
				<div>
					<input type="number" style="margin-top: -68% !important; margin-left: 12% !important; width: 6%; display:none;" id="" name="" class="btn btn-welcome" value="APPLY">
				</div> <!--Apply-->

			</form> <!--Form-->

			<!--Breadcrumps-->
			<div class="col s12 m12 l9 rightSide" style="margin-left:-2px !important;">
				<div class="page-header">
					<div class="container">
						<h2 class="page-title">My Connections</h2>
						<ol class="breadcrumbs" style="margin-top: 10px !important;">
							<li>
							<a class="page-link" href="<?= base_url(); ?>community/my-marketplace">MY COMMUNITY</a>
							/
							<a class="page-link" href="<?= base_url(); ?>community/my-marketplace">MY Connections</a>
							/
							<a class="page-link">COMPANIES SEARCH RESULTS</a>
							</li>
						</ol>
					</div>
				</div>
			</div> <!--Breadcrumps-->


			<!--Page Content-->
			<div class="page-content result-content"> 

				<!--Row-->
				<div class="row">
					<div class="col s12 m12 l12" style="margin-top: 20px !important;">

						<!--BELOW BREADCRUMPS-->
						<div class="col s12 m12 l12">	
							<div class="col s12 m12 l12">

								<!--Location-->
								<!--Location title-->
								<!--div class="col s1 m1 l2" style="width: 12%;">
									<label class="loc_title" style="margin-left: 155px;">LOCATION: </label>
								</div--> <!--Location title-->

								<!--div class="col s1 m1 l5 selected-city" style="width: 32% !important; margin-left: 125px !important;">

									<?php
										if(count($selected_cities)>0){
											for($i=0;$i<count($selected_cities);$i++) { ?>
											<label class="select_loc"><?php echo $selected_cities[$i]['name']; ?></label>
										
										<?php } }
									?>
								</div--> <!--Location-->

								<!-- No. of results found-->
								<div class="col s1 m1 l5" style="text-align: right; width: 100% !important; margin-top: 75px !important;">
									<label>TOTAL RESULTS FOUND: <span id='count_update'><?php echo count($result); ?></span> </label>
								</div> <!-- No. of results found-->
							</div>

						</div> <!--BELOW BREADCRUMPS-->


						<!--COMPANIES-->
						<div class="col s12 m12 l12 searchmark">

							<!-- Search Company -->
				            <div class="col s2 m6 l4 serachBy" style="height: 80px;">
				            	<form method="post" action="<?php echo base_url();?>community/company-search">
									<?php $csrf = array(
					'name' => $this->security->get_csrf_token_name(),
					'hash' => $this->security->get_csrf_hash()
				);
				?>
				<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
				                	<input type="text" id="search_comp" name="search" value="<?php if(@$post_data['search']){echo $post_data['search']; } ?>" placeholder="SEARCH BY COMPANY NAME" style="width: 80% !important; margin: -12px 0px 5px 77% !important;">
				                	<div class="col s12 m12 l12" style="text-align:center; margin-top:20px !important;">
										<input type="submit" name="submit" style="margin-top: -120px !important; margin-left: 166% !important; width: 25% !important;" id="submit" class="btn btn-welcome" value="Search">
									</div>
				                </form>

				                <div style="color: #ff85a1; margin-top: 15% !important; margin-left: 75%; font-size: 18px !important; width: 125% !important;">
									<?php  
										if (count($result) < 1) {
											echo "THERE ARE NO COMPANIES LISTED IN THIS CATEGORY";
										}
									?>
								</div>

				            </div>
				            <!-- End of Search Company -->

							<div class="col s12 m12 l12 upD" style="margin-top: -45px !important;">
								<div id="search_update">								

									<?php for($i=0;$i<count($result);$i++){ 
										$bus_id = $this->user_session['bus_id'];
										$reg_id = $this->user_session['reg_id'];
										if($result[$i]['bus_id']!=$bus_id || $result[$i]['reg_id']!=$reg_id){
											$code = $this->user_session['bus_id'];
											$connections = $this->Community_model->selectData('connections', '*',  array('bus_id' => $code, 'bus_id_connected_to' => $result[$i]['code']));
											//echo "<pre>"; print_r($connections); echo "</pre>";
											//print $this->db->last_query();
										?>
										<div class="col l6 m6 s6 white-box boxx" style="height: 190px !important; width: 42% !important; margin:20px 20px 0 0; padding: 10px 0 !important;">

											<!--Company Name, Nature of Business & Logo-->
											<div class="row" style="height: 55px; margin-bottom: 5px !important;">
												<div class="col s6 m6 l8" style="margin-top: 5px !important;">
													<label class="comp_name"><b><?php echo strtoupper($result[$i]['company_name']); ?></b></label><br>
													<label class="nature_buzz"><?php echo strtoupper($result[$i]['nature']); ?></label>
												</div>

												<?php
													$imgUrl=DOC_ROOT_DOWNLOAD_PATH_CO.'company_logos/'.$result[$i]['code'].'/'.$result[$i]['company_logo'];
													$filefound = "0";
													if (!file_exists($imgUrl)) {   
														$filefound = '1';
													}
													if($result[$i]['company_logo'] != '' && $filefound =='0') {
														$company_logo = '<img src="'.DOC_ROOT_DOWNLOAD_PATH_CO.'company_logos/'.$result[$i]['code'].'/'.$result[$i]['company_logo'].'" height="50px" class="logo_style_2" width="50px">';
													} else {
														$company_logo = '<img src="'.base_url().'asset/css/img/icons/company-icon.png" height="35px" class="logo_style_2" width="35px">';	
														//$company_logo = '<img src="'.base_url().'public/upload/company_logos/'.$result[$i]['code'].'/'.$result[$i]['company_logo'].'" height="50px" class="logo_style" width="50px">';
													}
												?>
												<div class="col s6 m6 l4 <?php echo $filefound; ?>" style="text-align: right">
													<?php echo $company_logo; ?>
												</div>
											</div> <!--Company Name, Nature of Business & Logo-->

											<!--Company location-->
											<div class="row" style="height:25px; margin-bottom:5px !important;">
												<div class="col s6 m6 l8">
													<div class="col l2 s1 m1" style="margin-left:-10px !important;">
														<i class="material-icons view-icons">location_on</i>
													</div>
													<div class="col l2 s1 m1" style="width: 40%;">
														<label> <?php echo strtoupper($result[$i]['name']); ?> </label>
													</div>
												</div>
												<div class="col s6 m6 l4"></div>
											</div> <!--Company location-->

											<!--View Profile, Connect, Rate me-->
											<div class="row decon" style="margin-bottom: 5px !important; margin-top: 10%;">
												<div class="col s6 m6 l6" style="font-size:14px;"> 	
													<a href="<?php echo base_url(); ?>community/view-company-details?token=<?php echo $result[$i]['code']; ?>" style="color:#7965E9;" onclick=""><b>VIEW PROFILE</b></a>
												</div>

												<div class="col s6 m6 l6 connectdeal">

													
													<?php 
													if(count($connections)>0){
														//print_r($connections);
														if($connections[0]->accepted == 0 && $connections[0]->status == 1){ ?>
															<a style="padding: 3px 7px;" class="rate-btn"><b>PENDING</b></a>
														<?php } elseif ($connections[0]->accepted == 1 && $connections[0]->status == 1) { ?>
															<a style="padding: 3px 7px;"  class="rate-btn"><b>CONNECTED</b></a>
														<?php } else {?>
															<a style="padding: 3px 7px;" target="_blank" href="#connect_modal<?=$result[$i]['code']?>" class="modal-trigger rate-btn"><b>CONNECT</b></a>
														<?php }												
													}else{?>
														<a style="font-size: 14px; padding: 3px 7px;" target="_blank" href="#connect_modal<?=$result[$i]['code']?>" class="modal-trigger rate-btn"><b>CONNECT</b></a>
													<?php } ?>


														<a style="font-size: 14px; padding: 3px 7px;" target="_blank" href="<?php echo base_url(); ?>community/company-deals?token=<?php echo $result[$i]['code']; ?>" class="modal-trigger rate-btn"><b>DEALS</b></a>


													


<!-- New Connection Modal -->
<div id="connect_modal<?=$result[$i]['code'];?>" class="modal modal-md modal-rating" style="height:auto; width: 40%;">
	<div class="modalheader">
		<h4 style="float: left; text-align: left !important; padding-left:20px;">Connection Box</h4>
		<a class="modal-close close-pop"><img src="<?php echo base_url();?>asset/images/popupdelete1.png"></a>
	</div>

	<hr style="border: 0.5px solid #ccc;">

	<div class="modalbody">
		<form class="addconn" id="add_connection_info" name="add_connection_info" method="post">
			<?php $csrf = array(
					'name' => $this->security->get_csrf_token_name(),
					'hash' => $this->security->get_csrf_hash()
				);
				?>
				<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
			<div class="row" style="margin-bottom:0px !important;">
				<div class="col l12 s10 m10 fieldset">
					<div class="row">

						<div class="col l12 s12 m12 fieldset org_details" style="text-align: center;">
							<p class="conn-org" style="text-align: center !important;"><b><?php echo $result[$i]['company_name']; ?></b></p>
							<label class="conn-org-tagline" style="font-size: 14px;">THIS COMPANY IS CONNECTED WITH ME AS</label>
							<input type="text" hidden name="con_to" id="con_to<?=$result[$i]['code'];?>" value="<?=$result[$i]['code']?>">
						</div>

						<div class="col l12 s12 m12 fieldset org_details radio-list" style="margin-top: 25px;">
							<div class="col l2 s10 m10 fieldset"></div>

							<div class="col l2 s12 m12 fieldset">
								<input type="radio" class="form-check-input" id="con-client<?=$result[$i]['code'];?>" name="connect">
								<label class="form-check-label" for="con-client<?=$result[$i]['code'];?>">CLIENT</label>
							</div>

							<div class="col l2 s12 m12 fieldset" style="margin: 0px 0 0 15px !important;">
								<input type="radio" class="form-check-input" id="con-vendor<?=$result[$i]['code'];?>" name="connect">
								<label class="form-check-label" for="con-vendor<?=$result[$i]['code'];?>">VENDOR</label>
							</div>

							<div class="col l2 s12 m12 fieldset">
								<input type="radio" class="form-check-input" id="con-both<?=$result[$i]['code'];?>" name="connect">
								<label class="form-check-label" for="con-both<?=$result[$i]['code'];?>">BOTH</label>
							</div>

							<div class="col l2 s12 m12 fieldset">
								<input type="radio" class="form-check-input" id="con-none<?=$result[$i]['code'];?>" name="connect">
								<label class="form-check-label" for="con-none<?=$result[$i]['code'];?>">NONE</label>
							</div>

							<div class="col l2 s10 m10 fieldset"></div>

						</div>

					</div>

					<div class="row">
						<div class="col l11 s12 m12" style="margin-left: 3.333333%; margin-top: 20px;">
							<div class="row notess note-mor">
								<div class="s12 m12 l12">

									<div class="note">
										<label for="message"></label>
										<textarea id="message<?=$result[$i]['code'];?>" placeholder="MESSAGE" type="text" name="message" class="bill-box ex-noss materialize-textarea ratingtextbox" style="height: 54px;min-height: 6rem;margin-bottom:0;"></textarea>
									</div>

								</div>
							</div>
						</div>
					</div>

				</div>

				<div class="row">
					<div class="col l6 m6 s12 fieldset"></div>
					<div class="col l6 m6 s12 fieldset buttonset">
						<div class="right">
							<button type="button" class="connecting modal-close btn-flat theme-primary-btn theme-btn theme-btn-large right" onclick="con_company('<?php echo $result[$i]['code'];?>')">CONNECT</button>
							<button type="button" class="modal-close btn-flat theme-flat-btn theme-btn theme-btn-large modal-close">CANCEL</button>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div> <!-- End of New Connection Modal -->

													<!--Rating Modal-->
													<!--div id="rating_modal<?=$result[$i]['bus_id']?>" class="modal modal-rating" style="max-height:100%;height:75%; width:56%;">
														<img class="geen" src="<?php echo base_url();?>asset/images/green.png" alt="green">

														<form name="rating_frm" id="rating_frm" method="post">

															<div class="modal-content">
																<div class="modal-header">
																	<a class="modal-close close-pop"><img src="<?php echo base_url();?>asset/images/popupdelete.png" alt="close"></a>
																</div>
															</div>

															<div class="modal-content" style="padding: 24px 0 0 0;">

																<div class="row">
																	<h4 style="width: unset;margin: 0 0 0 0px;font-size: 20px;text-align: center;">How likely are you to recommend <b> <?php echo $result[$i]['bus_company_name']; ?> </b> to a friend or colleague?</h4>
																</div>

																<div class="row">
																	<div class="col l10 s12 m12 offset-l1"-->
																		<!-- offset-s6 -->
																		<!--div class="rating-rating">
																			<label class="radio-lable">
																				<input name="comp_id" class="rate" type="hidden" value="<?=$result[$i]['bus_id']?>"/>
																				<input name="rate" class="rate" type="radio" value="1"/>
																				<span>1</span>
																			</label>

																			<label class="radio-lable">
																				<input name="comp_id" class="rate" type="hidden" value="<?=$result[$i]['bus_id']?>"/>
																				<input name="rate" class="rate" type="radio" value="2"/>
																				<span>2</span>
																			</label>

																			<label class="radio-lable">
																				<input name="rate" class="rate" type="radio"  value="3"/>
																				<span>3</span>
																			</label>

																			<label class="radio-lable">
																				<input name="rate" class="rate" type="radio"value="4" />
																				<span>4</span>
																			</label>

																			<label class="radio-lable">
																				<input name="rate" class="rate" type="radio"   value="5"/>
																				<span>5</span>
																			</label>

																			<div class="">
																				<i class="Medium material-icons" style="margin-top: 17px; float: left;">thumb_down</i>
																				<i class="Medium material-icons" style="float:right; margin-top: 11px;">thumb_up</i>
																			</div>
																		</div>
																		<span id="rating_error"></span>
																	</div>
																</div>

																<div class="row">
																	<div class="col l11 s12 m12" style="margin-left: 3.333333%;">
																		<p class="title-note" style="margin-bottom: 15px;">Please tell us why you rated <?php echo $result[$i]['bus_company_name']; ?> <span id="rate_ratio"></span> out of 5?</p>
																		<div class="row notess note-mor">
																			<div class="s12 m12 l12">
																				<div class="note">
																					<textarea id="bill1" placeholder="GIVE YOUR FEEDBACK HERE" type="text" name="feedback" class="bill-box ex-noss materialize-textarea ratingtextbox" style="height: 54px;min-height: 6rem;margin-bottom:0;"></textarea>
																					<label for="bill1"></label>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>

															<div class="modal-footer">
																<div class="row">
																	<div class="col l11 s12 m12 cancel-deactiv pull-right" style="margin-left: 35px;">
																		<a class="modal-close btn-flat theme-flat-btn theme-btn theme-btn-large model-cancel" type="button">CANCEL</a>

																		<button class="btn-flat theme-primary-btn theme-btn theme-btn-large  modal-trigger dea rating_comp">SUBMIT</button>
																	</div>
																</div>
															</div>
														</form>
													</div--> <!--Rating Modal-->



													<!--Rating Picture-->
													<!--
													<label>
														<?php 
															$rating= $this->Community_model->selectData('rating', '*', array('bus_id'=> $result[$i]['bus_id']));
															// print_r($rating);

															$count=0;
															foreach($rating as $valueRate){
															$count+=$valueRate->rate;
															}

															if(sizeof($rating)>0) {
																$xyz = round($count/sizeof($rating));
																if($xyz==5) {
																	echo '<span style="font-size:150%; color:#50e3c2;">&starf;</span><span style="font-size:150%; color:#50e3c2;">&starf;</span><span style="font-size:150%;color:#50e3c2;">&starf;</span><span style="font-size:150%;color:#50e3c2;">&starf;</span><span style="font-size:150%;color:#50e3c2;">&starf;</span>';
																}

																if($xyz==4) {
																	echo '<span style="font-size:150%; color:#50e3c2;">&starf;</span><span style="font-size:150%; color:#50e3c2;">&starf;</span><span style="font-size:150%;color:#50e3c2;">&starf;</span><span style="font-size:150%;color:#50e3c2;">&starf;</span>';
																}

																if($xyz==3) {
																	echo '<span style="font-size:150%; color:#50e3c2;">&starf;</span><span style="font-size:150%; color:#50e3c2;">&starf;</span><span style="font-size:150%;color:#50e3c2;">&starf;</span>';
																}

																if($xyz==2) {
																	echo '<span style="font-size:150%; color:#50e3c2;">&starf;</span><span style="font-size:150%; color:#50e3c2;">&starf;</span>';
																}

																if($xyz==1) {
																	echo '<span style="font-size:150%; color:#50e3c2;">&starf;</span>';
																}
															//print $count/sizeof($rating);
															} else {
																echo '<span style="font-size:150%; color:#ccc;">&starf;</span><span style="font-size:150%; color:#ccc;">&starf;</span><span style="font-size:150%;color:#ccc;">&starf;</span><span style="font-size:150%;color:#ccc;">&starf;</span><span style="font-size:150%;color:#ccc;">&starf;</span>';
															}
														?>
													-->
													</label> <!--Rating Picture-->

												</div>
											</div>
											<!--View Profile, Connect, Rate me-->
										</div><?php }?>
									<?php  }?>
								</div>
							</div> 
						</div> <!--COMPANIES-->

					</div>
				</div> <!--Row-->

			</div> <!--Page Content-->

		</section> <!-- END CONTENT -->
	</div> <!-- END WRAPPER -->
</div> <!-- END MAIN -->

<script type="text/javascript">
	$(document).ready(function(){
		if ($('.menu-icon').hasClass('open')) {
			$('.menu-icon').removeClass('open');

			$('.menu-icon').addClass('close');

			$('#left-sidebar-nav').removeClass('nav-lock');

			$('.header-search-wrapper').removeClass('sideNav-lock');

			$('#header').addClass('header-collapsed');

			$('#logo_img').show();
			$('#eazy_logo').hide();

			$('#left-sidebar-nav').toggleClass('nav-expanded nav-collapsed');

			$('#main').toggleClass('main-full');
		}
	});
</script>

<script type="text/javascript">
	$(document).ready(function() {
	//$('.js-example-basic-multiple').select2();
	$('.js-example-basic-single').select2();
	$('#multi_loc').select2({
	placeholder: 'SELECT LOCATION',
	});
	});
</script>

<script type="text/javascript">

	$(document).ready(function() {

	});

	function con_company(id) {

		var messages	= $('#message'+id).val();
		var con_to_id	= $('#con_to'+id).val();

		if($('#con-client'+id).is(":checked")) {
			con_client = 1;
			con_vendor = 0;
		}

		if($('#con-vendor'+id).is(":checked")) {
			con_vendor = 1;
			con_client = 0;
		}

		if($('#con-both'+id).is(":checked")) {
			con_vendor = 1;
			con_client = 1;
		}

		if($('#con-none'+id).is(":checked")) {
			con_vendor = 0;
			con_client = 0;
		}
		if(csrf_hash===""){
			csrf_hash=csrf_hash;
		}
		$.ajax({

			dataType: 'json',
			type: "POST",
			url:base_url+'Community/add_connection_info',

			data:{'csrf_test_name':csrf_hash,'client':con_client,'vendor':con_vendor,'message':messages, 'bus_id_connected_to':con_to_id},

			success: function(data){
				if(data['csrf_hash']){
					csrf_hash=data['csrf_hash'];
				}
				$('#connect_modal'+id).modal('close');
				
				$('#con-client'+id).val('');
				$('#con-vendor'+id).val('');
				$('#con-both'+id).val('');
				$('#con-none'+id).val('');
				$('#message'+id).val('');
				$('#con_to'+id).val('');
				 
				Materialize.toast('Your connection request has been sent', 5000, 'green rounded');
				//location.href=base_url+'community/search-company?nature_of_business=&submit=APPLY';
				setTimeout(function(){ 
					location.reload();
				}, 5000);
			}
		});
	}

</script>