<style type="text/css">

	#breadcrumbs-wrapper{
		padding-bottom:10px !important;
	}
	
	#search_comp[type=text] {
		width: 90% !important;
		box-sizing: border-box !important;
		border: 1px solid #ccc !important;
		border-radius:4px !important;
		font-size: 16px !important;
		background-color: white !important;
		background-image: url('<?php echo base_url(); ?>asset/css/img/icons/search.png') !important;
		background-position: 260px 10px !important; 
		background-repeat: no-repeat !important;
		padding: 12px 20px 12px 14px !important;
		outline: none !important;
	}
	
	#find_contact{
		height:45px;
		line-height:45px;
	}
	
	#view_sent_request{
		color:#50E2C2 !important;
		font-size:15px;
	}
	
	.request{
		padding-top:12px !important;
		/*border-left:1px solid #bec4d4;*/
		margin-left:-15px !important;
	}
	
	.top-label{
		font-size:13px;
		margin-top:30px;
	}
	
	.content-div{
		margin-top:20px;
		margin-left:20px;
	}
	
	.company-div{
		height: 250px;
		width: 24% !important;
		margin: 5px;
		padding: 10px;
		border-radius:5px;
	}
	
	.company-div:hover{
		box-shadow: 0 1px 10px #7864e94d;
	}
	
	.white-box{
		background-color:#fff !important;
	}
	
	.div_close{
		margin-right:8px; 
		margin-top:15px;
	}
	
	.text-center{
		text-align:center;
	}
	
	.org-name{
		margin:8px 0;
		font-size:15px !important;
	}
	
	.connect{
		background-color:#fff !important;
		border:1px solid #7864e9 !important;
		color: #7864e9 !important;
		height:35px !important;
		line-height:35px !important;
		margin:15px 0;
	}
	
	.com-loc{
		font-size:20px !important;
	}
	
	.org-tagline{
		font-size:13px !important;
	}
	
	.org-loc{
		font-size:12px !important;
	}
		
	.org_details{
		background-color:#fff !important;
		text-align:center;
	}
		
	#new-connection{
		overflow-x:hidden !important;
		top:25% !important;
	}
	
	#new-connection .modalheader{
		padding:22px 30px !important;
	}
	
	#new-connection .conn-org{
		text-align:center;
		font-size:14px;
	}
	
	#new-connection .conn-org-tagline{
		font-size:12px;
	}
	
	.radio-list{
		padding:30px 0 !important;
	}
	
	 #con-client[type="radio"] + label:after {
		 margin:1px !important;
	 }
	 
	 #con-vendor[type="radio"] + label:after {
		 margin:1px !important;
	 }
	
	 #con-both[type="radio"] + label:after {
		 margin:1px !important;
	 }
	 
	 .suceess-msg{
		 font-size:14px;
	 }
	 
	.top-activity{
		text-align:right;
		font-size:12px;
		margin-top:30px;
		margin-right:35px;
	}
	
	.close-pop{
		cursor:pointer !important;
	}

</style>

<!-- START MAIN -->
<div id="main" style="padding:10px 0 0 0px !important;" height="80%">

  	<!-- START WRAPPER -->
  	<div class="wrapper">

    	<!-- START LEFT SIDEBAR NAV-->
    	<?php //$this->load->view('template/sidebar'); ?>
    	<!-- END LEFT SIDEBAR NAV-->

    	<!-- START CONTENT -->
    	<section id="content" class="bg-cp sales_invoices-search">
	     	<div id="breadcrumbs-wrapper">
	        	<div class="container">
		          	<div class="row">

		          		<!-- Breadcrumps -->
		            	<div class="col s10 m6 l4">
		              		<h5 class="breadcrumbs-title my-ex">My Connections</h5>
		              		<ol class="breadcrumbs">
		                		<li><a href="">MY COMMUNITY / MY CONNECTIONS</a>
		              		</ol>
		            	</div>
		            	<!-- End of Breadcrumps -->

	         		</div>
	       		</div>
	     	</div>
   		</section> <!-- END CONTENT -->
  	</div> <!-- END WRAPPER -->
</div> <!-- END MAIN -->

<script type="text/javascript">
	$(document).ready(function() {
		$('.div_close').on('click', function(){
			console.log('OK');
			$(this).closest(".company-div").remove();
		});
		$('.connecting').on('click', function(){
			console.log('OK');
			$("#conn").hide();
			console.log('BYE');
			$("#ok").show();
			var bus_id=$(this).val();
 		if(bus_id!=''){
 			srec_counter=0;
 			console.log('yeah');
	 		$.ajax({
					type: "POST",
					data: {'csrf_test_name':csrf_hash,'bus_id':bus_id},
					url: base_url+'connection/sent_receive',
					
	                success:function(result)
					{
						var data=JSON.parse(result);

                          $("#employee_name").val(data.name);
                           $("#employee_designation").val(data.designation);
                           $("#reporting_manager_name").val(data.reporting_to);
                           $("#reporting_managers_designation").val(data.manager_designation);

                        

						$('select').material_select();	
					}
	 		});
	 		}	
		});
		
	}); 
</script>