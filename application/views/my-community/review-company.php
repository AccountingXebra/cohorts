
<style type="text/css">
	#breadcrumbs-wrapper{
		padding-bottom:10px !important;
	}
	
	#search_comp[type=text] {
		width: 90% !important;
		box-sizing: border-box !important;
		border: 1px solid #ccc !important;
		border-radius:4px !important;
		font-size: 16px !important;
		background-color: white !important;
		background-image: url('<?php echo base_url(); ?>asset/css/img/icons/search.png') !important;
		background-position: 260px 10px !important; 
		background-repeat: no-repeat !important;
		padding: 12px 20px 12px 14px !important;
		outline: none !important;
	}
	
	#find_contact{
		height:45px;
		line-height:45px;
	}
	
	#view_sent_request{
		color:#50E2C2 !important;
		font-size:15px;
	}
	
	.request{
		padding-top:12px !important;
		/*border-left:1px solid #bec4d4;*/
		margin-left:-15px !important;
	}
	
	.top-label{
		font-size:13px;
		margin-top:30px;
	}
	
	.content-div{
		margin-top:20px;
		margin-left:20px;
	}
	
	.company-div{
		height: 250px;
		width: 24% !important;
		margin: 5px;
		padding: 10px;
		border-radius:5px;
	}
	
	.company-div:hover{
		box-shadow: 0 1px 10px #7864e94d;
	}
	
	.white-box{
		background-color:#fff !important;
	}
	
	.div_close{
		margin-right:8px; 
		margin-top:15px;
	}
	
	.text-center{
		text-align:center;
	}
	
	.org-name{
		margin:8px 0;
		font-size:15px !important;
	}
	
	.connect{
		background-color:#fff !important;
		border:1px solid #7864e9 !important;
		color: #7864e9 !important;
		height:35px !important;
		line-height:35px !important;
		margin:15px 0;
	}
	
	.com-loc{
		font-size:20px !important;
	}
	
	.org-tagline{
		font-size:13px !important;
	}
	
	.org-loc{
		font-size:12px !important;
	}
		
	.org_details{
		background-color:#fff !important;
		text-align:center;
	}
		
	#new-connection{
		overflow-x:hidden !important;
		top:25% !important;
	}
	
	#new-connection .modalheader{
		padding:22px 30px !important;
	}
	
	#new-connection .conn-org{
		text-align:center;
		font-size:14px;
	}
	
	#new-connection .conn-org-tagline{
		font-size:12px;
	}
	
	.radio-list{
		padding:30px 0 !important;
	}
	
	 #con-client[type="radio"] + label:after {
		 margin:1px !important;
	 }
	 
	 #con-vendor[type="radio"] + label:after {
		 margin:1px !important;
	 }
	
	 #con-both[type="radio"] + label:after {
		 margin:1px !important;
	 }
	 
	 .suceess-msg{
		 font-size:14px;
	 }
	 
	.top-activity{
		text-align:right;
		font-size:12px;
		margin-top:30px;
		margin-right:35px;
	}
	
	.close-pop{
		cursor:pointer !important;
	}

	.plain-page-header {
    	padding: 20px 0;
	}
	textarea {
		border: 1px solid #ccc !important;
		height: 100% !important;
		width: 100% !important;
	}

	textarea:focus::placeholder {
  		color: transparent;
	}

</style>
<!-- START MAIN -->
<div id="main" height="80%">

  	<!-- START WRAPPER -->
  	<div class="wrapper">

	    <!-- START LEFT SIDEBAR NAV -->
	    <?php //$this->load->view('template/sidebar'); ?>
	    <!-- END LEFT SIDEBAR NAV -->

	    <!-- START CONTENT -->
		<div class="container">
			<div class="plain-page-header">
				<div class="row">
					<div class="col l6 s12 m6">
						<a class="go-back underline" href="<?php echo base_url();?>community/my-marketplace">Back to My Marketplace</a>
					</div>
				</div>
			</div>
		</div>

	    <!--Start of Breadcrumps-->
		<div class="col s12 m12 l9" style="margin-top: -20px;">
			<div class="page-header">
				<div class="container">
					<h2 class="page-title">Ratings and Reviews</h2>
					<ol class="breadcrumbs" style="margin-top: 10px !important;">
						<li>
						<a class="page-link" href="<?= base_url(); ?>community/my-marketplace">MY COMMUNITY</a>
						/
						<a class="page-link" href="<?= base_url(); ?>community/my-marketplace">MY MARKETPLACE</a>
						/
						<a class="page-link">REVIEW COMPANY</a>
						</li>
					</ol>
				</div>
				<br>
			</div>
		</div> <!--End of Breadcrumps-->

		<!-- Start of Rating -->
		<div class="col s12 m12 l9">
			<div class="page-header">
				<br>
				<div class="container">
					<h2 style="margin-bottom: 5px;" class="page-title">Rate <?php echo $company[0]['bus_company_name'];?></h2>
					<label>
						<?php 
							$rating= $this->Community_model->selectData('rating', '*', array('bus_id'=> $result[0]['bus_id']));
							// print_r($rating);

							$count=0;
							foreach($rating as $valueRate){
							$count+=$valueRate->rate;
							}

							if(sizeof($rating)>0) {
								$xyz = round($count/sizeof($rating));
								if($xyz==5) {
									echo '<span style="font-size:200%; color:#50e3c2;">&starf;</span><span style="font-size:200%; color:#50e3c2;">&starf;</span><span style="font-size:200%;color:#50e3c2;">&starf;</span><span style="font-size:200%;color:#50e3c2;">&starf;</span><span style="font-size:200%;color:#50e3c2;">&starf;</span>';
								}

								if($xyz==4) {
									echo '<span style="font-size:200%; color:#50e3c2;">&starf;</span><span style="font-size:200%; color:#50e3c2;">&starf;</span><span style="font-size:200%;color:#50e3c2;">&starf;</span><span style="font-size:200%;color:#50e3c2;">&starf;</span>';
								}

								if($xyz==3) {
									echo '<span style="font-size:200%; color:#50e3c2;">&starf;</span><span style="font-size:200%; color:#50e3c2;">&starf;</span><span style="font-size:200%;color:#50e3c2;">&starf;</span>';
								}

								if($xyz==2) {
									echo '<span style="font-size:200%; color:#50e3c2;">&starf;</span><span style="font-size:200%; color:#50e3c2;">&starf;</span>';
								}

								if($xyz==1) {
									echo '<span style="font-size:200%; color:#50e3c2;">&starf;</span>';
								}
							//print $count/sizeof($rating);
							} else {
								echo '<span style="font-size:200%; color:#ccc;">&starf;</span><span style="font-size:200%; color:#ccc;">&starf;</span><span style="font-size:200%;color:#ccc;">&starf;</span><span style="font-size:200%;color:#ccc;">&starf;</span><span style="font-size:200%;color:#ccc;">&starf;</span>';
							}
						?>
					</label> <!--Rating Picture-->
				</div>
				<br>
				<div class="container">
					<form method="post" action="">
						<?php $csrf = array(
					'name' => $this->security->get_csrf_token_name(),
					'hash' => $this->security->get_csrf_hash()
				);
				?>
				<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
						<label style="font-size: 17px; color: black;">Write a review</label>
						<textarea placeholder="WRITE YOUR REVIEW HERE" rows="5" cols="10"></textarea>
						<input type="submit" style="width: 9%; margin-top: 0px;" id="submit" name="submit" class="btn btn-welcome" value="SUBMIT">
					</form>
				</div>
				<br>
			</div>
		</div> <!-- End of Rating -->

		<!--Reviews-->
		<div class="col s12 m12 l9">
			<div class="page-header" style="border-bottom: none;">
				<br>
				<div class="container">
					<h2 class="page-title">Reviews</h2>
				</div>
				<br>
			</div>
		</div> <!-- End of Reviews -->



	</div> <!-- END WRAPPER -->
</div> <!-- END MAIN -->
    