 <?php $this->load->view('my-community/Cohorts-header');?>

    <!-- END HEADER -->
    <style type="text/css">
    .add-new {
         margin: 0 0 0 0 !important;
    }
     .section_li {
         margin: 0 0 0 0 !important;
    }
    .categories>li {
      float: none;
      display: inline-block;
    }
    a.btn.btn-theme.btn-large.section_li {
        background-color: #f8f9fd;
        color: #7864e9;
        border: 1px solid #7864e9;
    }
    .btn-theme{
            padding: 0 40px !important;
            border-radius: 6px !important;
            line-height: 45px !important;
            height: 45px !important;
      }
      ul.categories {
          text-align: center;
           margin:0;
           width: 100%;
      }
     /* li {
          padding: 0 5px 0 5px;

      }*/
      .tabs {
          height:auto;
          background: transparent !important;
          width: 100% !important;
      }
      ul.tabs li {
        border:none !important;
        float: left;
        width: 121px !important;
    }
    li.tab a.active {
      min-height: auto !important;
      }
  a.btn.btn-theme.btn-large.section_li.active {
    background-color: #7864e9 !important;
    color: #f8f9fd;
    border: 0px;
}
.tabs .tab{
  padding: 0 5px 0 5px !important;
}
.collapsible {
    border: 0px;
    -webkit-box-shadow: none;
  }
  .document-box-view.redborder.active {
    border-left: 4px solid #ff7c9b;

}
.collapsible-header{
  border-bottom:none;

}

.collapsible-header.active{

    padding-bottom: 0px;
        font-weight: 600;
}

.collapsible-body{
         padding: 0 1rem 1rem 1rem;
}
/*.collapsible-body span {
    color: #464646;
}*/
.red-bg-right:before {

    top: -32px !important;
  }
  .green-bg-right:after{
    bottom: -25px !important;
    left: 50px;
  }

  ul:not(.browser-default) > li .li_cls {
    list-style-type: disc !important;
}
	.btn-theme{
		padding:0px !important;
	}
    </style>
    <!-- START MAIN -->
    <div id="main" style="padding-left:0px !important;">
      <!-- START WRAPPER -->
      <div class="wrapper">
        <!-- START LEFT SIDEBAR NAV-->
        <?php //$this->load->view('template/sidebar'); ?>
        <!-- END LEFT SIDEBAR NAV-->
        <!-- START CONTENT -->
        <section id="content" class="bg-cp">
          <div id="breadcrumbs-wrapper" style="margin-bottom: -2%;">
            <div class="container custom">
              <div class="row">
                <div class="col s10 m6 l6">
                  <h5 class="breadcrumbs-title my-ex">Frequently Asked Questions</h5>
                </div>

              </div>

            </div>
        </div>
        <!--div id="bulk-action-wrapper">
          <div class="container custom">
              <div class="row">
                  <div class="col l12 s12 m12">
                    <ul class="tabs tabs-fixed-width tab-demo categories">
						<li class="tab"><a href="#test3" class="btn btn-theme btn-large section_li"><p style="padding: 0px; line-height: 18px; margin: 5px 0 0 0;">Business </br>Intelligence
						</p></a></li>
						<li class="tab"><a href="#test1" class="btn btn-theme btn-large section_li"><p style="padding: 0px; line-height: 18px; margin: 5px 0 0 0;">Sales </br>& Receipts</p></a></li>
						<li class="tab"><a  href="#test2"  class="btn btn-theme btn-large section_li"><p style="padding: 0px; line-height: 18px; margin: 5px 0 0 0;">Expenses </br>& Payments</p></a></li>
						<li class="tab"><a  href="#test7"  class="btn btn-theme btn-large section_li"><p style="padding: 0px; line-height: 18px; margin: 5px 0 0 0;">Assets & </br>Depreciation</p></a></li>
						<li class="tab"><a  href="#test8"  class="btn btn-theme btn-large section_li"><p style="padding: 0px; line-height: 18px; margin: 5px 0 0 0;">Employees </br>& Salary Expense</p></a></li>
						<li class="tab"><a  href="#test9"  class="btn btn-theme btn-large section_li"><p style="padding: 0px; line-height: 18px; margin: 5px 0 0 0;">Cash, Bank </br>& Cards</p></a></li>
						<li class="tab"><a href="#test4"  class="btn btn-theme btn-large section_li">Tax Queries</a></li>
						<li class="tab"><a href="#test10"  class="btn btn-theme btn-large section_li">Accountancy</a></li>
						<li class="tab"><a href="#test11"  class="btn btn-theme btn-large section_li">Profile & Subscription</a></li>
						<li class="tab"><a href="#test5"  class="btn btn-theme btn-large section_li"><p style="padding: 0px; line-height: 18px; margin: 5px 0 0 0;">Subscription </br>& Purchase</p></a></li>
						<li class="tab"><a href="#test6"  class="btn btn-theme btn-large section_li"><p style="padding: 0px; line-height: 18px; margin: 5px 0 0 0;">Security </br>& Privacy</p></a></li>
						<li class="tab"><a href="#test12"  class="btn btn-theme btn-large section_li"><p style="padding: 0px; line-height: 18px; margin: 5px 0 0 0;">Mobile </br>Application</p></a></li>
					</ul>

                   <!-- <a  href="#" class="btn btn-theme btn-large  add-new modal-trigger">ADD INVOICES</a>  -->
                 <!--/div>
              </div>
          </div>
        </div-->

          <div class="container custom">
              <div class="row">
                  <div class="col l12 s12 m12 mt-5 before-red-bg red-bg-right after-green-bg green-bg-right">
                  <div id="test1" class="col s12 inner_row_container">
                   <!--  <div class="row document-box-view blueborder"> -->
                    <ul class="collapsible">
                      <li class="row document-box-view redborder">
                        <div class="collapsible-header">What is Xebra Cohorts?</div>
                        <div class="collapsible-body"><span>
                          <p>Xebra cohorts is a community for founders. It allows them to learn & interact with each other, get access to their category events and deals. It’s also a place where you can share learnings and demonstrate thought leadership</p></span>
						</div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">Will Cohorts platform help me get more clients?</div>
                        <div class="collapsible-body"><span>
                          <p>Yes, you can network in the marketplace module and generate more business for your firm</p></span>
						</div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">Will I able to access Xebra Cohorts, even if I am not using any paid plans of Xebra?</div>
                        <div class="collapsible-body"><span>
                          <p>Xebra and Xebra Cohorts are two independent programs. You don’t have to subscribe to Xebra’s paid plans to be a part of Xebra Cohorts program</p></span>
						</div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I add or remove details from my company’s profile page?</div>
                        <div class="collapsible-body"><span>
                          <p>You can manage your profile by clicking on the profile dropdown on top right corner of the page</p></span>
						</div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">Can I add event that my company is organizing and invite fellow Cohort members?</div>
                        <div class="collapsible-body"><span>
                          <p>Yes, you can add your event details that fellow members can see. However, you can send any personalised invites to them. They can choose to attend if they so like</p></span>
						</div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">Do I have to pay to access the deals offered in Xebra Cohorts?</div>
                        <div class="collapsible-body"><span>
                          <p>No. These deals are made available to all the members of Cohorts.</p></span>
						</div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">Can I offers deals on my service to Cohort members?</div>
                        <div class="collapsible-body"><span>
                          <p>Yes. You need to fill in the details of the deals correctly and generate more business by offering it to fellow members</p></span>
						</div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">What is this referral program that you are offering? Can I participate?</div>
                        <div class="collapsible-body"><span>
                          <p>Referral program is a feature where by you are rewarded with money if you refer Xebra software in your network and they subscribe to our paid plans. Anyone can participate in it.</p></span>
						</div>
                      </li>
                     <li class="row document-box-view redborder">
                        <div class="collapsible-header">How does the referral program work?</div>
                        <div class="collapsible-body"><span><p>A unique referral code will be generated for you when you sign up.  You need to add this referral code when you subscribe to Xebra’s paid plans on behalf of your clients. In case the client is subscribing on their own, then they will need to enter it. Once the plan goes live, you will be able to track that entry in your referral page. That will display the total amount that you have earned that month from referral.</p></span></div>
                      </li>
                      <li class="row document-box-view redborder">
                        <div class="collapsible-header">Can I opt-out of the program at any stage</div>
                        <div class="collapsible-body"><span><p>Yes. We have ensured that this program doesn’t bind you in any manner</p></span></div>
                      </li>
                    </ul> <!-- </div> -->
                  </div>
                  
				  </div>
                </div>
              </div>

        </section>
        <!-- END CONTENT -->
        </div>
        <!-- END WRAPPER -->
      </div>
      <!-- END MAIN -->


      <!-- ================================================
    Scripts
    ================================================ -->
	<script>
			if ($('.menu-icon').hasClass('open')) {
				$('.menu-icon').removeClass('open');
				$('.menu-icon').addClass('close');
				$('#left-sidebar-nav').removeClass('nav-lock');
				$('.header-search-wrapper').removeClass('sideNav-lock');
				$('#header').addClass('header-collapsed');
				$('#logo_img').show();
				$('#eazy_logo').hide();
				$('#left-sidebar-nav').toggleClass('nav-expanded nav-collapsed');
				$('#main').toggleClass('main-full');
			}
		</script>
    <?php $this->load->view('template/footer'); ?>
