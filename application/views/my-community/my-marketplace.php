<!--link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script-->
<style>
	.select-wrapper label.error:not(.active) {
		margin: -30px 0 0 -11px;
	}

	/*----------START SEARCH DROPDOWN CSS--------*/
	.select2-container {
		width: 100% !important;
	}

	.select2-container--default .select2-selection--multiple {
		border:1px solid #afb8c9 !important;
		border-radius:5px !important;
	}

	input[type="search"]:not(.browser-default) {
		height: 30px;
		font-size: 14px;
		margin: 0;
		margin-top:8px !important;
		border-radius: 5px;
	}

	.select2-container--default .select2-selection--multiple .select2-selection__rendered {
		font-size: 13px;
		line-height: 35px;
		color: #666;
		font-weight: 400;
	}

	.select2-container--default .select2-selection--multiple .select2-selection__arrow {
		height: 32px;
	}

	.select2-search--dropdown {
		padding: 0;
		margin-top:-10px !important;
	}

	input[type="search"]:not(.browser-default):focus:not([readonly]) {
		border-bottom: 1px solid #bbb;
		box-shadow: none;
	}

	.select2-container--default .select2-selection--multiple:focus {
		outline: none;
	}

	.select2-container--default .select2-results__option--highlighted[aria-selected] {
		background: #fffaef;
		color: #666;
	}

	.select2-container--default .select2-results > .select2-results__options {
		font-size: 14px;
		border-radius: 5px;
		box-shadow: 0px 2px 6px #B0B7CA;
	}

	.select2-dropdown {
		border: none;
		border-radius: 5px;
	}

	.select2-container .select2-selection--multiple {
		height: 48px;
	}

	.select2-container .select2-selection--single {
		height: 48px;
	}

	.select2-results__option[aria-selected] {
		border-bottom: 1px solid #f2f7f9;
		padding: 14px 16px;
		border-top-left-radius: 0;
	}

	.select2-container--default .select2-search--dropdown .select2-search__field {
		/* border: none;*/
		border: 1px solid #d0d0d0;
		padding: 0 0 0 15px !important;
		width: 93.5%;
		max-width: 100%;
		background: #fff;
		border-radius: 4px;
		border-top-left-radius: 4px;
		border-top-right-radius: 4px;
	}

	.select2-container--open .select2-dropdown--below {
		margin-top: 0px;
		/*border-top: 1px solid #aaa;
		border-radius: 5px !important;*/
	}

	ul#select2-currency-results {
		width: 97%;
	}

	/*---Dropdown error message format---*/
	.select-wrapper + label.error{
		margin: 18px 0 0 -10px;
	} 

	select.error + label.error:not(.active){
		margin: -20px 0 0 -10px; 
	}

	select + label.error.active{
		margin-left: -10px;
	}

	body {
		/*overflow-y:hidden;*/
	}

	#search_comp[type=text] {
		width: 55% !important;
		box-sizing: border-box !important;
		border: 1px solid #ccc !important;
		border-radius:5px !important;
		font-size: 16px !important;
		background-color: none !important;
		background-image: url('<?php echo base_url(); ?>asset/css/img/icons/search.png') !important;
		background-position: 10px 10px !important; 
		background-repeat: no-repeat !important;
		padding: 12px 20px 12px 40px !important;
		outline: none !important;
	}

	#select2-market_country-container{
		margin-top:10px !important;
	}

	.select2-container--default .select2-selection--single .select2-selection__arrow {
		top:10px !important;
	}

	/* Select Box css */

	.multi-loc input[type=text]:not(.browser-default) {
		font-size: 13px !important;
		padding-left:10px !important;
		color: #9e9e9e !important;
	}

	.multi-loc .dropdown-content.select-dropdown {
		margin-top:47px !important;
		overflow-y: scroll !important;
	}

	.loc_select .select2{
		width:200px !important;
	}

	#nature input[type=text]:not(.browser-default) {
		font-size: 13px !important;
		padding-left:10px !important;
		color: #9e9e9e !important;
	}

	#nature .dropdown-content.select-dropdown {
		margin-top:47spx!important;
		overflow-y: scroll !important;
	}

	.loc_select{
		width:105% !important;
		border:1px solid #e8ebf4;
		height:50px;
		border-radius:5px;
		padding-left:0px;
		margin-bottom:20px;
	}

	.loc_select input[type=text] {
		margin-left: 10px;
		padding-top: 3px;
	}

	.nat_select{
		width:240px !important;
		margin-left:23px;
		border:1px solid #e8ebf4;
		height:50px;
		border-radius:5px;
		padding-left:15px;
		margin-bottom:20px;
	}
	
	.city-market .dropdown-content.select-dropdown{
		overflow-y: scroll !important;
		height:300px !important;
		margin-top:50px !important;
	}
	
	.natbuzz .dropdown-content.select-dropdown{
		overflow-y: scroll !important;
		height:300px !important;
		margin-top:50px !important;
	}
	
	.city-market .select-wrapper span.caret {
		margin: 20px 12px 0 0 !important;
	}
	
	.natbuzz .select-wrapper span.caret {
		margin: 20px 12px 0 0 !important;
	}
	/* Select Box css end */

	.page-link{
		font-size:13px;
	}

	.rate-star{
		padding:10px 0  10px 10px !important;
		margin-top:-5px;
	}

	#five_star + label {
		padding:0 0 0 25px !important;
	}

	#four_star + label {
		padding:0 0 0 25px !important;
	}

	#three_star + label {
		padding:0 0 0 25px !important;
	}

	#two_star + label {
		padding:0 0 0 25px !important;
	}

	#one_star + label {
		padding:0 0 0 25px !important;
	}

	[type="checkbox"] + label:before, [type="checkbox"]:not(.filled-in) + label:after {
		margin: 4px 0 0 0 !important;
	}
	
	.select2-container--default .select2-selection--multiple .select2-selection__choice {
		background-color: transparent;
		border: none ;
	}
	
	input[type="search"]:not(.browser-default):focus:not([readonly]) {
		border-bottom: none;
    }
	
	.select2-container--default .select2-selection--multiple{
		background-color:transparent !important;
	}
	
	.city-market .select2-container--default .select2-search--inline .select2-search__field{
		padding-left:5px !important;
	}
	
	::placeholder{
		color:#000 !important;
		font-weight:500;
	}
	.white-box{ margin-left: -10px !important; padding-bottom: 450px !important; width: 85% !important; }
	.page-header{ /*(when ratings is added) margin: -910.5px 0 0 260px !important;*/margin: -781px 0 0 19% !important; }
	.page-content{ /*(when ratings is added) margin: -970px 0 0 155px !important;*/margin: -45% 0 0 18% !important; width: 87% !important }
	@media only screen and (max-width: 600px) {
		.leftSide .white-box{ padding-bottom:50px !important; width:100% !important; }
		.rightSide{ margin-top:204%; height:90px; }
		.rightSide .page-header{ margin:-781px 0 0 2% !important; }
		.rightSide .page-content{ margin:-865px 0 0 2% !important; }
	}
	.view-profile{ background-color:#7864e9; }
	
</style>

<!-- START MAIN -->
<div id="main" style="padding-left:0px !important;">
	<!-- START WRAPPER -->
	<div class="wrapper">
		<!-- START CONTENT -->
		<section id="content">

			<!--Form-->
			<form action="<?php echo base_url();?>community/search-company" method="GET" id="search-comp-form">
				<?php $csrf = array(
					'name' => $this->security->get_csrf_token_name(),
					'hash' => $this->security->get_csrf_hash()
				);
				?>
				<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
				<!--Row-->
				<div class="row" style="padding-top:10px;">
					<!--Col-->
					<div class="col s12 m12 l3 leftSide" style="margin-right:-10px !important;">

						<!-- Filter by-->
						<div class="col l12 m12 s12 white-box">
							<div class="row border-bottom" style="margin-left:-12px !important; margin-bottom:-13px;">
								<h6 class="white-box-title" style="padding: 11px 25px !important">Filter by</h6>
							</div>

							<!--Nature of Business-->
							<div class="row form-group" style="margin-top: 0px !important;">
								<div class="col s12 m12 l12">
									<div class="col s12 m12 l12">
										<!--label>NATURE OF BUSINESS</label-->
									</div>								
									<div class="col s12 m12 l12 natbuzz" style="margin-top: 20px !important; margin-left: -15px !important;">
										<select  id="nature_of_business" name="nature_of_business" class="loc_select js-example-basic-multiple form-control"  data-live-search="true">
											<option value="">BUSINESS CATEGORY</option>
											<option value="Accounting & Taxation">ACCOUNTING & TAXATION</option>
											<option value="Advertising">ADVERTISING</option>
											<option value="Animation Studio">ANIMATION STUDIO</option>
											<option value="Architecture">ARCHITECTURE</option>
											<option value="Arts & Crafts">ARTS & CRAFTS</option>
											<option value="Audit & Tax">AUDIT & TAX</option>
											<option value="Brand Consulting">BRAND CONSULTING</option>
											<option value="Celebrity Management">CELEBRITY MANAGEMENT</option>
											<option value="Consultant">CONSULTANT</option>
											<option value="Content Studio">CONTENT STUDIO</option>
											<option value="Cyber Security">CYBER SECURITY</option>
											<option value="Data Analytics">DATA ANALYTICS</option>
											<option value="Digital Influencer">DIGITAL INFLUENCER</option>
											<option value="Digital & Social Media">DIGITAL & SOCIAL MEDIA</option>
											<option value="Direct Marketing">DIRECT MARKETING</option>
											<option value="Entertainment">ENTERTAINMENT</option>
											<option value="Event Planning">EVENT PLANNING</option>
											<option value="Florist">FLORIST</option>
											<option value="Foreign Exchange">FOREIGN EXCHANGE</option>
											<option value="Financial and Banking">FINANCIAL & BANKING</option>
											<option value="Gaming Studio">GAMING STUDIO</option>
											<option value="DESIGN & UI/UX">DESIGN & UI/UX</option>
											<option value="Hardware Servicing">HARDWARE SERVICING</option>
											<option value="Industry Bodies">INDUSTRY BODIES</option>
											<option value="Insurance">INSURANCE</option>
											<option value="Interior Designing ">INTERIOR DESIGNING</option>
											<option value="Legal Firm">LEGAL FIRM</option>
											<option value="Media Planning & Buying">MEDIA PLANNING & BUYING</option>
											<option value="Mobile Services">MOBILE SERVICES</option>
											<option value="Music">MUSIC</option>
											<option value="Non-Profit">NON-PROFIT</option>
											<option value="Outdoor / Hoarding">OUTDOOR / HOARDING</option>
											<option value="Photography">PHOTOGRAPHY</option>
											<option value="Printing">PRINTING</option>
											<option value="Production Studio">PRODUCTION STUDIO</option>
											<option value="PR / Image Management">PR / IMAGE MANAGEMENT</option>
											<option value="Publishing">PUBLISHING</option>
											<option value="Real Estate">REAL ESTATE</option>
											<option value="Recording Studio">RECORDING STUDIO</option>
											<option value="Research">RESEARCH</option>
											<option value="Sales Promotion">SALES PROMOTION</option>
											<option value="Staffing & Recruitment">STAFFING & RECRUITMENT</option>
											<option value="Stock & Shares">STOCK & SHARES</option>
											<option value="Technology (AI, AR, VR)">TECHNOLOGY (AI, AR, VR)</option>
											<option value="Tours & Travel">TOURS & TRAVELS</option>
											<option value="Training & Coaching">TRAINING & COACHING</option>
											<option value="Translation & Voice Over">TRANSLATION & VOICE OVER</option>
											<option value="Therapists">THERAPISTS</option>
											<option value="Visual Effects / VFX">VISUAL EFFECTS / VFX</option>
											<option value="Web Development">WEB DEVELOPMENT</option>
										</select>
									</div>
								</div>
							</div> <!--Nature of Business-->

							<!--Location-->
							<div class="row form-group">
								<div class="col s12 m12 l12" style="margin-top: -40px;">
									<div class="col s12 m12 l12">
										<!--label>LOCATION</label-->
									</div>		

									<div class="col s12 m12 l12 city-market" style="margin-top: 20px !important; margin-left: -15px !important; width:104% !important;">
										<select id="multi_loc" name="multi_loc[]" class="loc_select js-example-basic-multiple form-control"  multiple="multiple" data-live-search="true">
											<option disabled value="">SELECT CITY</option>

												<?php 
													for($i=0; $i<count($cities); $i++) {
														?> <option style="text-align: left !important" value="<?php echo $cities[$i]['city_id']; ?>"><?php echo strtoupper($cities[$i]['name']); ?></option> <?php
													}
												?>
										</select>	
									</div>
								</div>
							</div> <!--Location-->

							<div class="row form-group">
								<div class="col s12 m12 l12" style="margin-top: -10px; text-align:right;">
									<div class="col s12 m12 l12">
										<!--label>LOCATION</label-->
									</div>		

									<div class="col s12 m12 l12" style="margin-left: -15px !important;">
										<input type="submit" style="width: 30%;" id="submit" name="submit" class="btn btn-welcome" value="APPLY" hidden>
									</div>
								</div>
							</div> <!--Location-->

							<!--Rating-->
							<div class="row" style="margin-top: -20px;">
								<div class="col s12 m12 l12">
									<!--label>RATING</label-->
								</div>	

								<!--div class="col s12 m12 l12 rate-star">

									<div style="float: left; width: 30%;">
										<input type="checkbox" id="five_star"  name="star[]" value="5" />
										<label class="label-star" for="five_star">5 Star</label>
									</div>

									<div style="float:left; width:70%; text-align:right; margin-top:-5px;">
										<span style="font-size:150%; color:#50e3c2;">&starf;</span>
										<span style="font-size:150%; color:#50e3c2;">&starf;</span>
										<span style="font-size:150%;color:#50e3c2;">&starf;</span>
										<span style="font-size:150%;color:#50e3c2;">&starf;</span>
										<span style="font-size:150%;color:#50e3c2;">&starf;</span>
									</div>

									<div style="float:left; width:30%;">
										<input type="checkbox" id="four_star"  name="star[]" value="4" /><label class="label-star" for="four_star">4 Star</label>
									</div>

									<div style="float:left; width:70%; text-align:right;margin-top:-5px;">
										<span style="font-size:150%; color:#50e3c2;">&starf;</span>
										<span style="font-size:150%; color:#50e3c2;">&starf;</span>
										<span style="font-size:150%;color:#50e3c2;">&starf;</span>
										<span style="font-size:150%;color:#50e3c2;">&starf;</span>
									</div>

									<div style="float:left; width:30%;">
										<input type="checkbox" id="three_star"  name="star[]" value="3" /><label class="label-star" for="three_star">3 Star</label>
									</div>

									<div style="float:left; width:70%; text-align:right; margin-top:-5px;">
										<span style="font-size:150%; color:#50e3c2;">&starf;</span>
										<span style="font-size:150%; color:#50e3c2;">&starf;</span>
										<span style="font-size:150%;color:#50e3c2;">&starf;</span>
									</div>

									<div style="float:left; width:30%;">
										<input type="checkbox" id="two_star"  name="star[]" value="2" /><label class="label-star" for="two_star">2 Star</label>
									</div>

									<div style="float:left; width:70%; text-align:right; margin-top:-5px;">
										<span style="font-size:150%; color:#50e3c2;">&starf;</span>
										<span style="font-size:150%; color:#50e3c2;">&starf;</span>
									</div>

									<div style="float:left; width:30%;">
										<input type="checkbox" id="one_star"  name="star[]" value="1" /><label class="label-star" for="one_star">1 Star</label>
									</div>

									<div style="float:left; width:70%; text-align:right; margin-top:-5px;">
										<span style="font-size:150%; color:#50e3c2;">&starf;</span>
									</div>
								</div-->
							</div> <!--Rating-->

						</div> <!-- Filter by-->

					</div> <!--Col-->
				</div> <!--Row-->

				<!--Apply-->
				<div>
					<input type="number" style="margin-top: -68% !important; margin-left: 12% !important; width: 6%; display:none;" id="" name="" class="btn btn-welcome" value="APPLY">
				</div> <!--Apply-->

			</form> <!--Form-->

			<!--Center contents-->
			<div class="col s12 m12 l9 rightSide" style="margin-left:-2px !important;">
				<div class="page-header">
					<div class="container">
						<div class="row">
						<div class="col s12 m12 l8">
						<h2 class="page-title">My Marketplace</h2>
						<ol class="breadcrumbs" style="margin-top: 10px !important;">
							<li>
							<a class="page-link" href="<?= base_url(); ?>community/my-marketplace">GROWTH</a>
							/
							<a class="page-link">MY MARKETPLACE</a>
							</li>
						</ol>
						</div>
						<div class="col s12 m12 l4">
							<?php 
								$reg_id=$this->session->userdata['user_session']['reg_id'];
								$bus_id=$this->session->userdata['user_session']['bus_id'];
								if($this->session->userdata['user_session']['reg_admin_type'] == 3 || $this->session->userdata['user_session']['reg_admin_type'] == -1){
								$bus_id_reg=$this->Adminmaster_model->selectData("businesslist","*",array('reg_id'=>$reg_id));
								if(count($bus_id_reg)>0){
							?>
						    <a href="<?= base_url(); ?>community/manage_profile?token=<?php echo $bus_id_reg[0]->bus_id; ?>" style="margin:0 5% 0 -10% !important;" class="btn btn-theme btn-large view-profile">VIEW MY COMPANY PROFILE</a>
							<?php } } ?>
						</div>
						</div>
					</div>
				</div>

				<!--Find Companies-->
				<div class="page-content">
					<div class="container">
						<div class="col s12 m12 l12 welcom-container">
							<h5><b>Find Companies</b></h5>
						</div>

						<div class="col s12 m12 l12" style="margin-bottom:15px !important;">
							<p style="text-align:center;">
							Find companies in different industrial segments to connect with <br>
							and grow your business synergies
							</p>
						</div>

						<div class="col s12 m12 l12">
							<!--Keyword Search Form-->
							<form action="<?php echo base_url();?>community/company-search" method="post">
								<?php $csrf = array(
					'name' => $this->security->get_csrf_token_name(),
					'hash' => $this->security->get_csrf_hash()
				);
				?>
				<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
								<div class="col s12 m12 l12" style="text-align:center;">
									<input type="text" id="search_comp" name="search" placeholder="SEARCH BY NAME OR NATURE OF BUSINESS"  autocomplete="off" required>
								</div>

								<div class="col s12 m12 l12" style="margin:-18px 0px 0px 17.5% !important;">
									<label style="margin-left: 6.3%; font-size: 13px !important;">E.g. Google or Digital Media</label>
								</div>

								<div class="col s12 m12 l12" style="text-align:center; margin-top:20px !important;">
									<input type="submit" name="submit" style="margin-top:0px !important;" id="submit" class="btn btn-welcome" value="Search">
								</div>

							</form> <!--Keyword Search Form-->
						</div>
					</div>
				</div> <!--Find Companies-->

			</div> <!--Center contents-->

		</section> <!-- END CONTENT -->
	</div> <!-- END WRAPPER -->
</div> <!-- END MAIN -->

<script type="text/javascript">
	$(document).ready(function(){	

		$("#search-rslt-form").submit(function(e){
		e.preventDefault();
		}).validate({
			rules: {
				bs_enddate:{
					//required:true,
				},
			},
			messages:{
				bs_enddate:{
					//required:"Date is required",
				},
			},
			submitHandler: function(form) {
				$("input[name='csrf_test_name']").val(csrf_hash);
				form.submit();
			}
		});
		
		if ($('.menu-icon').hasClass('open')) {
			$('.menu-icon').removeClass('open');

			$('.menu-icon').addClass('close');

			$('#left-sidebar-nav').removeClass('nav-lock');

			$('.header-search-wrapper').removeClass('sideNav-lock');

			$('#header').addClass('header-collapsed');

			$('#logo_img').show();
			$('#eazy_logo').hide();

			$('#left-sidebar-nav').toggleClass('nav-expanded nav-collapsed');

			$('#main').toggleClass('main-full');
		}

		$("#market_country").on("change",function(){
			var country_id = $(this).val();
			if(country_id!=''){

				// $.ajax({
				//   url:base_url+'profile/get_states',
				//   type:"POST",
				//   data:{'country_id':country_id},
				//   success:function(res){
				//     //$(".state_label").hasClass('select-dropdown').find('ul').html(res);
				//     $("#state1").html(res);
				//     $("#state1").parents('.input-field').addClass('label-active');
				//     $('#state1').material_select();
				//        //  $('#statesec').addClass('bodrpinkright');
				//   },
				// }); 

				$.ajax({
					url:base_url+'market_place/get_cities',
					type:"POST",
					data:{'csrf_test_name':csrf_hash,'country_id':country_id},
					success:function(res){
					//alert(res);
					//$(".state_label").hasClass('select-dropdown').find('ul').html(res);
					$("#multi_loc").html(res);
					$("#multi_loc").parents('.input-field').addClass('label-active');
					$('#multi_loc').material_select();
					//  $('#statesec').addClass('bodrpinkright');
					},
				}); 


				setTimeout(function(){ 
					makeredcolor("select2-state-container");     
				}, 400);

			} else{
				// Materialize.toast('Please select the country', 2000,'red rounded');
			}
		});
	});
</script>

<script type="text/javascript">
	$(document).ready(function() {
	//$('.js-example-basic-multiple').select2();
	$('.js-example-basic-single').select2();
	$('#multi_loc').select2({
		placeholder: 'SELECT LOCATION',
		allowClear: true
	});

	$('#multi_loc').on('select2:open', function () {
		$('.select2-search__field').attr('placeholder', '');
	});

	$('#multi_loc').on('select2:close', function () {
		$('.select2-search__field').attr('placeholder', 'SELECT LOCATION');
	});

	});
</script>

<!--script>
	$(document).ready(function() {
		$('#submit').click(function(){
			var multiple_loc = $("#multi_loc").val();  
			var search_query = $("#search_comp").val();
			var country = $("#market_country").val();
			alert(country);
			$.ajax({
				type:"GET",
				url : base_url+'market_place/search_company_result',
				data:{ 
					"multipleloc":multiple_loc,
					"searchquery":search_query,
					"country": country,
				}, 
					success : function(data){
					alert(data);	
				},
			});
		});
	});
</script-->