<?php $this->load->view('my-community/Cohorts-header'); ?>
    <style>
		.ps-scrollbar-y{ display:none !important; } 
		#main .ps-container.ps-active-y > .ps-scrollbar-y-rail{ display:none !important; } 
		.table-type1 tbody td a{
			text-transform: initial;
		}
		.city-row{ margin-left: -10px; }
		@media only screen and (max-width: 600px) {
			table.responsive-table tbody tr{
				display:table-row !important;
			}	
			table.responsive-table tbody{
				width:100%;
			}
			.action-btn-wapper .cowork_name{ width:232px !important; margin:0 0 15px 15% !important; }
			.btn-dropdown-select > input.select-dropdown{ margin-bottom:15px; }
			.cowork_city, .cowork_country { margin:0 0 15px 15% !important; }
			a.addmorelink{ margin-right:21%; }
			.city-row{ margin-left: 2px !important; }
		}
		@media all and (-ms-high-contrast: none), (-ms-high-contrast: active) { 
		a{
			color:#000;
		}
		.btn-date-bill {
			line-height: 34px !important;
		}
		.select2-container{
			margin-top:-4px;
		}
		}	
		.dataTables_scrollHead{
			height:0px !important;
		}
		.dataTables_scrollBody{
			height:auto !important;
		}	
		.select-country, .city-select, .type_sub, .type_pack{
			margin-right:10px !important;
		}
		
		.sub_bulk{
			min-width:140px !important;
			top:203px !important;
		}
		
		div#breadcrumbs-wrapper {
			margin-top: 0px !important;
			padding: 15px 0 !important;
		}
		
		.btn-dropdown-action{
			min-width:145px !important;
		}
		
		/*....................*/
	
		.add-new {
			margin: 0px 0 0 0 !important;
		}

		.btn-date{
			max-width:72px;
			font-size:12px !important;
		    margin: 0 0 0 6px !important;
		}

		.date-cng[type=text]:not(.browser-default) {
			font-size: 12px !important;
		}
		
		.days_since_reg{
			width:200px !important;
			margin-left:10px;
		}
		
		.cowork_name{ width:290px !important; }
		.cowork_city, .cowork_country{ width:230px !important; }

		.btn-dropdown-select > input.select-dropdown {			
			width:250px !important;
			font-size:13px !important;
		}

		.action-btn-wapper span.caret {
			margin: 15px 8px 0 0;
		}
		.client_name{
			max-width:155px !important;
		}

		.select-emp{
			max-width:156px !important;
			margin-right:5px !important;
			font-size:13px;
		}

		.btn-search{
			margin: 0px !important;
		}

		a.filter-search.btn-search.btn.active {
			margin-right: -20px !important;
		}

		.dataTables_length {
			margin-left: 500px;
		}

		#cowork_table_length .dropdown-content {
			min-width: 95px;
			margin-top:-46px !important;
		}

		#cowork_table_length{
			border:1px solid #B0B7CA !important;
			height:38px;
			border-radius:4px;
			width:110px;
			margin-top:5px;
			margin-left:50%;
		}

		#cowork_table_length .select-wrapper input.select-dropdown {
			margin-top:-3px !important;
			margin-left:10px !important;
		}

		.coupon_bulk_action:not(:checked) + label:after {
			top:5px !important;
			left:6px !important;
		}

		#cowork_table_length .select-wrapper span.caret {
			margin: 17px 7px 0 0;
		}
		
		.coupon_bulk_action:not(:checked) + label:after {
			top:5px !important;
			left:6px !important;
		}
		
		.coupon_bulk_action label{
			margin-left:0px !important;
		}
		
		.coupon_bulk_action.filled-in:checked + label:after {
			top:5px !important;
		}
		
		.coupon_bulk_action.filled-in:checked + label:before {
			top:5px !important;
		}
		
		::placeholder{
			color:#000000 !important;			
			font-size:12px !important;
		}
		
		a.addmorelink {
			margin-top:-25px !important;
		}
		.dropdown-content.sub_bulk{
			margin-top:10px !important;
		}
		.btn-stated {
			background-position: 90px center !important;
		}
		
		.offer-1{
		border-radius:7px;
		padding:0 2%;
		border-left:4px solid #fff;
	}

	.offer-1:hover{
		border-left:4px solid #ff7d9a;
	}

	.img-offer{
		padding: 1% 6% !important;
	}

	.offer-info{
		padding: 1% 0 !important;
	}

	.set_remin{
		background-color: #ff7d9a !important;
		height: 35px;
		line-height: 37px;
	}

	a.showmore{
		color: #ff7d9a !important;
		text-decoration: underline;
		text-align:center;
		font-size: 12px !important;
		padding-top: 15px;
	}

	.offer-info h6{
		color:#7864e9 !important;
	}

	.offer_p, .offer_description{
		font-size:13px;
		margin:15px 0 !important;
	}
	::placeholder{
		font-size: 11.8px !important;
		line-height: 30px;
		color: #000 !important;
		font-weight: 400 !important;
		font-family: "Roboto", sans-serif !important;
	}

	.event-dropdown {
    margin: 5px 0 0 -138px !important;
	}
	
	 .dataTables_length {
		margin-left: 500px;
	 }

	 #event_table_length {
	   border:1px solid #B0B7CA;
	   height:38px;
	   border-radius:4px;
	   width:96px;
	   margin-top:5px;
	   margin-left:50%;
	 }

	 #event_table_length .select-wrapper input.select-dropdown {
	   margin-top:-3px !important;
	   margin-left:10px !important;
	 }

	 #event_table_length .select-wrapper span.caret {
	   margin: 17px 7px 0 0;
	 }

	 #event_table_length .dropdown-content {
	   min-width: 95px;
	 }
	 
	 .action-tab{
		 vertical-align: top !important;
	 }
 
	.eve-email:hover{
		color:#7864e9 !important;
	}
	
	a.filter-search.btn-search.btn.active {
		width: 20.5% !important;
		margin-right: 7px !important;
		margin-left: -2%;
	}
	
	.btn-search.active .search-hide-show {
		width: calc(82% - 29px) !important;
	}
	.cowork_name.btn-dropdown-select ul.dropdown-content, .cowork_city.btn-dropdown-select ul.dropdown-content{
		height:300px !important;
		overflow-y:scroll !important;
	}
	</style>
	<!-- END HEADER -->
    
    <!-- START MAIN -->
    <div id="main" style="padding-left:0px !important;">
      <!-- START WRAPPER -->
      <div class="wrapper">
        <!-- START CONTENT -->
        <section id="content" class="bg-cp coupon-search" >
			<div id="breadcrumbs-wrapper">
				<div class="container">
					<div class="row" style="padding-top:1%;">
						<div class="col s12 m12 l6">
							<h5 class="breadcrumbs-title my-ex">Co-Working Spaces<small class="grey-text"></small></h5>
							<ol class="breadcrumbs">
								<li><a href="">RESOURCES / CO-WORKING SPACES</a>
							</ol>
						</div>
						<div class="col s12 m12 l6" style="text-align:right;">
							<!--a class="btn btn-theme btn-large right" href="<?php //echo base_url();?>admin_dashboard/coworking_space">ADD CO-WORKING SPACE</a-->	
						</div>
					</div>
				</div>
			</div>
          <div id="bulk-action-wrapper">
            <div class="container">
			  <div class="row">
                <div class="col l12 s12 m12">
					<a href="javascript:void(0);" class="addmorelink right" onclick="reset_couponfilter();" title="Reset all">Reset</a>
				</div>
			  </div>
              <div class="row">
                <div class="col l3 s12 m12">
				<div class="col l6 s12 m12">
					<!--a class='dropdown-button border-radius-6 btn-dropdown-action btn-bulk btn-default uppercase no-background' href='#' data-activates='dropdown004'>Bulk Actions <i class="arrow-icon"></i></a>
                    <ul id='dropdown004' class='dropdown-content sub_bulk'>
						<li hidden><a id="email_coupon_data"><i class="dropdwon-icon icon email"></i>Email</a></li>
						<li><a id="download_multiple_coupon"  data-multi_sub="0" ><i class="dropdwon-icon icon download"></i>Export</a></li>
						<li hidden><a  id="deactive_multiple_coupon"><i class="dropdwon-icon icon deactivate" style="margin-right: 23px;"></i> Deactivate</a></li>
                    </ul-->
				</div>
				<div class="col l6 s12 m12 searchbtn">
					<!--a class="filter-search btn-search btn">
						<input type="text" name="search_coupon" id="search_coupon" class="search-hide-show" style="display:none" />
						<i class="material-icons ser search-btn-field-show">search</i>
					</a-->
				</div>

                </div>
				<div class="col l9 s12 m12">
				<div class="action-btn-wapper right">
				   <select class='ml-3px border-radius-6 border-split-form btn-dropdown-select select-like-dropdown by-statys days_since_reg cowork_name' id="cowork_name" name="cowork_name">
						<option value="">NAME</option>
						<option value="">ALL</option>
						<?php foreach($name as $val){ ?>
                          <option value="<?=$val->name?>"><?= strtoupper($val->name);?></option>

						<?php }?>
				   </select>
				   <select class='ml-3px border-radius-6 border-split-form btn-dropdown-select select-like-dropdown by-statys days_since_reg cowork_country' id="cowork_country" name="cowork_country">
						<option value="">COUNTRY</option>
						<option value="">ALL</option>
						<?php foreach($country as $cnt){
                                $country_name= $this->Community_model->selectData('countries',"*",array('country_id'=>$cnt->country));
						 ?>
                          <option value="<?=$cnt->country?>"><?= strtoupper($country_name[0]->country_name);?></option>

						<?php }?>
				   </select>
				   <select class='ml-3px border-radius-6 border-split-form btn-dropdown-select select-like-dropdown by-statys days_since_reg cowork_city' id="cowork_city" name="cowork_city">
						<option value="">CITY</option>
						<option value="">ALL</option>
						<?php foreach($city as $cty){ 
                              $city_name= $this->Community_model->selectData('cities',"*",array('city_id'=>$cty->city))
							?>
                          <option value="<?=$cty->city?>"><?= strtoupper($city_name[0]->name);?></option>

						<?php }?>
				   </select>
				   
				   <!--input type="text" placeholder="START DATE" class="btn-date icon-calendar-green rangedatepicker_list date-cng btn-stated out-line" id="coupon_start_date" name="coupon_start_date" readonly="readonly">
				   <input type="text" placeholder="END DATE" class="btn-date icon-calendar-red rangedatepicker_list date-cng btn-stated out-line" id="coupon_end_date" name="coupon_end_date" readonly="readonly"-->
                </div>
				<div class="row" style="margin-top:50px !important;">
					<div class="col s6 m6 l6"></div>
					<div class="col s6 m6 l6" style="text-align:right;">
						
					</div>
				</div>
				</div>
               </div>
              </div>
            </div>
          </div>

          <div class="container">
            <div class="row">
              <div class="col l12 s12 m12">
                  <table id="cowork_table" class="responsive-table display table-type1" cellspacing="0" style="border-collapse: separate; border-spacing: 0 0.6em;">
                    <thead id="fixedHeader">
						<tr>
							<th style="width:15%;"></th>
							<th style="width:80%;"></th>
							<th style="width:5%;"></th>
						</tr>
					</thead>
					<tbody class="scrollbody">

					</tbody>
                  </table>
              </div>

            </div>
          </div>
        </section>
        <!-- END CONTENT -->
        </div>
        <!-- END WRAPPER -->
      </div>

    <div id="send_email_coupon_modal" class="modal modal-md ps-active-y" style="margin-top:-45px !important; max-width:510px !important;">
	    <?php //$this->load->view('admin_dashboard/email_template_for_coupon'); ?>
	</div>

      <!-- END MAIN -->
	  <script>
		$(".offer-time").hide();
		$(".ssds").click(function(){
			if($(this).hasClass('active')){
				$(".offer-time").hide();
				$(".showmore").removeClass('active');
				$('.less').hide();
				$('.more').show();
			}else{
				$(".offer-time").show();
				$(this).addClass('active');
				$('.less').show();
				$('.more').hide();
			}
		});
		
		function reset_couponfilter(){
			$('.action-btn-wapper').find('select').prop('selectedIndex',0);
			$('.js-example-basic-single').trigger('change.select2');
			$('.btn-date,.search-hide-show').val('');
			//coworkingDatatable(base_path()+'community/get_coworking_details','cowork_table');
			 location.reload();
			$('select').material_select();
		}
		
		function showmorelist(id){
		if($("#showmorelist"+id).hasClass('active')){
				$("#offer-time"+id).hide();
				$("#showmorelist"+id).removeClass('active');
				$('#less'+id).show();
				$('#showmorelist'+id).hide();
				$('#morelist'+id).show();
				$('#morelist1'+id).show();
			}else{
				$("#offer-time"+id).show();
				$("#showmorelist"+id).addClass('active');
				$('#showmorelist'+id).show();
				$('#less'+id).hide();
				$('#morelist'+id).hide();
				$('#morelist1'+id).hide();
			}
		}
		
		var bulk_activity = [];
		$("input[id='coupon_bulk']").on("click",function(){
			if($(this).is(':checked',true)) {
				$(".coupon_bulk_action").prop('checked', true);
				$(".coupon_bulk_action:checked").each(function() {
					bulk_activity.push($(this).val());
				});
				bulk_activity = bulk_activity.join(",");
				$('#download_multiple_coupon').attr('data-multi_sub',bulk_activity);
			}
			else {
				$(".coupon_bulk_action").prop('checked',false);
				bulk_activity = [];
				$('#download_multiple_coupon').attr('data-multi_sub',0);
			}
		});
		
	  </script>
      <?php $this->load->view('template/footer'); ?>
