<?php $this->load->view('my-community/Cohorts-header'); ?>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style type="text/css">
	.icon-font{
		color:#ff7d9a !important;
		font-size:15px;
		width:10% !important;
	}
	p.event-desc{ height:95px; margin:-6px 0; }
	
	
	
	@media all and (-ms-high-contrast: none), (-ms-high-contrast: active) { 
		a{
			color:#000;
		}
		.btn-date-bill {
			line-height: 34px !important;
		}
		.select2-container{
			margin-top:-4px;
		}
	}
		
	.dataTables_scrollBody{
		overflow:hidden !important;
		height:100% !important;
	}

	.dataTables_scrollHead{
		margin-bottom:-24px !important;
	}

	.btn-theme {
    font-size: 12px;
    font-weight: 500;
    padding: 0 10px;
    border-radius: 5px;
    line-height: 42px;
    height: 40px;
    letter-spacing: 0.20px !important;
    background-color: #7864e9;
    margin: 0 -10px 0px 0px;
}

.btn-search {
    border: 1px solid #d4d8e4;
    border-radius: 6px;
    background: none;
    height: 40px;
    line-height: 40px;
    padding: 0;
    box-shadow: unset;
    -webkit-box-shadow: unset;
    -o-box-shadow: unset;
    -moz-box-shadow: unset;
    -ms-box-shadow: unset;
    margin: 0 0 0 0px;
    color: #B0B7CA;
}

	table.dataTable thead .sorting {
		background-position: 110px 15px !important;
	}
	table.dataTable thead .sorting_asc {
		background-position: 110px 15px !important;
	}
	table.dataTable thead .sorting_desc {
		background-position: 110px 15px !important;
	}

	.select-dropdown{
		max-height: 350px !important;
	}
	.btn-theme-disabled{
		cursor: not-allowed;
	}
	.icon-img {
		margin: 0 30px 0 2px;
	}

	/*----------START SEARCH DROPDOWN CSS--------*/
	.select2-container--default .select2-selection--single {
	  border:none;
	}

	input[type="search"]:not(.browser-default) {
	  height: 30px;
	  font-size: 12px;
	  margin: 0;
	  border-radius: 5px;

	}
	.select2-container--default .select2-selection--single .select2-selection__rendered {
	  font-size: 13px !important;
	  line-height: 30px;
	  color: #000 !important;
	  font-weight: 400 !important;

	}
	.select2-container--default .select2-selection--single .select2-selection__arrow {
	  height: 40px;
	}
	.select2-search--dropdown {
	  padding: 0;
	}
	input[type="search"]:not(.browser-default):focus:not([readonly]) {
	  border-bottom: 1px solid #bbb;
	  box-shadow: none;
	}
	.select2-container--default .select2-selection--single:focus {
		outline: none;
	}
	.select2-container--default .select2-results__option--highlighted[aria-selected] {
		background: #fffaef;
	  color: #666;
	}
	.select2-container--default .select2-results > .select2-results__options {
	  font-size: 12px;
	  border-radius: 5px;
	  box-shadow: 0px 2px 6px #B0B7CA;
	}
	.select2-dropdown {
	  border: none;
	  border-radius: 5px;
	}
	.select2-container .select2-selection--single {
	  height: 40px;
		padding: 6px;
		border: 1px solid #d4d8e4;
		background: #f8f9fd;
		border-radius: 5px;
	}
	.select2-results__option[aria-selected] {
	  border-bottom: 1px solid #f2f7f9;
	  padding: 14px 16px;
	}
	.select2-container--default .select2-search--dropdown .select2-search__field {
		border: 1px solid #d0d0d0;
		padding: 0 0 0 0px !important;
		width: 99.5%;
		max-width: 100%;
	}
	.select2-search__field::placeholder {
	  content: "Search Here";
	}
	.select2-container--open .select2-dropdown--below {
	  margin-top: 0;
	}
	/*----------END SEARCH DROPDOWN CSS--------*/

	  .dataTables_length {

		margin-left: 500px;
	}
	#my-customers_length{
		border:1px solid #B0B7CA;
		height:38px;
		border-radius:4px;
		width:90px;
		margin-top:5px;
		margin-left:52%;
	}

	#my-customers_length .select-wrapper input.select-dropdown {
		margin-top:-3px !important;
		margin-left:10px !important;
	}

	#my-customers_length .dropdown-content {
		min-width: 90px;
		margin-top:-50% !important;
	}

	.customer_profile_bulk_action.filled-in:not(:checked) + label:after {
		top:5px !important;
	}

	#my-customers_length .select-wrapper span.caret {
		margin: 17px 5px 0 0;
	}

	.sticky {
		position: fixed;
		top: 70px;
		width: 76.2%;
		z-index:999;
		background: white;
		color: black;
	}

	.sticky + .scrollbody {
		padding-top: 102px;
	}

	.tipxebra{
		margin-top:-10px;
	}

	#c_start_date, #c_end_date{
		max-width:80px !important;
		height:35px !important;
		padding-top:2px !important;
		margin-left:0px !important;
	}
	.select2-container {
		width: 230px !important;
		margin-top:-1px;
	}
	
	@-moz-document url-prefix() {
		#c_start_date, #c_end_date{
			max-width:80px !important;
			height:33px !important;
			padding-top:6px !important;
			margin-left:0px !important;
		}
		
		.select2-container {
			width: 230px !important;
			margin-top:-5px;
		}
		
		.btn-date{
			max-width:80px !important;
			height:33px !important;
			padding-top:6px !important;
			margin-left:0px !important;
		}
		
		.btn-search{
			margin-top:-5px;
		}
	}

	span.tabl {
		margin: -4px 0 0 21px !important;
	}

	#c_start_date::placeholder{
		color:#000;
		font-size:12px;
	}
	#c_end_date::placeholder{
		color:#000;
		font-size:12px;
	}

	.ex{
		margin:-28px 0 0 48px !important;
	}

	.btn-stated {
		background-position: 95px center !important;
	}

	.offer-1{
		border-radius:7px;
		padding:0 2%;
		border-left:4px solid #fff;
	}

	.offer-1:hover{
		/*border-left:4px solid #ff7d9a;*/
	}

	.img-offer{
		padding: 1% 6% !important;
	}

	.offer-info{
		padding: 1% 0 !important;
	}
	
	.color-purple{
		color: #7864e9 !important;
		font-size:16px;
	}

	.set_remin{
		/*background-color: #ff7d9a !important;*/
		background-color: #7864e9 !important;
		height: 35px;
		line-height: 37px;
		padding-left: 0px;
		margin: 6px 2px;
	}

	a.showmore{
		color: #ff7d9a !important;
		text-decoration: underline;
		text-align:center;
		font-size: 12px !important;
		padding-top: 15px;
	}

	.offer-info h6{
		color:#7864e9 !important;
	}

	.offer_description{
		font-size:13px;
		margin:15px 0 !important;
	}
	
	.offer_p{
		font-size:13px;
		margin:15px 0 !important;
		word-wrap: break-word;
		/*width:305px;*/
	}
	
	::placeholder{
		font-size: 11.8px !important;
		line-height: 30px;
		color: #000 !important;
		font-weight: 400 !important;
		font-family: "Roboto", sans-serif !important;
	}

	.event-dropdown {
    margin: 5px 0 0 -138px !important;
	}
	
	 .dataTables_length {
		margin-left: 500px;
	 }

	 #event_table_length {
	   border:1px solid #B0B7CA;
	   height:38px;
	   border-radius:4px;
	   width:96px;
	   margin-top:5px;
	   margin-left:50%;
	 }

	 #event_table_length .select-wrapper input.select-dropdown {
	   margin-top:-3px !important;
	   margin-left:10px !important;
	 }

	 #event_table_length .select-wrapper span.caret {
	   margin: 17px 7px 0 0;
	 }

	 #event_table_length .dropdown-content {
	   min-width: 95px;
	 }
	 
	 .action-tab{
		 vertical-align: top !important;
	 }
 
	.eve-email:hover{
		color:#7864e9 !important;
	}
	
	a.filter-search.btn-search.btn {
		margin-right: -15px !important;
		margin-left: 1%;
	}
	
	a.filter-search.btn-search.btn.active {
		width: 20% !important;
		margin-right: -15px !important;
		margin-left: 1%;
	}
	
	.btn-search.active .search-hide-show {
		width: calc(82% - 29px) !important;
	}
	
	.location{ height:30px; }
	#submit{ height: 38px; background-color: #7864e9; border: 1px solid #ccc; border-radius: 5px; color:#fff; font-size:13px;}
	#breadcrumbs-wrapper{ padding: 20px 0 15px 0 !important; }
	@media only screen and (max-width: 600px) {
		.btn-theme.set_remin{ margin-top:-15%; }
	}
</style>
    <div id="main" style="padding-left:0px !important;">
      <div class="wrapper">
        <?php //$this->load->view('template/sidebar'); ?>
        <section id="content" class="bg-cp customer-search">
          <div id="breadcrumbs-wrapper">
            <div class="container">
               <div class="row">
               <div id="plan_msg"></div>
             </div>
              <div class="row">
                <div class="col s10 m6 l6">
                  <h5 class="breadcrumbs-title">CA Infolists<small class="grey-text">(Total <?=count($CA)?>)</small></h5>

                  <ol class="breadcrumbs">
                    <li><a>RESOURCES</a>
                    </li>
                    <li class="active">CA Infolists</li>
                  </ol>
                </div>
                <div class="col s10 m6 l6">
					<!--a class="btn btn-theme btn-large right add-new cus-new" href="<?= base_url(); ?>community/add">ADD NEW </a-->
                </div>
              </div>
            </div>
          </div>

          <div id="bulk-action-wrapper">
            <div class="container">
              <div class="row">
              	<form name="search_form" id="search_form" method="post">
					<?php $csrf = array(
					'name' => $this->security->get_csrf_token_name(),
					'hash' => $this->security->get_csrf_hash()
				);
				?>
				<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
				<div class="col l12 s12 m12" style="margin:-20px 0 0 -12px;">
                 <a href="javascript:void(0);" onclick="formsubmit();" class="addmorelink right" id="reset_event" title="Reset all">Reset</a>
                </div>
                <!--div class="col l2 s12 m12">-->
                	
                  <!--a class='dropdown-button border-radius-6 btn-dropdown-action btn-bulk btn-default uppercase no-background' href='#' data-activates='dropdown04'>Bulk Actions <i class="arrow-icon"></i></a-->
                    <!--ul id='dropdown04' class='dropdown-content'>-->
                      <!--li><a id="email_multiple_customers"><i class="dropdwon-icon icon email"></i>Email</a></li-->
                      <!--li><a id="download_multiple_customers" data-multi_download="0" style="display: flex;"><img class="icon-img" src="<?php echo base_url(); ?>public/icons/export.png" alt="export" style="width: 15px;height: 22px;">Export</a></li-->
                      <!--li><a id="print_multiple_customers"><i class="material-icons">print</i>Print</a></li-->
                    <!--/ul>
					
                </div-->
                
                 <div class="col l9 s12 m12">
                     <a class="filter-search btn-search btn right <?php if(isset($search['search']) && $search['search']!=""){?>active <?php } ?>">
							<input type="text" name="search" id="search" class="search-hide-show" value="<?php if(isset($search['search']) && $search['search']!=""){echo $search['search']; } ?>" <?php if(isset($search['search']) && $search['search']!=""){?>style="display:block"<?php }else{ ?>style="display:none"<?php } ?> />
							<i class="material-icons ser search-btn-field-show">search</i>
						</a>
                 </div>
                 <div class="col l3 s12 m12">
					<div class="action-btn-wapper right">
                    <div class="bulk location-drop" style="margin: 0 0 0 -8%; width: 105%;">
						<select class="js-example-basic-single" name="location" id="location">
							<option value="">SELECT CITY</option>
						
							<option value="">ALL</option>
							<?php 
                      			foreach($city as $loc)  { 
                      				$cities=$this->Community_model->selectData('cities',"*",array('city_id' =>$loc->city ));

                      				?>
                        			<option value="<?php echo $loc->city; ?>" <?php if(isset($search['location']) && $search['location']!=""){ if($search['location']== $loc->city){ echo "selected";} } ?>><?php echo strtoupper($cities[0]->name); ?></option>
                        		<?php  } 
                    		?>
                        </select>
						<input type="submit" id="submit" name="submit" value="APPLY" style="width:20%;">
						</div>
					</div>
                </div>
            </form>
              </div>
            </div>
          </div>

		<div class="container">
			<div class="row">
				
				<!-- Start -->
				<div class="col s12 m12 l12" style="padding-top:1%;">
				<div class="offers_list">
					<div class="row">
						<?php if (count($CA) > 0){ foreach($CA as $ev) {?>
					<div class="col s12 m12 l6">
						<div class="offer-1 box-wrapper bg-white shadow" style="margin: 0 0 10px 0px;">
							<!--div class="col s12 m12 l3 img-offer" style="width:20%;">
								<?php
									if($ev['event_image'] != '') {
										$event_image = '<img src="'.base_url().'public/upload/event_image/'.$ev['id'].'/'.$ev['event_image'].'" height="130px" class="logo_style_2" width="130px">';
									} else {
										$event_image = '<img src="'.base_url().'public/upload/event_image/'.$ev['id'].'/'.$ev['event_image'].'" height="130px" class="logo_style" width="130px">';
									}
								?>
								<div class="col s6 m6 l4" style="text-align: right">
									<?php echo $event_image; ?>
								</div>
							</div-->
							
							<div class="col s12 m12 l12 offer-info">
								<div class="col s12 m12 l10" style="border-left: 7px solid #7864e9; margin: -5px 0 0 -2.8%; border-top-left-radius: 5px; border-bottom-left-radius: 5px; padding: 4px 4.7% 0px 4.7%;">
									<h5 style="height:30px; font-size:20px; color:#7864e9 !important;"><strong><?php echo strtoupper($ev['reg_username']);?></strong></h5>
								</div>	
								<?php //$bus_id = $this->user_session['bus_id']; ?>
								
								<!--<div class="col s12 m12 l2" style="padding: 2% 0% 2% 14.5%;">
									<a href="javascript:void(0);" class="waves-effect waves-block waves-light event-button" data-activates="event-dropdown<?php echo $ev['id'];?>"><i style="color:#24292c; font-size:17px;" class="fa fa-ellipsis-v" aria-hidden="true"></i></a>
									<ul id="event-dropdown<?php echo $ev['id'];?>" class="event-dropdown dropdown-content user-profile-down" style="margin-left:0% !important;">
						            <li class="user-profile-fix">
						            	
										<ul>
											<li><a href="<?php echo base_url();?>community/edit-event/<?php echo $ev['id'];?>"><i class="material-icons" style="color: #000;">mode_edit</i>EDIT</a></li>
											<li><a href="javascript:void(0);" class="deactive_event" data-cd_id="<?php echo $ev['id'];?>"><i class="material-icons">delete</i>DELETE</a></li>
										</ul>
										
									</li>
									</ul>
								</div>-->
							</div>
						
							<div class="col s12 m12 l12 offer-info">
								<div class="col s12 m12 l9" style="padding: 2px 0px;">
									<div class="col s12 m12 l12">
										<p class="offer_p"><i class="icon-font fas fa-building"></i> <strong>COMPANY NAME:<?=$ev['company']?> </strong> </p>
										<?php 
                                        $city=$this->Community_model->selectData('cities',"*",array('city_id' =>$ev['city'] ));

										?>
										<p class="offer_p"><i class="icon-font fas fa-map-marker-alt"></i> <strong>CITY:<?=$city[0]->name?> </strong> </p>
										<p class="offer_p"><i class="icon-font far fa-id-card"></i> <strong>CONTACT NAME:<?=$ev['reg_username']?> </strong></p>
										<p class="offer_p"><i class="icon-font fa fa-briefcase" aria-hidden="true"></i> <strong>DESIGNATION:<?=$ev['reg_designation']?> </strong> </p>
										<p class="offer_p"><i class="icon-font fa fa-envelope" aria-hidden="true"></i> <strong>EMAIL ID:<?=$ev['reg_email']?> </strong> </p>
										<p class="offer_p"><i class="icon-font fa fa-mobile" aria-hidden="true"></i> <strong>MOBILE NO:<?=$ev['reg_mobile']?> </strong> </p>
									</div>
								</div>	
								<div class="col s12 m12 l3" style="padding: 2% 0% 2% 0%;">
									<?php 
										$img=$this->Community_model->selectData('registration',"*",array('reg_email'=>$ev['reg_email']));
										$proimg=@$img[0]->reg_profile_image;
										$xreg_id=@$img[0]->reg_id;
									?>
									<?php if($proimg !=""){ ?>
									<img src="https://xebra.in/public/upload/personal_images/<?php echo $xreg_id; ?>/<?php echo $proimg; ?>" height="130px" class="logo_style_2" width="130px">
									<? } ?>
								</div>	
							</div>
							<div class="col s12 m12 l12">
								<div class="col s12 m12 l6">
									<p class=""><strong><a class="color-purple weblink" href="#" target="_blank">WEBSITE</a></strong></p>
								</div>
								<div class="col s12 m12 l6">
									<a class="btn btn-theme btn-large right set_remin" href="<?php echo base_url();?>community/view-company-details?token=<?=$ev['reg_id']?>" target="_blank">&nbsp; VIEW PROFILE</a>
								</div>
							</div>
						</div>
					</div>
			
					<?php }} else{ ?>
						<div class="col s12 m12 l4"></div>
						<div class="col s12 m12 l4">
							<div class="text-center" style="margin: 0 0 10px 0px;">
								<div style="background: #ff85a1; width: 130%; padding: 0px 5px 0px 5px; border-radius: 50px; color: white; height: 50px; line-height: 50px; font-size: 18px; margin-left:-11%;">
								<?php if($search['location']!=""){ ?>
									<p>Currently, there are no Chartered Accountants listed for this city</p>
								<?php }else{ ?>
									<p>Currently, there are no Chartered Accountants listed by that query</p>
								<?php } ?>
								</div>
							</div>
						</div>
						<div class="col s12 m12 l4"></div>

					<?php } ?>

					</div>
				</div>
				</div>
				<!-- END -->
				<div class="row"><div class="col s12 m12 l12"><p></p></div></div>
				<div class="row"><div class="col s12 m12 l12"><p></p></div></div>
				<div class="row"><div class="col s12 m12 l12"><p></p></div></div>
            </div>
        </div>


		</section>
	</div>
</div>

<div id="send_email_event_modal" class="modal modal-md ps-active-y" style="margin-top:-45px !important; max-width:510px !important;">
	<?php $this->load->view('my-community/email-popup-event'); ?>
</div>

<div id="remove_event" class="modal" style="width:40% !important;">
	<img class="geen" src="<?php echo base_url();?>asset/images/green.png" alt="green">
	<div class="modal-content">
		<div class="modal-header" style="text-align:right; margin-left:0%;">
			<h4> Delete Event</h4>
			<input type="hidden" id="remove_event_id" name="remove_event_id" value="" />
			<a class="modal-close close-pop"><img src="<?php echo base_url();?>asset/images/popupdelete.png" alt="delete" ></a>
		</div>
	</div>
	<div class="modal-body" style="margin-left: 0px !important; width: 100%;">
		<p style="font-size:18px !important; color:#595959 !important; text-align:center;">Are you sure you want to delete this event?</p>
	</div>
	<div class="modal-content">
	<div class="modal-footer">
		<div class="row">
			<div class="col l4 s12 m12"></div>
			<div class="col l8 s12 m12 cancel-deactiv">
				<a class=" modal-close btn-flat theme-flat-btn theme-btn theme-btn-large model-cancel " type="button">CANCEL</a>
				<button id="deactive_event" class="btn-flat theme-primary-btn theme-btn theme-btn-large  modal-trigger dea modal-close deactive_this_service">DELETE</button>
			</div>
		</div>
	</div>
	</div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
		$('.js-example-basic-single').select2();
	});
</script>
<script type="text/javascript">
    $(document).ready(function() {
		
		if($('.weblink').attr("href") == "#"){
			$('.weblink').css('cursor','default');
		}else{
			$('.weblink').css('cursor','pointer');
		}
		
		
		$(".offer-time").hide();
		$(".ssds").click(function(){
			if($(this).hasClass('active')){
				$(".offer-time").hide();
				$(".showmore").removeClass('active');
				$('.less').hide();
				$('.more').show();
			}else{
				$(".offer-time").show();
				$(this).addClass('active');
				$('.less').show();
				$('.more').hide();
			}
		});

       	$('.deactive_event').on('click',function(){

				var event_id = $(this).data('cd_id');

				 $('#remove_event').modal('open');

				 $('#remove_event #remove_event_id').val(event_id);

			});

			$('#deactive_event').off().on('click',function(){
				var event_id = $('#remove_event_id').val();
				if(csrf_hash===""){
					csrf_hash=csrf_hash;
				}
				$.ajax({
					url:base_url+'Community/delete_event',
					type:"POST",
					data:{'csrf_test_name':csrf_hash,"event_id":event_id,},
					success:function(res){
						var myArray = res.split("@");
						if(myArray[1]){
							csrf_hash=myArray[1];
						}
							if(myArray[0] == 'true')
							{
								window.location.href=base_url+'Community/events';
								Materialize.toast('Your Event has been successfully deleted', 2000,'green rounded');
							}
							else
							{
								//window.location.href=base_url+'Community/deals';
								Materialize.toast('Error while processing!', 2000,'red rounded');
							}
						},
					});
			});

	});

	function email_event($id){

		var event_id = $id;
		$('#send_email_event_modal').modal('open');
		$('#send_email_event_modal #event_id').val(event_id);

	}

	function formsubmit() {
		$(window).off('beforeunload');
		$("#search_form").submit();
		location.reload();
	}

	function showmorelist(id){
		if($("#showmorelist"+id).hasClass('active')){
				$("#offer-time"+id).hide();
				$("#showmorelist"+id).removeClass('active');
				$('#less'+id).show();
				$('#showmorelist'+id).hide();
				$('#morelist'+id).show();
				$('#morelist1'+id).show();
			}else{
				$("#offer-time"+id).show();
				$("#showmorelist"+id).addClass('active');
				$('#showmorelist'+id).show();
				$('#less'+id).hide();
				$('#morelist'+id).hide();
				$('#morelist1'+id).hide();
			}
	}

			window.onbeforeunload = function() {
  };



  window.onbeforeunload = null;

    if ( window.history.replaceState ) {
        window.history.replaceState( null, null, window.location.href );
    }
</script>

<?php $this->load->view('template/footer'); ?>
