<?php $this->load->view('my-community/Cohorts-header');?>
    <style type="text/css">
			
	#sharevideo .modal-header h4{ width:92%; }	
	.jssocials-share{ float:left; margin:25px 0 0px 15px;	}
	.jssocials-share img{ width:35px; height:35px; }
	.jssocials-share.jssocials-share-sharechat img{ width:45px;}
	.jssocials-share.jssocials-share-moj img{ width:45px;}
	::placeholder{ font:15px; color:grey;}	
    #content{ padding: 0 0 0 0; border-bottom: 1px solid #f7f7f7; }
	.interview_row .pp-header{ padding:2.5% 0; }
	.interview_row .pp-header h1{ color:#595959; font-size:32px; }
	.interview_row .pp{ padding: 5px 0; line-height:25px; font-size:18px; text-align: justify; } 
	.interview_row .tc-info{ padding: 0 12%; }
	.bvfoot{ margin-top:5% !important; }
	.a-link{ padding:1% 0px 0px 30px; }
	.playpause {
		background-image:url("<?=base_url('asset/images/overlay.png');?>");
		background-repeat:no-repeat;
		background-repeat: no-repeat;
		width: 40%;
		height: 15%;
		position: absolute;
		left: -18%;
		right: 0%;
		top: -30%;
		bottom: 0%;
		margin: auto;
		background-size: contain;
		background-position: center;
		cursor:pointer;
	}
	.mar25{ margin-top:25px; }
	.container { width: 100% !important;}
	.cat_videos{ height: 100%; margin:8% 0 0 0; }
	.btn-dropdown-select{ width:210px; } 
	.video-owner, .video-title, .brief-content{ text-align:left; margin:5px 0 0 0; } 
	#breadcrumbs-wrapper { padding: 25px 0 15px 0; }
	.views { color: #000; line-height: 1.5; font-family: Arial; font-size: 14px; padding:5px 20px; }
	.video-like, .video-share{ font-size:15px; color:#000; }
	.video-like label, .video-share label{ font-size:15px; color:#000; }
	.vid-content{ background-color:#fff; padding: 0px 0 0 3%; border-radius:10px; }
	.container{ margin:2% 1%; width:98% !important; }
	.avatar img{ border-radius:50%;	}
	.acc_details h5{ font-size:16px; font-weight:600; margin:5px 0 10px 0px; }
	.comments { font-size: 15px; }
		
		.comments p {
			font-family: Arial;
			color: grey;
		}

		.comments textarea {
			width: 100%;
		    padding: 10px;
		    border: 1px solid #ccc;
		    border-radius: 5px;
		    font-family: Arial;
		    height: 110px !important;
			background-color: #fff !important;
			border: 1px solid #ccc !important;
			margin-bottom:10px;
		}

		.comments input[type=text] {
			width: 49.5%;
		    padding: 10px;
		    border: 1px solid #ccc;
		    border-radius: 5px;
		    font-family: Arial;
		}

		.comments input[type=email] {
			width: 49.5%;
		    padding: 10px;
		    border: 1px solid #ccc;
		    border-radius: 5px;
		    font-family: Arial;
		}

		.submitbtn {
			background-color: #bf4b3d;
			border: none;
			color: white;
			border-radius: 5px;
			padding: 10px 14px;
			font-family: Arial;
		}

		.commented {
			margin-top: 10px;
			font-family: Arial;
			font-size: 14px;
			color: grey;
			line-height: 1.5;
			margin-left: 20px;
		}

		.error {
		    font-family: Arial;
		    color: green;
		    margin-top: 20px;
		    margin-bottom: -15px;
		    text-align: center;
		}
		.blog-disp{ margin:	0}
		.allvideos{ margin: 3% 0; padding: 0 1.5%; }
		.allvideos video{ width:85%; }
		.allvideos .video-title, .allvideos .video-owner, .allvideos .brief-content{ padding:0 0 0 20px;}
		.video-like{ color:#808080; font-size: 16px; }
		.video-share{ color:#808080; font-size: 16px; }
		.video-like:hover{ color:#bc343a; font-size: 16px; }
		.video-like.active{ color:#bc343a !important; font-size: 16px; }
		.video-share:hover{ color:#bc343a; font-size: 16px; }
    </style>
    <div id="main" style="padding-left:0px !important;">

    	<?php		
							$countLikes=count($likes);
							if($countLikes >=0 && $countLikes<=1000){
								$subNo=$countLikes;
								$number=$subNo;
							}else if($countLikes >=1000 && $countLikes<=100000){
								$subNo=$countLikes/1000;
								$number=$subNo."K";
							}else{
								$subNo=$countLikes/100000;
								$number=$subNo."L";
							}
							$countViews=count($views);
							if($countViews >=0 && $countViews<=1000){
								$subNoV=$countViews;
								$numberView=$subNoV;
							}else if($countViews >=1000 && $countViews<=100000){
								$subNoV=$countViews/1000;
								$numberView=$subNoV."K";
							}else{
								$subNoV=$countViews/100000;
								$numberView=$subNoV."L";
							}
							if (isset($this->session->userdata['user_session']['reg_id'])){
								$reg_id=$this->session->userdata['user_session']['reg_id'];
                            }else{
								$reg_id=0;
							}
						?>
		<div class="wrapper">
			<section id="content" class="bg-cp">
			<div class="container">
				<div class="row vid-content">
					<div class="col s12 m12 l7 mar25">
						<div class="col-md-12 classsdccc">
						<?php if($video[0]->video_url !=""){ ?>
							<iframe class="explainer" src="<?php echo $video[0]->video_url; ?>?loop=1&autoplay=1&rel=0&showinfo=0&color=white&mute=0&controls=1&amp;start=0" style="border-radius: 15px; height: 450px !important; margin: 1% 0 0 0; width: 100%; border: 1px solid #000;" width="560" height="315" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
						<?php }else{ ?>
							<video id="viewvid" style="height: 450px !important; width: 100% !important; border-radius: 10px; border:1px solid #ccc;"  class="responsive-iframe img-responsive" width="1080" height="610" controls autoplay allowfullscreen><source src="<?php echo $video[0]->video; ?>/<?php echo $video[0]->filename; ?>"></video>
						<?php } ?>
						</div>
						<div class="row views">
							<div class="col s12 m12 l2"> <p class="view_count"><?=$numberView?> Views</p></div>
							<div class="col s12 m12 l2"> <p><a class="video-like" id="video_like"><span><i class="fa fa-thumbs-up" aria-hidden="true"></i></span>&nbsp; <span id="like_count"><?=$number?></span></a></p></div>
							<div class="col s12 m12 l2"> <p>
							<a id="video_share" class="modal-trigger video-share" href="#sharevideo"><span><i class="fas fa-share"></i></span>&nbsp;<label>Share</label></a></p></div>
							<a href="#" id="viewshare" data-toggle="modal" data-target="#sharevideo" hidden></a>
						</div>
					</div>	
					
					<div class="col s12 m12 l5 mar25">
						<div class="row views">
							<div class="col s12 m12 l2 avatar">
								<img src="<?php echo base_url(); ?>asset/images/avatar.jpg" width="55" height="55" alt="Bharat Vaani Youtube" title="Bharat Vaani Youtube">
							</div>
							<div class="col s12 m12 l9 acc_details">
								<h5><?php echo $video[0]->title; ?></h5>
								<span>Published on <?= date('M d, Y',strtotime($video[0]->date_created)); ?></span>
								
								<p> <?php echo $video[0]->description; ?></p>
								
							</div>
						</div>
						<div class="col-md-12">
							<div class="comments">
								<p>Add a Comment...<?php if(isset($this->session->userdata['user_session']) && $this->session->userdata['user_session']['reg_id']!=""){}else{ ?>(You have to sign-up to comment)<?php } ?>
								<a style="color: #bf4b3d; font-family: Arial; margin-left: 25px;">

								</a>
								</p>
								<form method="post" action="" id="comment_frm" name="comment_frm">
									<input type="hidden" name="video_id" id="video_id" value="<?php echo $video[0]->id;?>">
									<textarea name="comment" id="comment" rows="4" cols="5" maxlength="499" placeholder="Enter your comment (Max 500 words)" required></textarea>
									<input type="button" class="submitbtn" name="submit_comment" id="submit_comment" value="SUBMIT">
								</form><hr>
							</div>
						</div>
							<div class="commented">
					<div class="scroll">
						<p style="font-size: 17px;"><span id="comment_count"><?php echo count($comments); ?></span> Comments</p>
                         <div id="comment_div">
						<?php if(count($comments)>0) { foreach($comments as $com){ ?>
							<p style="margin-top: 5px;"><a onMouseOver="this.style.color='#bf4b3d'" onMouseOut="this.style.color='grey'" href="mailto:<?php echo $com->email;?>" style="font-weight: bold; text-decoration: none; color: grey;"><?php echo $com->name?></a> <?= date('M d, Y',strtotime($com->date_created)); ?></p>
							<p><?php echo $com->comment?></p>
						<?php } } else { ?>
				            <p style="margin-top: 5px;">No comment yet</p>
				        <?php } ?>
				    </div>
					</div>
				</div>
						</div>	
					</div>	
				</div>
				<div class="row allvideos">
					<div class="col s12 m12 l3">
						<div class="text-center blog-disp">
							<a href="#"><!-- <?php echo base_url();?>video/<?=$vid->url?> -->
								<video class="otherVideo youtube editor-vl" src="<?//=$vid->video?>/<?//=$vid->filename?>" poster="<?=$video[0]->thumbnail ?>" data-src="" frameborder="0"></video>
								<!--<div class="playpause"></div>-->		
							</a>	
							<p class="video-title">Title<?php echo $video[0]->title;?></p>
							<p class="video-owner"><strong>Owner <?=$video[0]->uploaded_by?></strong></p>
							<p class="brief-content"><?=$numberView?> Views | <?= date('M d, Y',strtotime($video[0]->date_created)); ?></p>
						</div>	
					</div>
					
				</div>
			</div>	
			</section>
		</div>	
	</div>	
	<!-- Upload Video -->
	<div class="modal" id="sharevideo" style="width:35%;">
    <div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title"><strong>Share</strong></h4>
				<button type="button" class="close modal-close" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body">
				<form action="" id="" name="" class="" method="post" accept-charset="utf-8" novalidate="true">
				<div class="row" id="share">
					
				</div>
				</form>
			</div>
		</div>
	</div>
	</div>
	<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery.jssocials/1.4.0/jssocials.min.js"></script>
	<script>
	$(document).ready(function(){
		$('#subscribe').click(function(){
		var id=$("#profile_id").val();
		if($(this).hasClass("subscribe")){
            if(csrf_hash===""){
				csrf_hash=csrf_hash;
			}  
			$.ajax({
			url:base_url+'community/subscribe',
			type:"POST",
			cache: false,
			data:{'id':id,'status':1},
			    success:function(result){
					var data=JSON.parse(result);
					console.log("1"+data.csrf_hash);
					if(data.csrf_hash){
						csrf_hash=data.csrf_hash;
					}
					if(data.msg=="login"){
						window.location.href= base_url+"signin";
					}else{
						if(data.subscription.length >=0 && data.subscription.length<=1000){
							var subNo=data.subscription.length;
							var number=subNo;
						}
						else if(data.subscription.length >=1000 && data.subscription.length<=100000){
							var subNo=data.subscription.length/1000;
							var number=subNo+"K";
						}else{
							var subNo=data.subscription.length/100000;
							var number=subNo+"L";
						}
						$("#subscriber").html(number);
             
						$('#subscribe').css('background-color','#2da5a6','!important');
						$('#subscribe').css('border','1px solid #2da5a6','!important');
						$('#subscribe').html('<i style="font-size:16px !important;" class="fa fa-user" aria-hidden="true"></i>&nbsp; &nbsp;SUBSCRIBED');
						$('#subscribe').removeClass('subscribe');
						$('#subscribe').addClass('unsubscribe');	
					}
				},
			});


		}else{
			if(csrf_hash===""){
				csrf_hash=csrf_hash;
			}
			  $.ajax({
			url:base_url+'community/subscribe',
			type:"POST",
			cache: false,
			data:{'id':id,'status':0},
			    success:function(result){
					var data=JSON.parse(result);
					console.log("2"+data.csrf_hash);
					if(data.csrf_hash){
						csrf_hash=data.csrf_hash;
					}
					if(data.msg=="login"){
                       window.location.href= base_url+"login";
					}else{
                  
					if(data.subscription.length >=0 && data.subscription.length<=1000){
                         var subNo=data.subscription.length;

					 	var number=subNo;
					}
					 else if(data.subscription.length >=1000 && data.subscription.length<=100000){

					 	var subNo=data.subscription.length/1000;

					 	var number=subNo+"K";
					 }else{
					 	var subNo=data.subscription.length/100000;

					 	var number=subNo+"L";
					 }

					
					 $("#subscriber").html(number);
					
			$('#subscribe').css('background-color','#bc343a','!important');
			$('#subscribe').css('border','1px solid #bc343a','!important');
			$('#subscribe').html('<i style="font-size:16px !important;" class="fa fa-user" aria-hidden="true"></i>&nbsp; &nbsp;SUBSCRIBE');
			$('#subscribe').removeClass('unsubscribe');
			$('#subscribe').addClass('subscribe');
					}
					
				
			},
			});
			
		}
	});

		$('#submit_comment').click(function(){
			
		var vid_id=$("#video_id").val();
		var profile_id=$("#profile_id").val();
		var comment=$("#comment").val();
			if(csrf_hash===""){
				csrf_hash=csrf_hash;
			}
			$.ajax({
			url:base_url+'community/video_comment',
			type:"POST",
			cache: false,
			data:{'vid_id':vid_id,'profile_id':profile_id,'comment':comment},
			    success:function(result){
					var data=JSON.parse(result);
					console.log("3"+data.csrf_hash);
					if(data.csrf_hash){
						csrf_hash=data.csrf_hash;
					}
					if(data.msg=="login"){
						window.location.href= base_url+"signin";
					}else{
						

						$("#comment_count").html(data.comment.length);
						$("#comment_div").html("");
						var str="";
						
						for(i=0;i<data.comment.length;i++){
							
                       str +="<p style='margin-top: 5px;'><a onMouseOver='this.style.color=#bf4b3d' onMouseOut='this.style.color=grey' href='mailto:'"+data.comment[i].email+"' style='font-weight: bold; text-decoration: none; color: grey;'>"+data.comment[i].name+" "+data.comment[i].date_created+"</a></p><p>"+data.comment[i].comment+"</p>";
						}
                         $("#comment_div").html(str);
                         $("#comment_msg").html("Comment added successfully").fadeIn( "slow" );
                         $("#comment").val("");
					
					}
				},
			});


		
	});

		$('#video_share').click(function(){
			
			var vid_id=$("#video_id").val();
			var profile_id=$("#profile_id").val();
			if(csrf_hash===""){
				csrf_hash=csrf_hash;
			}
			$.ajax({
			url:base_url+'community/video_share',
			type:"POST",
			cache: false,
			data:{'vid_id':vid_id,'profile_id':profile_id},
			    success:function(result){
					var data=JSON.parse(result);
					console.log("4"+data.csrf_hash);
					if(data.csrf_hash){
						csrf_hash=data.csrf_hash;
					}
					if(data.msg=="login"){
						window.location.href= base_url+"signin";
					}else{
						$('#viewshare').trigger('click');
						//$("<a href='#sharevideo'></a>").click();
					}
				},
			});
		});
		
		$("#viewshare").click(function(e){
			$(".modal-backdrop").show();
			$("#sharevideo").show();
			$("#viewshare")[0].click();
		});

   $('#video_like').click(function(){

   	    var vid_id=$("#video_id").val();
		var profile_id=$("#profile_id").val();

		if($("#video_like").hasClass('active')){
			if(csrf_hash===""){
				csrf_hash=csrf_hash;
			}
			$.ajax({
			url:base_url+'community/video_like',
			type:"POST",
			cache: false,
			data:{'vid_id':vid_id,'profile_id':profile_id,'like':0},
			    success:function(result){
					var data=JSON.parse(result);
					console.log("5"+data.csrf_hash);
					if(data.csrf_hash){
						csrf_hash=data.csrf_hash;
					}
					if(data.msg=="login"){
						window.location.href= base_url+"signin";
					}else{
						
						if(data.likes.length >=0 && data.likes.length<=1000){
                         var subNo=data.likes.length;

					 	var number=subNo;
					}
					 else if(data.likes.length >=1000 && data.likes.length<=100000){

					 	var subNo=data.likes.length/1000;

					 	var number=subNo+"K";
					 }else{
					 	var subNo=data.likes.length/100000;

					 	var number=subNo+"L";
					 }
						$("#like_count").html(number);
						$("#video_like").removeClass('active');
					
					}
				},
			});
			
			
		}else{
			if(csrf_hash===""){
				csrf_hash=csrf_hash;
			}
			$.ajax({
			url:base_url+'community/video_like',
			type:"POST",
			cache: false,
			data:{'vid_id':vid_id,'profile_id':profile_id,'like':1},
			    success:function(result){
					var data=JSON.parse(result);
					console.log("6"+data.csrf_hash);
					if(data.csrf_hash){
						csrf_hash=data.csrf_hash;
					}
					if(data.msg=="login"){
						window.location.href= base_url+"signin";
					}else{
						
							if(data.likes.length >=0 && data.likes.length<=1000){
                         var subNo=data.likes.length;

					 	var number=subNo;
					}
					 else if(data.likes.length >=1000 && data.likes.length<=100000){

					 	var subNo=data.likes.length/1000;

					 	var number=subNo+"K";
					 }else{
					 	var subNo=data.likes.length/100000;

					 	var number=subNo+"L";
					 }
						$("#like_count").html(number);
						$(".video-like").addClass('active');
					
					}
				},
			});
			
		}
		
	});
	//$('.bvfoot').hide();
	 var vid_id=$("#video_id").val();
	var url=base_url+'community/video_comment/'+vid_id;
	jsSocials.shares.instagram = {
		label: "Instagram",
		logo:"<?php echo base_url(); ?>asset/images/sm_icons/icon_instagram.png",
		shareUrl: "https://instagram.com/bharatvaani?url={url}&text={text}&via={via}&hashtags={hashtags}",
		getCount: function(data) {
			return data.count;
		}
	};
	jsSocials.shares.koo = {
		label: "KOO",
		logo:"<?php echo base_url(); ?>asset/images/sm_icons/koo.png",
		shareUrl: "https://www.kooapp.com/share?url={url}&text={text}&via={via}&hashtags={hashtags}",
		getCount: function(data) {
			return data.count;
		}
	};
	jsSocials.shares.sharechat = {
		label: "Sharechat",
		logo:"<?php echo base_url(); ?>asset/images/sm_icons/sharechat.png",
		shareUrl: "https://sharechat.com/share?url={url}&text={text}&via={via}&hashtags={hashtags}",
		getCount: function(data) {
			return data.count;
		}
	};
	jsSocials.shares.moj = {
		label: "Moj",
		logo:"<?php echo base_url(); ?>asset/images/sm_icons/moj.png",
		shareUrl: "https://mojapp.in/share?url={url}&text={text}&via={via}&hashtags={hashtags}",
		getCount: function(data) {
			return data.count;
		}
	};
	jsSocials.shares.takatak = {
		label: "Takatak",
		logo:"<?php echo base_url(); ?>asset/images/sm_icons/takatak.png",
		shareUrl: "https://www.mxtakatak.com/share?url={url}&text={text}&via={via}&hashtags={hashtags}",
		getCount: function(data) {
			return data.count;
		}
	};
	$("#share").jsSocials({

		//FB, twitter, LinkedIn, Whatsapp, Instagram, Koo, Sharechat, Moj app, Josh, Takatak
		showLabel: false,
		showCount: false,
		shares: [{
			share: "facebook",
			url:url,
			logo:"<?php echo base_url(); ?>asset/images/sm_icons/icon_facebook.png",
		}, {
			share: "twitter",
			url:url,
			logo:"<?php echo base_url(); ?>asset/images/sm_icons/icon_twitter.png",
		},{
			share: "linkedin",
			url:url,
			logo:"<?php echo base_url(); ?>asset/images/sm_icons/icon_linkedin.png",
		},{
			share: "whatsapp",
			url:url,
			logo:"<?php echo base_url(); ?>asset/images/sm_icons/icon_whatsapp.png",
		},{
			share: "instagram",
			label: "instagram",
			url:url,
			logo:"<?php echo base_url(); ?>asset/images/sm_icons/icon_instagram.png",
		},{
			share: "koo",
			url:url,
			logo:"<?php echo base_url(); ?>asset/images/sm_icons/koo.png",
		},{
			share: "sharechat",
			url:url,
			logo:"<?php echo base_url(); ?>asset/images/sm_icons/sharechat.png",
		},{
			share: "moj",
			url:url,
			logo:"<?php echo base_url(); ?>asset/images/sm_icons/moj.png",
		},{
			share: "takatak",
			url:url,
			logo:"<?php echo base_url(); ?>asset/images/sm_icons/takatak.png",
		}]
	});
	

	

});
</script>
<?php $this->load->view('template/footer'); ?>	