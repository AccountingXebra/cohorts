<?php $this->load->view('my-community/Cohorts-header');?>
	<!-- Include DataTables CSS & JS -->
	<link rel="stylesheet" href="https://cdn.datatables.net/1.13.6/css/jquery.dataTables.min.css">
	<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
	<script src="https://cdn.datatables.net/1.13.6/js/jquery.dataTables.min.js"></script>

    <style type="text/css">
		#interviewTable_wrapper{ margin:0 1%; }
		#interviewTable_length label {
			display: flex;
			font-size: 0px;
		}

		#interviewTable_length .select-wrapper span.caret {
    		color: #595959;
    		top: -2px;
		}

		/* Style the dropdown to fit correctly */
		#interviewTable_length select {
			padding: 5px 10px;
			border: 1px solid #ccc;
			border-radius: 5px;
			background: #fff;
			font-size: 14px;
			cursor: pointer;
			min-width: 100px; /* Ensure the dropdown has a proper width */
		}


	#interviewTable_length .dropdown-content {
		min-width: 96px;
		margin-top:40% !important;
	}
	
	#interviewTable_length{
		border:1px solid #B0B7CA !important;
		height:38px;
		border-radius:4px;
		width:95px;
		margin-top:5px;
		margin-left:52%;
	}
	
	#interviewTable_length .select-wrapper input.select-dropdown {
		margin-top:-3px !important;
		margin-left:10px !important;
	}
	
	#interviewTable_length .select-wrapper span.caret {
		margin: 17px 7px 0 0;
	}	

	@media only screen and (max-width: 600px) {
		.videosection .blog-disp {
			margin: 0 0 20% 20px;
			width: 80%;
		}	
	}
	.videosection.mar25{ padding-top:25px !important; }	
    #content{ padding: 0% 0 0% 0; border-bottom: 1px solid #f7f7f7; }
	.interview_row .pp-header{ padding:2.5% 0; }
	.interview_row .pp-header h1{ color:#595959; font-size:32px; }
	.interview_row .pp{ padding: 5px 0; line-height:25px; font-size:18px; text-align: justify; } 
	.interview_row .tc-info{ padding: 0 12%; }
	.bvfoot{ margin-top:5% !important; }
	.a-link{ padding:1% 0px 0px 30px; }
	.blog-disp .blog-title{
		width:250px;
		margin:15px 0 15px 10px;
		text-align:left;
		line-height:25px;
	}
	.blog-disp .brief-content{
		width:250px;
		margin:0 0 6% 10px;
		text-align:left;
	}
	.playpause {
		background-image:url("<?=base_url('asset/images/overlay.png');?>");
		background-repeat:no-repeat;
		background-repeat: no-repeat;
		width: 40%;
		height: 15%;
		position: absolute;
		left: -18%;
		right: 0%;
		top: -30%;
		bottom: 0%;
		margin: auto;
		background-size: contain;
		background-position: center;
		cursor:pointer;
	}
	.top-filter{ border-bottom:1px solid #ecedef; width:105.5% !important }
	.top-filter p{ font-size:18px; font-weight:600;	}
	.container { width: 100% !important; padding: 0px;}
	.cat_videos{ height: 100%; margin:8% 0 0 0; }
	.btn-dropdown-select{ width:105%; } 
	.videosection{ border-left:1px solid #ecedef; margin: 0; padding: 0px 0 0 25px !important; }
	.video-owner, .video-title, .brief-content{ text-align:left; margin:5px 0 0 0; } 
	#breadcrumbs-wrapper { padding: 25px 0 15px 0; }
	.select-cat p{ font-size:16px; font-weight:600;	}
	.catselect{ text-transform:uppercase; }
	.vid_cat{ margin-bottom:6%; }
	.blog-disp{ margin:0 0 20% -15px; border-radius: 10px; width: 103%; height:260px; box-shadow: 0 2px 5px #ebecf0; -moz-box-shadow: 0 2px 5px #EBECF0; -o-box-shadow: 0 2px 5px #EBECF0; -webkit-box-shadow: 0 2px 5px #ccc; -ms-box-shadow: 0 2px 5px #EBECF0;}
	#submit{ height: 38px; background-color: #7864e9; border: 1px solid #ccc; border-radius: 5px; color:#fff; font-size:13px; width:45%; margin:5% 0 0 0;}
	.pagination .current{ background-color: #ff7c9b; border-radius: 5px; color:#fff; }
	.pagination .prev, .pagination .next{ font-size:25px; }
	.pagination .page:hover{ cursor:pointer; }
	.pagination{  margin: 3% 5% 3% 0; text-align:right; }
	.pagination a{ padding: 10px 15px; color:#313131; }	
	.pageindex.current{ padding: 10px 15px;  background-color: #ff7c9b;
    border-radius: 5px; color: #fff; text-align:  unset; margin: 0 10px; font-size: 15px; font-weight: 500; border:1px solid #ff7c9b;}
	.pageindex{ padding: 10px 15px;  background-color: transparent;
		border-radius: 5px; color: #595959; text-align:  unset; margin: 0 10px; font-size: 15px; font-weight: 500; border:1px solid #595959; }
	#paginator{ float: right; margin: 0 5%; }
	.mediaInterview{ width:24% !important; }
	body{ overflow-x:hidden; }
    </style>
    <div id="main" style="padding-left:0px !important; margin-top:-1%;">
		<div class="wrapper">
			<section id="content" class="bg-cp">
			<div id="breadcrumbs-wrapper">
				<div class="container custom">
					<div class="row">
						<div class="col s10 m6 l6" style="margin:0 0 0 1%;">
							<h5 class="breadcrumbs-title my-ex">Interviews</h5>
							<ol class="breadcrumbs">
								<li><a href="index.html">MY COMMUNITY / INTERVIEWS</a>
							</ol>
						</div>
					</div>
				</div>
			</div>
			<div class="container">
					<div class="row blog-content">
						<div class="col s12 m12 l2">
							<div class="col s12 m12 l12 top-filter text-center">
								<p>Filter By</p>
							</div>
							<div class="col s12 m12 l12 cat_videos">
								<form name="search_catform" id="search_catform" method="post">
									<?php $csrf = array(
					                    'name' => $this->security->get_csrf_token_name(),
					                    'hash' => $this->security->get_csrf_hash()
				                    );
				                    ?>
				                    <input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
									<select class='ml-3px border-radius-6 border-split-form btn-dropdown-select select-like-dropdown vid_cat' id="innovo_name" name="innovo_name" onchange="javascript:form.submit()">
										<option value="">INNOVATOR'S NAME</option>
										<option value="">ALL</option>
										<?php foreach($interviews as $inws){ ?>
											<option value="<?=$inws->name?>" <?php if(isset($innovo_name)){if($innovo_name==$inws->name){echo "selected";}}?>><?=strtoupper($inws->name);?></option>
										<?php } ?>
									</select>
									
								</form>
								<!--<select class='ml-3px border-radius-6 border-split-form btn-dropdown-select select-like-dropdown vid_cat' id="company_name" name="company_name">
									<option value="">COMPANY NAME</option>
									<option value="all">ALL</option>
								</select>-->
							</div>
						</div>
						<div class="col s12 m12 l10 mar25 videosection">
						<div class="col s12 m12 l12 select-cat" hidden>
							<p class="catselect">ALL INNOVATOR</p>
						</div>
						<div class="row" id="interviewList">
						<?php if(count($interviews)>0){ ?>
						<?php 
							//$nb_elem_per_page = 8;
							//$page = isset($_GET['page'])?intval($_GET['page']-1):0;
							//$number_of_pages = intval(count($interviews)/$nb_elem_per_page)+2;
							//foreach (array_slice($interviews, $page*$nb_elem_per_page, $nb_elem_per_page) as $inws) { ?>
							<?php foreach ($interviews as $inws) { ?>
							<div class="col s12 m12 l3 mediaInterview interview-card">
							<div class="text-center blog-disp">
								<a href="<?php echo base_url();?>interviews/<?=$inws->url?>"><video style="width:280px; height:158px; border-radius:10px; object-fit:fill; margin-right: 340px;" class="" src="<?=$inws->video."/".$inws->filename?>" poster="<?=$inws->cover?>" data-src="#" frameborder="0"></video>
						</a>
								<!--<a href="<?php //echo base_url();?>interviews/<?//=$inws->url?>"><div class="playpause"></div></a>-->
						<p class="blog-title"></p>
						<p class="blog-title"><strong>Interview of</strong> - <?=$inws->name?></p>
						<!--p class="brief-content"><strong>Conducted by</strong> - <?=$inws->conduct?></p>-->
							</div>	
						</div>
					
							<?php }?>
							</div>		
							<!-- <div class="row" id='paginator'>
							<?php 
							// if(count($interviews) > 6 ) {
							// 	for($i=1;$i<$number_of_pages;$i++){ 
							// 		if(isset($_GET['page']) && $_GET['page'] == $i){ $current="current"; 
							// 		}else if(!isset($_GET['page']) && $i == 1){ $current="current"; 
							// 		}else{ $current=""; }
							// 		echo '<a class="pageindex '.$current.'" href="?page='.$i.'">'.$i.'</a>';
							// 	}
							// }
							?>
							</div>						 -->
							<?php }else{ ?> 
							<div class="col s12 m12 l12">
								<h5 style="font-size:18px;">Currently, there are no interviews listed of the innovator</h5>
							</div>
							<?php } ?>
						</div>	
					</div>
			</div>	
			</section>
		</div>	
	</div>	
	<script>
		$(document).ready(function () {
			let interviewList = $('#interviewList .interview-card'); // Get all divs
			let table = $('<table id="interviewTable"><thead><tr><th>Hidden</th></tr></thead><tbody></tbody></table>'); 
			$('body').append(table.hide()); // Append hidden table for pagination

			// Insert each div as a row in the fake table
			interviewList.each(function () {
				$('#interviewTable tbody').append('<tr><td></td></tr>');
			});

			// Initialize DataTables
			let dataTable = $('#interviewTable').DataTable({
				"bPaginate": true,
			    "bLengthChange": true,
    			"sDom": 'Rfrtlip',
    			"bFilter": true,
    			"bInfo": false,
    			"searching":false,
    			"iDisplayLength": 10,
				lengthMenu: [
					[ 10, 20, 30, 50, -1 ],
					[ 'SHOW 10', 'SHOW 20', 'SHOW 30', 'SHOW 50', 'SHOW ALL' ]
				],
			});

			// Hide all divs initially and only show the first page
			function showPage() {
				interviewList.hide();
				let pageIndexes = dataTable.rows({ page: 'current' }).indexes();
				pageIndexes.each(function (index) {
					interviewList.eq(index).show();
				});
			}

			showPage(); // Show initial page

			// Update visible divs on page change
			$('#interviewTable').on('page.dt length.dt', function () {
				showPage();
			});
		});

	$(document).ready(function() {
			
		$("#innovo_name").on('select2:open', function () {
    		$('.select2-search__field').attr('placeholder', '');
			$('.select-dropdown').val('');
		});
		$("#innovo_name").on("change",function(){
			var cat_name = $(this).val();
			if(cat_name == 'all'){
				$('.catselect').text("ALL INNOVATOR");
			}else{ $('.catselect').text(cat_name); }
		});
		
		$("#company_name").on("change",function(){
			var cat_name = $(this).val();
			if(cat_name == 'all'){
				$('.catselect').text("ALL COMPANY");
			}else{ $('.catselect').text(cat_name); }
		});

		(function($) {
		var pagify = {
			items: {},
			container: null,
			totalPages: 1,
			perPage: 3,
			currentPage: 0,
			createNavigation: function() {
				this.totalPages = Math.ceil(this.items.length / this.perPage);
				$('.pagination', this.container.parent()).remove();
				var pagination = $('<div class="pagination"></div>').append('<a class="nav prev disabled" data-next="false"><</a>');
				for (var i = 0; i < this.totalPages; i++) {
					var pageElClass = "page";
					if (!i)
						pageElClass = "page current";
					var pageEl = '<a class="' + pageElClass + '" data-page="' + (
						i + 1) + '">' + (
						i + 1) + "</a>";
					pagination.append(pageEl);
				}
				pagination.append('<a class="nav next" data-next="true">></a>');
				this.container.after(pagination);
				var that = this;
				$("body").off("click", ".nav");
					this.navigator = $("body").on("click", ".nav", function() {
					var el = $(this);
					that.navigate(el.data("next"));
				});
				$("body").off("click", ".page");
				this.pageNavigator = $("body").on("click", ".page", function() {
					var el = $(this);
					that.goToPage(el.data("page"));
					$("html, body").animate({ scrollTop: 0 }, "slow");
				});
			},
			navigate: function(next) {
				// default perPage to 5
				if (isNaN(next) || next === undefined) {
					next = true;
				}
				$(".pagination .nav").removeClass("disabled");
				if (next) {
					this.currentPage++;
					if (this.currentPage > (this.totalPages - 1))
						this.currentPage = (this.totalPages - 1);
					if (this.currentPage == (this.totalPages - 1))
						$(".pagination .nav.next").addClass("disabled");
					}
				else {
					this.currentPage--;
					if (this.currentPage < 0)
						this.currentPage = 0;
					if (this.currentPage == 0)
					$(".pagination .nav.prev").addClass("disabled");
				}
				this.showItems();
			},
			updateNavigation: function() {
				var pages = $(".pagination .page");
				pages.removeClass("current");
				$('.pagination .page[data-page="' + (
				this.currentPage + 1) + '"]').addClass("current");
			},
			goToPage: function(page) {
				this.currentPage = page - 1;
				$(".pagination .nav").removeClass("disabled");
				if (this.currentPage == (this.totalPages - 1))
				$(".pagination .nav.next").addClass("disabled");
				if (this.currentPage == 0)
				$(".pagination .nav.prev").addClass("disabled");
				this.showItems();
			},
			showItems: function() {
				this.items.hide();
				var base = this.perPage * this.currentPage;
				this.items.slice(base, base + this.perPage).show();
				this.updateNavigation();
			},
			init: function(container, items, perPage) {
				this.container = container;
				this.currentPage = 0;
				this.totalPages = 1;
				this.perPage = perPage;
				this.items = items;
				this.createNavigation();
				this.showItems();
			}
		};
		// stuff it all into a jQuery method!
		$.fn.pagify = function(perPage, itemSelector) {
			var el = $(this);
			var items = $(itemSelector, el);
			// default perPage to 5
			if (isNaN(perPage) || perPage === undefined) {
				perPage = 3;
			}
			// don't fire if fewer items than perPage
			if (items.length <= perPage) {
				return true;
			}
			pagify.init(el, items, perPage);
		};
		})(jQuery);	
	
		//$(".videosection").pagify(8, ".mediaInterview");	

	});		
	</script>
<?php $this->load->view('template/footer'); ?>	