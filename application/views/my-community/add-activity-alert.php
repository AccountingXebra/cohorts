<?php $this->load->view('template/header.php'); ?>

<style type="text/css">

  input.valid:not([type]), input.valid:not([type]):focus, input[type=text].valid:not(.browser-default) {
  /*border-bottom: 0px !important;*/
  }
  .border-split-form .select-wrapper{
  padding: 7px 0 2px 0 !important;
  }
  /*.fl-r img.green-bel {
  margin: 0px -16px 0 19px;
  width: 20px;
  }*/
  .full-bg{
  border-bottom: none !important;
  padding-top: 20px !important;
  display: inline-flex;
  }
  img.green-bel {
  width: 21px !important;
  margin: 12px 0 0 5px;
  }
  .no-type{
  color: #666;
  font-size: 12px;
  margin-top: 17px !important;
  font-weight: normal;
  text-transform: uppercase;
  }
  .w-25{
  width: 25px !important;
  }
  .select-wrapper label.error:not(.active) {
  margin: -30px 0 0 -11px;
  }
  .selected_notification {
  border:none !important;
  }
  a.info-ref.tooltipped.info-tooltipped {
  position: absolute;
  }
  /*----------START SEARCH DROPDOWN CSS--------*/
  .select2-container--default .select2-selection--single {
  border:none;
  }
  input[type="search"]:not(.browser-default) {
  height: 30px;
  font-size: 14px;
  margin: 0;
  border-radius: 5px;

  }
  .select2-container--default .select2-selection--single .select2-selection__rendered {
  font-size: 12px;
  line-height: 35px;
  color: #666;
  font-weight: 400;
  }
  .select2-container--default .select2-selection--single .select2-selection__arrow {
  height: 32px;
  right: 14px;
  }
  .select2-search--dropdown {
  padding: 0;
  }
  input[type="search"]:not(.browser-default):focus:not([readonly]) {
  border-bottom: 1px solid #bbb;
  box-shadow: none;
  }
  .select2-container--default .select2-selection--single:focus {
  outline: none;
  }
  .select2-container--default .select2-results__option--highlighted[aria-selected] {
  background: #fffaef;
  color: #666;
  }
  .select2-container--default .select2-results > .select2-results__options {
  font-size: 14px;
  border-radius: 5px;
  box-shadow: 0px 2px 6px #B0B7CA;
  width: 98%;
  }
  .select2-dropdown {
  border: none;
  border-radius: 5px;
  }
  .select2-container .select2-selection--single {
  height: 53px;
  }
  .select2-results__option[aria-selected] {
  border-bottom: 1px solid #f2f7f9;
  padding: 14px 16px;
  }
  .select2-container--default .select2-search--dropdown .select2-search__field {
  border: 1px solid #d0d0d0;
  padding: 0 0 0 15px !important;
  width: 91%;
  max-width: 100%;
  background: #fff;
  }
  .select2-container--open .select2-dropdown--below {
  margin-top: -15px;
  }
  /*----------END SEARCH DROPDOWN CSS--------*/
  /*---Dropdown error message format---*/
  .select-wrapper + label.error{
  margin: 18px 0 0 -10px;

  } 

  .tooltip_i{
  right: 18px;
  top: 8px;
  }

  body.add-vo .invoi-drop input, select, textarea {
    
    height: 100px !important;
   
}



  select.error + label.error:not(.active){

  margin: -20px 0 0 -10px; 
  }
  select + label.error.active{
  margin-left: -10px;
  }
  /*---End dropdown error message format---*/

  h4.box-inner-title {
  margin: 20px 0 0 0 !important;
  }

</style>

<div id="main"> <!-- START MAIN -->
  <div class="wrapper"> <!--Wrapper-->

    <?php $this->load->view('template/sidebar.php'); ?>

    <?php 
      $email = $this->user_session['email'];
      $mobile = $this->user_session['mobile'];
    ?>

    <input id="email" name="email" class="readonly-bg-grey full-bg adjust-width border-right" type="hidden"  value="<?= $email; ?>" readonly="readonly">
    
    <input id="mobile" name="mobile" class="readonly-bg-grey full-bg adjust-width border-right" type="hidden"  value="<?= $mobile; ?>" readonly="readonly">

    <!--Form-->
    <form class="create-company-form border-split-form" id="add_activity_alert" name="add_activity_alert" method="post">
      <?php $csrf = array(
					'name' => $this->security->get_csrf_token_name(),
					'hash' => $this->security->get_csrf_hash()
				);
				?>
				<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
	  <section id="content"> <!--Section-->
        <div class="container"> <!--Container-->

          <div class="plain-page-header"> <!--Page header-->
            <div class="row">
              <div class="col l6 s12">
                <a class="go-back underline" href="<?php echo base_url(); ?>settings/activity-alerts">Back to My Alerts</a> 
              </div>
              <div class="col l6 s12 m6"></div>
            </div>
          </div> <!--End Page header-->

          <div class="page-content"> <!--Page content-->
            <div class="row" style="margin-top: -20px !important"> <!--Row-->
              <div class="col s12 m12 l3"></div>
              <div class="col s12 m12 l6"> <!--1st-->

                <div class="box-wrapper bg-white bg-img-green shadow border-radius-6"> <!--Alert no. - date-->

                  <div class="box-header">
                    <h3 class="box-title">New Activity Alert</h3>
                  </div>

                  <div class="box-body">

                    <div class="row ">
                      <div class="col s12 m12 l12">

                        <div class="input-field col s12 m12 l6 padd-n">
                          <label for="alert_number" class="full-bg-label">Alert No.</label>
                          <input id="alert_number" name="alert_number" class="readonly-bg-grey full-bg adjust-width border-right" type="text"  value="<?= $alertNo; ?>" readonly="readonly">
                        </div>

                        <div class="input-field col s12 m12 l6 padd-n">
                          <label for="alert_date" class="full-bg-label">Alert Date</label>
                          <input id="alert_date" name="alert_date" class="readonly-bg-grey full-bg adjust-width border-top-none valid" type="text" value="<?= date('d-m-Y'); ?>" readonly="readonly">
                        </div>

                      </div>
                    </div>

                    <div class="row">
                      <div class="col s12 m12 l12">

                        <div class="col s12 m12 l12 padd-n">
                          <div class="row">
                            <div class="input-field border-bottom">
                              <label for="alert_name" class="full-bg-label">Activity / Task Title</label>
                              <input id="alert_name" name="alert_name" class="full-bg adjust-width border-top-none border-right border-bottom-none" type="text">
                            </div>
                          </div>
                        </div>

                        
                      </div>
                    </div>

                    <div class="row">
                      <div class="col s12 m12 l12">

                        <div class="col l6 s12 m6 fieldset">
                          <div class="input-field ">
                            <label for="alert_date_time" class="full-bg-label">DATE<span class="required_field"> *</span></label>
                            <input name="alert_date_time" id="alert_date_time" class="full-bg adjust-width border-top-none bdatepicker icon-calendar-gray border-right" type="text" autocomplete="off" readonly="readonly">
                          </div>
                        </div>
                        
                        <div class="col l6 s12 m6 fieldset">
                          <div class="input-field">
                            <label for="alert_time" class="full-bg-label">TIME<span class="required_field"> *</span> &nbsp&nbsp (e.g: 1800 / 0700)</label>
                              <input id="alert_time" name="alert_time" class="numeric_number full-bg adjust-width border-top-none border-right border-bottom-none" type="text">
                          </div>
                        </div>

                      </div>
                    </div>
                  </div>
                </div> <!--End Alert no. - date-->

                <div class="step22"> <!--Alert Info-->
                  <h4 class="box-inner-title">Alert Info</h4>
                  <a class="info-ref tooltipped info-tooltipped" data-position="bottom" data-delay="50" data-tooltip="Will be update, Coming Soon..!" data-tooltip-id="a4791c04-e6b5-9640-532d-84ec1e9ac670"></a>
                  <div class="box-wrapper bg-white bg-img-green-after bg-right shadow border-radius-6">
                    <div class="box-body">

                      <div class="row pl-2 pr-2">

                        <div class="col l6 s12 m6 fieldset">
                          <div class="input-field ">
                            <label class="full-bg-label">By When<span class="required_field"> *</span></label>
                            <input name="alert_reminder" id="alert_reminder" class="full-bg adjust-width border-top-none bdatepicker icon-calendar-gray border-right" type="text" autocomplete="off" readonly="readonly">
                          </div>
                        </div>

                        <div class="col l6 s12 m6 full-bg">
                          <span id="notification_error"></span>
                          <input type="hidden" name="alert_notification" id="alert_notification" value=""/>
                          <input type="hidden" name="alert_msg" id="alert_msg" value="" />
                          <input type="hidden" name="alert_mail" id="alert_mail" value="" />

                          <span class="no-type">TYPES OF ALERT<span class="required_field">*</span></span> 
                          &nbsp;&nbsp;&nbsp;&nbsp; <a class="cur noti_type" data-noti_type="1"><img src="<?php echo base_url(); ?>asset/css/img/icons/bell-grey.png" alt="bel" class="green-bel" title="Notification"></a>
                          <a class="cur noti_type" data-noti_type="2"><img src="<?php echo base_url(); ?>asset/css/img/icons/msg-grey.png" alt="msg" class="green-bel msg" title="Message"></a>
                          <a class="cur noti_type" data-noti_type="3"><img src="<?php echo base_url(); ?>asset/css/img/icons/mail-grey.png" alt="email" class="green-bel w-25 email" title="Email"></a>
                        </div>

                      </div>

                      <div class="row pl-2 pr-2">
                        <div class="col l6 s12 m12 fieldset noti_mobile">
                          <div class="input-field">
                            <label class="full-bg-label">ENTER MOBILE NUMBER</label>
                            <input name="alert_mobile" id="alert_mobile" class="full-bg adjust-width gstin border-bottom-none border-bottom-none numeric_number" type="text" value="" readonly="readonly">
                          </div>
                        </div>

                        <div class="col l6 s12 m12 fieldset noti_email">
                          <div class="input-field">
                            <label class="full-bg-label">ENTER EMAIL ADDRESS</label>
                            <input id="alert_email" name="alert_email" class="full-bg adjust-width border-bottom-none border-bottom-none" type="email" value="" readonly="readonly">
                          </div>
                        </div>

                      </div>

                    </div>
                  </div>
                </div> <!--End Alert Info-->


                <h4 class="box-inner-title step22"> <!--Add notes-->
                  Add Notes
                  <a class="info-ref tooltipped info-tooltipped" data-position="bottom" data-delay="50" data-tooltip="Notes added here are for internal reference only and will not appear on print"></a>
                </h4>

                <div class="box-wrapper bg-white sec-statur shadow border-radius-6">
                  <div class="box-body">
                    <div class="row">
                      <div class="col l12 s12 m12">
                        <div class="input-field input-set note-div">
                          <textarea style="background-color:#fff !important;" id="notes" name="notes" class="full-bg adjust-width border-top-none" type="text" placeholder="ADD LOCATION, URL OR POINTERS"></textarea>
                        </div>
                      </div>
                    </div>
                  </div>
                </div> <!--End Add notes-->

                <div class="row"> <!--Bottom line-->
                  <div class="col s12 m12 l12">
                    <div class="form-botom-divider"></div>
                  </div>
                </div> <!--End Bottom line-->

                <div class="row"> <!--Button-->
                  <div class="col s12 m12 l6 right">
                    <button class="btn-flat theme-primary-btn theme-btn theme-btn-large right ml-5" type="submit">Save</button>
                    <button class="btn-flat theme-flat-btn theme-btn theme-btn-large right" type="button" onclick="location.href = '<?php echo base_url(); ?>settings/activity-alerts';">Cancel</button>
                  </div>
                </div> <!--End button-->

              </div> <!--End 1st-->
            </div> <!--End Row-->
            <div class="col s12 m12 l3"></div>
          </div> <!--End Page content-->
        </div> <!--End Container-->
      </section> <!--End Section-->
    </form> <!--End Form-->
  </div> <!--End Wrapper-->
</div> <!--End Start Main-->

<script type="text/javascript">
  $(document).ready(function() {
    $('.js-example-basic-single').select2();
      $('.select2-selection__rendered').each(function () {
      $(this).html($(this).html().replace(/(\*)/g, '<span style="color: red;">$1</span>'));
    });

    $("select").change(function () {
      if($(this).val()!=''){
        $(this).valid();
        $(this).closest('.input-field').find('.error').remove();
      }
    });
  });

  
</script>

<?php $this->load->view('template/footer.php'); ?>