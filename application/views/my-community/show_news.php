<?php $this->load->view('my-community/Cohorts-header'); ?>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style type="text/css">
	.icon-font{
		color:#ff7d9a !important;
		font-size:20px;
		width:10% !important;
	}
	p.event-desc{ 
		height:150px; 
		margin:-6px 0; 
		display: block; -webkit-line-clamp: 3; -webkit-box-orient: vertical; overflow: hidden; text-overflow: ellipsis;
	}
	@media all and (-ms-high-contrast: none), (-ms-high-contrast: active) { 
		a{
			color:#000;
		}
		.btn-date-bill {
			line-height: 34px !important;
		}
		.select2-container{
			margin-top:-4px;
		}
	}
		
	.dataTables_scrollBody{
		overflow:hidden !important;
		height:100% !important;
	}

	.dataTables_scrollHead{
		margin-bottom:-24px !important;
	}

	.btn-theme {
		font-size: 12px;
		font-weight: 500;
		padding: 0 10px;
		border-radius: 5px;
		line-height: 42px;
		height: 40px;
		letter-spacing: 0.20px !important;
		background-color: #7864e9;
		margin: 0 -10px 0px 0px;
	}

	.btn-search {
		border: 1px solid #d4d8e4;
		border-radius: 6px;
		background: none;
		height: 40px;
		line-height: 40px;
		padding: 0;
		box-shadow: unset;
		-webkit-box-shadow: unset;
		-o-box-shadow: unset;
		-moz-box-shadow: unset;
		-ms-box-shadow: unset;
		margin: 0 0 0 0px;
		color: #B0B7CA;
	}

	table.dataTable thead .sorting {
		background-position: 110px 15px !important;
	}
	table.dataTable thead .sorting_asc {
		background-position: 110px 15px !important;
	}
	table.dataTable thead .sorting_desc {
		background-position: 110px 15px !important;
	}

	.select-dropdown{
		max-height: 350px !important;
	}
	.btn-theme-disabled{
		cursor: not-allowed;
	}
	.icon-img {
		margin: 0 30px 0 2px;
	}

	/*----------START SEARCH DROPDOWN CSS--------*/
	.select2-container--default .select2-selection--single {
	  border:none;
	}

	input[type="search"]:not(.browser-default) {
	  height: 30px;
	  font-size: 12px;
	  margin: 0;
	  border-radius: 5px;

	}
	.select2-container--default .select2-selection--single .select2-selection__rendered {
	  font-size: 13px !important;
	  line-height: 30px;
	  color: #000 !important;
	  font-weight: 400 !important;

	}
	.select2-container--default .select2-selection--single .select2-selection__arrow {
	  height: 40px;
	}
	.select2-search--dropdown {
	  padding: 0;
	}
	input[type="search"]:not(.browser-default):focus:not([readonly]) {
	  border-bottom: 1px solid #bbb;
	  box-shadow: none;
	}
	.select2-container--default .select2-selection--single:focus {
		outline: none;
	}
	.select2-container--default .select2-results__option--highlighted[aria-selected] {
		background: #fffaef;
	  color: #666;
	}
	.select2-container--default .select2-results > .select2-results__options {
	  font-size: 12px;
	  border-radius: 5px;
	  box-shadow: 0px 2px 6px #B0B7CA;
	}
	.select2-dropdown {
	  border: none;
	  border-radius: 5px;
	}
	.select2-container .select2-selection--single {
	  height: 40px;
		padding: 6px;
		border: 1px solid #d4d8e4;
		background: #f8f9fd;
		border-radius: 5px;
	}
	.select2-results__option[aria-selected] {
	  border-bottom: 1px solid #f2f7f9;
	  padding: 14px 16px;
	}
	.select2-container--default .select2-search--dropdown .select2-search__field {
		border: 1px solid #d0d0d0;
		padding: 0 0 0 0px !important;
		width: 99.5%;
		max-width: 100%;
	}
	.select2-search__field::placeholder {
	  content: "Search Here";
	}
	.select2-container--open .select2-dropdown--below {
	  margin-top: 0;
	}
	/*----------END SEARCH DROPDOWN CSS--------*/

	  .dataTables_length {

		margin-left: 500px;
	}
	#my-customers_length{
		border:1px solid #B0B7CA;
		height:38px;
		border-radius:4px;
		width:90px;
		margin-top:5px;
		margin-left:52%;
	}

	#my-customers_length .select-wrapper input.select-dropdown {
		margin-top:-3px !important;
		margin-left:10px !important;
	}

	#my-customers_length .dropdown-content {
		min-width: 90px;
		margin-top:-50% !important;
	}

	.customer_profile_bulk_action.filled-in:not(:checked) + label:after {
		top:5px !important;
	}

	#my-customers_length .select-wrapper span.caret {
		margin: 17px 5px 0 0;
	}

	.sticky {
		position: fixed;
		top: 70px;
		width: 76.2%;
		z-index:999;
		background: white;
		color: black;
	}

	.sticky + .scrollbody {
		padding-top: 102px;
	}

	.tipxebra{
		margin-top:-10px;
	}

	#c_start_date, #c_end_date{
		max-width:80px !important;
		height:35px !important;
		padding-top:2px !important;
		margin-left:0px !important;
	}
	.select2-container {
		width: 230px !important;
		margin-top:-1px;
	}
	
	@-moz-document url-prefix() {
		#c_start_date, #c_end_date{
			max-width:80px !important;
			height:33px !important;
			padding-top:6px !important;
			margin-left:0px !important;
		}
		
		.select2-container {
			width: 230px !important;
			margin-top:-5px;
		}
		
		.btn-date{
			max-width:80px !important;
			height:33px !important;
			padding-top:6px !important;
			margin-left:0px !important;
		}
		
		.btn-search{
			margin-top:-5px;
		}
	}

	span.tabl {
		margin: -4px 0 0 21px !important;
	}

	#c_start_date::placeholder{
		color:#000;
		font-size:12px;
	}
	#c_end_date::placeholder{
		color:#000;
		font-size:12px;
	}

	.ex{
		margin:-28px 0 0 48px !important;
	}

	.btn-stated {
		background-position: 95px center !important;
	}

	.offer-1{
		border-radius:7px;
		padding:0 2%;
		border-left:4px solid #fff;
	}

	.offer-1:hover{
		/*border-left:4px solid #ff7d9a;*/
	}

	.img-offer{
		padding: 1% 6% !important;
	}

	.offer-info{
		padding: 1% 0 !important;
	}
	
	.color-purple{
		color: #644dd5 !important;
		font-size:16px;
	}

	.set_remin{
		/*background-color: #ff7d9a !important;*/
		background-color: #644dd5 !important;
		height: 35px;
		line-height: 37px;
		padding-left: 0px;
		margin: 6px 10px;
	}

	a.showmore{
		color: #ff7d9a !important;
		text-decoration: underline;
		text-align:center;
		font-size: 12px !important;
		padding-top: 15px;
	}

	.offer-info h6{
		color:#644dd5 !important;
	}

	.offer_description{
		font-size:13px;
		margin:15px 0 !important;
	}
	
	.offer_p{
		font-size:13px;
		margin:15px 0 !important;
		word-wrap: break-word;
		/*width:305px;*/
	}
	
	::placeholder{
		font-size: 11.8px !important;
		line-height: 30px;
		color: #000 !important;
		font-weight: 400 !important;
		font-family: "Roboto", sans-serif !important;
	}

	.event-dropdown {
    margin: 5px 0 0 -138px !important;
	}
	
	 .dataTables_length {
		margin-left: 500px;
	 }

	 #event_table_length {
	   border:1px solid #B0B7CA;
	   height:38px;
	   border-radius:4px;
	   width:96px;
	   margin-top:5px;
	   margin-left:50%;
	 }

	 #event_table_length .select-wrapper input.select-dropdown {
	   margin-top:-3px !important;
	   margin-left:10px !important;
	 }

	 #event_table_length .select-wrapper span.caret {
	   margin: 17px 7px 0 0;
	 }

	 #event_table_length .dropdown-content {
	   min-width: 95px;
	 }
	 
	 .action-tab{
		 vertical-align: top !important;
	 }
 
	.eve-email:hover{
		color:#7864e9 !important;
	}
	
	a.filter-search.btn-search.btn.active {
		width: 18% !important;
		margin-right: 7px !important;
		margin-left: 1%;
	}
	
	.btn-search.active .search-hide-show {
		width: calc(82% - 29px) !important;
		margin-left: 24px !important;
	}
	
	.location{ height:30px; }
	#submit{ height: 38px; background-color: #644dd5; border: 1px solid #ccc; border-radius: 5px; color:#fff; font-size:13px;}
	
	.challangeImg{ padding: 4% 4% !important; border-bottom: 1px solid #ccc; margin: 0px 0 0 -13px !important; width: 105.5% !important; text-align:center; }
	.challangeImg img{ width:85%; height:235px; }
	#breadcrumbs-wrapper{
		padding:18px 0 10px 0;
	}
	.offers_list .svg-inline--fa{ margin:4px 5px -3px 1px; }
</style>
    <div id="main" style="padding-left:0px !important;">
      <div class="wrapper">
        <section id="content" class="bg-cp customer-search">
          <div id="breadcrumbs-wrapper">
			<div class="container">
				<div class="row"><div id="plan_msg"></div></div>
				<div class="row">
                <div class="col s10 m6 l6">
					<h5 class="breadcrumbs-title">News<small class="grey-text">(Total <?//=count($challanges)?> )</small></h5>
					<ol class="breadcrumbs">
						<li><a>COMMUNITY</a></li>
						<li class="active">News</li>
					</ol>
                </div>
                <div class="col s10 m6 l6"></div>
				</div>
            </div>
          </div>

          <div id="bulk-action-wrapper">
            <div class="container">
              <div class="row">
              	<form name="search_form" id="search_form" method="post">
					<?php $csrf = array(
					'name' => $this->security->get_csrf_token_name(),
					'hash' => $this->security->get_csrf_hash()
				);
				?>
				<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
				<div class="col l12 s12 m12" style="margin-top:-20px;">
					<a href="javascript:void(0);" onclick="formsubmit();" class="addmorelink right" id="reset_event" title="Reset all">Reset</a>
                </div>
				<div class="col l12 s12 m12" style="width: 100%;">
					<div class="action-btn-wapper right">
                    <div class="bulk location-drop" style="margin: 0 0 0 -3%; width: 105%;">
                    	<a class="filter-search btn-search btn">
							<input type="text" name="search" id="search" class="search-hide-show" value="<?php if(isset($search['search']) && $search['search']!=""){echo $search['search']; } ?>" style="display:none" />
							<i class="material-icons ser search-btn-field-show">search</i>
						</a>
                        <select class="select-stat js-example-basic-single" name="nature" id="nature">
							<option value="">NATURE OF BUSINESS</option>
							<?php if(isset($search['nature']) && $search['nature']!=""){ ?>
							<option value="<?php $search['nature'];?>" selected><?php echo $search['nature'];?></option>
							<?php } ?>
							<option value="">ALL</option>
							<option value="Accounting & Taxation">ACCOUNTING & TAXATION</option>
							<option value="Advertising">ADVERTISING</option>
							<option value="Animation Studio">ANIMATION STUDIO</option>
							<option value="Architecture">ARCHITECTURE</option>
							<option value="Arts & Crafts">ARTS & CRAFTS</option>
							<option value="Audit & Tax">AUDIT & TAX</option>
							<option value="Brand Consulting">BRAND CONSULTING</option>
							<option value="Celebrity Management">CELEBRITY MANAGEMENT</option>
							<option value="Consultant">CONSULTANT</option>
							<option value="Content Studio">CONTENT STUDIO</option>
							<option value="Cyber Security">CYBER SECURITY</option>
							<option value="Data Analytics">DATA ANALYTICS</option>
							<option value="Digital Influencer">DIGITAL INFLUENCER</option>
							<option value="Digital & Social Media">DIGITAL & SOCIAL MEDIA</option>
							<option value="Direct Marketing">DIRECT MARKETING</option>
							<option value="Entertainment">ENTERTAINMENT</option>
							<option value="Event Planning">EVENT PLANNING</option>
							<option value="Florist">FLORIST</option>
							<option value="Foreign Exchange">FOREIGN EXCHANGE</option>
							<option value="Financial and Banking">FINANCIAL & BANKING</option>
							<option value="Gaming Studio">GAMING STUDIO</option>
							<option value="DESIGN & UI/UX">DESIGN & UI/UX</option>
							<option value="Hardware Servicing">HARDWARE SERVICING</option>
							<option value="Industry Bodies">INDUSTRY BODIES</option>
							<option value="Insurance">INSURANCE</option>
							<option value="Interior Designing ">INTERIOR DESIGNING</option>
							<option value="Legal Firm">LEGAL FIRM</option>
							<option value="Media Planning & Buying">MEDIA PLANNING & BUYING</option>
							<option value="Mobile Services">MOBILE SERVICES</option>
							<option value="Music">MUSIC</option>
							<option value="Non-Profit">NON-PROFIT</option>
							<option value="Outdoor / Hoarding">OUTDOOR / HOARDING</option>
							<option value="Photography">PHOTOGRAPHY</option>
							<option value="Printing">PRINTING</option>
							<option value="Production Studio">PRODUCTION STUDIO</option>
							<option value="PR / Image Management">PR / IMAGE MANAGEMENT</option>
							<option value="Publishing">PUBLISHING</option>
							<option value="Real Estate">REAL ESTATE</option>
							<option value="Recording Studio">RECORDING STUDIO</option>
							<option value="Research">RESEARCH</option>
							<option value="Sales Promotion">SALES PROMOTION</option>
							<option value="Staffing & Recruitment">STAFFING & RECRUITMENT</option>
							<option value="Stock & Shares">STOCK & SHARES</option>
							<option value="Technology (AI, AR, VR)">TECHNOLOGY (AI, AR, VR)</option>
							<option value="Tours & Travel">TOURS & TRAVELS</option>
							<option value="Training & Coaching">TRAINING & COACHING</option>
							<option value="Translation & Voice Over">TRANSLATION & VOICE OVER</option>
							<option value="Therapists">THERAPISTS</option>
							<option value="Visual Effects / VFX">VISUAL EFFECTS / VFX</option>
							<option value="Web Development">WEB DEVELOPMENT</option>
						  </select>
						<select class="js-example-basic-single" name="location" id="location">
							<option value="">LOCATION</option>
							<?php if(isset($search['location']) && $search['location']!=""){ ?>
							<option value="<?php $search['location'];?>" selected><?php echo $search['location'];?></option>
							<?php } ?>
							<option value="">ALL</option>
							<?php if($location != '') {
                      			foreach($location as $loc)  { ?>
                        			<option value="<?php echo $loc->location; ?>"><?php echo strtoupper($loc->location); ?></option>
                        		<?php  } }
                    		?>
						</select>
						<input type="text" placeholder="START DATE" class="btn-date eventdatepicker icon-calendar-green date-cng btn-stated out-line" id="c_start_date" name="c_start_date" value="<?php if(isset($search['c_start_date']) && $search['c_start_date']!=""){ echo $search['c_start_date']; } ?>" readonly="readonly">
						<input type="text" placeholder="END DATE" class="btn-date eventdatepicker icon-calendar-red date-cng btn-stated out-line" id="c_end_date" name="c_end_date" readonly="readonly" value="<?php if(isset($search['c_end_date']) && $search['c_end_date']!=""){ echo $search['c_end_date']; } ?>">
						<input type="submit" id="submit" name="submit" value="APPLY" style="width:8.5%;">
					</div>
					</div>
                </div>
				</form>
              </div>
            </div>
          </div>

		  <div class="container">
			<div class="row">
				<!-- Start -->
				<div class="col s12 m12 l12" style="padding-top:1%;">
				<div class="offers_list">
					<div class="row">
						<?php //if (count($challanges) > 0){ foreach($challanges as $ev) {?>
						<!-- Add data here -->
						<div class="col s12 m12 l4">
						<div class="offer-1 box-wrapper bg-white shadow" style="margin: 0 0 10px 0px;">
							<div class="col s12 m12 l12 challangeImg">
								<img src="" alt="News">
							</div>	
							<div class="col s12 m12 l12 offer-info" style="margin: -4px 0 0 2.5%; border-top-left-radius: 5px; border-bottom-left-radius: 5px; padding: 2px 4.7%; width:95%;">
								<h5 style="height:35px; font-size:20px; color:#644dd5 !important;"><strong><?//= $ev['challange_name']?></strong></h5>
							</div>	
							<div class="col s12 m12 l12">
								<p class="event-desc" style="padding-top:15px; text-align: justify;"><?//= $ev['challange_desc']?></p>
							</div>
							<div class="col s12 m12 l12 offer-info">
								<div class="col s12 m12 l12" style="padding: 2px 0px;">
									<div class="col s6 m6 l6">
									<p class="offer_p"><i class="icon-font fa fa-calendar" aria-hidden="true"></i></p>
									<p class="offer_p location"><i class="icon-font fas fa-map-marker-alt"></i> <?//= strtoupper($ev['location'])?></p>
									</div>
									<div class="col s6 m6 l6">
										<p class="offer_p" style="float:right; margin:30px -8px 0 0 !important;"><a class="btn btn-theme btn-large set_remin" href="<?//= $ev['url']?>" target="_blank">&nbsp; CHECK WEBSITE</a></p>
									</div>
								</div>	
							</div>
						</div>
						</div>
						
						<!-- End -->
						
					<?php //}} else{ ?>
						<!--<div class="col s12 m12 l4"></div>
						<div class="col s12 m12 l4">
						<div class="text-center" style="margin: 0 0 10px 0px;">
						<div style="background: #f05734; width: 350px; padding: 0px 5px 0px 5px; border-radius: 50px; color: white; height: 50px; line-height: 50px; font-size: 18px; margin-left:8%;"><p>There are no News right now</p></div>
						</div>
						</div>
						<div class="col s12 m12 l4"></div>-->
					<?php //} ?>

					</div>
				</div>
				</div>
				<!-- END -->
				<div class="row"><div class="col s12 m12 l12"><p></p></div></div>
				<div class="row"><div class="col s12 m12 l12"><p></p></div></div>
				<div class="row"><div class="col s12 m12 l12"><p></p></div></div>
            </div>
        </div>


		</section>
	</div>
</div>

<div id="send_email_event_modal" class="modal modal-md ps-active-y" style="margin-top:-45px !important; max-width:510px !important;">
	<?php $this->load->view('my-community/email-popup-event'); ?>
</div>

<div id="remove_event" class="modal" style="width:40% !important;">
	<img class="geen" src="<?php echo base_url();?>asset/images/green.png" alt="green">
	<div class="modal-content">
		<div class="modal-header" style="text-align:right; margin-left:0%;">
			<h4> Delete Event</h4>
			<input type="hidden" id="remove_event_id" name="remove_event_id" value="" />
			<a class="modal-close close-pop"><img src="<?php echo base_url();?>asset/images/popupdelete.png" alt="delete" ></a>
		</div>
	</div>
	<div class="modal-body" style="margin-left: 0px !important; width: 100%;">
		<p style="font-size:18px !important; color:#595959 !important; text-align:center;">Are you sure you want to delete this event?</p>
	</div>
	<div class="modal-content">
	<div class="modal-footer">
		<div class="row">
			<div class="col l4 s12 m12"></div>
			<div class="col l8 s12 m12 cancel-deactiv">
				<a class=" modal-close btn-flat theme-flat-btn theme-btn theme-btn-large model-cancel " type="button">CANCEL</a>
				<button id="deactive_event" class="btn-flat theme-primary-btn theme-btn theme-btn-large  modal-trigger dea modal-close deactive_this_service">DELETE</button>
			</div>
		</div>
	</div>
	</div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
		$('.js-example-basic-single').select2();
	});
</script>
<script type="text/javascript">
    $(document).ready(function() {


	
		
    	$(".offer-time").hide();
		$(".ssds").click(function(){
			if($(this).hasClass('active')){
				$(".offer-time").hide();
				$(".showmore").removeClass('active');
				$('.less').hide();
				$('.more').show();
			}else{
				$(".offer-time").show();
				$(this).addClass('active');
				$('.less').show();
				$('.more').hide();
			}
		});

       	$('.deactive_event').on('click',function(){

				var event_id = $(this).data('cd_id');

				 $('#remove_event').modal('open');

				 $('#remove_event #remove_event_id').val(event_id);

			});

			$('#deactive_event').off().on('click',function(){

				var event_id = $('#remove_event_id').val();

				$.ajax({

					url:base_url+'Community/delete_event',

					type:"POST",

					data:{'csrf_test_name':csrf_hash,"event_id":event_id,},

					success:function(res){
							if(res == true)
							{
								window.location.href=base_url+'Community/events';
								Materialize.toast('Your Event has been successfully deleted', 2000,'green rounded');
							}
							else
							{
								//window.location.href=base_url+'Community/deals';
								Materialize.toast('Error while processing!', 2000,'red rounded');
							}
						},
					});
			});

	});

	function email_event($id){

		var event_id = $id;
		$('#send_email_event_modal').modal('open');
		$('#send_email_event_modal #event_id').val(event_id);

	}

	function formsubmit() {
		$(window).off('beforeunload');
		$("#search_form").submit();
	}

	function showmorelist(id){
		if($("#showmorelist"+id).hasClass('active')){
				$("#offer-time"+id).hide();
				$("#showmorelist"+id).removeClass('active');
				$('#less'+id).show();
				$('#showmorelist'+id).hide();
				$('#morelist'+id).show();
				$('#morelist1'+id).show();
			}else{
				$("#offer-time"+id).show();
				$("#showmorelist"+id).addClass('active');
				$('#showmorelist'+id).show();
				$('#less'+id).hide();
				$('#morelist'+id).hide();
				$('#morelist1'+id).hide();
			}
	}

			window.onbeforeunload = function() {
  };



  window.onbeforeunload = null;

    if ( window.history.replaceState ) {
        window.history.replaceState( null, null, window.location.href );
    }
</script>

<?php $this->load->view('template/footer'); ?>
