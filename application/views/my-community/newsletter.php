<?php $this->load->view('my-community/Cohorts-header'); ?>
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Include DataTables CSS & JS -->
<link rel="stylesheet" href="https://cdn.datatables.net/1.13.6/css/jquery.dataTables.min.css">
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script src="https://cdn.datatables.net/1.13.6/js/jquery.dataTables.min.js"></script>

    <style type="text/css">
		#newsTable_length_wrapper{ margin:0 1%; }
		#newsTable_length label {
			display: flex;
			font-size: 0px;
		}

		#newsTable_length .select-wrapper span.caret {
    		color: #595959;
    		top: -2px;
		}

		/* Style the dropdown to fit correctly */
		#newsTable_length select {
			padding: 5px 10px;
			border: 1px solid #ccc;
			border-radius: 5px;
			background: #fff;
			font-size: 14px;
			cursor: pointer;
			min-width: 100px; /* Ensure the dropdown has a proper width */
		}


	#newsTable_length .dropdown-content {
		min-width: 96px;
		margin-top:40% !important;
	}
	
	#newsTable_length{
		border:1px solid #B0B7CA !important;
		height:38px;
		border-radius:4px;
		width:95px;
		margin-top:5px;
		margin-left:52%;
	}
	
	#newsTable_length .select-wrapper input.select-dropdown {
		margin-top:-3px !important;
		margin-left:10px !important;
	}
	
	#newsTable_length .select-wrapper span.caret {
		margin: 17px 7px 0 0;
	}	
	/*----------START SEARCH DROPDOWN CSS--------*/
	@media only screen and (max-width: 600px) {
		.newsletter-container .tab{ width:65% !important; }
		.tabnews .mediaPost { width:90% !important; }
		.tabnews{ margin-left:20px !important; }
	}	
	.select2-container--default .select2-selection--single {
		border:none;
	}
	
	input[type="search"]:not(.browser-default) {
	  height: 30px;
	  font-size: 12px;
	  margin: 0;
	  border-radius: 5px;

	}
	.select2-container--default .select2-selection--single .select2-selection__rendered {
		font-size: 14px !important;
		line-height: 30px;
		color: #000 !important;
		font-weight: 400 !important;
		padding-left:0px !important;
	}
	.select2-container--default .select2-selection--single .select2-selection__arrow {
		height: 40px;
		top:-1px;
	}
	.select2-search--dropdown {
	  padding: 0;
	}
	input[type="search"]:not(.browser-default):focus:not([readonly]) {
	  border-bottom: 1px solid #bbb;
	  box-shadow: none;
	}
	.select2-container--default .select2-selection--single:focus {
		outline: none;
	}
	.select2-container--default .select2-results__option--highlighted[aria-selected] {
		background: #fffaef;
	  color: #666;
	}
	.select2-container--default .select2-results > .select2-results__options {
	  font-size: 12px;
	  border-radius: 5px;
	  box-shadow: 0px 2px 6px #B0B7CA;
	}
	.select2-dropdown {
	  border: none;
	  border-radius: 5px;
	}
	.select2-container .select2-selection--single {
		height: 40px;
		padding: 6px;
		border: none;
		background: #f8f9fd;
		border-radius: 5px;
	}
	.select2-results__option[aria-selected] {
	  border-bottom: 1px solid #f2f7f9;
	  padding: 14px 16px;
	}
	.select2-container--default .select2-search--dropdown .select2-search__field {
		border: 1px solid #d0d0d0;
		padding: 0 0 0 0px !important;
		width: 99.5%;
		max-width: 100%;
	}
	.select2-search__field::placeholder {
	  content: "Search Here";
	}
	.select2-container--open .select2-dropdown--below {
	  margin-top: 0;
	}
	.select2-container{ width:65% !important; }
	/*----------END SEARCH DROPDOWN CSS--------*/
	/* Modal CSS  Start */
	#show_newsletter .modal{
		max-width: 51.3%;
		height: 450px;
		left: 2%;
		top: 20% !important;
		background-color: #fff;
		border-radius: 10px;
		padding-right:13px !important;
	}
	#show_newsletter .modalheader{ 
		text-align:end !important;
		margin:1% 0 !important;
		padding:5px 0 0px 0 !important;
	}
	#show_newsletter .modal-close{
		cursor:pointer;
	}
	/* Modal CSS end */
	
	.icon-font{
		color:#ff7d9a !important;
		font-size:15px;
		width:10% !important;
	}
	p.event-desc{ height:95px; margin:-6px 0; }
	
	
	
	@media all and (-ms-high-contrast: none), (-ms-high-contrast: active) { 
		a{
			color:#000;
		}
		.btn-date-bill {
			line-height: 34px !important;
		}
		.select2-container{
			margin-top:-4px;
		}
	}
	
	.newsletter-container .tab {
		padding: 5px 10px;
		border: 1px solid #ccc !important;
		border-radius: 33px !important;
	}
	.tab {
		overflow: hidden;
		background-color: transparent;
		margin-top: 8%;
	}
	.text-center {
		text-align: center!important;
	}
		
	.newsletter-container .tab button {
		/*width: auto;*/
	}
	
	.newsletter-container .tab button {
		width: 85px;
	}
	.tab button {
		background-color: inherit;
		float: left;
		border: none;
		outline: 0;
		cursor: pointer;
		padding: 8px 16px;
		transition: .3s;
		font-size: 17px;
		color: #999fae;
	}
	
	.pagination .current{
		background-color: #ff7c9b;
		border-radius: 5px;
		color:#fff;
	}
	.pagination .prev, .pagination .next{
		font-size:25px;
	}
	.pagination .page:hover{
		cursor:pointer;
	}
	.pagination{ 
		margin: 3% 0 3% 0;
	}
	.pagination a{
		padding: 10px 15px;
		color:#313131;
	}	
	.mediaPost img {
		border-radius: 10px;
		object-fit: cover;
		width: 68%;
	}
	@media only screen and (max-width: 600px) {
		.mediaPost img {
			border-radius: 10px;
			object-fit: cover;
			width: 100%;
		}	
		.media_container .tab {
			margin-left:35px;
		}
	}
	.marleft4{
		margin-left:5%;
	}
	.media_container .tabcontent {
		padding: 0px 1%;
	}
	.tabnews{ margin:0 -1%; }
	.letterDiv{ height:175px; width:160px; margin:20px 25px 9% 20px; float:left; }
	.letterDiv img{ border:1px solid #ccc; object-fit:cover; }
	.newsletter_title{ color: #000; text-decoration: none; text-align: center; margin: 0 -15px 0 -15px; font-size: 13px; }
	.newsletter-container .tab { padding: 5px 10px; border: none; margin:auto; width:18%; display:table; }
	.newsletter-container .tab button { width: auto; border-right:1px solid #ccc; font-size: 14.3px; padding: 10.1px 16px; color: #000; }
	.tab button.tablinks1.active { padding: 10.1px 16px; border-radius: 1px; color: #7965ea; background-color:transparent; font-weight:600; font-size:14.3px;}
	.tab button.tablinks1:hover { padding: 10.1px 16px; color: #7965ea; border-radius: 1px; background-color:transparent; font-weight:600; font-size:14.3px;}
	.letterul{ list-style-type: none; }
	.mar40{ margin-top:40px; }
	.borderleftnone{ border-left:none !important; }
	
	#show_newsletter #table-price-desk table th, table td { text-align: inherit; padding: 0em 0em; border: none !important; }
	#show_newsletter  p{ text-align:center; }
	.firstTable{ margin:0px !important; width:102% !important; }
	.secondImg{ margin:-9.5% 0 17px 0 !important; }
	.lastImg{ margin:0 0 -6px 0 !important; }
	.mediaPost{ margin:3% 2% 3% 0; width:23% !important; }
	.mediaPost img { border-radius: 10px; object-fit: cover; width: 100%; height: 228px; margin: 3.2% 0; }
	.mediaPost .postName { font-size: 14px; }
	.color_purple { color: #7965ea!important; }
	.mediaPost .postInfo { font-size: 14px; text-align: justify; display: -webkit-box; -webkit-line-clamp: 3; -webkit-box-orient: vertical;
    overflow: hidden; text-overflow: ellipsis; height: 72px; padding-top: 5px; }
	.mediaPost .postRead { font-size: 12px; float: left; }
</style>
    <div id="main" style="padding-left:0px !important;">
      <div class="wrapper">
        <?php //$this->load->view('template/sidebar'); ?>
        <section id="content" class="bg-cp customer-search">
          <div id="breadcrumbs-wrapper">
            <div class="container">
               <div class="row">
               <div id="plan_msg"></div>
             </div>
              <div class="row">
                <div class="col s10 m6 l6">
                  <h5 class="breadcrumbs-title">News<small class="grey-text">(Total <?php echo sizeof($blogscount); ?> )</small></h5>

                  <ol class="breadcrumbs">
                    <li><a>My Community</a>
                    </li>
                    <li class="active">News</li>
                  </ol>
                </div>
                <div class="col s10 m6 l6">
					
                </div>
              </div>
            </div>
		</div>
		  
		<div class="container">
			<div class="row">
				<!-- Start -->
				<div class="col s12 m12 l12 media_container" style="padding-top:1%;">
					<?php  $year=date('Y'); ?>
					<form action="<?php echo base_url(); ?>news" method="post" id="searchpostform" class="searchpostform" novalidate="">
					<?php $csrf = array(
					'name' => $this->security->get_csrf_token_name(),
					'hash' => $this->security->get_csrf_hash()
					);
					?>
					<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
					<div class="row">
						<div class="col-md-12 newsletter-container mar25">
							<div class="tab text-center">
								<?php $month= date('m'); ?>
								<button class="tablinks1 newsallactive borderleftnone active">ALL</button>
								<select class="js-example-basic-single" name="location" id="location" onchange='getElementById("searchpostform").submit();'>

									<option value="newsall">ARCHIVE</option>

									<?php /* for($i=$month;$i > $month-1;$i--){ ?>
										<?php $dateObj   = DateTime::createFromFormat('!m', $i);
										$monthName = $dateObj->format('F'); ?>
										<option value="<?=$monthName?>"><?=$monthName?></option>
									<?php }*/ ?>
									<?php foreach($newsletters as $news){ 
                       if($news['newsletter_month']==1){
                       	$month= "January";
                       }
                       if($news['newsletter_month']==2){
                       	$month= "February";
                       }
                       if($news['newsletter_month']==3){
                       	$month= "March";
                       }
                       if($news['newsletter_month']==4){
                       	$month= "April";
                       }
                       if($news['newsletter_month']==5){
                       	$month= "May";
                       }
                       if($news['newsletter_month']==6){
                       	$month= "June";
                       }
                       if($news['newsletter_month']==7){
                       	$month= "July";
                       }
                       if($news['newsletter_month']==8){
                       	$month= "August";
                       }
                       if($news['newsletter_month']==9){
                       	$month= "September";
                       }
                       if($news['newsletter_month']==10){
                       	$month= "October";
                       }
                       if($news['newsletter_month']==11){
                       	$month= "November";
                       }
                       if($news['newsletter_month']==12){
                       	$month= "December";
                       }

									  ?>	
									<!-- <option value="<?//=$news['newsletter_month']." ".$news['newsletter_year']?>"><?//=strtoupper($month)." ".$news['newsletter_year']?></option>
									<?php //} ?> -->

									<?php if($news['newsletter_year'] !='0'){ ?>	
									<option value="<?=$news['newsletter_month']." ".$news['newsletter_year']?>" <?php if($select_month !="" && $select_year !=""){ if($news['newsletter_month'] == $select_month && $news['newsletter_year'] == $select_year){ ?>selected<?php } } ?>><?=strtoupper($month)." ".$news['newsletter_year']?></option>
									<?php }} ?>
								</select>
								<!--<?php //for($i=$year;$i > $year-1;$i--){ ?>
									<button class="tablinks1" onclick="openNewsletters(event, '<?//=$i?>')"><?//=$i?></button>
								<?php //} ?>-->
							</div>						
						</div>
									</form>	
						<div id="newsall" class="row tabnews newsList">
							<?php foreach($blogs as $blg){ 
									if($blg->category=="news"){
								?>	
							<div class="col s12 m12 l3 mediaPost text-center news-card">
								<div <?php echo $blg->id; ?>><!-- <?php echo base_url(); ?>blog/<?=$blg->url?> -->
									<a href="<?php echo base_url(); ?>blog/<?=$blg->url?>" target="_blank"><img width="350" height="300" src="<?=$blg->image?>/<?=$blg->filename?>" alt="" class="" <?php if($blg->id == 14 ||$blg->id == 17 ){ echo 'style="width:75%;"'; }else{  } ?> <?php if($blg->id == 18 || $blg->id == 19 || $blg->id == 20 || $blg->id == 21 || $blg->id == 23 || $blg->id == 24 || $blg->id == 25 || $blg->id == 29 ){ echo 'style="width: 80%; height: 240px; margin: 6.5% 0; object-fit: fill;"'; }else{  } ?> <?php if($blg->id == 26 ){ echo 'style="width: 85%; height: 265px; margin: 4% 0; object-fit: cover;"'; }else{  } ?> <?php if($blg->id == 27 || $blg->id == 30 || $blg->id == 31 || $blg->id == 32 || $blg->id == 33 || $blg->id == 34 || $blg->id == 35 ){ echo 'style="width: 100%; height: 228px; margin: 3.2% 0; object-fit: cover;"'; }else{  } ?> ></a>
								</div>
								<p style="height:60px;"><a class="color_purple postName" href="<?php echo base_url(); ?>blog/<?=$blg->url?>" target="_blank"><?=$blg->post_name?></a></p>
								<p class="postInfo"><?=$blg->excerpt?></p>
								<a class="postRead color_purple" href="<?php echo base_url(); ?>blog/<?=$blg->url?>" target="_blank">Read More...</a>
							</div>
							 <?php } } ?>
							<!--<ul class="letterul">
								<?php foreach($newsletters as $news){   ?>
									<li class="letterDiv">
										<a class="openewsletter modal-trigger" data-id="<?=$news->id ?>" data-file="<?=$news->file?>"><img width="155" height="155" src="<?php echo $news->image."/".$news->filename; ?>" alt="">
										<p class="newsletter_title"><?= $news->newsletter_name?></p></a>
									</li>
								<?php } ?>
							</ul>-->
						</div>	
						<?php //for($i=$year;$i > $year-1;$i--){ ?>
						<?php foreach($newsletters as $news){   ?>
						<div id="<?=$news['newsletter_month']." ".$news['newsletter_year']?>" class="row tabnews">
							<!--<ul class="letterul">								
								<li class="letterDiv">
									<a class="openewsletter modal-trigger" data-id="<?=$news->id ?>" data-file="<?=$news->file?>"><img width="155" height="155" src="<?php echo $news->image."/".$news->filename; ?>" alt="">
									<p class="newsletter_title"><?= $news->newsletter_name?></p></a>
								</li>
							</ul>-->
							<?php foreach($blogs as $blg){ 
								if($blg->category=="news"){
							?>	
							<div class="col s12 m12 l3 mediaPost text-center">
								<div><!-- <?php echo base_url(); ?>blog/<?=$blg->url?> -->
									<a href="https://xebra.in/blog/<?=$blg->url?>" target="_blank"><img width="350" height="300" src="<?=$blg->image?>/<?=$blg->filename?>" alt="" class="" <?php if($blg->id == 14 ||$blg->id == 17 ){ echo 'style="width:75%;"'; }else{  } ?> <?php if($blg->id == 18 || $blg->id == 19 || $blg->id == 20 || $blg->id == 21 || $blg->id == 23 || $blg->id == 24 || $blg->id == 25 || $blg->id == 29 ){ echo 'style="width: 80%; height: 240px; margin: 6.5% 0; object-fit: fill;"'; }else{  } ?> <?php if($blg->id == 26 ){ echo 'style="width: 85%; height: 265px; margin: 4% 0; object-fit: cover;"'; }else{  } ?> <?php if($blg->id == 27 || $blg->id == 30 || $blg->id == 31 || $blg->id == 32 || $blg->id == 33 || $blg->id == 34 || $blg->id == 35 ){ echo 'style="width: 100%; height: 228px; margin: 3.2% 0; object-fit: cover;"'; }else{  } ?> ></a>
								</div>
								<p style="height:60px;"><a class="color_purple postName" href="https://xebra.in/blog/<?=$blg->url?>" target="_blank"><?=$blg->post_name?></a></p>
								<p class="postInfo"><?=$blg->excerpt?></p>
								<a class="postRead color_purple" href="https://xebra.in/blog/<?=$blg->url?>" target="_blank">Read More...</a>
							</div>
							<?php } } ?>
						</div>
						<?php } ?>
						<?php //} ?>
					</div>
				</div>
				<!-- END -->
				<div class="row"><div class="col s12 m12 l12"><p></p></div></div>
				<div class="row"><div class="col s12 m12 l12"><p></p></div></div>
				<div class="row"><div class="col s12 m12 l12"><p></p></div></div>
            </div>
        </div>


		</section>
	</div>
</div>

<div id="show_newsletter" class="modal modal-md" aria-hidden="true">
		<div class="modalheader">
			<a class="modal-close close-pop"><img src="<?php echo base_url();?>asset/images/popupdelete1.png" alt="delete"></a>
		</div>
		<div class="modalbody">
			<div class="row loadnews">
				<?php //$this->load->view('newsletters/newletter1'); ?>
			</div> 	
		</div>
	</div>

<script>
	$(document).ready(function () {
			let newsListing = $('.newsList .news-card'); // Get all divs
			var pageLength =  localStorage.getItem('newsTable_length') || 10;
			let table = $('<table id="newsTable"><thead><tr><th>Hidden</th></tr></thead><tbody></tbody></table>'); 
			$('body').append(table.hide()); // Append hidden table for pagination

			// Insert each div as a row in the fake table
			newsListing.each(function () {
				$('#newsTable tbody').append('<tr><td></td></tr>');
			});

			// Initialize DataTables
			let dataTable = $('#newsTable').DataTable({
				"bPaginate": true,
			    "bLengthChange": true,
    			"sDom": 'Rfrtlip',
    			"bFilter": true,
    			"bInfo": false,
    			"searching":false,
				"iDisplayLength": parseInt(pageLength),
				"stateSave": true,
    			//"iDisplayLength": 10,
				lengthMenu: [
					[ 10, 20, 30, 50, -1 ],
					[ 'SHOW 10', 'SHOW 20', 'SHOW 30', 'SHOW 50', 'SHOW ALL' ]
				],
			});

			// Hide all divs initially and only show the first page
			function showPage() {
				newsListing.hide();
				let pageIndexes = dataTable.rows({ page: 'current' }).indexes();
				pageIndexes.each(function (index) {
					newsListing.eq(index).show();
				});
			}

			showPage(); // Show initial page

			// Update visible divs on page change
			$('#newsTable').on('page.dt length.dt', function () {
				showPage();
			});
		});
	jQuery(document).ready(function() {
		$('.js-example-basic-single').select2();
		$(".close-pop").click(function () {
			$("#show_newsletter").modal("hide");
		});

			$('#location').change(function(){
			var id=$(this).val();
			
			openNewsletters(event, id);
			
		});
		
		$('.openewsletter').click(function(){
			var id=$(this).data('id');
			var file=$(this).data('file');
			console.log(file);
			$('.loadnews').load(file);
			//$('#show_newsletter').modal('show');
			$('#show_newsletter').modal('open');
		});
		
		$(function () {
			$("a[class='modal-trigger']").click(function () {
				$("#show_newsletter").modal("show");
				return false;
			});
		});
		
        openNewsletters(event, 'newsall');
		
		$('.nav-link.loginxb').css('color','rgba(0,0,0,.5) !important');
			
		(function($) {
		var pagify = {
			items: {},
			container: null,
			totalPages: 1,
			perPage: 3,
			currentPage: 0,
			createNavigation: function() {
				this.totalPages = Math.ceil(this.items.length / this.perPage);
				$('.pagination', this.container.parent()).remove();
				var pagination = $('<div class="pagination"></div>').append('<a class="nav prev disabled" data-next="false"><</a>');
				for (var i = 0; i < this.totalPages; i++) {
					var pageElClass = "page";
					if (!i)
						pageElClass = "page current";
					var pageEl = '<a class="' + pageElClass + '" data-page="' + (
						i + 1) + '">' + (
						i + 1) + "</a>";
					pagination.append(pageEl);
				}
				pagination.append('<a class="nav next" data-next="true">></a>');
				this.container.after(pagination);
				var that = this;
				$("body").off("click", ".nav");
					this.navigator = $("body").on("click", ".nav", function() {
					var el = $(this);
					that.navigate(el.data("next"));
				});
				$("body").off("click", ".page");
				this.pageNavigator = $("body").on("click", ".page", function() {
					var el = $(this);
					that.goToPage(el.data("page"));
				});
			},
			navigate: function(next) {
				// default perPage to 5
				if (isNaN(next) || next === undefined) {
					next = true;
				}
				$(".pagination .nav").removeClass("disabled");
				if (next) {
					this.currentPage++;
					if (this.currentPage > (this.totalPages - 1))
						this.currentPage = (this.totalPages - 1);
					if (this.currentPage == (this.totalPages - 1))
						$(".pagination .nav.next").addClass("disabled");
					}
				else {
					this.currentPage--;
					if (this.currentPage < 0)
						this.currentPage = 0;
					if (this.currentPage == 0)
					$(".pagination .nav.prev").addClass("disabled");
				}
				this.showItems();
			},
			updateNavigation: function() {
				var pages = $(".pagination .page");
				pages.removeClass("current");
				$('.pagination .page[data-page="' + (
				this.currentPage + 1) + '"]').addClass("current");
			},
			goToPage: function(page) {
				this.currentPage = page - 1;
				$(".pagination .nav").removeClass("disabled");
				if (this.currentPage == (this.totalPages - 1))
				$(".pagination .nav.next").addClass("disabled");
				if (this.currentPage == 0)
				$(".pagination .nav.prev").addClass("disabled");
				this.showItems();
			},
			showItems: function() {
				this.items.hide();
				var base = this.perPage * this.currentPage;
				this.items.slice(base, base + this.perPage).show();
				this.updateNavigation();
			},
			init: function(container, items, perPage) {
				this.container = container;
				this.currentPage = 0;
				this.totalPages = 1;
				this.perPage = perPage;
				this.items = items;
				this.createNavigation();
				this.showItems();
			}
		};
		// stuff it all into a jQuery method!
		$.fn.pagify = function(perPage, itemSelector) {
			var el = $(this);
			var items = $(itemSelector, el);
			// default perPage to 5
			if (isNaN(perPage) || perPage === undefined) {
				perPage = 3;
			}
			// don't fire if fewer items than perPage
			if (items.length <= perPage) {
				return true;
			}
			pagify.init(el, items, perPage);
		};
		})(jQuery);	
	
		$(".allPost").pagify(6, ".mediaPost");
		$(".allMedia").pagify(6, ".mediaPost");
		$(".allBlog").pagify(6, ".mediaPost");
	});
		
		//For Newsletters
		function openNewsletters(evt, moduleName) {
			var i, tabcontent, tablinks1;
			tabcontent = document.getElementsByClassName("tabnews");
			for (i = 0; i < tabcontent.length; i++) {
				tabcontent[i].style.display = "none";
			}
			tablinks1 = document.getElementsByClassName("tablinks1");
			for (i = 0; i < tablinks1.length; i++) {
				tablinks1[i].className = tablinks1[i].className.replace(" active", "");
			}
			document.getElementById(moduleName).style.display = "block";
			evt.currentTarget.className += " active";
		}
		
		setTimeout(function(){ 
			//$('.newsallactive').click();
		}, 1000);
		$('.newsallactive').click(function(){
			$("#location").val("newsall").change();
		});
	</script>	

<?php $this->load->view('template/footer'); ?>
