<!DOCTYPE html>
<html>
	<head>
		<link rel="stylesheet" href="../css/create-profile.css">
		<style>
			@import url('https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');
			
			.container-fluid{
				border-top:1px solid #ccc;
			}
		</style>	
	</head>
	<?php include('header.php'); ?>
	<body>
		<div class="container-fluid">
			<div class="row">
			<main class="col main-div">
				<div class="row">
					<div class="col-sm-4"></div>
					<div class="col-sm-4">
						<div class="box-wrapper bg-white border-radius-6 shadow">
							<div class="box-header border-bottom">
								<h3 class="box-title"><strong>Create Profile</strong></h3>
							</div>
							
							<div class="row">
								<div class="col-sm-7 col-left border-right">
									<div class="input-field border-bottom">
										<label for="company_name" class="full-bg-label">Contact Name<span class="required_field"> *</span></label>
										<input id="reg_username" name="reg_username" class="full-bg adjust-width" type="text">
									</div>
								</div>
								<div class="col-sm-5 col-right">
									<div class="border-bottom">
									<div class="uploader-placeholder" style="background-image: url('../img/placeholder.png');">
                                    <label class="up_pic" style="font-size:12px; color:#696969; margin-top:-13px !important; margin-left:32px;">(Upload your Photo)</label>
									<input type="file" class="hide-file" id="reg_profile_image" name="reg_profile_image">
                                    <input type="hidden" id="image_info" name="image_info" value="" />
									</div>
									</div>
								</div>
							</div>
							
							<div class="row">
								<div class="col-sm-12 col-twelth col-left">
									<div class="input-field border-bottom">
										<label for="company_email" class="full-bg-label">Email ID<span class="required_field"> *</span></label>
										<input id="company_email" name="company_email" class="full-bg adjust-width" type="email">
									</div>
								</div>
							</div>
							
							<div class="row">
								<div class="col-sm-6 col-left border-right">
									<div class="input-field border-bottom">
										<label for="user_pass" class="full-bg-label">Password<span class="required_field"> *</span></label>
										<input id="user_pass" name="user_pass" class="full-bg adjust-width" type="password">
									</div>
								</div>
								<div class="col-sm-6 col-right">
									<div class="input-field border-bottom">
										<label for="user_conf_pass" class="full-bg-label">Confirm Password<span class="required_field"> *</span></label>
										<input id="user_conf_pass" name="user_conf_pass" class="full-bg adjust-width" type="password">
									</div>
								</div>
							</div>
							
							<div class="row">
								<div class="col-sm-6 col-left border-right">
									<div class="input-field border-bottom">
										<label for="user_mob" class="full-bg-label">Mobile<span class="required_field"> *</span></label>
										<input id="user_mob" name="user_mob" class="full-bg adjust-width" type="number">
									</div>
								</div>
								<div class="col-sm-6 col-right">
									<div class="input-field border-bottom">
										<label for="user_conf_pass" class="full-bg-label">Access<span class="required_field"> *</span></label>
										<select class="select">
											<option value="">Access</option>
											<option value="first">First</option>
										</select>
									</div>
								</div>
							</div>
							
							<div class="row">
								<div class="col-sm-7 col-left border-right">
									<div class="input-field border-bottom">
										<label for="company_name" class="full-bg-label">Company Name<span class="required_field"> *</span></label>
										<input id="company_name" name="company_name" class="full-bg adjust-width" type="text">
									</div>
								</div>
								<div class="col-sm-5 col-right">
									<div class="border-bottom">
									<div class="uploader-placeholder" style="background-image: url('../img/placeholder.png');">
                                    <label class="up_pic" style="font-size:12px; color:#696969; margin-top:-13px !important; margin-left:32px;">(Upload your Photo)</label>
									<input type="file" class="hide-file" id="reg_profile_image" name="reg_profile_image">
                                    <input type="hidden" id="image_info" name="image_info" value="" />
									</div>
									</div>
								</div>
							</div>
							
							<div class="row">
								<div class="col-sm-6 col-left border-right">
									<div class="input-field border-bottom">
										<label for="user_conf_pass" class="full-bg-label">Country<span class="required_field"> *</span></label>
										<select class="select">
											<option value="">Country</option>
											<option value="first">India</option>
										</select>
									</div>
								</div>
								<div class="col-sm-6 col-right">
									<div class="input-field border-bottom">
										<label for="user_conf_pass" class="full-bg-label">State<span class="required_field"> *</span></label>
										<select class="select">
											<option value="">State</option>
											<option value="first">MAHARASTRA</option>
										</select>
									</div>
								</div>
							</div>
							
							<div class="row">
								<div class="col-sm-6 col-left border-right">
									<div class="input-field border-bottom">
										<label for="user_conf_pass" class="full-bg-label">CITY<span class="required_field"> *</span></label>
										<select class="select">
											<option value="">City</option>
											<option value="first">MUMBAI</option>
										</select>
									</div>
								</div>
								<div class="col-sm-6 col-right">
									<div class="input-field border-bottom">
										<label for="company_pin" class="full-bg-label">PIN CODE<span class="required_field"> *</span></label>
										<input id="company_pin" name="company_pin" class="full-bg adjust-width" type="text">
									</div>
								</div>
							</div>
							
						</div>
						
						<!-- Company Information -->
						<div class="box-wrapper bg-white border-radius-6 shadow" style="margin-top:10%;">
							<div class="box-header border-bottom">
								<h3 class="box-title"><strong>Company Information</strong></h3>
							</div>
							
							<div class="row">
								<div class="col-sm-6 col-left border-right">
									<div class="input-field border-bottom">
										<label for="type_cmp" class="full-bg-label">Type Of Company<span class="required_field"> *</span></label>
										<select class="select" id="type_cmp">
											<option value="">Type Of Company</option>
											<option value="first">Partnership</option>
										</select>
									</div>
								</div>
								<div class="col-sm-6 col-right">
									<div class="input-field border-bottom">
										<label for="company_pin" class="full-bg-label">Date of incorporation<span class="required_field"> *</span></label>
										<input type="date" name="date" id="date-incr">
									</div>
								</div>
							</div>
							
							<div class="row">
								<div class="col-sm-6 col-left border-right">
									<div class="input-field border-bottom">
										<label for="no_emp" class="full-bg-label">NO. Of Employee<span class="required_field"> *</span></label>
										<input id="no_emp" name="no_emp" class="full-bg adjust-width" type="number">
									</div>
								</div>
								<div class="col-sm-6 col-right">
									<div class="input-field border-bottom">
										<label for="revenue" class="full-bg-label">Revenue<span class="required_field"> *</span></label>
										<select class="select" id="revenue">
											<option value="">Revenue</option>
											<option value="first">4 CR</option>
										</select>
									</div>
								</div>
							</div>
							
							<div class="row">
								<div class="col-sm-12 col-right">
									<div class="input-field emty-field-row border-field">
										<h6 class="label">Services offered <small class="badge-cover tot_service">0</small>
										<a class="right modal-trigger" type="button" href="#" style="margin-top:-8px;">
											<img width="35" height="35" src="../img/pluse.png" class="" alt="plus"/>
										</a>
										</h6>
									</div>
								</div>
							</div>
							
							<div class="row">
								<div class="col-sm-12 col-right">
									<div class="input-field emty-field-row border-field ">
										<h6 class="label">Add Branch<small class="badge-cover tot_service">0</small>
										<a class="right modal-trigger" type="button" href="#" style="margin-top:-8px;">
											<img width="35" height="35" src="../img/pluse.png" class="" alt="plus"/>
										</a>
										</h6>
									</div>
								</div>
							</div>
							
						</div>
						
						<!-- Web Presence -->
						
						<div class="box-wrapper bg-white border-radius-6 shadow" style="margin-top:10%;">
							<div class="box-header border-bottom">
								<h3 class="box-title"><strong>Web Presence</strong></h3>
							</div>
							
							<div class="row">
								<div class="col-sm-12 col-twelth">
									<div class="input-field border-bottom">
										<label for="web_url" class="full-bg-label">Add website url</label>
										<input id="web_url" name="web_url" class="full-bg adjust-width" type="text">
									</div>
								</div>
							</div>
							
							<div class="row">
								<div class="col-sm-12 col-twelth">
									<div class="input-field border-bottom">
										<label for="donate_url" class="full-bg-label">Add Donate Button Url</label>
										<input id="donate_url" name="donate_url" class="full-bg adjust-width" type="text">
									</div>
								</div>
							</div>
							
							<div class="row">
								<div class="col-sm-12 col-twelth">
									<div class="input-field border-bottom">
										<label for="add_url" class="full-bg-label">Additional Link</label>
										<input id="add_url" name="add_url" class="full-bg adjust-width" type="text">
									</div>
								</div>
							</div>

						</div>
						
						<!-- Media -->
						
						<div class="box-wrapper bg-white border-radius-6 shadow" style="margin-top:10%;">
							<div class="box-header border-bottom">
								<h3 class="box-title"><strong>Social Media</strong></h3>
							</div>
							
							<div class="row">
								<div class="col-sm-6 col-left border-right">
									<div class="sc-input-field border-bottom">
										<i class="left-field-prefix facebook"></i>
										<label for="web_url" class="full-bg-label">Add Facebook url</label>
										<input id="web_url" name="web_url" class="full-bg adjust-width" type="text">
									</div>
								</div>
								<div class="col-sm-6 col-right">
									<div class="sc-input-field border-bottom">
										<i class="field-prefix twitter"></i>
										<label for="web_url" class="full-bg-label">Add Twitter url</label>
										<input id="web_url" name="web_url" class="full-bg adjust-width" type="text">
									</div>
								</div>
							</div>
							
							<div class="row">
								<div class="col-sm-6 col-left border-right">
									<div class="sc-input-field border-bottom">
										<i class="left-field-prefix linkedin"></i>
										<label for="web_url" class="full-bg-label">Add Linkedin url</label>
										<input id="web_url" name="web_url" class="full-bg adjust-width" type="text">
									</div>
								</div>
								<div class="col-sm-6 col-right">
									<div class="sc-input-field border-bottom">
										<i class="field-prefix instagram"></i>
										<label for="web_url" class="full-bg-label">Add Instagram url</label>
										<input id="web_url" name="web_url" class="full-bg adjust-width" type="text">
									</div>
								</div>
							</div>
							
							<div class="row">
								<div class="col-sm-6 col-left border-right">
									<div class="sc-input-field border-bottom">
										<i class="left-field-prefix youtube"></i>
										<label for="web_url" class="full-bg-label">Add Youtube url</label>
										<input id="web_url" name="web_url" class="full-bg adjust-width" type="text">
									</div>
								</div>
								<div class="col-sm-6 col-right">
									<div class="sc-input-field border-bottom">
										<i class="field-prefix vimeo"></i>
										<label for="web_url" class="full-bg-label">Add Vimeo url</label>
										<input id="web_url" name="web_url" class="full-bg adjust-width" type="text">
									</div>
								</div>
							</div>

						</div>
					</div>
					<div class="col-sm-4"></div>
				</div>
			</main>
			</div>
		</div>
	</body>
	<?php include('footer.php'); ?>
	<script>
		$(document).ready(function() {
			
		});
	</script>
</html>