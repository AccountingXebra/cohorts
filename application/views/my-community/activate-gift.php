<!DOCTYPE html>
<html>
<head>
	<!-- Favicon -->
	<!--link rel="shortcut icon" href="<?php echo base_url();?>assets/images/logo/xc-favicon.png"/-->

	<!-- Title -->
	<title>Activate Gift</title>

	<!-- CSS -->
	<link rel="stylesheet" href="<?php echo base_url();?>asset/gift/css/marketplace.css">
	<link rel="stylesheet" href="<?php echo base_url();?>asset/gift/css/bootstrap.css">
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700" rel="stylesheet">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

	<!-- JS -->
	<script src="<?php echo base_url();?>asset/gift/js/jquery.js"></script>
    <script src="<?php echo base_url();?>asset/gift/js/popper.js"></script>
    <script src="<?php echo base_url();?>asset/gift/js/bootstrap.js"></script>
	<script src="<?php echo base_url();?>asset/gift/js/select2.min.js"></script>

	<style type="text/css">
		body{margin:0; background:#7965E9;}
		
		.partitioned {
	  padding-left: 15px;
	  letter-spacing: 22px;
	  border: 0;
	  background-image: linear-gradient(to left, black 70%, rgba(255, 255, 255, 0) 0%);
	  background-position: bottom;
	  background-size: 31px 1px;
	  background-repeat: repeat-x;
	  background-position-x: 30px;
	  width: 220px;
	  min-width:220px;
	}

	#divInner{
	  left: 0;
	  position: sticky;
	}

	#divOuter{
	  width:190px; 
	  overflow:hidden;
	  margin-left:30%;
	}
	
	.vl {
	  border-left: 1px solid;
	  height: 100px;
	  position: absolute;
	  left: 49%;
	}
	.sgn-select{
	  margin-top:-1vh;		
	  min-width: 9em;
	  /*position: relative;*/
	  display: inline-block;
	  margin-right: 1em;
	  min-height: 2em;
	  max-height:3em;
	  /*overflow:hidden;*/
	  top: .5em;  
	  cursor: pointer;
	  text-align: left;
	  white-space: nowrap;
	  color: #e0e0d1;
	  outline: none;
	  border: -0.94em solid transparent;
	  border-radius: 0.5em;
	}
		
		#term_cond[type="checkbox"]:checked + label:before {
		    border-right: 2px solid #fff;
		    border-bottom: 2px solid #fff;
		    width: 8px;
		    height: 11px;
		    z-index: 1;
		    top: 0;
		    left: 0;
		}
		#term_cond[type="checkbox"] + label:after {
		    border: 1px solid #50E3C2;
		    background: #50E3C2;
		    width: 15px;
		    height: 15px;
		    transform: scale(1);
		}
		.right-list{
			margin-left: -5%;
		}
	</style>
</head>
<body>

	<div class="container-fluid">

		<div class="first-label">
			<h3 style="font-family: 'Roboto', sans-serif; color: #fff; text-align :center;">Welcome to Xebra. Here's our gift to you<h3/><!-- Here is your gift for being a part of Xebra business cohorts -->
		</div>

		<div class="img-div">
			<img height="220" width="800" src="<?php echo base_url();?>asset/images/gift.png">
		</div>

		<div class="middle-div">
			<h3 style="font-family: 'Roboto', sans-serif; color: #fff; text-align: center;">Get complete access to Free Forever Walk plan of Xebra. Now boost your revenues & profits<h3/><!-- You will get complete access to Booster pack of Xebra Business Intelligence Application for 3 months -->
			<hr class="one-hr">
		</div>

		<div class="row">
			<div class="col-sm-12">
				<h3 style="font-size:25px; font-family: 'Roboto', sans-serif; color: #fff; text-align:center;">What does the Walk Plan include?</h3>
			</div>
		</div>
		<div class="row list-buzz">
			<div class="col-sm-2"></div>
			<div class="col-sm-4 mar-top">
				<ul class="feature_list">
					<li>Customised invoices & one click online receipts</li>
					<li>Expense recording & payment</li>
					<li>Employee Master & Payroll</li>
					<li>Asset Tracker & Depreciation</li>
				</ul>
			</div>
			<div class="col-sm-4 mar-top">
				<ul class="feature_list right-list">
					<li>Give out portal access to your clients and vendors</li>
					<li>One-click GST & TDS excel ready</li>
					<li>Auto record your Cash & Bank entries</li>
					<li>Grow your business on Marketplace</li>
				</ul>
			</div>
			<div class="col-sm-2"></div>
			<hr class="one-hr">
		</div>

		<div class="row">
			<div class="col-sm-12 mar-top">
				<h3 style="font-size: 16px; font-family: 'Roboto', sans-serif; color: #fff; text-align: center; margin-top: 20px; margin-bottom: 20px;">Follow this simple 3 steps process <h3/>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-3"></div>
			<div class="col-sm-2">
				<p style="float:left;" class="numberCircle">1</p><div class="step-one">Fill Personal Profile </div>
			</div>
			<div class="col-sm-2">
				<p style="float:left;" class="numberCircle">2</p><div class="step-one">Fill Company Profile </div>
			</div>
			<div class="col-sm-2">
				<p style="float:left;" class="numberCircle">3</p><div class="step-three">Start preparing Invoices </div>
			</div>
			<div class="col-sm-3"></div>
		</div>

		<footer class="gift-terms">
			<form method="post" action="marketplace/validate-activate-gift">
				<?php $csrf = array(
					'name' => $this->security->get_csrf_token_name(),
					'hash' => $this->security->get_csrf_hash()
				);
				?>
				<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
				<input type="checkbox" id="term_cond" class="term_cond_chkbox" value="1" name="term_cond">
				<?php 
					$cust_detail = $this->Adminmaster_model->selectData('sales_customer_contacts','*',array('cust_id'=>$this->session->userdata['client_session']['cust_id']));
                    foreach($cust_detail as $cust) {
						$cust_mobile = $cust->cp_mobile;
					}
				?>
		    <label class="checkbox" for="term_cond"><span class="check-boxs">I accept the <a>terms and conditions</a> and <a>privacy policy</a></span></label>
				<a style="color:#fff" class="btn btn-theme btn-large right accept-btn disabled" href="<?php echo base_url()?>signup?ne=<?php echo $this->session->userdata['client_session']['client_email']; ?>&name=<?php echo $this->session->userdata['client_session']['client_username'];?>&mobile=<?php echo $cust_mobile; ?>&portal=vencl">Accept</a>
				<!--button type="button" class="accept-btn btn btn-info" disabled>Accept</button-->
				<!--a data-target="#enter_mobile" data-toggle="modal" class="MainNavText" id="MainNavHelp" href="#myModal">HELP</a-->
			</form>
		</footer>
	</div>
	
	<div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Modal Header</h4>
                </div>
                <div class="modal-body">
                    <p>Some text in the modal.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
	
	<div id="enter_mobile" class="modal fade" role="dialog" style="margin-top:50px;">
        <style>
			.login_page_co {
				padding-left: 54px;
				margin-left: 9.6%;
				width: 57% !important;
			}
		</style>
		<div class="modal-dialog modal-lg otp_check_form">
            <!-- Modal content-->
            <div class="container">
                <div class="row">
                <div class="col-md-1"></div>
				<div class="col-md-8 login_page_co">
                    <div class="modal-content">
                        <div class="" style="text-align:center; margin-top:15px;">
							<img width="180" height="70" src="<?php echo base_url(); ?>public/images/cohorts.png" alt="xebra-logo" class="logo_style_2"/>
                        </div>
                        <div class="modal-body" style="margin-bottom:-5%;">
                            <form action="" id="check_for_otp" name="check_for_otp" class="" method="post" accept-charset="utf-8" novalidate="true">
							<?php $csrf = array(
					'name' => $this->security->get_csrf_token_name(),
					'hash' => $this->security->get_csrf_hash()
				);
				?>
				<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
                            <div class="row" style="display:block; text-align:center;">
								<div class="popup_top_text">We take data security very seriously</div>
                                <div class="popup_top_text_2" style="color:#8533ff !important;"> Please enter your 10-digit mobile number</span></div>
                                    <div class="col-lg-12">
                                    	<div id="otp_error_popup" name="error_popup" style="font-size: 14px; color: #f44336; text-align:center;">
                            			</div>
                                    </div>
                                    
								<input type="hidden" name="reg_id" id="reg_id_otp" value="">
								<div class="col-lg-12 login_form_textbox" style="margin-top:13px;">
									<p style="text-align:center;"><i class="fa fa-mobile" style="font-size:24px;"></i></p>
									<input class="" style="" name="mbl_no" id="mbl_no" type="number" maxlength="10"/>
								</div>
                                <div class="col-lg-12">
                                     <div class="" style="text-align: center; margin-top: 25px;">
                                        <div class="form-group login_btn_1">
                                            <input type="submit" onMouseOver="this.style.color='white'" style="background-color:#8533ff !important; color:#fff;" class="btn login_form_btn" value="SUBMIT" name="submit" id="submit">
                                        </div>
                                     </div>
                                     <div class="" style="float:right; text-align: center; margin-top: 25px;">
                                        <div class="form-group login_btn_1">
                                            
                                            <input type="text" class="numeric_number" name="resend_email_mobile" id="resend_email_mobile" value="" hidden>
                                            <input type="password" name="resend_password_mobile" id="resend_password_mobile" value="" hidden>
                                        </div>
                                     </div>
                                </div>
                         
                             </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
	</div>
	
	<div id="login_mobile_otp" class="modal fade" role="dialog" style="margin-top:50px;">
        <style>
			.login_page_co {
				padding-left: 54px;
				margin-left: 9.6%;
				width: 57% !important;
			}
		</style>
		<div class="modal-dialog modal-lg otp_check_form">
            <!-- Modal content-->
            <div class="container">
                <div class="row">
                <div class="col-md-1"></div>
				<div class="col-md-8 login_page_co">
                    <div class="modal-content">
                        <div class="" style="text-align:center; margin-top:15px;">
							<img width="180" height="70" src="<?php echo base_url(); ?>public/images/cohorts.png" alt="xebra-logo" class="logo_style_2"/>
                        </div>
                        <div class="modal-body" style="margin-bottom:-5%;">
                            <form action="" id="check_for_otp" name="check_for_otp" class="" method="post" accept-charset="utf-8" novalidate="true">
							<?php $csrf = array(
					'name' => $this->security->get_csrf_token_name(),
					'hash' => $this->security->get_csrf_hash()
				);
				?>
				<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
                            <div class="row" style="display:block; text-align:center;">
								<div class="popup_top_text">We take data security very seriously</div>
                                <div class="popup_top_text_2" style="color:#8533ff !important;"> Please enter 6-digit OTP Sent on your mobile number</span></div>
                                    <div class="col-lg-12">
                                    	<div id="otp_error_popup" name="error_popup" style="font-size: 14px; color: #f44336; text-align:center;">
                            			</div>
                                    </div>
                                    
								<input type="hidden" name="reg_id" id="reg_id_otp" value="">
								<div class="col-lg-12 login_form_textbox" style="margin-top:13px;">
									<div class="col-lg-1"></div>
									<div class="col-lg-10" style="display:block; text-align:center;">
										<p style="margin-left:20%; text-align:center;"><i class="fa fa-mobile" style="font-size:24px;"></i></p>
										<div id="divOuter">
											<div id="divInner">
												<input class="partitioned" style="letter-spacing:24px;" name="mbl_code" id="mbl_code" type="text" maxlength="6" />
											</div>
										</div>
									</div>
									<div class="col-lg-4"></div>
								</div>
                                
                                <div class="col-lg-12">
                                     <div class="" style="text-align: center; margin-top: 25px;">
                                        <div class="form-group login_btn_1">
                                            <input type="submit" onMouseOver="this.style.color='white'" style="background-color:#8533ff !important; color:#fff;" class="btn login_form_btn" value="VERIFY" name="submit" id="submit">
											<input type="submit" onMouseOver="this.style.color='white'" style="background-color:#8533ff !important; color:#fff;" class="btn login_form_btn" value="RE-SEND MOBILE" name="resend-mobile-otp" id="resend-mobile-otp">
                                        </div>
                                     </div>
                                     <div class="" style="float:right; text-align: center; margin-top: 25px;">
                                        <div class="form-group login_btn_1">
                                            
                                            <input type="text" class="numeric_number" name="resend_email_mobile" id="resend_email_mobile" value="" hidden>
                                            <input type="password" name="resend_password_mobile" id="resend_password_mobile" value="" hidden>
                                        </div>
                                     </div>
                                </div>
                         
                             </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
	</div>
</body>
<script type="text/javascript">
      $(document).ready(function() {
				$("#term_cond").change(function(){
					if ($(this).prop('checked')==true){
							$('.accept-btn').removeClass('disabled');
    			}else if ($(this).prop('checked')==false){
							$('.accept-btn').addClass('disabled');
    			}
				});
			});
</script>
</html>
