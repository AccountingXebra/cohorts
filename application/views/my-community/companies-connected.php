<style>
	.total-req{
		font-size:13px;
	}

	.request-div:hover{
		box-shadow: 0 1px 10px #7864e94d;
	}

	.request-div{
		height: 220px;
		width: 32%;
		margin: 10px;
		padding: 20px 10px;
		border-radius:5px;
	}

	.req_accept{
		background-color:#fff !important;
		border:1px solid #7864e9 !important;
		color: #7864e9 !important;
		height:35px !important;
		line-height:35px !important;
		margin:15px -5px;
	}

	.req_decline{
		background-color:#fff !important;
		border:1px solid #ccc !important;
		color: #ccc !important;
		height:35px !important;
		line-height:35px !important;
		margin:15px -5px;
	}

	.org-logo-req{
		margin-top:20px;
	}

	.top-all{
		text-align:right;
		font-size:12px;
		margin-top:30px;
		margin-right:35px;
	}
</style>

<?php 
	
?>

<div class="container custom content-div">

	<div class="row">
		<div class="col l12 s12 m12" style="margin: -30px 0 -5px 0px;">

			<div class="col l6 s12 m12">
				<p class="top-label">MY CONNECTIONS <label class="total-req">(<?=count($result1)?>)</label></p>
			</div>

			<!--div class="col l6 s12 m12 right">
				<p class="top-all"><a href="<?php base_url(); ?>" id="view_sent_request" class="">View All</a></p>
			</div-->

		</div>
	</div>

	<div class="row">
		<div class="col l12 s12 m12">
			<div class="col l12 s12 m12">

				<?php for($i=0; $i<count($result1); $i++) {?>
					<div class="col l4 s12 m12 white-box request-div" style="height: 110%;">
						<div class="row">
							<?php
								if($result1[$i]['bus_company_logo'] != '') {
									$company_logo = '<img src="'.DOC_ROOT_DOWNLOAD_PATH_CO.'company_logos/'.$result1[$i]['bus_id'].'/'.$result1[$i]['bus_company_logo'].'" height="50px" class="logo_style_2" width="50px">';
								} else {
									$company_logo = '<img src="'.DOC_ROOT_DOWNLOAD_PATH_CO.'company_logos/'.$result1[$i]['bus_id'].'/'.$result1[$i]['bus_company_logo'].'" height="50px" class="logo_style" width="50px">';
								}
							?>
							
							<div class="col s12 m12 l8">
								<p class="org-name" style="width: 120%;"><b><?php echo strtoupper($result1[$i]['bus_company_name']); ?></b></p>
								<label class="org-tagline"><?php echo strtoupper($result1[$i]['nature_of_bus']); ?></label><br><br>
								<i class="material-icons view-icons com-loc" style="margin-left: -6px;">location_on</i><label class="org-loc"><?php echo strtoupper($result1[$i]['name']); ?></label><br>
							</div>

							<div class="col s12 m12 l4 org-logo-req" style="width: 65%; margin: -110px 0 0 260px;">
								<div class="col s6 m6 l4" style="text-align: left">
									<?php echo $company_logo; ?>
								</div>
							</div>

							<div class="col s6 m6 l6" style="font-size: 14px; margin: 15px 0px 0px 0px;"> 	
								<a href="<?php echo base_url(); ?>community/view-company-details?token=<?php echo $result1[$i]['bus_id']; ?>" style="color:#7965E9;" onclick=""><b>VIEW PROFILE</b></a>
							</div>
						</div>
					</div>
				<?php }?>
			</div>
		</div>
	</div>
</div>