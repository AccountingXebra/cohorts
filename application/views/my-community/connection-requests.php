<style type="text/css">

	#breadcrumbs-wrapper{
		padding-bottom:10px !important;
	}
	
	#search_comp[type=text] {
		width: 90% !important;
		box-sizing: border-box !important;
		border: 1px solid #ccc !important;
		border-radius:4px !important;
		font-size: 16px !important;
		background-color: white !important;
		background-image: url('<?php echo base_url(); ?>asset/css/img/icons/search.png') !important;
		background-position: 95% 10px !important; 
		background-repeat: no-repeat !important;
		padding: 12px 20px 12px 14px !important;
		outline: none !important;
	}
	
	#find_contact{
		height:45px;
		line-height:45px;
	}
	
	#view_sent_request{
		color:#50E2C2 !important;
		font-size:15px;
	}
	
	.request{
		padding-top:12px !important;
		/*border-left:1px solid #bec4d4;*/
		margin-left:-15px !important;
	}
	
	.top-label{
		font-size:13px;
		margin-top:30px;
	}
	
	.content-div{
		margin-top:20px;
		margin-left:20px;
	}
	
	.company-div{
		height: 250px;
		width: 24% !important;
		margin: 5px;
		padding: 10px;
		border-radius:5px;
	}
	
	.company-div:hover{
		box-shadow: 0 1px 10px #7864e94d;
	}
	
	.white-box{
		background-color:#fff !important;
	}
	
	.div_close{
		margin-right:8px; 
		margin-top:15px;
	}
	
	.text-center{
		text-align:center;
	}
	
	.org-name{
		margin:8px 0;
		font-size:15px !important;
	}
	
	.connect{
		background-color:#fff !important;
		border:1px solid #7864e9 !important;
		color: #7864e9 !important;
		height:35px !important;
		line-height:35px !important;
		margin:15px 0;
	}
	
	.com-loc{
		font-size:20px !important;
	}
	
	.org-tagline{
		font-size:13px !important;
	}
	
	.org-loc{
		font-size:12px !important;
	}
		
	.org_details{
		background-color:#fff !important;
		text-align:center;
	}
		
	#new-connection{
		overflow-x:hidden !important;
		top:25% !important;
	}
	
	#new-connection .modalheader{
		padding:22px 30px !important;
	}
	
	#new-connection .conn-org{
		text-align:center;
		font-size:14px;
	}
	
	#new-connection .conn-org-tagline{
		font-size:12px;
	}
	
	.radio-list{
		padding:30px 0 !important;
	}
	
	 #con-client[type="radio"] + label:after {
		 margin:1px !important;
	 }
	 
	 #con-vendor[type="radio"] + label:after {
		 margin:1px !important;
	 }
	
	 #con-both[type="radio"] + label:after {
		 margin:1px !important;
	 }
	 
	 .suceess-msg{
		 font-size:14px;
	 }
	 
	.top-activity{
		text-align:right;
		font-size:12px;
		margin-top:30px;
		margin-right:35px;
	}
	
	.close-pop{
		cursor:pointer !important;
	}

	#breadcrumbs-wrapper {
	    border-bottom: 1px solid #f3f4f9;
	    padding: 0px 0;
	    float: left;
	    width: 100%;
	}

	#breadcrumbs-wrapper .breadcrumbs-title {
	    font-size: 21px;
	    color: #413A68;
	    margin: -12px 0 0 0px;
	    line-height: unset !important;
	    letter-spacing: 0.10px;
	}

	.select-wrapper label.error:not(.active) {
	margin: -30px 0 0 -11px;
	}

	/*----------START SEARCH DROPDOWN CSS--------*/
	.select2-container {
	width: 100% !important;
	}

	[type="radio"]:not(:checked) + label, [type="radio"]:checked + label {
    position: relative;
    padding-left: 20px;
    cursor: pointer;
    display: inline-block;
    height: 25px;
    line-height: 25px;
    font-size: 1rem;
    -webkit-transition: .28s ease;
    transition: .28s ease;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
}

[type="radio"] + label:before, [type="radio"] + label:after {
    content: '';
    position: absolute;
    left: 0;
    top: 0;
    margin: 4px 0 0 0;
    width: 16px;
    height: 16px;
    z-index: 0;
    -webkit-transition: .28s ease;
    transition: .28s ease;
}

body.easy-tab1 [type="radio"] + label:before, [type="radio"] + label:after {
    content: '';
    position: absolute;
    left: 0;
    top: 0;
    margin: 3px 0 0 -2px;
    width: 20px;
    height: 20px;
    z-index: 0;
    -webkit-transition: .28s ease;
    transition: .28s ease;
}

	.select2-container--default .select2-selection--multiple {
	border:1px solid #afb8c9 !important;
	border-radius:5px !important;
	}

	input[type="search"]:not(.browser-default) {
	height: 30px;
	font-size: 14px;
	margin: 0;
	margin-top:8px !important;
	border-radius: 5px;
	}

	.select2-container--default .select2-selection--multiple .select2-selection__rendered {
	font-size: 13px;
	line-height: 35px;
	color: #666;
	font-weight: 400;
	}

	.select2-container--default .select2-selection--multiple .select2-selection__arrow {
	height: 32px;
	}

	.select2-search--dropdown {
	padding: 0;
	margin-top:-10px !important;
	}

	input[type="search"]:not(.browser-default):focus:not([readonly]) {
	border-bottom: 1px solid #bbb;
	box-shadow: none;
	}

	.select2-container--default .select2-selection--multiple:focus {
	outline: none;
	}

	.select2-container--default .select2-results__option--highlighted[aria-selected] {
	background: #fffaef;
	color: #666;
	}

	.select2-container--default .select2-results > .select2-results__options {
	font-size: 14px;
	border-radius: 5px;
	box-shadow: 0px 2px 6px #B0B7CA;
	}

	.select2-dropdown {
	border: none;
	border-radius: 5px;
	}

	.select2-container .select2-selection--multiple {
	height: 48px;
	}

	.select2-container .select2-selection--single {
	height: 48px;
	}

	.select2-results__option[aria-selected] {
	border-bottom: 1px solid #f2f7f9;
	padding: 14px 16px;
	border-top-left-radius: 0;
	}

	.select2-container--default .select2-search--dropdown .select2-search__field {
	/* border: none;*/
	border: 1px solid #d0d0d0;
	padding: 0 0 0 15px !important;
	width: 93.5%;
	max-width: 100%;
	background: #fff;
	border-radius: 4px;
	border-top-left-radius: 4px;
	border-top-right-radius: 4px;
	}

	input[type="radio"]+span:before, input[type="radio"]+span:after {
		left: -15px !important;
    	top: 4px !important;
	}

	.select2-container--open .select2-dropdown--below {
	margin-top: 0px;
	/*border-top: 1px solid #aaa;
	border-radius: 5px !important;*/
	}

	ul#select2-currency-results {
	width: 97%;
	}

	/*---Dropdown error message format---*/
	.select-wrapper + label.error{
	margin: 18px 0 0 -10px;
	} 

	select.error + label.error:not(.active){
	margin: -20px 0 0 -10px; 
	}

	select + label.error.active{
	margin-left: -10px;
	}

	body {
	/*overflow-y:hidden;*/
	}

	#search_comp[type=text] {
	width: 55% !important;
	box-sizing: border-box !important;
	border: 1px solid #ccc !important;
	border-radius:5px !important;
	font-size: 16px !important;
	background-color: none !important;
	background-image: url('<?php echo base_url(); ?>asset/css/img/icons/search.png') !important;
	background-position: 10px 10px !important; 
	background-repeat: no-repeat !important;
	padding: 12px 20px 12px 40px !important;
	outline: none !important;
	}

	#select2-market_country-container{
	margin-top:10px !important;
	}

	.select2-container--default .select2-selection--single .select2-selection__arrow {
	top:10px !important;
	}

	/* Select Box css */

	.multi-loc input[type=text]:not(.browser-default) {
	font-size: 13px !important;
	padding-left:10px !important;
	color: #9e9e9e !important;
	}

	.multi-loc .dropdown-content.select-dropdown {
	margin-top:47px !important;
	overflow-y: scroll !important;
	}

	.loc_select .select2{
	width:200px !important;
	}

	#nature input[type=text]:not(.browser-default) {
	font-size: 13px !important;
	padding-left:10px !important;
	color: #9e9e9e !important;
	}

	#nature .dropdown-content.select-dropdown {
	margin-top:47spx!important;
	overflow-y: scroll !important;
	}

	.loc_select{
	width:240px !important;
	border:1px solid #e8ebf4;
	height:50px;
	border-radius:5px;
	padding-left:0px;
	margin-bottom:20px;
	}

	.loc_select input[type=text] {
	margin-left: 10px;
	padding-top: 3px;
	}

	.nat_select{
	width:240px !important;
	margin-left:23px;
	border:1px solid #e8ebf4;
	height:50px;
	border-radius:5px;
	padding-left:15px;
	margin-bottom:20px;
	}
	/* Select Box css end */

	.page-link{
	font-size:13px;
	}

	.multi-loc .select-dropdown {
	border:1px solid #afb8c9 !important;
	border-radius:5px !important;
	}

	.loc_title{
	font-size:14px !important;
	}

	.select_loc{
	padding-left:10px; 
	padding-right:10px;
	padding-top:3px;
	padding-bottom:3px;		   
	text-align:center; 
	width:15px !important; 
	height:15px !important;
	color:#ffff; 
	background-color:#B0B7CA !important; 
	border-radius:15px; 
	font-size:14px;
	}

	.remove_loc{
	padding-left:5px !important;
	color: white !important;
	}

	.comp_name{
	font-size:14px;
	color:#000000;
	}

	.rate-star{
	padding:10px 0  10px 10px !important;
	margin-top:-5px;
	}

	#five_star + label {
	padding:0 0 0 25px !important;
	}

	#four_star + label {
	padding:0 0 0 25px !important;
	}

	#three_star + label {
	padding:0 0 0 25px !important;
	}

	#two_star + label {
	padding:0 0 0 25px !important;
	}

	#one_star + label {
	padding:0 0 0 25px !important;
	}

	[type="checkbox"] + label:before, [type="checkbox"]:not(.filled-in) + label:after {
		margin: 4px 0 0 0 !important;		
	}

	.rate-btn{
		background-color:#7965E9 !important;
		color: white !important;
		border:1px solid #7965E9;
		border-radius:5px;
	}

	.modalheader {
	    padding: 20px;
	}

</style>

<!-- START MAIN -->
<div id="main" style="padding:10px 0 0 0px !important;" height="80%">

  	<!-- START WRAPPER -->
  	<div class="wrapper">

    	<!-- START LEFT SIDEBAR NAV-->
    	<?php //$this->load->view('template/sidebar'); ?>
    	<!-- END LEFT SIDEBAR NAV-->

    	<!-- START CONTENT -->
    	<section id="content" class="bg-cp sales_invoices-search">
	     	<div id="breadcrumbs-wrapper">
	        	<div class="container">
		          	<div class="row">

		          		<div class="container">
							<div class="plain-page-header">
								<div class="row">
									<div class="col l6 s12 m6" style="margin-left: -26px;">
										<a class="go-back underline" href="<?php echo base_url();?>community/my-connections">Back to My Connections</a>
									</div>
								</div>
							</div>
						</div>

		          		<!-- Breadcrumps -->
		            	<div class="col s10 m6 l4" style="width: 37% !important;">
		              		<h5 class="breadcrumbs-title my-ex">Connection Requests</h5>
		              		<ol class="breadcrumbs">
		                		<li><a href="<?php echo base_url();?>community/my-connections">MY COMMUNITY / MY CONNECTIONS</a> /
		                			<a>CONNECTION REQUESTS</a>
		              		</ol>
		            	</div>
		            	<!-- End of Breadcrumps -->

		            	<!-- Request Results -->
		            	<div class="col s12 m12 l12" style="margin-top: 50px; margin-left: 140px;">
		            		<!-- Search Company -->
				            <!--div class="col s2 m6 l4" style="height: 80px;">
				            	<form method="post" action="<?php echo base_url();?>community/company-search">
				                	<input type="text" id="search_comp" name="search" value="<?php if(@$post_data['search']){echo $post_data['search']; } ?>" placeholder="SEARCH BY COMPANY NAME" style="width: 80% !important; margin: -12px 0px 5px 77% !important;">
				                	<div class="col s12 m12 l12" style="text-align:center; margin-top:20px !important;">
										<input type="submit" name="submit" style="margin-top: -120px !important; margin-left: 166% !important; width: 25% !important;" id="submit" class="btn btn-welcome" value="Search">
									</div>
				                </form>

				                <div style="color: black; margin-top: 50% !important; margin-left: 240px !important; font-size: 17px !important; width: 125% !important;">
									<?php  
										if (count($result) > 1) {
											echo "THERE ARE NO COMPANIES LISTED IN THIS CATEGORY";
										}
									?>
								</div>

				            </div--> <!-- End of Search Company -->

				            <!-- Companies -->
				            <div class="col s12 m12 l12" style="margin-top: -45px !important;">
								<div id="search_update">

									<?php for($i=0; $i<count($result); $i++){ ?>
											<!--Company Name, Nature of Business & Logo-->
											<div class="row" style="height: 55px; margin-bottom: 5px !important;">
												<div class="col s6 m6 l8" style="margin-top: 5px !important;">
													<label class="comp_name"><b><?php echo strtoupper($result[$i]['bus_company_name']); ?></b></label><br>
													
												</div>

												<?php
													if($result[$i]['bus_company_logo'] != '') {
														$company_logo = '<img src="'.DOC_ROOT_DOWNLOAD_PATH_CO.'company_logos/'.$result[$i]['bus_id'].'/'.$result[$i]['bus_company_logo'].'" height="50px" class="logo_style_2" width="50px">';
													} else {
														$company_logo = '<img src="'.DOC_ROOT_DOWNLOAD_PATH_CO.'company_logos/'.$result[$i]['bus_id'].'/'.$result[$i]['bus_company_logo'].'" height="50px" class="logo_style" width="50px">';
													}
												?>
												<div class="col s6 m6 l4" style="text-align: right">
													<?php echo $company_logo; ?>
												</div>
											</div> <!--Company Name, Nature of Business & Logo-->

											<!--Company location-->
											<div class="row" style="height:25px; margin-bottom:5px !important;">
												<div class="col s6 m6 l8">
													<div class="col l2 s1 m1" style="margin-left:-10px !important;">
														<i class="material-icons view-icons">location_on</i>
													</div>
													<div class="col l2 s1 m1" style="width: 29%;">
														<label> <?php echo strtoupper($result[$i]['name']); ?> </label>
													</div>
												</div>
												<div class="col s6 m6 l4"></div>
											</div> <!--Company location-->

								</div>
							</div> <!-- End of Companies --> <?php }?>
		            	</div> <!-- End of Request Results -->

	         		</div>
	       		</div>
	     	</div>
   		</section> <!-- END CONTENT -->
  	</div> <!-- END WRAPPER -->
</div> <!-- END MAIN -->

<script type="text/javascript">
	$(document).ready(function() {
		$('.div_close').on('click', function(){
			console.log('OK');
			$(this).closest(".company-div").remove();
		});
		$('.connecting').on('click', function(){
			console.log('OK');
			$("#conn").hide();
			console.log('BYE');
			$("#ok").show();
			var bus_id=$(this).val();
 		if(bus_id!=''){
 			srec_counter=0;
 			console.log('yeah');
	 		$.ajax({
					type: "POST",
					data: {'csrf_test_name':csrf_hash,'bus_id':bus_id},
					url: base_url+'connection/sent_receive',
					
	                success:function(result)
					{
						var data=JSON.parse(result);

                          $("#employee_name").val(data.name);
                           $("#employee_designation").val(data.designation);
                           $("#reporting_manager_name").val(data.reporting_to);
                           $("#reporting_managers_designation").val(data.manager_designation);

                        

						$('select').material_select();	
					}
	 		});
	 		}	
		});
		
	}); 
</script>