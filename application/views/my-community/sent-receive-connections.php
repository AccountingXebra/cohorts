
<style type="text/css">

	#breadcrumbs-wrapper {
	border-bottom: 1px solid #f3f4f9;
	padding: 0px 0;
	float: left;
	width: 100%;
	}

	#search_comp[type=text] {
	width: 90% !important;
	box-sizing: border-box !important;
	border: 1px solid #ccc !important;
	border-radius:4px !important;
	font-size: 16px !important;
	background-color: white !important;
	background-image: url('<?php echo base_url(); ?>asset/css/img/icons/search.png') !important;
	background-position: 95% 10px !important; 
	background-repeat: no-repeat !important;
	padding: 12px 20px 12px 14px !important;
	outline: none !important;
	}

	#find_contact{
	height:45px;
	line-height:45px;
	}

	#view_sent_request{
	color:#50E2C2 !important;
	font-size:15px;
	}

	.request{
	padding-top:12px !important;
	/*border-left:1px solid #bec4d4;*/
	margin-left:-15px !important;
	}

	.top-label{
	font-size:13px;
	margin-top:30px;
	}

	.content-div{
	margin-top:20px;
	margin-left:20px;
	}

	.company-div:hover{
	box-shadow: 0 1px 10px #7864e94d;
	}

	.company-div{
	height: 170px;
	width: 48% !important;
	margin: 10px;
	padding: 10px;
	border-radius:5px;
	}

	.white-box{
	background-color:#fff !important;
	}

	.div_close{
	margin-right:8px; 
	margin-top:15px;
	}

	.text-center{
	text-align:center;
	}

	.org-name{
	margin:8px 0;
	font-size:14px !important;
	}

	.withdraw{
	background-color:#7864e9 !important;
	border:1px solid #ccc !important;
	color: white !important;
	height:35px !important;
	line-height:35px !important;
	margin:15px -5px;
	}

	.req_accept{
	background-color:#7864e9 !important;
	border:1px solid #ccc !important;
	color: white !important;
	height:35px !important;
	line-height:35px !important;
	margin:15px -5px;
	}

	.req_decline{
	background-color:#fff !important;
	border:1px solid #7864e9 !important;
	color: #7864e9 !important;
	height:35px !important;
	line-height:35px !important;
	margin:15px -5px;
	}

	.com-loc{
	font-size:20px !important;
	}

	.org-tagline{
	font-size:13px !important;
	}

	.org-logo{
	margin:10px 0;
	}

	.org-loc{
	font-size:12px !important;
	}

	.bulk-action{
	margin-top:15px;
	}

	.bulk-check{
	margin-right:-50px;
	}

	.send-rec-tab{
	text-align:right;
	margin-top:25px;
	}

	.recei-tab{
	border-left:3px solid #ccc;
	}

	#sent_tab, #recei_tab{
	color:#ccc !important;
	}

	#sent_tab.active, #recei_tab.active{
	color:#000 !important;
	}

	.org-info{
	margin: 8% 0 !important;
	}

	.org-person{
	margin: 5% 0 !important;
	}

	label.label-bulk {
	margin: 27px 0 0 0 !important;
	}

	.new-req-noti{
	background-color:#50E2C2 !important;
	height:43px !important;
	margin-top:2px;
	padding:10px;
	}

	.noti-label{
	color:#fff !important;
	font-size:15px;
	}
	.no-pending{ color: #ff85a1 !important; font-weight:500; }

</style>

<div id="main" style="padding:10px 0 0 0px !important;">
	<div class="wrapper"> <!-- START WRAPPER -->

		<!-- START LEFT SIDEBAR NAV-->
		<?php //$this->load->view('template/sidebar'); ?>
		<!-- END LEFT SIDEBAR NAV-->
		
		<section id="content" class="bg-cp sales_invoices-search"> <!-- START CONTENT -->

			<div class="row text-center new-req-noti" hidden> <!--Notification-->
				<label class="noti-label"><b>You have made 3 new connections</b></label>
			</div> <!--End of Notification-->
			
			<div id="breadcrumbs-wrapper"> <!--Breadcrump wrapper-->
				<div class="container">

					<div class="row" style="margin-bottom: 5px;">
						<div class="col l6 s12 m6">
							<a class="go-back underline" href="<?php base_url();?>my-connections">Back to My Connections</a>
						</div>
					</div>

					<div class="row">

						<div class="col s10 m6 l6"> <!--Breadcrump-->
							<h5 class="breadcrumbs-title my-ex">Connection Requests</h5>

							<ol class="breadcrumbs">
								<li><a href="">MY COMMUNITY / MY Connections / NEW CONNECTION REQUESTS</a>
							</ol>
						</div> <!--End of Breadcrump-->

						<div class="col s2 m6 l4"> <!-- Search Company -->
							<form method="post" action="<?php echo base_url();?>community/company-search">
								<?php $csrf = array(
					'name' => $this->security->get_csrf_token_name(),
					'hash' => $this->security->get_csrf_hash()
				);
				?>
				<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
								<input type="text" id="search_comp" name="search" value="<?php if(@$post_data['search']){echo $post_data['search']; } ?>" placeholder="SEARCH BY COMPANY NAME">

								<div class="col s12 m12 l12" style="text-align:center; margin-top:20px !important;">
									<input type="submit" name="submit" style="margin-top: -150px !important; margin-left: 95% !important; width: 25% !important;" id="submit" class="btn btn-welcome" value="Search">
								</div>
							</form>
						</div> <!-- End of Search Company -->
					</div>
				</div>
			</div> <!--End of Breadcrump wrapper-->

			<div class="container custom content-div"> <!--Page content-->
				<div class="row">

					<div class="col l12 s12 m12"> <!--Bulk Actions-->
						<div class="col l1 s12 m12 bulk-check">
							<input style="display: none;" type="checkbox" id="sent_rece_bulk" name="sent_rece_bulk" value="1" class="left-label sent_receive_bulk modal-trigger modal-close">
							<label style="display: none;" class="checkboxx2 checkboxx-in " for="sent_rece_bulk"><span class="check-boxs"></span></label>      
						</div>

						<div class="col l4 s12 m12 bulk-action">
							<a style="display: none;" class='dropdown-button border-radius-6 btn-dropdown-action btn-bulk btn-default uppercase no-background' href='#' data-activates='dropdown004'>Bulk Actions <i class="arrow-icon"></i></a>
							<ul style="display: none;" id='dropdown004' class='dropdown-content'>
								<li><a id="email_multiple_billing_documents"><i class="dropdwon-icon icon email"></i>Email</a></li>
								<li><a id="download_multiple_invoices" data-multi_invdownload="0" style="display: flex;"><img class="icon-img" src="<?php echo base_url(); ?>public/icons/export.png" style="width: 15px;height: 22px">Download</a></li>
								<li><a id="print_multiple_billing_documents"><i class="material-icons">print</i>Print</a></li>
								<li><a id="deactive_multiple_invoices" data-multi_invoices="0" ><i class="material-icons">delete</i>Delete</a></li>
							</ul>
						</div>

						<div class="col l7 s12 m12 send-rec-tab">
							<div class="col l9 s12 m12"></div>

							<div class="col l2 s12 m12">
								<a id="recei_tab" href="#receive_request">RECEIVED</a>
							</div>

							<div class="col l1 s12 m12 recei-tab">
								<a id="sent_tab" class="active" href="#sent_request">SENT</a>
							</div>
						</div>
					</div> <!--End of Bulk Actions-->

					<div class="row" id="sent_request" style="margin-top:20px !important;"> <!--Requests Sent-->
						<div class="col l12 s12 m12">
							<div class="col l12 s12 m12">
								<?php if($result1_count == 0){ ?>
									<section><p class="text-center no-pending">THERE ARE NO PENDING INVITATIONS</p></section>
								<?php } ?>
								<?php for($i=0; $i<count($result1); $i++){ ?>

									<div class="col l6 s12 m12 white-box company-div">
										<div class="row">

											<div class="col s12 m12 l1 text-center org-info">	
												<input style="display: none;" type="checkbox" id="req_bulk_action1" name="req_bulk_action1" value="1" class="left-label sent_bulk_action">
												<label style="display: none;" class="label-bulk checkboxx2 checkboxx-in " for="req_bulk_action1"><span class="check-boxs"></span></label>
											</div>

											<?php
												if($result1[$i]['company_logo'] != '') {
													$company_logo = '<img src="'.DOC_ROOT_DOWNLOAD_PATH_CO.'company_logos/'.$result1[$i]['bus_id_connected_to'].'/'.$result1[$i]['company_logo'].'" height="50px" class="logo_style_2" width="50px">';
												} else {
													$company_logo = '<img src="'.DOC_ROOT_DOWNLOAD_PATH_CO.'company_logos/'.$result1[$i]['bus_id_connected_to'].'/'.$result1[$i]['company_logo'].'" height="50px" class="logo_style" width="50px">';
												}
											?>

											<div class="col s12 m12 l2 text-center org-info" style="width:13%">	
												<?php echo $company_logo; ?>
											</div>

											<div class="col s12 m12 l5 org-person" style="width:52%">
												<input type="hidden" name="conn_id1" id="conn_id1" value="<?php echo $result1[$i]['conn_id'];?>">
												<p class="org-name"><b><?php echo strtoupper($result1[$i]['company_name']); ?></b></p>
												<label style="color: #6e6e6e !important;" class="org-tagline"><?php echo strtoupper($result1[$i]['nature']??''); ?></label><br><br>
												<i class="material-icons view-icons com-loc">location_on</i><label style="color: #6e6e6e !important;" class="org-loc"><b><?php echo strtoupper($result1[$i]['name']??''); ?></b></label>
											</div>

											<div class="col s12 m12 l2 org-info">
												<a onclick="req_withdraw('<?=$result1[$i]["conn_id"]?>')" class="withdraw btn btn-theme btn-large">WITHDRAW</a>
											</div>

										</div>
									</div>
								<?php }?>
							</div>
						</div>
					</div> <!--End of Requests Sent-->

					<div class="row" id="receive_request" style="margin-top:20px !important;"> <!--Request Received-->
						<div class="col l12 s12 m12">
							<div class="col l12 s12 m12">
								<?php if($result1_count == 0){ ?>
									<section><p class="text-center no-pending">THERE ARE NO PENDING INVITATIONS</p></section>
								<?php } ?>
								<?php for($i=0; $i<count($result); $i++){ ?>
									<div class="col l6 s12 m12 white-box company-div">
										<div class="row">

											<div class="col s12 m12 l1 text-center org-info" style="width:7%">	
												<input style="display: none;" type="checkbox" id="req_bulk_action1" name="req_bulk_action1" value="1" class="left-label sent_bulk_action">
												<label style="display: none;" class="label-bulk checkboxx2 checkboxx-in " for="req_bulk_action1"><span class="check-boxs"></span></label>
											</div>

											<?php
												if($result[$i]['company_logo'] != '') {
													$company_logo = '<img src="'.DOC_ROOT_DOWNLOAD_PATH_CO.'company_logos/'.$result[$i]['bus_id_connected_from'].'/'.$result[$i]['company_logo'].'" height="50px" class="logo_style_2" width="50px">';
												} else {
													$company_logo = '<img src="'.DOC_ROOT_DOWNLOAD_PATH_CO.'company_logos/'.$result[$i]['bus_id_connected_from'].'/'.$result[$i]['company_logo'].'" height="50px" class="logo_style" width="50px">';
												}
											?>

											<div class="col s12 m12 l2 text-center org-info" style="width:13%">	
												<?php echo $company_logo; ?>
											</div>

											<div class="col s12 m12 l5 org-person" style="width:44%">	
												<p class="org-tagline"><b><?php echo strtoupper($result[$i]['company_name']??''); ?></b></p>
												<label style="color: #6e6e6e !important;" class="org-tagline"><?php echo strtoupper($result[$i]['nature']??''); ?></label><br><br>
												<i class="material-icons view-icons com-loc" style="margin: 0 0 0 -5px;">location_on</i><label style="color: #6e6e6e !important;" class="org-loc"><b><?php echo strtoupper($result[$i]['name']??''); ?></b></label>
											</div>

											<div class="col s12 m12 l3 text-center org-info" style="width:35%; padding:0px !important; margin-left:-20px;">	

												<input type="hidden" name="conn_id" id="conn_id" value="<?php echo $result[$i]['conn_id'];?>">
												<a style="width:40%; padding:0px !important; margin-right:5px;" onclick="req_accept('<?=$result[$i]["conn_id"]?>')" class="req_accept btn btn-theme btn-large">ACCEPT</a>

												<a style="width:40%; padding:0px !important;" onclick="req_decline('<?=$result[$i]["conn_id"]?>')" class="req_decline btn btn-theme btn-large">DECLINE</a>

											</div>

										</div>
									</div>
								<?php }?>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col l12 s12 m12"> <p> &nbsp; &nbsp; </p> </div>
					</div>
				</div>
			</div> <!--End of Page content-->
		</section> <!-- END CONTENT -->
	</div> <!-- END WRAPPER -->
</div> <!-- END MAIN -->


<script type="text/javascript">
	$(document).ready(function() {

		$("#receive_request").hide();

		$('#recei_tab').on('click', function(){
			$("#sent_request").hide();
			$("#receive_request").show();
			$("#recei_tab").addClass('active');
			$("#sent_tab").removeClass('active');
		});

		

		$('#sent_tab').on('click', function(){
			$("#sent_request").show();
			$("#receive_request").hide();
			$("#recei_tab").removeClass('active');
			$("#sent_tab").addClass('active');
		});


		

	}); 

	function req_withdraw(conn_id){
		if(csrf_hash===""){
			csrf_hash=csrf_hash;
		}
		$.ajax({

			dataType: "json",
			type: "post",
			url: base_url+'Community/withdraw_sent_request',

			data: {'csrf_test_name':csrf_hash,'conn_id': conn_id},

			success: function(data){
				console.log(data);
				if(data.csrf_hash){
					csrf_hash=data.csrf_hash;
				}
				$('#conn_id1').val('');

				if(data.status == true){
					Materialize.toast('Sent Request has been withdrawn successfully', 4000, 'green rounded');
					location.href=base_url+'community/connection-requests';
				} else {
					Materialize.toast('Some errors occurred, please try again...', 4000, 'red rounded');
				}
			}
		})

	}

	function req_accept(conn_id){
		if(csrf_hash===""){
			csrf_hash=csrf_hash;
		}
		$.ajax({

			dataType: "json",
			type: "post",
			url: base_url+'Community/change_accepted',

			data: {'csrf_test_name':csrf_hash,'accepted': 1, 'conn_id': conn_id},

			success: function(data){
				console.log(data);
				if(data.csrf_hash){
					csrf_hash=data.csrf_hash;
				}
				$('#conn_id').val('');

				if(data.status == true){
					Materialize.toast('Connection Request accepted successfully', 4000, 'green rounded');
					location.href=base_url+'community/my-connections';
				} else {
					Materialize.toast('Some errors occurred, please try again...', 4000, 'red rounded');
				}
			}
		})

	}

	function req_decline(conn_id){
		if(csrf_hash===""){
			csrf_hash=csrf_hash;
		}
		$.ajax({

			dataType: "json",
			type: "post",
			url: base_url+'Community/delete_request',

			data: {'csrf_test_name':csrf_hash,'conn_id': conn_id},

			success: function(data){
				console.log(data);
				if(data.csrf_hash){
					csrf_hash=data.csrf_hash;
				}
				$('#conn_id').val('');

				if(data.status == true){
					Materialize.toast('Connection Request has been declined successfully', 4000, 'green rounded');
					location.href=base_url+'community/my-connections';
				} else {
					Materialize.toast('Some errors occurred, please try again...', 4000, 'red rounded');
				}
			}
		})

	}

</script>

<?php $this->load->view('template/footer'); ?>