<style>
	.total-req{
		font-size:13px;
	}

	.request-div:hover{
		box-shadow: 0 1px 10px #7864e94d;
	}

	.request-div{
		height: 220px;
		width: 32%;
		margin: 10px;
		padding: 20px 10px;
		border-radius:5px;
	}

	.req_accept{
		background-color:#7864e9 !important;
		border:1px solid #ccc !important;
		color: white !important;
		height:35px !important;
		line-height:35px !important;
		margin:15px 0px;
	}

	.req_decline{
		background-color:#fff !important;
		border:1px solid #7864e9 !important;
		color: #7864e9 !important;
		height:35px !important;
		line-height:35px !important;
		margin:15px 0px;
	}

	.org-logo-req{
		margin-top:20px;
	}

	.top-all{
		text-align:right;
		font-size:12px;
		margin-top:30px;
		margin-right:35px;
	}
</style>

<?php 
	
?>

<div class="container custom content-div">

	<div class="row">
		<div class="col l12 s12 m12" style="margin: -40px 0 -10px 0px !important;">

			<div class="col l6 s12 m12">
				<p class="top-label">NEW CONNECTION REQUESTS  <label class="total-req">(<?=count($result)?>)</label></p>
			</div>

			<!--div class="col l6 s12 m12 right">
				<p class="top-all"><a href="<?php base_url(); ?>" id="view_sent_request" class="">View All</a></p>
			</div-->

		</div>
	</div>

	<div class="row">
		<div class="col l12 s12 m12">
			<div class="col l12 s12 m12">

				<?php for($i=0; $i<count($result); $i++) {?>
					<div class="col l4 s12 m12 white-box request-div1">
						<div class="row">
							<?php
								if($result[$i]['bus_company_logo'] != '') {
									$company_logo = '<img src="'.DOC_ROOT_DOWNLOAD_PATH_CO.'company_logos/'.$result[$i]['bus_id'].'/'.$result[$i]['bus_company_logo'].'" height="50px" class="logo_style_2" width="50px">';
								} else {
									$company_logo = '<img src="'.DOC_ROOT_DOWNLOAD_PATH_CO.'company_logos/'.$result[$i]['bus_id'].'/'.$result[$i]['bus_company_logo'].'" height="50px" class="logo_style" width="50px">';
								}
							?>
							
							<div class="col s12 m12 l4 org-logo-req">
								<div class="col s6 m6 l4" style="text-align: left">
									<?php echo $company_logo; ?>
								</div>
							</div>
							
							<div class="col s12 m12 l8">

								<input type="hidden" name="conn_id" id="conn_id" value="<?php echo $result[$i]['conn_id'];?>">

								<p class="org-name"><b><?php echo strtoupper($result[$i]['bus_company_name']); ?></b></p>

								<i class="material-icons view-icons com-loc">location_on</i><label class="org-loc"><?php echo strtoupper($result[$i]['name']); ?></label>

								<label class="org-tagline" style="margin: 0px 0px 0px 20px;"><?php echo strtoupper($result[$i]['nature_of_bus']); ?></label></br>


								<a style="width:40%; padding:0px !important; margin-right:5px;" onclick="req_accept('<?=$result[$i]["conn_id"]?>')" class="req_accept btn btn-theme btn-large">ACCEPT</a>
								
								<a style="width:40%; padding:0px !important;" onclick="req_decline('<?=$result[$i]["conn_id"]?>')" class="req_decline btn btn-theme btn-large">DECLINE</a>
							</div>
						</div>
					</div>
				<?php }?>
			</div>
		</div>
	</div>
</div>

<script>
	
	function req_accept(conn_id){

		$.ajax({

			dataType: "json",
			type: "post",
			url: base_url+'Community/change_accepted',

			data: {'csrf_test_name':csrf_hash,'accepted': 1, 'conn_id': conn_id},

			success: function(data){
				console.log(data);
				$('#conn_id').val('');

				if(data.status == true){
					Materialize.toast('Connection Request accepted successfully', 4000, 'green rounded');
					location.href=base_url+'community/my-connections';
				} else {
					Materialize.toast('Some errors occurred, please try again...', 4000, 'red rounded');
				}
			}
		})

	}

	function req_decline(conn_id){

		$.ajax({

			dataType: "json",
			type: "post",
			url: base_url+'Community/delete_request',

			data: {'csrf_test_name':csrf_hash,'conn_id': conn_id},

			success: function(data){
				console.log(data);
				$('#conn_id').val('');

				if(data.status == true){
					Materialize.toast('Connection Request has been declined successfully', 4000, 'green rounded');
					location.href=base_url+'community/my-connections';
				} else {
					Materialize.toast('Some errors occurred, please try again...', 4000, 'red rounded');
				}
			}
		})

	}
	
</script>