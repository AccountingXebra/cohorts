<?php $this->load->view('my-community/Cohorts-header');?>
<style type="text/css">
  .green-bg-right:before {
      top: -39px !important;
      width: 270px;
   }
   .green-bg-right:after {
      width: 270px;
   }

   .act_details{
	   font-size:11px !important;
	   color:white !important;
   }

   .btn-welcome {
    width: 120px;
    height: 44px;
    font-size: 14px;
    padding: 5px 0 !important;
    text-align: center;
    font-weight: 500;
    border-radius: 8px;
    position: relative;
    letter-spacing: 0.40px;
}

   [type="radio"]:not(:checked) + label, [type="radio"]:checked + label {
    position: relative;
    padding-left: 20px;
    cursor: pointer;
    display: inline-block;
    height: 25px;
    line-height: 25px;
    font-size: 1rem;
    -webkit-transition: .28s ease;
    transition: .28s ease;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
}

[type="radio"] + label:before, [type="radio"] + label:after {
    content: '';
    position: absolute;
    left: 0;
    top: 0;
    margin: 4px 0 0 0;
    width: 16px;
    height: 16px;
    z-index: 0;
    -webkit-transition: .28s ease;
    transition: .28s ease;
}

@media only screen and (min-width: 993px) {
.row .col.l6 {
    width: 50%;
    margin-left: auto;
    left: auto;
    right: auto;
    margin-bottom: 20px;
}
}
body.easy-tab1 [type="radio"] + label:before, [type="radio"] + label:after {
    content: '';
    position: absolute;
    left: 0;
    top: 0;
    margin: 3px 0 0 -2px;
    width: 20px;
    height: 20px;
    z-index: 0;
    -webkit-transition: .28s ease;
    transition: .28s ease;
}

   .act_data{
	   font-size:13px !important;
	   color:white !important;
   }

   #my_activity th{
	   color:#808080 !important;
	   font-size:13px;
   }

   #my_activity td{
	   font-size:12px;
	   padding: 15px 12px !important;
   }

   #my_activity thead{
	   border-bottom:none !important;
   }

   #my_activity{
	    border-collapse:separate;
		border-spacing:0px 5px;
   }

   .company_name{
	   font-size:15px !important;
	   font-weight:500 !important;
	   /*color:#000000 !important;*/
   }

   .logo_co{
	   margin-bottom:-10px !important;
   }

   .nature_buzz{
	   font-size:13px !important;
   }

   #send-mail-comp{
	   background: white !important;
	   color:#000000;
	   width:120px !important;
	   border:1px solid #7864e9 !important;
	   font-size:14px !important;
   }

   #connect-to-comp{
	   width:150px !important;
	   font-size:14px !important;
   }

   #connect-to-comp:active {
		background: #7864e9 !important;
   }

   .cmp_info{
	   font-size:13px !important;
   }

   .cmp_details{
	   font-size:13px !important;
	   color:#000000 !important;
   }

	.loc-add{
		margin-left:16px !important;
		width:40px !important;
	}

	.view-label{
		font-size:13px !important;
	}

	.view-content{
		font-size:13px !important;
		color:#000000 !important;
	}

	.select_loc{
		padding-left:10px;
		padding-right:10px;
		padding-top:12px;
		padding-bottom:12px;
		text-align:center;
		width:15px !important;
		height:15px !important;
		color:#1a1a1a !important;
		background-color:#f0f5f5 !important;
		border-radius:18px;
		font-size:15px;
	}

	#cke_1_contents{
		height:120px !important;
	}

	label.fill_details{
		font-size:0.875rem !importantl
	}

	.filed_name{
		font-size:14px !important;
		font-weight:500;
	}

	input[type="radio"]+span:before, input[type="radio"]+span:after {
    content: '';
    position: absolute;
    left: -15px !important;
    top: 4px !important;
    margin: 0px 0px 0px 31px;
    width: 16px;
    height: 16px;
    z-index: 0;
    -webkit-transition: .28s ease;
    transition: .28s ease;
}

 .modal-body{
   margin:0px !important;
   width:100%;
 }

 .modal-header {
   padding: 30px;
 }

 #mail_sub{
   background-color: #7864e9 !important;
   height:42px !important;
   padding: 0px 2rem !important;
   font-size:13px;
 }

 #mailto_id{
   border:1px solid #e0e0d1 !important;
   height:2rem !important;
   margin:0 0 8px 0 !important;
 }

 #subject{
   border:1px solid #e0e0d1 !important;
   height:2rem !important;
   margin:0 0 0px 0 !important;
 }

 #sr_cc_mailto{
   border:1px solid #e0e0d1 !important;
   height:2rem !important;
   margin:0 0 0px 0 !important;
 }

 #message{
   border:1px solid #e0e0d1 !important;
 }

 #mail_close{
   background-color:white;
   color:#c2c2a3;
   font-size:13px;
 }

 .filed_name{
   font-size:14px;
   color:#595959;
 }

 #cke_1_contents{
   height:120px !important;
 }
</style>
    <!-- START MAIN -->
    <div id="main" style="padding-left:0px !important;">
      <!-- START WRAPPER -->
      <div class="wrapper">
        <!-- START LEFT SIDEBAR NAV-->
        <?php //$this->load->view('template/sidebar.php');?>
        <!-- END LEFT SIDEBAR NAV-->
        <!-- //////////////////////////////////////////////////////////////////////////// -->
        <!-- START CONTENT -->
          <section id="content" class="bg-theme-gray">
            <div class="container">
              <div class="plain-page-header">
                <div class="row">
                  <div class="col l6 s12 m6">
                    <a class="go-back underline" href="javascript:window.history.go(-1);">Back to My Marketplace</a>
                  </div>
                  <div class="col l6 s12 m6">
                    <div class="right-2">

                    </div>

                  </div>
                </div>
              </div>
            </div>
            <div class="container">
				<div class="row">
					<div class="col s12 m12 l12"><!--before-red-bg red-bg-right-->
						<div class="col l12 m12 s12 view-box white-box">
							<div class="row">
								<div class="col l12 m12 s12" style="text-align:right; padding-top:20px !important; padding-right:40px !important;">
									<a href="<?php echo $result[0]['facebook']; ?>" target="_blank"><img width="20" height="20" src="<?php echo base_url(); ?>asset/css/img/icons/facebook.png" alt="Logo"/></a>
									<a href="<?php echo $result[0]['twitter']; ?>" target="_blank"><img width="20" height="20" src="<?php echo base_url(); ?>asset/css/img/icons/twitter.png" alt="Logo"/></a>
									<a href="<?php echo $result[0]['linkedin']; ?>" target="_blank"><img width="20" height="20" src="<?php echo base_url(); ?>asset/css/img/icons/linkedin.png" alt="Logo"/></a>
									<a href="<?php echo $result[0]['googleplus']; ?>" target="_blank"><img width="20" height="20" src="<?php echo base_url(); ?>asset/css/img/icons/instagram.png" alt="Logo"/></a>
								</div>
							</div>

							<div class="row">
								<div class="col l12 m12 s12" style="text-align:center;">
									<?php if($result[0]['company_logo'] !=""){ ?>
									    <img width="100" height="100" src="<?php echo DOC_ROOT_DOWNLOAD_PATH_CO ?>company_logos/<?php echo $result[0]['code'].'/'.$result[0]['company_logo']; ?>" alt="Logo" class="logo_co"></img>
									<?php } else { ?>
                                    	<img src="<?= base_url(); ?>asset/css/img/icons/company-icon.png" height="50px" class="logo_style_2" width="50px"></img>
                                    <?php } ?><br><br>
									<label class="company_name"><b><?php echo strtoupper($result[0]['company_name']); ?></b></label>
								</div>
								<div class="col l12 m12 s12" style="text-align:center;">
									<label class="nature_buzz"><?php echo strtoupper($result[0]['nature']); ?></label>
								</div>
								<div class="col l12 m12 s12" style="text-align:center; margin-bottom:20px !important;">
									&nbsp&nbsp&nbsp
								</div>
								<div class="col l12 m12 s12" style="text-align:center;">
									<!--a style="margin-top:0px !important;" id="send-mail-comp" class="btn btn-welcome modal-trigger modal-close" href="#send_mailto_comp_modal">SEND MAIL</a-->

									<?php if(count($offers) > 0) {?>
										<a style="margin-top:0px !important;" id="send-mail-comp" class="btn btn-welcome modal-trigger modal-close" href="<?php echo base_url(); ?>community/company-deals?token=<?php echo $result[0]['code']; ?>">DEALS</a>
									<?php }?>

									<a style="margin-top:0px !important;" id="send-mail-comp" class="btn btn-welcome modal-trigger modal-close" href="#email_modal<?=$result[0]['code']?>">SEND MAIL</a>


									<?php
									$bus_id = $this->user_session['bus_id'];
									$connections = $this->Community_model->selectData('connections', '*',  array('bus_id' => $bus_id, 'bus_id_connected_to' => $result[0]['code']));

									if(count($connections)>0){
										//print_r($connections);
										if($connections[0]->accepted == 0 && $connections[0]->status == 1){ ?>
											<a style="margin-top:0px !important;" class="btn btn-welcome">REQUESTED</a>
										<?php } elseif ($connections[0]->accepted == 1 && $connections[0]->status == 1) { ?>
											<a style="margin-top:0px !important;"  class="btn btn-welcome">CONNECTED</a>
										<?php } else {?>
											<a style="margin-top:0px !important;" id="send-mail-comp" class="btn btn-welcome modal-trigger modal-close" href="#connect_modal<?=$result[0]['code']?>">CONNECT</a>
										<?php }
									}else{?>
										<a style="margin-top:0px !important;" id="send-mail-comp" class="btn btn-welcome modal-trigger modal-close" href="#connect_modal<?=$result[0]['code']?>">CONNECT</a>
									<?php } ?>

									<div class="col l11 m12 s12 border-bottom" style="margin-left:11px !important; width:98% !important; margin-top:30px !important;"></div>


								</div>
								<!--Bottom Border -->

							</div>

<!-- Send Email Modal-->
<div id="email_modal<?=$result[0]['code'];?>" class="modal modal-md modal-header email-modal" style="padding-top:21px !important;">
	<div style="margin-top:-9px;">
		<a class="modal-close close-pop"><img src="<?php echo base_url(); ?>asset/images/popupdelete1.png" alt="delete" style="margin: 0 0 0 20px !important;"></a>
		<p style="font-weight: bold; font-size:16px; text-align: center;">Send Email to <?php echo strtoupper($result[0]['company_name']); ?></p>
	</div>

	<div class="modal-body">
		<form id="mail_frm" name="mail_to" class="" method="POST" accept-charset="utf-8" novalidate="true">
			<div class="row" style="margin-top:20px;">
				<?php $csrf = array(
					'name' => $this->security->get_csrf_token_name(),
					'hash' => $this->security->get_csrf_hash()
				);
				?>
				<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
				<div class="col s12 m12 l12">
					<div class="col s12 m12 l12 input-group">
						<span class="filed_name"><b> To:</b><span class="required_field">
						<input class="form-control" name="email_id" id="mailto_id" type="text" placeholder="" value="<?=$email[0]['reg_email']?>">
					</div>
				</div>

				<div class="col s12 m12 l12">
					<div class="col s12 m12 l12 input-group">
						<span class="filed_name"><b> Cc:</b></span>
						<input class="form-control" name="sr_cc_mail" id="sr_cc_mailto" type="text" placeholder="">
					</div>
				</div>

				<div class="col s12 m12 l12 login_form_textbox">
					<div class="col s12 m12 l12 input-group" style="margin-top:10px;">
						<span class="filed_name"><b> Subject<span class="required_field">*</span>:</b></span>
						<input class="form-control" name="subject" id="subject" type="text" placeholder="">
					</div>
				</div>

				<div class="col s12 m12 l12 login_form_textbox">
					<div class="col s12 m12 l12 input-group" style="margin-top:10px;">
						<span class="filed_name"><b> Message<span class="required_field">*</span>:</b></span>
						<textarea style="height:75px !important;" name="message" class="form-control" rows="7" id="message" placeholder="ENTER MESSAGE" style="font-size:13px;"></textarea>
					</div>
				</div>
				
				<input type="hidden" id='bus_id' name="bus_id" value="<?php echo $result[0]['bus_id']; ?>">

				<div class="col s12 m12 l12" style="margin: 0 0 0 -10px !important;">
					<div class="form-group login_btn_1" style="text-align:right; margin-top:15px;">
						<button type="button" id="mail_close" class="btn btn-flat theme-primary-btn theme-btn theme-btn-large modal-close" data-dismiss="modal" style="color: black !important;">CANCEL</button>
						<input type="submit" id="mail_sub" class="btn" value="SEND" name="mail_submit">
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
<!-- End of Send Email Modal-->

<!-- New Connection Modal -->
<div id="connect_modal<?=$result[0]['code']?>" class="modal modal-md modal-rating connection" style="height:auto; width: 40%; overflow:hidden;">
	<div class="modalheader">
		<h4 style="float: left; text-align: left !important;">Connection Box</h4>
		<a class="modal-close close-pop"><img src="<?php echo base_url();?>asset/images/popupdelete1.png"></a>
	</div>

	<hr style="border: 0.5px solid #ccc;">

	<div class="modalbody">
		<form class="addconn" id="add_connection_info" name="add_connection_info">
			<?php $csrf = array(
					'name' => $this->security->get_csrf_token_name(),
					'hash' => $this->security->get_csrf_hash()
				);
				?>
				<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
			<div class="row" style="margin-bottom:0px !important;">
				<div class="col l12 s10 m10 fieldset">
					<div class="row">
						<div class="col l12 s12 m12 fieldset org_details" style="text-align: center;">
							<p class="conn-org" style="text-align: center !important;"><b><?php echo $result[0]['company_name']; ?></b></p>
							<label class="conn-org-tagline" style="font-size: 14px;">THIS COMPANY IS CONNECTED WITH ME AS</label>
							<input type="text" hidden name="con_to" id="con_to<?=$result[0]['code']?>" value="<?=$result[0]['code']?>">
						</div>
						<div class="col l12 s12 m12 fieldset org_details radio-list" style="margin-top: 25px;">
							<div class="col l2 s10 m10 fieldset"></div>
							<div class="col l2 s12 m12 fieldset">
								<input type="radio" class="form-check-input" id="con-client<?=$result[0]['code']?>" name="connect" value="client" checked>
								<label class="form-check-label" for="con-client<?=$result[0]['code']?>">CLIENT</label>
							</div>
							<div class="col l2 s12 m12 fieldset">
								<input type="radio" class="form-check-input" id="con-vendor<?=$result[0]['code']?>" name="connect" value="vendor">
								<label class="form-check-label" for="con-vendor<?=$result[0]['code']?>">VENDOR</label>
							</div>
							<div class="col l2 s12 m12 fieldset">
								<input type="radio" class="form-check-input" id="con-both<?=$result[0]['code']?>" name="connect" value="both">
								<label class="form-check-label" for="con-both<?=$result[0]['code']?>">BOTH</label>
							</div>
							<div class="col l2 s12 m12 fieldset">
								<input type="radio" class="form-check-input" id="con-none<?=$result[0]['code']?>" name="connect" value="none">
								<label class="form-check-label" for="con-none<?=$result[0]['code']?>">NONE</label>
							</div>
							<div class="col l2 s10 m10 fieldset"></div>
						</div>
					</div>

					<div class="row">
						<div class="col l11 s12 m12" style="margin-left: 3.333333%; margin-top: 20px;">
							<div class="row notess note-mor">
								<div class="s12 m12 l12">
									<div class="note">
										<label for="message"></label>
										<textarea id="message<?=$result[0]['code']?>" placeholder="MESSAGE" type="text" name="message" class="bill-box ex-noss materialize-textarea ratingtextbox" style="height: 54px;min-height: 6rem;margin-bottom:0;"></textarea>
									</div>
								</div>
							</div>
						</div>
					</div>

				</div>

				<div class="row" style="padding:3% 0;">
					<div class="col l6 m6 s12 fieldset"></div>
					<div class="col l6 m6 s12 fieldset buttonset" style="margin: 10px 0 0 -7px !important;">
						<div class="right">
							<button type="button" class="connecting modal-close btn-flat theme-primary-btn theme-btn theme-btn-large right" onclick="con_company('<?php echo $result[0]['code'];?>')">CONNECT</button>
							<button type="button" class="modal-close btn-flat theme-flat-btn theme-btn theme-btn-large modal-close">CANCEL</button>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div> <!-- End of New Connection Modal -->

							<?php

								if($result[0]['incorporation_date'])
								{
									$timestamp = strtotime($result[0]['incorporation_date']);
									$inc_date =  date('d F, Y', $timestamp);

								}else{
									$inc_date = '';
								}

								if($result[0]['company_size'])
								{
									$no_of_employee = $result[0]['company_size'];

								}else{

									$no_of_employee = '';
								}



								if($result[0]['company_revenue'])
								{
									if($result[0]['company_revenue'] ==  5 )
									{
										$revenue = '> '.$result[0]['company_revenue'].' CRORES';
									}
									if($result[0]['company_revenue'] ==  100)
									{
										$revenue = $result[0]['company_revenue'].' CRORES +';

									}else{

										$revenue = $result[0]['company_revenue'].' CRORES';
									}


								}else{
									$revenue = '';
								}
							?>
							<?php
                                   if(count($connections)>0){
								if($connections[0]->client == 1 && $connections[0]->vendor == 1) {
									$type = "BOTH";
								}
								elseif($connections[0]->client == 1 && $connections[0]->vendor == 0) {
									$type = "CLIENT";
								}
								elseif($connections[0]->client == 0 && $connections[0]->vendor == 1) {
								}
								elseif($connections[0]->client == 0 && $connections[0]->vendor == 0) {
									$type = "NONE";
								}
								elseif($connections[0]->client == "" && $connections[0]->vendor == "") {
									$type = "";
								}
							}
							?>

							<div class="row">
									<div class="col l12 m12 s12" style="margin-top: 20px;">
										<div class="col l1 m1 s1"></div>

										<?php if($inc_date != '') {?>
										<div class="col l3 m3 s3" style="text-align: -webkit-center;">
											<label class="cmp_info">CONNECTED AS</label><br>
											<label class="cmp_details" style="text-align: center;"><b><?php echo strtoupper($inc_date); ?></b></label>
										</div>
										<?php }?>

										<?php if($inc_date != '') {?>
										<div class="col l3 m3 s3" style="text-align: -webkit-center;">
											<label class="cmp_info">DATE OF INCORPORATION</label><br>
											<label class="cmp_details" style="text-align: center;"><b><?php echo strtoupper($inc_date); ?></b></label>
										</div>
										<?php }?>

										<?php if($no_of_employee != '') {?>
										<div class="col l2 m2 s2" style="text-align: -webkit-center;">
											<label class="cmp_info">NO OF EMPLOYEES</label><br>
											<label class="cmp_details"><b><?php echo $no_of_employee; ?></b></label>
										</div>
										<?php }?>

										<?php if($revenue != '') {?>
										<div class="col l2 m2 s2" style="text-align: -webkit-center;">
											<label class="cmp_info">REVENUE</label><br>
											<label class="cmp_details"><b> <?php echo $revenue; ?></b></label>
										</div>
										<?php }?>
									</div>
								</div>




						</div>
					</div>
				</div>
			</div>

				<!-- Address Details Start --->
				<div class="row">
					<div class="col s12 m12 l12">
					<div class="col s12 m12 l12">
					<div class="col s12 m12 l12">
					<div class="col s12 m12 l12">
						<div class="col l12 m12 s12 view-box white-box before-red-bg red-bg-right">
							<div class="row">
								<div class="col l12 m12 s12">
									<h6 class="white-box-title" style="padding: 11px 34px !important">Address Details</h6>
								</div>
							</div>
							<?php

							if($result[0]['billing_address'])
							{
								$address = $result[0]['billing_address'].', '.$result[0]['name'].', '.$result[0]['state_name'].', '.$result[0]['country_name'].' '.$result[0]['billing_zipcode'];
							}

							?>
							<div class="row">
								<div class="col l6 m6 s6">
									<div class="col l12 s12 m12">
										<div class="col l1 s1 m1 loc-add"><i class="material-icons view-icons">location_on</i></div>
										<div class="col l10 s10 m10">
											<span class="view-label">BILLING ADDRESS</span>
											<p class="view-content"><?php echo $address; ?> </p>
										</div>
									</div>
									<div class="col l12 s12 m12" style="margin:20px 0px 0px 55px !important; ">
										<div class="col l4 s4 m4">
											<label class="cmp_info">STATE</label><br>
											<label class="cmp_details"><b> <?php echo $result[0]['state_name']; ?> </b></label>
										</div>
										<div class="col l4 s4 m4">
											<label class="cmp_info">COUNTRY</label><br>
											<label class="cmp_details"><b> <?php echo $result[0]['state_name']; ?> </b></label>
										</div>
									</div>
								</div>
								<?php //echo '<pre>'; print_r($branch); echo '</pre>'; ?>
								<?php

								for($i=0; $i<count($branch); $i++)
								{
									?>
									<div class="col l6 m6 s6">
										<div class="col l12 s12 m12">
											<div class="col l1 s1 m1 loc-add"><i class="material-icons view-icons">location_on</i></div>
											<div class="col l10 s10 m10">
												<span class="view-label">BRANCH ADDRESS</span>
												<p class="view-content"> <?php echo $branch[$i]['address'].', '. $branch[$i]['name'].', '. $branch[$i]['state_name'].', '. $branch[$i]['country_name'] ?> </p>
											</div>
										</div>
										<div class="col l12 s12 m12" style="margin:20px 0px 0px 55px !important; ">
											<div class="col l4 s4 m4">
												<label class="cmp_info">STATE</label><br>
												<label class="cmp_details"><b> <?php echo $branch[$i]['state_name']; ?> </b></label>
											</div>
											<div class="col l4 s4 m4">
												<label class="cmp_info">COUNTRY</label><br>
												<label class="cmp_details"><b> <?php echo $branch[$i]['country_name']; ?> </b></label>
											</div>
										</div>
									</div>
									<?php
								}

								?>

							</div>
						</div>
					</div>
					</div>
					</div>
					</div>
				</div>
				<!-- Address Details End --->

				<div class="row">
					<div class="col s12 m12 l12">
					<div class="col s12 m12 l12">
					<div class="col s12 m12 l12">
					<div class="col s12 m12 l12">
						<!-- Service Offer Start --->	<!-- before-red-bg red-bg-right -->
						<div class="col l6 m6 s6 view-box white-box" style="width:40% !important; margin-right:20px !important;">
							<div class="row">
								<div class="col l12 m12 s12">
									<h6 class="white-box-title" style="padding: 11px 34px !important"> Services Offered</h6>
								</div>
							</div>

							<div class="row" style="width:94% !important;">
								<div class="col l12 s12 m12" style="margin-left:32px !important; margin-bottom:20px !important;">
									<?php
									$services = explode("|@|",$result['0']['services_keywords']);
									for($i=0;$i<count($services);$i++)
									{
										if($services[$i])
										{
											?>
											<div class="col l6 s6 m6" style="height:40px !important;">
												<label class="select_loc"> <?php echo strtoupper($services[$i]); ?> </label>
											</div>
											<?php
										}
									}
									?>

								</div>
							</div>
						</div>
						<!-- Service Offer End --->

						<!-- Web Presence Start --->
						<div class="col l3 m3 s3 view-box white-box" style="width:30% !important; margin-right:20px !important;">
							<div class="row">
								<div class="col l12 m12 s12">
									<h6 class="white-box-title" style="padding: 11px 34px !important">Web Presence</h6>
								</div>
							</div>
							<div class="row">
								<div class="col l12 s12 m12" style="margin-left:32px !important; margin-bottom:20px !important;">
									<label class="cmp_info">WEBSITE URL</label><br>
									<label class="cmp_details"><b> <?php echo $result[0]['website_url']; ?> </b></label>
								</div>
							</div>
							<div class="row">
								<div class="col l12 s12 m12" style="margin-left:32px !important; margin-bottom:20px !important;">
									<label class="cmp_info"> CLIENTS </label><br>
									<label class="cmp_details"><b> <?php  ?> </b></label>
								</div>
							</div>
							<div class="row">
								<div class="col l12 s12 m12" style="margin-left:32px !important;">
									<label class="cmp_info">CASE STUDIES</label><br>
									<label class="cmp_details"><b> <?php echo $result[0]['case_study']; ?></b></label>
								</div>
							</div>
						</div>
						<!-- Web Presence End --->

						<!-- Contact Details Start --->
						<div class="col l3 m3 s3 view-box white-box" style="width:25% !important;">
							<div class="row">
								<div class="col l12 m12 s12">
									<h6 class="white-box-title" style="padding: 11px 34px !important">Contact Details</h6>
								</div>
							</div>

							<div class="row">
								<div class="col l12 s12 m12" style="margin-left:32px !important; margin-bottom:20px !important;">
									<label class="cmp_info">CONTACT PERSON</label><br>
									<?php if(count($contact)>0) {?>
										<label class="cmp_details"><b> <?php echo $contact[0]['reg_username']; ?>  </b></label>
									<?php }?>
								</div>
							</div>
							<div class="row">
								<div class="col l12 s12 m12" style="margin-left:32px !important; margin-bottom:20px !important;">
									<label class="cmp_info"> CONTACT NUMBER </label><br>
									<?php if(count($contact)>0) {?>
									<label class="cmp_details"><b>  <?php echo $contact[0]['reg_mobile']; ?> </b></label>
									<?php }?>
								</div>
							</div>
							<div class="row">
								<div class="col l12 s12 m12" style="margin-left:32px !important;">
									<label class="cmp_info">EMAIL ID</label><br>
									<?php if(count($contact)>0) {?>
									<label class="cmp_details"><b>  <?php echo $contact[0]['reg_email']; ?> </b></label>
									<?php }?>
								</div>
							</div>
						</div>
						<!-- Contact Details End --->
					</div>
					</div>
					</div>
					</div>
				</div>

				<?php if(count($offers) >= 1) {?>
	 				<?php $this->load->view('my-community/offers-on-profile'); ?>
	 			<?php }?>

				</div>
        </section>
    </div>
    </div>


	<div id="send_mailto_comp_modal" class="modal modal-md modal-display" style="max-width:600px !important;">
		<div class="modalheader border-bottom" style="padding-bottom:15px; padding-top:25px !important; text-align:left;">
			<h4 style="padding-left:0px; font-size:20px !important;	">Send Email</h4>
			<a class="modal-close close-pop" style="margin-right:-15px !important;"><img src="<?php echo base_url();?>asset/images/popupdelete1.png" alt="delete"></a>
		</div>
		<div class="modalbody">
			<div class="choose_subscription" style="margin-top:15px;">
				<form class="" id="send_emailto_form_form" name="emailto_form" method="post">
					<?php $csrf = array(
					'name' => $this->security->get_csrf_token_name(),
					'hash' => $this->security->get_csrf_hash()
				);
				?>
				<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
					<div class="row border-bottom">
						<div class="col s12 m12 l12 login_form_textbox">
							<div class="col s12 m12 l12" style="margin-top:5px !important; margin-bottom:5px !important;">
								<span class="filed_name"> Send a message to <?php echo $result[0]['bus_company_name']; ?> </span>
							</div>
							<div class="col s12 m12 l12 input-group" style="margin-top:5px;">
								<textarea style="height:75px !important;" id="message" name="message" class="form-control" rows="7" id="message" placeholder="ENTER MESSAGE" style="font-size:13px;"required></textarea>
							</div>
							<div class="col s12 m12 l12" style="margin-top:5px !important; margin-bottom:20px !important;">
								<label class="fill_details"> Please enter your name, email id, phone number, so that service provider can reply you </label>
							</div>
						</div>
					</div>
					<input type="hidden" id='bus_id' name="bus_id" value="<?php echo $result[0]['bus_id']; ?>">
					<div class="row" style="margin-bottom:20px !important; margin-top:0px !important;">
						<div class="col l12 s12 m6"></div>
						<div class="col l12 s12 m6 fieldset" style='text-align:right; margin-left:-20px;'>
							<input id="tocomp_email_close" type="button" class="btn btn-default modal-close" data-dismiss="modal" style="font-size:12px; color:black;" value="CANCEL"></input>
							<input id="tocomp_email_proceed" type="submit" class="btn btn-default" data-dismiss="modal" style="font-size:12px; background-color:#7864e9; color:white;" value="SEND"></input>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>

	<script src="<?= base_url(); ?>asset/ckeditor/ckeditor.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {


		$("#send_emailto_form_form").submit(function(e){
				e.preventDefault();
				for ( instance in CKEDITOR.instances ) {
			        CKEDITOR.instances[instance].updateElement();
			    }
			}).validate({

				rules:{

					message:{
						required:true,
					},

				},

				messages:{

					message:{
						required:"Message is required",
					},

				},
				submitHandler:function(form){

						var bus_id = $('#bus_id').val();
						var email = $('#email').val();
						var message = $('#message').val();

						//$('#tocomp_email_proceed').prop('disabled', true);

								$.ajax({
								url:base_url+'community/email_company',
								type:"POST",
								data:{
										'csrf_test_name':csrf_hash,
										"bus_id":bus_id,
										"email":email,
										"message":message,
									 },
								success:function(res){

										if(res == 1)
										{
											$("#message").val('');
											$('#send_mailto_comp_modal').modal('close');
											$('#tocomp_email_proceed').prop('disabled', false);
											Materialize.toast('Email has been sent.', 2000,'green rounded');
										}
										else
										{
											$("#message").val('');
											$('#send_mailto_comp_modal').modal('close');
											$('#tocomp_email_proceed').prop('checked', false);
											Materialize.toast('Error. Email was not sent.', 2000,'red rounded');
										}
									},
					});
				},
			});


		CKEDITOR.replace( 'message', { toolbar : 'Basic' });
		var ce_type=$('#ce_type').val();
		$('#Subject').val('');
		CKupdate();
		setTimeout(function(){
		if(csrf_hash===""){
			csrf_hash=csrf_hash;
		}
		console.log(data);
		$.ajax({
			 url:base_url+'customise_emails/get_customise_email',
			 type:"POST",
			 data:{'csrf_test_name':csrf_hash,"ce_type":ce_type},
			 success:function(res){
					 var data = JSON.parse(res);
					 console.log(data);
					 //alert(data);
					 //alert(ce_type);
					if(data['data'].length != 0){
						$('#subject').val(data[0].ce_subject);
						//$('#ce_id').val(data[0].ce_id);
						CKEDITOR.instances['message'].setData(data[0].ce_message);
					}
					if(data['csrf_hash']){
						csrf_hash=data['csrf_hash'];
					}
				},
		});
		}, 8000);
		});

		function CKupdate(){
			for ( instance in CKEDITOR.instances ){
				CKEDITOR.instances[instance].updateElement();
				CKEDITOR.instances[instance].setData('');
		   }
		}
	</script>




<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
 <script>
 $(document).ready(function($) {

   $("#mail_frm").submit(function(e){

       e.preventDefault();
       for ( instance in CKEDITOR.instances ) {
             CKEDITOR.instances[instance].updateElement();
         }
     }).validate({

       rules:{
         email_id:{
           required:true,
           //email:true,
           multiemail:true,
         },
         subject:{
           required:true,
         },
         message:{
           required:true,
         },
         sr_cc_mailto:{
           multiemail:true,
         },

       },

       messages:{
         email_id:{
           required:"Email address is required",
           email:"Invlid Email"
         },
         subject:{
           required:"Subject is required",
         },
         message:{
           required:"Message is required",
         },
       },
       submitHandler:function(form){

           var bus_id = $('#bus_id').val();
           var mailto_id = $('#mailto_id').val();
           var subject = $('#subject').val();
           var message = $('#message').val();
           var cc_mail = $('#sr_cc_mailto').val();
           $('.btn').prop('disabled', true);
				if(csrf_hash===""){
					csrf_hash=csrf_hash;
				}
				$.ajax({
				url:base_url+'community/email_company',
				type:"POST",
				data:{
					'csrf_test_name':csrf_hash,
                   "bus_id":bus_id,
                   "email_to":mailto_id,
                   "email_subject":subject,
                   "email_message":message,
                   "email_cc":cc_mail,
                },
				success:function(res){
					var myArray = res.split("@");
					if(myArray[1]){
						csrf_hash=myArray[1];
					}
										if(myArray[0] == 1)
										{
											$("#message").val('');
											$('.email-modal').modal('close');
											$('#tocomp_email_proceed').prop('disabled', false);
											Materialize.toast('Email has been sent.', 2000,'green rounded');
										}
										else
										{
											$("#message").val('');
											$('.email-modal').modal('close');
											$('#tocomp_email_proceed').prop('checked', false);
											Materialize.toast('Error. Email was not sent.', 2000,'red rounded');
										}
									},
         });
       },
     });

   jQuery.validator.addMethod(
       "multiemail",
        function(value, element) {
            if (this.optional(element)) // return true on optional element
                return true;
            var emails = value.split(/[;,]+/); // split element by , and ;
            valid = true;
            for (var i in emails) {
                value = emails[i];
                valid = valid &&
                        jQuery.validator.methods.email.call(this, $.trim(value), element);
            }
            return valid;
        },

       jQuery.validator.messages.email
   );

 });

 </script>


<script src="<?= base_url(); ?>asset/ckeditor/ckeditor.js"></script>
<script type="text/javascript">


$(document).ready(function() {
	CKEDITOR.replace( 'message', { toolbar : 'Basic' });
	var ce_type=$('#ce_type').val();
	$('#Subject').val('');
	CKupdate();
	setTimeout(function(){
	if(csrf_hash===""){
		csrf_hash=csrf_hash;
	}
	$.ajax({
         url:base_url+'customise_emails/get_customise_email',
         type:"POST",
         data:{'csrf_test_name':csrf_hash,"ce_type":ce_type},
         success:function(res){
                 var data = JSON.parse(res);
				 console.log(data);
                 //alert(data);
                 //alert(ce_type);
				if(data['data'].length != 0){
					$('#subject').val(data[0].ce_subject);
                    //$('#ce_id').val(data[0].ce_id);
                    CKEDITOR.instances['message'].setData(data[0].ce_message);
                }
				if(data['csrf_hash']){
					csrf_hash=data['csrf_hash'];
				}
            },
   });
	}, 8000);

});

function CKupdate(){
   for ( instance in CKEDITOR.instances ){
       CKEDITOR.instances[instance].updateElement();
       CKEDITOR.instances[instance].setData('');
  }
}

function con_company(id) {

		var con_client 	= $('#con-client'+id).val();
		var con_vendor 	= $('#con-vendor'+id).val();
		var con_both 	= $('#con-both'+id).val();
		var con_none 	= $('#con-none'+id).val();
		var messages	= $('#message'+id).val();
		var con_to_id	= $('#con_to'+id).val();
		if(csrf_hash===""){
			csrf_hash=csrf_hash;
		}	
		$.ajax({

			dataType: 'json',
			type: "POST",
			url:base_url+'Community/add_connection_info',

			data:{'csrf_test_name':csrf_hash,'client':con_client,'vendor':con_vendor,'message':messages, 'bus_id_connected_to':con_to_id},

			success: function(data){
				if(data['csrf_hash']){
					csrf_hash=data['csrf_hash'];
				}
				$('#connect_modal'+id).modal('close');

				$('#con-client'+id).val('');
				$('#con-vendor'+id).val('');
				$('#con-both'+id).val('');
				$('#con-none'+id).val('');
				$('#message'+id).val('');
				$('#con_to'+id).val('');

				Materialize.toast('Your connection request has been sent', 2000, 'green rounded');
			}
		});
	}

</script>

<?php $this->load->view('template/footer.php');?>
