 <?php $this->load->view('my-community/Cohorts-header');?>

    <!-- END HEADER -->
    <style type="text/css">
    .add-new {
		margin: 0 0 0 0 !important;
    }
	.section_li {
		margin: 0 0 0 0 !important;
    }
    .categories>li {
		float: none;
		display: inline-block;
    }
    a.btn.btn-theme.btn-large.section_li {
        background-color: #f8f9fd;
        color: #7864e9;
        border: 1px solid #7864e9;
    }
    .btn-theme{
        padding: 0 40px !important;
		border-radius: 6px !important;
        line-height: 45px !important;
        height: 45px !important;
	}
    ul.categories {
		text-align: center;
        margin:0;
        width: 100%;
    }
    .tabs {
		height:auto;
        background: transparent !important;
        width: 100% !important;
    }
    ul.tabs li {
        border:none !important;
        float: left;
        width: 121px !important;
    }
    li.tab a.active {
		min-height: auto !important;
    }
	a.btn.btn-theme.btn-large.section_li.active {
		background-color: #7864e9 !important;
		color: #f8f9fd;
		border: 0px;
	}
	.tabs .tab{
		padding: 0 5px 0 5px !important;
	}
	.collapsible {
		border: 0px;
		-webkit-box-shadow: none;
	}
	.document-box-view.redborder.active {
		border-left: 4px solid #ff7c9b;
	}
	.collapsible-header{
		border-bottom:none;
	}
	.collapsible-header.active{
		padding-bottom: 0px;
		font-weight: 600;
	}
	.collapsible-body{
		padding: 0 1rem 1rem 1rem;
	}
	.red-bg-right:before {
		top: -32px !important;
	}
	.green-bg-right:after{
		bottom: -25px !important;
		left: 50px;
	}
	ul:not(.browser-default) > li .li_cls {
		list-style-type: disc !important;
	}
	.btn-theme{
		padding:0px !important;
	}
	.art-title{
		margin:2% 0 0 0;	
	}
	.short-article h6, a{
		color:#7864e9;
	}
	.short-article p{
		text-align:left;
	}
	
	.short-article{
		margin-bottom:4%;
	}
    </style>
    <!-- START MAIN -->
    <div id="main" style="padding-left:0px !important;">
      <!-- START WRAPPER -->
      <div class="wrapper">
        <!-- START LEFT SIDEBAR NAV-->
        <?php //$this->load->view('template/sidebar'); ?>
        <!-- END LEFT SIDEBAR NAV-->
        <!-- START CONTENT -->
        <section id="content" class="bg-cp">
			
		<div class="container custom">
			<div class="row">
				<div class="col l12 s12 m12 art-title">
					<!-- Article Start -->
					<div id="test1" class="col s12 inner_row_container" style="margin:0 8%;">
						<div class="col l2 s12 m12"></div>
						<div class="col l7 s12 m12">
							<h5>The 10 features in an ideal Online Invoicing Software</h5>
							<div class="col l12 s12 m12">
							<div class="col l4 s12 m12">
								<img width="250" height="250" style="" src="<?php echo base_url(); ?>public/images/article-images/four.png" alt="" class="text-hide-collapsible logo_style_2"/>
							</div>
							<div class="col l8 s12 m12">
								<p style="text-align:justify;"><strong>Unlock The Power Of Automated Invoicing</strong></br>
								Whether you’re a service provider or a product-based enterprise, today’s market is getting competitive than it was ever before. Retaining clients & growing revenue is the key to business growth. For an SME, time and money are essential resources and thus should be optimized. Begin that by opting for reputed online invoicing software. </p>
								<p style="text-align:justify;">Invoicing software will increase your productivity by automating the manual process of making invoices on excel sheets. What’s better is that most of these are SaaS-based, making them affordable for smaller businesses. Here are the 10 features that keep in mind</p>
							</div>
							</div>
							<div class="col l12 s12 m12">
								<p style="text-align:justify;"><strong>1) Generating custom-made invoices</strong></br>
								Your company has a unique personality, and that should be reflected in your invoice. It’s your company’s first impression and so has to be the best. Opt for an invoicing product that offers you multiple templates to choose from and customisation in terms of the details you want printed on it. </p>
							</div>
							<div class="col l12 s12 m12">
								<p style="text-align:justify;"><strong>2) Accept multi currencies</strong></br>
								When the world has gone global, why should you be bound with just one currency type to operate with. Ensure that the invoicing software allows you to record receipts & payments in multiple foreign currencies. Also, it should have the ability to convert foreign currency into local for your accounting purpose. Secondly, it should allow you to record forex gain or loss that accrues at the time of receipt or payment.</p>
								<p style="text-align:justify;"><strong>3) Integratable with other software</strong></br>
								Most current software products are stand-alone invoicing software. You will have to download your data into excel and then import it to your accounting software, creating more work for you or your accountant. Also, this export and import of data can seriously harm the accuracy of data if not done carefully. Opt for a completely integrated application like Xebra that merges all the software modules together in one place.</p> 
								<p style="text-align:justify;"><strong>4) Can attach supporting documents</strong></br>
								You might want to attach specific expense vouchers that are reimbursable by the client with your invoice. Your software should allow you to seamlessly add that to your invoice total thereby removing the hassle of creating separate invoices just for expenses</p>
							</div>
							<div class="col l12 s12 m12">
							<div class="col l8 s12 m12">
								<p style="text-align:justify;"><strong>Unlock The Power Of Automated Invoicing</strong></br>
								Whether you’re a service provider or a product-based enterprise, today’s market is getting competitive than it was ever before. Retaining clients & growing revenue is the key to business growth. For an SME, time and money are essential resources and thus should be optimized. Begin that by opting for reputed online invoicing software. </p>
								<p style="text-align:justify;">Invoicing software will increase your productivity by automating the manual process of making invoices on excel sheets. What’s better is that most of these are SaaS-based, making them affordable for smaller businesses. Here are the 10 features that keep in mind</p>
							</div>
							<div class="col l4 s12 m12">
								<img width="250" height="250" style="" src="<?php echo base_url(); ?>public/images/article-images/atr10.png" alt="" class="text-hide-collapsible logo_style_2"/>
							</div>
							</div>
					</div>
					<!-- Article End -->
				</div>
			</div>
		</div>

        </section>
        <!-- END CONTENT -->
		</div>
		<!-- END WRAPPER -->
	</div>
    <!-- END MAIN -->

	<script>
		if ($('.menu-icon').hasClass('open')) {
			$('.menu-icon').removeClass('open');
			$('.menu-icon').addClass('close');
			$('#left-sidebar-nav').removeClass('nav-lock');
			$('.header-search-wrapper').removeClass('sideNav-lock');
			$('#header').addClass('header-collapsed');
			$('#logo_img').show();
			$('#eazy_logo').hide();
			$('#left-sidebar-nav').toggleClass('nav-expanded nav-collapsed');
			$('#main').toggleClass('main-full');
		}
	</script>
    <?php $this->load->view('template/footer'); ?>
