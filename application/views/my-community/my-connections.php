<style type="text/css">

	#breadcrumbs-wrapper {
	    border-bottom: 0px solid #f3f4f9;
	    padding: 0 0;
	    float: left;
	    width: 100%;
	}
	
	#search_comp[type=text] {
		width: 90% !important;
		box-sizing: border-box !important;
		border: 1px solid #ccc !important;
		border-radius:4px !important;
		font-size: 16px !important;
		background-color: white !important;
		background-image: url('<?php echo base_url(); ?>asset/css/img/icons/search.png') !important;
		background-position: 95% 10px !important; 
		background-repeat: no-repeat !important;
		padding: 12px 20px 12px 14px !important;
		outline: none !important;
	}
	
	#find_contact{
		height:45px;
		line-height:45px;
	}

	[type="radio"]:checked + label:after, [type="radio"].with-gap:checked + label:after {

    border: none;
    background-repeat: no-repeat;
    background-position: 3px 4px;
}
	
	#view_sent_request{
		color:#50E2C2 !important;
		font-size:15px;
	}

	.request-div1 {
	    height: 150px !important;
	    width: 32% !important;
	    margin: 10px !important;
	    padding: 0px 0px !important;
	    border-radius: 5px !important;
	}
	
	.request{
		padding-top:12px !important;
		/*border-left:1px solid #bec4d4;*/
		margin-left:-15px !important;
	}

	[type="radio"] + label:before, [type="radio"] + label:after {
    content: '';
    position: absolute;
    left: 0;
    top: 0;
    margin: 4px 0 0 0;
    width: 16px;
    height: 16px;
    z-index: 0;
    -webkit-transition: .28s ease;
    transition: .28s ease;
}
	
	.top-label{
		font-size:13px;
	}
	
	.content-div{
		margin-top:20px;
		margin-left:20px;
	}
	
	.company-div{
		height: 250px;
		width: 17.9% !important;
		margin: 5px !important;
		padding: 10px !important;
		border-radius:5px;
	}
	
	.company-div:hover{
		box-shadow: 0 1px 10px #7864e94d;
	}
	
	.white-box{
		background-color:#fff !important;
	}
	
	.div_close{
		margin-right:8px; 
		margin-top:15px;
	}
	
	.text-center{
		text-align:center;
	}
	
	.org-name{
		margin:8px 0;
		font-size:14px !important;
	}
	
	.connect{
		background-color:#fff !important;
		border:1px solid #7864e9 !important;
		color: #7864e9 !important;
		height:35px !important;
		line-height:35px !important;
		margin:15px 0;
	}
	
	.com-loc{
		font-size:20px !important;
	}
	
	.org-tagline{
		font-size:13px !important;
	}
	
	.org-loc{
		font-size:12px !important;
	}
		
	.org_details{
		background-color:#fff !important;
		text-align:center;
	}
		
	#new-connection{
		overflow-x:hidden !important;
		top:25% !important;
	}
	
	#new-connection .modalheader{
		padding:22px 30px !important;
	}
	
	#new-connection .conn-org{
		text-align:center;
		font-size:14px;
	}
	
	#new-connection .conn-org-tagline{
		font-size:12px;
	}
	
	.radio-list{
		padding:30px 0 !important;
	}
	
	 #con-client[type="radio"] + label:after {
		 margin: 1px 0 0 -3px !important;
	 }
	 
	 #con-vendor[type="radio"] + label:after {
		 margin: 1px 0 0 -3px !important;
	 }
	
	 #con-both[type="radio"] + label:after {
		 margin: 1px 0 0 -3px !important;
	 }

	 #con-none[type="radio"] + label:after {
		 margin: 1px 0 0 -3px !important;
	 }
	 
	 .suceess-msg{
		 font-size:14px;
	 }
	 
	.top-activity{
		text-align:right;
		font-size:12px;
		margin-top:30px;
		margin-right:35px;
	}
	
	.close-pop{
		cursor:pointer !important;
	}

	[type="radio"]:not(:checked) + label, [type="radio"]:checked + label {
    position: relative;
    padding-left: 20px;
    cursor: pointer;
    display: inline-block;
    height: 25px;
    line-height: 25px;
    font-size: 1rem;
    -webkit-transition: .28s ease;
    transition: .28s ease;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
}

#view_sent_request{ text-decoration:underline; }
@media only screen and (max-width: 600px) {
		.company-div{
			width: 96% !important;
		}
		.connec4{ width: 85% !important; margin-top: 6%; }
	}
</style>

<!-- START MAIN -->
<div id="main" style="padding:10px 0 0 0px !important;">
<!--div id="main" height="80%"-->

  	<!-- START WRAPPER -->
  	<div class="wrapper">

    	<!-- START LEFT SIDEBAR NAV-->
    	<?php //$this->load->view('template/sidebar'); ?>
    	<!-- END LEFT SIDEBAR NAV-->

    	<!-- START CONTENT -->
    	<section id="content" class="bg-cp sales_invoices-search">
	     	<div id="breadcrumbs-wrapper">
	        	<div class="container">
		          	<div class="row">

		          		<!-- Breadcrumps -->
		            	<div class="col s10 m6 l4">
		              		<h5 class="breadcrumbs-title my-ex">My Connections</h5>
		              		<ol class="breadcrumbs">
		                		<li><a href="">MY COMMUNITY / MY CONNECTIONS</a>
		              		</ol>
		            	</div>
		            	<!-- End of Breadcrumps -->

		            	<!-- Search Company -->
			            <div class="col s2 m6 l4 connec4">
			            	<form method="post" action="<?php echo base_url();?>community/company-search-connect">
								<?php $csrf = array(
					'name' => $this->security->get_csrf_token_name(),
					'hash' => $this->security->get_csrf_hash()
				);
				?>
				<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
			                	<input type="text" id="search_comp" name="search" value="<?php if(@$post_data['search']){echo $post_data['search']; } ?>" placeholder="SEARCH BY COMPANY NAME">
			                	<div class="col s12 m12 l12" style="text-align:center; margin-top:20px !important;">
									<input type="submit" name="submit" style="margin-top: -150px !important; margin-left: 95% !important; width: 25% !important;" id="submit" class="btn btn-welcome" value="Search">
								</div>
			                </form>
			            </div>
			            <!-- End of Search Company -->

			            <!-- Connection Requests -->
			            <div class="request">
							<a href="<?php base_url(); ?>connection-requests" id="view_sent_request" class=""><i class="community-icons" style="margin: -24px 5px 0px 110px !important;"></i><strong>CONNECTION REQUESTS</strong></a>
						</div>
						<!-- End of Connection Requests -->

						<!--div class="col s2 m6 l2">
							<a href="<?php base_url(); ?>with_connection" id="find_contact" class="btn btn-theme btn-large modal-trigger">FIND CONTACTS</a>
						</div-->

	         		</div>
	       		</div>
	     	</div>
			
			<?php if(count($result) >= 1) {?>
	 			<?php $this->load->view('my-community/new-conn-request'); ?>
	 		<?php }?>
	 		
     		<div class="container custom content-div">

				<div class="row">
					<div class="col l12 s12 m12" style="margin: -35px 0 0px 12px;">
						<div class="col l6 s12 m12">
							<p class="top-label">COMPANIES YOU MAY KNOW</p>
						</div>

						<!--div class="col l6 s12 m12 right">
							<p class="top-all"><a href="<?php base_url(); ?>" id="view_sent_request" class="">View All</a></p>
						</div-->
						<!--div class="col l6 s12 m12 right">
							<p class="top-activity"><a href="<?php base_url(); ?>sent_receive" id="view_sent_request" class="">View Activity History</a></p>
						</div-->
		            </div>
		       	</div>

	   			<div class="row">
					<div class="col l12 s12 m12">
						<div class="col l12 s12 m12">

							<?php $bus_id = $this->user_session['bus_id'];?>
							<?php foreach($conn as $key => $value){?>
							<?php if($value->bus_id != $bus_id) { ?>
								<!-- Connect Sent Successfully -->
								<div class="col l2 s12 m12 white-box company-div" id="ok<?=$conn[$key]->bus_id;?>" hidden>

									<div class="row">
										<div class="col s12 m12 l12">
											<a id="div_close" class="div_close modal-close close-pop"><img width="18" height="18" src="<?php echo base_url();?>asset/images/popupdelete1.png" alt="delete"></a>
										</div>
									</div>

									<div class="row">
										<div class="col s12 m12 l12 text-center">	
										  <img width="150" height="100" src="<?php echo base_url();?>public/images/success.png" alt="Logo" class="">
										</div>
									</div>

									<div class="row">
										<div class="col s12 m12 l12 text-center">	
											<label class="suceess-msg"><b>Connection request sent</b></label><br>
											<label class="suceess-msg"><b>successfully</b></label>
										</div>
									</div>

								</div>
								<!-- End of Connect Sent successfully -->
							<?php }?>
							<?php }?>

							<?php $bus_id = $this->user_session['bus_id'];?>
							<?php foreach($conn as $key => $value){?>
							<?php if($value->bus_id != $bus_id) { ?>
								<div class="col l3 s12 m12 white-box company-div" id="conn" style="margin: 0 15px 0px 12px;">
									
									<div class="row">
										<div class="col s12 m12 l12">
											<a id="div_close" class="div_close modal-close close-pop"><img width="18" height="18" src="<?php echo base_url();?>asset/images/popupdelete1.png" alt="delete"></a>
										</div>
									</div>

									<div class="row">
										<div class="col s12 m12 l12 text-center">	
											<?php if($conn[$key]->bus_company_logo != ''){?>
												<img src="<?php echo DOC_ROOT_DOWNLOAD_PATH_CO ?>company_logos/<?=$conn[$key]->bus_id;?>/<?=$conn[$key]->bus_company_logo;?>" height="50px" class="logo_style_2" width="50px">
											<?php }else{ ?>
												<img src="<?=base_url();?>asset/css/img/icons/company-icon.png" height="50px" class="logo_style_2" width="50px">
											<?php } ?>
										</div>
									</div>

									<div class="row">
										<div class="col s12 m12 l12 text-center" style="height:55px;">	
											<p class="org-name"><b><a style="color:#000;" target="_blank" href="<?php echo base_url(); ?>community/view-company-details?token=<?php echo $conn[$key]->bus_id; ?>"><?php echo strtoupper($conn[$key]->bus_company_name ?? ''); ?></b></a></p>
										</div>
									</div>

									<div class="row">
										<div class="col s12 m12 l12 text-center">	
											
										</div>
									</div>
								
									<div class="row">
										<div class="col s12 m12 l12 text-center">	
											<i class="material-icons view-icons com-loc" style="margin-left: -13px;">location_on</i><label class="org-loc"><?php echo strtoupper($conn[$key]->name ?? ''); ?></label>
										</div>
									</div>
						
									<div class="row">
										<div class="col s12 m12 l12 text-center">

											<?php

											$connections = $this->Community_model->selectData('connections', '*',  array('bus_id' => $bus_id, 'bus_id_connected_to' => $conn[$key]->bus_id));
											//print $this->db->last_query();

											if(count($connections)>0){
												//print_r($connections);
												if($connections[0]->accepted == 0 && $connections[0]->status == 1){ ?>
													<a class="connect btn btn-theme btn-large">REQUESTED</a>
												<?php } elseif ($connections[0]->accepted == 1 && $connections[0]->status == 1) { ?>
													<a  class="connect btn btn-theme btn-large">CONNECTED</a>
												<?php } else {?>
													<a id="connect" type="button" class="connect btn btn-theme btn-large modal-trigger" href="#new-connection<?=$conn[$key]->bus_id;?>">CONNECT</a>
												<?php }												
											}else{?>
												<a id="connect" type="button" class="connect btn btn-theme btn-large modal-trigger" href="#new-connection<?=$conn[$key]->bus_id;?>">CONNECT</a>
											<?php } ?>

										</div>
									</div>
						
								</div>
							<?php }?>
							<?php }?>
						</div>
            		</div>
       			</div>
    		</div>

    		<?php if(count($result1) >= 1){?>
    			<?php $this->load->view('my-community/companies-connected'); ?>
    		<?php }?>

   		</section> <!-- END CONTENT -->
  	</div> <!-- END WRAPPER -->
</div> <!-- END MAIN -->

<div id="want_as_client" class="modal modal-set" style="min-height:auto !important;">
   <style>
		#want_as_client .modal-body{
			text-align:center;
			border-top:none !important;
			font-family:"Roboto", sans-serif !important;
			padding: 0% 0 5% 0 !important;
			margin-left: 0px !important;
			width:100%;
			border-bottom:1px solid #eee;
		}
		
		#want_as_client textarea.materialize-textarea.text-area-made{
			margin: -29px 0 -6px 0px !important;
			width: 90% !important;
		}
		
		#want_as_client{
			width:40% !important;
			margin-top:4% !important;
		}
		
		#want_as_client .modal-body p {
			font-size: 17px;
			color: #000;
			font-weight: 400;
		}
		
		#want_as_client .reject-content{
			padding:15px 10px 40px 10px !important;
			border-bottom:1px solid #eee;
		}
	</style>
	<!--div class="modal-content reject-content">
		<div class="modal-header">
			<h4>Reason for rejection</h4>
			<!--input type="hidden" id="act_expid" name="act_expid" value=""/>
			<a class="modal-close close-pop"><img src="<?php echo base_url();?>asset/images/popupdelete.png" alt="delete"></a>
		</div>
	</div-->
   <div class="modal-body">
		<p>Do you want to add this user in your client master?</p>
   </div>
   <div class="modal-content">
      <div class="modal-footer">
         <div class="row">
            <div class="col l4 s12 m12"></div>
            <div class="col l12 s12 m12 cancel-deactiv text-right">
               <a style="min-width: 15%; height: 40px; line-height: 40px;" class=" modal-close btn-flat theme-flat-btn theme-btn theme-btn-large model-cancel" type="button">NO</a>
               <a style="width: 15%; height: 40px; line-height: 40px;" id="" class="btn btn-theme btn-large modal-trigger" data-target="add_comp_purexp">YES</a>
            </div>
         </div>
      </div>
   </div>
</div>

<div id="want_as_vendor" class="modal modal-set" style="min-height:auto !important;">
   <style>
		#want_as_vendor .modal-body{
			text-align:center;
			border-top:none !important;
			font-family:"Roboto", sans-serif !important;
			padding: 0% 0 5% 0 !important;
			margin-left: 0px !important;
			width:100%;
			border-bottom:1px solid #eee;
		}
		
		#want_as_vendor textarea.materialize-textarea.text-area-made{
			margin: -29px 0 -6px 0px !important;
			width: 90% !important;
		}
		
		#want_as_vendor{
			width:40% !important;
			margin-top:4% !important;
		}
		
		#want_as_vendor .modal-body p {
			font-size: 17px;
			color: #000;
			font-weight: 400;
		}
		
		#want_as_vendor .reject-content{
			padding:15px 10px 40px 10px !important;
			border-bottom:1px solid #eee;
		}
	</style>
	<!--div class="modal-content reject-content">
		<div class="modal-header">
			<h4>Reason for rejection</h4>
			<!--input type="hidden" id="act_expid" name="act_expid" value=""/>
			<a class="modal-close close-pop"><img src="<?php echo base_url();?>asset/images/popupdelete.png" alt="delete"></a>
		</div>
	</div-->
   <div class="modal-body">
		<p>Do you want to add this user in your vendor master?</p>
   </div>
   <div class="modal-content">
      <div class="modal-footer">
         <div class="row">
            <div class="col l4 s12 m12"></div>
            <div class="col l12 s12 m12 cancel-deactiv text-right">
               <a style="min-width: 15%; height: 40px; line-height: 40px;" class=" modal-close btn-flat theme-flat-btn theme-btn theme-btn-large model-cancel" type="button">NO</a>
               <a style="width: 15%; height: 40px; line-height: 40px;" id="" class="btn btn-theme btn-large modal-trigger" data-target="add_comp_purexp">YES</a>
            </div>
         </div>
      </div>
   </div>
</div>

<!-- New Connection Modal -->
<?php foreach($conn as $key => $value) { ?>
<?php if($value->bus_id != $bus_id) { ?>
<div id="new-connection<?=$conn[$key]->bus_id;?>" class="modal modal-md">
	<div class="modalheader">
		<h4>Connection Box</h4>
		<a class="modal-close close-pop"><img src="<?php echo base_url();?>asset/images/popupdelete1.png"></a>
	</div>

	

	<div class="modalbody">
		<form class="addconn" id="add_connection_info" name="add_connection_info">
			<div class="row" style="margin-bottom:0px !important;">
				<div class="col l12 s10 m10 fieldset">
					<div class="row">
						<div class="col l12 s12 m12 fieldset org_details">
							<p class="conn-org" style="text-align: center;"><b><?php echo strtoupper($conn[$key]->bus_company_name); ?></b></p>
							<label class="conn-org-tagline">THIS COMPANY IS CONNECTED WITH ME AS</label>
							<input type="text" hidden name="con_to" id="con_to<?=$conn[$key]->bus_id;?>" value="<?=$conn[$key]->bus_id;?>">
						</div>
						<div class="col l12 s12 m12 fieldset org_details radio-list">
							<div class="col l2 s10 m10 fieldset"></div>
							<div class="col l2 s12 m12 fieldset">
								<input type="radio" class="form-check-input" id="con-client<?=$conn[$key]->bus_id;?>" name="connect" value="client" checked>
								<label class="form-check-label" for="con-client<?=$conn[$key]->bus_id;?>">CLIENT</label>
							</div>
							<div class="col l2 s12 m12 fieldset">
								<input type="radio" class="form-check-input" id="con-vendor<?=$conn[$key]->bus_id;?>" name="connect" value="vendor">
								<label class="form-check-label" for="con-vendor<?=$conn[$key]->bus_id;?>">VENDOR</label>
							</div>
							<div class="col l2 s12 m12 fieldset">
								<input type="radio" class="form-check-input" id="con-both<?=$conn[$key]->bus_id;?>" name="connect" value="both">
								<label class="form-check-label" for="con-both<?=$conn[$key]->bus_id;?>">BOTH</label>
							</div>
							<div class="col l2 s12 m12 fieldset">
								<input type="radio" class="form-check-input" id="con-none<?=$conn[$key]->bus_id;?>" name="connect" value="none">
								<label class="form-check-label" for="con-none<?=$conn[$key]->bus_id;?>">NONE</label>
							</div>
							<div class="col l2 s10 m10 fieldset"></div>
						</div>
					</div>
					<div class="row">
						<div class="col l11 s12 m12" style="margin-left: 3.333333%; margin-top: 20px;">
							<div class="row notess note-mor">
								<div class="s12 m12 l12">
									<div class="note">
										<label for="message"></label>
										<textarea id="message<?=$conn[$key]->bus_id;?>" placeholder="MESSAGE" type="text" name="message" class="bill-box ex-noss materialize-textarea ratingtextbox" style="height: 54px;min-height: 6rem;margin-bottom:0;"></textarea>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col l6 m6 s12 fieldset"></div>
					<div class="col l6 m6 s12 fieldset buttonset">
						<div class="right">
							<button type="button" class="connecting modal-close btn-flat theme-primary-btn theme-btn theme-btn-large right" onclick="con_company('<?php echo $conn[$key]->bus_id;?>')">CONNECT</button>
							<button type="button" class="modal-close btn-flat theme-flat-btn theme-btn theme-btn-large modal-close">CANCEL</button>
						</div>
					</div>
				</div>
			</div>
		</form>
		
	</div>
</div> <!-- End of New Connection Modal -->
<?php }?>
<?php }?>

<script type="text/javascript">
	$(document).ready(function() {
		$('.div_close').on('click', function(){
			console.log('OK');
			$(this).closest(".company-div").remove();
		});
		$('.connecting').on('click', function(){
			console.log('OK');
			$("#conn").hide();
			console.log('BYE');
			$("#ok").show();
			var bus_id=$(this).val();
 		if(bus_id!=''){
 			srec_counter=0;
 			console.log('yeah');
			if(csrf_hash===""){
				csrf_hash=csrf_hash;
			}
	 		$.ajax({
					type: "POST",
					data: {'csrf_test_name':csrf_hash,'bus_id':bus_id},
					url: base_url+'connection/sent_receive',
					
	                success:function(result)
					{
						var data=JSON.parse(result);

                          $("#employee_name").val(data.name);
                           $("#employee_designation").val(data.designation);
                           $("#reporting_manager_name").val(data.reporting_to);
                           $("#reporting_managers_designation").val(data.manager_designation);

                        

						$('select').material_select();	
					}
	 		});
	 		}	
		});
		
	});

	function con_company(id) {
		if($('#con-client'+id).is(":checked") ) {
			//$('#want_as_client').modal('open');
		}
		
		if($('#con-vendor'+id).is(":checked") ) {
			//$('#want_as_vendor').modal('open');
		}
		var con_client 	= $('#con-client'+id).val();
		var con_vendor 	= $('#con-vendor'+id).val();
		var con_both 	= $('#con-both'+id).val();
		var con_none 	= $('#con-none'+id).val();

		var messages	= $('#message'+id).val();
		var con_to_id	= $('#con_to'+id).val();
          	if($('#con-client'+id).is(":checked")) {
			con_client = 1;
			con_vendor = 0;
		}

		if($('#con-vendor'+id).is(":checked")) {
			con_vendor = 1;
			con_client = 0;
		}

		if($('#con-both'+id).is(":checked")) {
			con_vendor = 1;
			con_client = 1;
		}

		if($('#con-none'+id).is(":checked")) {
			con_vendor = 0;
			con_client = 0;
		}
		if(csrf_hash===""){
			csrf_hash=csrf_hash;
		}
		$.ajax({

			dataType: 'json',
			type: "POST",
			url:base_url+'Community/add_connection_info',

			data:{'csrf_test_name':csrf_hash,'client':con_client,'vendor':con_vendor,'message':messages, 'bus_id_connected_to':con_to_id},

			success: function(data){
				if(data['csrf_hash']){
					csrf_hash=data['csrf_hash'];
				}
				$('#connect_modal'+id).modal('close');
				$('#con-client'+id).val('');
				$('#con-vendor'+id).val('');
				$('#con-both'+id).val('');
				$('#con-none'+id).val('');
				$('#message'+id).val('');
				$('#con_to'+id).val('');

				Materialize.toast('Your connection request has been sent', 5000, 'green rounded');
				setTimeout(function(){ 
					location.href=base_url+'community/connection-requests';
				}, 5000); 	
			}
		});
	}

</script>