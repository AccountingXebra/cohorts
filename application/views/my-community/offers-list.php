<?php $this->load->view('my-community/Cohorts-header'); ?>
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Include DataTables CSS & JS -->
<link rel="stylesheet" href="https://cdn.datatables.net/1.13.6/css/jquery.dataTables.min.css">
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script src="https://cdn.datatables.net/1.13.6/js/jquery.dataTables.min.js"></script>

    <style type="text/css">
		#dealsTable_length_wrapper{ margin:0 1%; }
		#dealsTable_length label {
			display: flex;
			font-size: 0px;
		}

		#dealsTable_length .select-wrapper span.caret {
    		color: #595959;
    		top: -2px;
		}

		/* Style the dropdown to fit correctly */
		#dealsTable_length select {
			padding: 5px 10px;
			border: 1px solid #ccc;
			border-radius: 5px;
			background: #fff;
			font-size: 14px;
			cursor: pointer;
			min-width: 100px; /* Ensure the dropdown has a proper width */
		}


	#dealsTable_length .dropdown-content {
		min-width: 96px;
		margin-top:40% !important;
	}
	
	#dealsTable_length{
		border:1px solid #B0B7CA !important;
		height:38px;
		border-radius:4px;
		width:95px;
		margin-top:5px;
		margin-left:52%;
	}
	
	#dealsTable_length .select-wrapper input.select-dropdown {
		margin-top:-3px !important;
		margin-left:10px !important;
	}
	
	#dealsTable_length .select-wrapper span.caret {
		margin: 17px 7px 0 0;
	}	


	#submit1.mandate{ border:3px solid red !important; }
	.event-dropdown li.user-profile-fix{ margin:-8px 0 0 0 !important; }
	@media only screen and (max-width: 600px) {
		.btn-theme{ margin:-14% 0 0 0 !important; }
		#submit1{ width:18% !important; }
		.event-button.waves-block{ margin-left:90%; }
	}
	.icon-font{
		color:#ff7d9a !important;
		font-size:15px;
		width:5% !important;
	}
	.event-desc{ height:95px; margin:-6px 0; }
	.color-purple{
		color: #7864e9 !important;
		font-size:16px;
	}
	
	.set_remin{
		/*background-color: #ff7d9a !important;*/
		background-color: #7864e9 !important;
		height: 35px !important;
		line-height: 37px !important;
		padding-left: 0px !important;
		margin: 6px 2px !important;
	}
	
	@media all and (-ms-high-contrast: none), (-ms-high-contrast: active) { 
		a{
			color:#000;
		}
		.btn-date-bill {
			line-height: 34px !important;
		}
		.select2-container{
			margin-top:-4px;
		}
	}	
	.dataTables_scrollBody{
		overflow:hidden !important;
		height:100% !important;
	}

	.dataTables_scrollHead{
		margin-bottom:-24px !important;
	}

	.btn-theme {
    font-size: 12px;
    font-weight: 500;
    padding: 0 15px;
    border-radius: 7px;
    line-height: 42px;
    height: 40px;
    letter-spacing: 0.20px !important;
    background-color: #7864e9;
    margin: 0 -10px 0px 0px;
}

.btn-search {
    border: 1px solid #B0B7CA;
    border-radius: 6px;
    background: none;
    height: 40px;
    line-height: 40px;
    padding: 0;
    box-shadow: unset;
    -webkit-box-shadow: unset;
    -o-box-shadow: unset;
    -moz-box-shadow: unset;
    -ms-box-shadow: unset;
    margin: 0 0 0 0px;
    color: #B0B7CA;
}

	table.dataTable thead .sorting {
		background-position: 110px 15px !important;
	}
	table.dataTable thead .sorting_asc {
		background-position: 110px 15px !important;
	}
	table.dataTable thead .sorting_desc {
		background-position: 110px 15px !important;
	}

  .select-dropdown{
    max-height: 350px !important;
  }
  .btn-theme-disabled{
     cursor: not-allowed;
  }
  .icon-img {
    margin: 0 30px 0 2px;
  }

/*----------START SEARCH DROPDOWN CSS--------*/
.select2-container--default .select2-selection--single {
  border:none;
}

input[type="search"]:not(.browser-default) {
  height: 30px;
  font-size: 12px;
  margin: 0;
  border-radius: 5px;

}
.select2-container--default .select2-selection--single .select2-selection__rendered {
  font-size: 13px !important;
  line-height: 30px;
  color: #000 !important;
  font-weight: 400 !important;

}
.select2-container--default .select2-selection--single .select2-selection__arrow {
  height: 40px;
}
.select2-search--dropdown {
  padding: 0;
}
input[type="search"]:not(.browser-default):focus:not([readonly]) {
  border-bottom: 1px solid #bbb;
  box-shadow: none;
}
.select2-container--default .select2-selection--single:focus {
    outline: none;
}
.select2-container--default .select2-results__option--highlighted[aria-selected] {
    background: #fffaef;
  color: #666;
}
.select2-container--default .select2-results > .select2-results__options {
  font-size: 12px;
  border-radius: 5px;
  box-shadow: 0px 2px 6px #B0B7CA;
}
.select2-dropdown {
  border: none;
  border-radius: 5px;
}
.select2-container .select2-selection--single {
  height: 40px;
    padding: 6px;
    border: 1px solid #d4d8e4;
    background: #f8f9fd;
    border-radius: 5px;
}
.select2-results__option[aria-selected] {
  border-bottom: 1px solid #f2f7f9;
  padding: 14px 16px;
}
.select2-container--default .select2-search--dropdown .select2-search__field {
    border: 1px solid #d0d0d0;
    padding: 0 0 0 15px !important;
    width: 93%;
    max-width: 100%;
}
.select2-search__field::placeholder {
  content: "Search Here";
}
.select2-container--open .select2-dropdown--below {
  margin-top: 0;
}
.select2-container {
	width: 255px !important;
	margin-top:-5px;
}
/*----------END SEARCH DROPDOWN CSS--------*/

  .dataTables_length {

    margin-left: 500px;
}
	#my-customers_length{
		border:1px solid #B0B7CA;
		height:38px;
		border-radius:4px;
		width:90px;
		margin-top:5px;
		margin-left:52%;
	}

	#my-customers_length .select-wrapper input.select-dropdown {
		margin-top:-3px !important;
		margin-left:10px !important;
	}

	#my-customers_length .dropdown-content {
		min-width: 90px;
		margin-top:-50% !important;
	}

	.customer_profile_bulk_action.filled-in:not(:checked) + label:after {
		top:5px !important;
	}

	#my-customers_length .select-wrapper span.caret {
		margin: 17px 5px 0 0;
	}

	.sticky {
		position: fixed;
		top: 70px;
		width: 76.2%;
		z-index:999;
		background: white;
		color: black;
	}

	.sticky + .scrollbody {
		padding-top: 102px;
	}

	.tipxebra{
		margin-top:-10px;
	}

	a.filter-search.btn-search.btn.active {
		width:40% !important;
		margin-right: -60px !important;
	}

	
	
	.btn-date{
		max-width:80px !important;
		height:33px !important;
		padding-top:6px !important;
		margin-left:0px !important;
	}
	
	.btn-search{
		margin:-5px 5px 0px 0;
	}
	
	@-moz-document url-prefix() {
		#c_start_date, #c_end_date{
			max-width:80px !important;
			height:33px !important;
			padding-top:6px !important;
			margin-left:0px !important;
		}
		
		.btn-date{
			max-width:80px !important;
			height:33px !important;
			padding-top:6px !important;
			margin-left:0px !important;
		}
		.btn-search{
			margin-top:-5px;
		}
	}

	span.tabl {
		margin: -4px 0 0 21px !important;
	}

	#c_start_date::placeholder{
		color:#000;
		font-size:12px;
	}
	#c_end_date::placeholder{
		color:#000;
		font-size:12px;
	}

	.ex{
		margin:-28px 0 0 48px !important;
	}

	.btn-stated {
		background-position: 95px center !important;
	}

	.offer-1{
		border-radius:7px;
		padding:0 2%;
		width:100%;
		border-left:4px solid #fff;
	}

	.offer-1:hover{
		/*border-left:4px solid #ff7d9a;*/
		width:100%;
	}

	.img-offer{
		padding: 1% 0 0 0 !important;
	}

	.offer-info{
		padding: 1% 0 !important;
	}

	a.showmore{
		color: #ff7d9a !important;
		text-decoration: underline;
		text-align:center;
		font-size: 12px !important;
		padding-top: 15px;
	}

	.offer-info h6{
		color:#7864e9 !important;
	}

	.offer_description{
		font-size:13px;
		margin:15px 0 !important;
	}
	
	.offer_p{
		font-size:13px;
		margin:15px 0 !important;
		word-wrap: break-word;
		width:885px;
	}
	
	::placeholder{
		font-size: 11.8px !important;
		line-height: 30px;
		color: #000 !important;
		font-weight: 400 !important;
		font-family: "Roboto", sans-serif !important;
	}
	.event-dropdown {
    margin: 5px 0 0 -138px !important;
	}
	
	.dataTables_length {
	margin-left: 500px;
 }

 #deal_table_length {
   border:1px solid #B0B7CA;
   height:38px;
   border-radius:4px;
   width:96px;
   margin-top:5px;
   margin-left:50%;
 }

 #deal_table_length .select-wrapper input.select-dropdown {
   margin-top:-3px !important;
   margin-left:10px !important;
 }

 #deal_table_length .select-wrapper span.caret {
   margin: 17px 7px 0 0;
 }

 #deal_table_length .dropdown-content {
   min-width: 95px;
 }
 
 .action-tab{
		vertical-align: top !important;
	}
	a.filter-search.btn-search.btn.active {
		    width: 20% !important;
    margin-right: 2px !important;
    margin-left: -7%;
	}
	
	.btn-search.active .search-hide-show {
		width: calc(82% - 29px) !important;
		margin-left: 24px !important;
	}
	.btn-date{ max-width:82px !important; }
	
	#submit1{ height: 39px; background-color: #7864e9; border: 1px solid #ccc; border-radius: 5px; color:#fff; font-size:13px; line-height: 41px;}
</style>
    <div id="main" style="padding-left:0px !important;">
      <div class="wrapper">
        <?php //$this->load->view('template/sidebar'); ?>
        <section id="content" class="bg-cp customer-search">
          <div id="breadcrumbs-wrapper">
            <div class="container">
               <div class="row">
               <div id="plan_msg"></div>
             </div>
              <div class="row">
                <div class="col s10 m6 l6">

                  <h5 class="breadcrumbs-title">Deals<small class="grey-text">(Total <?php echo count($offers);?>)</small></h5>

                  <ol class="breadcrumbs">
                    <li><a>GROWTH</a>
                    </li>
                    <li class="active">Deals</li>
                  </ol>
                </div>
                <div class="col s10 m6 l6">
					<a class="btn btn-theme btn-large right add-new cus-new" href="<?= base_url(); ?>community/add-offer">ADD NEW DEALS</a>
                </div>
              </div>
            </div>
          </div>

          <div id="bulk-action-wrapper">
            <div class="container">
              <div class="row">
              	<form  name="search_form_deal" id="search_form_deal" method="post"> 
					<?php $csrf = array(
					'name' => $this->security->get_csrf_token_name(),
					'hash' => $this->security->get_csrf_hash()
				);
				?>
				<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
				<div class="col l12 s12 m12" style="margin-top:-20px;">
                 <a href="javascript:void(0);" onclick="formsubmit();" class="addmorelink right" id="reset_offer" title="Reset all">Reset</a>
                </div>
                <!--div class="col l2 s12 m12">
                	
                  <!--a class='dropdown-button border-radius-6 btn-dropdown-action btn-bulk btn-default uppercase no-background' href='#' data-activates='dropdown04'>Bulk Actions <i class="arrow-icon"></i></a-->
                    <!--ul id='dropdown04' class='dropdown-content'>
                      <!--li><a id="email_multiple_customers"><i class="dropdwon-icon icon email"></i>Email</a></li-->
                      <!--li><a id="download_multiple_customers" data-multi_download="0" style="display: flex;"><img class="icon-img" src="<?php echo base_url(); ?>public/icons/export.png" alt="export" style="width: 15px;height: 22px;">Export</a></li-->
                      <!--li><a id="print_multiple_customers"><i class="material-icons">print</i>Print</a></li-->
                    <!--/ul>
				
                </div-->
                <div class="col l3 s12 m12"></div>
                 <div class="col l9 s12 m12">
					<div class="action-btn-wapper right">
                    <div class="bulk location-drop" style="margin: 0 0 0 -3%; width: 105%;">
                        <a class="filter-search btn-search btn">
							<input type="text" name="search" id="search_offer" class="search-hide-show" value="<?php if(isset($search['search']) && $search['search']!=""){echo $search['search']; } ?>"style="display:none" />
							<i class="material-icons ser search-btn-field-show">search</i>
						</a>
                        <select class="select-stat js-example-basic-single" name="offer_category" id="offer_category" >
							<option value="">DEALS CATEGORIES</option>
							<?php if(isset($search['offer_category']) && $search['offer_category']!=""){ ?>
							<option value="<?php $search['offer_category'];?>" selected><?php echo strtoupper($search['offer_category']);?></option>
						<?php } ?>
							<option value="">ALL</option>
							<option value="Education & E-learning">EDUCATION & E-LEARNING</option>
							<option value="Finance & Legal">FINANCE & LEGAL</option>
							<option value="Marketing Services">MARKETING SERVICES</option>
							<option value="Office Admin & Supplies">OFFICE ADMIN & SUPPLIES</option>
							<option value="SaaS & Software">SAAS & SOFTWARE</option>
							<option value="Travel & Hospitality">TRAVEL & HOSPITALITY</option>
						  </select>
						  <select class="js-example-basic-single" name="offer_company" id="offer_company" >
							<option value="">COMPANY NAME</option>
							
							<option value="">ALL</option>
							<?php if(count($company)>0) {
                      			foreach($company as $com)  { ?>
                        			<option value="<?php echo $com['bus_id']; ?>"  <?php if(isset($search['offer_company']) && $search['offer_company']!=""){ if($com['bus_id']==$search['offer_company']){ echo "selected";} }?>><?php echo strtoupper($com['bus_company_name']); ?></option>
		                        <?php  } }
		                    ?>
                          </select>

                          <input type="text" placeholder="START DATE" class="btn-date icon-calendar-green eventdatepicker date-cng btn-stated out-line" id="offer_start_date" name="offer_start_date"  value="<?php if(isset($search['offer_start_date']) && $search['offer_start_date']!=""){ echo $search['offer_start_date']; } ?>" readonly="readonly" style="margin-left:2px !important;">
						  <input type="text" placeholder="END DATE" class="btn-date icon-calendar-red eventdatepicker date-cng btn-stated out-line" id="offer_end_date" name="offer_end_date" value="<?php if(isset($search['offer_end_date']) && $search['offer_end_date']!=""){ echo $search['offer_end_date']; } ?>" readonly="readonly" style="margin-left:2px !important;">
						 <input type="submit" id="submit1" name="submit" value="APPLY" style="width:8.5%;">
                    </div>
                  </div>
                </div>
            </form>
              </div>
            </div>
          </div>

   

		<div class="container">
			<div class="row">
				
				<!--div class="col l12 s12 m12" >
					<table id="deal_table" class="responsive-table display table-type1 mb-2" cellspacing="0">
						<thead id="fixedHeader">
						<tr>
							<th style="width:15%;"></th>
							<th style="width:80%;"></th>
							<th style="width:5%;"></th>
						</tr>
						</thead>
						<tbody class="scrollbody">

						</tbody>
					</table>
				</div-->
				
				<div class="col s12 m12 l12" style="padding-top:1%;">
				<div class="offers_list">
					<div class="row" id="dealsList">
						<?php if (count($offers) > 0){ foreach($offers as $off) {?>
						<div class="col s12 m12 l6 deals-card">
						<div class="offer-1 box-wrapper bg-white shadow" style="margin: 0 0 10px 0px;">
							<!--div class="col s12 m12 l3 img-offer" style="width:20%;">
								<?php
									if($off['offer_image'] != '') {
										$offer_image = '<img style="object-fit: contain;" src="'.DOC_ROOT_DOWNLOAD_PATH.'offer_image/'.$off['id'].'/'.$off['offer_image'].'" height="130px" class="logo_style_2" width="130px">';
									} else {
										$offer_image = '';
									}
								?>
								<div class="col s6 m6 l4" style="text-align: right;">
									<?php echo $offer_image; ?>
								</div>
							</div-->
							<div class="col s12 m12 l12 offer-info">
								<div class="col s12 m12 l10" style="border-left: 7px solid #7864e9; margin: -5px 0 0 -2.8%; border-top-left-radius: 5px; border-bottom-left-radius: 5px; padding: 2px 5.2%">
									<h5 style="height:40px; font-size:20px; color:#7864e9 !important;"><strong><?php echo strtoupper($off['offer_title']);?></strong></h5>
								</div>	
								<?php $bus_id = $this->user_session['bus_id']; ?>
								
								<div class="col s12 m12 l2" style="padding: 2% 0% 2% 14.5%;">
									<?php if ($off['bus_id'] == $bus_id) {?>
									<a href="javascript:void(0);" class="waves-effect waves-block waves-light event-button" data-activates="event-dropdown<?php echo $off['id'];?>"><i style="color:#24292c; font-size:17px;" class="fa fa-ellipsis-v" aria-hidden="true"></i></a>
									<ul id="event-dropdown<?php echo $off['id'];?>" class="event-dropdown dropdown-content user-profile-down" style="margin-left:0% !important;">
						            <li class="user-profile-fix">
										<ul>
											<li><a href="<?php echo base_url();?>community/edit-offer/<?php echo $off['id'];?>"><i class="material-icons" style="color: #000;">mode_edit</i>EDIT</a></li>
											<li><a href="javascript:void(0);" class="deactive_deal" data-deal_id="<?php echo $off['id'];?>"><i class="material-icons">delete</i>DELETE</a></li>
										</ul>
									</li>
									</ul>
									<?php  } ?>
								</div>	
							
							</div>
							<div class="col s12 m12 l12 offer-info">
								<div class="col s12 m12 l9" style="padding: 2px 0px; margin-left:-0.5%;">
									<div class="col s12 m12 l12">
										<p class="offer_p"><i class="icon-font fas fa-building"></i> <strong>COMPANY NAME: </strong> <?php echo strtoupper($off['company_name']);?></p>
										<p class="offer_p"><i class="icon-font far fa-id-card"></i> <strong>CONTACT NAME: </strong><?php echo strtoupper($off['contact_name']);?></p>
										<p class="offer_p"><i class="icon-font fas fa-calendar-alt"></i> <strong>VALIDITY DATE:</strong> <?php if($off["start_date"] !=""){ echo strtoupper(date("jS M, y", strtotime($off["start_date"])).' to '. date("jS M, y", strtotime($off["end_date"]))); } ?></p>
										<p class="offer_p"><i class="icon-font fas fa-clock"></i> <strong>VALIDITY TIME:</strong> <?php if($off["start_time"] !=""){ echo strtoupper($off["start_time"].' to '. $off["end_time"]); } ?></p>
									</div>
								</div>	
								<div class="col s12 m12 l3" style="padding: 2% 0% 2% 0%;">
									<?php
									if($off['offer_image'] != '') {
										$offer_image = '<img style="object-fit: contain;" src="'.DOC_ROOT_DOWNLOAD_PATH.'offer_image/'.$off['id'].'/'.$off['offer_image'].'" height="130px" class="logo_style_2" width="130px">';
									} else {
										$offer_image = '';
									}
									?>
									<?php echo $offer_image; ?>
								</div>	
							</div>
							<div class="col s12 m12 l12">
								<div class="col s12 m12 l12 offer_description">
									<p class="event-desc" style="text-align: justify;"><?php echo $off['offer_description'];?></p>
								</div>
							</div>
							<div class="col s12 m12 l12">
								<div class="col s12 m12 l6">
									<p class=""><strong><a class="color-purple" href="<?php echo $off['website_link'];?>" target="_blank">WEBSITE</a></strong></p>
								</div>
								<div class="col s12 m12 l6">
									<a class="btn btn-theme btn-large right set_remin" href="<?php echo base_url();?>settings/add-event-activity-alert/<?php echo $off['id'];?>" target="_blank">&nbsp; SET REMINDER</a>
								</div>
							</div>
							
							<!--div class="col s12 m12 l9 offer-info" style="width:78%;">
								<div class="row">
									<div class="col s12 m12 l9">

										<h5 style="font-size:20px; color:#7864e9 !important;"><strong><?php echo strtoupper($off['offer_title']);?></strong></h5>

										<?php $bus_id = $this->user_session['bus_id']; ?>
										<div class="col s12 m12 l4" style="margin: -35px 0px 4px 95%;">
											<a class="btn btn-theme btn-large right set_remin" href="<?php echo base_url();?>settings/add-offer-activity-alert/<?php echo $off['id'];?>" target="_blank"><img src="<?php echo base_url();?>public/images/clock.png" style="width: 20px; margin: 0px 0px -6px 0px;">&nbsp; SET REMINDER</a>

										</div>

										<?php if ($off['bus_id'] == $bus_id) {?>
										<div class="col s12 m12 l4" style="margin: -35px 0px 0px 130%;">
											<a href="javascript:void(0);" class="waves-effect waves-block waves-light event-button" data-activates="event-dropdown<?=$off['id']?>"><i class="material-icons">edit</i></a>
											<ul id="event-dropdown<?=$off['id']?>" class="event-dropdown dropdown-content user-profile-down" style="left: 1007.33px !important; top: 358.484px !important;">

						              <li class="user-profile-fix">
						                <ul>
															<li>
						                    <a href="<?php echo base_url();?>community/edit-offer/<?php echo $off['id'];?>"><i class="material-icons" style="color: #000;">mode_edit</i>EDIT</a>
						                  </li>
															<li>
						                    <a href="javascript:void(0);" class="deactive_deal" data-deal_id="<?=$off['id']?>"><i class="material-icons">delete</i>DELETE</a>
						                  </li>
														</ul>
													</li>
												</ul>
										</div>
										<?php }?>

										<div class="col s12 m12 l7" style="margin-left:-12px;">
											<p class="offer_p"><strong>VALIDITY DATE:</strong> <?php echo date("jS M, y", strtotime($off['start_date'])); ?> to <?php echo date("jS M, y", strtotime($off['end_date'])); ?></p>
										</div>

										<div class="col s12 m6 l5">
											<p class="offer_p"><strong>VALIDITY TIME:</strong> <?php echo $off['start_time'];?> to <?php echo $off['end_time'];?></p>
										</div>
										<div class="col s12 m12 l12" style="margin: -15px 0 0 0px;">
											<p class="offer_p" style="margin-left: -12px !important;"><strong>WEBSITE:</strong> <?php echo $off['website_link'];?></p>
										</div>
									</div>


								</div>

								<div class="row">
									
								</div>

								<div class="row offer-time" id="morelist<?php echo $off['id'];?>">
									<div class="col s12 m12 l12 offer_description" style="margin: -10px 0px -10px 0 !important;">
										<p style="text-align: justify;"><?php echo $off['offer_description'];?></p>
									</div>
									<div class="col s12 m12 l12">
										<div class="col s12 m12 l5" style="margin: 0 12px 0 -13px;">
											<p class="offer_p"><strong>COMPANY NAME:</strong> <?php echo $off['company_name'];?></p>
										</div>
										<div class="col s12 m12 l6">
											<p class="offer_p"><strong>CONTACT NAME:</strong> <?php echo $off['contact_name'];?></p>
										</div>
											<div class="col s12 m12 l5" style="margin:-12px 12px 0 -13px;">
												<p class="offer_p"><strong>EMAIL:</strong> <?php echo $off['email'];?></p>
										</div>
										<div class="col s12 m12 l6" style="margin-top:-12px;">
											<p class="offer_p"><strong>MOBILE NUMBER:</strong> <?php echo $off['mobile_no'];?></p>
										</div>
									</div>
								</div>

								<div class="row" style="text-align: center; margin: 0px 0 0 -200px;">
									<div class="col l12 s12 m12">
										<a style="font-family:Tahoma;" href="javascript:void(0);" id="showmorelist<?php echo $off['id'];?>" class="more showmore active" title="Show More" onclick="showmorelist(<?php echo $off['id'];?>)">SHOW MORE</a>
										<a style="font-family:Tahoma;" href="javascript:void(0);" id="less<?php echo $off['id'];?>" class="less showmore" title="Show More" onclick="showmorelist(<?php echo $off['id'];?>)" hidden>SHOW LESS</a>
									</div>
								</div>

							</div-->
						</div>
						</div>

					<?php }} else{ ?>
						<div class="col s12 m12 l4"></div>
						<div class="col s12 m12 l4">
						<div class="text-center" style="margin: 0 0 10px 0px;">
						<div style="background: #ff85a1; width: 500px; padding: 0px 5px 0px 5px; border-radius: 50px; color: white; height: 50px; line-height: 50px; font-size: 18px; margin-left:8%;">
							<?php if($search['offer_company']!=""){ ?>
								<p>Currently, there are no deals being offered by this company</p>
							<?php }else{ ?>
								<p>Currently, there are no deals available in this category</p>
							<?php } ?>	
						</div>
						</div>
						</div>
						<div class="col s12 m12 l4"></div>
					<?php }?>

					</div>
				</div>
				</div>
				<div class="row"><div class="col s12 m12 l12"><p></p></div></div>
				<div class="row"><div class="col s12 m12 l12"><p></p></div></div>
				<div class="row"><div class="col s12 m12 l12"><p></p></div></div>
            </div>
        </div>


		</section>
	</div>
</div>

<div id="send_email_deal_modal" class="modal modal-md ps-active-y" style="margin-top:-45px !important; max-width:510px !important;">
	<?php $this->load->view('my-community/email-popup-deal'); ?>
</div>

<div id="remove_deal" class="modal" style="width:30% !important;">
	<img class="geen" src="<?php echo base_url();?>asset/images/green.png" alt="green">
	<div class="modal-content">
		<div class="modal-header" style="text-align:right; margin-left:0%;">
			<h4> Delete Deal</h4>
			<input type="hidden" id="remove_deal_id" name="remove_deal_id" value="" />
			<a class="modal-close close-pop"><img src="<?php echo base_url();?>asset/images/popupdelete.png" alt="delete" ></a>
		</div>
	</div>
	<div class="modal-body" style="margin-left: 0px !important; width: 100%;">
		<p style="font-size:18px !important; color:#595959 !important; text-align:center;">Are you sure you want to delete this deal?</p>
	</div>
	<div class="modal-content">
	<div class="modal-footer">
		<div class="row">
			<div class="col l4 s12 m12"></div>
			<div class="col l8 s12 m12 cancel-deactiv">
				<a class=" modal-close btn-flat theme-flat-btn theme-btn theme-btn-large model-cancel " type="button">CANCEL</a>
				<button id="deactive_deal" class="btn-flat theme-primary-btn theme-btn theme-btn-large  modal-trigger dea modal-close deactive_this_service">DELETE</button>
			</div>
		</div>
	</div>
	</div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
		$('.js-example-basic-single').select2();
	});

	$(document).ready(function () {
			let dealsList = $('#dealsList .deals-card'); // Get all divs
			var pageLength =  localStorage.getItem('dealsTable_length') || 10;
			let table = $('<table id="dealsTable"><thead><tr><th>Hidden</th></tr></thead><tbody></tbody></table>'); 
			$('body').append(table.hide()); // Append hidden table for pagination

			// Insert each div as a row in the fake table
			dealsList.each(function () {
				$('#dealsTable tbody').append('<tr><td></td></tr>');
			});

			// Initialize DataTables
			let dataTable = $('#dealsTable').DataTable({
				"bPaginate": true,
			    "bLengthChange": true,
    			"sDom": 'Rfrtlip',
    			"bFilter": true,
    			"bInfo": false,
    			"searching":false,
				"iDisplayLength": parseInt(pageLength),
				"stateSave": true,
    			//"iDisplayLength": 10,
				lengthMenu: [
					[ 10, 20, 30, 50, -1 ],
					[ 'SHOW 10', 'SHOW 20', 'SHOW 30', 'SHOW 50', 'SHOW ALL' ]
				],
			});

			// Hide all divs initially and only show the first page
			function showPage() {
				dealsList.hide();
				let pageIndexes = dataTable.rows({ page: 'current' }).indexes();
				pageIndexes.each(function (index) {
					dealsList.eq(index).show();
				});
			}

			showPage(); // Show initial page

			// Update visible divs on page change
			$('#dealsTable').on('page.dt length.dt', function () {
				showPage();
			});
		});
</script>
<script type="text/javascript">
    $(document).ready(function() {
		window.onbeforeunload = function() {};
		window.onbeforeunload = null;
		if ( window.history.replaceState ) {
			window.history.replaceState( null, null, window.location.href );
		}
		$(".offer-time").hide();
		
		$('#offer_category').change(function(){
			if($(this).val() != ""){	
				$('#submit1').addClass('mandate');
			}
		});
		$('#offer_company').change(function(){
			if($(this).val() != ""){	
				$('#submit1').addClass('mandate');
			}
		});
		$('#offer_end_date').change(function(){
			if($(this).val() != ""){	
				$('#submit1').addClass('mandate');
			}
		});
		$(".akshdkajsd").click(function(){
			if($(this).hasClass('active')){
				$(".offer-time").hide();
				$(".showmore").removeClass('active');
				$('.less').hide();
				$('.more').show();
			}else{
				$(".offer-time").show();
				$(".showmore").addClass('active');
				$('.less').show();
				$('.more').hide();
			}
		});

		$('.deactive_deal').on('click',function(){
			var deal_id = $(this).data('deal_id');
			$('#remove_deal').modal('open');
			$('#remove_deal #remove_deal_id').val(deal_id);
		});

		$('#deactive_deal').off().on('click',function(){
			var deal_id = $('#remove_deal_id').val();
			if(csrf_hash===""){
				csrf_hash=csrf_hash;
			}
			$.ajax({
				url:base_url+'Community/delete_deal',
				type:"POST",
				data:{'csrf_test_name':csrf_hash,"deal_id":deal_id,},
				success:function(res){
					var myArray = res.split("@");
					if(myArray[1]){
						csrf_hash=myArray[1];
					}
					if(myArray[0] == 'true'){
						Materialize.toast('Your Deal has been successfully deleted', 3000,'green rounded');
						setTimeout(function(){
						    window.location.href=base_url+'Community/deals';
						}, 2500);    
					}else{
						//window.location.href=base_url+'Community/deals';
						Materialize.toast('Error while processing!', 2000,'red rounded');
					}
				},
			});
		});
	});
	
	function email_deals($id){

		var deal_id = $id;
		$('#send_email_deal_modal').modal('open');
		$('#send_email_deal_modal #deal_id').val(deal_id);

	}
	
	function reset_clientfilter() {
		//location.href=base_url+'community/offers';
	}

	function formsubmit() {
		var category= $("#offer_category").val();
		var offer_company= $("#offer_category").val();
		var c_start_date= $("#c_start_date").val();
		var c_end_date= $("#c_end_date").val();
		if(category !="" && offer_company!="" && c_start_date!="" && c_end_date!=""){
			document.getElementById("search_filter").submit();
		}
		location.reload(true);
	}

	function showmorelist(id){
		if($("#showmorelist"+id).hasClass('active')){
				$("#offer-time"+id).hide();
				$("#showmorelist"+id).removeClass('active');
				$('#less'+id).show();
				$('#showmorelist'+id).hide();
				$('#morelist'+id).show();
				$('#morelist1'+id).show();
			}else{
				$("#offer-time"+id).show();
				$("#showmorelist"+id).addClass('active');
				$('#showmorelist'+id).show();
				$('#less'+id).hide();
				$('#morelist'+id).hide();
				$('#morelist1'+id).hide();
			}
	}
</script>


<?php $this->load->view('template/footer'); ?>
