<?php $this->load->view('my-community/Cohorts-header');?>
<title>Xebra Cohorts login page; a community to showcase your company & achievements</title>
<meta name="description" content="Login to the Xebra Cohorts,  a platform that offers growth and a community to discuss ideas and thoughts. Showcase your work, organise events and offer deals to scale your business. Understand financial and accounting terms to get a better understanding of numbers">
<meta name="keywords" content="Xebra Cohorts Intro, Xebra Cohorts Introduction, Founder message">
<style>
	
	p{
		text-align: justify;
	}
	
	.direct:hover {
		color: #7863e8 !important;
	}
	
	@media only screen and (max-width: 600px) {
		img.logo_style_2 { width:100%; height:100%; }	
	}

</style>

<!-- START MAIN -->
<div id="main" style="padding:2% 0 0 0px !important; background-color:#fff; height:auto;">
	<!-- START WRAPPER -->
	<div class="wrapper">
		<!-- START CONTENT -->
		<section id="content">
			<?php $message = $this->session->flashdata('error');
						if($message){
							echo '<div class="pro toast red rounded" style="opacity: 1; max-width: 90% !important; top: 10% !important; left: 55% !important; position: fixed !important; transform: translate(-50%, 0px) !important; z-index: 9999 !important;">' . $message . '</div>';
						}
					?>
					<?php $message = $this->session->flashdata('success');
						if($message){
							echo '<div class="pro toast green rounded" style="opacity: 1; max-width: 90% !important; top: 10% !important; left: 55% !important; position: fixed !important; transform: translate(-50%, 0px) !important; z-index: 9999 !important;">' . $message . '</div>';
						}
					?>
			<div class="row" style="padding:3.5% 0;">
				<div class="col s12 m12 l12">
					<div class="col s12 m12 l1"></div>
					<div class="col s12 m12 l4" style="padding:4% 0;">
						<img width="410" height="330" style="" src="<?php echo base_url(); ?>public/images/cohort-welcome.png" alt="Logo" class="text-hide-collapsible logo_style_2"/>
					</div>
					<div class="col s12 m12 l6">
						<div class="row">
							<p>Welcome to Xebra Cohorts</p>
							<p>
								It’s a platform for you to leverage the power of a group, a Cohort! A cohort that you can use as a sounding board or share your success, failure, emotions and grind that went behind building your venture. Use it as a platform to talk about your emotional health and wellbeing as well as growth and revenues.
							</p>
							<p>
								The irony of entrepreneurship is that while all of us go through a relatively similar set of successes and failures, the journey ends up being lonely for most times. We, too, felt it ourselves and decided to bring as many entrepreneurs together on a single platform to leverage the power of a group, 
							</p>
							<p>
								We have a marketplace where you can list your company and generate business from the eco-system. We keep updating it with deals that you encash and leverage for your business.  We also will be sharing details about industry-led events taking place and how to participate in it. 
							</p>
							<p>
								We would love to hear about your experiences with Xebra Cohorts and how we can improvise it. Do send in your ideas and suggestions to <a style="color:#7864e9;" href="mailto:cohorts@xebra.in">cohorts(at)xebra.in</a> 
							</p>
						</div>	
						<div class="row">
							<div class="col s12 m12 l2" style="margin-right:-15px;">
								<img style="border-radius:50%;" width="80" height="80" style="" src="<?php echo base_url(); ?>public/images/nimesh.JPG" alt="Logo" class=""/>
							</div>
							<div class="col s12 m12 l4">
								<p style="padding-top:4px;"><a class="direct" style="color: #000;" href="https://www.linkedin.com/in/nimesh9" target="_blank" rel="noopener noreferrer"><strong>Nimesh Shah</strong></a></br>CEO - Xebra Cohorts</p>
							</div>
							
						</div>
						<div class="row" style="margin-left:10%;">
							<?php 
								$reg_id=$this->session->userdata['user_session']['reg_id'];
								$bus_id=$this->session->userdata['user_session']['bus_id'];
								if($this->session->userdata['user_session']['reg_admin_type'] == 3 || $this->session->userdata['user_session']['reg_admin_type'] == -1){
								$bus_id_reg=$this->Adminmaster_model->selectData("businesslist","*",array('reg_id'=>$reg_id));
								if(count($bus_id_reg)>0){
							?>
							<center><a style="color:#7864e9; margin-top: -46px;" class="nav-link" href="<?= base_url(); ?>community/manage_profile?token=<?php echo $bus_id_reg[0]->bus_id; ?>">LET'S BEGIN BY CREATING YOUR CORPORATE PROFILE, IF YOU HAVEN'T</a> <!--| <a style="color:#7864e9; margin-top: -46px;" class="nav-link" href="<?//= base_url(); ?>community/my-connections">See Connections</a> | <a style="color:#7864e9; margin-top: -46px;" class="nav-link" href="<?//= base_url(); ?>community/my-marketplace">Marketplace</a>--></center>
							<?php }else{ ?>
								<center><a style="color:#7864e9; margin-top: -46px;" class="nav-link" href="<?= base_url(); ?>community/my-profile">LET'S BEGIN BY CREATING YOUR CORPORATE PROFILE, IF YOU HAVEN'T</a> <!--| <a style="color:#7864e9; margin-top: -46px;" class="nav-link" href="<?//= base_url(); ?>community/my-connections">See Connections</a> | <a style="color:#7864e9; margin-top: -46px;" class="nav-link" href="<?//= base_url(); ?>community/my-marketplace">Marketplace</a>--></center>
							<?php } } ?>
						</div>
					</div>					
					<div class="col s12 m12 l1"></div>
					
				</div>
			</div>
		</section> <!-- END CONTENT -->
	</div> <!-- END WRAPPER -->
</div> <!-- END MAIN -->


<script type="text/javascript">
	$('.js-example-basic-multiple').select2();
</script>
<?php $this->load->view('template/footer.php');?>