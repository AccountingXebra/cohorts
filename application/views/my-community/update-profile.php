<?php $this->load->view('my-community/Cohorts-header');?>

  <style type="text/css">
  .com-nature + label.error {
    margin: 0px 0 0 -10px !important;
  }
  
  .url label.error {
    margin-top: -10px !important;
  }
  
  .url label.error.active {
    margin-top: 0px !important;
  }
  
  .tooltip {
    position: relative;
    display: inline-block;
    margin-bottom:-6px !important;
  }

  .tooltip .tooltiptext {
    visibility: hidden;
    width: 150px;
    background-color: #7864e9;
    color: #fff;
    text-align: center;
    font-size:13px !important;
    border-radius: 6px;
    padding: 5px 0;
    margin: 20px 0 -20px 0;
    /* Position the tooltip */
    position: absolute;
    z-index: 1;
  }

  .tooltip:hover .tooltiptext {
    visibility: visible;
  }
  
  .uploader-placeholder{
    height:90px !important;
  }
  
  .date-start.end-date a.dropdown-button {
     padding: 0px;
   }
  .li-autocomplete{
  padding: 2px 0px 2px 20px !important;

  box-shadow: 0 0 1px 0 #bbb !important;

  background-color: #fff !important;
  }
  .ul-autocomplete{
    z-index: 99999 !important;

  }
  .select-wrapper label.error:not(.active) {
     margin: -30px 0 0 -11px;
  }
  /*----------START SEARCH DROPDOWN CSS--------*/
.select2-container {
  width: 100% !important;
}
.select2-container--default .select2-selection--single {
  border:none;
}
input[type="search"]:not(.browser-default) {
  height: 30px;
  font-size: 14px;
  margin: 0;
  border-radius: 5px;

}
.select2-container--default .select2-selection--single .select2-selection__rendered {
  font-size: 14px;
  line-height: 35px;
  color: #000;
  font-weight: 500;
}
.select2-container--default .select2-selection--single .select2-selection__arrow {
  height: 32px;
}
.select2-search--dropdown {
  padding: 0;
}
input[type="search"]:not(.browser-default):focus:not([readonly]) {
  border-bottom: 1px solid #bbb;
  box-shadow: none;
}
.select2-container--default .select2-selection--single:focus {
    outline: none;
}
.select2-container--default .select2-results__option--highlighted[aria-selected] {
    background: #fffaef;
  color: #666;
}
.select2-container--default .select2-results > .select2-results__options {
  font-size: 14px;
  border-radius: 5px;
  box-shadow: 0px 2px 6px #B0B7CA;
}
.select2-dropdown {
  border: none; 
  border-radius: 5px;
}
.select2-container .select2-selection--single {
  height: 53px;
}
.select2-results__option[aria-selected] {
  border-bottom: 1px solid #f2f7f9;
  padding: 14px 16px;
  border-top-left-radius: 0;
}
.select2-container--default .select2-search--dropdown .select2-search__field {
   /* border: none;*/
    border: 1px solid #d0d0d0;
    padding: 0 0 0 15px !important;
    width: 93.5%;
    max-width: 100%;
    background: #fff;
    border-radius: 4px;
    border-top-left-radius: 4px;
    border-top-right-radius: 4px;
}
.select2-container--open .select2-dropdown--below {
  margin-top: -15px;
/*  border-top: 1px solid #aaa;
  border-radius: 5px !important;*/
}
ul#select2-currency-results {
  width: 97%;
}
/*---Dropdown error message format---*/
.select-wrapper + label.error{
 margin: 18px 0 0 -10px;

}
select.error + label.error:not(.active){

 margin: -20px 0 0 -10px;
}
select + label.error.active{
  margin-left: -10px;
}

#pincode-error{
  top:-6px !important;
}
/*---End dropdown error message format---*/
/*.select2-container--open .select2-dropdown--above{
    border-top: 1px solid #aaa;
    border-radius: 5px !important;
}*/

/*----------END SEARCH DROPDOWN CSS--------*/
  #edit_company_profile .datefor .dropdown-content.select-dropdown {
    height:20vh !important;
    overflow-y: scroll !important;
  }
  
  #edit_company_profile .decpoint .dropdown-content.select-dropdown {
    height:26vh !important;
    overflow-y: scroll !important;
  }
  
  #edit_company_profile .dropdown-content.select-dropdown {
    height:30vh !important;
    overflow-y: scroll !important;
  }
  
  .footer_btn{
    height:35px !important;
    line-height: 35px;
  }

  .country-dropdown span.caret {
    margin: 20px 15px 0 0 !important;
  }
  
  #add-card .card-drop .select-dropdown {
    height:13vh !important;
    overflow-y: none !important;
  }
  
  .country-dropdown .select-dropdown {
    height:8vh !important;
    overflow-y: scroll !important;
  }

  /*.gst-country span.caret {
    margin: 28px 15px 0 0 !important;
  }*/
  
  #add_gst_frm_2 .select-wrapper{
    padding-top:0px !important;
    margin-top:10px !important;
  }
  
  /*#add_gst_frm_2 .gst_state span.caret {
    margin: 28px 15px 0 0 !important;
  }*/

  /*#edit_company_gstmodal .select-wrapper span.caret {
      margin: 30px 12px 0 0;
  }*/
  /*.modal input.select-dropdown {
    margin: -10px 0 0 0 !important;
  }*/
  
  .ml-5 {
    margin-left: 3% !important;
  }
  .deactive_record{
    pointer-events: none;
    opacity: 0.5;
    background: #f2f2f2;
  }
  
  .info-ref{
    text-align: justify !important;
  }
  
   </style>
   <!-- START MAIN -->
    <div id="main">
      <!-- START WRAPPER -->
      <div class="wrapper">
        <!-- START LEFT SIDEBAR NAV-->
        <?php //$this->load->view('template/sidebar.php');?>
        <!-- END LEFT SIDEBAR NAV-->
        <!-- //////////////////////////////////////////////////////////////////////////// -->
        <!-- START CONTENT -->
        <form class="create-company-form border-split-form" name="edit_company_profile" id="edit_company_profile" method="post" action="<?php echo base_url(); ?>profile/update_company_profile" enctype="multipart/form-data">
          <section id="content">
		  <?php $csrf = array(
					'name' => $this->security->get_csrf_token_name(),
					'hash' => $this->security->get_csrf_hash()
				);
				?>
				<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
            <div class="container">
              <div class="plain-page-header">
                <div class="row">
                  <div class="col l6 s12">
                    <a class="go-back underline" href="<?php echo base_url();?>profile/company_profile">Back to My Company Profiles</a>
                  </div>
                  <div class="col l6 s12 m6">
                  </div>
                </div>
              </div>
              <div class="page-content">
                <div class="row">
        <?php foreach($company_profile as $row){ ?>
                  <div class="col s12 m12 l3"></div>
                  <div class="col s12 m12 l6">
                    <div class="box-wrapper bg-white bg-img-green shadow border-radius-6">
                      <div class="box-header">
                        <h3 class="box-title">Edit Company Profile</h3>
                      </div>
                      <div class="box-body">
                        <div class="row">
                          <div class="col s12 m12 l12">
                            <div class="col s12 m12 l9">
                              <div class="row">
                                <div class="input-field">
                    <input type="hidden"  name="id" id="id" value="<?php echo $row->bus_id; ?>"/>
                                  <input type="hidden"  name="userid" id="userid" value="<?php echo $row->reg_id; ?>"/>
                                  <label for="company_name" class="full-bg-label">Company Name<span class="required_field">*</span></label>
                                  <input id="company_name" name="bus_company_name" class="full-bg adjust-width" type="text" value="<?php echo $row->bus_company_name; ?>"  style="height:60px !important;">
                                </div>
                              </div>
                            </div>
                            <div class="col s12 m12 l3">
                              <div class="row">
                                <div class="input-field tooltip">
                                  <div class="uploader-placeholder">
                   <?php if($row->bus_company_logo == ""){?>
                   <label class="up_pic" style="font-size:11px; color:#696969; margin-top:-8px !important; margin-left:30px;">Upload Logo</label>
                   <?php } ?>
                                     <input type="file" class="hide-file" id="company_logo" name="bus_company_logo"value="<?php echo $row->bus_company_logo; ?>">
                   <input type="hidden" id="image_info" name="image_info" value="<?php echo $row->bus_company_logo; ?>" />
                                    <input type="hidden" id="img_folder" name="img_folder" value="<?php echo "company_logos/$row->bus_id";  ?>">
                  <span class="tooltiptext">Only JPG, JPEG & PNG format. Upto 25MB</span> 
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                            <div class="col s12 m12 l12">
                              <div class="input-field">
                                <label for="billaddress" class="full-bg-label">BILLING ADDRESS<span class="required_field">*</span></label>
                                <input id="billing_address" name="bus_billing_address" class="full-bg adjust-width" type="text" value="<?php echo $row->bus_billing_address; ?>">
                              </div>
                            </div>
                          </div>
                        <div class="row">
                          <div class="col s12 m12 l12">
                            <div class="col s12 m12 l6 input-set" id="countrysec">
                              <div class="input-field label-active">
                               <label for="country" class="full-bg-label select-label">Country<span class="required_field"></span></label>
                                <select class="js-example-basic-single" name="bus_billing_country" id="country">
                                     <option value="">SELECT COUNTRY</option>
                                      <?php foreach ($countries as $country) { ?>
                                        <option value="<?php echo $country->country_id; ?>" <?php echo ($country->country_id == $row->bus_billing_country)?'selected':''; ?>><?php echo strtoupper($country->country_name); ?></option>
                                      <?php } ?>
                                  </select>
                              </div>
                            </div>
                            <div class="col s12 m12 l6 input-set" id="statesec">
                              <div class="input-field label-active">
                                <label for="state" class="full-bg-label select-label state_label">State<span class="required_field"></span></label>
                                  <select class="js-example-basic-single" name="bus_billing_state" id="state">
                                         <option value="">SELECT STATE</option>
                                         <?php if($state!=''){
                                      foreach($state as $st){
                                      ?>
                                        <option value="<?php echo @$st->state_id; ?>" <?php echo ($row->bus_billing_state == $st->state_id)?"selected":""; ?>><?php echo strtoupper($st->state_name.' ('.$st->state_gst_code.')'); ?></option>
                                      <?php } }?>
                                  </select>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col s12 m12 l12">
                            <div class="col s12 m12 l6 input-set border-field" id="citysec">
                              <div class="input-field label-active">
                                <label for="city" class="full-bg-label select-label">City<span class="required_field"></span></label>
                                  <select class="js-example-basic-single" name="bus_billing_city" id="city">
                                   <option value="">SELECT CITY</option>
                                   <?php
                                    if($cities != '') {
                                    foreach($cities as $ct) {?>
                                          <option value="<?php echo $ct->city_id; ?>" <?php echo ($ct->city_id == $row->bus_billing_city)?'selected':'';?>><?php echo strtoupper($ct->name); ?></option>
                                    <?php } }?>
                                  </select>
                              </div>
                            </div>
                            <div class="col s12 m12 l6 input-set border-field border-right-none">
                              <div class="row">
                                <div class="input-field">
                                    <label for="zipcode" class="full-bg-label">Pin Code<span class="required_field"></span></label>
                                    <input id="pincode" name="bus_billing_zipcode" class="full-bg adjust-width" type="text" value="<?php echo $row->bus_billing_zipcode; ?>">
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>

<!--                         <div class="row">
                            <div class="col s12 m12 l12">

                              <div class="col s12 m12 l6">
                                <div class="row">
                                  <div class="input-field border-field ">
                                    <label for="shipping" class="full-bg-label">SHIPPING ADDRESS<span class="optionalss"></span></label>
                                    <input id="shipping_address" name="bus_shipping_address" class="full-bg adjust-width shipp-add" type="text" value="<?php echo $row->bus_shipping_address; ?>">
                                  </div>
                                </div>
                              </div>
                              <div class="col s12 m12 l6 border-botom-field border-field">
                                <div class="drop-down invoice-no">
                                  <div class="col s12 chkbox2">
                                    <input type="checkbox" id="same-bill" name="same-bill">
                                    <label class="checkboxx2" for="same-bill"><span class="check-boxs">Same as billing address</span></label>
                                  </div>
                                </div>
                              </div>

                           <div class="row">
                            <div class="col s12 m12 l12">
                                <div class="col s12 m12 l6 input-set label-active border-botom-field" id="shipcountrysec">
                                  <div class="input-field shipping_values">
                                    <label for="country" class="full-bg-label select-label"> </label>
                                      <select class="js-example-basic-single" name="bus_shipping_country" id="shipping_country">
                                         <option value="">SELECT COUNTRY</option>
                                            <?php foreach ($countries as $country) { ?>
                                              <option value="<?php echo $country->country_id; ?>" <?php echo ($country->country_id == $row->bus_shipping_country)?'selected':''; ?>><?php echo $country->country_name; ?></option>
                                            <?php } ?>
                                      </select>

                                  </div>
                                </div>


                                <div class="col s12 m12 l6 input-set label-active border-right-none border-botom-field" id="shipstatesec">
                                  <div class="input-field shipping_values">
                                    <label for="state" class="full-bg-label select-label"> </label>
                                    <select class="js-example-basic-single" name="bus_shipping_state" id="shipping_state">
                                         <option value="">SELECT COUNTRY</option>
                                              <?php
                                                if($shipping_state != '') {
                                                foreach($shipping_state as $state) {?>
                                                <option value="<?php echo $state->state_id; ?>" <?php echo ($state->state_id == $row->bus_shipping_state)?'selected':'';?>><?php echo $state->state_name; ?></option>
                                              <?php } }?>
                                      </select>
                                  </div>
                                </div>

                            </div>
                          </div>

                          <div class="row">
                            <div class="col s12 m12 l12">

                              <div class="col s12 m12 l6 input-set " id="shipcitysec">
                                <div class="input-field label-active shipping_values">
                                  <label for="country" class="full-bg-label  select-label">City</label>
                                    <select class="js-example-basic-single" name="bus_shipping_city" id="shipping_city">
                                         <option value="">SELECT COUNTRY</option>
                                              <?php
                                                if($shipping_cities != '') {
                                               foreach($shipping_cities as $ct) {?>
                                               <option value="<?php echo $ct->city_id; ?>" <?php echo ($ct->city_id == $row->bus_shipping_city)?'selected':'';?>><?php echo $ct->name; ?></option>
                                              <?php } }?>
                                      </select>

                                </div>
                              </div>

                              <div class="col s12 m12 l6 input-set border-right-none">
                                <div class="row">
                                  <div class="input-field">
                                      <label for="pincode" class="full-bg-label">PIN CODE</label>
                                      <input id="shipping_pincode" name="bus_shipping_zipcode" class="full-bg adjust-width border-top-none" type="text" value="<?php echo $row->bus_shipping_zipcode; ?>">
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>


                            </div>
                          </div> -->


                      </div>
                    </div>

                    <div class="step2">
                      <h4 class="box-inner-title" hidden>
                        Statutory Info
                        <!--a class="info-ref tooltipped info-tooltipped" data-position="bottom" data-delay="50" data-tooltip="Will be update, Coming Soon..!"></a-->
                      </h4>

                      <div class="box-wrapper bg-white bg-img-red shadow border-radius-6" hidden>
                        <div class="box-body">
                          <div class="row">
                            <div class="col s12 m12 l12">
                              <div class="col s12 m12 l6 input-set">
                                <div class="row">
                                  <div class="input-field">
                                      <a style="position: absolute;" class="sac info-ref tooltipped info-tooltipped" data-html="true" data-position="bottom" data-delay="50" data-tooltip=" PAN  is to be quoted during all the financial transactions of the <br/>Company as well as in the invoices and other registrations" data-tooltip-id="84856db1-7567-524a-fbcc-15c63776f866"></a>
                                    <label for="country" class="full-bg-label">PAN NUMBER<span class="required_field"></span></label>
                                     <input id="pan_num" name="bus_pancard" class="full-bg adjust-width border-top-none" type="text" value="<?php echo $row->bus_pancard; ?>" title="Please enter 10 digits for a valid PAN number">
                                  </div>
                                </div>
                              </div>
                              <div class="col s12 m12 l6 input-set">
                                <div class="row">
                                  <div class="input-field">
                                    <a style="position: absolute;" class="sac info-ref tooltipped info-tooltipped" data-html="true" data-position="bottom" data-delay="50" data-tooltip="Corporate Identity Numbet is a unique identification number assigned by Registrars of Companies (ROC)<br/>functioning in various states under Ministry of Corporate Affairs" data-tooltip-id="84856db1-7567-524a-fbcc-15c63776f866"></a>
                                      <label for="zipcode" class="full-bg-label">CIN NUMBER</label>
                                      <input id="cin" name="bus_cin_no" class="full-bg adjust-width border-top-none" type="text" value="<?php echo $row->bus_cin_no; ?>">
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>


                          <div class="row">
                            <div class="col l12 s12 m12">
                              <div class="input-field emty-field-row border-botom-field">
                                <div class="col l12 s12 m12">
                                  <div class="col l12 s12 m12">
                                    <h6 class="label" title="Please enter 15 digits for a valid GSTIN number">GSTIN<span class="required_field">*</span> <small class="badge-cover tot_gst"><?php echo ($gst_details != '')?count($gst_details):'0'; ?></small>
                                      <a class="modal-trigger right add_gst_2" data-company_id="<?php echo $row->bus_id; ?>" data-user_id="<?php echo $row->bus_id; ?>">
                                        <img src="<?php echo base_url(); ?>asset/css/img/icons/pluse.png" class="" alt="plus">
                                      </a>
                                    </h6>
                                  </div>
                                </div>
                              </div>

                              <div class="" id="company_gst_array_list_edit">
                              <?php
                              $i = 1;
                              if($gst_details != false) {
                              foreach($gst_details as $gst) {
                              ?>
                                <div class="<?php if($i>1){ echo "gstmore"; } ?>">
                                  <div class="gst-boxs add-gst">
                  <div class="col l1 s1 m1 sno <?php if($gst->status=='Inactive'){?> deactive_record <?php } ?>">
                                      <div class="row">
                                       <?php echo $i; ?>
                                      </div>
                                    </div>

                                    <div class="col l11 s1 m1">
                                      <div class="col l5 s5 m5 gstnum <?php if($gst->status=='Inactive'){?> deactive_record <?php } ?>">
                                        <span class="gstlabel">GST No</span><p><?php echo $gst->gst_no; ?></p>
                                      </div>
                                      <div class="col l6 s6 m6 gstplace <?php if($gst->status=='Inactive'){?> deactive_record <?php } ?>">
                                        <span class="gstlabel">Place Of Supply</span><p><?php echo $gst->place; ?></p>
                                      </div>
                                      <div class="col l1 s1 m1 gstaction">
                                        <a class="edit_remove_cmpgst" data-id="<?php echo $gst->gst_id; ?>" data-userid="<?php echo $gst->bus_id; ?>" data-companyid="<?php echo $gst->bus_id; ?>">
                                          <img src="<?php echo base_url(); ?>asset/images/delete.png" alt="delete" class="delete-icon">
                                        </a>
                                        <a style="cursor:pointer !important;" class="edit_gst_info" data-id="<?php echo $gst->gst_id; ?>" data-userid="<?php echo $gst->bus_id; ?>" data-companyid="<?php echo $gst->bus_id; ?>" data-gst_no="<?php echo $gst->gst_no; ?>" data-gst_place="<?php echo $gst->place; ?>" data-address="<?php echo $gst->address; ?>" data-country="<?php echo $gst->country; ?>" data-state_code="<?php echo $gst->state_code; ?>" data-city="<?php echo $gst->city; ?>" data-zipcode="<?php echo $gst->zipcode; ?>" data-email="<?php echo $gst->email; ?>" data-contact="<?php echo $gst->contact; ?>" data-contact="<?php echo $gst->status; ?>" >
                                          <i class="material-icons" style="color: #000;">mode_edit</i>
                                        </a>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <?php
                                $i++;
                                  }
                                }
                                ?>
                                </div>
                                <div class="row">
                                <div class="col l12 s12 m12 text-center">
                                  <a onclick="showGstMore()" id="showgstboxs" class="cursor"><i class="material-icons">keyboard_arrow_down</i></a>
                                </div>
                              </div>

                            </div>
                          </div>
                        </div>
                      </div>

                      <h4 class="box-inner-title" hidden>Bank Details<span class="required_field">*</span>
                        <!--a class="info-ref tooltipped info-tooltipped" data-position="bottom" data-delay="50" data-tooltip="Will be update, Coming Soon..!"></a-->
                      </h4>

                      <div class="box-wrapper bg-white bg-img-green-after bg-right shadow border-radius-6" hidden>
                        <div class="box-body">
                          <div class="row">
                            <div class="col s12 m12 l12">
                              <div class="input-field emty-field-row border-field">
                                <div class="col l12 s12 m12">
                                  <div class="col l12 s12 m12">
                                    <h6 class="label">Edit Bank Details <small class="tot_bank badge-cover"><?php echo count($bank_details); ?></small>
                                      <a href="#addbank_with_existing" class="right add_new_bank modal-trigger" type="button" data-user_id="<?php echo $row->reg_id; ?>" data-company_id="<?php echo $row->bus_id; ?>"></small>

                                        <img src="<?php echo base_url(); ?>asset/css/img/icons/pluse.png" class="" alt="plus">
                                      </a>

                                    </h6>
                                  </div>
                                </div>
                              </div>
                              <div  class="input-field" id="edit_company_bank_list">
                    <?php if($bank_details != '') {
                        $j = 1;
                         foreach($bank_details as $bank) { ?>
                            <div class="<?php if($j>1){ echo "bankmore"; } ?> popup-append-box-cover">
                              <div class="gst-boxs add-gst ">
                                <div class="col l1 s1 m1 sno">
                                  <div class="row">
                                    <?php echo $j; ?>
                                  </div>
                                </div>
                                <div class="col l11 s1 m1">
                                  <div class="col l5 s5 m5 gstnum  <?php if($bank->status=='Inactive'){?> deactive_record <?php } ?>">
                                    <span class="gstlabel">ENTER BANK ACCOUNT NUMBER</span><p><?php echo $bank->cbank_account_no;?></p>
                                  </div>
                                  <div class="col l6 s6 m6 gstplace  <?php if($bank->status=='Inactive'){?> deactive_record <?php } ?>">
                                    <span class="gstlabel">BANK NAME</span><p><?php echo $bank->cbank_name; ?></p>
                                  </div>
                                  <div class="col l1 s1 m1 gstaction">
                                    <a class="edited_bank_removed modal-trigger  <?php if($bank->status=='Inactive'){?> deactive_record <?php } ?>" data-cbank_id="<?php echo $bank->cbank_id; ?>" data-bus_id="<?php echo $row->bus_id; ?>" data-target="removed_existing_bank">
                                      <img src="<?php echo base_url(); ?>asset/images/delete.png" alt="delete" class="delete-icon">
                                    </a>
                                    <a class="edit_bankinfo modal-trigger" data-target="edit_bank_branch" data-acc_no="<?php echo $bank->cbank_account_no; ?>" data-cbank_name="<?php echo $bank->cbank_name; ?>" data-cbank_branch_name="<?php echo $bank->cbank_branch_name; ?>" data-cbank_account_type="<?php echo $bank->cbank_account_type; ?>" data-cbank_ifsc_code="<?php echo $bank->cbank_ifsc_code; ?>"
                                    data-cbank_swift_code="<?php echo $bank->cbank_swift_code; ?>" data-cbank_swift_code="<?php echo $bank->cbank_swift_code; ?>" data-cbank_currency_code="<?php echo $bank->cbank_currency_code; ?>" data-cbank_currency="<?php echo $bank->currency; ?>" data-cbank_currencycode="<?php echo $bank->currencycode; ?>" data-cbank_id="<?php echo $bank->cbank_id; ?>" data-country_id="<?php echo $bank->cbank_country_code; ?>" data-country_name="<?php echo $bank->country_name; ?>" data-bus_id="<?php echo $row->bus_id; ?>" data-opening_balance="<?php echo $bank->opening_bank_balance; ?>" data-opening_balance_date="<?php echo $bank->opening_balance_date; ?>"><i class="material-icons" style="color: #000;">mode_edit</i></a>
                                  </div>
                                </div>
                              </div>
                            </div>
                           <?php $j++; } } ?>
                            </div>
                            </div>
                          </div>
                          <div class="row">
                              <div class="col l12 s12 m12 text-center">
                                <a onclick="showMoredata('bankmore','showbankboxs')" id="showbankboxs" class="cursor"><i class="material-icons">keyboard_arrow_down</i></a>

                                <!-- <a class="showmore-row cursor"><i data-id="company_bank_list" class="material-icons">keyboard_arrow_down</i></a> -->
                              </div>
                            </div>
                        </div>
                      </div>
            
            <h4 class="box-inner-title" hidden>Debit & Credit Card details</h4>

                      <div class="box-wrapper bg-white bg-img-green-after bg-right shadow border-radius-6" hidden>
                        <div class="box-body">
                          <div class="row">
                            <div class="col s12 m12 l12">
                              <div class="input-field emty-field-row border-field">
                                <div class="col l12 s12 m12">
                                  <div class="col l12 s12 m12">
                                    <h6 class="label">Edit Debit & Credit Card details <small class="badge-cover tot_card"><?php echo count($card_details); ?></small>
                                      <a href="#edit-card" class="right modal-trigger add_new_card">
                                        <img src="<?php echo base_url(); ?>asset/css/img/icons/pluse.png" class="" alt="plus">
                                      </a>
                                    </h6>
                                  </div>
                                </div>
                              </div>
                            <div  class="input-field" id="edit_company_card_list">
              <?php if($card_details != '') {
              $k = 1;
              foreach($card_details as $card) { ?>
              <div class="<?php if($k>1){ echo "cardmore"; } ?> popup-append-box-cover">
                              <div class="gst-boxs add-gst">
                                <div class="col l1 s1 m1 sno">
                                  <div class="row">
                                    <?php echo $k; ?>
                                  </div>
                                </div>
                                <div class="col l11 s1 m1">
                                  <div class="col l5 s5 m5 gstnum  <?php if($card->status=='Inactive'){?> deactive_record <?php } ?>">
                                    <span class="gstlabel">CARD TYPE</span><p><?php echo $card->card_type;?></p>
                                  </div>
                                  <div class="col l6 s6 m6 gstplace  <?php if($card->status=='Inactive'){?> deactive_record <?php } ?>">
                                    <span class="gstlabel">BANK NAME</span><p><?php echo $card->bank_name; ?></p>
                                  </div>
                                  <div class="col l1 s1 m1 gstaction">
                                    <a class="edited_card_removed modal-trigger  <?php if($card->status=='Inactive'){?> deactive_record <?php } ?>" data-ccard_id="<?php echo $card->ccard_id; ?>" data-bus_id="<?php echo $row->bus_id; ?>" data-target="removed_existing_card" >
                                      <img src="<?php echo base_url(); ?>asset/images/delete.png" alt="delete" class="delete-icon">
                                    </a>
                  <a class="edit_cardinfo modal-trigger" data-target="edit-card" data-ccard_id="<?php echo $card->ccard_id; ?>" data-bus_id="<?php echo $row->bus_id; ?>" data-card_type="<?php echo $card->card_type; ?>" data-issue_bank="<?php echo $card->bank_name; ?>" data-status="<?php echo $card->status; ?>"><i class="material-icons" style="color: #000;">mode_edit</i></a>
                                  </div>
                                </div>
                              </div>
                            </div>
                           <?php $k++; } } ?>
                            </div>
                            </div>
                          </div>
                          <div class="row">
              <div class="col l12 s12 m12 text-center">
                <a onclick="showMoredata('cardmore','showcardboxs')" id="showcardboxs" class="cursor"><i class="material-icons">keyboard_arrow_down</i></a>
              </div>
              </div>
                        </div>
                      </div>

            <a data-html="true" style="text-align:center;" class="info-ref tooltipped info-tooltipped" data-position="top" data-delay="50" data-tooltip="<p>1. You can integrate with our third-party payment gateway to add 'Make Payment' </br> in your invoice so that your clients can pay you directly with a click. </p> <p style='text-align:left;'>2. You will incur a fee of 2% + Rs 3 for every transaction carried out.</p>"></a>
           
                      <div class="box-wrapper bg-white lable-badge shadow border-radius-6" style="margin-top:10px !important;" hidden>
                        <div class="box-body">
                          <div class="row">
                            <div class="col s12 m12 l12">
                              <div class="row">
                                <div class="col l12 m12 s12">
                                  <div class="long-lable">
                                  <a href='#instamojo_modal' data-toggle='modal' class='modal-trigger modal-close' style="color: #fff;"> Integrate payment Gateway <i class="material-icons right">chevron_right</i></a>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>

                      <h4 class="box-inner-title afetr-bg-title" style="margin: 33px 0 0 0;" hidden>Customization
                        <!--<a class="info-ref tooltipped info-tooltipped" data-position="bottom" data-delay="50" data-tooltip="Will be update, Coming Soon..!"></a>-->
                      </h4>

                      <div class="box-wrapper bg-white no-bg shadow border-radius-6" hidden>
                        <div class="box-body">
                          <div class="row">
                            <div class="col s12 m12 l12">
                              <div class="col s12 m12 l6 input-set">
                                <div class="row">
                                  <div class="input-field">
                                    <label for="fy_start_date" class="full-bg-label green-txt">Financial Year Start Date<span class="required_field">*</span></label>
                                    <input id="fy_start_date" name="bus_fy_startdate" class="full-bg icon-calendar-green adjust-width border-top-none rangedatepicker_customize" type="text" value="<?php echo ($row->bus_fy_startdate !='' && $row->bus_fy_startdate !='0000-00-00')?date("d/m/Y",  strtotime($row->bus_fy_startdate)):''; ?>">
                                  </div>
                                </div>
                              </div>

                              <div class="col s12 m12 l6 input-set">
                                <div class="row">
                                  <div class="input-field">
                                    <label for="fy_end_date" class="full-bg-label red-txt">Financial Year End Date<span class="required_field">*</span></label>
                                    <input id="fy_end_date" name="bus_fy_enddate" class="full-bg icon-calendar-red adjust-width border-top-none rangedatepicker_customize" type="text" value="<?php  echo ($row->bus_fy_enddate != '' && $row->bus_fy_enddate != '0000-00-00')?date("d/m/Y",  strtotime($row->bus_fy_enddate)):''; ?>" readonly>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="row">
                            <div class="col s12 m12 l12">
                              <div class="col s12 m12 l6 input-set" id="currencysec">
                                <div class="input-field label-active">
                                  <label for="select_currency" class="full-bg-label select-label">Select Currency<span class="required_field">*</span></label>
                                     <select class="js-example-basic-single" name="bus_currency_format" id="currency_format">
                                    <option value="">SELECT CURRENCY<span class="required_field">*</span></option>
                                    <?php foreach ($currency as $type) {              ?>
                                      <option value="<?php echo $type->currency_id ?>" <?php echo ($row->bus_currency_format == $type->currency_id)?'selected':'';?>><?php echo strtoupper($type->currency." (".$type->currencycode.")"); ?></option>
                                   <?php } ?>
                                  </select>
                                </div>
                              </div>
                              <div class="col s12 m12 l6 input-set datefor">
                <div class="input-field label-active" style="height:50px !important;">
                                <label for="date_format" class="full-bg-label select-label">Select Date Format<span class="required_field">*</span></label>
                                <select id="date_format" name="bus_dateformat" class="country-dropdown check-label">
                                    <option value=""></option>
                                    <option value="d/m/Y" <?php echo ("d/m/Y" == $row->bus_dateformat)?'selected':''; ?>>dd-mm-yyyy</option>
                            <option value="m-d-Y" <?php echo ("m-d-Y" == $row->bus_dateformat)?'selected':''; ?>>mm-dd-yyyy</option>
                            <option value="Y-m-d" <?php echo ("Y-m-d" == $row->bus_dateformat)?'selected':''; ?>>yyyy-mm-dd</option>
                          </select>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="row">
                            <div class="col s12 m12 l12">
                              <div class="col s12 m12 l6 border-field input-set" id="timezonesec">
                                  <div class="input-field label-active" style="height:73px !important; padding-top:5px !important;">
                                    <label for="timezone" class="full-bg-label select-label">Select TimeZone<span class="required_field">*</span></label>
                                    <select class="js-example-basic-single" name="bus_timezone" id="timezone">
                      <option value="">SELECT TIMEZONE<span class="required_field">*</span></option>
                                      <option value="GMT-12:00" <?php if($row->bus_timezone=="GMT-12:00"){ echo "selected";} ?>>GMT -12:00</option>
                                      <option value="GMT-11:00" <?php if($row->bus_timezone=="GMT-11:00"){ echo "selected";} ?>>GMT -11:00</option>
                                      <option value="GMT-10:00" <?php if($row->bus_timezone=="GMT-10:00"){ echo "selected";} ?>>GMT -10:00</option>
                                      <option value="GMT-09:00" <?php if($row->bus_timezone=="GMT-09:00"){ echo "selected";} ?>>GMT -09:00</option>
                                      <option value="GMT-08:00" <?php if($row->bus_timezone=="GMT-08:00"){ echo "selected";} ?>>GMT -08:00</option>
                                      <option value="GMT-07:00" <?php if($row->bus_timezone=="GMT-07:00"){ echo "selected";} ?>>GMT -07:00</option>
                                      <option value="GMT-06:00" <?php if($row->bus_timezone=="GMT-06:00"){ echo "selected";} ?>>GMT -06:00</option>
                                      <option value="GMT-06:00" <?php if($row->bus_timezone=="GMT-06:00"){ echo "selected";} ?>>GMT-06:00</option>
                                      <option value="GMT-06:00" <?php if($row->bus_timezone=="GMT-06:00"){ echo "selected";} ?>>GMT -06:00</option>
                                      <option value="GMT-06:00" <?php if($row->bus_timezone=="GMT-06:00"){ echo "selected";} ?>>GMT -06:00</option>
                                      <option value="GMT-05:00" <?php if($row->bus_timezone=="GMT-05:00"){ echo "selected";} ?>>GMT -05:00</option>
                                      <option value="GMT-05:00" <?php if($row->bus_timezone=="GMT-05:00"){ echo "selected";} ?>>GMT -05:00</option>
                                      <option value="GMT-05:00" <?php if($row->bus_timezone=="GMT-05:00"){ echo "selected";} ?>>GMT -05:00</option>
                                      <option value="GMT-04:00" <?php if($row->bus_timezone=="GMT-04:00"){ echo "selected";} ?>>GMT -04:00</option>
                                      <option value="GMT-04:00" <?php if($row->bus_timezone=="GMT-04:00"){ echo "selected";} ?>>GMT -04:00</option>
                                      <option value="GMT-04:00" <?php if($row->bus_timezone=="GMT-04:00"){ echo "selected";} ?>>GMT -04:00</option>
                                      <option value="GMT-04:00" <?php if($row->bus_timezone=="GMT-04:00"){ echo "selected";} ?>>GMT -04:00</option>
                                      <option value="GMT-03:30" <?php if($row->bus_timezone=="GMT-03:30"){ echo "selected";} ?>>GMT -03:30</option>
                                      <option value="GMT-03:00" <?php if($row->bus_timezone=="GMT-03:00"){ echo "selected";} ?>>GMT -03:00</option>
                                      <option value="GMT-03:00" <?php if($row->bus_timezone=="GMT-03:00"){ echo "selected";} ?>>GMT -03:00</option>
                                      <option value="GMT-03:00" <?php if($row->bus_timezone=="GMT-03:00"){ echo "selected";} ?>>GMT -03:00</option>
                                      <option value="GMT-03:00" <?php if($row->bus_timezone=="GMT-03:00"){ echo "selected";} ?>>GMT -03:00</option>
                                      <option value="GMT-02:00" <?php if($row->bus_timezone=="GMT-02:00"){ echo "selected";} ?>>GMT-02:00</option>
                                      <option value="GMT-01:00" <?php if($row->bus_timezone=="GMT-01:00"){ echo "selected";} ?>>GMT-01:00</option>
                                      <option value="GMT-01:00" <?php if($row->bus_timezone=="GMT-01:00"){ echo "selected";} ?>>GMT -01:00</option>
                                      <option value="GMT+00:00" <?php if($row->bus_timezone=="GMT+00:00"){ echo "selected";} ?>>GMT +00:00</option>
                                      <option value="GMT+00:00" <?php if($row->bus_timezone=="GMT+00:00"){ echo "selected";} ?>>GMT +00:00</option>
                                      <option value="GMT+01:00" <?php if($row->bus_timezone=="GMT+01:00"){ echo "selected";} ?>>GMT +01:00</option>
                                      <option value="GMT+01:00" <?php if($row->bus_timezone=="GMT+01:00"){ echo "selected";} ?>>GMT +01:00</option>
                                      <option value="GMT+01:00" <?php if($row->bus_timezone=="GMT+01:00"){ echo "selected";} ?>>GMT +01:00</option>
                                      <option value="GMT+01:00" <?php if($row->bus_timezone=="GMT+01:00"){ echo "selected";} ?>>GMT +01:00</option>
                                      <option value="GMT+01:00" <?php if($row->bus_timezone=="GMT+01:00"){ echo "selected";} ?>>GMT +01:00</option>
                                      <option value="GMT+02:00" <?php if($row->bus_timezone=="GMT+02:00"){ echo "selected";} ?>>GMT +02:00</option>
                                      <option value="GMT+02:00" <?php if($row->bus_timezone=="GMT+02:00"){ echo "selected";} ?>>GMT +02:00</option>
                                      <option value="GMT+02:00" <?php if($row->bus_timezone=="GMT+02:00"){ echo "selected";} ?>>GMT +02:00</option>
                                      <option value="GMT+02:00" <?php if($row->bus_timezone=="GMT+02:00"){ echo "selected";} ?>>GMT +02:00</option>
                                      <option value="GMT+02:00" <?php if($row->bus_timezone=="GMT+02:00"){ echo "selected";} ?>>GMT +02:00</option>
                                      <option value="GMT+02:00" <?php if($row->bus_timezone=="GMT+02:00"){ echo "selected";} ?>>GMT +02:00</option>
                                      <option value="GMT+02:00" <?php if($row->bus_timezone=="GMT+02:00"){ echo "selected";} ?>>GMT +02:00</option>
                                      <option value="GMT+02:00" <?php if($row->bus_timezone=="GMT+02:00"){ echo "selected";} ?>>GMT +02:00</option>
                                      <option value="GMT+02:00" <?php if($row->bus_timezone=="GMT+02:00"){ echo "selected";} ?>>GMT +02:00</option>
                                      <option value="GMT+03:00" <?php if($row->bus_timezone=="GMT+03:00"){ echo "selected";} ?>>GMT +03:00</option>
                                      <option value="GMT+03:00" <?php if($row->bus_timezone=="GMT+03:00"){ echo "selected";} ?>>GMT +03:00</option>
                                      <option value="GMT+03:00" <?php if($row->bus_timezone=="GMT+03:00"){ echo "selected";} ?>>GMT +03:00</option>
                                      <option value="GMT+03:00" <?php if($row->bus_timezone=="GMT+03:00"){ echo "selected";} ?>>GMT +03:00</option>
                                      <option value="GMT+03:30" <?php if($row->bus_timezone=="GMT+03:30"){ echo "selected";} ?>>GMT +03:30</option>
                                      <option value="GMT+04:00" <?php if($row->bus_timezone=="GMT+04:00"){ echo "selected";} ?>>GMT +04:00</option>
                                      <option value="GMT+04:00" <?php if($row->bus_timezone=="GMT+04:00"){ echo "selected";} ?>>GMT +04:00</option>
                                      <option value="GMT+04:00" <?php if($row->bus_timezone=="GMT+04:00"){ echo "selected";} ?>>GMT +04:00</option>
                                      <option value="GMT+04:30" <?php if($row->bus_timezone=="GMT+04:30"){ echo "selected";} ?>>GMT +04:30</option>
                                      <option value="GMT+05:00" <?php if($row->bus_timezone=="GMT+05:00"){ echo "selected";} ?>>GMT +05:00</option>
                                      <option value="GMT+05:00" <?php if($row->bus_timezone=="GMT+05:00"){ echo "selected";} ?>>GMT +05:00</option>
                                      <option value="GMT+05:30" <?php if($row->bus_timezone=="GMT+05:30"){ echo "selected";} ?>>GMT +05:30</option>
                                      <option value="GMT+05:30" <?php if($row->bus_timezone=="GMT+05:30"){ echo "selected";} ?>>GMT +05:30</option>
                                      <option value="GMT+05:45" <?php if($row->bus_timezone=="GMT+05:45"){ echo "selected";} ?>>GMT +05:45</option>
                                      <option value="GMT+06:00" <?php if($row->bus_timezone=="GMT+06:00"){ echo "selected";} ?>>GMT +06:00</option>
                                      <option value="GMT+06:00" <?php if($row->bus_timezone=="GMT+06:00"){ echo "selected";} ?>>GMT +06:00</option>
                                      <option value="GMT+06:30" <?php if($row->bus_timezone=="GMT+06:30"){ echo "selected";} ?>>GMT +06:30</option>
                                      <option value="GMT+07:00" <?php if($row->bus_timezone=="GMT+07:00"){ echo "selected";} ?>>GMT +07:00</option>
                                      <option value="GMT+07:00" <?php if($row->bus_timezone=="GMT+07:00"){ echo "selected";} ?>>GMT +07:00</option>
                                      <option value="GMT+08:00" <?php if($row->bus_timezone=="GMT+08:00"){ echo "selected";} ?>>GMT +08:00</option>
                                      <option value="GMT+08:00" <?php if($row->bus_timezone=="GMT+08:00"){ echo "selected";} ?>>GMT +08:00</option>
                                      <option value="GMT+08:00" <?php if($row->bus_timezone=="GMT+08:00"){ echo "selected";} ?>>GMT +08:00</option>
                                      <option value="GMT+08:00" <?php if($row->bus_timezone=="GMT+08:00"){ echo "selected";} ?>>GMT +08:00</option>
                                      <option value="GMT+08:00" <?php if($row->bus_timezone=="GMT+08:00"){ echo "selected";} ?>>GMT +08:00</option>
                                      <option value="GMT+09:00" <?php if($row->bus_timezone=="GMT+09:00"){ echo "selected";} ?>>GMT +09:00</option>
                                      <option value="GMT+09:00" <?php if($row->bus_timezone=="GMT+09:00"){ echo "selected";} ?>>GMT +09:00</option>
                                      <option value="GMT+09:00" <?php if($row->bus_timezone=="GMT+09:00"){ echo "selected";} ?>>GMT +09:00</option>
                                      <option value="GMT+09:30" <?php if($row->bus_timezone=="GMT+09:30"){ echo "selected";} ?>>GMT +09:30</option>
                                      <option value="GMT+09:30" <?php if($row->bus_timezone=="GMT+09:30"){ echo "selected";} ?>>GMT +09:30</option>
                                      <option value="GMT+10:00" <?php if($row->bus_timezone=="GMT+10:00"){ echo "selected";} ?>>GMT +10:00</option>
                                      <option value="GMT+10:00" <?php if($row->bus_timezone=="GMT+10:00"){ echo "selected";} ?>>GMT +10:00</option>
                                      <option value="GMT+10:00" <?php if($row->bus_timezone=="GMT+10:00"){ echo "selected";} ?>>GMT +10:00</option>
                                      <option value="GMT+10:00" <?php if($row->bus_timezone=="GMT+10:00"){ echo "selected";} ?>>GMT +10:00</option>
                                      <option value="GMT+10:00" <?php if($row->bus_timezone=="GMT+10:00"){ echo "selected";} ?>>GMT +10:00</option>
                                      <option value="GMT+11:00" <?php if($row->bus_timezone=="GMT+11:00"){ echo "selected";} ?>>GMT +11:00</option>
                                      <option value="GMT+12:00" <?php if($row->bus_timezone=="GMT+12:00"){ echo "selected";} ?>>GMT +12:00</option>
                                      <option value="GMT+12:00" <?php if($row->bus_timezone=="GMT+12:00"){ echo "selected";} ?>>GMT +12:00</option>
                                      <option value="GMT+13:00" <?php if($row->bus_timezone=="GMT+13:00"){ echo "selected";} ?>>GMT +13:00</option>

                                     <option value="">SELECT TIMEZONE<span class="required_field">*</span></option>
                                  <option value="GMT-12:00" <?php if($row->bus_timezone=="GMT+10:00"){ echo "selected";} ?>>GMT -12:00</option>
                                  <option value="GMT-11:00" <?php if($row->bus_timezone=="GMT+10:00"){ echo "selected";} ?>>GMT -11:00</option>
                                  <option value="GMT-10:00" <?php if($row->bus_timezone=="GMT+10:00"){ echo "selected";} ?>>GMT -10:00</option>
                                  <option value="GMT-09:00" <?php if($row->bus_timezone=="GMT+10:00"){ echo "selected";} ?>>GMT -09:00</option>
                                  <option value="GMT-08:00" <?php if($row->bus_timezone=="GMT+10:00"){ echo "selected";} ?>>GMT -08:00</option>
                                  <option value="GMT-07:00" <?php if($row->bus_timezone=="GMT+10:00"){ echo "selected";} ?>>GMT -07:00</option>
                                  <option value="GMT-06:00" <?php if($row->bus_timezone=="GMT+10:00"){ echo "selected";} ?>>GMT -06:00</option>
                                  <option value="GMT-05:00" <?php if($row->bus_timezone=="GMT+10:00"){ echo "selected";} ?>>GMT -05:00</option>
                                  <option value="GMT-04:00" <?php if($row->bus_timezone=="GMT+10:00"){ echo "selected";} ?>>GMT -04:00</option>
                                  <option value="GMT-03:30" <?php if($row->bus_timezone=="GMT+10:00"){ echo "selected";} ?>>GMT -03:30</option>
                                  <option value="GMT-03:00" <?php if($row->bus_timezone=="GMT+10:00"){ echo "selected";} ?>>GMT -03:00</option>
                                  <option value="GMT-02:00" <?php if($row->bus_timezone=="GMT+10:00"){ echo "selected";} ?>>GMT -02:00</option>
                                  <option value="GMT-01:00" <?php if($row->bus_timezone=="GMT+10:00"){ echo "selected";} ?>>GMT -01:00</option>
                                  <option value="GMT+00:00" <?php if($row->bus_timezone=="GMT+10:00"){ echo "selected";} ?>>GMT +00:00</option>
                                  <option value="GMT+01:00" <?php if($row->bus_timezone=="GMT+10:00"){ echo "selected";} ?>>GMT +01:00</option>
                                  <option value="GMT+02:00" <?php if($row->bus_timezone=="GMT+10:00"){ echo "selected";} ?>>GMT +02:00</option>
                                  <option value="GMT+03:00" <?php if($row->bus_timezone=="GMT+10:00"){ echo "selected";} ?>>GMT +03:00</option>
                                  <option value="GMT+03:30" <?php if($row->bus_timezone=="GMT+10:00"){ echo "selected";} ?>>GMT +03:30</option>
                                  <option value="GMT+04:00" <?php if($row->bus_timezone=="GMT+10:00"){ echo "selected";} ?>>GMT +04:00</option>
                                  <option value="GMT+04:30" <?php if($row->bus_timezone=="GMT+10:00"){ echo "selected";} ?>>GMT +04:30</option>
                                  <option value="GMT+05:00" <?php if($row->bus_timezone=="GMT+10:00"){ echo "selected";} ?>>GMT +05:00</option>
                                  <option value="GMT+05:30" <?php if($row->bus_timezone=="GMT+10:00"){ echo "selected";} ?>>GMT +05:30</option>
                                  <option value="GMT+05:45" <?php if($row->bus_timezone=="GMT+10:00"){ echo "selected";} ?>>GMT +05:45</option>
                                  <option value="GMT+06:00" <?php if($row->bus_timezone=="GMT+10:00"){ echo "selected";} ?>>GMT +06:00</option>
                                  <option value="GMT+06:30" <?php if($row->bus_timezone=="GMT+10:00"){ echo "selected";} ?>>GMT +06:30</option>
                                  <option value="GMT+07:00" <?php if($row->bus_timezone=="GMT+10:00"){ echo "selected";} ?>>GMT +07:00</option>
                                  <option value="GMT+08:00" <?php if($row->bus_timezone=="GMT+10:00"){ echo "selected";} ?>>GMT +08:00</option>
                                  <option value="GMT+09:00" <?php if($row->bus_timezone=="GMT+10:00"){ echo "selected";} ?>>GMT +09:00</option>
                                  <option value="GMT+10:00" <?php if($row->bus_timezone=="GMT+10:00"){ echo "selected";} ?>>GMT +10:00</option>
                                  <option value="GMT+11:00" <?php if($row->bus_timezone=="GMT+10:00"){ echo "selected";} ?>>GMT +11:00</option>
                                  <option value="GMT+12:00" <?php if($row->bus_timezone=="GMT+10:00"){ echo "selected";} ?>>GMT +12:00</option>
                                  <option value="GMT+13:00" <?php if($row->bus_timezone=="GMT+10:00"){ echo "selected";} ?>>GMT +13:00</option>
              </select>
                                    </div>
                              </div>
                              <div class="col s12 m12 l6 border-field input-set">
                                <div class="row decpoint" style="margin-top:0px !important; width:96%; margin-left:5px !important;">
                  <div class="input-field label-active" style="height:73px !important; padding-top:5px !important;">
                    <label for="decimal_points" style="margin-left:-7px !important;" class="full-bg-label select-label">Set Decimal Points<span class="required_field">*</span></label>
                    <select class="js-example-basic-single" name="bus_decimal_point" id="decimal_points">
                    <option value="<?php echo $row->bus_decimal_point; ?>"><?php echo $row->bus_decimal_point; ?></option>
                    <option value="0">0</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    </select>
                    </div>
                                    <!--<input id="decimal_points" name="bus_decimal_point" class="full-bg adjust-width" type="text" value="<?php //echo $row->bus_decimal_point; ?>">-->
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>


                    <h4 class="box-inner-title">Company Information</h4>
                      <div class="box-wrapper bg-white bg-img-green-after shadow border-radius-6">
                        <div class="box-body">

                          <div class="row">
                            <div class="col s12 m12 l12">
                              <div class="col s12 m12 l6 input-set label-active" id="companytypesec" hidden>
                                <div class="input-field">
                                 <label for="company_type" class="full-bg-label select-label"> <span class="required_field"> </span></label>
                                  <select class="js-example-basic-single" name="bus_company_type" id="company_type">
                                  <option value="">TYPE OF COMPANY *</option>
                                  <?php foreach ($company_type as $type) {
                                               ?>
                                      <option value="<?php echo $type->id ?>" <?php  echo($type->id == $row->bus_company_type)?'selected':''?> ><?php echo strtoupper($type->company_type); ?></option>
                                   <?php } ?>
                </select>
                                </div>
                              </div>
                              <div class="col s12 m12 l6 input-set">
                                <div class="row">
                                  <div class="input-field">
                                      <label for="incorporation" class="full-bg-label">Date of incorporation<span class="required_field">*</span></label>
                                      <input id="incorporation" name="bus_incorporation_date" class="full-bg icon-calendar-gray adjust-width border-top-none bdatepicker_inco bdatepicker" type="text" value="<?php echo ($row->bus_incorporation_date != '' && $row->bus_incorporation_date != '0000-00-00')?date("d/m/Y",  strtotime($row->bus_incorporation_date)):''; ?>">
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
              
              <div class="row" hidden>
                            <div class="col s12 m12 l12">
                              <div class="col s12 m12 l6 input-set">
                                <div class="row">
                                  <div class="input-field" style="border-top:1px solid #eef2fe;">
                                    <label for="opening_cash_balance" class="full-bg-label">Opening Cash Balance <span class="required_field">*</span></label>
                                    <input id="opening_cash_balance" name="opening_cash_balance" value="<?php echo round($row->opening_cash_balance); ?>" class="full-bg adjust-width border-bottom-none numeric_number" type="text" autocomplete="off" >
                                  </div>
                                </div>
                              </div>
                
                <div class="col s12 m12 l6 input-set">
                                <div class="row">
                                  <div class="input-field" style="border-top:1px solid #eef2fe;">
                                    <label style="padding-left:20px !important;" for="cash_balance_date" class="full-bg-label">Date of Opening Cash Bal<span class="required_field">*</span></label>
                  <input id="" name="cash_balance_date" class="full-bg adjust-width border-top-none bdatepicker icon-calendar-gray" value="<?php echo ($row->cash_balance_date != '' && $row->cash_balance_date != '0000-00-00')?date("d/m/Y",  strtotime($row->cash_balance_date)):''; ?>" type="text" autocomplete="off">
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
              
              <div class="row" hidden>
                            <div class="col s12 m12 l12">
                              <div class="col s12 m12 l6 input-set">
                                <div class="row">
                                  <div class="input-field" style="border-top:1px solid #eef2fe;">
                                    <label for="opening_petty_balance" class="full-bg-label">Opening Petty Cash Balance <span class="required_field">*</span></label>
                                    <input id="opening_petty_balance" name="opening_petty_balance" value="<?php echo round($row->opening_petty_balance); ?>" class="full-bg adjust-width border-bottom-none numeric_number" type="text" autocomplete="off" >
                                  </div>
                                </div>
                              </div>
                
                <div class="col s12 m12 l6 input-set">
                                <div class="row">
                                  <div class="input-field" style="border-top:1px solid #eef2fe;">
                                    <label style="padding-left:20px !important;" for="petty_balance_date" class="full-bg-label">Opening Date Petty Cash Bal <span class="required_field">*</span></label>
                  <input id="petty_balance_date" name="petty_balance_date" class="full-bg adjust-width border-top-none bdatepicker icon-calendar-gray" value="<?php echo ($row->petty_balance_date != '' && $row->petty_balance_date != '0000-00-00')?date("d/m/Y",  strtotime($row->petty_balance_date)):''; ?>" type="text" autocomplete="off">
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="row">
                            <div class="col s12 l12 m12">
                              <div class="col s12 m12 l6 input-set border-field border-bottom">
                                <div class="row">
                                  <div class="input-field">
                                    <a style="position: absolute;" class="sac info-ref tooltipped info-tooltipped" data-html="true" data-position="bottom" data-delay="50" data-tooltip="Fill this to get better insights in the Business Analytics module" data-tooltip-id="84856db1-7567-524a-fbcc-15c63776f866"></a>
                                      <label for="total_employee" class="full-bg-label">No. Of Employees</label>
                                       <input id="total_employee" name="bus_company_size" class="full-bg adjust-width border-bottom-none" min="0" step="1" type="number" value="<?php echo $row->bus_company_size; ?>">
                                  </div>
                                </div>
                              </div>

                              <div class="col s12 m12 l6 input-set border-field border-bottom" id="revenuesec">
                                <div class="input-field label-active" style="margin-bottom:73px !important;">
                                  <label for="revenue" class="full-bg-label select-label">Revenue</label>
                                  <select class="js-example-basic-single" name="bus_company_revenue" id="revenue">
                                  <option value="">REVENUE</option>
                                    <option value="5" <?php  echo ($row->bus_company_revenue == '5')?'selected':''; ?>>&lt; 5 Crores</option>
                                    <option value="5-20" <?php  echo ($row->bus_company_revenue == '5-20')?'selected':''; ?>>5-20 Crores</option>
                                    <option value="20-50" <?php  echo ($row->bus_company_revenue == '20-50')?'selected':''; ?>>20-50 Crores</option>
                                    <option value="50-100" <?php  echo ($row->bus_company_revenue == '50-100')?'selected':''; ?>>50-100 Crores</option>
                                    <option value="100+" <?php  echo ($row->bus_company_revenue == '100+')?'selected':''; ?>>100 Crores+</option>
                                </select>
                                </div>
                              </div>
                            </div>
                          </div>


                          <div class="row">
                            <div class="col s12 m12 l12">
                              <div class="col s12 m12 l12 input-set label-active" id="companytypesec">
                                <div class="input-field" style="height:73px !important; padding-top:5px !important;">
                                 <label for="company_type" class="full-bg-label select-label" style="font-size:10px !important;">Nature of Business<span class="required_field">*</span><span class="required_field"> </span></label>
                                  <select class="js-example-basic-single nature_buzz com-nature" name="nature_of_bus" id="nature_of_bus">
                                  <option value="<?php echo $row->nature_of_bus; ?>"><?php echo strtoupper($row->nature_of_bus); ?></option>
                                  
                                  <option value="Accounting & Taxation">ACCOUNTING & TAXATION</option>
                                  <option value="Advertising">ADVERTISING</option>
                                  <option value="Animation Studio">ANIMATION STUDIO</option>
                                  <option value="Architecture">ARCHITECTURE</option>
                                  <option value="Arts & Crafts">ARTS & CRAFTS</option>
                                  <option value="Audit & Tax">AUDIT & TAX</option>
                                  <option value="Brand Consulting">BRAND CONSULTING</option>
                                  <option value="Celebrity Management">CELEBRITY MANAGEMENT</option>
                                  <option value="Consultant">CONSULTANT</option>
                                  <option value="Content Studio">CONTENT STUDIO</option>
                                  <option value="Cyber Security">CYBER SECURITY</option>
                                  <option value="Data Analytics">DATA ANALYTICS</option>
                                  <option value="Digital Influencer">DIGITAL UNFLUENCER</option>
                                  <option value="Digital & Social Media">DIGITAL & SOCIAL MEDIA</option>
                                  <option value="Direct Marketing">DIRECT MARKETING</option>
                                  <option value="Entertainment">ENTERTAINMENT</option>
                                  <option value="Event Planning">EVENT PLANNING</option>
                                  <option value="Florist">FLORIST</option>
                                  <option value="Foreign Exchange">FOREIGN EXCHANGE</option>
                                  <option value="Financial and Banking">FINANCIAL & BANKING</option>
                                  <option value="Gaming Studio">GAMING STUDIO</option>
                                  <option value="DESIGN & UI/UX">DESIGN & UI / UX</option>
                                  <option value="Hardware Servicing">HARDWARE SERVICING</option>
                                  <option value="Industry Bodies">INDUSTRY BODIES</option>
                                  <option value="Insurance">INSURANCE</option>
                                  <option value="Interior Designing ">INTERIOR DESIGNING</option>
                                  <option value="Legal Firm">LEGAL FIRM</option>
                                  <option value="Media Planning & Buying">MEDIA PLANNING & BUYING</option>
                                  <option value="Mobile Services">MOBILE SERVICES</option>
                                  <option value="Music">MUSIC</option>
                                  <option value="Non-Profit">NON-PROFIT</option>
                                  <option value="Outdoor / Hoarding">OUTDOOR / HOARDING</option>
                                  <option value="Photography">PHOTOGRAPHY</option>
                                  <option value="Printing">PRINTING</option>
                                  <option value="Production Studio">PRODUCTION STUDIO</option>
                                  <option value="PR / Image Management">PR/ IMAGE MANAGEMENT</option>
                                  <option value="Publishing">PUBLISHING</option>
                                  <option value="Real Estate">REAL ESTATE</option>
                                  <option value="Recording Studio">RECORDING STUDIO</option>
                                  <option value="Research">RESEARCH</option>
                                  <option value="Sales Promotion">SALES PROMOTION</option>
                                  <option value="Staffing & Recruitment">STAFFING & RECRUITMENT</option>
                                  <option value="Stock & Shares">STOCK & SHARES</option>
                                  <option value="Technology (AI, AR, VR)">TECHNOLOGY(AI, AR, VR)</option>
                                  <option value="Tours & Travel">TOURS & TRAVEL</option>
                                  <option value="Training & Coaching">TRAINING & COACHING</option>
                                  <option value="Translation & Voice Over">TRANSLATION & VOICE OVER</option>
                                  <option value="Therapists">THERAPISTS</option>
                  <option value="Utilities">UTILITIES</option>
                                  <option value="Visual Effects / VFX">VISUAL EFFECTS / VFX</option>
                                  <option value="Web Development">WEB DEVELOPMENT</option>
                                </select>
                                </div>
                              </div>
                              <div class="col s12 m12 l6 input-set">
                                <!-- <div class="row">
                                  <div class="input-field">
                                      <label for="incorporation" class="full-bg-label">Date of incorporation</label>
                                      <input id="incorporation" name="bus_incorporation_date" class="full-bg icon-calendar-gray adjust-width border-top-none bdatepicker_inco" type="text" value="<?php //echo ($row->bus_incorporation_date != '' && $row->bus_incorporation_date != '0000-00-00')?date("d/m/Y",  strtotime($row->bus_incorporation_date)):''; ?>">
                                  </div>
                                </div> -->
                              </div>
                            </div>
                          </div>


                          <div class="row">
                            <div class="col s12 l12 m12">
                              <div class="input-field emty-field-row border-field">
                                <div class="col l12 s12 m12">
                                  <div class="col l12 s12 m12">
                                  <?php
                                      $tot_service = '';
                                      if($row->bus_services_keywords != '') {
                                        $tot_services = explode("|@|",$row->bus_services_keywords);
                                      $tot_service = count($tot_services)-1;
                                      } else {
                                      $tot_service = '0';
                                      }
                                      ?>
                                    <h6 class="label">Services offered  <small class="badge-cover tot_service"><?php echo $tot_service; ?></small>
                                      <a class="right add_new_service_2" type="button"  data-company_id="<?php echo $row->bus_id; ?>">
                                        <img src="<?php echo base_url();?>asset/css/img/icons/pluse.png" class="" alt="plus">
                                      </a>
                                    </h6>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="" id="services_array_list_edit">
 <?php
                            $services = $row->bus_services_keywords;
                           if($services != '') {
                            $services = explode("|@|",$services);

                            
                                $i = 1;
                            foreach($services as $key=>$service) { if($key!=0){?>
                            <div class="<?php if($i>1){ echo "popup-append-box"; } ?> popup-append-box-cover">
                              <div class="gst-boxs add-gst">
                                <div class="col l1 s1 m1 sno">
                                  <div class="row">
                                    <?php echo $i; ?>
                                  </div>
                                </div>
                                <div class="col l11 s1 m1">
                                  <!-- <div class="col l5 s5 m5 gstnum">
                                    <span class="gstlabel">KEYWORD</span><p>KEYWORD <?php echo $i;?></p>
                                  </div> -->
                                  <div class="col l11 s6 m6 gstplace">
                                    <span class="gstlabel">SERVICE</span><p><?php echo $service; ?></p>
                                  </div>
                                  <div class="col l1 s1 m1 gstaction"  style="padding:4% 0 0 1%;">
                                    <a class="editcmp_remove_service modal-trigger" data-service_id="<?php echo $service; ?>" data-company_id="<?php echo $row->bus_id; ?>"  data-target="remove_company_service_modal">
                                      <img src="<?php echo base_url(); ?>asset/images/delete.png" alt="delete" class="delete-icon">
                                    </a>
                                  </div>
                                </div>
                              </div>
                            </div>
                           <?php $i++; } } } ?>
                          </div>
                          <div class="row">
                                <div class="col l12 s12 m12 text-center">
                                  <a class="showmore-row cursor"><i data-id="services_array_list_edit" class="material-icons">keyboard_arrow_down</i></a>
                                </div>
                          </div>

                          <!--<div class="row">
                            <div class="col s12 l12 m12">
                              <div class="input-field emty-field-row border-field">
                                <div class="col l12 s12 m12">
                                  <div class="col l12 s12 m12">
                                    <h6 class="label">Add Branch Office <small class="badge-cover gray-box two-i tot_branch"><?php //echo count($branch_details); ?></small>
                                      <a class="right add_new_branch_cmp" type="button" data-user_id="<?php //echo $row->reg_id; ?>" data-company_id="<?php //echo $row->bus_id; ?>">
                                        <img src="<?php //echo base_url(); ?>/asset/css/img/icons/pluse.png" class="">
                                      </a>
                                    </h6>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        <div class="" id="branch_array_list_edit">
                          <?php
               //if($branch_details != '') {
                    //$i = 1;
                    //foreach($branch_details as $branch) {?>
                            <div class="<?php //if($i>1){ echo "popup-append-box"; } ?> popup-append-box-cover">
                              <div class="gst-boxs add-gst">
                                <div class="col l1 s1 m1 sno">
                                  <div class="row">
                                    <?php //echo $i; ?>
                                  </div>
                                </div>
                                <div class="col l11 s1 m1">
                                  <div class="col l5 s5 m5 gstnum">
                                    <span class="gstlabel">BRANCH</span><p><?php //echo $branch->cbranch_name;?></p>
                                  </div>
                                  <div class="col l6 s6 m6 gstplace">
                                    <span class="gstlabel">CITY</span><p><?php //echo $branch->cbranch_city; ?></p>
                                  </div>
                                  <div class="col l1 s1 m1 gstaction">
                                    <a class="editcmp_remove_branch" data-branch_id="<?php //echo $branch->cbranch_id; ?>" data-company_id="<?php //echo $row->bus_id; ?>">
                                      <img src="<?php //echo base_url(); ?>asset/images/delete.png" alt="" class="delete-icon">
                                    </a>
                                  </div>
                                </div>
                              </div>
                            </div>
                           <?php //$i++; } } ?>
                          </div>-->
                           <!--<div class="row">
                                <div class="col l12 s12 m12 text-center">
                                  <a class="showmore-row cursor"><i data-id="branch_array_list_edit" class="material-icons">keyboard_arrow_down</i></a>
                                </div>
                          </div>-->
                        </div>
                      </div>  
                 


                      <h4 class="box-inner-title">Web Presence
                        <!--<a class="info-ref tooltipped info-tooltipped" data-position="bottom" data-delay="50" data-tooltip="Will be update, Coming Soon..!"></a>-->
                      </h4>

                      <div class="box-wrapper bg-white no-bg shadow border-radius-6">
                        <div class="box-body">
                          <div class="row">
                            <div class="col s12 m12 l12">
                              <div class="col s12 m12 l6 input-set">
                                <div class="row">
                                  <div class="input-field icon-field url">
                                    <i class="field-prefix web"></i>
                                    <label for="website" class="full-bg-label">Add Website URL</label>
                                    <input id="website_url" name="bus_website_url" class="full-bg adjust-width border-top-none autofill_url" type="text" value="<?php echo $row->bus_website_url; ?>">
                                  </div>
                                </div>
                              </div>

                              <div class="col s12 m12 l6 input-set">
                                <div class="row">
                                  <div class="input-field icon-field url">
                                    <i class="field-prefix client"></i>
                                    <label for="website" class="full-bg-label">Add Client List URL</label>
                                    <input id="list_of_clients" name="bus_loc" class="full-bg adjust-width border-top-none autofill_url" type="text" value="<?php echo $row->bus_loc; ?>">
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col s12 m12 l12">
                              <div class="col s12 m12 l12 input-set">
                                <div class="row">
                                  <div class="input-field icon-field url">
                                    <i class="field-prefix study"></i>
                                    <label for="website" class="full-bg-label">Add Case Studies URL</label>
                                    <input id="case_study" name="bus_case_study" class="full-bg adjust-width border-top-none autofill_url" type="text" value="<?php echo $row->bus_case_study; ?>">
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>

                      <h4 class="box-inner-title">Social Media
                        <!--<a class="info-ref tooltipped info-tooltipped" data-position="bottom" data-delay="50" data-tooltip="Will be update, Coming Soon..!"></a>-->
                      </h4>

                      <div class="box-wrapper bg-white no-bg shadow border-radius-6">
                        <div class="box-body">
                          <div class="row">
                            <div class="col s12 m12 l12">
                              <div class="col s12 m12 l6 input-set">
                                <div class="row">
                                  <div class="input-field icon-field">
                                    <i class="field-prefix facebook"></i>
                                    <label for="website" class="full-bg-label">Add Facebook URL</label>
                                    <input id="facebook" name="bus_facebook" class="full-bg adjust-width border-top-none autofacebook_url" type="text" value="<?php echo $row->bus_facebook; ?>">
                                  </div>
                                </div>
                              </div>

                              <div class="col s12 m12 l6 input-set">
                                <div class="row">
                                  <div class="input-field icon-field">
                                    <i class="field-prefix twitter"></i>
                                    <label for="website" class="full-bg-label">Add Twitter URL</label>
                                    <input id="twitter" name="bus_twitter" class="full-bg adjust-width border-top-none autotwitter_url" type="text" value="<?php echo $row->bus_twitter; ?>">
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="row">
                            <div class="col s12 m12 l12">
                              <div class="col s12 m12 l6 input-set">
                                <div class="row">
                                  <div class="input-field icon-field">
                                    <i class="field-prefix linkedin"></i>
                                    <label for="website" class="full-bg-label">Add Linkedin URL</label>
                                    <input id="linkedin" name="bus_linkedin" class="full-bg adjust-width border-top-none autolinkedin_url" type="text" value="<?php echo $row->bus_linkedin; ?>">
                                  </div>
                                </div>
                              </div>

                              <div class="col s12 m12 l6 input-set">
                                <div class="row">
                                  <div class="input-field icon-field">
                                    <i class="field-prefix instagram"></i>
                                    <label for="website" class="full-bg-label">Add Instagram URL</label>
                                    <input id="instagram" name="bus_instagram" class="full-bg adjust-width border-top-none autoinstagram_url" type="text" value="<?php echo $row->bus_instagram; ?>">
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="row">
                            <div class="col s12 m12 l12">
                              <div class="col s12 m12 l6 input-set">
                                <div class="row">
                                  <div class="input-field icon-field">
                                    <i class="field-prefix youtube"></i>
                                    <label for="website" class="full-bg-label">Add Youtube URL</label>
                                    <input id="youtube" name="bus_youtube" class="full-bg adjust-width border-top-none autoyoutube_url" type="text" value="<?php echo $row->bus_youtube; ?>">
                                  </div>
                                </div>
                              </div>

                              <div class="col s12 m12 l6 input-set">
                                <div class="row">
                                  <div class="input-field icon-field">
                                    <i class="field-prefix whatsapp"></i>
                                    <label for="website" class="full-bg-label">Add Whatsapp Number</label>
                                    <input id="googleplus" name="bus_googleplus" class="full-bg adjust-width border-top-none autogoogleplus_url" type="number" value="<?php echo $row->bus_googleplus; ?>">
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                        </div>
                      </div>
            
            <h4 class="box-inner-title" hidden>Document Locker <span style="font-size:13px !important; color:#FA7C9B !important;">(Only PDF & PNG Format. Upto 25MB)</span>
                        <a class="info-ref tooltipped info-tooltipped" data-position="bottom" data-delay="50" data-tooltip="Upload company documents like MoA, Incorporation Certificate and set reminders for their renewal." hidden></a>
                      </h4>
                      <div class="box-wrapper bg-white no-bg shadow border-radius-6" hidden>
                        <div class="box-body">
                          <div class="row">
                            <div class="col s12 m12 l12">
                              <div class="row">
                                <div class="col s12 m12 l10">
                                  <div class="input-set legal-secs">
                                    <div class="input-field">
                                      <div class="col s12 m12 l12">
                                        <p class="revenue-p">Legal Document (MoA)</p>
                                      </div>
                                      <span class="legal-span">
                                        Upload your legal & tax documents
                                      </span>
                                    </div>
                                  </div>
                                </div>
                                <div class="col s12 m12 l2">
                                  <div class="clold">
                                     <input type="file" name="Legaldocuments[]" id="Legaldocuments_bus" multiple class="docup">
                                  </div>
                                </div>
                              </div>
                                <div id="LegalRemoveImage_edit">
                                            <?php
                                $l=100;

                                if($legal_documents != '' ) {

                                  foreach($legal_documents as $legal)
                                  {
                                  if($legal->legal_doc_type=='legal' ) {
                                  ?>

                                  <div class="docubox <?php echo $l; ?>_lclasss"  id="legaldoc<?php echo $legal->legal_id; ?>">
                                     <input type="hidden" id="legal_id" name="legal_id" value="<?php echo $legal->legal_id; ?>" />
                                    <div class="col s4 m4 l4 divr-border">
                                      <div class="pdf-file">
                                        <div class="col l3 s3 m3">
                                          <img src="<?php echo base_url(); ?>asset/images/pdf.png" alt="pdf" class="pdf">
                                        </div>
                                        <div class="col l9 s9 m9">
                                          <p class="uploadfilename edit_legel_doc"><?php echo $legal->legal_document;?></p>
                                          <span id="filesize">File Size <?php echo $legal->legal_document_size; ?> kb</span>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col s3 m3 l3 divr-border">
                                      <input type="text" placeholder="Doc No." class="no-border-field" name="legal_po_num<?php echo $l; ?>" id="legal_po_num<?php echo $l; ?>" value="<?php echo $legal->legal_po_num; ?>">
                                    </div>
                                        <div class="col s1 m1 l1 divr-border">
                                          <div class="date-start setdate" data-tooltip="Legal Documents Date">
                                            <input type="text" class="idatepicker hidendate" name="legal_po_date<?php echo $l; ?>" id="legal_po_date<?php echo $l; ?>" value="<?php echo ($legal->legal_po_date != '0000-00-00')?date("d/m/Y",  strtotime($legal->legal_po_date)):'';?>" title="<?php echo ( $legal->legal_po_date != '0000-00-00')?date("d/m/Y",  strtotime($legal->legal_po_date)):'';?>">
                                                  <img src="<?php echo base_url(); ?>asset/css/img/icons/calendar-purple.png" alt="calender">
                                          </div>
                                             <img src="<?php echo base_url(); ?>/asset/css/img/icons/tick.png" alt='tick' id='legal_po_date<?php echo $l; ?>_tick' class='po-tick-format'  style=<?php echo ($legal->legal_po_date != '0000-00-00')?'':"display:none" ?>>
                                        </div>
                                    <div class="col s1 m1 l1 divr-border">
                                      <div class="date-start setdate">
                                        <input type="text" class="idatepicker hidendate" name="start_date<?php echo $l; ?>" id="start_date<?php echo $l; ?>" value="<?php echo ($legal->legal_start_date != '0000-00-00')?date("d/m/Y",  strtotime($legal->legal_start_date)):'';?>"  title="<?php echo ($legal->legal_start_date != '0000-00-00')?date("d/m/Y",  strtotime($legal->legal_start_date)):'';?>">
                                        <img src="<?php echo base_url(); ?>asset/css/img/icons/calendar-green.png" alt="calender">
                                      </div>
                                      <img src="<?php echo base_url(); ?>/asset/css/img/icons/tick.png" alt='tick' id='start_date<?php echo $l; ?>_tick' class='tick-format' style=<?php echo ($legal->legal_start_date != '0000-00-00')?'':"display:none" ?>>
                                    </div>
                                    <div class="col s1 m1 l1 divr-border">
                                      <div class="date-start setdate end-date">
                                        <input type="text" class="idatepicker hidendate" name="end_date<?php echo $l; ?>" id="end_date<?php echo $l; ?>" value="<?php echo ($legal->legal_end_date != '0000-00-00')?date("d/m/Y",  strtotime($legal->legal_end_date)):'';?>" title="<?php echo ($legal->legal_end_date != '0000-00-00')?date("d/m/Y",  strtotime($legal->legal_end_date)):'';?>">
                                        <img src="<?php echo base_url(); ?>asset/css/img/icons/calendar-red.png" alt="calender">
                                      </div>
                                            <img src="<?php echo base_url(); ?>/asset/css/img/icons/tick.png" alt='tick' id='end_date<?php echo $l; ?>_tick' class='tick-format'style=<?php echo ($legal->legal_end_date != '0000-00-00')?'':"display:none" ?>>
                                    </div>
                                    <div class="col s1 m1 l1 alarom-col divr-border">
                                      <div class="date-start end-date">
                                        <img src="<?php echo base_url(); ?>asset/css/img/icons/alarmclock.png" alt="alarm">
                                        <select id="legal_remainder<?php echo $l; ?>" name="legal_remainder<?php echo $l; ?>" class="check-label">
                                          <option value=""></option>
                                          <option value="1" <?php echo ($legal->legal_reminder == '1')?'selected':''; ?> title="One week before">One week before</option>
                                          <option value="2" <?php echo ($legal->legal_reminder == '2')?'selected':''; ?> title="Fortnight">Fortnight</option>
                                          <option value="3" <?php echo ($legal->legal_reminder == '3')?'selected':''; ?> title="One month before">One month before</option>
                                          <option value="4" <?php echo ($legal->legal_reminder == '4')?'selected':''; ?> title="Two months before">Two months before</option>
                                        </select>
                                      </div>
                                       <img src="<?php echo base_url(); ?>/asset/css/img/icons/tick.png" alt='tick' id='legal_remainder<?php echo $l; ?>_tick' class='tick-format' style=<?php echo ($legal->legal_reminder != '' && $legal->legal_reminder != '0')?'':"display:none" ?>>
                                    </div>
                                    <div class="col s1 m1 l1 delete-col">
                                      <!-- <a href="javascript:void(0);" onclick="LegalRemoveImage_edit('<?php echo $l; ?>_lclasss','<?php echo $legal->legal_document;?>')" class="delete_legal_doc" data-cmp_legal_id="<?php echo $legal->legal_id; ?>"><img src="<?php echo base_url(); ?>asset/images/delete.png" alt=""></a> -->
                                      <a href="javascript:void(0);" class="delete_legal_doc" data-legal_id="<?php echo $legal->legal_id; ?>"><img src="<?php echo base_url(); ?>asset/images/delete.png" alt="delete"></a>
                                    </div>
                                  </div>
                  <?php $l++; } } } ?>
                 </div>
                              <div id="LegalRemoveImage_add"></div>
                <div id="edit_legel_doc" style="width:885px; margin-left:-230px; margin-right:-100px;">

                </div>
                              <div class="row">
                                <div class="col s12 m12 l2">
                                  <div class="col s12 m12 l2">
                                    <div class="line"></div>
                                  </div>
                                </div>
                              </div>
                <input type="hidden" id="legal_counter" name="legal_counter" value="<?php echo $l; ?>">
                            </div>
                          </div>
                          <div class="row">
                            <div class="col s12 m12 l12">
                              <div class="row">
                                <div class="col s12 m12 l10">
                                  <div class="input-set legal-secs">
                                    <div class="input-field">
                                      <div class="col s12 m12 l12">
                                        <p class="revenue-p">Company Documents (Certificate of Incorporation)</p>
                                      </div>
                                      <span class="legal-span">
                                        Upload your company documents
                                      </span>
                                    </div>
                                  </div>
                                </div>
                                <div class="col s12 m12 l2">
                                  <div class="clold">
                                    <input type="file" name="purchaseorderdoc[]" id="po_edit" class="docup" multiple>
                                  </div>
                                </div>
                                <div id="purchaseorder_edit" class="col s12 m12 l12">
                                <?php
                $p=100;
                if($legal_documents != '') {
                  foreach($legal_documents as $po) {
                    if($po->legal_doc_type=='purchase_order' ) {

                  ?>
                                <div class="docubox <?php echo $p; ?>_pclasss" >
                                   <input type="hidden" id="po_id" name="po_id" value="<?php echo $legal->legal_id; ?>" />
                                  <div class="col s4 m4 l4 divr-border">
                                    <div class="pdf-file">
                                        <div class="col l3 s3 m3">
                                            <img src="<?php echo base_url();?>asset/images/pdf.png" alt="pdf" class="pdf">
                                        </div>
                                        <div class="col l9 s9 m9">
                                             <p class="uploadfilename edit_po_doc"><?php echo $po->legal_document;?></p>
                                                <span id="filesize_po">File Size <?php echo $po->legal_document_size;?> kb</span>
                                        </div>
                                    </div>
                                    </div>

                                  <div class="col s3 m3 l3 divr-border">
                                      <input type="text" placeholder="Purchase Order" class="no-border-field" name="purchase_order<?php echo $p; ?>" id="purchase_order<?php echo $p; ?>" value="<?php echo $po->legal_po_num;?>">
                  </div>
                                        <div class="col s1 m1 l1 divr-border">
                                          <div class="date-start setdate" data-tooltip="Purchase Order Date">
                                            <input type="text" class="idatepicker hidendate" name="po_date<?php echo $p; ?>" id="po_date<?php echo $p; ?>" value="<?php echo ($po->legal_po_date != '0000-00-00')?date("d/m/Y",  strtotime($po->legal_po_date)):'';?>" title="<?php echo ($po->legal_po_date != '0000-00-00')?date("d/m/Y",  strtotime($po->legal_po_date)):'';?>">
                                                  <img src="<?php echo base_url(); ?>asset/css/img/icons/calendar-purple.png" alt="calender">
                                          </div>
                                             <img src="<?php echo base_url(); ?>/asset/css/img/icons/tick.png" alt='tick' id='po_date<?php echo $p; ?>_tick' class='po-tick-format'  style=<?php echo ($po->legal_po_date != '0000-00-00')?'':"display:none" ?>>
                                        </div>
                                    <div class="col s1 m1 l1 divr-border">
                                       <div class="date-start setdate" data-tooltip="Start Date">
                                        <input type="text" class="idatepicker hidendate" name="po_startdate<?php echo $p; ?>" id="po_startdate<?php echo $p; ?>" value="<?php echo ($po->legal_start_date != '0000-00-00')?date("d/m/Y",  strtotime($po->legal_start_date)):'';?>" title="<?php echo ($po->legal_start_date != '0000-00-00')?date("d/m/Y",  strtotime($po->legal_start_date)):'';?>">
                                        <img src="<?php echo  base_url(); ?>asset/css/img/icons/calendar-green.png" alt="calender">
                                        </div>
                                         <img src="<?php echo base_url(); ?>/asset/css/img/icons/tick.png" alt='tick' id='po_startdate<?php echo $p; ?>_tick' class='po-tick-format' style=<?php echo ($po->legal_start_date != '0000-00-00')?'':"display:none" ?>>
                                     </div>
                                     <div class="col s1 m1 l1 divr-border">
                                        <div class="date-start setdate end-date" data-tooltip="End Date">
                                        <input type="text" class="idatepicker hidendate" id="po_enddate<?php echo $p; ?>" name="po_enddate<?php echo $p; ?>" value="<?php echo ($po->legal_end_date != '0000-00-00')?date("d/m/Y",  strtotime($po->legal_end_date)):'';?>" title="<?php echo ($po->legal_end_date != '0000-00-00')?date("d/m/Y",  strtotime($po->legal_end_date)):'';?>">
                                            <img src="<?php echo base_url(); ?>asset/css/img/icons/calendar-red.png" alt="calender">
                                     </div>
                                     <img src="<?php echo base_url(); ?>/asset/css/img/icons/tick.png" alt='tick' id='po_enddate<?php echo $p; ?>_tick' class='po-tick-format' style=<?php echo ($po->legal_end_date != '0000-00-00')?'':"display:none" ?> >
                                  </div>
                                  <div class="col s1 m1 l1 alarom-col divr-border">
                                    <div class="date-start end-date">
                                    <img src="<?php echo base_url(); ?>asset/css/img/icons/alarmclock.png" alt="alarm">
                                     <select name="purchase_remainder<?php echo $p; ?>" id="purchase_remainder<?php echo $p; ?>" class="country-dropdown check-label" title="test">
                                      <option value=""></option>
                                      <option value="1"  <?php echo ($po->legal_reminder == '1')?'selected':''; ?>>One week before</option>
                                        <option value="2"  <?php echo ($po->legal_reminder == '2')?'selected':''; ?>>Fortnight</option>
                                        <option value="3"  <?php echo ($po->legal_reminder == '3')?'selected':''; ?>>One month before</option>
                                        <option value="4"  <?php echo ($po->legal_reminder == '4')?'selected':''; ?>>Two months before</option>
                                      </select>
                                      </div>
                                      <img src="<?php echo base_url(); ?>/asset/css/img/icons/tick.png" alt='tick' id='purchase_remainder<?php echo $p; ?>_tick' class='po-tick-format' style=<?php echo ($po->legal_reminder != '' && $po->legal_reminder != '0')?'':"display:none" ?>>
                                     </div>

                                 <div class="col s1 m1 l1 delete-col">
                                     <a href="javascript:void(0);" class="delete_po_doc" data-po_id="<?php echo $po->legal_id; ?>"><img src="<?php echo base_url(); ?>asset/images/delete.png" alt="delete"></a>
                                 </div>
              </div>

                  <?php $p++; }} } ?>
                </div>
                                    <div id="purchaseorder_add"></div>
                  <div id="edit_pur_order" style="width:970px; margin-left:-230px; margin-right:-100px;">

                  </div>
                  <input type="hidden" name="po_counter" id="po_counter" value="<?php echo $p; ?>">
                                <div class="col s12 m12 l12">
                                  <div class="line"></div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col s12 m12 l12">
                              <div class="row">
                                <div class="col s12 m12 l10">
                                  <div class="input-set legal-secs">
                                    <div class="input-field">
                                      <div class="col s12 m12 l12">
                                        <p class="revenue-p">Upload Multiple Other Documents (GST, Pancard)</p>
                                      </div>
                                      <span class="legal-span">
                                         Upload other documents(Rent agreement, Contracts, etc.)
                                      </span>
                                    </div>
                                  </div>
                                </div>
                                <div class="col s12 m12 l2" id="other_doc_file">
                                <div class="clold" id="other_doc0">
                                  <input type="file" name="otherdoc[]" id="otherdoc_edit0" class="docup otherdoc_edit" data-valId="0" multiple>
                                </div>
                              </div>
                              </div>
                 <div id="other_documents_edit">
                              <?php
                $o=0;
               if($other_documents != '')
               {
                 foreach($other_documents as $other) {
                   if($other->other_doc_type=='other' ) {
              ?>
                              <div class="docubox <?php echo $o; ?>_oclasss" id="otherdoc<?php echo $other->other_doc_id; ?>">
                                 <input type="hidden" id="other_id<?php $other->other_doc_id ?>" name="other_id[]" value="<?php echo $other->other_doc_id;?>" />
                                    <div class="col s6 m6 l6 divr-border">
                                        <div class="pdf-file"><div class="col l3 s3 m3">
                                            <img src="<?php echo base_url(); ?>asset/images/pdf.png" alt="pdf" class="pdf">
                                        </div>
                                       <div class="col l9 s9 m9">
                                        <p class="uploadfilename"><?php echo $other->other_doc_name; ?></p>
                                            <span>File Size <?php echo $other->other_doc_size; ?> kb</span>
                                        </div>
                                    </div>
                                  </div>
                                  <div class="col s4 m4 l4 divr-border">
                                    <input type="text" placeholder="Document Name" class="no-border-field" name="otherdoc_name[]" id="otherdoc_name" value="<?php echo $other->other_document; ?>">
                                  </div>
                                  <div class="col s2 m2 l2"></div>
                                  <div class="col s2 m2 l2 alarom-col"></div>
                                  <div class="col s2 m2 l2 delete-col">
                                      <a href="javascript:void(0);" class="delete_other_doc" data-other_id="<?php echo $other->other_doc_id; ?>"><img src="<?php echo base_url(); ?>asset/images/delete.png" alt="delete"></a>
                                  </div>
                            </div>
                            <?php $o++; } } } ?>
                              </div>
                              <div id="otherdoccover">

                              </div>
                           <input type="hidden" name="o_counter" id="o_counter" value="<?php echo $o; ?>">
                            </div>
                          </div>
                          <div class="row" hidden>
                            <div class="col s12 m12 l12">
                              <div class="row">
                                <div class="col s12 m12 l10">
                                  <div class="input-set legal-secs">
                                    <div class="input-field">
                                      <div class="col s12 m12 l12">
                                        <p class="revenue-p">Registration Document</p>
                                      </div>
                                      <span class="legal-span">
                                        Upload MOAs, MOUs, etc.
                                      </span>
                                    </div>
                                  </div>
                                </div>
                                <div class="col s12 m12 l2">
                                  <div class="clold">
                                    <input type="file" id="memorandum_edit" name="memorandum[]" class="docup" multiple="">
                                  </div>
                                </div>
                              </div>
                 <div id="memorandum1_edit">
                              <?php
                $m=0;
               if($other_documents != '')
               {
                 foreach($other_documents as $mdoc) {
                    if($mdoc->other_doc_type=='Memorandum' ) {
              ?>
                              <div class="docubox <?php echo $m; ?>_mclasss">
                                    <div class="col s6 m6 l6 divr-border">
                                        <div class="pdf-file"><div class="col l3 s3 m3">
                                            <img src="<?php echo base_url(); ?>asset/images/pdf.png" alt="pdf" class="pdf">
                                        </div>
                                        <div class="col l9 s9 m9">
                                        <p class="uploadfilename"><?php echo $mdoc->other_doc_name; ?></p>
                                            <span>File Size <?php echo $mdoc->other_doc_size; ?> kb</span>
                                        </div>
                                    </div>
                                  </div>
                                  <div class="col s4 m4 l4 divr-border">
                                    <input type="text" placeholder="Document Name" class="no-border-field" name="doc_name<?php echo $m; ?>" id="doc_name<?php echo $m; ?>" value="<?php echo $mdoc->other_document; ?>">
                                  </div>
                                  <div class="col s2 m2 l2"></div>
                                  <div class="col s2 m2 l2 alarom-col"></div>
                                  <div class="col s2 m2 l2 delete-col">
                                      <!--<a href="javascript:void(0);" onclick="MOAImage_remove('<?php echo $m; ?>_mclasss','<?php echo $mdoc->other_doc_name; ?>','memorandum1_edit')" class="delete_company_memo_doc" data-cmp_memo_id="<?php echo $mdoc->other_doc_id; ?>"><img src="<?php echo base_url(); ?>asset/images/delete.png" alt="">

                                      </a>-->

                                      <a href="javascript:void(0);" class="delete_memo_doc" data-other_id="<?php echo $mdoc->other_doc_id; ?>"><img src="<?php echo base_url(); ?>asset/images/delete.png" alt="delete"></a>
                                  </div>
                            </div>
               <?php $m++; } } } ?>
                               </div>
                              <div id="memorandumcover">

                              </div>
                <input type="hidden" name="m_counter" id="m_counter" value="<?php echo $m; ?>">
                            </div>
                          </div>
                        </div>
                      </div>
            
                    </div>

                    <div class="step1 footer-btns">
                      <div class="row">
                        <div class="col s12 m12 l12">
                          <div class="text-center"><i class="material-icons know-more-form" onclick="showStpe('.step2','.step1')">keyboard_arrow_down</i></div>
                        </div>
                      </div>

                      <div class="row">
                        <div class="col s12 m12 l12">
                          <div class="form-botom-divider"></div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col s12 m12 l6 right">
                          <button class="btn-flat theme-flat-btn theme-btn theme-btn-large right ml-5" type="button" onclick="location.href = '<?php echo base_url();?>profile/company-profile';">Cancel</button>
                          <button class="btn-flat theme-primary-btn theme-btn theme-btn-large right">Save</button>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col s12 m12 l3"></div>
                </div>
        <?php } ?>
              </div>
            </div>
          </section>
          <div class="step2 footer-btns last-sec">
            <div class="form-botom-divider" style="margin: 0 0 10px 0 !important;"></div>
            <div class="row" style="margin-bottom:7px !important;">
              <div class="text-center">
                <div class="col s12 m3 l4"></div>
                <div class="col s12 m12 l4">
                  <i class="material-icons know-more-form" onclick="showStpe('.step1','.step2')">keyboard_arrow_up</i>
                </div>
                <div class="col s12 m12 l4">
                  <button class="btn-flat theme-flat-btn theme-btn footer_btn right ml-5" type="button" onclick="location.href = '<?php echo base_url();?>profile/company-profile';">Cancel</button>
                  <button class="btn-flat theme-primary-btn theme-btn footer_btn right">Save</button>
                </div>
              </div>
            </div>
          </div>
        </form>
        <!-- END CONTENT -->
        </div>
        <!-- END WRAPPER -->
      </div>
      <!-- END MAIN -->

<div id="remove_company_legal_doc" class="modal modal-set">

  <img class="geen" src="<?php echo base_url();?>asset/images/green.png" alt="green">

  <div class="modal-content">

    <div class="modal-header">

      <h4>Remove Legal Document</h4>

      <input type="hidden" id="cmp_legal_doc_id" name="cmp_legal_doc_id" value="" />

      <a class="modal-close close-pop"><img src="<?php echo base_url();?>asset/images/popupdelete.png" alt="delete"></a>

    </div></div>

    <div class="modal-body">

      <p>Are you sure you want to remove this legal document?</p>

    </div>

    <div class="modal-content">

    <div class="modal-footer">

      <div class="row">

        <div class="col l12 s12 m12 cancel-deactiv">

          <a class=" modal-close btn-flat theme-flat-btn theme-btn theme-btn-large model-cancel " type="button">Cancel</a>

          <button class="btn-flat theme-primary-btn theme-btn theme-btn-large  modal-trigger dea modal-close remove_cmp_legal_doc">Remove</button>

        </div>

      </div>

    </div>

  </div>

</div>

<div id="remove_company_po_doc" class="modal modal-set">

  <img class="geen" src="<?php echo base_url();?>asset/images/green.png" alt="green">

  <div class="modal-content">

    <div class="modal-header">

      <h4>Remove Purchase Order Document</h4>

      <input type="hidden" id="cmp_po_doc_id" name="cmp_po_doc_id" value="" />

      <a class="modal-close close-pop"><img src="<?php echo base_url();?>asset/images/popupdelete.png" alt="delete"></a>

    </div></div>

    <div class="modal-body">

      <p>Are you sure you want to remove this purchase order document?</p>

    </div>

    <div class="modal-content">

    <div class="modal-footer">

      <div class="row">

        <div class="col l12 s12 m12 cancel-deactiv">

          <a class=" modal-close btn-flat theme-flat-btn theme-btn theme-btn-large model-cancel " type="button">Cancel</a>

          <button class="btn-flat theme-primary-btn theme-btn theme-btn-large  modal-trigger dea modal-close remove_cmp_po_doc">Remove</button>

        </div>

      </div>

    </div>

  </div>

</div>

<div id="remove_company_other_doc" class="modal moda-set">

  <img class="geen" src="<?php echo base_url();?>asset/images/green.png" alt="green">

  <div class="modal-content">

    <div class="modal-header">

      <h4>Remove This Document</h4>

      <input type="hidden" id="cmp_other_doc_id" name="cmp_other_doc_id" value="" />

      <a class="modal-close close-pop"><img src="<?php echo base_url();?>asset/images/popupdelete.png" alt="delete"></a>

    </div></div>

    <div class="modal-body">

      <p>Are you sure you want to remove this document?</p>

    </div>

    <div class="modal-content">

    <div class="modal-footer">

      <div class="row">

        <div class="col l12 s12 m12 cancel-deactiv">

          <a class=" modal-close btn-flat theme-flat-btn theme-btn theme-btn-large model-cancel " type="button">Cancel</a>

          <button class="btn-flat theme-primary-btn theme-btn theme-btn-large  modal-trigger dea modal-close remove_cmp_other_doc">Remove</button>

        </div>

      </div>

    </div>

  </div>

</div>

<div id="remove_company_memo_doc" class="modal modal-set">

  <img class="geen" src="<?php echo base_url();?>asset/images/green.png" alt="green">

  <div class="modal-content">

    <div class="modal-header">

      <h4>Remove This Document</h4>

      <input type="hidden" id="cmp_memo_doc_id" name="cmp_memo_doc_id" value="" />

      <a class="modal-close close-pop"><img src="<?php echo base_url();?>asset/images/popupdelete.png"></a>

    </div></div>

    <div class="modal-body">

      <p>Are you sure you want to remove this document?</p>

    </div>

    <div class="modal-content">

    <div class="modal-footer">

      <div class="row">

        <div class="col l12 s12 m12 cancel-deactiv">

          <a class=" modal-close btn-flat theme-flat-btn theme-btn theme-btn-large model-cancel " type="button">Cancel</a>

          <button class="btn-flat theme-primary-btn theme-btn theme-btn-large  modal-trigger dea modal-close remove_cmp_memo_doc">Remove</button>

        </div>

      </div>

    </div>

  </div>

</div>

<div id="remove_company_gstin_modal" class="modal modal-set">

  <img class="geen" src="<?php echo base_url();?>asset/images/green.png" alt="green">

  <div class="modal-content">

    <div class="modal-header">

      <h4>Deactivate GSTIN Information</h4>

      <input type="hidden" id="remove_cmp_gst_id" name="remove_cmp_gst_id" value="" />
      <input type="hidden" id="remove_cmp_gst_user_id" name="remove_cmp_gst_user_id" value="" />
      <input type="hidden" id="remove_cmp_gst_cmp_id" name="remove_cmp_gst_cmp_id" value="" />

      <a class="modal-close close-pop"><img src="<?php echo base_url();?>asset/images/popupdelete.png" alt="delete"></a>

    </div></div>

    <div class="modal-body confirma">

      <p>Are you sure you want to deactivate this GSTIN?</p>

    </div>

    <div class="modal-content">

    <div class="modal-footer">

      <div class="row">

        <div class="col l12 s12 m12 cancel-deactiv">

          <a class=" modal-close btn-flat theme-flat-btn theme-btn theme-btn-large model-cancel " type="button">CANCEL</a>

          <button class="btn-flat theme-primary-btn theme-btn theme-btn-large  modal-trigger dea modal-close remove_company_gstin_info">Deactivate</button>

        </div>

      </div>

    </div>

  </div>

</div>

<div id="remove_company_service_modal" class="modal modal-set">

  <img class="geen" src="<?php echo base_url();?>asset/images/green.png" alt="green">

  <div class="modal-content">

    <div class="modal-header">

      <h4>Remove This Service Info</h4>

      <input type="hidden" id="rem_cmp_service_id" name="rem_cmp_service_id" value="" />
      <input type="hidden" id="rem_service_cmp_id" name="rem_service_cmp_id" value="" />

      <a class="modal-close close-pop"><img src="<?php echo base_url();?>asset/images/popupdelete.png" alt="delete"></a>

    </div></div>

    <div class="modal-body confirma">

      <p>Are you sure you want to remove this Service Info?</p>

    </div>

    <div class="modal-content">

    <div class="modal-footer">

      <div class="row">

        <div class="col l12 s12 m12 cancel-deactiv">

          <a class=" modal-close btn-flat theme-flat-btn theme-btn theme-btn-large model-cancel " type="button">CANCEL</a>

          <button class="btn-flat theme-primary-btn theme-btn theme-btn-large  modal-trigger dea remove_company_service_info">Remove</button>

        </div>

      </div>

    </div>

  </div>

</div>

<div id="remove_company_branch_modal" class="modal modal-set">

  <img class="geen" src="<?php echo base_url();?>asset/images/green.png" alt="green">

  <div class="modal-content">

    <div class="modal-header">

      <h4>Remove This Branch Info</h4>

      <input type="hidden" id="remove_cmp_branch_id" name="remove_cmp_branch_id" value="" />
      <input type="hidden" id="remove_branch_cmp_id" name="remove_branch_cmp_id" value="" />

      <a class="modal-close close-pop"><img src="<?php echo base_url();?>asset/images/popupdelete.png" alt="delete"></a>

    </div></div>

    <div class="modal-body confirma">

      <p>Are you sure you want to remove this Branch Info?</p>

    </div>

    <div class="modal-content">

    <div class="modal-footer">

      <div class="row">

        <div class="col l12 s12 m12 cancel-deactiv">

          <a class=" modal-close btn-flat theme-flat-btn theme-btn theme-btn-large model-cancel " type="button">CANCEL</a>

          <button class="btn-flat theme-primary-btn theme-btn theme-btn-large  modal-trigger dea remove_company_branch_info">Remove</button>

        </div>

      </div>

    </div>

  </div>

</div>


<div id="add_gst_frm_2" class="modal add_gst_modal modal-md">
  <img class="geen" src="<?php echo base_url();?>asset/images/green.png" alt="green">
  <style>
  #add_gst_frm_2 .country-dropdown span.caret {
    margin: 12px 15px 0 0 !important;
  }
  
  #add_gst_frm_2 .select-wrapper {
    padding-top: 0px !important;
    margin-top: 6px !important;
  }
  
  #add_gst_frm_2{
    overflow-y:none !important;
  }

  </style>
  
  <div class="modalheader">
    <h4>ADD NEW GSTIN</h4>
    <a class="modal-close close-pop"><img src="<?php echo base_url();?>asset/images/popupdelete1.png" alt="delete"></a>
  </div>
  <div class="modalbody">
    <form class="addgstin" name="add_gst_from_2" id="add_gst_from_2" method="post">
	<?php $csrf = array(
					'name' => $this->security->get_csrf_token_name(),
					'hash' => $this->security->get_csrf_hash()
				);
				?>
				<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
      <div class="row gstrow">
        <div class="col l12 s12 m12 fieldset">
          <div class="row">
            <div class="col l6 s12 m12 fieldset">
              <div class="input-field">
                <a class="tooltipped info-tooltipped" data-position="bottom" data-delay="50" data-tooltip="It's a state-wise, PAN-based, 15-digit Goods and Services Taxpayer Identification Number (GSTIN)"></a>
                <label class="full-bg-label active">GSTIN<span class="required_field">* </span></label>
                <input type="hidden" name="gst_array[]" value="1" />
                <input type="hidden" name="company_id_gst_2" id="company_id_gst_2" value=""/>
                <input type="hidden" name="user_id_gst_2" id="user_id_gst_2" value=""/>
                <input name="gstin1" id="gstin1" class="full-bg adjust-width gstin statecode_gst" type="text" title="Enter GSTIN in a valid format">
              </div>
            </div>
            <div class="col l6 s12 m12 fieldset">
              <div class="input-field">
        <a class="tooltipped info-tooltipped" data-position="bottom" data-delay="50" data-tooltip="It is a destination where the services are consumed and hence determines whther IGST or CGST/SGST tax will be levied"></a>
                <label class="full-bg-label active">PLACE OF SUPPLY<span class="required_field">* </span></label>
                <input id="location1" name="location1" class="full-bg adjust-width js-typeahead" type="text" autocomplete="off">
                <div id="suggesstion-box_location1"></div>
              </div>
            </div>
      <!--div class="col l2 s2 m2 fieldset">
        <div class="col l12 s12 m12 fieldset">
          <a class="add-remove-btn add-icon addgstrow"><i class="material-icons add-bloue">add</i></a>
        </div>
      </div-->
          </div>
          <div class="row" style="display:none;">
            <div class="col s12 m12 l12">
              <div class="col s12 m12 l6 input-set border-field" id="citysec">
                <div class="input-field" style="margin-left:-22px !important; width:205px !important;">
                 <label for="email1" id="email_id" class="full-bg-label">Email<span class="required_field"></span></label>
                      <input id="email1" name="bus_email_id1" class="full-bg adjust-width" type="text">
                </div>
              </div>
              <div class="col s12 m12 l6 input-set border-field border-right-none">
                <div class="row">
                  <div class="input-field">
                      <label for="contact1" id="contact_no" class="full-bg-label">Contact No.<span class="required_field"></span></label>
                      <input style="margin-left:-10px !important;" id="contact1" name="bus_billing_contact1" class="full-bg adjust-width" type="text">
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col s12 m12 l12">
              <div class="input-field" style="width:102% !important; margin-left:-10px !important;">
                <label for="billing_address1" class="full-bg-label">BILLING ADDRESS<span class="required_field">* </span></label>
                <input id="billing_address1" name="bus_billing_address1" class="full-bg adjust-width border-top-none billing_add" type="text" value="">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col s12 m12 l12">
              <div class="col s12 m12 l6 input-set border-field">
                <div class="input-field" style="width:222px !important; margin-left:-12px !important; margin-top:0px !important; height:67px;">
          <label for="country1" class="full-bg-label select-label">Country<span class="required_field"> </span></label>
          <select class="gst-country js-example-basic-single country-dropdown check-label" name="bus_billing_country1" id="country1">
            <option value=""></option>
            <?php foreach ($countries as $country) { ?>
                        <option value="<?php echo $country->country_id; ?>"><?php echo $country->country_name; ?></option>
            <?php } ?>
                   </select>
        </div>
              </div>
              <div class="col s12 m12 l6 input-set border-right-none" id="statesec" style="margin-top:-10px !important;">
                <div class="row" style="margin-left:-12px !important;">
                <div class="input-field" style="width:222px !important; margin-left:16px !important; margin-top:10px !important;">
          <label for="state1" class="full-bg-label select-label state_label">State<span class="required_field"></span></label>
                    <select class="js-example-basic-single country-dropdown check-label gst_states" name="bus_billing_state1" id="state1">
                       <option value=""></option>
                    </select>
                </div>
              </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col s12 m12 l12">
              <div class="col s12 m12 l6 input-set border-field border-bottom" style="padding-top:0px !important;">
                <div class="input-field" style="width:222px !important; margin:-8px 0 0 -12px !important;">
          <label for="city1" class="full-bg-label select-label">City<span class="required_field"> </span></label>
          <select class="gst_city js-example-basic-single country-dropdown check-label" name="bus_billing_city1" id="city1">
            <option value=""></option>
                   </select>
        </div>
        
        <!--div class="row" style="margin-left:-12px !important; margin-top:-24px !important;  height:67px;">
                <div class="input-field" style="width:222px !important; margin-left:0px !important; margin-top:30px !important;">
          <label for="city" class="full-bg-label">City<span class="required_field"></span></label>
                    <select class="gst_city js-example-basic-single country-dropdown check-label" name="bus_billing_city1" id="city1">
                     <option value=""></option>
                    </select>
                </div>
        </div-->
            </div>
              <div class="col s12 m12 l6 input-set border-field border-right-none">
                  <div class="input-field" style="margin:0 0 10px -11px !important;">
          <label for="zipcode" class="full-bg-label">Pin Code <span class="required_field"></span></label>
          <input id="pincode1" name="bus_billing_zipcode1" class="full-bg adjust-width" type="text">
          </div>

                <!--div class="row" style="margin-left:-10px !important;">
                  <div class="input-field">
                      <label for="pincode1" id="pin_code" class="full-bg-label">PIN Code<span class="required_field"></span></label>
                      <input id="pincode1" name="bus_billing_zipcode1" class="full-bg adjust-width" type="text">
                  </div>
                </div-->
              </div>
            </div>
          </div>

        </div>
        <!-- <div class="col l2 s2 m2 fieldset">
          <div class="col l12 s12 m12 fieldset">
            <a class="add-remove-btn"><i class="material-icons  rem-red">remove</i></a>
          </div>
        </div> -->
      </div>
       <div id="rep-element">
      </div>
      <div class="row">
        <!--<div class="col l12 m12 s12 state-sugestion">
          * Please select state name from sugested list
        </div>-->
        <div class="col l6 m6 s12 fieldset">

        </div>
        <div class="col l6 m6 s12 fieldset buttonset">
          <div class="right">
            <button class="btn-flat theme-primary-btn theme-btn theme-btn-large right">Save</button>
            <button type="button" class="modal-close btn-flat theme-flat-btn theme-btn theme-btn-large">Cancel</button>
          </div>
        </div>
      </div>
    </form>
  </div>
</div>

<div id="edit_company_gstmodal" class="modal modal-md">
  <style>
  #edit_company_gstmodal .select-wrapper span.caret {
    margin-top:20px !important;
  }
  
  #edit_company_gstmodal .country-dropdown .dropdown-content{
    height:30vh !important;
    overflow-y: scroll !important;
  }
  
  #edit_company_gstmodal .country-dropdown .dropdown-content{
    height:30vh !important;
    overflow-y: scroll !important;
  }
  
  #edit_company_gstmodal .gst-status .dropdown-content{
    height:10vh !important;
    overflow-y: scroll !important;
  }
  
  #edit_company_gstmodal .gs-city .dropdown-content{
    height:20vh !important;
    overflow-y: scroll !important;
  }
  
  </style>
  <div class="modalheader">
    <h4>EDIT GSTIN</h4>
    <a class="modal-close close-pop"><img src="<?php echo base_url(); ?>asset/images/popupdelete1.png" alt="delete"></a>
  </div>

  <div class="modalbody">
    <form class="addgstin" id="edit_gst_from" name="edit_gst_from">
      <?php $csrf = array(
					'name' => $this->security->get_csrf_token_name(),
					'hash' => $this->security->get_csrf_hash()
				);
				?>
				<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
	  <div class="row gstrow">
        <div class="col l12 s12 m12 fieldset">
          <div class="row">
            <div class="col l6 s12 m6 fieldset">
              <div class="input-field">
                <a class="tooltipped info-tooltipped" data-position="bottom" data-delay="50" data-html="true" data-tooltip="It's a state-wise, PAN-based, 15-digit Goods and </br>Services Taxpayer Identification Number (GSTIN)"></a>
                <input name="cmp_gst_id" id="cmp_gst_id" class="full-bg adjust-width gstin" type="hidden">
                <input name="cmp_user_id" id="cmp_user_id" class="full-bg adjust-width gstin" type="hidden">
                <input name="cmp_id_edit_gst" id="cmp_id_edit_gst" class="full-bg adjust-width gstin" type="hidden">
                <label class="full-bg-label active">GSTIN<span class="required_field">* </span></label>
                <input name="gst_no_edit" style="text-transform:uppercase" id="gst_no_edit" class="full-bg adjust-width gstin" type="text" value="">
              </div>
            </div>
            <div class="col l6 s12 m6 fieldset">
              <div class="input-field">
                <a class="tooltipped info-tooltipped" data-position="bottom" data-delay="50" data-tooltip="Will be update, Coming Soon..!"></a>
                <label class="full-bg-label active">PLACE OF SUPPLY<span class="required_field">* </span></label>
                <input id="gst_edit_location" name="gst_edit_location" class="full-bg adjust-width" type="text" value="">
                <div id="suggesstion_edit_location"></div>
              </div>
            </div>
            <div class="row" style="display:none;">
            <div class="col s12 m12 l12">
              <div class="col s12 m12 l6 input-set border-field" id="citysec">
                <div class="input-field" style="margin-left:-22px !important; width:245px !important;">
                 <label for="email1" id="email_id" class="full-bg-label active">Email<span class="required_field"></span></label>
                      <input id="email2" name="bus_email_id1" class="full-bg adjust-width" type="text" value="0">
                </div>
              </div>
              <div class="col s12 m12 l6 input-set border-field border-right-none">
                <div class="row">
                  <div class="input-field" style="margin-left:-11px !important;">
                      <label for="contact2" id="contact_no" class="full-bg-label active">Contact No.<span class="required_field"></span></label>
                      <input id="contact2" name="bus_billing_contact1" class="full-bg adjust-width" type="text" value="0">
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col s12 m12 l12">
              <div class="input-field" style="width:490px !important; margin-left:-10px !important;">
                <label for="billing_address1" class="full-bg-label active">BILLING ADDRESS<span class="required_field">* </span></label>
                <input id="billing_address2" name="bus_billing_address1" class="full-bg adjust-width border-top-none billing_add" type="text" value="0">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col s12 m12 l12">
              <div class="col s12 m12 l6 input-set  border-field">
                 <div class="row" style="height:65px !important;">
                <div class="input-field" style="width:210px !important; margin-left:-12px !important; margin-top:0px !important;">
          <label for="country2" class="full-bg-label select-label">Country<span class="required_field"> </span></label>
                    <select class="js-example-basic-single country-dropdown check-label" name="bus_billing_country1" id="country2">
                       <option value=""></option>
                      <?php foreach ($countries as $country) { ?>
                          <option value="<?php echo $country->country_id; ?>"><?php echo $country->country_name; ?></option>
                       <?php } ?>
                    </select>
                </div>
              </div>
              </div>
              <div class="col s12 m12 l6 input-set border-right-none" id="statesec" style="margin-top:-10px !important;">
                 <div class="row" style="margin-left:-12px !important;">
                <div class="input-field" style="width:210px !important; margin-left:15px !important; margin-top:10px !important;">
          <label for="state2" class="full-bg-label select-label state_label">State<span class="required_field"></span></label>
                    <select class="check-label" name="bus_billing_state1" id="state2">
                       <option value=""></option>
                    </select>
                </div>
              </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col s12 m12 l12">
              <div class="col s12 m12 l6 input-set  border-field" style="padding-top:0px !important; height:74px;">
         <div class="input-field" style="width:210px !important; margin-left:-12px !important; margin-top:0px !important;">

          <label style="padding-top:10px;" for="city2" class="full-bg-label select-label">City<span class="required_field"> </span></label>
                    <select class="js-example-basic-single country-dropdown check-label gs-city" name="bus_billing_city1" id="city2">

                       <option value=""></option>
                    </select>
                 </div>
            </div>
              <div class="col s12 m12 l6 input-set border-field border-right-none">
                <div class="row" style="margin-left:-10px !important;">
                  <div class="input-field">
                      <label for="zipcode" id="pin_code" class="full-bg-label">PIN Code<span class="required_field"></span></label>
                      <input id="pincode2" name="bus_billing_zipcode1" class="full-bg adjust-width" type="text" value="0">
                  </div>
                </div>
              </div>
            </div>
          </div>


           <div class="row">
            <div class="col s12 m12 l12">
              <div class="col s12 m12 l6 input-set border-field border-bottom">
                <div class="row" style="height:65px !important;">
                <div class="input-field label-active" style="width:210px !important; margin-left:-12px !important; margin-top:0px !important;">
          <label for="status" class="full-bg-label select-label">status<span class="required_field"> </span></label>
                    <select class="country-dropdown check-label gst-status" name="bus_billing_status1" id="status">
                       <option value="Active">Active</option>
                       <option value="Inactive">Inactive</option>
                    </select>
                </div>
        </div>
              </div>
              <div class="col s12 m12 l6 input-set border-field border-right-none">
                <div class="row" style="margin-left:-10px !important;">
                  <div class="input-field">
                      
                  </div>
                </div>
              </div>
            </div>
          </div>


          </div>
        </div>

      </div>

      <div class="row">
        <!--<div class="col l12 m12 s12 state-sugestion">
          * Please select state name from sugested list
        </div>-->
        <div class="col l6 m6 s12 fieldset">

        </div>
        <div class="col l6 m6 s12 fieldset buttonset">
          <div class="right">
            <button class="btn-flat theme-primary-btn theme-btn theme-btn-large right" type="submit">Save</button>
            <button class="btn-flat theme-flat-btn theme-btn theme-btn-large modal-close" type="button">Cancel</button>
          </div>
        </div>
      </div>
    </form>
  </div>
</div>

<div id="edit-add-new-service" class="modal modal-md" style="max-height: 100%;">
  <div class="modalheader">
    <h4>Add Services Offered</h4>
    <a class="modal-close close-pop"><img src="<?php echo base_url();?>asset/images/popupdelete1.png" alt="delete"></a>
  </div>

  <div class="modalbody">
    <form class="addgstin" id="add_new_service_keywords_2" name="add_new_service_keywords_2" method="post">
		<?php $csrf = array(
					'name' => $this->security->get_csrf_token_name(),
					'hash' => $this->security->get_csrf_hash()
				);
				?>
				<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
      <div class="row gstrow">
        <div class="col l12 s10 m10 fieldset">
          <div class="row">
            <div class="col l6 s12 m6 fieldset">
              <div class="input-field">
              <input type="hidden" id="add_for_company_id" name="add_for_company_id" value=""/>
                <!--<a class="tooltipped info-tooltipped" data-position="bottom" data-delay="50" data-tooltip="Will be update, Coming Soon..!" data-tooltip-id="60d65848-8e58-2b5b-95af-d2f989eb68b8"></a>-->
                <label class="full-bg-label">Service 1</label>
                <input name="keyword_1" id="keyword_1" class="full-bg adjust-width gstin" type="text">
              </div>
            </div>
            <div class="col l6 s12 m6 fieldset">
              <div class="input-field">
                <!--<a class="tooltipped info-tooltipped" data-position="bottom" data-delay="50" data-tooltip="Will be update, Coming Soon..!" data-tooltip-id="91e5ff65-2b4f-fcce-c75e-92e50815775c"></a>-->
                <label class="full-bg-label">Service 2</label>
                <input id="keyword_2" name="keyword_2" class="full-bg adjust-width" type="text">
              </div>
            </div>
          </div>
        </div>
        <div class="col l12 s10 m10 fieldset">
          <div class="row">
            <div class="col l6 s12 m6 fieldset">
              <div class="input-field">
                <!--<a class="tooltipped info-tooltipped" data-position="bottom" data-delay="50" data-tooltip="Will be update, Coming Soon..!" data-tooltip-id="60d65848-8e58-2b5b-95af-d2f989eb68b8"></a>-->
                <label class="full-bg-label">Service 3</label>
                <input name="keyword_3" id="keyword_3" class="full-bg adjust-width gstin" type="text">
              </div>
            </div>
            <div class="col l6 s12 m6 fieldset">
              <div class="input-field">
                <!--<a class="tooltipped info-tooltipped" data-position="bottom" data-delay="50" data-tooltip="Will be update, Coming Soon..!" data-tooltip-id="91e5ff65-2b4f-fcce-c75e-92e50815775c"></a>-->
                <label class="full-bg-label">Service 4</label>
                <input id="keyword_4" name="keyword_4" class="full-bg adjust-width" type="text">
              </div>
            </div>
          </div>
        </div>
        <div class="col l12 s10 m10 fieldset">
          <div class="row">
            <div class="col l6 s12 m6 fieldset">
              <div class="input-field">
                <!--<a class="tooltipped info-tooltipped" data-position="bottom" data-delay="50" data-tooltip="Will be update, Coming Soon..!" data-tooltip-id="60d65848-8e58-2b5b-95af-d2f989eb68b8"></a>-->
                <label class="full-bg-label">Service 5</label>
                <input name="keyword_5" id="keyword_5" class="full-bg adjust-width gstin" type="text">
              </div>
            </div>
            <div class="col l6 s12 m6 fieldset">
              <div class="input-field">
                <!--<a class="tooltipped info-tooltipped" data-position="bottom" data-delay="50" data-tooltip="Will be update, Coming Soon..!" data-tooltip-id="91e5ff65-2b4f-fcce-c75e-92e50815775c"></a>-->
                <label class="full-bg-label">Service 6</label>
                <input id="keyword_6" name="keyword_6" class="full-bg adjust-width" type="text">
              </div>
            </div>
          </div>
        </div>
        <div class="col l12 s10 m10 fieldset">
          <div class="row">
            <div class="col l6 s12 m6 fieldset">
              <div class="input-field">
                <!--<a class="tooltipped info-tooltipped" data-position="bottom" data-delay="50" data-tooltip="Will be update, Coming Soon..!" data-tooltip-id="60d65848-8e58-2b5b-95af-d2f989eb68b8"></a>-->
                <label class="full-bg-label">Service 7</label>
                <input name="keyword_7" id="keyword_7" class="full-bg adjust-width gstin" type="text">

              </div>
            </div>
            <div class="col l6 s12 m6 fieldset">
              <div class="input-field">
                <!--<a class="tooltipped info-tooltipped" data-position="bottom" data-delay="50" data-tooltip="Will be update, Coming Soon..!" data-tooltip-id="91e5ff65-2b4f-fcce-c75e-92e50815775c"></a>-->
                <label class="full-bg-label">Service 8</label>
                <input id="keyword_8" name="keyword_8" class="full-bg adjust-width" type="text">
              </div>
            </div>
          </div>
        </div>
        <div class="col l12 s10 m10 fieldset">
          <div class="row">
            <div class="col l6 s12 m6 fieldset">
              <div class="input-field">
                <!--<a class="tooltipped info-tooltipped" data-position="bottom" data-delay="50" data-tooltip="Will be update, Coming Soon..!" data-tooltip-id="60d65848-8e58-2b5b-95af-d2f989eb68b8"></a>-->
                <label class="full-bg-label">Service 9</label>
                <input name="keyword_9" id="keyword_9" class="full-bg adjust-width gstin" type="text">
              </div>
            </div>
            <div class="col l6 s12 m6 fieldset">
              <div class="input-field">
                <!--<a class="tooltipped info-tooltipped" data-position="bottom" data-delay="50" data-tooltip="Will be update, Coming Soon..!" data-tooltip-id="91e5ff65-2b4f-fcce-c75e-92e50815775c"></a>-->
                <label class="full-bg-label">Service 10</label>
                <input id="keyword_10" name="keyword_10" class="full-bg adjust-width" type="text">
              </div>
            </div>
          </div>
        </div>

      </div>
      <div id="rep-element">
      </div>
      <div class="row">
        <div class="col l6 m6 s12 fieldset">
        </div>
        <div class="col l6 m6 s12 fieldset buttonset">
          <div class="right">
            <button type="submit" class="btn-flat theme-primary-btn theme-btn theme-btn-large right">Save</button>
            <button type="button" class="modal-close btn-flat theme-flat-btn theme-btn theme-btn-large modal-close">Cancel</button>
          </div>
        </div>
      </div>
    </form>
  </div>
</div>

<div id="edit-new-branch" class="modal modal-md">
  <div class="modalheader">
    <h4>Add new Branch Details</h4>
    <a class="modal-close close-pop"><img src="<?php echo base_url();?>asset/images/popupdelete1.png" alt="delete"></a>
  </div>

  <div class="modalbody">
    <form class="addgstin" id="add_branch_info_2" name="add_branch_info_2">
      <div class="row gstrow">
        <div class="col l12 s10 m10 fieldset">
          <div class="row">
            <div class="col l6 s12 m6 fieldset">
              <div class="input-field">
              <input type="hidden" id="add_branch_for_cmp" name="add_branch_for_cmp" value="" />
              <input type="hidden" id="add_branch_for_user" name="add_branch_for_user" value="" />
                <a class="tooltipped info-tooltipped" data-position="bottom" data-delay="50" data-tooltip="For eg. North, Central, Mid West, East etc." data-tooltip-id="60d65848-8e58-2b5b-95af-d2f989eb68b8"></a>
                <label class="full-bg-label">Enter Region</label>
                <input name="branch_name" id="branch_name" class="full-bg adjust-width gstin" type="text">
              </div>
            </div>
            <div class="col l6 s12 m6 fieldset">
              <div class="input-field">
                <a class="tooltipped info-tooltipped" data-position="bottom" data-delay="50" data-tooltip="Regional office city name." data-tooltip-id="91e5ff65-2b4f-fcce-c75e-92e50815775c"></a>
                <label class="full-bg-label">CITY</label>
                <input id="branch_city" name="branch_city" class="full-bg adjust-width" type="text">
              </div>
            </div>
          </div>
        </div>
       </div>
      <div id="rep-element">
      </div>
      <div class="row">
        <div class="col l6 m6 s12 fieldset">
        </div>
        <div class="col l6 m6 s12 fieldset buttonset">
          <div class="right">
            <button type="submit" class="btn-flat theme-primary-btn theme-btn theme-btn-large right">Save</button>
            <button type="button" class="modal-close btn-flat theme-flat-btn theme-btn theme-btn-large modal-close">Cancel</button>
          </div>
        </div>
      </div>
    </form>
  </div>
</div>
<div id="removed_existing_bank" class="modal modal-set">

  <img class="geen" src="<?php echo base_url();?>asset/images/green.png" alt="green">

  <div class="modal-content">

    <div class="modal-header">

      <h4> Deactivate Bank Information </h4>

      <input type="hidden" id="remove_cbank_id" name="remove_cbank_id" />
      <input type="hidden" id="remove_bus_id" name="remove_bus_id"  />

      <a class="modal-close close-pop"><img src="<?php echo base_url();?>asset/images/popupdelete.png" alt="delete"></a>

    </div></div>

    <div class="modal-body confirma">

      <p>Are you sure you want to deactivate this bank information?</p>

    </div>

    <div class="modal-content">

    <div class="modal-footer">

      <div class="row">

        <div class="col l12 s12 m12 cancel-deactiv">

          <a class=" modal-close btn-flat theme-flat-btn theme-btn theme-btn-large model-cancel " type="button">CANCEL</a>

          <button class="btn-flat theme-primary-btn theme-btn theme-btn-large  modal-trigger dea modal-close removed_existing_bank_data">DEACTIVATE</button>

        </div>

      </div>

    </div>

  </div>

</div>

<div id="removed_existing_card" class="modal modal-set">

  <img class="geen" src="<?php echo base_url();?>asset/images/green.png" alt="green">

  <div class="modal-content">

    <div class="modal-header">

      <h4> Deactivate Card Information </h4>

      <input type="hidden" id="remove_ccard_id" name="remove_ccard_id" />

      <input type="hidden" id="remove_bus_id" name="remove_bus_id"  />

      <a class="modal-close close-pop"><img src="<?php echo base_url();?>asset/images/popupdelete.png" alt="delete"></a>

    </div></div>

    <div class="modal-body confirma">

      <p>Are you sure you want to deactivate this card information?</p>

    </div>

    <div class="modal-content">

    <div class="modal-footer">

      <div class="row">

        <div class="col l12 s12 m12 cancel-deactiv">

          <a class=" modal-close btn-flat theme-flat-btn theme-btn theme-btn-large model-cancel " type="button">CANCEL</a>

          <button class="btn-flat theme-primary-btn theme-btn theme-btn-large  modal-trigger dea modal-close removed_existing_card_data">DEACTIVATE</button>

        </div>

      </div>

    </div>

  </div>

</div>


<script type="text/javascript">
  $(document).ready(function() {  
  $("#company_logo").change(function(event){
    $('.up_pic').hide();
  });
  });
</script>

 <script type="text/javascript">
  $(document).ready(function() {
  
  $("#company_logo").change(function(event){
    alert('POPO');
    $('.up_pic').hide();
  });

      var billing_address1=$("#billing_address").val();
  $("#billing_address1").val(billing_address1);
  $("#billing_address1").focus().click();
    $("#billing_address1").parents('.input-field').addClass('label-active');
 
 var pincode1=$("#pincode").val();
 $("#pincode1").val(pincode1);
   $("#pincode").focus().click();
          
  
    $("#pincode1").parents('.input-field').addClass('label-active');

     var country_id=$('select#country').val();



   var state1=$('select#state').val();

  

    var city1=$("select#city").val();

 


    setTimeout(function(){ 
           
  
 
          $("#country1").val(country_id).prop("selected", "selected").change();
          $('#country1').material_select();
        setTimeout(function(){ 
          $("select#state1").val(state1).prop("selected", "selected").change();
          $('#state_local').material_select();
        setTimeout(function(){ 
          $('select#city1 option[value='+city1+']').attr("selected", 'selected').change();
          //("select#city_local").val(city).prop("selected", "selected").change();
          $('#city_local').material_select();


        }, 2000); 
        }, 1500);
        }, 1000);


   if($("#tot_gst_nu").val()<=0){
$("#billing_address").keyup(function(){
  var billing_address1=$("#billing_address").val();
  $("#billing_address1").val(billing_address1);
  $("#billing_address1").focus().click();
    $("#billing_address1").parents('.input-field').addClass('label-active');
})

$("#pincode").keyup(function(){
  var pincode1=$("#pincode").val();
  $("#pincode1").val(pincode1);
  $("#pincode1").focus().click();
    $("#pincode1").parents('.input-field').addClass('label-active');
})

$("#country").change(function(){
  var country_id=$(this).val();

  //$("select#country1 option:selected").val(country_id);
  $('#country1 option[value='+country_id+']').attr("selected", 'selected').change();
})

$("#state").change(function(){
  var state1=$(this).val();

  $("select#state1").val(state1).attr("selected", 'selected').change();

})
$("#city").change(function(){
  var city1=$(this).val();

  $("select#city1").val(city1).attr("selected", 'selected').change();

})

  }


     var minDate ;
    var maxDate;
    var mDate;

    $( "#fy_start_date" ).datepicker({
    onSelect: function() {
    minDate = $( "#fy_start_date" ).datepicker("getDate");
    var mDate = new Date(minDate.setDate(minDate.getDate()));
    maxDate = new Date(minDate.setDate(minDate.getDate() + 1 year));
$("#fy_end_date").datepicker("setDate", maxDate);
$("#fy_end_date" ).datepicker( "option", "minDate", mDate);
$("#fy_end_date" ).datepicker( "option", "maxDate", maxDate);
}
});
var tdate = new Date();
var ddate = new Date(tdate.setDate(tdate.getDate() + 1 year));
$("#fy_start_date").datepicker("setDate", new Date());
$("#fy_end_date" ).datepicker();
$("#fy_end_date").datepicker("setDate",ddate);
   $('.js-example-basic-single').select2();
  $("select").change(function () {
  if($(this).val()!=''){
    $(this).valid();
  }
  });

});
</script>

<script type="text/javascript">
  /*$(document).ready(function() {
   $("#country").change(function(){
      var country_id=$(this).val();
      // $("select#country1 option:selected").val(country_id);
      $('#country1 option[value='+country_id+']').prop("selected", "selected").change();
      //$('#bank_country option[value='+country_id+']').prop("selected", "selected").change();
      $('#addbranch #bank_country option[value='+country_id+']').attr('selected','selected').change();
      $('#bank_country').material_select();
  
    if(country_id==101){
      var curyr = new Date(new Date().getFullYear(), 3, 1);
      $("#fy_start_date").datepicker("setDate", new Date(curyr));
      //$("#fy_end_date")
    }
  }); 
  });*/
</script>
<?php $this->load->view('template/footer.php');?>
