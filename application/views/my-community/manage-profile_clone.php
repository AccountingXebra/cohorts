<?php //$this->load->view('my-community/Cohorts-header');?>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="msapplication-tap-highlight" content="no">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<title>Xebra.in</title>
	<meta name="msapplication-TileColor" content="#00bcd4">
	<link rel="stylesheet" href="<?php echo base_url(); ?>public/css/select2.min.css" type="text/css"/>
	<link href="<?php echo base_url();?>asset/materialize/css/materialize.css" type="text/css" rel="stylesheet">
	<link href="<?php echo base_url();?>asset/css/style.css" type="text/css" rel="stylesheet">
	<link href="/your-path-to-fontawesome/css/solid.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url('asset/datetimepick/css/bootstrap-datetimepicker.css'); ?>" />
    <link href="<?php echo base_url();?>asset/css/custom.css" type="text/css" rel="stylesheet">
	<link href="<?php echo base_url();?>asset/css/userprofiletable.css" type="text/css" rel="stylesheet">
	<link href="<?php echo base_url();?>asset/css/jquery.dataTables.min.css" type="text/css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.5.3/css/bootstrapValidator.css" type="text/css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Fira+Sans:100,400,500,600,700" rel="stylesheet">
	<link href="<?php echo base_url();?>asset/css/perfect-scrollbar.css" type="text/css" rel="stylesheet">
    <link href="<?php echo base_url();?>asset/css/customei.css" type="text/css" rel="stylesheet">
	<script>var base_url = '<?php echo base_url(); ?>';</script>
	<script type="text/javascript" src="<?php echo base_url();?>asset/js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript"  src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.1/js/all.min.js"></script>
<style type="text/css">
	.deals-row{
		padding: 0 3% !important;
	}
  .green-bg-right:before {
      top: -39px !important;
      width: 270px;
   }
   .green-bg-right:after {
      width: 270px;
   }

   .act_details{
	   font-size:11px !important;
	   color:white !important;
   }

   .btn-welcome {
    width: 120px;
    height: 44px;
    font-size: 14px;
    padding: 5px 0 !important;
    text-align: center;
    font-weight: 500;
    border-radius: 8px;
    position: relative;
    letter-spacing: 0.40px;
}

   [type="radio"]:not(:checked) + label, [type="radio"]:checked + label {
    position: relative;
    padding-left: 20px;
    cursor: pointer;
    display: inline-block;
    height: 25px;
    line-height: 25px;
    font-size: 1rem;
    -webkit-transition: .28s ease;
    transition: .28s ease;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
}

[type="radio"] + label:before, [type="radio"] + label:after {
    content: '';
    position: absolute;
    left: 0;
    top: 0;
    margin: 4px 0 0 0;
    width: 16px;
    height: 16px;
    z-index: 0;
    -webkit-transition: .28s ease;
    transition: .28s ease;
}

@media only screen and (min-width: 993px) {
.row .col.l6 {
    width: 50%;
    margin-left: auto;
    left: auto;
    right: auto;
    margin-bottom: 20px;
}
}
body.easy-tab1 [type="radio"] + label:before, [type="radio"] + label:after {
    content: '';
    position: absolute;
    left: 0;
    top: 0;
    margin: 3px 0 0 -2px;
    width: 20px;
    height: 20px;
    z-index: 0;
    -webkit-transition: .28s ease;
    transition: .28s ease;
}

   .act_data{
	   font-size:13px !important;
	   color:white !important;
   }

   #my_activity th{
	   color:#808080 !important;
	   font-size:13px;
   }

   #my_activity td{
	   font-size:12px;
	   padding: 15px 12px !important;
   }

   #my_activity thead{
	   border-bottom:none !important;
   }

   #my_activity{
	    border-collapse:separate;
		border-spacing:0px 5px;
   }

   .company_name{
	   font-size:15px !important;
	   font-weight:500 !important;
	   /*color:#000000 !important;*/
   }

   .logo_co{
	   margin-bottom:-10px !important;
   }

   .nature_buzz{
	   font-size:13px !important;
   }

   #send-mail-comp{
	   background: white !important;
	   color:#000000;
	   width:120px !important;
	   border:1px solid #7864e9 !important;
	   font-size:14px !important;
   }

   #connect-to-comp{
	   width:150px !important;
	   font-size:14px !important;
   }

   #connect-to-comp:active {
		background: #7864e9 !important;
   }

   .cmp_info{
	   font-size:13px !important;
   }

   .cmp_details{
	   font-size:13px !important;
	   color:#000000 !important;
   }

	.loc-add{
		margin-left:16px !important;
		width:40px !important;
	}

	.view-label{
		font-size:13px !important;
	}

	.view-content{
		font-size:13px !important;
		color:#000000 !important;
	}

	.select_loc{
		padding-left:10px;
		padding-right:10px;
		padding-top:12px;
		padding-bottom:12px;
		text-align:center;
		width:15px !important;
		height:15px !important;
		color:#1a1a1a !important;
		background-color:#f0f5f5 !important;
		border-radius:18px;
		font-size:15px;
	}

	#cke_1_contents{
		height:120px !important;
	}

	label.fill_details{
		font-size:0.875rem !importantl
	}

	.filed_name{
		font-size:14px !important;
		font-weight:500;
	}

	input[type="radio"]+span:before, input[type="radio"]+span:after {
    content: '';
    position: absolute;
    left: -15px !important;
    top: 4px !important;
    margin: 0px 0px 0px 31px;
    width: 16px;
    height: 16px;
    z-index: 0;
    -webkit-transition: .28s ease;
    transition: .28s ease;
}

 .modal-body{
   margin:0px !important;
   /*background-color:#f5f5f0*/
   margin-left:25px !important;
   width:90%;
 }

 .modal-header {
   padding: 30px;
 }

 #mail_sub{
   background-color: #7864e9 !important;
   height:42px !important;
   padding: 0px 2rem !important;
   font-size:13px;
 }

 #mailto_id{
   border:1px solid #e0e0d1 !important;
   height:2rem !important;
   margin:0 0 8px 0 !important;
 }

 #subject{
   border:1px solid #e0e0d1 !important;
   height:2rem !important;
   margin:0 0 0px 0 !important;
 }

 .plain-page-header {
    padding: 20px 0;
}

 #sr_cc_mailto{
   border:1px solid #e0e0d1 !important;
   height:2rem !important;
   margin:0 0 0px 0 !important;
 }

 #message{
   border:1px solid #e0e0d1 !important;
 }

 #mail_close{
   background-color:white;
   color:#c2c2a3;
   font-size:13px;
 }

 .filed_name{
   font-size:14px;
   color:#595959;
 }

 #cke_1_contents{
   height:120px !important;
 }

 
</style>
    <!-- START MAIN -->
    <div id="main" style="padding-left:0px !important;">
      <!-- START WRAPPER -->
      <div class="wrapper">
        <!-- START LEFT SIDEBAR NAV-->
        <?php //$this->load->view('template/sidebar.php');?>
          <section id="content" class="bg-theme-gray">
            <div class="container">
              <div class="plain-page-header">
                <div class="row">
                  <div class="col l6 s12 m6">
                    <!--a class="go-back underline" href="<?php echo base_url();?>community/my-connections">Back to My Connections</a-->
                  </div>
                  <div class="col l6 s12 m6">
                    <div class="right-2">

                    </div>

                  </div>
                </div>
              </div>
            </div>
            <div class="container" style="padding:0 3%;">
				<div class="row">
					<div class="col s12 m12 l12"><!--before-red-bg red-bg-right-->
						<div class="col l12 m12 s12 view-box white-box">
							<div class="row">
								<div class="col l12 m12 s12" style="text-align:right; padding-top:20px !important; padding-right:40px !important;">
									<a href="<?php echo $result[0]['facebook']; ?>" target="_blank"><img width="20" height="20" src="<?php echo base_url(); ?>asset/css/img/icons/facebook.png" alt="Logo"/></a>
									<a href="<?php echo $result[0]['twitter']; ?>" target="_blank"><img width="20" height="20" src="<?php echo base_url(); ?>asset/css/img/icons/twitter.png" alt="Logo"/></a>
									<a href="<?php echo $result[0]['linkedin']; ?>" target="_blank"><img width="20" height="20" src="<?php echo base_url(); ?>asset/css/img/icons/linkedin.png" alt="Logo"/></a>
								</div>
								<!--div class="col l12 m12 s12" style="text-align:right; padding-top:20px !important; padding-right:40px !important;">
									<a href="<?php echo base_url(); ?>community/my_profile"><i class="material-icons" style="color: grey; font-size: 20px; ">edit</i></a>
								</div-->
							</div>

							<div class="row">
								<div class="col l12 m12 s12" style="text-align:center;">
									<?php if($result[0]['company_logo'] !=""){ ?>
									<img width="100" height="100" src="<?php echo DOC_ROOT_DOWNLOAD_PATH_CO ?>company_logos/<?php echo $result[0]['code'].'/'.$result[0]['company_logo']; ?>" alt="Logo" class="logo_co"></img>
									<?php } ?>
									<br><br>
									<label class="company_name"><b><?php echo strtoupper($result[0]['company_name']); ?></b></label>
								</div>
								<div class="col l12 m12 s12" style="text-align:center;">
									<label class="nature_buzz"><?php echo strtoupper($result[0]['nature']); ?></label>
								</div>
								<div class="col l12 m12 s12" style="text-align:center; margin-bottom:20px !important;">
									&nbsp&nbsp&nbsp
								</div>
								
								<!--Bottom Border -->

							</div>

							<?php

								if($result[0]['incorporation_date'])
								{
									$timestamp = strtotime($result[0]['incorporation_date']);
									$inc_date =  date('d F, Y', $timestamp);

								}else{
									$inc_date = '';
								}

								if($result[0]['company_size'])
								{
									$no_of_employee = $result[0]['company_size'];

								}else{

									$no_of_employee = '';
								}



								if($result[0]['company_revenue'])
								{
									if($result[0]['company_revenue'] ==  5 )
									{
										$revenue = '> '.$result[0]['company_revenue'].' CRORES';
									}
									if($result[0]['company_revenue'] ==  100)
									{
										$revenue = $result[0]['company_revenue'].' CRORES +';

									}else{

										$revenue = $result[0]['company_revenue'].' CRORES';
									}


								}else{
									$revenue = '';
								}
							?>

							



								<div class="row">

									

										

							

								
									
									<div class="col l12 m12 s12" style="margin-top: 20px; margin-left: 20px;">
										<div class="col l1 m1 s1"></div>
										<div class="col l1 m1 s1"></div>

										
										<?php if($inc_date != '') {?>
										<div class="col l3 m3 s3" style="text-align: -webkit-center;">
											<label class="cmp_info">DATE OF INCORPORATION</label><br>
											<label class="cmp_details" style="text-align: center;"><b><?php echo strtoupper($inc_date); ?></b></label>
										</div>
										<?php }?>

										<?php if($no_of_employee != '') {?>
										<div class="col l2 m2 s2" style="text-align: -webkit-center;">
											<label class="cmp_info">NO OF EMPLOYEES</label><br>
											<label class="cmp_details"><b><?php echo $no_of_employee; ?></b></label>
										</div>
										<?php }?>

										<?php if($revenue != '') {?>
										<div class="col l2 m2 s2" style="text-align: -webkit-center;">
											<label class="cmp_info">REVENUE</label><br>
											<label class="cmp_details"><b> <?php echo $revenue; ?></b></label>
										</div>
										<?php }?>
									</div>
								</div>




						</div>
					</div>
				</div>
			</div>

				<!-- Address Details Start --->
				<div class="row" style="padding:0 3%;">
					<div class="col s12 m12 l12">
						<div class="col l12 m12 s12 view-box white-box before-red-bg red-bg-right">
							<div class="row">
								<div class="col l12 m12 s12">
									<h6 class="white-box-title" style="padding: 11px 34px !important">Address Details</h6>
								</div>
							</div>
							<?php

							if(isset($result[0]['billing_address']))
							{
								$address = $result[0]['billing_address'].', '.$result[0]['name'].', '.$result[0]['state_name'].', '.$result[0]['country_name'].' '.$result[0]['billing_zipcode'];
							}else{
								$address="";
							}

							?>
							<div class="row">
								<div class="col l6 m6 s6">
									<div class="col l12 s12 m12">
										<div class="col l1 s1 m1 loc-add"><i class="material-icons view-icons">location_on</i></div>
										<div class="col l10 s10 m10">
											<span class="view-label">BILLING ADDRESS</span>
											<p class="view-content"><?php echo $address; ?> </p>
										</div>
									</div>
									<div class="col l12 s12 m12" style="margin:20px 0px 0px 55px !important; ">
										<div class="col l4 s4 m4">
											<label class="cmp_info">STATE</label><br>
											<label class="cmp_details"><b> <?php echo $result[0]['state_name']; ?> </b></label>
										</div>
										<div class="col l4 s4 m4">
											<label class="cmp_info">COUNTRY</label><br>
											<label class="cmp_details"><b> <?php echo $result[0]['state_name']; ?> </b></label>
										</div>
									</div>
								</div>
								<?php //echo '<pre>'; print_r($branch); echo '</pre>'; ?>
								<?php

								for($i=0; $i<count($branch); $i++)
								{
									?>
									<div class="col l6 m6 s6">
										<div class="col l12 s12 m12">
											<div class="col l1 s1 m1 loc-add"><i class="material-icons view-icons">location_on</i></div>
											<div class="col l10 s10 m10">
												<span class="view-label">BRANCH ADDRESS</span>
												<p class="view-content"> <?php echo $branch[$i]['address'].', '. $branch[$i]['name'].', '. $branch[$i]['state_name'].', '. $branch[$i]['country_name'] ?> </p>
											</div>
										</div>
										<div class="col l12 s12 m12" style="margin:20px 0px 0px 55px !important; ">
											<div class="col l4 s4 m4">
												<label class="cmp_info">STATE</label><br>
												<label class="cmp_details"><b> <?php echo $branch[$i]['state_name']; ?> </b></label>
											</div>
											<div class="col l4 s4 m4">
												<label class="cmp_info">COUNTRY</label><br>
												<label class="cmp_details"><b> <?php echo $branch[$i]['country_name']; ?> </b></label>
											</div>
										</div>
									</div>
									<?php
								}

								?>

							</div>
						</div>
					</div>
				</div>
				<!-- Address Details End --->

				<div class="row" style="padding:0 1.2% 0 3%;">
					<div class="col s12 m12 l12">
						<!-- Service Offer Start --->	<!-- before-red-bg red-bg-right -->
						<div class="col l6 m6 s6 view-box white-box" style="width:40% !important; margin-right:20px !important;">
							<div class="row">
								<div class="col l12 m12 s12">
									<h6 class="white-box-title" style="padding: 11px 34px !important"> Services Offered</h6>
								</div>
							</div>

							<div class="row" style="width:94% !important;">
								<div class="col l12 s12 m12" style="margin-left:32px !important; margin-bottom:20px !important;">
									<?php
									$services = explode("|@|",$result['0']['services_keywords']);
									for($i=0;$i<count($services);$i++)
									{
										if($services[$i])
										{
											?>
											<div class="col l6 s6 m6" style="height:40px !important;">
												<label class="select_loc"> <?php echo strtoupper($services[$i]); ?> </label>
											</div>
											<?php
										}
									}
									?>

								</div>
							</div>
						</div>
						<!-- Service Offer End --->

						<!-- Web Presence Start --->
						<div class="col l3 m3 s3 view-box white-box" style="width:30% !important; margin-right:20px !important;">
							<div class="row">
								<div class="col l12 m12 s12">
									<h6 class="white-box-title" style="padding: 11px 34px !important">Web Presence</h6>
								</div>
							</div>
							<div class="row">
								<div class="col l12 s12 m12" style="margin-left:32px !important; margin-bottom:20px !important;">
									<label class="cmp_info">WEBSITE URL</label><br>
									<label class="cmp_details"><b> <?php echo $result[0]['website_url']; ?> </b></label>
								</div>
							</div>
							<div class="row">
								<div class="col l12 s12 m12" style="margin-left:32px !important; margin-bottom:20px !important;">
									<label class="cmp_info"> CLIENTS </label><br>
									<label class="cmp_details"><b> <?php  ?> </b></label>
								</div>
							</div>
							<div class="row">
								<div class="col l12 s12 m12" style="margin-left:32px !important;">
									<label class="cmp_info">CASE STUDIES</label><br>
									<label class="cmp_details"><b> <?php echo $result[0]['case_study']; ?></b></label>
								</div>
							</div>
						</div>
						<!-- Web Presence End --->

						<!-- Contact Details Start --->
						<div class="col l3 m3 s3 view-box white-box" style="width:25% !important;">
							<div class="row">
								<div class="col l12 m12 s12">
									<h6 class="white-box-title" style="padding: 11px 34px !important">Contact Details</h6>
								</div>
							</div>

							<div class="row">
								<div class="col l12 s12 m12" style="margin-left:32px !important; margin-bottom:20px !important;">
									<label class="cmp_info">CONTACT PERSON</label><br>
									<?php if(count($contact)>0) {?>
										<label class="cmp_details"><b> <?php echo $contact[0]['reg_username']; ?>  </b></label>
									<?php }?>
								</div>
							</div>
							<div class="row">
								<div class="col l12 s12 m12" style="margin-left:32px !important; margin-bottom:20px !important;">
									<label class="cmp_info"> CONTACT NUMBER </label><br>
									<?php if(count($contact)>0) {?>
									<label class="cmp_details"><b>  <?php echo $contact[0]['reg_mobile']; ?> </b></label>
									<?php }?>
								</div>
							</div>
							<div class="row">
								<div class="col l12 s12 m12" style="margin-left:32px !important;">
									<label class="cmp_info">EMAIL ID</label><br>
									<?php if(count($contact)>0) {?>
									<label class="cmp_details"><b>  <?php echo $contact[0]['reg_email']; ?> </b></label>
									<?php }?>
								</div>
							</div>
						</div>
						<!-- Contact Details End --->
					</div>
				</div>

				<?php if(count($offers) >= 1) {?>
	 				<?php $this->load->view('my-community/offers-on-profile'); ?>
	 			<?php }?>

				</div>
        </section>
    </div>
    </div>


	<!--<div id="send_mailto_comp_modal" class="modal modal-md modal-display" style="max-width:600px !important;">
		<div class="modalheader border-bottom" style="padding-bottom:15px; padding-top:25px !important; text-align:left;">
			<h4 style="padding-left:0px; font-size:20px !important;	">Send Email</h4>
			<a class="modal-close close-pop" style="margin-right:-15px !important;"><img src="<?php echo base_url();?>asset/images/popupdelete1.png" alt="delete"></a>
		</div>
		<div class="modalbody">
			<div class="choose_subscription" style="margin-top:15px;">
				<form class="" id="send_emailto_form_form" name="emailto_form" method="post">
					<div class="row border-bottom">
						<div class="col s12 m12 l12 login_form_textbox">
							<div class="col s12 m12 l12" style="margin-top:5px !important; margin-bottom:5px !important;">
								<span class="filed_name"> Send a message to <?php echo $result[0]['bus_company_name']; ?> </span>
							</div>
							<div class="col s12 m12 l12 input-group" style="margin-top:5px;">
								<textarea style="height:75px !important;" id="message" name="message" class="form-control" rows="7" id="message" placeholder="ENTER MESSAGE" style="font-size:13px;"required></textarea>
							</div>
							<div class="col s12 m12 l12" style="margin-top:5px !important; margin-bottom:20px !important;">
								<label class="fill_details"> Please enter your name, email id, phone number, so that service provider can reply you </label>
							</div>
						</div>
					</div>
					<input type="hidden" id='bus_id' name="bus_id" value="<?php echo $result[0]['bus_id']; ?>">
					<div class="row" style="margin-bottom:20px !important; margin-top:0px !important;">
						<div class="col l12 s12 m6"></div>
						<div class="col l12 s12 m6 fieldset" style='text-align:right; margin-left:-20px;'>
							<input id="tocomp_email_close" type="button" class="btn btn-default modal-close" data-dismiss="modal" style="font-size:12px; color:black;" value="CANCEL"></input>
							<input id="tocomp_email_proceed" type="submit" class="btn btn-default" data-dismiss="modal" style="font-size:12px; background-color:#7864e9; color:white;" value="SEND"></input>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>-->

	<script src="<?= base_url(); ?>asset/ckeditor/ckeditor.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {


		$("#send_emailto_form_form").submit(function(e){
				e.preventDefault();
				for ( instance in CKEDITOR.instances ) {
			        CKEDITOR.instances[instance].updateElement();
			    }
			}).validate({

				rules:{

					message:{
						required:true,
					},

				},

				messages:{

					message:{
						required:"Message is required",
					},

				},
				submitHandler:function(form){

						var bus_id = $('#bus_id').val();
						var email = $('#email').val();
						var message = $('#message').val();
 
						//$('#tocomp_email_proceed').prop('disabled', true);

								$.ajax({
								url:base_url+'community/email_company',
								type:"POST",
								data:{
										'csrf_test_name':csrf_hash,
										"bus_id":bus_id,
										"email":email,
										"message":message,
									 },
								success:function(res){

										if(res == 1)
										{
											$("#message").val('');
											$('#send_mailto_comp_modal').modal('close');
											$('#tocomp_email_proceed').prop('disabled', false);
											Materialize.toast('Email has been sent.', 2000,'green rounded');
										}
										else
										{
											$("#message").val('');
											$('#send_mailto_comp_modal').modal('close');
											$('#tocomp_email_proceed').prop('checked', false);
											Materialize.toast('Error. Email was not sent.', 2000,'red rounded');
										}
									},
					});
				},
			});


		CKEDITOR.replace( 'message', { toolbar : 'Basic' });

		var ce_type=$('#ce_type').val();
		$('#Subject').val('');
		CKupdate();
		$.ajax({
				url:base_url+'customise_emails/get_customise_email',
				type:"POST",
				data:{'csrf_test_name':csrf_hash,"ce_type":ce_type},
				success:function(res){
					var data = JSON.parse(res);
					//alert(data);
					//alert(ce_type);
					if(data != false){
						$('#subject').val(data[0].ce_subject);
						//$('#ce_id').val(data[0].ce_id);
						CKEDITOR.instances['message'].setData(data[0].ce_message);
					}
				},
			});
		});

		function CKupdate(){
			for ( instance in CKEDITOR.instances ){
				CKEDITOR.instances[instance].updateElement();
				CKEDITOR.instances[instance].setData('');
		   }
		}
	</script>




<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
 <script>
 $(document).ready(function($) {

   $("#mail_frm").submit(function(e){

       e.preventDefault();
       for ( instance in CKEDITOR.instances ) {
             CKEDITOR.instances[instance].updateElement();
         }
     }).validate({

       rules:{
         email_id:{
           required:true,
           //email:true,
           multiemail:true,
         },
         subject:{
           required:true,
         },
         message:{
           required:true,
         },
         sr_cc_mailto:{
           multiemail:true,
         },

       },

       messages:{
         email_id:{
           required:"Email address is required",
           email:"Invlid Email"
         },
         subject:{
           required:"Subject is required",
         },
         message:{
           required:"Message is required",
         },
       },
       submitHandler:function(form){

           var bus_id = $('#bus_id').val();
           var mailto_id = $('#mailto_id').val();
           var subject = $('#subject').val();
           var message = $('#message').val();
           var cc_mail = $('#sr_cc_mailto').val();
           $('.btn').prop('disabled', true);

               $.ajax({
               url:base_url+'community/email_company',
               type:"POST",
               data:{
					'csrf_test_name':csrf_hash,
                   "bus_id":bus_id,
                   "email_to":mailto_id,
                   "email_subject":subject,
                   "email_message":message,
                   "email_cc":cc_mail,
                  },
               success:function(res){

										if(res == 1)
										{
											$("#message").val('');
											$('.email-modal').modal('close');
											$('#tocomp_email_proceed').prop('disabled', false);
											Materialize.toast('Email has been sent.', 2000,'green rounded');
										}
										else
										{
											$("#message").val('');
											$('.email-modal').modal('close');
											$('#tocomp_email_proceed').prop('checked', false);
											Materialize.toast('Error. Email was not sent.', 2000,'red rounded');
										}
									},
         });
       },
     });

   jQuery.validator.addMethod(
       "multiemail",
        function(value, element) {
            if (this.optional(element)) // return true on optional element
                return true;
            var emails = value.split(/[;,]+/); // split element by , and ;
            valid = true;
            for (var i in emails) {
                value = emails[i];
                valid = valid &&
                        jQuery.validator.methods.email.call(this, $.trim(value), element);
            }
            return valid;
        },

       jQuery.validator.messages.email
   );

 });

 </script>


<script src="<?= base_url(); ?>asset/ckeditor/ckeditor.js"></script>
<script type="text/javascript">


$(document).ready(function() {


CKEDITOR.replace( 'message', { toolbar : 'Basic' });

var ce_type=$('#ce_type').val();
$('#Subject').val('');
 CKupdate();
 $.ajax({
         url:base_url+'customise_emails/get_customise_email',
         type:"POST",
         data:{'csrf_test_name':csrf_hash,"ce_type":ce_type},
         success:function(res){
                 var data = JSON.parse(res);
                 //alert(data);
                 //alert(ce_type);
                 if(data != false)
                 {
                   $('#subject').val(data[0].ce_subject);
                   //$('#ce_id').val(data[0].ce_id);
                   CKEDITOR.instances['message'].setData(data[0].ce_message);
                 }
            },
   });


});

function CKupdate(){
   for ( instance in CKEDITOR.instances ){
       CKEDITOR.instances[instance].updateElement();
       CKEDITOR.instances[instance].setData('');
  }
}

function con_company(id) {

		var con_client 	= $('#con-client'+id).val();
		var con_vendor 	= $('#con-vendor'+id).val();
		var con_both 	= $('#con-both'+id).val();
		var con_none 	= $('#con-none'+id).val();
		var messages	= $('#message'+id).val();
		var con_to_id	= $('#con_to'+id).val();
		if(csrf_hash===""){
			csrf_hash=csrf_hash;
		}
		$.ajax({

			dataType: 'json',
			type: "POST",
			url:base_url+'Community/add_connection_info',

			data:{'csrf_test_name':csrf_hash,'client':con_client,'vendor':con_vendor,'message':messages, 'bus_id_connected_to':con_to_id},

			success: function(data){
				if(data['csrf_hash']){
					csrf_hash=data['csrf_hash'];
				}
				$('#connect_modal'+id).modal('close');

				$('#con-client'+id).val('');
				$('#con-vendor'+id).val('');
				$('#con-both'+id).val('');
				$('#con-none'+id).val('');
				$('#message'+id).val('');
				$('#con_to'+id).val('');

				Materialize.toast('Your connection request has been sent', 2000, 'green rounded');
			}
		});
	}

</script>

<?php //$this->load->view('template/footer.php');?>