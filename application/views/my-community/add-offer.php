<?php $this->load->view('my-community/Cohorts-header');?>

<style type="text/css">
	#add_new_offer .uploader-placeholder { background-size: contain !important; }
	.disable_cls{ pointer-events:none !important; background-color:#e6e6e6 !important; }
	label.full-bg-label + input.adjust-height{
		height:45px !important;
	}

	.tooltip {
		position: relative;
		display: inline-block;
		margin-bottom:-6px !important;
	}

	.tooltip .tooltiptext {
	  visibility: hidden;
	  width: 150px;
	  background-color: #7864e9;
	  color: #fff;
	  text-align: center;
	  font-size:13px !important;
	  border-radius: 6px;
	  padding: 5px 0;
	  margin: 20px 0 -20px 0;
	  /* Position the tooltip */
	  position: absolute;
	  z-index: 1;
	}

	.tooltip:hover .tooltiptext {
	  visibility: visible;
	}

	.border-split-form .select-wrapper{
        padding: 7px 0 2px 0 !important;
		top: 0px !important;
	}

	.li-autocomplete{
		padding: 2px 0px 2px 20px !important;
		box-shadow: 0 0 1px 0 #bbb !important;
		background-color: #fff !important;
	}

	.ul-autocomplete{
		z-index: 99999 !important;
	}

	.select-dropdown {
		font-size: 14px !important;
	}

	.byWhen{
		right: 20px !important;
		top: 8px !important;
	}

	img.green-bel {
		width: 23px;
		margin: 12px 0 0 16px;
    }

    .full-bg{
        border-bottom: none !important;
        padding-top: 10px !important;
        display: inline-flex;
    }

    .no-type{
        color: #666;
        font-size: 12px;
        margin-top: 17px !important;
        font-weight: normal;
        text-transform: uppercase;
    }

    label.full-bg-label {
        color: #666;
    }

    .input-field label.select-label{
        color: #666;
    }

    .w-25{
		width: 25px !important;
    }

    .select-wrapper label.error:not(.active) {
         margin: -30px 0 0 -11px;
    }

    .selected_notification {
		border:none !important;
    }

    a.info-ref.tooltipped.info-tooltipped {
		position: absolute;
    }

    a.info-ref.tooltipped.contack-perosn.info-tooltipped {
 	   position: relative !important;
 	  right: 0px;
 	  margin-top: -34px !important;

	}

	/*----------START SEARCH DROPDOWN CSS--------*/
	.select2-container {
		width: 100% !important;
	}

	.select2-container--default .select2-selection--single {
		border:none;
	}

	input[type="search"]:not(.browser-default) {
		height: 30px;
		font-size: 14px;
		margin: 0;
		border-radius: 5px;
	}

	.select2-container--default .select2-selection--single .select2-selection__rendered {
		font-size: 12px;
		line-height: 35px;
		color: #444;
		font-weight: 500;
	}

	.select2-container--default .select2-selection--single .select2-selection__arrow {
		height: 32px;
	}

	.select2-search--dropdown {
		padding: 0;
	}

	input[type="search"]:not(.browser-default):focus:not([readonly]) {
		border-bottom: 1px solid #bbb;
		box-shadow: none;
	}

	.select2-container--default .select2-selection--single:focus {
		outline: none;
	}

	.select2-container--default .select2-results__option--highlighted[aria-selected] {
		background: #fffaef;
		color: #666;
	}

	.select2-container--default .select2-results > .select2-results__options {
		font-size: 14px;
		border-radius: 5px;
		box-shadow: 0px 2px 6px #B0B7CA;
	}

	.select2-dropdown {
		border: none;
		border-radius: 5px;
	}

	.select2-container .select2-selection--single {
		height: 45px !important;
	}

	.select2-results__option[aria-selected] {
		border-bottom: 1px solid #f2f7f9;
		padding: 14px 16px;
		border-top-left-radius: 0;
	}

	.select2-container--default .select2-search--dropdown .select2-search__field {
		/* border: none;*/
		border: 1px solid #d0d0d0;
		padding: 0 0 0 15px !important;
		width: 93.5%;
		max-width: 100%;
		background: #fff;
		border-radius: 4px;
		border-top-left-radius: 4px;
		border-top-right-radius: 4px;
	}

	.select2-container--open .select2-dropdown--below {
		margin-top: -15px;
	}

	ul#select2-currency-results {
		width: 97%;
	}

	.toolattr{
		position: absolute;
		top: -6px;
		right: 7px;
	}
	.primary-tip{
		right: -23px !important;
		top: -6px !important;
	}

	.select-wrapper + label.error{
		margin: 18px 0 0 -10px;
	}

	select.error + label.error:not(.active){
		margin: -20px 0 0 -10px;
	}

	select + label.error.active{
		margin-left: -10px;
	}
	/*---End dropdown error message format---*/

	.footer_btn{
		height:35px !important;
		line-height: 35px;
	}

	#gstmodal .select2-container--default .select2-selection--single {
		background-color: transparent !important;
	}

	#search_gstin{
		/*height:20px !important;*/
		padding: 0px 0px 0px 10px !important;
		margin-bottom:10px !important;
		border:1px solid #EEF2FE !important;
		border-radius:5px !important;
	}

	#entergstin{
		margin-top:4px !important;
	}

	.cr-client{
		border-bottom:1px solid #EEF2FE;
		margin-bottom:20px !important;
	}

	.cust-N-D{
		border:1px solid #fff;
	}

	#gstingo{
		border-radius:10px;
		border:1px solid #50f0AE !important;
		color:#ffffff;
		background-color:#50f0AE !important;
		margin:5px 0px 0px -25px !important;
	}

	.tipcredit{
		top:4px !important;
		right:7px !important;
	}

	.correct-gstin{
		margin:9px 0px 0px -25px;
	}

	#add_customer .high {
		border:1px solid #ff7d9a !important;
	}

	select-wrapper.high .select-dropdown {
		border:1px solid #ff7d9a !important;
	}

	.revenue-p img.pluse-icon-rev {
		margin: -13px -12px 0 0 !important;
	}

	#add_customer .select2-container .select2-selection--single {
		margin: 10px 0 0 0 !important;
		padding:0 0 0 0 !important;
	}

	#add_customer .select2-container--default .select2-selection--single .select2-selection__rendered {
		line-height: 28px !important;
	}

	#add_customer .select2-container--default .select2-selection--single .select2-selection__arrow {
		top:5px !important;
	}

	body.add-vo .invoi-drop input, select, textarea {
	    background: white !important;
	    width: 100% !important;
	    border-radius: 2px !important;
	    border: none !important;
	    height: 150px !important;
	    box-shadow: unset !important;
	}

	#offer_desc::placeholder{
		color: #666 !important;
	}

</style>

<div id="main" style="padding-left:0px !important;"> <!--Main-->
 	<div class="wrapper"> <!--Wrapper-->

 		<!---- SIDEBAR ---->
 		<?php //$this->load->view('template/sidebar.php');?>
 		<!---- END SIDEBAR ---->

 		<form class="create-company-form border-split-form" name="add_new_offer" id="add_new_offer" method="post" enctype="multipart/form-data" action="<?php echo base_url(); ?>community/add_new_offer"> <!--Form-->
			<?php $csrf = array(
					'name' => $this->security->get_csrf_token_name(),
					'hash' => $this->security->get_csrf_hash()
				);
				?>
				<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
 			<section id="content"> <!--Section-->
 				<div class="container"> <!--Container-->

 					<div class="plain-page-header"> <!-- Page header -->
 						<div class="row">
				 			<div class="col l6 s12 m6">
				 				<a class="go-back bg-l underline" href="<?php echo base_url(); ?>community/deals">Back to Deals</a>
				 			</div>
				 			<div class="col l6 s12 m6"></div>
				 		</div>
 					</div> <!-- End Page header -->

 					<div class="page-content"> <!--Page content-->
 						<div class="row"> <!--Page content Row-->

 							<div class="col s12 m12 l3"></div>

 							<div class="col s12 m12 l6"> <!--1st-->
 								<div class="box-wrapper bg-img-green bg-white shadow border-radius-6" style="width:85% !important; margin: 15px 0 0px 7% !important;"> <!--2nd-->
 									<div class="box-header">
										<h3 class="box-title">Create Deals</h3>
				                    </div>

				                    <div class="box-body"> <!--Box Body-->

				                    	<div class="row"> <!--Offer no. & Date-->
                      						<div class="col s12 m12 l12">

                      							<div class="input-field col s12 m12 l6 padd-n" style="border-right:1px solid #ccc;">
													<label for="offer_number" class="full-bg-label">Deal No.</label>
													<input id="offer_number" name="offer_number" class="readonly-bg-grey full-bg adjust-width border-right" type="text"  value="<?= $alertNo; ?>" readonly="readonly">
												</div>

												<div class="input-field col s12 m12 l6 padd-n">
													<label for="offer_date" class="full-bg-label">Deal Date</label>
													<input id="offer_date" name="offer_date" class="readonly-bg-grey full-bg adjust-width border-top-none valid" type="text" value="<?= date('d-m-Y'); ?>" readonly="readonly">
												</div>

                      						</div>
                      					</div> <!--End Offer no. & Date-->

                      					<div class="row"> <!--Offer title & image-->
                      						<div class="col s12 m12 l12">

											<div class="col s12 m12 l9">
												<div class="row">
													<div class="input-field">
														<label for="offer_title" class="full-bg-label">DEAL TITLE<span class="required_field">* </span></label>
														<input id="offer_title" name="offer_title" class="full-bg adjust-width" type="text"  style="height:45px !important;">
													</div>
												</div>
											</div>

											<div class="col s12 m12 l3">
												<div class="row">
													<div class="input-field tooltip">
														<label class="" style="font-size:11px; color:#696969; margin-top:-11px !important; margin-left:42px;">DEAL IMAGE</label>
														<div class="uploader-placeholder">
															<input type="file" class="hide-file" id="offer_image" name="offer_image">
														</div>
														<span class="tooltiptext">Only JPG, JPEG & PNG format. Upto 25MB</span>
													</div>
												</div>
											</div>

                      						</div>
                      					</div> <!--End Offer title & image-->

										<div class="row"> <!--Offer Description-->
											<div class="col s12 m12 l12">
												<div class="input-field bill-add border-bottom">
													<label for="offer_description" class="full-bg-label"></label>
													<textarea maxlength="1000" id="offer_description" name="offer_description" placeholder="DEAL DESCRIPTION" class="full-bg adjust-width" type="text"></textarea>
													<div style="font-size: 11px; margin: 0 0 0 80%;">(200 Character max)</div>
												</div>
											</div>
										</div> <!--End Offer Description-->

										<div class="row"> <!--Offer Category & Promo code-->
											<div class="col s12 m12 l12">

												<div class="col s12 m12 l6 input-set border-bottom"  id="countrysec">
													<span id="billing_country_error"></span>
													<div class="input-field required_field" style="height:73px;">
														<label for="offer_category" class="full-bg-label select-label">DEAL CATEGORY<span class="required_field"> *</span></label>
														<select class="js-example-basic-single country-dropdown check-label" name="offer_category" id="offer_category">
															<option value="">DEAL CATEGORY<span class="required_field">* </span></option>
															<option value="Education & E-learning">EDUCATION & E-LEARNING</option>
															<option value="Finance & Legal">FINANCE & LEGAL</option>
															<option value="Marketing Services">MARKETING SERVICES</option>
															<option value="Office Admin & Supplies">OFFICE ADMIN & SUPPLIES</option>
															<option value="SaaS & Software">SAAS & SOFTWARE</option>
															<option value="Travel & Hospitality">TRAVEL & HOSPITALITY</option>
														</select>
														<span id="offer_error"></span>
													</div>
												</div>

												<div  class="col s12 m12 l6 input-set border-bottom">
													<div class="row">
														<div class="input-field">
															<label for="promo_code" class="full-bg-label">PROMO CODE</label>
															<input id="promo_code" name="promo_code" class="full-bg adjust-width adjust-height" type="text" style="border-bottom:none !important; height:45px;">
														</div>
													</div>
												</div>

											</div>
										</div> <!--End Offer Category & Promo code-->

										<div class="row"> <!--Website Link-->
											<div class="col s12 m12 l12">
												<div class="input-field com-name">
													<label for="website_link" class="full-bg-label ex-com-name">WEBSITE LINK</label>
													<input id="website_link" name="website_link" class="full-bg adjust-width" type="text" placeholder="https://xebra.in">
												</div>
											</div>
										</div> <!--End Website Link-->

										<div class="row"> <!--Start date & End date-->
											<div class="col s12 m12 l12">

												<div class="input-field col s12 m12 l6 padd-n cust-N-D border-top-none border-bottom-none">
													<label for="start_date" class="full-bg-label">START DATE<span class="required_field">* </span></label>
													<input id="start_date" name="start_date" class="rangedatepicker bdatepicker full-bg icon-calendar-green adjust-width border-top-none" autocomplete="off" type="text">
												</div>

												<div class="input-field col s12 m12 l6 border-bottom-none border-top-none padd-n cust-N-D" style="border-left:1px solid #eef2fe;">
													<label for="end_date" class="full-bg-label">END DATE<span class="required_field">* </span></label>
													<input id="end_date" name="end_date" class="rangedatepicker bdatepicker icon-calendar-red full-bg  adjust-width border-top-none" autocomplete="off" type="text">
												</div>

											</div>
										</div> <!--End Start date & End date-->

										<div class="row"> <!--Start time & End time-->
											<div class="col s12 m12 l12">

												<div class="col s12 m12 l6 input-set" id="citysec">
													<div class="row">
														<div class="input-field">
															<label for="start_time" class="full-bg-label active">START TIME &nbsp; (e.g: 1800 / 0700)</label>
															<input id="start_time" name="start_time" class="full-bg adjust-width adjust-height" type="time" style="border-bottom:none !important;">
														</div>
													</div>
												</div>

												<div class="col s12 m12 l6 input-set">
													<div class="row">
														<div class="input-field">
															<label for="end_time" class="full-bg-label active">END TIME &nbsp; (e.g: 1800 / 0700)</label>
															<input id="end_time" name="end_time" class="full-bg adjust-width adjust-height" type="time" style="border-bottom:none !important;">
														</div>
													</div>
												</div>
											</div>
										</div> <!--End Start time & End time-->
                                         <?php $compCount=count($company);?>
										<div class="row"> <!--Company name-->
											<div class="col s12 m12 l12 border-top-none">
												<div class="input-field com-name disable_cls">
													<label for="company_name" class="full-bg-label ex-com-name">COMPANY NAME<span class="required_field">* </span></label>
													<input id="company_name" name="company_name" class="full-bg adjust-width" type="text" value="<?php if($compCount>0){echo $company[0]['bus_company_name'];}?>">
												</div>
											</div>
										</div> <!--End Company name-->

										<div class="row"> <!--Contact name-->
											<div class="col s12 m12 l12">
												<div class="input-field com-name disable_cls">
													<label for="contact_name" class="full-bg-label ex-com-name">CONTACT NAME<span class="required_field">* </span></label>
													<input id="contact_name" name="contact_name" class="full-bg adjust-width" type="text" value="<?php if($compCount>0){echo $company[0]['reg_username'];}?>">
												</div>
											</div>
										</div> <!--End Contact name-->

										<div class="row"> <!--Email id & Mobile no-->
											<div class="col s12 m12 l12">

												<div class="col s12 m12 l6 input-set" id="citysec">
													<div class="row">
														<div class="input-field disable_cls">
															<label for="email" class="full-bg-label">EMAIL ID<span class="required_field">* </span></label>
															<input id="email" name="email" class="full-bg adjust-width adjust-height" value="<?php if($compCount>0){echo $company[0]['reg_email'];}?>" type="text" style="border-bottom:none !important;">
														</div>
													</div>
												</div>

												<div class="col s12 m12 l6 input-set">
													<div class="row">
														<div class="input-field disable_cls">
															<label for="mobile_no" class="full-bg-label">MOBILE NO<span class="required_field">* </span></label>
															<input id="mobile_no" placeholder="1234567890" name="mobile_no" class="full-bg adjust-width adjust-height" value="<?php  if($compCount>0){echo $company[0]['reg_mobile'];}?>" type="text" style="border-bottom:none !important;">
														</div>
													</div>
												</div>
											</div>
										</div> <!--End Email id & Mobile no-->
				                    </div> <!--End Box Body-->
 								</div> <!--End 2nd-->
 							</div> <!--End 1st-->

							<div class="row"><div class="col s12 m12 l12"><p></p></div></div>
							<div class="row"><div class="col s12 m12 l12"><p></p></div></div>
							<div class="row"><div class="col s12 m12 l12"><p></p></div></div>
							<div class="row"><div class="col s12 m12 l12"><p></p></div></div>
							<div class="row"><div class="col s12 m12 l12"><p></p></div></div>

 						</div> <!--Page content Row-->
 					</div> <!--End Page content-->
 				</div> <!--End Container-->
 			</section> <!--End Section-->

			<div class="footer-btns last-sec" style="margin-top:-60px !important;"> <!--Save & Cancel button-->
				<div class="form-botom-divider" style="margin: 0 0 10px 0 !important;"></div>
					<div class="row" style="margin-bottom:7px !important;">
					<div class="text-center">

						<div class="col s12 m3 l4"></div>

						<div class="col s12 m12 l4">
							<i class="material-icons know-more-form" onclick="showStpe('.step1','.step2')">keyboard_arrow_up</i>
						</div>

						<div class="col s12 m12 l4">
							<button type="submit" class="add btn-flat theme-primary-btn theme-btn theme-btn-large footer_btn right">Save</button>
							<button class="btn-flat theme-flat-btn theme-btn theme-btn-large footer_btn right mr-5" type="button" onclick="location.href = '<?php echo base_url();?>community/offers';">CANCEL</button>
						</div>

					</div>
				</div>
			</div> <!--End Save & Cancel button-->

 		</form> <!--End Form-->
 	</div> <!--End Wrapper-->
</div> <!--End Main-->

<script type="text/javascript">
	$(document).ready(function() {

		$('.js-example-basic-single').select2();

		$('.select2-selection__rendered').each(function () {
			$(this).html($(this).html().replace(/(\*)/g, '<span style="color: red;">$1</span>'));
		});

		$("select").change(function () {
			if($(this).val()!=''){
				$(this).valid();
				$(this).closest('.input-field').find('.error').remove();
			}
		});
	});
</script>

<script>
$(document).ready(function() {
    
    		var end_time2=$("#start_time").val();
	if(end_time2 !=""){
		var timeSplit = end_time2.split(':'),
		hours,
		minutes,
		meridian;
		hours = timeSplit[0];
		minutes = timeSplit[1];
		if (hours > 12) {
			meridian = 'PM';
			hours -= 12;
		} else if (hours < 12) {
			meridian = 'AM';
			if (hours == 0) {
				hours = 12;
			}	
		} else {
			meridian = 'PM';
		}
		var end_time = hours + ':' + minutes + ' ' + meridian;
	}

	$('.hmbhkjhkh').click(function(){

		var offer_number 		= $('#offer_number').val();
		var offer_date 			= $('#offer_date').val();
		var offer_title 		= $('#offer_title').val();
		//var offer_image		= $('#offer_image').val();
		var offer_description 	= $('#offer_description').val();
		var offer_category 		= $('#offer_category').val();
		var promo_code			= $('#promo_code').val();
		var website_link		= $('#website_link').val();
		var start_date			= $('#start_date').val();
		var end_date			= $('#end_date').val();
		var start_time			= $('#start_time').val();
		var end_time			= $('#end_time').val();
		var company_name		= $('#company_name').val();
		var contact_name		= $('#contact_name').val();
		var email				= $('#email').val();
		var mobile_no			= $('#mobile_no').val();
		var files				= $('#offer_image').prop('files')[0];

		var fd 					= new FormData();
		fd.append('offer_number', offer_number);
		fd.append('offer_date', offer_date);
		fd.append('offer_title', offer_title);
		fd.append('offer_image', files);
		fd.append('offer_description', offer_description);
		fd.append('offer_category', offer_category);
		fd.append('promo_code', promo_code);
		fd.append('website_link', website_link);
		fd.append('start_date', start_date);
		fd.append('end_date', end_date);
		fd.append('start_time', start_time);
		fd.append('end_time', end_time);
		fd.append('company_name', company_name);
		fd.append('contact_name', contact_name);
		fd.append('email', email);
		fd.append('mobile_no', mobile_no);
		fd.append('csrf_test_name', csrf_hash);
		$.ajax({


			type: "POST",
			contentType: false,
			processData: false,
			url:base_url+'Community/add_offer_db',

			//data:{'offer_number':offer_number, 'offer_date':offer_date, 'offer_title':offer_title, 'offer_image':files, 'offer_description':offer_description, 'offer_category':offer_category, 'promo_code':promo_code, 'website_link':website_link, 'start_date':start_date, 'end_date': end_date, 'start_time':start_time, 'end_time':end_time, 'company_name':company_name, 'contact_name':contact_name, 'email':email, 'mobile_no':mobile_no},

			data: fd,

			success: function(data){

				$('#offer_number').val('');
				$('#offer_date').val('');
				$('#offer_title').val('');
				$('#offer_image').val('');
				$('#offer_description').val('');
				$('#offer_category').val('');
				$('#promo_code').val('');
				$('#website_link').val('');
				$('#start_date').val('');
				$('#end_date').val('');
				$('#start_time').val('');
				$('#end_time').val('');
				$('#company_name').val('');
				$('#contact_name').val('');
				$('#email').val('');
				$('#mobile_no').val('');

				Materialize.toast('New offer added successfully', 4000,'green rounded');
				location.href=base_url+'community/offers';

			}
		});

	});
});
</script>

<?php $this->load->view('template/footer.php');?>
