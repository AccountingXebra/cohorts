<?php $this->load->view('my-community/Cohorts-header');?>

<style type="text/css">
	input{
		height:45px !important;
	}
	.tooltip {
		position: relative;
		display: inline-block;
		margin-bottom:-6px !important;
	}

	#edit_event .select2-container--default .select2-selection--single {
    	background-color: transparent;
    	border-radius: 4px;
    	padding-top: 15px;
    	margin-left: -6px;
	}

	.tooltip .tooltiptext {
	  visibility: hidden;
	  width: 150px;
	  background-color: #7864e9;
	  color: #fff;
	  text-align: center;
	  font-size:13px !important;
	  border-radius: 6px;
	  padding: 5px 0;
	  margin: 20px 0 -20px 0;
	  /* Position the tooltip */
	  position: absolute;
	  z-index: 1;
	}

	.tooltip:hover .tooltiptext {
	  visibility: visible;
	}

	.border-split-form .select-wrapper{
        padding: 7px 0 2px 0 !important;
		top: 0px !important;
	}

	.li-autocomplete{
		padding: 2px 0px 2px 20px !important;
		box-shadow: 0 0 1px 0 #bbb !important;
		background-color: #fff !important;
	}

	.ul-autocomplete{
		z-index: 99999 !important;
	}

	.select-dropdown {
		font-size: 14px !important;
	}

	.byWhen{
		right: 20px !important;
		top: 8px !important;
	}

	img.green-bel {
		width: 23px;
		margin: 12px 0 0 16px;
    }

    .full-bg{
        border-bottom: none !important;
        padding-top: 10px !important;
        display: inline-flex;
    }

    .no-type{
        color: #666;
        font-size: 12px;
        margin-top: 17px !important;
        font-weight: normal;
        text-transform: uppercase;
    }

    label.full-bg-label {
        color: #666;
    }

    .input-field label.select-label{
        color: #666;
    }

    .w-25{
		width: 25px !important;
    }

    .select-wrapper label.error:not(.active) {
         margin: -30px 0 0 -11px;
    }

    .selected_notification {
		border:none !important;
    }

    a.info-ref.tooltipped.info-tooltipped {
		position: absolute;
    }

    a.info-ref.tooltipped.contack-perosn.info-tooltipped {
 	   position: relative !important;
 	  right: 0px;
 	  margin-top: -34px !important;

	}

	/*----------START SEARCH DROPDOWN CSS--------*/
	.select2-container {
		width: 100% !important;
	}

	.select2-container--default .select2-selection--single {
		border:none;
	}

	input[type="search"]:not(.browser-default) {
		height: 30px;
		font-size: 14px;
		margin: 0;
		border-radius: 5px;
	}

	.select2-container--default .select2-selection--single .select2-selection__rendered {
		font-size: 12px;
		line-height: 35px;
		color: #444;
		font-weight: 500;
	}

	.select2-container--default .select2-selection--single .select2-selection__arrow {
		height: 32px;
	}

	.select2-search--dropdown {
		padding: 0;
	}

	input[type="search"]:not(.browser-default):focus:not([readonly]) {
		border-bottom: 1px solid #bbb;
		box-shadow: none;
	}

	.select2-container--default .select2-selection--single:focus {
		outline: none;
	}

	.select2-container--default .select2-results__option--highlighted[aria-selected] {
		background: #fffaef;
		color: #666;
	}

	.select2-container--default .select2-results > .select2-results__options {
		font-size: 14px;
		border-radius: 5px;
		box-shadow: 0px 2px 6px #B0B7CA;
	}

	.select2-dropdown {
		border: none;
		border-radius: 5px;
	}

	.select2-container .select2-selection--single {
		height: 45px !important;
	}

	.select2-results__option[aria-selected] {
		border-bottom: 1px solid #f2f7f9;
		padding: 14px 16px;
		border-top-left-radius: 0;
	}

	.select2-container--default .select2-search--dropdown .select2-search__field {
		/* border: none;*/
		border: 1px solid #d0d0d0;
		padding: 0 0 0 15px !important;
		width: 93.5%;
		max-width: 100%;
		background: #fff;
		border-radius: 4px;
		border-top-left-radius: 4px;
		border-top-right-radius: 4px;
	}

	.select2-container--open .select2-dropdown--below {
		margin-top: -15px;
	}

	ul#select2-currency-results {
		width: 97%;
	}

	.toolattr{
		position: absolute;
		top: -6px;
		right: 7px;
	}
	.primary-tip{
		right: -23px !important;
		top: -6px !important;
	}

	.select-wrapper + label.error{
		margin: 18px 0 0 -10px;
	}

	select.error + label.error:not(.active){
		margin: -20px 0 0 -10px;
	}

	select + label.error.active{
		margin-left: -10px;
	}
	/*---End dropdown error message format---*/

	.footer_btn{
		height:35px !important;
		line-height: 35px;
	}

	#gstmodal .select2-container--default .select2-selection--single {
		background-color: transparent !important;
	}

	#search_gstin{
		/*height:20px !important;*/
		padding: 0px 0px 0px 10px !important;
		margin-bottom:10px !important;
		border:1px solid #EEF2FE !important;
		border-radius:5px !important;
	}

	#entergstin{
		margin-top:4px !important;
	}

	.cr-client{
		border-bottom:1px solid #EEF2FE;
		margin-bottom:20px !important;
	}

	.cust-N-D{
		border:1px solid #fff;
	}

	#gstingo{
		border-radius:10px;
		border:1px solid #50f0AE !important;
		color:#ffffff;
		background-color:#50f0AE !important;
		margin:5px 0px 0px -25px !important;
	}

	.tipcredit{
		top:4px !important;
		right:7px !important;
	}

	.correct-gstin{
		margin:9px 0px 0px -25px;
	}

	#add_customer .high {
		border:1px solid #ff7d9a !important;
	}

	select-wrapper.high .select-dropdown {
		border:1px solid #ff7d9a !important;
	}

	.revenue-p img.pluse-icon-rev {
		margin: -13px -12px 0 0 !important;
	}

	#add_customer .select2-container .select2-selection--single {
		margin: 10px 0 0 0 !important;
		padding:0 0 0 0 !important;
	}

	#add_customer .select2-container--default .select2-selection--single .select2-selection__rendered {
		line-height: 28px !important;
	}

	#add_customer .select2-container--default .select2-selection--single .select2-selection__arrow {
		top:5px !important;
	}

	body.add-vo .invoi-drop input, select, textarea {
	    background: white !important;
	    width: 100% !important;
	    border-radius: 2px !important;
	    border: none !important;
	    height: 150px !important;
	    box-shadow: unset !important;
	}

	#event_desc::placeholder{
		color: #666 !important;
	}
	.uploader-placeholder-event{ background-size:cover; }
</style>

<div id="main" style="padding-left:0px !important;"> <!--Main-->
 	<div class="wrapper"> <!--Wrapper-->
 		<!---- SIDEBAR ---->
 		<?php //$this->load->view('template/sidebar.php');?>
 		<!---- END SIDEBAR ---->
 		<form class="create-company-form border-split-form" name="edit_event" id="edit_event" method="post" enctype="multipart/form-data" action="<?php echo base_url(); ?>community/edit_event/<?php echo $results[0]->id;?>"> <!--Form-->
 			<?php $csrf = array(
					'name' => $this->security->get_csrf_token_name(),
					'hash' => $this->security->get_csrf_hash()
				);
				?>
				<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
			<section id="content"> <!--Section-->
 				<div class="container"> <!--Container-->

 					<div class="plain-page-header"> <!-- Page header -->
 						<div class="row">
				 			<div class="col l6 s12 m6">
				 				<a class="go-back bg-l underline" href="<?php echo base_url(); ?>community/events">Back to Events</a>
				 			</div>
				 			<div class="col l6 s12 m6"></div>
				 		</div>
 					</div> <!-- End Page header -->

 					<div class="page-content"> <!--Page content-->
 						<div class="row"> <!--Page content Row-->

 							<div class="col s12 m12 l3"></div>

 							<div class="col s12 m12 l6"> <!--1st-->
 								<div class="box-wrapper bg-img-green bg-white shadow border-radius-6" style="width:85% !important; margin: 15px 0 0px 7% !important;"> <!--2nd-->
 									<div class="box-header">
										<h3 class="box-title">Edit Event</h3>
				                    </div>

				                    <div class="box-body"> <!--Box Body-->

				                    	<div class="row"> <!--event no. & Date-->
                      						<div class="col s12 m12 l12">

                      							<div class="input-field col s12 m12 l6 padd-n" style="border-right:1px solid #ccc;">
													<label for="event_number" class="full-bg-label">event No.</label>
													<input id="event_number" name="event_number" class="readonly-bg-grey full-bg adjust-width border-right" type="text"  value="<?= @$results[0]->event_number; ?>" readonly="readonly">
												</div>

												<div class="input-field col s12 m12 l6 padd-n">
													<label for="event_date" class="full-bg-label">event Date</label>
													<input id="event_date" name="event_date" class="readonly-bg-grey full-bg adjust-width border-top-none valid" type="text" value="<?= ($results[0]->event_date != '' && $results[0]->event_date != '0000-00-00')?date("d/m/Y",  strtotime($results[0]->event_date)):''; ?>" readonly="readonly">
												</div>

                      						</div>
                      					</div> <!--End event no. & Date-->

				                    	<div class="row"> <!--event title & image-->
                      						<div class="col s12 m12 l12">

												<div class="col s12 m12 l9" style="border-right:1px solid #eef2fe !important;">
													<div class="row">
														<div class="input-field">
															<label for="event_name" class="full-bg-label">EVENT NAME<span class="required_field">* </span></label>
															<input id="event_name" name="event_name" class="full-bg adjust-width" type="text" value="<?= $results[0]->event_name; ?>" style="height:45px !important;">
														</div>
													</div>
												</div>

												<div class="col s12 m12 l3">
													<div class="row">
														<div class="input-field tooltip">
															<?php
									if($results[0]->event_image != '') {
										$image = $results[0]->event_image;
										$id = $results[0]->id;
										$event_image = '<img src="'.DOC_ROOT_DOWNLOAD_PATH.'event_image/'.$results[0]->id.'/'.$results[0]->event_image.'" height="85px" class="logo_style_2" width="85px">';
									} else {
										$event_image = '<img src="'.DOC_ROOT_DOWNLOAD_PATH.'event_image/'.$results[0]->id.'/'.$results[0]->event_image.'" height="85px" class="logo_style" width="85px">';
									}
								?>

								<!--<label class="" style="font-size:11px; color:#696969; margin-top: -8px !important; margin-left:30px;">EVENT IMAGE</label>-->
								        <?php if($results[0]->event_image!= ''){ ?>
                                  <div class="uploader-placeholder-event" data-profile_img="<?php echo $results[0]->event_image; ?>" >
                                  <?php } else{ ?>                                    
                                  <div class="uploader-placeholder-event">
									<label class="up_pic" style="font-size:11px; color:#696969; margin-top:-10px !important; margin-left:15px;">Upload your Photo</label>
                                  <?php } ?>
															
															<input type="file" class="hide-file-event" id="event_image" name="event_image">
															<input type="hidden" id="image_info_event" name="image_info_event" value='<?php  echo $results[0]->event_image;?>'/>
															<input type="hidden" id="image_id_event" name="image_id_event" value='<?php echo $results[0]->id;?>'/>

															</div>
															<span class="tooltiptext">Only JPG, JPEG & PNG format. Upto 25MB</span>
														</div>
													</div>
												</div>


                      						</div>
                      					</div> <!--End event title & image-->

										<div class="row">
                      						<div class="col s12 m12 l12">
											<div class="col s12 m12 l12">
												<div class="row">
													<div class="input-field">
														<label for="event_address" class="full-bg-label">ADDRESS<span class="required_field">* </span></label>
														<input id="event_address" name="event_address" class="full-bg adjust-width" value="<?php echo $results[0]->event_address; ?>" type="text"  style="height:45px !important;">
													</div>
												</div>
											</div>
                      						</div>
                      					</div>

										<div class="row">
											<div class="col s12 m12 l12">
												<div class="input-field col s12 m12 l6 padd-n" style="border-right:1px solid #eef2fe;">
													<label for="event_city" class="full-bg-label">CITY<span class="required_field"> *</span></label>
													<input id="event_city" name="event_city" value="<?php echo $results[0]->event_city; ?>" class="full-bg adjust-width" type="text"  style="height:45px !important;">
												</div>

												<div class="col s12 m12 l6 input-set border-bottom">
													<div class="input-field required_field label-active" style="height:74px;">
														<label for="event_nature" class="full-bg-label select-label">NATURE OF BUSINESS<span class="required_field"> *</span></label>
														<select class="js-example-basic-single country-dropdown check-label" name="event_nature" id="event_nature">
															<option value="<?php echo $results[0]->event_nature; ?>"><?php echo strtoupper($results[0]->event_nature); ?></option>
															<option value="Accounting & Taxation">ACCOUNTING & TAXATION</option>
															<option value="Advertising">ADVERTISING</option>
															<option value="Animation Studio">ANIMATION STUDIO</option>
															<option value="Architecture">ARCHITECTURE</option>
															<option value="Arts & Crafts">ARTS & CRAFTS</option>
															<option value="Audit & Tax">AUDIT & TAX</option>
															<option value="Brand Consulting">BRAND CONSULTING</option>
															<option value="Celebrity Management">CELEBRITY MANAGEMENT</option>
															<option value="Consultant">CONSULTANT</option>
															<option value="Content Studio">CONTENT STUDIO</option>
															<option value="Cyber Security">CYBER SECURITY</option>
															<option value="Data Analytics">DATA ANALYTICS</option>
															<option value="Digital Influencer">DIGITAL INFLUENCER</option>
															<option value="Digital & Social Media">DIGITAL & SOCIAL MEDIA</option>
															<option value="Direct Marketing">DIRECT MARKETING</option>
															<option value="Entertainment">ENTERTAINMENT</option>
															<option value="Event Planning">EVENT PLANNING</option>
															<option value="Florist">FLORIST</option>
															<option value="Foreign Exchange">FOREIGN EXCHANGE</option>
															<option value="Financial and Banking">FINANCIAL & BANKING</option>
															<option value="Gaming Studio">GAMING STUDIO</option>
															<option value="DESIGN & UI/UX">DESIGN & UI/UX</option>
															<option value="Hardware Servicing">HARDWARE SERVICING</option>
															<option value="Industry Bodies">INDUSTRY BODIES</option>
															<option value="Insurance">INSURANCE</option>
															<option value="Interior Designing ">INTERIOR DESIGNING</option>
															<option value="Legal Firm">LEGAL FIRM</option>
															<option value="Media Planning & Buying">MEDIA PLANNING & BUYING</option>
															<option value="Mobile Services">MOBILE SERVICES</option>
															<option value="Music">MUSIC</option>
															<option value="Non-Profit">NON-PROFIT</option>
															<option value="Outdoor / Hoarding">OUTDOOR / HOARDING</option>
															<option value="Photography">PHOTOGRAPHY</option>
															<option value="Printing">PRINTING</option>
															<option value="Production Studio">PRODUCTION STUDIO</option>
															<option value="PR / Image Management">PR / IMAGE MANAGEMENT</option>
															<option value="Publishing">PUBLISHING</option>
															<option value="Real Estate">REAL ESTATE</option>
															<option value="Recording Studio">RECORDING STUDIO</option>
															<option value="Research">RESEARCH</option>
															<option value="Sales Promotion">SALES PROMOTION</option>
															<option value="Staffing & Recruitment">STAFFING & RECRUITMENT</option>
															<option value="Stock & Shares">STOCK & SHARES</option>
															<option value="Technology (AI, AR, VR)">TECHNOLOGY (AI, AR, VR)</option>
															<option value="Tours & Travel">TOURS & TRAVELS</option>
															<option value="Training & Coaching">TRAINING & COACHING</option>
															<option value="Translation & Voice Over">TRANSLATION & VOICE OVER</option>
															<option value="Therapists">THERAPISTS</option>
															<option value="Visual Effects / VFX">VISUAL EFFECTS / VFX</option>
															<option value="Web Development">WEB DEVELOPMENT</option>
														</select>
														<span id="event_error"></span>
													</div>
												</div>

											</div>
										</div>

										<div class="row"> <!--Event Description-->
											<div class="col s12 m12 l12">
												<div class="input-field bill-add border-bottom">
													<label for="event_description" class="full-bg-label"></label>
													<textarea maxlength="1000" id="event_description" name="event_description" placeholder="ADD DESCRIPTION" class="full-bg adjust-width" type="text"><?php echo $results[0]->event_description; ?></textarea>
													<div style="font-size: 11px; margin: 0 0 0 80%;"><strong>(200 Character max)</strong></div>
												</div>
											</div>
										</div>

										<div class="row">
											<div class="col s12 m12 l12">
												<div class="col s12 m12 l6 input-set">
													<div class="row">
													<div class="input-field">
														<label for="website_link" class="full-bg-label ex-com-name">WEBSITE LINK</label>
														<input id="website_link" name="website_link" class="full-bg adjust-width" type="text" value="<?php echo $results[0]->website_link; ?>">
												</div>
													</div>
												</div>

												<div  class="col s12 m12 l6 input-set" style="border-right:none !important;">
													<div class="row">
														<div class="input-field">
															<label for="entry_fee" class="full-bg-label">ENTRY FEES (INR)</label>
															<input id="entry_fee" name="entry_fee" class="full-bg adjust-width numeric_number" value="<?php echo $results[0]->entry_fee; ?>" type="text" style="border-bottom:none !important;">
														</div>
													</div>
												</div>

											</div>
										</div>

										<div class="row">
											<div class="col s12 m12 l12">

												<div class="input-field col s12 m12 l6 padd-n cust-N-D border-top-none border-bottom-none">
													<label for="start_date" class="full-bg-label">START DATE</label>
													<input id="start_date" name="start_date" class="rangedatepicker bdatepicker full-bg icon-calendar-green adjust-width border-top-none" value="<?= ($results[0]->start_date != '' && $results[0]->start_date != '0000-00-00')?date("d/m/Y",  strtotime($results[0]->start_date)):''; ?>" type="text">
												</div>

												<div class="input-field col s12 m12 l6 border-bottom-none border-top-none padd-n cust-N-D" style="border-left:1px solid #eef2fe;">
													<label for="end_date" class="full-bg-label">END DATE</label>
													<input id="end_date" name="end_date" class="rangedatepicker bdatepicker icon-calendar-red full-bg  adjust-width border-top-none" value="<?= ($results[0]->end_date != '' && $results[0]->end_date != '0000-00-00')?date("d/m/Y",  strtotime($results[0]->end_date)):''; ?>" type="text">
												</div>

											</div>
										</div>

										<div class="row"> <!--Company name-->
											<div class="col s12 m12 l12 border-top-none">
												<div class="input-field com-name">
													<label for="company_name" class="full-bg-label ex-com-name">COMPANY NAME<span class="required_field">* </span></label>
													<input id="company_name" name="company_name" class="full-bg adjust-width" type="text" value="<?php echo $results[0]->company_name;?>" readonly="readonly">
												</div>
											</div>
										</div> <!--End Company name-->

										<div class="row">
											<div class="col s12 m12 l12 border-top-none">
												<div class="input-field com-name">
													<label for="contact_name" class="full-bg-label ex-com-name">CONTACT NAME<span class="required_field">* </span></label>
													<input id="contact_name" name="contact_name" class="full-bg adjust-width" type="text" value="<?php echo $results[0]->contact_name; ?>" readonly="readonly">
												</div>
											</div>
										</div>

										<div class="row"> <!--Email id & Mobile no-->
											<div class="col s12 m12 l12">

												<div class="col s12 m12 l6 input-set" id="citysec">
													<div class="row">
														<div class="input-field">
															<label for="email" class="full-bg-label">EMAIL ID<span class="required_field">* </span></label>
															<input id="email" name="email" class="full-bg adjust-width adjust-height" value="<?php echo $results[0]->email; ?>" type="text" style="border-bottom:none !important;">
														</div>
													</div>
												</div>

												<div class="col s12 m12 l6 input-set">
													<div class="row">
														<div class="input-field">
															<label for="mobile_no" class="full-bg-label">MOBILE NO<span class="required_field">* </span></label>
															<input id="mobile_no" placeholder="1234567890" name="mobile_no" class="full-bg adjust-width adjust-height" value="<?php echo $results[0]->mobile_no; ?>" type="text" style="border-bottom:none !important;">
														</div>
													</div>
												</div>
											</div>
										</div> <!--End Email id & Mobile no-->
				                    </div> <!--End Box Body-->
 								</div> <!--End 2nd-->
 							</div> <!--End 1st-->

							<div class="row"><div class="col s12 m12 l12"><p></p></div></div>
							<div class="row"><div class="col s12 m12 l12"><p></p></div></div>
							<div class="row"><div class="col s12 m12 l12"><p></p></div></div>
							<div class="row"><div class="col s12 m12 l12"><p></p></div></div>
							<div class="row"><div class="col s12 m12 l12"><p></p></div></div>

 						</div> <!--Page content Row-->
 					</div> <!--End Page content-->
 				</div> <!--End Container-->
 			</section> <!--End Section-->

			<div class="footer-btns last-sec" style="margin-top:-60px !important;"> <!--Save & Cancel button-->
				<div class="form-botom-divider" style="margin: 0 0 10px 0 !important;"></div>
					<div class="row" style="margin-bottom:7px !important;">
					<div class="text-center">

						<div class="col s12 m3 l4"></div>

						<div class="col s12 m12 l4">
							<i class="material-icons know-more-form" onclick="showStpe('.step1','.step2')">keyboard_arrow_up</i>
						</div>

						<div class="col s12 m12 l4">
							<button type="submit" class="add btn-flat theme-primary-btn theme-btn theme-btn-large footer_btn right">Save</button>
							<button class="btn-flat theme-flat-btn theme-btn theme-btn-large footer_btn right mr-5" type="button" onclick="location.href = '<?php echo base_url();?>community/events';">CANCEL</button>
						</div>

					</div>
				</div>
			</div> <!--End Save & Cancel button-->

 		</form> <!--End Form-->
 	</div> <!--End Wrapper-->
</div> <!--End Main-->

<script type="text/javascript">
	$(document).ready(function() {

		$('.js-example-basic-single').select2();

		$('.select2-selection__rendered').each(function () {
			$(this).html($(this).html().replace(/(\*)/g, '<span style="color: red;">$1</span>'));
		});

		$("select").change(function () {
			if($(this).val()!=''){
				$(this).valid();
				$(this).closest('.input-field').find('.error').remove();
			}
		});
	});
</script>

<script>
$(document).ready(function() {

	$('.ashkajs').click(function(){

		var event_number 		= $('#event_number').val();
		var event_date 			= $('#event_date').val();
		var event_name 			= $('#event_name').val();
		//var event_image		= $('#event_image').val();
		var event_address 		= $('#event_address').val();
		var event_city			= $('#event_city').val();
		var event_nature		= $('#event_nature').val();
		var event_description 	= $('#event_description').val();
		var website_link		= $('#website_link').val();
		var entry_fee 			= $('#entry_fee').val();
		var start_date			= $('#start_date').val();
		var end_date			= $('#end_date').val();
		var company_name		= $('#company_name').val();
		var contact_name		= $('#contact_name').val();
		var email				= $('#email').val();
		var mobile_no			= $('#mobile_no').val();
		//var files				= $('#event_image').prop('files')[0];

		var fd 					= new FormData();

		fd.append('event_number', event_number);
		fd.append('event_date', event_date);
		fd.append('event_name', event_name);
		//fd.append('event_image', files);
		fd.append('event_address', event_address);
		fd.append('event_city', event_city);
		fd.append('event_nature', event_nature);
		fd.append('event_description', event_description);
		fd.append('website_link', website_link);
		fd.append('entry_fee', entry_fee);
		fd.append('start_date', start_date);
		fd.append('end_date', end_date);
		fd.append('company_name', company_name);
		fd.append('contact_name', contact_name);
		fd.append('email', email);
		fd.append('mobile_no', mobile_no);
		fd.append('csrf_test_name', csrf_hash);
		

		$.ajax({

			type: "POST",
			contentType: false,
			processData: false,
			url:base_url+'Community/edit_event',

			//data:{'event_number':event_number, 'event_date':event_date, 'event_name':event_name, 'event_image':files, 'event_address':event_address, 'event_city':event_city, 'event_nature':event_nature, 'event_description':event_description, 'website_link':website_link, 'entry_fee':entry_fee, 'start_date':start_date, 'end_date': end_date, 'company_name':company_name, 'contact_name':contact_name, 'email':email, 'mobile_no':mobile_no},

			data: fd,

			success: function(data){

				$('#event_number').val('');
				$('#event_date').val('');
				$('#event_name').val('');
				//$('#event_image').val('');
				$('#event_address').val('');
				$('#event_city').val('');
				$('#event_nature').val('');
				$('#event_description').val('');
				$('#website_link').val('');
				$('#entry_fee').val('');
				$('#start_date').val('');
				$('#end_date').val('');
				$('#company_name').val('');
				$('#contact_name').val('');
				$('#email').val('');
				$('#mobile_no').val('');

				Materialize.toast('Event updated successfully', 4000,'green rounded');
				location.href=base_url+'community/events';
			}
		});

	});
});
</script>

<?php $this->load->view('template/footer.php');?>
