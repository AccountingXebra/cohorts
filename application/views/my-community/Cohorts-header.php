<!DOCTYPE html>

<html lang="en">

<head>

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

	<meta name="viewport" content="width=device-width, initial-scale=1">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta name="msapplication-tap-highlight" content="no">

	<meta name="description" content="">

	<meta name="keywords" content="">
	<!--<meta http-equiv="refresh" content="1200;url=<?php echo base_url();?>logoff" />-->

	<title>Xebra.in</title>

	<!-- Favicons-->

	<!-- <link rel="icon" href="../../images/favicon/favicon-32x32.png" sizes="32x32"> -->

	<!-- Favicons-->

	<!-- <link rel="apple-touch-icon-precomposed" href="../../images/favicon/apple-touch-icon-152x152.png"> -->

	<!-- For iPhone -->

	<meta name="msapplication-TileColor" content="#00bcd4">

	<!-- <meta name="msapplication-TileImage" content="images/favicon/mstile-144x144.png"> -->

	<!-- For Windows Phone -->

	<!-- CORE CSS-->

	<link rel="stylesheet" href="<?php echo base_url(); ?>public/css/select2.min.css" type="text/css"/>

	<link href="<?php echo base_url();?>asset/materialize/css/materialize.css" type="text/css" rel="stylesheet">

	<link href="<?php echo base_url();?>asset/css/style.css" type="text/css" rel="stylesheet">

	<!-- <link href="/your-path-to-fontawesome/css/solid.css" rel="stylesheet"> -->

    <link rel="stylesheet" href="<?php echo base_url('asset/datetimepick/css/bootstrap-datetimepicker.css'); ?>" />




    <?php /*?><link href="<?php echo base_url();?>asset/css/css/style-form.css" type="text/css" rel="stylesheet"><?php */?>

    <link href="<?php echo base_url();?>asset/css/custom.css" type="text/css" rel="stylesheet">

     <link href="<?php echo base_url();?>asset/css/userprofiletable.css" type="text/css" rel="stylesheet">

     <link href="<?php echo base_url();?>asset/css/jquery.dataTables.min.css" type="text/css" rel="stylesheet">
     <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.5.3/css/bootstrapValidator.css" type="text/css" rel="stylesheet">

  <!--Import Google Icon Font-->

  <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">

  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

  <link href="https://fonts.googleapis.com/css?family=Fira+Sans:100,400,500,600,700" rel="stylesheet">



    <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->


    <link href="<?php echo base_url();?>asset/css/perfect-scrollbar.css" type="text/css" rel="stylesheet">
    <link href="<?php echo base_url();?>asset/css/customei.css" type="text/css" rel="stylesheet">
	<script>var base_url = '<?php echo base_url(); ?>';</script>
	<script>var base_p = '<?php echo DOC_ROOT_DOWNLOAD_PATH_CO; ?>';</script>
	<script>var base_upload = '<?php echo DOC_ROOT_DOWNLOAD_PATH_COUPLOAD; ?>';</script>
	<script>var csrf_token = '<?php echo $this->security->get_csrf_token_name(); ?>';</script>
	<script>var csrf_hash = '<?php echo $this->security->get_csrf_hash(); ?>';</script>
	
	<script type="text/javascript" src="<?php echo base_url();?>asset/js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript"  src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>

	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.1/js/all.min.js"></script>
    <style type="text/css">
    .focus-icon{ opacity:0.6; }
	.focus-icon:hover{ opacity:1; }
	.nav-link:hover { color:#7864e9 !important; opacity:1 !important; }
	.nav ul.hide-on-med-and-down > li > a { padding: 0 3px; height: 70px; }
	.nav-item.active .navlink .ideal{ color:#7864e9 !important; }
	/*body.layout-semi-dark #header .nav-link .ideal { color: #808080 !important; }*/
	.dropdown-select-wrapper {
		margin-left:0px;
	}
	.notify h6{
		font-weight:500 !important;
	}

	.community-button:hover .community-icons{
	  background: url(<?=base_url('asset/css/img/icons/teamworks.png');?>) no-repeat center;
	}
	.community-button.active .community-icons{
	  background: url(<?=base_url('asset/css/img/icons/teamworks.png');?>) no-repeat center;
	}
	
	.community-icons {
	  background: url(<?=base_url('asset/css/img/icons/teamworks.png');?>) no-repeat center;
	  width: 40px;
	  float: left;
	  height: 68px !important;
	}
	/*...................................................*/
	#community-dropdown li {
		border-bottom: none !important;
	}
	i.settings-icons {
	  background-image: url(<?=base_url('asset/css/img/icons/settings-active.png');?>);
	  background-repeat: no-repeat;
	  background-position: center;
	  width: 20px;
	  height: 68px !important;
	}
	.setting-button:hover i.settings-icons {
	  background-image: url(<?=base_url('asset/css/img/icons/settings-active.png');?>);
	}
	.setting-button.active i.settings-icons {
	  background-image: url(<?=base_url('asset/css/img/icons/settings-active.png');?>);
	}
	.loading_data {
		position: fixed;
		left: 0px;
		top: 0px;
		width: 100%;
		height: 100%;
		z-index: 9999;
		background: url(<?=base_url('public/images/loading-ani.gif');?>) 50% 50% no-repeat rgb(249,249,249);
		background-size: 6%;
		opacity: .9;
	}
.notification-msg{
      font-size: 13px;
    font-weight: 300;
    color: #616161 !important;
}
.notifi-dropdown {
  white-space: normal !important;
  max-width: 400px !important;
  min-width: 400px !important;
}
.notisec {
    display: flex !important;
}
.notisec span {
   margin-right: 8px;
}

	#logout_for_new_bus_modal{
		left:150px;
		width:41% !important;
	}

	#logout_for_new_bus_modal .modal-body{
		margin: 0px;
	}

	#logout_for_new_bus_modal .modal-content {
		padding:0px;
	}

	#setting-dropdown li{
		background:  #7864e9 !important;
	}

	#community-dropdown li{
		background:  #7864e9 !important;
	}
	
	#infoo-dropdown li{
		background:  #7864e9 !important;
		border:none !important;
	}
	#resource-dropdown li{
		background:  #7864e9 !important;
		border:none !important;
	}
	#communityy-dropdown li{
		background:  #7864e9 !important;
		border:none !important;
	}
	#growth-dropdown, #growth-dropdown.active{ top:32px !important; }
	#communityy-dropdown, #communityy-dropdown.active{ top:32px !important; }
	#resource-dropdown, #resource-dropdown.active{ top:32px !important; }
	#infoo-dropdown, #infoo-dropdown.active{ top:32px !important; }
	#growth-dropdown li{
		background:  #7864e9 !important;
		border:none !important;
	}
	
	.gst_dropdown_select .dropdown-content li {
		background: #7864e9 !important;
		border-bottom: none  !important;
		overflow: hidden;
	}

	.gst_dropdown_select li{
		background:  #7864e9 !important;
	}

	.gst_dropdown_select span {
		color:#D0D3D4 !important;
	}

	.gst_dropdown_select span:hover {
		background:  #7864e9 !important;
		color:white !important;
		border-left:3px solid #50f1ae !important;
	}

	/*#setting-dropdown li:hover{
		border-left:4px solid #85e085 !important;
		background-repeat: no-repeat;
        background-position: left center;
	}*/

	.notify{
		padding-top:0px !important;
		margin-bottom:-15px !important;
	}

	.notify h6{
		font-size:11px !important;
	}

	.main_menu_options, .sub_menu_opt{
		color:#D0D3D4 !important;
	}
	
	.main_menu_options{	
		border-left: 3px solid #7864e9 !important;
	}
	
	#header .main_menu_options{
		color: #fff !important;
	}

	.main_menu_options:hover{
		color:white !important;
		border-left: 3px solid #50e3c2 !important;
	}

	/*After active Set Alert Dropdown*/
	.main_menu_options.active{
		background-color: rgba(0,0,0,0.12);
		color:white !important;
		border-left: 3px solid #50e3c2 !important;
	}

	.setalert-submenu > li > a > .sub_menu_opt:hover{
		color:white !important;
	}

	.company_dropdown span.caret {
		display:none;
	}

	ul#notifications-dropdown {
		overflow-y: scroll !important;
		/*height:400px !important;*/
	}

	.mod-name{
		font-size:8px;
	}

	input:-webkit-autofill,
	input:-webkit-autofill:hover,
	input:-webkit-autofill:focus,
	textarea:-webkit-autofill,
	textarea:-webkit-autofill:hover,
	textarea:-webkit-autofill:focus,
	select:-webkit-autofill,
	select:-webkit-autofill:hover,
	select:-webkit-autofill:focus {
		-webkit-text-fill-color: #595959;
		-webkit-box-shadow: 0 0 0px 1000px #000 inset;
		transition: background-color 5000s ease-in-out 0s;
	}

	.el-badge .el-badge__content.is-fixed {
		top: 5px;
		right: -3px;
		padding: 0 4px;
		font-size: 10px;
		font-weight: 500;
		line-height: 14px;
		background-color: #db2124;
		border-width: 2px;
		-webkit-transform: none;
		transform: none;
	}
	.el-badge__content.is-fixed {
		position: absolute;
		top: 28px;
		right: 29px;
		-webkit-transform: translateY(-50%) translateX(100%);
		transform: translateY(-50%) translateX(100%);
	}
	.el-badge__content{
		background-color: #ff85a1 !important;
		border-radius: 10px;
		color: #fff;
		display: inline-block;
		font-size: 9px;
		font-weight:500;
		height: 17px;
		line-height: 15px;
		padding: 0 6px;
		text-align: center;
		white-space: nowrap;
		border: 1px solid #fff;
	}

	nav ul.hide-on-med-and-down > li {
		margin: 2px 0 0 20px !important;
	}

	.nav-wrapper .one_gst span.caret {
		display:none !important;
	}
	.nav-link{
			text-align:center;
		}
		.navbar-nav li {
			position: relative !important;
			margin-right:30px !important;
		    font-weight:500!important;
		}
		
		#navbarSupportedContent{
			padding-left:39.2%;
		}
		.nav-link{
			text-align:center;
		}
		
		.icon-name{
			font-size:13px;
			/*font-weight:bold;*/
		}
		
		.dropdown{
			padding:10px 0;
		}
		
		.user-name, .down, .user-display-name{
			color:#000 !important;
		}
		
		#navbarSupportedContent .navbar-nav .nav-item:hover{
			box-shadow: 0 1px 10px #7864e94d;
		}
		
		.dropdown:hover{
			box-shadow: none !important;
		}
		
		.up{
			display:none;
		}

		.one_gst .gst_dropdown_select{
			pointer-events: none !important;
		}
		.user-pofile-image{
			border-bottom:1px solid #fff;
			padding:10px 2px;
			text-align:center;
		}
		body.layout-semi-dark #header nav.navbar-color {
			height: 72px;
			padding: 5px 0 0 0;
		}
		
		.head-img:hover{
			opacity:0.5;
		}
		
		.user-short-details .mail{
			color:#ccc;
		}
		
		body.layout-semi-dark #header .nav-link .ideal:hover{
			color:#000 !important;
			font-weight:400;
		}
		
		.nav-item{
			height:70px !important;
		}
		
		.nav-item:hover{
			color:#7864e9 !important;
			border-bottom:3px solid #7864e9 !important;
		}
		
		nav ul li.active{
			background-color:#fff !important;
		}
		.nav-item.active{
			color:#7864e9 !important;
			border-bottom:3px solid #7864e9 !important;
		}
		
		#blog-dropdown li{
			background:  #7864e9 !important;
			border-bottom: none !important;
		}
		.news-icon{ opacity:0.6; }
		.news-icon:hover{ opacity:1; }
		
		/*.focus-icon{ opacity:0.6; }
		.focus-icon:hover{ opacity:1; }*/
		@media only screen and (max-width: 600px) {
			.hide-on-med-and-down{ display:block !important; margin: 6px 0 0 -36px !important; padding: 10px 7.5% 0 0; border-bottom:1px solid #ccc; background-color:#fff; }
			.navbar-fixed nav{ position:inherit; }
			.cohortLogo{ height:75px !important; width:165px !important; }
		}
		.community-button, .info-button{ width: 88% !important; }
    </style>
</head>

<body class="layout-semi-dark white">
<div class="loading_data"></div>


    <?php

        $reg_id=$this->session->userdata['user_session']['reg_id'];
        $bus_id=$this->session->userdata['user_session']['bus_id'];

        if($reg_id) {

        $notifications = $this->Adminmaster_model->selectData('notifications','*',array('reg_id'=>$reg_id,'bus_id'=>$bus_id,'status'=>'Active', 'clear_noti' => 0,'noti_type'=>'connection_request_recd','noti_type'=>'birthday_anniversary_alert',),'noti_id','DESC');
        $newNotif = $this->Adminmaster_model->selectData('notifications','ref_id',array('reg_id'=>$reg_id,'bus_id'=>$bus_id,'status'=>'Active','noti_read_status'=>0 ,'clear_noti' => 0,'noti_type'=>'connection_request_recd', 'noti_type'=>'birthday_anniversary_alert',),'noti_id','DESC');

      }

    ?>

    <!-- Start Page Loading -->

   <!-- <div id="loader-wrapper">

		<div id="loader"></div>

		<div class="loader-section section-left"></div>

		<div class="loader-section section-right"></div>

    </div> -->

		<?php
      	  //$current_user =  $this->session->userdata('aduserData');
        ?>

    <header id="header" class="page-topbar">

      <!-- start header nav-->

      <div class="navbar-fixed">

        <nav class="navbar-color gradient-45deg-purple-deep-orange gradient-shadow">

          <div class="nav-wrapper">

            <div class="dropdown-select-wrapper">

              <div class="company_dropdown" style="margin-left:0%; /*border-right:1px solid rgb(229,235,247);*/">
				<div id="eazy_logo" style="background-color:white !important; width:252px !important; height:67px !important;">
					<a href="<?php echo base_url();?>"><img width="165" height="75" style="margin:-10px 10px 5px 18% !important;" src="<?php echo base_url(); ?>public/images/xebracohorts_c.png" alt="Logo" class="text-hide-collapsible cohortLogo logo_style_2"/></a>
				</div>
              </div>

			<ul class="right hide-on-med-and-down">
              <li>
                <a href="javascript:void(0);" class="waves-effect waves-block waves-light profile-button" data-activates="profile-dropdown">
                  <span class="avatar-status avatar-online">
                    <!--<?php // if($current_user['aduserprofilephoto'] != '') {?>
                      <img src="<?php //echo base_url();?>uploads/profile_images/<?php // echo $current_user['aduserprofilephoto']; ?>" alt="">
                    <?php //} ?>-->

                    <?php 
					//$imgUrl= 'https://'.$_SERVER['HTTP_HOST'].'/public/upload/personal_images/'.$this->session->userdata['user_session']['reg_id'].'/'.$this->session->userdata['user_session']['user_photo'];
					$imgUrl= DOC_ROOT_DOWNLOAD_PATH_CO.'personal_images/'.$this->session->userdata['user_session']['reg_id'].'/'.$this->session->userdata['user_session']['user_photo'];
					$filefound = "";
					if (!file_exists($imgUrl)) {   
						$filefound = '0';
					}
					if($this->session->userdata['user_session']['user_photo']!="" || $filefound !='0'){ ?>
						<img src="<?php echo $imgUrl;?>" alt="profile-pic">
                    <?php }else{ ?>
						<img src="<?php echo base_url();?>asset/css/img/icons/client.png" alt="profile"><!--//$imgUrl=base_url().'public/upload/personal_images/'.$this->session->userdata['user_session']['reg_id'].'/'.$this->session->userdata['user_session']['user_photo']; ?>-->
                   <?php } ?>

                  </span>
                 <!-- substr($this->session->userdata['user_session']['ei_username'],0,8) -->
				 <?php $arr = explode(' ',trim($this->session->userdata['user_session']['ei_username'])); ?>
                  <span class="user-name">Hi
					<span class="user-display-name" style="width:2px;"><?php echo ucfirst($arr[0]); ?>
					</span>
				  </span>
				  <span class="arrow-icon down">&#9660;</span>
                  <span class="arrow-icon up">&#x25B2;</span>
                </a>
              </li>
              <li>
				<a style="width: 145%;" href="javascript:void(0);" class="waves-effect waves-block waves-light set notification-button" data-activates="notifications-dropdown">
				<i class="notifications-icons"></i>
				<sup class="el-badge__content notiCount el-badge__content--undefined is-fixed"><?= ($newNotif!='')?count($newNotif):''; ?></sup>
				</a>
              </li>
				<li class="nav-item" style="height:64px !important;">
    					<a style="margin: -48px 0 0 -6px;" class="nav-link" data-portal="Xebra Cohorts" data-ne="<?php echo @$this->session->userdata['user_session']['email'] ?>"; data-mobile="<?php echo @$this->session->userdata['user_session']['mobile'] ?>"; data-name="<?php echo @$this->session->userdata['user_session']['ei_username'] ?>"; href="javascript:void(0)" id="xebra_redirect"><img style=" margin: -29px 0 -44px 0;" width="35" height="35" src="<?php echo base_url();?>public/images/Xebra.png" alt="conn"></img></br><span class="icon-name ideal">XEBRA</span></a>
				</li>
				<li class="nav-item <?php if($this->router->fetch_class()=="community"){ get_active_tab('index');  if($this->router->fetch_method()=="index" || $this->router->fetch_method()=="accounting_terms" || $this->router->fetch_method()=="financial_ratios" || $this->router->fetch_method()=="videos") { echo "active"; }} ?>" style="margin: -3px 12px 0 15px !important; width: 90px;">
					<a style="margin:-51% 0 0 0%; height:70px;" class="nav-link learnlink info-button" data-activates="infoo-dropdown" href="#"><img style="margin:0 0 -50px 0px;" class="head-img learn-1" width="40" height="40" src="<?php echo base_url();?>public/images/Learnings.png" alt="conn"></img><img style="margin:0 0 -50px 0px;" class="head-img learn-2" width="40" height="40" src="<?php echo base_url();?>public/images/Learnings1.png" alt="conn" hidden></img></br><span class="icon-name ideal">LEARNING</span><img style="margin:-43px 0px 65px 70px; opacity:0.7;" class="news-icon head-img" width="10" height="10" src="<?php echo base_url();?>public/images/caret-down.png" alt="conn"></img></a>
				</li>
				<li class="nav-item <?php if($this->router->fetch_class()=="community"){ get_active_tab('index');  if($this->router->fetch_method()=="index" || $this->router->fetch_method()=="coworking_list" || $this->router->fetch_method()=="incubator_list" || $this->router->fetch_method()=="info_list") { echo "active"; }} ?>" style="margin: -3px 12px 0 4px !important; width: 105px;">
					<a style="margin:-43% 0 0 0%; height:70px;" class="nav-link resourcelink resource-button" data-activates="resource-dropdown" href="#"><img style=" margin: -4px -5px -40px; opacity:0.6" class="head-img res-1" width="35" height="28" src="<?php echo base_url();?>public/images/resources.png" alt="conn"></img><img style=" margin: -4px -5px -44px" class="head-img res-2" width="35" height="28" src="<?php echo base_url();?>public/images/resources_red.png" alt="conn" hidden></img></br><span class="icon-name ideal">RESOURCES</span><img style="margin:-43px 0px 65px 90px; opacity:0.7;" class="news-icon head-img" width="10" height="10" src="<?php echo base_url();?>public/images/caret-down.png" alt="conn"></img></a>
				</li>
				<li class="nav-item <?php if($this->router->fetch_class()=="community"){ get_active_tab('index');  if($this->router->fetch_method()=="index" || $this->router->fetch_method()=="my_connections" || $this->router->fetch_method()=="events" || $this->router->fetch_method()=="interviews" || $this->router->fetch_method()=="news" ) { echo "active"; }} ?>" style="margin: -3px 15px 0 15px !important; width: 100px;">
					<a style="margin:-45% 0 0 0%; height:70px;" class="nav-link communitylink community-button" data-activates="communityy-dropdown" href="#"><img style="margin: -0px 0 -47px 0;" class="head-img commu-1" width="36" height="36" src="<?php echo base_url();?>public/images/connection.png" alt="conn"></img><img style="margin: -0px 0 -47px 0;" class="head-img commu-2" width="36" height="36" src="<?php echo base_url();?>public/images/connection1.png" alt="conn" hidden></img></br><span class="icon-name ideal">COMMUNITY</span><img style="margin:-43px 0px 65px 83px; opacity:0.7;" class="news-icon head-img" width="10" height="10" src="<?php echo base_url();?>public/images/caret-down.png" alt="conn"></img></a>
				</li>
				<li class="nav-item <?php if($this->router->fetch_class()=="community"){ get_active_tab('index');  if($this->router->fetch_method()=="index" || $this->router->fetch_method()=="my_marketplace" || $this->router->fetch_method()=="challenge" || $this->router->fetch_method()=="deals" ) { echo "active"; }} ?>" style="margin: -3px 10px 0 15px !important; width: 100px;">
					<a style="margin:-45% 0 0 -10%; height:70px;" class="nav-link growth-dropdown" data-activates="growth-dropdown" href="#"><span class="icon-name ideal"><i style="margin: 0px 0 -38px 0px; font-size: 22px; opacity:0.5;" class="fas fa-chart-line"></i></br>GROWTH</span><img style="margin:-43px 0px 65px 78px; opacity:0.7;" class="news-icon head-img" width="10" height="10" src="<?php echo base_url();?>public/images/caret-down.png" alt="conn"></img></a>
				</li>
				
				<li class="nav-item" style="margin: -3px -5px 0 -5px !important; width: 75px;" hidden>
					<a style="margin-top: -48px;" class="nav-link info-button" data-activates="info-dropdown" href="#"><img style=" margin: -29px 0 -44px 15px;" class="head-img" width="35" height="35" src="<?php echo base_url();?>public/images/Discussion.png" alt="conn"></img></br><span class="icon-name ideal">RESOURCES</span></a>
				</li>
				<li class="nav-item <?php if($this->router->fetch_class()=="resource_center"){ get_active_tab('index');  if($this->router->fetch_method()=="index" || $this->router->fetch_method()=="accounting_terms" || $this->router->fetch_method()=="financial_ratios" ) { echo "active"; }} ?>" style="height: 60px; margin-left: 0px !important; margin-right:-28px;" hidden><!-- border-left: 1px solid #ccc; -->
					<a style="margin-top: -47px; padding: 0 20px 0 12px; width:20%;" class="nav-link community-button" data-activates="community-dropdown" href="#"><img style="margin: -29px 0 -44px 10px;" class="head-img" width="35" height="35" src="<?php echo base_url();?>public/images/Learnings.png" alt="conn"></img></br><span class="icon-name ideal">LEARNING</span></a>
				</li>
				<li class="nav-item <?php if($this->router->fetch_class()=="community"){ get_active_tab('index');  if($this->router->fetch_method()=="index" || $this->router->fetch_method()=="news" ) { echo "active"; }} ?>" style="border-left: 1px solid #ccc; height: 60px;" hidden>
					<a style="width:85px; margin-top: -47px; padding:0 0 0 2px;" class="nav-link" href="<?= base_url(); ?>news"><img style="margin: -33px 0 -37px 0;" class="news-icon head-img" width="25" height="20" src="<?php echo base_url();?>public/images/news.png" alt="conn"></img></br><span class="icon-name ideal">NEWS</span></a>
				</li>
				<!--li class="nav-item <?php //if($this->router->fetch_class()=="community"){ get_active_tab('index');  if($this->router->fetch_method()=="index" || $this->router->fetch_method()=="cohorts_blog" || $this->router->fetch_method()=="cohorts_faqs" ) { echo "active"; }} ?>" style="border-left: 1px solid #ccc; height: 60px;">
					<a style="margin-top: -47px; padding: 0 10px; width:20%;" class="nav-link community-button" data-activates="blog-dropdown" href="#"><i style="margin: -36px 0 -32px 29px; font-size: 17px;" class="fas fa-blog"></i></br><span style="padding-left: 20px;" class="icon-name">BLOG</span></a>
				</li-->
				<li style="margin:2px -8px 0 8px !important;" class="nav-item <?php if($this->router->fetch_class()=="community"){ get_active_tab('index');  if($this->router->fetch_method()=="index" || $this->router->fetch_method()=="deals" || $this->router->fetch_method()=="add_offer" || $this->router->fetch_method()=="edit_offer" ) { echo "active"; }} ?>" hidden>
					<a style="width:71px; margin-top: -47px; padding:0  20px;" class="nav-link" href="<?= base_url(); ?>community/deals"><img style="margin: -29px 0 -44px 0; opacity:0.5;" class="head-img" width="32" height="32" src="<?php echo base_url();?>public/images/deal2.png" alt="conn"></img></br><span class="icon-name ideal">DEALS</span></a><!-- <i style="margin: -29px 0 -33px 0; font-size:18px;" class='fas fa-file-alt'></i> -->
				</li>
				<li class="nav-item <?php if($this->router->fetch_class()=="community"){ get_active_tab('index');  if($this->router->fetch_method()=="index" || $this->router->fetch_method()=="events" || $this->router->fetch_method()=="add_events" || $this->router->fetch_method()=="edit_event" ) { echo "active"; }} ?>" style="border-left: 1px solid #ccc; height: 60px; margin:2px 0 0 10px !important;" hidden>
					<a style="width:80px; margin-top: -47px; padding:0 20px 0 20px;" class="nav-link" href="<?= base_url(); ?>community/events"><img style="margin: -29px 0 -44px 0;" class="head-img" width="36" height="36" src="<?php echo base_url();?>public/images/Events.png" alt="conn"></img></br><span class="icon-name ideal">EVENTS</span></a>
				</li>
				<li class="nav-item <?php if($this->router->fetch_class()=="community"){ get_active_tab('index');  if($this->router->fetch_method()=="index" || $this->router->fetch_method()=="challenge" ) { echo "active"; }} ?>" style="border-left: 1px solid #ccc; height: 60px;" hidden>
					<a style="width:85px; margin: -47px 0 0 6px; padding:0 0 0 2px;" class="nav-link" href="<?= base_url(); ?>community/challenge"><img style="margin: -29px 0 -40px 0;" class="head-img focus-icon" width="32" height="27" src="<?php echo base_url();?>public/images/focus.png" alt="conn"></img></br><span class="icon-name ideal">CHALLENGES</span></a>
				</li>
				<li class="nav-item <?php if($this->router->fetch_class()=="community"){ get_active_tab('index');  if($this->router->fetch_method()=="index" || $this->router->fetch_method()=="my_connections" ) { echo "active"; }} ?>" hidden>
					<a style="margin-top: -46px;" class="nav-link" href="<?= base_url(); ?>community/my-connections"><img style="margin: -29px 0 -44px 0;" class="head-img" width="35" height="35" src="<?php echo base_url();?>public/images/connection.png" alt="conn"></img></br><span class="icon-name ideal">CONNECTION</span></a>
				</li>
				<li class="nav-item <?php if($this->router->fetch_class()=="community"){ get_active_tab('index');  if($this->router->fetch_method()=="index" || $this->router->fetch_method()=="my_marketplace" ) { echo "active"; }} ?>" hidden>
					<a style="margin-top: -46px;" class="nav-link" href="<?= base_url(); ?>community/my-marketplace"><img style=" margin: -29px 0 -44px 0;" class="head-img" width="35" height="35" src="<?php echo base_url();?>public/images/Marketplace.png" alt="conn"></img></br><span class="icon-name ideal">MARKETPLACE</span></a>
				</li>
				<li class="nav-item">
					<!--a style="margin: -47px 0 0 15px;" class="nav-link" href="<?//= base_url(); ?>community/temp"><img style="margin: -29px 0 -44px 0;" width="35" height="35" src="<?php //echo base_url();?>public/images/Xebra.png" alt="conn"></img></br><span class="icon-name">Temp</span></a-->
				</li>
				<!--li class="nav-item">
					<a class="nav-link" href="#"><img width="35" height="35" src="Videos.png" alt="conn"></img></br><span class="icon-name">Videos</span></a>
				</li-->
				<!--li class="nav-item">
					<a style="margin-top: -46px;" class="nav-link" href="#"><img style="margin: -29px 0 -44px 0;" width="35" height="35" src="<?php //echo base_url();?>public/images/Giftbox.png" alt="conn"></img></br><span class="icon-name">Activate Gift</span></a>
				</li-->
            </ul>

            <!-- translation-button -->

            <ul id="translation-dropdown" class="dropdown-content">

              <li>

                <a href="#!" class="grey-text text-darken-1">

                  <i class="flag-icon flag-icon-gb"></i> English</a>

              </li>

              <li>

                <a href="#!" class="grey-text text-darken-1">

                  <i class="flag-icon flag-icon-fr"></i> French</a>

              </li>

              <li>

                <a href="#!" class="grey-text text-darken-1">

                  <i class="flag-icon flag-icon-cn"></i> Chinese</a>

              </li>

              <li>

                <a href="#!" class="grey-text text-darken-1">

                  <i class="flag-icon flag-icon-de"></i> German</a>

              </li>

            </ul>


          <!-- notifications-dropdown -->

          <?php  if(@$this->user_session["reg_trial_expire"] != 1){ ?>
          <ul id="notifications-dropdown" class="dropdown-content notifi-dropdown" style="margin-top:5px;">

            <li class="notify">

              <h6>NOTIFICATIONS
                    <?php
                    if(count($newNotif) == 0)
                    {
                      ?>

                      <?php
                    }else{
                      ?>
                        <span class="new badge notiCount"><?= ($newNotif!='')?count($newNotif):''; ?></span>
                      <?php
                    }

                    ?>
              </h6>

            </li>

            <li class="divider"></li>

          

            <?php
              // Show Notification of user
              if($notifications!=''){
              foreach($notifications as $notif) {
                  $today=date('Y-m-d');
                  $created_date = $notif->createdat;
                  $date_diff=strtotime($today) - strtotime(date("Y-m-d", strtotime($created_date)));
                  $days=floor($date_diff / (60 * 60 * 24));
                  $left_time='';

                  if($notif->noti_read_status==1)
                    {

                      ?>
                        <li class="notificationStatus" id="<?php echo $notif->noti_id ?>">
                      <?php

                    }else{

                      ?>
                        <li class="notificationStatus" id="<?php echo $notif->noti_id ?>" style="background-color: #f9e8e8 !important;">
                      <?php

                    }

                    ?>
                          <a class="grey-text text-darken-2 notisec">
                            <input type="hidden" name="link" id="link" value="<?php echo base_url();?><?=$notif->noti_url; ?>">
                            <div><span class="material-icons icon-bg-circle red">event_available</span></div><div style="word-break: break-word; vertical-align: middle; padding: 7px 0 0 0; margin:0% 0 0 0.5%;"> <?=  wordwrap($notif->noti_message,60); ?>
                            </div>
                          </a>

                          <time class="media-meta" datetime="2015-06-12T20:50:48+08:00"><?= $days; ?> days ago</time>

                        </li>
                    <?php

                  }
                }
              ?>
			  <?php if(count($newNotif) == 0) { ?>

			  <?php }else{ ?>
				<li class="notify">
				<p style="text-align:center; font-size:14px !important;"><a style="text-decoration: underline; color:#50E2C2 !important;" onclick="change_status();">Clear notifications</a></p>
				</li>
              <?php } ?>
          </ul>
          <?php }else{} ?>


          <?php if(@$this->user_session["reg_trial_expire"] != 1){ ?>

          <!-- Profile Dropdown -->
			    <ul id="profile-dropdown" class="dropdown-content user-profile-down">
              <li>
                <div class="user-pofile-image">
                 <!--  <img src="<?php //echo base_url(); ?>asset/images/user.jpg" class="user-img"> -->
                  <div class="user-image-layout"></div>
                  <div class="user-short-details">
                    <span class="name"><?php echo ucfirst($this->session->userdata['user_session']['ei_username']); ?></span>
                    <span class="mail"><?php echo $this->session->userdata['user_session']['email']; ?></span>
                  </div>
                </div>
              </li>
              <li class="user-profile-fix">
                <ul>

                  <?php if($this->session->userdata['user_session']['reg_admin_type'] == 3 || $this->session->userdata['user_session']['reg_admin_type'] == -1)
                  {
                      $bus_id_reg=$this->Adminmaster_model->selectData("businesslist","*",array('reg_id'=>$reg_id));
                      if(count($bus_id_reg)>0){
                   ?>

                  	<li>
                    <a style="font-size:13px;" href="<?= base_url(); ?>community/manage_profile?token=<?php echo $bus_id_reg[0]->bus_id; ?>"><!-- my_profile -->
                      MY PROFILE
                    </a>
                  </li>
              <?php }else{ ?>
                  <li>
                    <a style="font-size:13px;" href="<?= base_url(); ?>community/my_profile"><!-- my_profile -->
                      MY PROFILE
                    </a>
                  </li>
                  <?php } } ?>
                      <!--li>
                        <a class=" Password-change modal-trigger" href="#password-modal">
                          <i class="menu-round-icon"></i>
                          <span>CHANGE PASSWORD</span>
                        </a>
                      </li>
                      <li>
                        <a class=" deactivate modal-trigger" href="#deactive_myaccount_modal">
                          <i class="menu-round-icon"></i>
                          <span>DEACTIVATE ACCOUNT</span>
                        </a>
                      </li-->
					<li class="divider"></li>
					<!--<li class='my-comp'>
						<a style="font-size:13px;" id="refer_earn" href="<?php //echo base_url();?>refer_earn">
							MY REFERRAL PROGRAM
						</a>
					<ul class="userprofile-submenu">
						<li>
						<a>
						<i class="menu-round-icon"></i>
						<span>Add new Referal</span>
						</a>

						</li>
					</ul>-->
                  <!--</li>-->
					<li class="divider"></li>
				  <li hidden>
                    <a style="font-size:13px;" href="#" id="ac-menu" class="open">
                      MY ACCOUNT
                    </a>
                    <ul class="userprofile-submenu my_ac">
                      <?php if($this->session->userdata['user_session']['reg_istrial'] == 0){ ?>
                      <li>
						<!--a class="renew_sub modal-trigger modal-close" href="#subscription_modal"-->
                        <a style="font-size:13px;" href="<?php echo base_url(); ?>subscription/my_subscription">
                        <i class="menu-round-icon"></i>
                        <span>PURCHASE SUBSCRIPTION</span>
                        </a>
                      </li>
                    <?php } ?>

                      <li>
						<!--<a class="modal-trigger modal-close" href="#subscription_modal">-->
                        <a style="font-size:13px;" href="<?php echo base_url(); ?>subscription">
                          <i class="menu-round-icon"></i>
                          <span>MY BILLING HISTORY</span>
                        </a>
                      </li>
                    </ul>
                  </li>
                    <li class="divider"></li>
					  <li>
                        <a style="font-size:13px;" class=" Password-change modal-trigger" href="#password-modal">

                          <span>CHANGE PASSWORD</span>
                        </a>
                      </li>
				  <li class="divider"></li>
				  <li>
                    <a style="font-size:13px;" href="#feedback_modal" class="modal-trigger">
                      MY FEEDBACK
                    </a>
                  </li>
				  <li class="divider"></li>
                  <li class="signout-link">
                    <a style="font-size:13px;" href="#logout_myacc_modal" class="singout modal-trigger">
                      SIGN OUT
                    </a>
                  </li>
                </ul>
              </li>
          </ul>
        <?php }else{ ?>

          <!-- Profile Dropdown -->
          <ul id="profile-dropdown" class="dropdown-content user-profile-down">
              <li>
                <div class="user-pofile-image">
                 <!--  <img src="<?php //echo base_url(); ?>asset/images/user.jpg" class="user-img"> -->
                  <div class="user-image-layout"></div>
                  <div class="user-short-details">
                    <span class="name"><?php echo ucfirst($this->session->userdata['user_session']['ei_username']); ?></span>
                    <span class="mail"><?php echo $this->session->userdata['user_session']['email']; ?></span>
                  </div>
                </div>
              </li>
              <li class="user-profile-fix">
                <ul>

                  <?php if($this->session->userdata['user_session']['reg_admin_type'] == 3 || $this->session->userdata['user_session']['reg_admin_type'] == -1)
                  { ?>
                  <li>
                    <a href="<?= base_url(); ?>community/manage-profile?token=<?php echo $bus_id; ?>">
                      My Personal Profile
                    </a>
                  </li>
                  <?php }?>

                  <?php if($this->session->userdata['user_session']['reg_admin_type'] == 3 || $this->session->userdata['user_session']['reg_admin_type'] == 1 || $this->session->userdata['user_session']['reg_admin_type'] == -1)
                  { ?>

                  <li>
                      <a href="#">
                        My Company Profile
                          </a>
                        <!-- <ul class="userprofile-submenu">
                        <li id="CheckForExistingBus">
                            <a data-target="#">
                            <i class="menu-round-icon"></i>
                            <span>Add new business</span>
                          </a>
                      </li>
                      </ul> -->
                  </li>

                  <?php }?>

                  <?php if($this->session->userdata['user_session']['reg_admin_type'] == 3 || $this->session->userdata['user_session']['reg_admin_type'] == 1 || $this->session->userdata['user_session']['reg_admin_type'] == -1)
                  { ?>

                  <li>
                    <a href="#">
                      Document Locker
                    </a>
                  </li>
                  <!-- <li>
                    <a href="#">
                      My Products / Services
                    </a>
                  </li> -->
                  <li>
                    <a href="#">
                      My Account
                    </a>
                  </li>

                  <?php }?>

                  <li>
                    <a href="#feedback_modal" class="modal-trigger">
                      My Feedback
                    </a>
                  </li>


                  <li class="divider"></li>
                  <li class="signout-link">
                    <a href="#logout_myacc_modal" class="singout modal-trigger">
                      Sign out
                    </a>
                  </li>
                </ul>
              </li>
          </ul>

        <?php } ?>	
					

        <?php $bus_id = $this->user_session['bus_id']; ?>

		  	<!-- Community Dropdown -->
			
          	<ul id="infoo-dropdown" class="dropdown-content user-profile-down" style="margin: 5px 0 0 5%; border:1px solid rgb(204, 204, 204) !important; transform: translate3d(5px, 30px, 0px) !important;">
              	<li class="user-profile-fix">
	                <ul>
	                	<li>
	                    	<a class="main_menu_options" target="_blank" href="https://xebra.in/product"><img style="margin: 0 10px -9px 3px;" class="news-icon head-img" width="25" height="25" src="<?php echo base_url();?>public/images/icon/write.png" alt="conn"></img>INSIGHTS</a>
	                  	</li>
	                  	<li>
	                    	<a class="main_menu_options" href="<?= base_url(); ?>resource_center/accounting-terms"><img style="margin: 0 10px -9px 0px;" class="news-icon head-img" width="25" height="25" src="<?php echo base_url();?>public/images/icon/account.png" alt="conn"></img>ACCOUNTING TERMS
	                    	</a>
	                  	</li>
					  	<li>
	                    	<a class="main_menu_options" href="<?= base_url(); ?>resource_center/financial_ratios"><img style="margin: 0 13px -5px -1px;" class="news-icon head-img" width="23" height="23" src="<?php echo base_url();?>public/images/icon/graph.png" alt="conn"></img>FINANCIAL RATIOS</a>
	                  	</li>
						<li>
	                    	<a class="main_menu_options" href="<?= base_url(); ?>community/videos"><img style="margin: 0 13px -6px -1px;" class="news-icon head-img" width="22" height="22" src="<?php echo base_url();?>public/images/icon/video.png" alt="conn"></img>VIDEOS</a>
	                  	</li>
					</ul>
			  	</li>
		  	</ul>
						
			<ul id="resource-dropdown" class="dropdown-content user-profile-down" style="margin: 5px 0 0 3.5%; border:1px solid rgb(204, 204, 204) !important; transform: translate3d(5px, 30px, 0px) !important;">
              	<li class="user-profile-fix">
	                <ul>
	                  	<li>
	                    	<a class="main_menu_options" href="<?= base_url(); ?>community/coworking_list"><img style="margin: 0 14px -6px -1px;" class="news-icon head-img" width="22" height="22" src="<?php echo base_url();?>public/images/icon/co-working.png" alt="conn"></img>CO-WORKING SPACES
	                    	</a>
	                  	</li>

					  	<li>
	                    	<a class="main_menu_options" href="<?= base_url(); ?>community/incubator_list"><img style="margin: 0 10px -9px -1px;" class="news-icon head-img" width="25" height="25" src="<?php echo base_url();?>public/images/icon/incubation.png" alt="conn"></img>INCUBATORS</a>
	                  	</li>
						<!--<li>
	                    	<a class="main_menu_options" href="<?//= base_url(); ?>community/faqs">FAQS & KNOWLEDGE BASE</a>
	                  	</li>-->
						<li>
	                    	<a class="main_menu_options" href="<?= base_url(); ?>community/info_list"><img style="margin: 0 10px -5px -1px;" class="news-icon head-img" width="22" height="22" src="<?php echo base_url();?>public/images/icon/finance.png" alt="conn"></img>CA INFOLISTS</a>
	                  	</li>
	                  	<li>
	                    	<a class="main_menu_options" href="<?= base_url(); ?>resource_center/govt_scheme"><img style="margin: 0 10px -5px -1px;" class="news-icon head-img" width="22" height="22" src="<?php echo base_url();?>public/images/icon/news.png" alt="conn"></img>GOVT SCHEMES</a>
	                  	</li>
					</ul>
			  	</li>
		  	</ul>
			
			<ul id="communityy-dropdown" class="dropdown-content user-profile-down" style="margin: 5px 0 0 2%; border:1px solid rgb(204, 204, 204) !important; transform: translate3d(5px, 30px, 0px) !important;">
              	<li class="user-profile-fix">
	                <ul>
	                	<li>
	                    	<a class="main_menu_options" href="<?= base_url(); ?>community/my-connections"><img style="margin: 0 10px -5px -4px;" class="news-icon head-img" width="22" height="21" src="<?php echo base_url();?>public/images/icon/connections.png" alt="conn" ></img>CONNECTIONS</a>
	                  	</li>
	                  	<li>
	                    	<a class="main_menu_options" href="<?= base_url(); ?>community/events"><img style="margin: 0px 11px -5px -5px;" class="news-icon head-img" width="22" height="22" src="<?php echo base_url();?>public/images/icon/event.png" alt="conn"></img>EVENTS</a>
	                  	</li>
					  	<li>
	                    	<a class="main_menu_options" href="<?= base_url(); ?>community/interviews"><img style="margin: 0 11px -5px -5px;" class="news-icon head-img" width="22" height="22" src="<?php echo base_url();?>public/images/icon/jinterview.png" alt="conn"></img>INTERVIEWS</a>
	                  	</li>
						<li>
	                    	<a class="main_menu_options" href="<?= base_url(); ?>news"><img style="margin: 0 10px -8px -6px;" class="news-icon head-img" width="25" height="27" src="<?php echo base_url();?>public/images/icon/news.png" alt="conn" ></img>NEWS</a>
	                  	</li>
					</ul>
			  	</li>
		  	</ul>
			
			<ul id="growth-dropdown" class="dropdown-content user-profile-down" style="margin: 5px 0 0 1%; border:1px solid rgb(204, 204, 204) !important; transform: translate3d(5px, 30px, 0px) !important;">
              	<li class="user-profile-fix">
	                <ul>
	                	<li>
	                    	<a class="main_menu_options" href="<?= base_url(); ?>community/my-marketplace"><img style=" margin: 0px 10px -6px -5px;" class="head-img news-icon" width="22" height="22" src="<?php echo base_url();?>public/images/icon/marketplace.png" alt="conn"></img>MARKETPLACE</a>
	                  	</li>
	                  	<li>
	                    	<a class="main_menu_options" href="<?= base_url(); ?>community/challenge"><img style="margin: 0 8px -9px -6px;" class="head-img news-icon" width="25" height="25" src="<?php echo base_url();?>public/images/icon/focus.png" alt="conn">CHALLENGES</a>
	                  	</li>
					  	<li>
	                    	<a class="main_menu_options" href="<?= base_url(); ?>community/deals"><img style="margin: 0 9px -10px -7px;" class="head-img news-icon focus-icon" width="25" height="25" src="<?php echo base_url();?>public/images/icon/deal2.png" alt="conn">DEALS</a>
	                  	</li>
						<li>
							<a style="font-size:13px;" class="main_menu_options modal-trigger" href="#buildcommunity-modal">
	                    		<img style="margin: 0px 11px -4px -5px;" class="head-img news-icon focus-icon" width="22" height="22" src="<?php echo base_url();?>public/images/icon/connections.png" alt="conn">BUILD COMMUNITY
							</a>
	                  	</li>
						<li>
							<a style="font-size:13px;" class="main_menu_options modal-trigger" href="#digitalmaket-modal">
	                    		<img style="margin: -4px 9px -7px -7px;" class="head-img news-icon focus-icon" width="25" height="25" src="<?php echo base_url();?>public/images/icon/wcfavi.png" alt="conn">DIGITAL MARKETING
							</a>
	                  	</li>
					</ul>
			  	</li>
		  	</ul>
			
			<ul id="blog-dropdown" class="dropdown-content user-profile-down" style="margin: -3.1% 0 0 5%;">
              	<li class="user-profile-fix">
	                <ul>
	                	<li>
	                    	<a class="main_menu_options" href="<?= base_url(); ?>community/cohorts-blog">ARTICLES</a>
	                  	</li>
	                  	<li>
	                    	<a class="main_menu_options" href="<?= base_url(); ?>community/cohorts-blog">MEDIA
	                    	</a>
	                  	</li>

					  	<li>
	                    	<a class="main_menu_options" href="<?= base_url(); ?>community/cohorts-faqs">FAQs</a>
	                  	</li>
					</ul>
			  	</li>
		  	</ul>

		  	<ul id="learning-dropdown" class="dropdown-content user-profile-down">
              <li class="user-profile-fix">
                <ul>
                  <li>
                    <a class="main_menu_options" href="<?= base_url(); ?>resource_center/accounting-terms">
                      Accounting Terms
                    </a>
                  </li>
				  <li>
                    <a class="main_menu_options" href="<?= base_url(); ?>resource_center/financial_ratios">
                      Financial Ratios
                    </a>
                  </li>
				</ul>
			  </li>
		  	</ul>

          <?php if(@$this->user_session["reg_trial_expire"] != 1 ){ ?>
          <!-- Settings Dropdown -->
          <ul id="setting-dropdown" class="dropdown-content user-profile-down">

              <li class="user-profile-fix">
                <ul>
									<!--li>
                    <a class="main_menu_options" href="<?php //echo base_url(); ?>business_analytics/import_data">
                      <i style="font-size:16px; margin-right:10px;" class="fas fa-file-import"></i>IMPORT DATA
                    </a>
                  </li-->
									<li>
                    <a class="main_menu_options" href="<?php echo base_url(); ?>customise-emails">
                      <i style="font-size:16px; margin-right:10px;" class="fas fa-envelope"></i>CUSTOMIZE EMAILS
                    </a>
                  </li>

                  <li>
                    <a class="main_menu_options open" id="set-alert" href="#">
					  <!--i style="font-size:16px; margin-right:8px;" class="fas fa-alarm-clock"></i--><i style="font-size:16px; margin-right:10px;" class="fas fa-bell"></i>SET ALERT FOR
                    </a>
                    <ul class="setalert-submenu">
                      <?php if($this->session->userdata['user_session']['reg_admin_type'] !=4)
					 { ?>
					  <li>
                        <a href="<?= base_url(); ?>settings">
                          <i class="menu-round-icon"></i>
                          <span class="sub_menu_opt">CLIENT / VENDOR</span>
                        </a>
                      </li>
                       <li>
                        <a href="<?= base_url(); ?>settings/item-alerts">
                          <i class="menu-round-icon"></i>
                          <span class="sub_menu_opt">ITEM / EXPENSE</span>
                        </a>
                      </li>
                      <!--li>
                        <a href="<?//= base_url(); ?>settings/vendor_alert_list">
                          <i class="menu-round-icon"></i>
                          <span class="sub_menu_opt">For Vendor</span>
                        </a>
                      </li>
                      <li>
                        <a href="<?//= base_url(); ?>settings/exp_alert">
                          <i class="menu-round-icon"></i>
                          <span class="sub_menu_opt">For Expense</span>
                        </a>
                      </li-->
                      <li>
                        <a href="<?= base_url(); ?>settings/credit-period-alerts">
                          <i class="menu-round-icon"></i>
                          <span class="sub_menu_opt">CREDIT PERIOD</span>
                        </a>
                      </li>
					 <?php } ?>
                      <li>
                        <a href="<?= base_url(); ?>settings/birthday-anniversary-alerts">
                          <i class="menu-round-icon"></i>
                          <span class="sub_menu_opt">BIRTHDAY & ANNIVERSARY</span>
                        </a>
                      </li>
					  <li>
                        <a href="<?= base_url(); ?>settings/activity-alerts">
                          <i class="menu-round-icon"></i>
                          <span class="sub_menu_opt">ACTIVITY / TASK</span>
                        </a>
                      </li>

                    </ul>
                  </li>

                </ul>
              </li>
          </ul>
          <?php }else{} ?>

          </div>

        </nav>

      </div>

      <!-- ----- Start Logout For New Company ----------- -->

      <div id="logout_for_new_bus_modal" class="modal modal-md modal-display" aria-hidden="true">
         <style>
			#logout_for_new_bus_modal .modal-header h4 {
				width: 90%;
			}
		 </style>
		 <img class="geen" src="<?php echo base_url(); ?>asset/images/green.png" alt="green-cir">
         <div class="modal-content">
            <div class="modal-header">
               <h4 class="text-center">Add New Company</h4>
               <a class="modal-close close-pop log_close"><img style="margin-left:-5px;" src="<?php echo base_url(); ?>asset/images/popupdelete.png" alt="delete"></a>
            </div>
         </div>
         <div class="modal-body confirma">
            <p class="ml-20">You will be signed out from your existing session & taken to subscription page for adding another company
			</p><!-- You will be signed out from your existing session and subscribe again for new company -->
         </div>
         <div class="modal-content">
            <div class="modal-footer">
               <div class="row">
                  <!--   <div class="col l4 s12 m12"></div> text-right-->
                  <div class="col l12 s12 m12 cancel-deactiv" style='text-align:right;'>
                     <a class="close modal-close btn-flat theme-primary-btn theme-btn theme-btn-large model-cancel" type="button" data-dismiss="modal" style="margin-top:-5px !important; background-color:white !important; color:black !important;">CANCEL</a>
					 <a style='margin-top:0px; margin-right:5px;' class="btn-flat theme-primary-btn theme-btn theme-btn-large dea modal-trigger modal-close logout_my_account centertext">CONFIRM</a>

                  </div>
               </div>
            </div>
         </div>
      </div>

    </header>

 <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-145159571-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-145159571-1');
</script>

    <!-- Start of  Zendesk Widget script -->
<!--<script id="ze-snippet" src="https://static.zdassets.com/ekr/snippet.js?key=3c42f582-437d-4c63-8bba-8e08f246232d"> </script>-->
<!-- End of  Zendesk Widget script -->

<!--<script type="text/javascript">(function() {var walkme = document.createElement('script'); walkme.type = 'text/javascript'; walkme.async = true; walkme.src = 'https://cdn.walkme.com/users/9d0d877be77b4684904a1bc20f005dbd/test/walkme_9d0d877be77b4684904a1bc20f005dbd_https.js'; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(walkme, s); window._walkmeConfig = {smartLoad:true}; })();</script>-->

	<script type="text/javascript">
		
		$( ".main_menu_options" ).mouseenter(function() {
			$(this).children().css('opacity','1');
		})
		.mouseleave(function() {
			$(this).children().css('opacity','0.5');
		});
		
		function change_status() {

			$.ajax({
				type: "post",
				dataType: "json",
				data: {'csrf_test_name':csrf_hash,'status':1},
				url: '<?php echo base_url();?>notification/change_status',
				cache: false,
				success: function(data) {

					if(data==true) {
						Materialize.toast('Notification cleared successfully.', 2000,'green rounded');
						$('#notifications-dropdown').html('');
						$('.notiCount').html(0);
					} else {
						//
					}
				}
			});

		}


		$(document).ready(function() {

			$("#xebra_redirect").click(function(){
			    var cl_email = $(this).data('ne');
			    var cl_name = $(this).data('name');
			    var cl_mobile = $(this).data('mobile');
			    var cl_portal = $(this).data('portal');
					$.ajax({
				url:base_url+'index/verify_registration',
				type:"POST",
				data:{'csrf_test_name':csrf_hash,},
				cache: false,
			    success:function(result){
			    	
					if(result=="true"){
						
						window.open("/sales/billing-documents");
					}else{
			            //to set null in value
			            localStorage.setItem("ne","");
			            localStorage.setItem("name","");
			            localStorage.setItem("mobile","");
		                localStorage.setItem("portal","");
			            //to set value
			            localStorage.setItem("ne",cl_email);
			            localStorage.setItem("name",cl_name);
			            localStorage.setItem("mobile",cl_mobile);
			            localStorage.setItem("portal",cl_portal);
    			
	            		//location.href=base_url+'new_signup?ne='+cl_email+'&name='+cl_name+'&mobile='+cl_mobile;			
			            window.open("/new_signup");
						//location.href=base_url+'community/my-gift';
					}
					
                     //$("#login_mobile_otp").modal('show');
				}

			});


			});

			if(navigator.onLine) {
        //alert("Hurray! You're online!!!");
    } else {
        //alert("Oops! You're offline. Please check your network connection...");
        location.href=base_url+'index/no_internet';
    }

		$("#close_msg").click(function(){
			$(".ex-date").css("display","none");
		});

		////////////////



        ////////////////////

var unloaded = false;
//$(window).on('beforeunload', unload);
//$(window).on('unload', unload);





// Attach the event click for all links in the page
	if($("#branch_gst_list option").hasClass("gstinactive")){
		$("#branch_gst_list .gstinactive").css("color","#000 !important");
	}

  $("a").on("click", function() {

     unloaded = true;
  });
   $("select").on("change", function() {

     unloaded = true;
  });
    $("input").on("click", function() {

     unloaded = true;
  });
    $("button").on("click", function() {

     unloaded = true;
  });
$('.select-dropdown').on("change",function(){

    unloaded = true;
});

   $("p").on("click", function() {
     unloaded = true;
  });
    $("span").on("click", function() {

     unloaded = true;
  });
    $("body").on("click", function() {

     unloaded = true;
  });
     $("div").on("click", function() {

     unloaded = true;
  });
      $("img").on("click", function() {

     unloaded = true;
  });
$("ul").on("click", function() {

     unloaded = true;
  });
$("li").on("click", function() {

     unloaded = true;
  });
$("ol").on("click", function() {

     unloaded = true;
  });
$("h1").on("click", function() {

     unloaded = true;
  });
$("h2").on("click", function() {

     unloaded = true;
  });
$("h3").on("click", function() {

     unloaded = true;
  });
$("h4").on("click", function() {

     unloaded = true;
  });
$("h5").on("click", function() {

     unloaded = true;
  });
$("h6").on("click", function() {

     unloaded = true;
  });
$("hr").on("click", function() {

     unloaded = true;
  });



  // Attach the event submit for all forms in the page
  $("form").on("submit", function() {
     unloaded = true;
  });

  $("input[type=submit]").on("click", function() {
unloaded = true;
});
$(window).on("keydown",function(){
  unloaded = true;
});

window.onhashchange = function() {
 //blah blah blah
 //alert('refresh');
 unloaded = true;
}


window.onbeforeunload = function(e) {

    if(window.screenLeft >screen.width){
            unloaded = false;

        }

  else{
  unloaded = true;

}
unload();
}

window.unload = function(e) {


        unload();
}

function unload(){
    if(!unloaded){
        $('body').css('cursor','wait');
        $.ajax({
            type: 'get',
            async: false,
            url: base_url+'index/logout/',
            success:function(){
                unloaded = true;
                $('body').css('cursor','default');
            },
            timeout: 50000
        });
    }
}
				//Hide and Show for 'MY ACCOUNT'
				$('#ac-menu').click(function(){
					if($(this).hasClass('open')){
						$('.my_ac').css('visibility','visible').css('opacity','1').css('height','auto');
						$('#ac-menu').removeClass("open");
						$('#ac-menu').addClass("close");
						//$('#profile-dropdown').css('');
						return false;
					}else if($(this).hasClass('close')){
						$('.my_ac').css('visibility','hidden').css('opacity','0').css('height',0);
						$('#ac-menu').removeClass("close");
						$('#ac-menu').addClass("open");
						return false;
					}
				});

				//Hide and Show for 'SET ALERTS'
				$('#set-alert').click(function(){
					if($(this).hasClass('open')){
						$('.setalert-submenu').css('visibility','visible').css('opacity','1').css('height','auto');
						$('#set-alert').removeClass("open");
						$('#set-alert').addClass("active");
						$('#set-alert').addClass("close");
						//$('#profile-dropdown').css('');
						return false;
					}else if($(this).hasClass('close')){
						$('.setalert-submenu').css('visibility','hidden').css('opacity','0').css('height',0);
						$('#set-alert').removeClass("close");
						$('#set-alert').removeClass("active");
						$('#set-alert').addClass("open");
						return false;
					}
				});
				
				$(".learnlink").hover(
					function() {
						$('.learn-2').show();
						$('.learn-1').hide();
						$('.learn-2').css('opacity','0.5');
				}, function() {
						$('.learn-1').show();
						$('.learn-2').hide();
				});
				
				$(".resourcelink").hover(
					function() {
						$('.res-2').show();
						$('.res-1').hide();
						$('.res-2').css('opacity','0.5');
				}, function() {
						$('.res-1').show();
						$('.res-2').hide();
				});
				
				$(".communitylink").hover(
					function() {
						$('.commu-2').show();
						$('.commu-1').hide();
						$('.commu-2').css('opacity','0.5');
				}, function() {
						$('.commu-1').show();
						$('.commu-2').hide();
				});

			});
	</script>

    <!-- END HEADER -->
