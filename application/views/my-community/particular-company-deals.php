<?php $this->load->view('my-community/Cohorts-header');?>
<style type="text/css">
  .green-bg-right:before {
      top: -39px !important;
      width: 270px;
   }
   .green-bg-right:after { 
      width: 270px;
   }
   
   .act_details{
	   font-size:11px !important;
	   color:white !important;
   }

   [type="radio"]:not(:checked) + label, [type="radio"]:checked + label {
    position: relative;
    padding-left: 20px;
    cursor: pointer;
    display: inline-block;
    height: 25px;
    line-height: 25px;
    font-size: 1rem;
    -webkit-transition: .28s ease;
    transition: .28s ease;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;   
}

[type="radio"] + label:before, [type="radio"] + label:after {
    content: '';
    position: absolute;
    left: 0;
    top: 0;
    margin: 4px 0 0 0;
    width: 16px;
    height: 16px;
    z-index: 0;
    -webkit-transition: .28s ease;
    transition: .28s ease;
}

@media only screen and (min-width: 993px) {
.row .col.l6 {
    width: 50%;
    margin-left: auto;
    left: auto;
    right: auto;
    margin-bottom: 20px;
}
}
body.easy-tab1 [type="radio"] + label:before, [type="radio"] + label:after {
    content: '';
    position: absolute;
    left: 0;
    top: 0;
    margin: 3px 0 0 -2px;
    width: 20px;
    height: 20px;
    z-index: 0;
    -webkit-transition: .28s ease;
    transition: .28s ease;
}
   
   .act_data{
	   font-size:13px !important;
	   color:white !important;
   }
   
   #my_activity th{
	   color:#808080 !important;
	   font-size:13px;
   }
   
   #my_activity td{
	   font-size:12px;
	   padding: 15px 12px !important;
   }
   
   #my_activity thead{
	   border-bottom:none !important;
   }
   
   #my_activity{
	    border-collapse:separate;
		border-spacing:0px 5px;
   }
   
   .company_name{
	   font-size:15px !important;
	   font-weight:500 !important;
	   /*color:#000000 !important;*/
   }
   
   .logo_co{
	   margin-bottom:-10px !important;
   }
   
   .nature_buzz{
	   font-size:13px !important;
   }
   
   #send-mail-comp{
	   background: white !important;
	   color:#000000;
	   width:120px !important;
	   border:1px solid #7864e9 !important;
	   font-size:14px !important;
   }
   
   #connect-to-comp{
	   width:150px !important;
	   font-size:14px !important;
   }
   
   #connect-to-comp:active {
		background: #7864e9 !important;
   }
   
   .cmp_info{
	   font-size:13px !important;
   }
   
   .cmp_details{
	   font-size:13px !important;
	   color:#000000 !important;
   }
   
	.loc-add{
		margin-left:16px !important;
		width:40px !important;
	}
	
	.view-label{
		font-size:13px !important;
	}
	
	.view-content{
		font-size:13px !important;
		color:#000000 !important;
	}
	
	.select_loc{
		padding-left:10px; 
		padding-right:10px;
		padding-top:12px;
		padding-bottom:12px;		   
		text-align:center; 
		width:15px !important; 
		height:15px !important;
		color:#1a1a1a !important; 
		background-color:#f0f5f5 !important; 
		border-radius:18px; 
		font-size:15px;
	}
	
	#cke_1_contents{
		height:120px !important;
	}
	
	label.fill_details{
		font-size:0.875rem !importantl
	}
	
	.filed_name{
		font-size:14px !important;
		font-weight:500;
	}

	input[type="radio"]+span:before, input[type="radio"]+span:after {
    content: '';
    position: absolute;
    left: -15px !important;
    top: 4px !important;
    margin: 0px 0px 0px 31px;
    width: 16px;
    height: 16px;
    z-index: 0;
    -webkit-transition: .28s ease;
    transition: .28s ease;
}

 .modal-body{
   margin:0px !important;
   /*background-color:#f5f5f0*/
   margin-left:25px !important;
   width:90%;
 }

 .modal-header {
   padding: 30px;
 }

 #mail_sub{
   background-color: #7864e9 !important;
   height:42px !important;
   padding: 0px 2rem !important;
   font-size:13px;
 }

 #mailto_id{
   border:1px solid #e0e0d1 !important;
   height:2rem !important;
   margin:0 0 8px 0 !important;
 }

 #subject{
   border:1px solid #e0e0d1 !important;
   height:2rem !important;
   margin:0 0 0px 0 !important;
 }

 #sr_cc_mailto{
   border:1px solid #e0e0d1 !important;
   height:2rem !important;
   margin:0 0 0px 0 !important;
 }

 #message{
   border:1px solid #e0e0d1 !important;
 }

 #mail_close{
   background-color:white;
   color:#c2c2a3;
   font-size:13px;
 }

 .filed_name{
   font-size:14px;
   color:#595959;
 }

 #cke_1_contents{
   height:120px !important;
 }
</style>

    <!-- START MAIN -->
    <div id="main" style="padding:10px 0 0 0px !important;">

      	<!-- START WRAPPER -->
      	<div class="wrapper">

	        <!-- START LEFT SIDEBAR NAV-->
	        <?php //$this->load->view('template/sidebar.php');?>
	        <!-- END LEFT SIDEBAR NAV-->

	        <!-- START CONTENT -->
	        <section id="content" class="bg-theme-gray">

	            <div class="container">
	              <div class="plain-page-header">
	                <div class="row">
	                  <div class="col l6 s12 m6">
	                    <a class="go-back underline" href="javascript:window.history.go(-1);">Back to My Marketplace</a>
	                  </div>
	                </div>
	              </div>
	            </div>

				<?php if(count($offers) >= 1) {?>
	 				<?php $this->load->view('my-community/offers-on-profile'); ?>
	 			<?php }?>
					
	        </section>
		</div>
    </div>
	
	<script src="<?= base_url(); ?>asset/ckeditor/ckeditor.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {


		$("#send_emailto_form_form").submit(function(e){
				e.preventDefault();
				for ( instance in CKEDITOR.instances ) {
			        CKEDITOR.instances[instance].updateElement();
			    }
			}).validate({

				rules:{
					
					message:{
						required:true,
					},
					
				},

				messages:{
					
					message:{
						required:"Message is required",
					},

				},
				submitHandler:function(form){

						var bus_id = $('#bus_id').val();
						var email = $('#email').val();
						var message = $('#message').val();

						//$('#tocomp_email_proceed').prop('disabled', true);

								$.ajax({
								url:base_url+'community/email_company',
								type:"POST",
								data:{
										'csrf_test_name':csrf_hash,
										"bus_id":bus_id,
										"email":email,
										"message":message,
									 },
								success:function(res){

										if(res == 1)
										{
											$("#message").val('');
											$('#send_mailto_comp_modal').modal('close');
											$('#tocomp_email_proceed').prop('disabled', false);
											Materialize.toast('Email has been sent.', 2000,'green rounded');
										}
										else
										{
											$("#message").val('');
											$('#send_mailto_comp_modal').modal('close');
											$('#tocomp_email_proceed').prop('checked', false);
											Materialize.toast('Error. Email was not sent.', 2000,'red rounded');											
										}
									},
					});
				},
			});

			
		CKEDITOR.replace( 'message', { toolbar : 'Basic' });

		var ce_type=$('#ce_type').val();
		$('#Subject').val('');
		CKupdate();
		$.ajax({
				url:base_url+'customise_emails/get_customise_email',
				type:"POST",
				data:{'csrf_test_name':csrf_hash,"ce_type":ce_type},
				success:function(res){
					var data = JSON.parse(res);
					//alert(data);
					//alert(ce_type);
					if(data != false){
						$('#subject').val(data[0].ce_subject);
						//$('#ce_id').val(data[0].ce_id);
						CKEDITOR.instances['message'].setData(data[0].ce_message);
					}
				},
			}); 
		});

		function CKupdate(){
			for ( instance in CKEDITOR.instances ){
				CKEDITOR.instances[instance].updateElement();
				CKEDITOR.instances[instance].setData('');
		   }
		}
	</script>




<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
 <script>
 $(document).ready(function($) {

   $("#mail_frm").submit(function(e){

       e.preventDefault();
       for ( instance in CKEDITOR.instances ) {
             CKEDITOR.instances[instance].updateElement();
         }
     }).validate({

       rules:{
         email_id:{
           required:true,
           //email:true,
           multiemail:true,
         },
         subject:{
           required:true,
         },
         message:{
           required:true,
         },
         sr_cc_mailto:{
           multiemail:true,
         },

       },

       messages:{
         email_id:{
           required:"Email address is required",
           email:"Invlid Email"
         },
         subject:{
           required:"Subject is required",
         },
         message:{
           required:"Message is required",
         },
       },
       submitHandler:function(form){

           var bus_id = $('#bus_id').val();
           var mailto_id = $('#mailto_id').val();
           var subject = $('#subject').val();
           var message = $('#message').val();
           var cc_mail = $('#sr_cc_mailto').val();
           $('.btn').prop('disabled', true);

               $.ajax({
               url:base_url+'community/email_company',
               type:"POST",
               data:{
					'csrf_test_name':csrf_hash,
                   "bus_id":bus_id,
                   "email_to":mailto_id,
                   "email_subject":subject,
                   "email_message":message,
                   "email_cc":cc_mail,
                  },
               success:function(res){

										if(res == 1)
										{
											$("#message").val('');
											$('.email-modal').modal('close');
											$('#tocomp_email_proceed').prop('disabled', false);
											Materialize.toast('Email has been sent.', 2000,'green rounded');
										}
										else
										{
											$("#message").val('');
											$('.email-modal').modal('close');
											$('#tocomp_email_proceed').prop('checked', false);
											Materialize.toast('Error. Email was not sent.', 2000,'red rounded');											
										}
									},
         });
       },
     });

   jQuery.validator.addMethod(
       "multiemail",
        function(value, element) {
            if (this.optional(element)) // return true on optional element
                return true;
            var emails = value.split(/[;,]+/); // split element by , and ;
            valid = true;
            for (var i in emails) {
                value = emails[i];
                valid = valid &&
                        jQuery.validator.methods.email.call(this, $.trim(value), element);
            }
            return valid;
        },

       jQuery.validator.messages.email
   );

 });

 </script>


<script src="<?= base_url(); ?>asset/ckeditor/ckeditor.js"></script>
<script type="text/javascript">


$(document).ready(function() {


CKEDITOR.replace( 'message', { toolbar : 'Basic' });

var ce_type=$('#ce_type').val();
$('#Subject').val('');
 CKupdate();
 $.ajax({
         url:base_url+'customise_emails/get_customise_email',
         type:"POST",
         data:{'csrf_test_name':csrf_hash,"ce_type":ce_type},
         success:function(res){
                 var data = JSON.parse(res);
                 //alert(data);
                 //alert(ce_type);
                 if(data != false)
                 {
                   $('#subject').val(data[0].ce_subject);
                   //$('#ce_id').val(data[0].ce_id);
                   CKEDITOR.instances['message'].setData(data[0].ce_message);
                 }
            },
   });


});

function CKupdate(){
   for ( instance in CKEDITOR.instances ){
       CKEDITOR.instances[instance].updateElement();
       CKEDITOR.instances[instance].setData('');
  }
}

function con_company(id) {

		var con_client 	= $('#con-client'+id).val();
		var con_vendor 	= $('#con-vendor'+id).val();
		var con_both 	= $('#con-both'+id).val();
		var con_none 	= $('#con-none'+id).val();
		var messages	= $('#message'+id).val();
		var con_to_id	= $('#con_to'+id).val();
		if(csrf_hash===""){
			csrf_hash=csrf_hash;
		}
		$.ajax({

			dataType: 'json',
			type: "POST",
			url:base_url+'Community/add_connection_info',

			data:{'csrf_test_name':csrf_hash,'client':con_client,'vendor':con_vendor,'message':messages, 'bus_id_connected_to':con_to_id},

			success: function(data){
				if(data['csrf_hash']){
					csrf_hash=data['csrf_hash'];
				}
				$('#connect_modal'+id).modal('close');
				
				$('#con-client'+id).val('');
				$('#con-vendor'+id).val('');
				$('#con-both'+id).val('');
				$('#con-none'+id).val('');
				$('#message'+id).val('');
				$('#con_to'+id).val('');

				Materialize.toast('Your connection request has been sent', 2000, 'green rounded');
			}
		});
	}

</script>

<?php $this->load->view('template/footer.php');?>