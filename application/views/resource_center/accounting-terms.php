<?php $this->load->view('my-community/Cohorts-header');?>

<!-- END HEADER -->
<!-- //////////////////////////////////////////////////////////////////////////// -->
<style type="text/css">
   .add-new {
   margin: 0 0 0 0 !important; 
   }
   .section_li {
   margin: 0 0 0 0 !important; 
   }
   .categories>li {
   float: none;
   display: inline-block;
   }
   a.btn.btn-theme.btn-large.section_li {
   background-color: #f8f9fd;
   color: #7864e9;
   border: 1px solid #7864e9;
   font-size: 12px;
   }
   .btn-theme{
   padding: 0 10px !important;
   border-radius: 6px !important;
   line-height: 40px !important;
   height: 40px !important;
   } 
   ul.categories {
   text-align: center;
   margin:0;
   width: 100%;
   }
   /* li {
   padding: 0 5px 0 5px;
   }*/
   .tabs {
   height:auto;
   background: transparent !important;
   width: 100% !important;
   }
   ul.tabs li {
   border:none !important;
   float: left;
   width: 121px !important;
   }
   li.tab a.active {
   min-height: auto !important;
   }
   a.btn.btn-theme.btn-large.section_li.active {
   background-color: #7864e9 !important;
   color: #f8f9fd;
   border: 0px;
   }
   .tabs .tab{
   padding: 0 2px 0  !important;
   }
   .collapsible {
   border: 0px;
   -webkit-box-shadow: none;
   }
   .document-box-view.redborder.active {
   border-left: 4px solid #ff7c9b;
   }
   .collapsible-header{
   border-bottom:none;
   font-weight:500;
   }
   .collapsible-header.active{
   padding-bottom: 0px;
   font-weight: 600;
   }
   .collapsible-body{
   padding: 0 1rem 1rem 1rem;
   }
   /*.collapsible-body span {
   color: #464646;
   }*/
   .red-bg-right:before {
   top: -32px !important;
   }
   .green-bg-right:after{
   bottom: -25px !important;
   left: 50px;
   }
   .collapsible-body p {
		margin: 0;
	}
</style>
<!-- START MAIN -->
<div id="main" style="padding-left:0px !important;">
   <!-- START WRAPPER -->
   <div class="wrapper">
      <!-- START LEFT SIDEBAR NAV-->
      <?php //$this->load->view('template/sidebar'); ?>
      <!-- END LEFT SIDEBAR NAV-->
      <!-- //////////////////////////////////////////////////////////////////////////// -->
      <!-- START CONTENT -->
      <section id="content" class="bg-cp">
         <div id="breadcrumbs-wrapper">
            <div class="container custom">
               <div class="row">
                  <div class="col s10 m6 l6">
                     <h5 class="breadcrumbs-title my-ex">Accounting Terms</h5>
                     <ol class="breadcrumbs">
                        <li><a href="">LEARNING / ACCOUNTING TERMS</a>
                     </ol>
                  </div>
               </div>
            </div>
         </div>
         <div id="bulk-action-wrapper">
            <div class="container custom">
               <div class="row">
                  <div class="col l12 s12 m12">
                     <!-- <ul class="categories db tabs">
                        <li> <a  href="#test" class="btn btn-theme btn-large section_li">CATEGORY A</a></li>
                        <li> <a  href="#" class="btn btn-theme btn-large section_li">CATEGORY B</a></li>
                        <li> <a  href="#" class="btn btn-theme btn-large section_li">CATEGORY C</a></li>
                        <li> <a  href="#" class="btn btn-theme btn-large section_li">CATEGORY D</a></li>
                        <li> <a  href="#" class="btn btn-theme btn-large section_li">CATEGORY E</a></li>
                        <li> <a  href="#" class="btn btn-theme btn-large section_li">CATEGORY F</a></li>
                        
                        
                        </ul> -->
                     <ul class="tabs tabs-fixed-width tab-demo categories">
                        <li class="tab"><a href="#test1"  class="btn btn-theme btn-large section_li">A</a></li>
                        <li class="tab"><a href="#test2"  class="btn btn-theme btn-large section_li">B</a></li>
                        <li class="tab"><a href="#test3"  class="btn btn-theme btn-large section_li">C</a></li>
                        <li class="tab"><a href="#test4"  class="btn btn-theme btn-large section_li">D</a></li>
                        <li class="tab"><a href="#test5"  class="btn btn-theme btn-large section_li">E</a></li>
                        <!-- <li class="tab"><a href="#test6"  class="btn btn-theme btn-large section_li">F</a></li> -->
                        <li class="tab"><a href="#test7"  class="btn btn-theme btn-large section_li">G</a></li>
                        <!-- <li class="tab"><a href="#test8"  class="btn btn-theme btn-large section_li">H</a></li> -->
                        <li class="tab"><a href="#test9"  class="btn btn-theme btn-large section_li">I</a></li>
                        <!-- <li class="tab"><a href="#test10"  class="btn btn-theme btn-large section_li">J</a></li> -->
                        <!-- <li class="tab"><a href="#test11"  class="btn btn-theme btn-large section_li">K</a></li> -->
                        <li class="tab"><a href="#test12"  class="btn btn-theme btn-large section_li">L</a></li>
                        <!-- <li class="tab"><a href="#test13"  class="btn btn-theme btn-large section_li">M</a></li> -->
                        <li class="tab"><a href="#test14"  class="btn btn-theme btn-large section_li">N</a></li>
                        <!-- <li class="tab"><a href="#test15"  class="btn btn-theme btn-large section_li">O</a></li> -->
                        <li class="tab"><a href="#test16"  class="btn btn-theme btn-large section_li">P</a></li>
                        <li class="tab"><a href="#test17"  class="btn btn-theme btn-large section_li">Q</a></li>
                        <li class="tab"><a href="#test18"  class="btn btn-theme btn-large section_li">R</a></li>
                        <li class="tab"><a href="#test19"  class="btn btn-theme btn-large section_li">S</a></li>
                        <li class="tab"><a href="#test20"  class="btn btn-theme btn-large section_li">T</a></li>
                        <!-- <li class="tab"><a href="#test21"  class="btn btn-theme btn-large section_li">U</a></li> -->
                        <!-- <li class="tab"><a href="#test22"  class="btn btn-theme btn-large section_li">V</a></li> -->
                        <!-- <li class="tab"><a href="#test23"  class="btn btn-theme btn-large section_li">W</a></li> -->
                        <!-- <li class="tab"><a href="#test24"  class="btn btn-theme btn-large section_li">X</a></li> -->
                        <!-- <li class="tab"><a href="#test25"  class="btn btn-theme btn-large section_li">Y</a></li> -->
                        <!-- <li class="tab"><a href="#test26"  class="btn btn-theme btn-large section_li">Z</a></li> -->
                     </ul>
                     <!-- <a  href="#" class="btn btn-theme btn-large  add-new modal-trigger">ADD INVOICES</a>  -->
                  </div>
               </div>
            </div>
         </div>
         <div class="container custom">
            <div class="row">
               <div class="col l12 s12 m12 mt-5 before-red-bg red-bg-right after-green-bg green-bg-right">
                  <div id="test1" class="col s12 inner_row_container ">
                     <!--  <div class="row document-box-view blueborder"> -->
                     <ul class="collapsible">
                        <li class="row document-box-view redborder">
                           <div class="collapsible-header">Accrual</div>
                           <div class="collapsible-body">
                              <span>
                                 <p>An accrual allows a business to record expenses or revenues now for a later time when it will spend money or receive cash</p>
                              </span>
                           </div>
                        </li>
                        <li class="row document-box-view redborder">
                           <div class="collapsible-header">Accounting Year</div>
                           <div class="collapsible-body"><span>An accounting year is a period of time, usually 12, during which businesses calculate their accounts and organize their financial activities.</span></div>
                        </li>
                        <li class="row document-box-view redborder">
                           <div class="collapsible-header">Accounts payable</div>
                           <div class="collapsible-body"><span>Accounts payable is an account that contains the money that needs to be paid to a company or supplier, but it may also refer to the department responsible for managing this account</span></div>
                        </li>
                        <li class="row document-box-view redborder">
                           <div class="collapsible-header">Accounts Receivable</div>
                           <div class="collapsible-body"><span>Accounts receivable is a claim for payment that is owed to a business that the customer hasn't yet paid for.</span></div>
                        </li>
                        <li class="row document-box-view redborder">
                           <div class="collapsible-header">Asset</div>
                           <div class="collapsible-body"><span>An asset is any resource that a business or individual owns and which can be converted into cash</span></div>
                        </li>
                        <li class="row document-box-view redborder">
                           <div class="collapsible-header">Amortization</div>
                           <div class="collapsible-body"><span>Amortization is the process of paying off a debt, such as a car loan or your mortgage, with a fixed repayment schedule with regular payments for a specified time period.</span></div>
                        </li>
                     </ul>
                     <!-- </div> -->
                  </div>
                  <div id="test2" class="col s12  inner_row_container">
                     <ul class="collapsible">
                        <li class="row document-box-view redborder">
                           <div class="collapsible-header">Bad Debt</div>
                           <div class="collapsible-body">
                              <span>
                                 <p>Bad debt expense represents the amount of uncollectible accounts receivable that occurs in a given period</p>
                              </span>
                           </div>
                        </li>
                        <li class="row document-box-view redborder">
                           <div class="collapsible-header">Budget</div>
                           <div class="collapsible-body"><span>A budget is a set of plans focused on present and future revenues and expenses. The goal of a budget is to plan spending and often to save money.</span></div>
                        </li>
                        <li class="row document-box-view redborder">
                           <div class="collapsible-header">Balance Sheet</div>
                           <div class="collapsible-body"><span>A balance sheet allows businesses to see their assets, liabilities and owner's or stockholders' equity for a specific point in time.</span></div>
                        </li>
                        <li class="row document-box-view redborder">
                           <div class="collapsible-header">Break-even point</div>
                           <div class="collapsible-body"><span>The Break-Even Point (BEP) is the price point at which the sales revenue is equal to the costs, generating zero profit.</span></div>
                        </li>
                     </ul>
                  </div>
                  <div id="test3" class="col s12 inner_row_container">
                     <ul class="collapsible">
                        <li class="row document-box-view redborder">
                           <div class="collapsible-header">Cash flow statement</div>
							<div class="collapsible-body">
								<span>
								 <p>A cash flow statement is a report of how much cash is flowing into and out of your business for a specified time period. In order to remain in business, you must have a positive level of cash flow.</p>
								</span>
							</div>
                        </li>
						<li class="row document-box-view redborder">
                           <div class="collapsible-header">Credit Note</div>
                           <div class="collapsible-body"><span>A credit note is a letter sent by the supplier to the customer notifying the customer that he or she has been credited a certain amount due to an error in the original invoice or other reasons.</span></div>
                        </li>
						<li class="row document-box-view redborder">
                           <div class="collapsible-header">Creditor</div>
                           <div class="collapsible-body"><span>A creditor is a person, organization, company or government to whom money is owed.</span></div>
                        </li>
						
                     </ul>
                  </div>
                  <div id="test4" class="col s12 inner_row_container">
                     <ul class="collapsible">
                        <li class="row document-box-view redborder">
                           <div class="collapsible-header">Debtor</div>
                           <div class="collapsible-body">
								<span>
								 <p>A debtor is an person, company or organization who owes money.</p>
								</span>
						   </div>
                        </li>
						<li class="row document-box-view redborder">
                           <div class="collapsible-header">Delivery Note</div>
                           <div class="collapsible-body"><span>A delivery note is a document sent with a shipment of goods that describes the goods and the quantities being delivered.</span></div>
                        </li>
						<li class="row document-box-view redborder">
                           <div class="collapsible-header">Depreciation</div>
                           <div class="collapsible-body"><span>In the accounting method known as depreciation, the cost of a tangible asset is written off over its useful life.</span></div>
                        </li>
						<li class="row document-box-view redborder">
                           <div class="collapsible-header">Dividend</div>
                           <div class="collapsible-body"><span>A dividend is a payment that a company makes to its shareholders, where the amount is determined by the board of directors.</span></div>
                        </li>
						<li class="row document-box-view redborder">
                           <div class="collapsible-header">Debt</div>
                           <div class="collapsible-body"><span>Debt describes a certain amount of money that one party owes to another, and it is used often by companies and individuals to make large purchases they cannot yet afford.</span></div>
                        </li>
						
                     </ul>
                  </div>
                  <div id="test5" class="col s12 inner_row_container">
                     <ul class="collapsible">
                        <li class="row document-box-view redborder">
                           <div class="collapsible-header">Expense</div>
                           <div class="collapsible-body">
							<span>
								 <p>An expense is any money that is spent or costs that are incurred as a business, individual or organization attempts to generate revenue.</p>
							</span>
						   </div>
                        </li>
						<li class="row document-box-view redborder">
                           <div class="collapsible-header">Estimate</div>
                           <div class="collapsible-body"><span>An estimate is an educated guess, based on certain factors the business is knowledgeable of, in providing a potential customer a price for goods and services.</span></div>
                        </li>
						
						
                     </ul>
                  </div>
				  <!-- <div id="test6" class="col s12 inner_row_container">
                     <ul class="collapsible">
                        <li class="row document-box-view redborder">
                           <div class="collapsible-header">Coming Soon</div>
                           <div class="collapsible-body">
							<span>
								 <p></p>
							</span>
						   </div>
                        </li>						
                     </ul>
                  </div> -->
                  <div id="test7" class="col s12 inner_row_container">
                     <ul class="collapsible">
                        <li class="row document-box-view redborder">
                           <div class="collapsible-header">Gross Profit</div>
                           <div class="collapsible-body">
							<span>
								 <p>Gross profit is the total revenue of a company minus the cost of goods sold.</p>
							</span>
						   </div>
                        </li>						
                     </ul>
                  </div>
				  <!-- <div id="test8" class="col s12 inner_row_container">
                     <ul class="collapsible">
                        <li class="row document-box-view redborder">
                           <div class="collapsible-header">Coming Soon</div>
                           <div class="collapsible-body">
							<span>
								 <p></p>
							</span>
						   </div>
                        </li>						
                     </ul>
                  </div> -->
				  <div id="test9" class="col s12 inner_row_container">
                     <ul class="collapsible">
                        <li class="row document-box-view redborder">
                           <div class="collapsible-header">Invoice</div>
                           <div class="collapsible-body">
							<span>
								 <p>An invoice is a payment request sent by the supplier that lists the goods or services provided to the buyer.</p>
							</span>
						   </div>
                        </li>						
                     </ul>
                  </div>
				  <!-- <div id="test10" class="col s12 inner_row_container">
                     <ul class="collapsible">
                        <li class="row document-box-view redborder">
                           <div class="collapsible-header">Coming Soon</div>
                           <div class="collapsible-body">
							<span>
								 <p></p>
							</span>
						   </div>
                        </li>						
                     </ul>
                  </div>
				  <div id="test11" class="col s12 inner_row_container">
                     <ul class="collapsible">
                        <li class="row document-box-view redborder">
                           <div class="collapsible-header">Coming Soon</div>
                           <div class="collapsible-body">
							<span>
								 <p></p>
							</span>
						   </div>
                        </li>						
                     </ul>
                  </div> -->
				  <div id="test12" class="col s12 inner_row_container">
                     <ul class="collapsible">
                        <li class="row document-box-view redborder">
                           <div class="collapsible-header">Liability</div>
                           <div class="collapsible-body">
							<span>
								 <p>A liability is the financial debt or financial obligations that come about because of normal business operations.</p>
							</span>
						   </div>
                        </li>		
						<li class="row document-box-view redborder">
                           <div class="collapsible-header">Limited Company</div>
                           <div class="collapsible-body"><span>A limited company is a type of incorporation that limits the amount of liability that a company's shareholders can be held accountable for.</span></div>
                        </li>
						<li class="row document-box-view redborder">
                           <div class="collapsible-header">Loss</div>
                           <div class="collapsible-body"><span>In accounting, a loss is an unrecoverable and unanticipated decrease in a resource or asset outside of normal business operations.</span></div>
                        </li>
						
                     </ul>
                  </div>
				  <!-- <div id="test13" class="col s12 inner_row_container">
                     <ul class="collapsible">
                        <li class="row document-box-view redborder">
                           <div class="collapsible-header">Coming Soon</div>
                           <div class="collapsible-body">
							<span>
								 <p></p>
							</span>
						   </div>
                        </li>						
                     </ul>
                  </div> -->
				  <div id="test14" class="col s12 inner_row_container">
                     <ul class="collapsible">
                        <li class="row document-box-view redborder">
                           <div class="collapsible-header">Net Profit</div>
                           <div class="collapsible-body">
							<span>
								 <p>Net income is the total earnings for a company (or its profit). It is calculated by subtracting the cost of doing business from the revenues.</p>
							</span>
						   </div>
                        </li>						
                     </ul>
                  </div>
				  <!-- <div id="test15" class="col s12 inner_row_container">
                     <ul class="collapsible">
                        <li class="row document-box-view redborder">
                           <div class="collapsible-header">Coming Soon</div>
                           <div class="collapsible-body">
							<span>
								 <p></p>
							</span>
						   </div>
                        </li>						
                     </ul>
                  </div> -->
				  <div id="test16" class="col s12 inner_row_container">
                     <ul class="collapsible">
                        <li class="row document-box-view redborder">
                           <div class="collapsible-header">Proforma Invoice</div>
                           <div class="collapsible-body">
							<span>
								 <p>A proforma invoice is a document that is either sent with goods shipped internationally to determine their value or sent as a commitment by a supplier to deliver goods to the customer.</p>
							</span>
						   </div>
                        </li>
						<li class="row document-box-view redborder">
                           <div class="collapsible-header">Profit & Loss Statement</div>
                           <div class="collapsible-body"><span>A Profit and Loss Statement, one of the four most important financial documents, lists the expenses, costs and revenues that a business incurs for a specified period of time.</span></div>
                        </li>
						<li class="row document-box-view redborder">
                           <div class="collapsible-header">Petty Cash</div>
                           <div class="collapsible-body"><span>Petty cash is a relatively small amount of cash that businesses, organizations or institutions set aside for reimbursements on small purchases made by employees of the business.</span></div>
                        </li>
                     </ul>
                  </div>
				  <div id="test17" class="col s12 inner_row_container">
                     <ul class="collapsible">
                        <li class="row document-box-view redborder">
                           <div class="collapsible-header">Quotation</div>
                           <div class="collapsible-body">
							<span>
								 <p>A quotation, or quote, is a document that a supplier submits to a potential client with a proposed price for the supplier's goods or services based on certain conditions.</p>
							</span>
						   </div>
                        </li>						
                     </ul>
                  </div>
				  <div id="test18" class="col s12 inner_row_container">
                     <ul class="collapsible">
                        <li class="row document-box-view redborder">
                           <div class="collapsible-header">Recurring Invoice</div>
                           <div class="collapsible-body">
							<span>
								 <p>A recurring invoice is a type of invoicing in which a supplier or merchant automatically charges a customer for goods or services at regular intervals.</p>
							</span>
						   </div>
                        </li>
						<li class="row document-box-view redborder">
                           <div class="collapsible-header">Revenue</div>
                           <div class="collapsible-body"><span>Revenue is essentially the amount of money a business receives for a specified period. It is calculated by multiplying the price for the goods or services by the number of units sold.</span></div>
                        </li>
                     </ul>
                  </div>
				  <div id="test19" class="col s12 inner_row_container">
                     <ul class="collapsible">
                        <li class="row document-box-view redborder">
                           <div class="collapsible-header">Sales</div>
                           <div class="collapsible-body">
							<span>
								 <p>Sales in accounting is a term that refers to any operating revenues that a company earns through its business activities, such as selling goods, services, products, etc.</p>
							</span>
						   </div>
                        </li>						
                     </ul>
                  </div>
				  <div id="test20" class="col s12 inner_row_container">
                     <ul class="collapsible">
                        <li class="row document-box-view redborder">
                           <div class="collapsible-header">Tax Invoice</div>
                           <div class="collapsible-body">
							<span>
								 <p>A tax invoice is a legal document that a seller submits to a customer in which the tax is included or a document (in India) from a registered supplier to a registered dealer.</p>
							</span>
						   </div>
                        </li>						
                     </ul>
                  </div>
				  <!-- <div id="test21" class="col s12 inner_row_container">
                     <ul class="collapsible">
                        <li class="row document-box-view redborder">
                           <div class="collapsible-header">Coming Soon</div>
                           <div class="collapsible-body">
							<span>
								 <p></p>
							</span>
						   </div>
                        </li>						
                     </ul>
                  </div>
				  <div id="test22" class="col s12 inner_row_container">
                     <ul class="collapsible">
                        <li class="row document-box-view redborder">
                           <div class="collapsible-header">Coming Soon</div>
                           <div class="collapsible-body">
							<span>
								 <p></p>
							</span>
						   </div>
                        </li>						
                     </ul>
                  </div>
				  <div id="test23" class="col s12 inner_row_container">
                     <ul class="collapsible">
                        <li class="row document-box-view redborder">
                           <div class="collapsible-header">Coming Soon</div>
                           <div class="collapsible-body">
							<span>
								 <p></p>
							</span>
						   </div>
                        </li>						
                     </ul>
                  </div>
				  <div id="test24" class="col s12 inner_row_container">
                     <ul class="collapsible">
                        <li class="row document-box-view redborder">
                           <div class="collapsible-header">Coming Soon</div>
                           <div class="collapsible-body">
							<span>
								 <p></p>
							</span>
						   </div>
                        </li>						
                     </ul>
                  </div>
				  <div id="test25" class="col s12 inner_row_container">
                     <ul class="collapsible">
                        <li class="row document-box-view redborder">
                           <div class="collapsible-header">Coming Soon</div>
                           <div class="collapsible-body">
							<span>
								 <p></p>
							</span>
						   </div>
                        </li>						
                     </ul>
                  </div>
				  <div id="test26" class="col s12 inner_row_container">
                     <ul class="collapsible">
                        <li class="row document-box-view redborder">
                           <div class="collapsible-header">Coming Soon</div>
                           <div class="collapsible-body">
							<span>
								 <p></p>
							</span>
						   </div>
                        </li>						
                     </ul>
                  </div> -->
				  
               </div>
            </div>
         </div>
      </section>
      <!-- END CONTENT -->
   </div>
   <!-- END WRAPPER -->
</div>
<!-- END MAIN -->
<!-- ================================================
   Scripts
   ================================================ -->
<?php $this->load->view('template/footer'); ?>