 <?php $this->load->view('my-community/Cohorts-header');?>

    <!-- END HEADER -->
    <!-- //////////////////////////////////////////////////////////////////////////// -->
    <style type="text/css">
    .add-new {
         margin: 0 0 0 0 !important;
    }
     .section_li {
         margin: 0 0 0 0 !important;
    }
    .categories>li {
      float: none;
      display: inline-block;
    }
    a.btn.btn-theme.btn-large.section_li {
        background-color: #f8f9fd;
        color: #7864e9;
        border: 1px solid #7864e9;
    }
    .btn-theme{
            padding: 0 40px !important;
            border-radius: 6px !important;
            line-height: 45px !important;
            height: 45px !important;
      }
      ul.categories {
          text-align: center;
           margin:0;
           width: 100%;
      }
     /* li {
          padding: 0 5px 0 5px;

      }*/
      .tabs {
          height:auto;
          background: transparent !important;
          width: 100% !important;
      }
      ul.tabs li {
        border:none !important;
        float: left;
        width: 121px !important;
    }
    li.tab a.active {
      min-height: auto !important;
      }
  a.btn.btn-theme.btn-large.section_li.active {
    background-color: #7864e9 !important;
    color: #f8f9fd;
    border: 0px;
}
.tabs .tab{
  padding: 0 5px 0 5px !important;
}
.collapsible {
    border: 0px;
    -webkit-box-shadow: none;
  }
  .document-box-view.redborder.active {
    border-left: 4px solid #ff7c9b;

}
.collapsible-header{
  border-bottom:none;

}

.collapsible-header.active{

    padding-bottom: 0px;
        font-weight: 600;
}

.collapsible-body{
         padding: 0 1rem 1rem 1rem;
}
/*.collapsible-body span {
    color: #464646;
}*/
.red-bg-right:before {

    top: -32px !important;
  }
  .green-bg-right:after{
    bottom: -25px !important;
    left: 50px;
  }

  ul:not(.browser-default) > li .li_cls {
    list-style-type: disc !important;
}
	.btn-theme{
		padding:0px !important;
	}
	a.btn.btn-theme.btn-large.section_li{ width:100px; }
	.tabs.tabs-fixed-width { display: contents !important; }
    </style>
    <!-- START MAIN -->
    <div id="main" style="padding-left:0px !important;">
      <!-- START WRAPPER -->
      <div class="wrapper">
        <!-- START LEFT SIDEBAR NAV-->
        <?php //$this->load->view('template/sidebar'); ?>
        <!-- END LEFT SIDEBAR NAV-->
        <!-- //////////////////////////////////////////////////////////////////////////// -->
        <!-- START CONTENT -->
        <section id="content" class="bg-cp">
          <div id="breadcrumbs-wrapper">
            <div class="container custom">
              <div class="row">
                <div class="col s10 m6 l6">
                  <h5 class="breadcrumbs-title my-ex">Frequently Asked Questions</h5>

                  <ol class="breadcrumbs">
                    <li><a href="index.html">MY RESOURCE CENTER / FAQs</a>
                  </ol>
                </div>

              </div>

            </div>
        </div>
        <div id="bulk-action-wrapper">
          <div class="container custom">
              <div class="row">
                  <div class="col l12 s12 m12">
                    <!-- <ul class="categories db tabs">
                      <li> <a  href="#test" class="btn btn-theme btn-large section_li">CATEGORY A</a></li>
                      <li> <a  href="#" class="btn btn-theme btn-large section_li">CATEGORY B</a></li>
                      <li> <a  href="#" class="btn btn-theme btn-large section_li">CATEGORY C</a></li>
                      <li> <a  href="#" class="btn btn-theme btn-large section_li">CATEGORY D</a></li>
                      <li> <a  href="#" class="btn btn-theme btn-large section_li">CATEGORY E</a></li>
                      <li> <a  href="#" class="btn btn-theme btn-large section_li">CATEGORY F</a></li>


                    </ul> -->
                    <ul class="tabs tabs-fixed-width tab-demo categories">
						<li class="tab"><a href="#test3" class="btn btn-theme btn-large section_li"><p style="padding: 0px; line-height: 18px; margin: 5px 0 0 0;">Business </br>Intelligence
						</p></a></li>
						<li class="tab"><a href="#test1" class="btn btn-theme btn-large section_li"><p style="padding: 0px; line-height: 18px; margin: 5px 0 0 0;">Clients, Sales </br>& Receipts</p></a></li>
						<li class="tab"><a  href="#test2"  class="btn btn-theme btn-large section_li"><p style="padding: 0px; line-height: 18px; margin: 5px 0 0 0;">Expenses </br>& Payments</p></a></li>
						<li class="tab"><a  href="#test7"  class="btn btn-theme btn-large section_li"><p style="padding: 0px; line-height: 18px; margin: 5px 0 0 0;">Assets & </br>Depreciation</p></a></li>
						<li class="tab"><a  href="#test8"  class="btn btn-theme btn-large section_li"><p style="padding: 0px; line-height: 18px; margin: 5px 0 0 0;">Employees </br>& Salary Expense</p></a></li>
						<li class="tab"><a  href="#test9"  class="btn btn-theme btn-large section_li"><p style="padding: 0px; line-height: 18px; margin: 5px 0 0 0;">Cash, Bank </br>& Cards</p></a></li>
						<li class="tab"><a href="#test4"  class="btn btn-theme btn-large section_li">Tax Queries</a></li>
						<li class="tab"><a href="#test10"  class="btn btn-theme btn-large section_li">Accountancy</a></li>
						<li class="tab"><a href="#test11"  class="btn btn-theme btn-large section_li">Profile & </br>Subscription</a></li>
						<li class="tab"><a href="#test5"  class="btn btn-theme btn-large section_li"><p style="padding: 0px; line-height: 18px; margin: 5px 0 0 0;">Subscription </br>& Purchase</p></a></li>
						<li class="tab"><a href="#test6"  class="btn btn-theme btn-large section_li"><p style="padding: 0px; line-height: 18px; margin: 5px 0 0 0;">Security </br>& Privacy</p></a></li>
						<li class="tab"><a href="#test12"  class="btn btn-theme btn-large section_li"><p style="padding: 0px; line-height: 18px; margin: 5px 0 0 0;">Mobile </br>Application</p></a></li>
					</ul>

                   <!-- <a  href="#" class="btn btn-theme btn-large  add-new modal-trigger">ADD INVOICES</a>  -->
                 </div>
              </div>
          </div>
        </div>

          <div class="container custom">
              <div class="row">
                  <div class="col l12 s12 m12 mt-5 before-red-bg red-bg-right after-green-bg green-bg-right">

                  <div id="test1" class="col s12 inner_row_container ">
                   <!--  <div class="row document-box-view blueborder"> -->
                    <ul class="collapsible">
                      <li class="row document-box-view redborder">
                        <div class="collapsible-header">Can I record international clients and make an estimate or invoice in international currency?</div>
                        <div class="collapsible-body"><span>
                          <p>Yes, you can record international clients from Client master. While filling up the data of that client, you can select the currency in which you want to invoice them in</p></span>
						</div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">What are Items?</div>
                        <div class="collapsible-body"><span>
                          <p>Items are used when you’re billing for goods or products sold to your Client. These Items can be tangible (like parts) or intangible (like a hosting package). You can create these from Item Master in Sales Module</p></span>
						</div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">I tried to create a new item, but an error says the name is taken</div>
                        <div class="collapsible-body"><span>
                          <p>Item names must be unique. If the name is taken, the Item name already exists elsewhere in your Archived Items section, or in the deactivated section.</p></span>
						</div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">Can I upload more than 1 company logo?</div>
                        <div class="collapsible-body"><span>
                          <p>No, you can upload only one company logo.</p></span>
						</div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">Does Xebra work outside India?</div>
                        <div class="collapsible-body"><span>
                          <p>Yes, you can use it but Xebra has been more customised to Indian scenario and laws</p></span>
						</div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">Does Xebra support other language apart from English?</div>
                        <div class="collapsible-body"><span>
                          <p>Not at the moment</p></span>
						</div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">How much time does it take to get Xebra set up?</div>
                        <div class="collapsible-body"><span>
                          <p>In an instant. Xebra is a SaaS product who you can start</p></span>
						</div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I convert an estimate into an invoice?</div>
                        <div class="collapsible-body"><span>
                          <p>You can select a particular estimate in list view of billing documents and click on ‘Convert
							To Invoice’ option in action buttons</p></span>
						</div>
                      </li>
                     <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I charge Equilisation levy to my client?</div>
                        <div class="collapsible-body"><span><p>There is a check box provided in invoice which you need to click. You will be asked to fill
						the percentage of Equilisation Levy and the amount of which it has to be collected. Once
						you enter in the details, it will appear as line item in your invoice</p></span></div>
                      </li>
                      <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I record my complete or partial bad debts against my invoice?</div>
                        <div class="collapsible-body"><span><p>Select the particular invoice from the List View and click on Bad Debts option in Action Button. You will be guided to Sales Receipt module where you will record either entire or partial amount as bad debts</p></span></div>
                      </li>
                       <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I record part payment and part bad debt on an invoice?</div>
                        <div class="collapsible-body"><span><p>In the Sales Receipt module, select the invoice against which you wish to record the details. In the receipt section select Part Received in Receipt Status and add the relevant amount in Receipt Amt and Bad Debts Amount cell</p></span></div>
                      </li>
                      <li class="row document-box-view redborder">
                        <div class="collapsible-header">I can’t find a section to add late fees?</div>
                        <div class="collapsible-body"><span><p>Click on customise in the invoice. In the content tab, select the option of adding Late fees. You can click on option of Set as Default if you want that cell to appear in all invoices </p></span></div>
                      </li>
                        <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I record amount received from my clients against multiple invoices?</div>
                        <div class="collapsible-body"><span><p>While recording the receipt from your client in Sales Receipt module, simply click on Add Another Invoice button and you can select the 2nd invoice against which you have received the payment. You will be saved the hassle of recording the second payment separately </p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I record forex gain or loss incurred because of international client payment timing?</div>
                        <div class="collapsible-body"><span><p>Select the appropriate currency for your client in the client master. Then enter the forex rate at the time of making the invoice and at the time of receiving the payment. Xebra will automatically the forex gain or loss derived from it </p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I customise or stop my alerts?</div>
                        <div class="collapsible-body"><span><p>Go to Settings and Click on Set Alerts & Notifications. Select the appropriate module and type of notification you wish to change</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">How many types of alerts are there?</div>
                        <div class="collapsible-body"><span><p>You can select from 3 types of notification: Web, Email and/or SMS</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">After providing access to clients in client portal, can we restrict them afterwards?</div>
                        <div class="collapsible-body"><span><p>Yes, you can deny them access by simple toggle button in client master</p></span></div>
                      </li>
                      <li class="row document-box-view redborder">
                        <div class="collapsible-header">Can I make invoices and record sales receipt from the mobile application?</div>
                        <div class="collapsible-body"><span><p>You will be able to record that from your web interface only</p></span></div>
                      </li>

                      <li class="row document-box-view redborder">
                        <div class="collapsible-header">I want to write my company’s address in invoice footer. How do I do that?</div>
                        <div class="collapsible-body"><span><p>Very simple. Click on customise in the bottom white bar of your invoice. Select Footer
option and write down the text that you wish to appear in the footer of your invoice. Don’t
forget to click on ‘Set As Default’ so that you don’t have to do it for every invoice</p></span></div>
                      </li>
                      <li class="row document-box-view redborder">
                        <div class="collapsible-header">What is the level of customisations that are possible with it?</div>
                        <div class="collapsible-body"><span><p>We realise that every organization is their way of conducting business and making billing
documents. So, we have provided a slew of features to adapt Xebra completely as
per your requirement. Few of things that you can do</p>
                      <ul style="margin-left: 20px;">
                        <li class="li_cls">Complete customisation of invoices</li>
                        <li class="li_cls">Add payments to your invoices</li>
                        <li class="li_cls">Alerts to notify about client’s birthdays and anniversaries</li>
                        <li class="li_cls">Personalise notification on late-payers</li>
                      </ul>
                    </span></div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">How will I be reminded of my outstanding payments?</div>
                        <div class="collapsible-body"><span><p>You can set reminders of credit period for each client individually and Xebra will automatically set reminder emails to your client if they cross the credit period.</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">Will I Able to change the credit period set for my clients?</div>
                        <div class="collapsible-body"><span><p>Yes, you can change it from client master & edit the information for that client.</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">Can I delete the clients, when they stop generating business for company?</div>
                        <div class="collapsible-body"><span><p>No, but you can deactivate them & also can reactivate if needed in future. You can deactivate clients in action button drop down in client master.</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">Is it possible to record recurring invoices like retainer amount without having to make an entry for it every time?</div>
                        <div class="collapsible-body"><span><p>Yes, in while creating an invoice, select recurring in nature of invoice. You can set the recurring to the number of times you want. Xebra will automatically create an invoice of that amount at the set frequency.</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">Will I able to delete the services which our company have discontinued?</div>
                        <div class="collapsible-body"><span><p>No, but you can deactivate them in item master from action button drop down, which can later on reactivated if needed.</p></span></div>
                      </li>
					  
                      <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I customise or stop my notifications?</div>
                        <div class="collapsible-body"><span><p>Go to Settings and Click on Set Alerts &amp; Notifications. Select the appropriate module and
type of notification you wish to change</p></span></div>
                      </li>
                      <li class="row document-box-view redborder">
                        <div class="collapsible-header">How many types of notification are there?</div>
                        <div class="collapsible-body"><span><p>You can select from 3 types of notification: Web, Email and/or SMS</p></span></div>
                      </li>

                    </ul> <!-- </div> -->
                  </div>
                  <div id="test2" class="col s12  inner_row_container">
                    <ul class="collapsible">
                      <li class="row document-box-view redborder">
                        <div class="collapsible-header">Can I record international vendors and record a payment in international currency?</div>
                        <div class="collapsible-body"><span><p>Yes, you can record international vendors from vendor master. While filling up the data of that vendor, you can select the currency in which you want to pay them</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">Will I able to delete the vendor whose services we have discontinued?</div>
                        <div class="collapsible-body"><span><p>No, but you can deactivate them in vendor master from action button drop down. You also have the option to reactivate them should you resume working with them</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">Can I create company expense with GST reverse charge or input tax credit?</div>
                        <div class="collapsible-body"><span><p>Yes, you have to just check the check box while creating a company expense.</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">Can we direct create a purchase order without creating expense?</div>
                        <div class="collapsible-body"><span><p>No, first you have to create expense than you have to select the vendor for whom you want to create a purchase order.</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">Is it possible to record recurring expenses like rent without having to make an entry for it every time?</div>
                        <div class="collapsible-body"><span><p>Yes, in company expense module, you select the expense and then select recurring in nature of expense. You can set the recurring to the number of times you want. Xebra will automatically record the expense of that amount at the set frequency.</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I record my non-payment or partial payment against my vendor’s invoice?</div>
                        <div class="collapsible-body"><span><p>Select the particular invoice from the List View in Company Expenses and click on Make Payment option in Action Button. You will be guided to Expense Payment module where you will record the entry</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I record bulk payment made to a vendor’s multiple invoices?</div>
                        <div class="collapsible-body"><span><p>While recording the expense payment, simply click on add another invoice button which will allow you to club another invoice from the same vendor for the payment</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I record the expenses paid to an International vendor in foreign exchange?</div>
                        <div class="collapsible-body"><span><p>Follow these steps for correct recording of an international currency payment</br>
						1. Select appropriate country & Currency while recording in the vendor master</br>
						2. When you record your company expense for that vendor, the system will automatically ask you for a foreign exchange rate to the filled that is prevailing in current time</br>
						3. When you make the payment after say a period of one month, the system will again prompt you to enter the foreign exchange rate prevalent then and auto-calculate the forex gain or loss that you might have incurred
						</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I record forex gain or loss incurred because of international client payment timing?</div>
                        <div class="collapsible-body"><span><p>Select the appropriate currency for your client in the client master. Then enter the forex rate at the time of making the invoice and at the time of receiving the payment. Xebra will automatically the forex gain or loss derived from it.</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">After providing access to vendors in vendor portal, can we restrict them afterwards?</div>
                        <div class="collapsible-body"><span><p>Yes, you can deny them access by simple toggle button in vendor master.</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">Can I record company expenses and expense payment from the mobile application?</div>
                        <div class="collapsible-body"><span><p>No, you will be able to record that from your web interface only</p></span></div>
                      </li>

                    </ul>
                  </div>
                  <div id="test3" class="col s12 inner_row_container">
                     <ul class="collapsible">
                      <li class="row document-box-view redborder">
                         <div class="collapsible-header">How is Revenue calculated in the formula?</div>
                        <div class="collapsible-body"><span><p>Our formula is Total Revenue (minus) Total Credit Notes (minus) Total Bad Debts</p></span></div>
                      </li>
                      <li class="row document-box-view redborder">
                         <div class="collapsible-header">How do I get additional graphs pertaining to my business?</div>
                        <div class="collapsible-body"><span><p>If you would like Customised graphs for your business, then do send us an email detailing the same. Our team will reach out to you and understand your requirements and take the discussion forward accordingly</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">Does BI tool help me understand my outstanding credit?</div>
                        <div class="collapsible-body"><span><p>Yes, In our credit history sub module in Business Intelligence, you will get O/S credit information both client-wise and invoice-wise.</p></span></div>
                      </li>
                    </ul>
                  </div>
                  <div id="test4" class="col s12 inner_row_container">
                     <ul class="collapsible">
                       <li class="row document-box-view redborder">
                         <div class="collapsible-header">How do I cross check my TDS amount?</div>
                        <div class="collapsible-body"><span><p>Xebra will give you the invoice wise TDS amount that has been deducted by your client. You can log-in to form 26AS and cross check whether your client has paid that amount to the government. Once you have ascertained that, you can toggle the status to Paid in your list view. </p></span></div>
                      </li>

					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">How do I reconcile the TDS that I have deducted from my vendor’s payment?</div>
                        <div class="collapsible-body"><span><p>Simply click on TDS Vendors in Tax Summary module to know the exact status of how much TDS you have paid and for which vendor. This saves your time immensely</p></span></div>
                      </li>

                      <li class="row document-box-view redborder">
                         <div class="collapsible-header">How does GST Tax Summary Work?</div>
                        <div class="collapsible-body"><span><p>GST Tax Summary List view gives you your GST liability for the time period basis the invoices that you have entered. Ensure that you have prepared all your invoices from Xebra itself for an accurate GST amount calculation. You can select a specific time period and then Bulk Export it to excel. This will save your time drastically which otherwise would have been spent in filling an excel sheet to be filled as part of GSTIN returns </p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">Where do I track the Professional tax, EPF or ESIC of each employee?</div>
                        <div class="collapsible-body"><span><p>You will get all the details of Prof Tax, EPF and ESIC, employee wise in Emp Tax & Reimb module in Tax summary. This will give you a comprehensively view of how much has been paid and in remaining to be paid</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">Can I add multiple GSTIN of my company in Xebra?</div>
                        <div class="collapsible-body"><span><p>Yes, you can add as many GSTIN you have for your company in the module My Company Profile</p></span></div>
                      </li>
                    </ul>
                  </div>
                  <div id="test5" class="col s12 inner_row_container">
                     <ul class="collapsible">

                      <li class="row document-box-view redborder">
                        <div class="collapsible-header">How often do I have to renew my subscription?</div>
                        <div class="collapsible-body"><span><p>You have to renew your subscription annually</p></span></div>
                      </li>
                      <li class="row document-box-view redborder">
                        <div class="collapsible-header">Does it have the feature of auto-renew?</div>
                        <div class="collapsible-body"><span><p>Yes, you can select that in Subscription section in Profile Dropdown</p></span></div>
                      </li>
                      <li class="row document-box-view redborder">
                        <div class="collapsible-header">How much cloud space do you allocate per user?</div>
                        <div class="collapsible-body"><span><p>With every paid business sign-up, we allocate a space of 1 GB for you to upload your
critical business documents for storage. You can always buy more space at minimal price
should you need more</p></span></div>
                      </li>
                      <li class="row document-box-view redborder">
                        <div class="collapsible-header">Is there a limit on how many SMS we have with signup?</div>
                        <div class="collapsible-body"><span><p>Yes, every paid business sign-up will be allocated 500 SMS that will act as notification
should you choose them as your preference. You can buy more at minimal price when these
run out</p></span></div>
                      </li>
                      <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I buy additional space or SMS?</div>
                        <div class="collapsible-body"><span><p>Simply click on the subscription &amp; Billing section and select the quantity of space or SMS
you require. You can make the payment at check-out.</p></span></div>
                      </li>
                      <li class="row document-box-view redborder">
                        <div class="collapsible-header">How much referral amount can I earn?</div>
                        <div class="collapsible-body"><span><p>If a user purchases an annual subscription; you will earn Rs. 2500 and the user will get Rs. 2500 as discount
If a user purchases a monthly subscription; you will earn Rs. 250 and the user will get Rs. 250 as discount
</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">What is the refund policy?</div>
                        <div class="collapsible-body"><span><p>Xebra will not refund any money should you choose to cancel your plan or stop using the product in the middle of your subscription period
						</p></span></div>
                      </li>

					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">Can I change my plan later?</div>
                        <div class="collapsible-body"><span><p>Yes, you can upgrade your plan at any point in time. Once you upgrade, we will deduct the pro-rata cost of your pending current plan from the total billing
						If you downgrade your plan, then that will be applicable only once the existing plan period is completed
						</p></span></div>
                      </li>
                    </ul>
                  </div>
                   <div id="test6" class="col s12 inner_row_container">
                     <ul class="collapsible">
                      <li class="row document-box-view redborder">
                        <div class="collapsible-header">Will I have to enter in dual OTP every time I log in?</div>
                        <div class="collapsible-body"><span><p>NO, it will ask you for it when you are signing up for the first time and every time you log
in after you have changed your password</p></span></div>
                      </li>
                      <li class="row document-box-view redborder">
                        <div class="collapsible-header">Why did the system simply log me out on its own?</div>
                        <div class="collapsible-body"><span><p>If your Xebra browser tab remains unattended for more than 60 minutes, then
system would auto log out for your safety</p></span></div>
                      </li>

                       <li class="row document-box-view redborder">
                        <div class="collapsible-header">On which cloud server have you hosted the application?</div>
                        <div class="collapsible-body"><span><p>We have partnered with Google Cloud and have hosted it there </p></span></div>
                      </li>

                    </ul>
                  </div>
				  <div id="test7" class="col s12 inner_row_container">
                     <ul class="collapsible">
                       <li class="row document-box-view redborder">
                         <div class="collapsible-header">Which assets should I add in Asset Tracker?</div>
                        <div class="collapsible-body"><span><p>We would encourage you to record all your company assets so that they are properly tracked, and you know their current status at any point in time</p></span></div>
                      </li>

                      <li class="row document-box-view redborder">
                         <div class="collapsible-header">Why can’t I add the asset directly in Asset Purchase module?</div>
                        <div class="collapsible-body"><span><p>You have to first add the asset in the Vendor & Asset Master module and basis that it will appear in the Asset Name dropdown in Asset Purchase.</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">Does Xebra automatically calculate depreciation of my assets?</div>
                        <div class="collapsible-body"><span><p>Yes, Xebra is automated accounting software and is capable of calculating the depreciation automatically.</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">Do I have to calculate depreciation for every year?</div>
                        <div class="collapsible-body"><span><p>No, you can just schedule the depreciation by setting start date, useful life & rate of depreciation in asset purchase module.</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">Which method is used to calculate depreciation?</div>
                        <div class="collapsible-body"><span><p>You can choose SLM or WDV method of depreciation from asset purchase while setting the date & rate of depreciation.</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">Can I add any pre-owned asset, which is currently used for business purpose?</div>
                        <div class="collapsible-body"><span><p>Yes, you can easily add pre owned assets in asset purchase by checking the check box of pre-owned asset, but before that you have to add asset & vendor in asset & vendor master.</p></span></div>
                      </li>

                    </ul>
                  </div>
				  <div id="test8" class="col s12 inner_row_container">
                     <ul class="collapsible">
                       <li class="row document-box-view redborder">
                         <div class="collapsible-header">What does Employee Access mean? How do I activate or deactivate that?</div>
                        <div class="collapsible-body"><span><p>Employee Access allows you to give log in permission to your employees. You can select the ones you want to access to the Xebra platform from toggle switch in Employee Master section. Once you give access that particular employee will receive his/her own login credentials and be able to log in. They will be able to:</br>
						•	Create their expense vouchers and see the payment status</br>
						•	View their monthly salary and download their salary slips</br>
						•	View their monthly TDS and other taxes deductions and download a copy for themselves
						</p></span></div>
                      </li>

                      <li class="row document-box-view redborder">
                         <div class="collapsible-header">Can an employee make any changes to company records on salary or other module from their login?</div>
                        <div class="collapsible-body"><span><p>No, the only module that an employee can use create and edit feature is their own expense voucher. The employee login feature does not allow any create or edit or delete feature. You can read more about the access list in My Person Profile / Create</p></span></div>
                      </li>

					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">What is the difference between HR & Employee login?</div>
                        <div class="collapsible-body"><span><p>When an employee logs in, he or she can only see their expense vouchers, salary and TDS deductions
						When an HR manager logs in, the person will get complete access to My Employees module along with their individual access to expense voucher, salary and TDS deduction. You can read more about the access list in My Person Profile / Create
						</p></span></div>
                      </li>

					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">How do I track leaves of each employee?</div>
                        <div class="collapsible-body"><span><p>The leaves are auto calculated when you record the entry in the Salary Expense sub-module. You will be able to see the leaves taken by an employee during each month or year
						</p></span></div>
                      </li>

					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">Which modules can be accessed with HR Login?</div>
                        <div class="collapsible-body"><span><p>When an HR manager logs in, the person will get complete access to My Employees module along with their individual access to expense voucher, salary and TDS deduction. You can read more about the access list in My Person Profile / Create
						</p></span></div>
                      </li>

					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">How do I record any advance that I have given to an employee?</div>
                        <div class="collapsible-body"><span><p>While recording the salary for a particular employee, you can select Advance given in the Additions tab
						</p></span></div>
                      </li>

					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">How do I record Gratuity, Super Annuation benefits or joining bonus or sales incentive?</div>
                        <div class="collapsible-body"><span><p>While recording the salary for a particular employee, you can select Advance given in the Additions tab
						</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">How do I record any advance given to an employee?</div>
                        <div class="collapsible-body"><span><p>Go to Salary Expense sub-module and select the particular employee. From there, click on Additions and select Advance from the dropdown option and Save
						</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">Can I record my share of EPF or ESIC?</div>
                        <div class="collapsible-body"><span><p>Yes, you can record it salary expense sub module for individual employees and then make payment entries in Receipts and Payments Module
						</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">Can I record appraisal discussion for individual employee and set reminders for it?</div>
                        <div class="collapsible-body"><span><p>Yes, you can set reminders for appraisal in Employee Master and record appraisal discussion of each employee in Appraisal sub module
						</p></span></div>
                      </li>
					  <!--li class="row document-box-view redborder">
                         <div class="collapsible-header">How do I record the EPF and ESIC in payments?</div>
                        <div class="collapsible-body"><span><p>While recording the salary for a particular employee, you can select Advance given in the Deductions tab
						</p></span></div>
                      </li-->
                    </ul>
                  </div>
                  <div id="test9" class="col s12 inner_row_container">
                     <ul class="collapsible">
                       <li class="row document-box-view redborder">
                         <div class="collapsible-header">Where do I record my opening Bank Balance?</div>
                        <div class="collapsible-body"><span><p>You can record it in My Company Profile module while entering your bank details</p></span></div>
                      </li>

                      <li class="row document-box-view redborder">
                         <div class="collapsible-header">How do I record money transferred from my one bank account to another?</div>
                        <div class="collapsible-body"><span><p>Go to Bank Statement and select Record bank Transfer. All the bank accounts that you entered from My Company Profile module will be reflected there in the dropdown. You can select the respective banks and make the entry</p></span></div>
                      </li>

					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">How do I edit or delete a bank entry about a sales receipt or vendor payment that I made?</div>
                        <div class="collapsible-body"><span><p>You will have to go to the relevant sub module under Receipts & Payments section and make the edit or delete from there. You will not be able to edit or delete from Bank Statement</p></span></div>
                      </li>

					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">Where do I record my opening Cash Balance?</div>
                        <div class="collapsible-body"><span><p>You can record it in My Company Profile module in Customisation section</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">How do I record cash deposited or withdrawn from my bank account?</div>
                        <div class="collapsible-body"><span><p>Go to Cash Statement and select Add Cash Entry. You can select deposit or withdraw and the appropriate bank name against it to make the entry</p></span></div>
                      </li>

					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">How do I edit or delete a cash entry about a sales receipt or vendor payment that I made?</div>
                        <div class="collapsible-body"><span><p>You will have to go to the relevant sub module under Receipts & Payments section and make the edit or delete from there. You will not be able to edit or delete from Cash Statement</p></span></div>
                      </li>

					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">How do I record my opening petty cash statement?</div>
                        <div class="collapsible-body"><span><p>You can record it in My Company Profile module in Customisation section</p></span></div>
                      </li>

					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">How do I record adding more cash for petty cash expenses?</div>
                        <div class="collapsible-body"><span><p>Go to Cash Statement and click on Add Cash Entry. Select Add to Petty Cash option in the particulars and enter the amount</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">Can I add more than 1 bank/card account?</div>
                        <div class="collapsible-body"><span><p>Yes, you can add multiple bank/card account</p></span></div>
                      </li>

						<li class="row document-box-view redborder">
                         <div class="collapsible-header">Where do I record my different types of credit and debit cards?</div>
                        <div class="collapsible-body"><span><p>You can record it in My Company Profile module. In that section, you can record as many of your Debit and Credit cards</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">How do I make card payments?</div>
                        <div class="collapsible-body"><span><p>Go to Card Statement and click on Record Card Payment. You Can select the card you want to make the payment for along with the statement period.</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">How do I edit or delete a card entry about a vendor payment that I made?</div>
                        <div class="collapsible-body"><span><p>You will have to go to the relevant sub module under Receipts & Payments section and make the edit or delete from there. You will not be able to edit or delete from Card Statement</p></span></div>
                      </li>
                    </ul>
                  </div>
				  
				  <div id="test10" class="col s12 inner_row_container">
                     <ul class="collapsible">
                       <li class="row document-box-view redborder">
                         <div class="collapsible-header">Do I have to prepare individual ledger account after making my expense, sales or any other entries?</div>
                        <div class="collapsible-body"><span><p>No, you just have to record your entry. Ledgers will be created automatically basis those.</p></span></div>
                      </li>

					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">Do I have to use another software to make my ledger & final account?</div>
                        <div class="collapsible-body"><span><p>No, Xebra will automatically create all ledgers & financial statements for your business.</p></span></div>
                      </li>

                      <li class="row document-box-view redborder">
                         <div class="collapsible-header">Are my numbers in the ledgers, P&L and Balance sheet updated real time?</div>
                        <div class="collapsible-body"><span><p>Yes. Xebra automatically updates all these accounting reports in real time with every entry you pass in the application.</p></span></div>
                      </li>
                    </ul>
                  </div>
				  
				  <div id="test11" class="col s12 inner_row_container">
                     <ul class="collapsible">
                       <li class="row document-box-view redborder">
                         <div class="collapsible-header">I didn’t get an email with an OTP while signing up</div>
                        <div class="collapsible-body"><span><p>Usually, this problem should not happen but in the event that it does, do check your spam box.</p></span></div>
                      </li>

					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">How can I access my uploaded documents in Xebra?</div>
                        <div class="collapsible-body"><span><p>You can access your documents through Document Locker.</p></span></div>
                      </li>

                      <li class="row document-box-view redborder">
                         <div class="collapsible-header">Will it be safe to upload my legal or company document here?</div>
                        <div class="collapsible-body"><span><p>Yes, your document will be highly secure as we have collaboration with google cloud for uploading documents through Xebra.</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">How often do I have to renew my subscription?</div>
                        <div class="collapsible-body"><span><p>You have to renew your subscription annually.</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">Does it have the feature of auto-renew?</div>
                        <div class="collapsible-body"><span><p>Yes, you can select that in Subscription section in Profile Dropdown.</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">After the end of my subscription will I able to get my documents from the cloud?</div>
                        <div class="collapsible-body"><span><p>Yes, you will be able to download PDFs & export to excel all your business financial documents.</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">How much cloud space do you allocate per user?</div>
                        <div class="collapsible-body"><span><p>It varies with the plan that you have subscribed for. For latest updates, check the pricing page on website or Buy Add-ons in Renew subscription in Profile dropdown</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                         <div class="collapsible-header">Is there a limit on how many SMS we have with signup?</div>
                        <div class="collapsible-body"><span><p>Yes. It varies with the plan that you have subscribed for. For latest updates, check the pricing page on website or Buy Add-ons in Renew subscription in Profile dropdown.</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I buy additional space or SMS?</div>
                        <div class="collapsible-body"><span><p>Simply click on the subscription &amp; Billing section and select the quantity of space or SMS
you require. You can make the payment at check-out.</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">When is a referral considered successful?</div>
                        <div class="collapsible-body"><span><p>For a referral to be successful, the user has to undertake a paid monthly or annual subscription of Xebra. Also at the time of payment, he has to punch in your referral code</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">Will the referral be considered valid if the user subscribes without entering the code?</div>
                        <div class="collapsible-body"><span><p>No, the user has to enter your specific referral code for you to earn your referral fee</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">Is there a limit on how much I can earn via the referral program?</div>
                        <div class="collapsible-body"><span><p>No, there is no limit. You can refer us to as many businesses as you like
</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">When do I get my referral reward?</div>
                        <div class="collapsible-body"><span><p>Your referral total amount will be calculated and summed up for each month. You will be paid in the first week of succeeding month
</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">What is the refund policy?</div>
                        <div class="collapsible-body"><span><p>Xebra will not refund any money should you choose to cancel your plan or stop using the product in the middle of your subscription period
</p></span></div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header">Can I change my plan later?</div>
                        <div class="collapsible-body"><span><p>Yes, you can upgrade your plan at any point in time. Once you upgrade, we will deduct the pro-rata cost of your pending current plan from the total billing
If you downgrade your plan, then that will be applicable only once the existing plan period is completed

</p></span></div>
                      </li>
                    </ul>
                  </div>
				  
				  <div id="test12" class="col s12 inner_row_container">
                     <ul class="collapsible">
                      <li class="row document-box-view redborder">
                        <div class="collapsible-header">Can I make invoices and record sales receipt from the mobile application?</div>
                        <div class="collapsible-body"><span><p>You will be able to record that from your web interface only</p></span></div>
                      </li>
                      <li class="row document-box-view redborder">
                        <div class="collapsible-header">Can I make invoices and record sales receipt from the mobile application?</div>
                        <div class="collapsible-body"><span><p>You will be able to record that from your web interface only</p></span></div>
                      </li>

                       <li class="row document-box-view redborder">
                        <div class="collapsible-header">How do I download Xebra mobile app?</div>
                        <div class="collapsible-body"><span><p>You can download it from Google Play Store or Apple App Store. </p></span></div>
                      </li>

                    <li class="row document-box-view redborder">
                        <div class="collapsible-header">Can I access every module through mobile app?</div>
                        <div class="collapsible-body"><span><p>No, only selected modules like Business Intelligence and set alerts can be accessed. Also individuals can also view and download their own Expense Voucher, Salary, TDS, Professional Tax, EPF & ESIC </p></span></div>
                      </li>
					</ul>
                  </div>
				  
				  </div>
                </div>
              </div>

        </section>
        <!-- END CONTENT -->
        </div>
        <!-- END WRAPPER -->
      </div>
      <!-- END MAIN -->


      <!-- ================================================
    Scripts
    ================================================ -->
	<script>
			if ($('.menu-icon').hasClass('open')) {
				$('.menu-icon').removeClass('open');
				$('.menu-icon').addClass('close');
				$('#left-sidebar-nav').removeClass('nav-lock');
				$('.header-search-wrapper').removeClass('sideNav-lock');
				$('#header').addClass('header-collapsed');
				$('#logo_img').show();
				$('#eazy_logo').hide();
				$('#left-sidebar-nav').toggleClass('nav-expanded nav-collapsed');
				$('#main').toggleClass('main-full');
			}
		</script>
    <?php $this->load->view('template/footer'); ?>
