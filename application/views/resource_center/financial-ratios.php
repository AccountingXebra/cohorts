<?php $this->load->view('my-community/Cohorts-header');?>

    <!-- //////////////////////////////////////////////////////////////////////////// -->
    <style type="text/css">
    .add-new {
         margin: 0 0 0 0 !important; 
    }
     .section_li {
         margin: 0 0 0 0 !important; 
    }
    .categories>li {
      float: none;
      display: inline-block;
    }
    a.btn.btn-theme.btn-large.section_li {
        background-color: #f8f9fd;
        color: #7864e9;
        border: 1px solid #7864e9;
    }
    .btn-theme{
		padding: 0 40px !important;
        border-radius: 6px !important;
        line-height: 45px !important;
        height: 45px !important;
    } 
    ul.categories {
		text-align: center;
        margin:0;
        width: 100%;
    }
    /* li {
		padding: 0 5px 0 5px;
    }*/
    .tabs {
		height:auto;
        background: transparent !important;
        width: 100% !important;
    }
    ul.tabs li {
		border:none !important;
        float: left;
        width: 121px !important;
    }
    li.tab a.active {
		min-height: auto !important;
    }
	
	a.btn.btn-theme.btn-large.section_li.active {
		background-color: #7864e9 !important;
		color: #f8f9fd;
		border: 0px;
	}
	
	.tabs .tab{
		padding: 0 5px 0 5px !important;
	}
	
	.collapsible {
		border: 0px;
		-webkit-box-shadow: none;
	}
  
	.document-box-view.redborder.active {
		border-left: 4px solid #ff7c9b;
	}

	.collapsible-header{
		border-bottom:none;  
	}

	.collapsible-header.active{
		padding-bottom: 0px;
        font-weight: 600;
	}

	.collapsible-body{
         padding: 0 1rem 1rem 1rem;
	}
	
	/*.collapsible-body span {
		color: #464646;
	}*/

	.red-bg-right:before {
		top: -32px !important;
	}
	
	.green-bg-right:after{
		bottom: -25px !important;
		left: 50px;
	}

	ul:not(.browser-default) > li .li_cls {
		list-style-type: disc !important;
	}
	
	.btn-theme{
		padding:0px !important;
	}
	
	.formula{
		font-size:15px;
		color: #7864e9 !important;
	}
	
	.mt-5 {
		margin-top: 3% !important;
	}
	
	.info-top{
		text-align:center;
		margin-bottom:20px;
	}
    </style>
    <!-- START MAIN -->
    <div id="main" style="padding-left:0px !important;">
      <!-- START WRAPPER -->
      <div class="wrapper">
        <!-- START LEFT SIDEBAR NAV-->
        <?php //$this->load->view('template/sidebar'); ?>
        <!-- END LEFT SIDEBAR NAV-->
        <!-- //////////////////////////////////////////////////////////////////////////// -->
        <!-- START CONTENT -->
        <section id="content" class="bg-cp">
          <div id="breadcrumbs-wrapper">
            <div class="container custom">
              <div class="row">
                <div class="col s10 m6 l6">
                  <h5 class="breadcrumbs-title my-ex">Financial Ratios</h5>
                  <ol class="breadcrumbs">
                    <li><a href="index.html">LEARNING / FINANCIAL RATIOS</a>
                  </ol>
                </div>
              </div>
            </div>
		 </div>
         <div id="bulk-action-wrapper">
          <div class="container custom">
              <div class="row">
                  <div class="col l12 s12 m12">
                    <ul class="tabs tabs-fixed-width tab-demo categories">
                    <li class="tab"><a href="#test1" class="btn btn-theme btn-large section_li">LIQUID RATIOS</a></li>
                    <li class="tab"><a  href="#test2"  class="btn btn-theme btn-large section_li">PROFITABILITY RATIOS</a></li>
                    <li class="tab"><a href="#test3"  class="btn btn-theme btn-large section_li">ACTIVITY RATIOS</a></li>
                    <li class="tab"><a href="#test4"  class="btn btn-theme btn-large section_li">SOLVENCY RATIOS</a></li>
                  </ul>
                  <!-- <a  href="#" class="btn btn-theme btn-large  add-new modal-trigger">ADD INVOICES</a>  -->
                 </div>
              </div>
          </div>
        </div>
        
          <div class="container custom">
              <div class="row">
                  <div class="col l12 s12 m12 mt-5 after-green-bg green-bg-right">
                  <div id="test1" class="col s12 inner_row_container ">
                   <!--  <div class="row document-box-view blueborder"> -->
                    <ul class="collapsible">
                      <li class="row document-box-view redborder">
                        <div class="collapsible-header"><b>CURRENT RATIO</b></div>
                        <div class="collapsible-body">
							<span>
								<p>This ratio, which is also called as a working capital ratio, tell abount the short-term solvency, ability to meet the ongoing abligations of the business</p>
							</span>
							<span>
								<p class="formula"><b>Formula: Current Ratio = Current assets / Current Liabilities</b></p>
							</span>
						</div>
                      </li>
					  
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header"><b>QUICK RATIO</b></div>
                        <div class="collapsible-body">
							<span>
								<p>It shows a relationship between the liquid asset and current liabilities present in the business</p>
								<p>Liquid assets: Total current assets minus inventories and prepaid expenses</p>
							</span>
							<span>
								<p class="formula"><b>Formula: Quick Ratio = Liquid assets / Current liabilities</b></p>
							</span>
						</div>
                      </li>
					  
                      <li class="row document-box-view redborder">
                        <div class="collapsible-header"><b>CURRENT CASH DEBT COVERAGE RATIO</b></div>
                        <div class="collapsible-body">
							<span>
								<p>It indicates the ability of the business unit to pay its current liabilities using the cash generated from day-to-day business operations</p>
							</span>
							<span>
								<p class="formula"><b>Formula: Current cash debt coverage ratio = Net cash provided by operating activities / Average current liabities</b></p>
							</span>
						</div>
                      </li>
                    </ul> 
                  </div>
                  
				  <div id="test2" class="col s12 inner_row_container">
					<div class="row">
						<div class="col s12 m12 l12 info-top">
							<p> <b>These ratios help in determining the efficiency of business to use its resources in generating profit and increasing shareholder value. </b></p>
							<p> <b>It is used for valuation purposes and understanding the success or failure of business operations against its competitors. </b></p>
						</div>
					</div>
                    <ul class="collapsible">
                      <li class="row document-box-view redborder">
                        <div class="collapsible-header"><b>NET PROFIT RATIO</b></div>
                        <div class="collapsible-body">
							<span>
								<p>The purpose of this ratio is to evaluate the profits from its primary operating activities. Higher net profit ratio indicates the efficient management of the affairs of business with better control on cost.</p>
							</span>
							<span>
								<p class="formula"><b>Formula: Net profit ratio = Net profit after tax / Net sales</b></p>
							</span>
						</div>
                      </li>
					  
                      <li class="row document-box-view redborder">
                        <div class="collapsible-header"><b>GROSS PROFIT RATIO</b></div>
                        <div class="collapsible-body">
							<span>
								<p>This ratio, also known as Gross Margin, is used to assess the company’s financial health by diagnosing the money left after accounting adjustments of Cost of Goods Sold (COGS)</p>
							</span>
							<span>
								<p class="formula"><b>Formula: Gross Profit ratio = (Revenue – COGS) / Revenue</b></p>
							</span>
						</div>
                      </li>
					  
                      <li class="row document-box-view redborder">
                        <div class="collapsible-header"><b>PRICE EARNINGS RATIO</b></div>
                        <div class="collapsible-body">
							<span>
								<p>This ratio enables the investor to know about his earning on per rupee of investment in the share price of a company</p>
							</span>
							<span>
								<p class="formula"><b>Formula: Price Earnings Ratio = Market Price / Earnings per share</b></p>
							</span>
						</div>
                      </li>
                      <li class="row document-box-view redborder">
                        <div class="collapsible-header"><b>OPERATING RATIO</b></div>
                        <div class="collapsible-body">
							<span>
								<p>This ratio defines the operating efficiency of the management of the company; managing to reduce the expenses and generate a profit of the company while revenues are decreasing. The smaller ratio is better for the company</p>
							</span>
							<span>
								<p class="formula"><b>Formula: Operating Ratio = Operating Expense / Net Sales</b></p>
							</span>
						</div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header"><b>EXPENSE RATIO</b></div>
                        <div class="collapsible-body">
							<span>
								<p>An asset management company is concerned with the operating cost and another cost incidental to that of a mutual fund. It is also known as ‘management expense ratio’.</p>
							</span>
							<span>
								<p class="formula"><b>Formula: Expense ratio = Administrative and Operating Expenses / Average of Fund’s Asset</b></p>
							</span>
						</div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header"><b>DIVIDEND YIELD RATIO</b></div>
                        <div class="collapsible-body">
							<span>
								<p>This ratio helps the investor analyse the return on his investment in the form of dividend income compared to the price paid by him in the market.</p>
							</span>
							<span>
								<p class="formula"><b>Formula: Dividend Yield = Annual Dividend Per Share / Market Price per Share</b></p>
							</span>
						</div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header"><b>DIVIDEND PAYOUT RATIO</b></div>
                        <div class="collapsible-body">
							<span>
								<p>It presents the percentage of net income a company is paying in the form of a dividend to its shareholders. The ratio also describes the financial strength of the company and prospects for maintaining or improving its dividend payout in future.</p>
							</span>
							<span>
								<p class="formula"><b>Formula: Dividend Payout Ratio = Annual Dividend paid / Earnings per Share</b></p>
							</span>
						</div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header"><b>RETURN ON CAPITAL EMPLOYED RATIO</b></div>
                        <div class="collapsible-body">
							<span>
								<p>This ratio indicates the operating effectiveness of the company in deploying its capital employed to generate earnings and increase the value of shareholders’ stake</p>
								<p>Earnings before Interest and Tax: Revenue minus COGS and operating expenses</p>
								<p>Capital Employed: Total Assets less Current Liabilities</p>
							</span>
							<span>
								<p class="formula"><b>Formula: Return on Capital Employed = Earnings before interest and tax / Capital Employed</b></p>
							</span>
						</div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header"><b>EARNINGS PER SHARE</b></div>
                        <div class="collapsible-body">
							<span>
								<p>It is a portion of the company’s profit after dividend allocated to equity shares. It is more accurate to use a weighted average number of shares outstanding over the term because of the number of shares outstanding can different over time</p>
							</span>
							<span>
								<p class="formula"><b>Formula: EPS = Earnings attributable to common stock / Weighted average number of outstanding common share</b></p>
							</span>
						</div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header"><b>RETURN ON EQUITY RATIO</b></div>
                        <div class="collapsible-body">
							<span>
								<p>It quantifies the efficiency of a company in generating profits, except it, only considers the equity portion of capital invested by the shareholders</p>
							</span>
							<span>
								<p class="formula"><b>Formula: ROE = Net Income / Shareholder’s Equity</b></p>
							</span>
						</div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header"><b>RETURN ON COMMON STOCKHOLDERS EQUITY RATIO</b></div>
                        <div class="collapsible-body">
							<span>
							 <p>It determines the realization percentage of earnings generated by the company for common stockholders</p>
							 <P>Stockholder’s Equity: Sum of common stock, additional paid-in capital and retained earnings</p>
							</span>
							<span>
								<p class="formula"><b>Formula: Return on Common Stockholders Equity Ratio = (Net Income – Preferential dividend) / Average common stockholders’ equity</b></p>
							</span>
						</div>
                      </li>

                    </ul> 
                  </div>
                  <div id="test3" class="col s12 inner_row_container">
					<div class="row">
						<div class="col s12 m12 l12 info-top">
							<p> <b>These ratios indicate the relative production efficiency of the company based on the use of its assets. It is used to determine</b></p>
							<p> <b>and quantify whether the company’s management’s efforts are enough in generating revenues and cash.</b></p>
						</div>
					</div>
                    <ul class="collapsible">
                      <li class="row document-box-view redborder">
                        <div class="collapsible-header"><b>INVENTORY TURNOVER RATIO</b></div>
                        <div class="collapsible-body">
							<span>
								<p>It shows how much inventory is sold over a period of time. It’s important to compare this ratio with the industry benchmark to assess the successful management of inventory.</p>
							</span>
							<span>
								<p class="formula"><b>Formula: Inventory Turnover Ratio = Sales / Inventory</b></p>
							</span>
						</div>
                      </li>
					  
                      <li class="row document-box-view redborder">
                        <div class="collapsible-header"><b>RECEIVABLES TURNOVER RATIO</b></div>
                        <div class="collapsible-body">
							<span>
								<p>It creates the relationship between the average debtors and the credit sales made over a period of time.</p>
							</span>
							<span>
								<p class="formula"><b>Formula: Receivables Turnover Ratio = Net Credit Sales / Average Accounts receivables</b></p>
							</span>
						</div>
                      </li>
					  
                      <li class="row document-box-view redborder">
                        <div class="collapsible-header"><b>AVERAGE COLLECTION PERIOD</b></div>
                        <div class="collapsible-body">
							<span>
								<p>It indicates the ability of management to collect and sustain the inflow of cash in the company.</p>
							</span>
							<span>
								<p class="formula"><b>Formula: Average Collection Period= (Days X Average amount of accounts receivables)/Net credit sales</b></p>
							</span>
						</div>
                      </li>
                      <li class="row document-box-view redborder">
                        <div class="collapsible-header"><b>AVERAGE PAYMENT PERIOD</b></div>
                        <div class="collapsible-body">
							<span>
								<p>It helps in indicating the short-term liquidity of the company and used to know in how many days does the company pay its outstanding dues to suppliers.</p>
							</span>
							<span>
								<p class="formula"><b>Formula: Average Payment Period = Total net credit purchases / Average account payable</b></p>
							</span>
						</div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header"><b>ASSET TURNOVER RATIO</b></div>
                        <div class="collapsible-body">
							<span>
								<p>It is a ratio used to measure the operating effectiveness and efficiency of the company to generate net sales from the total amount of assets held by the firm.</p>
								<p>Total Assets: It includes fixed assets (net of depreciation) and current assets</p>
							</span>
							<span>
								<p class="formula"><b>Formula: Fixed Asset Turnover Ratio = Net Sales / Total Asset</b></p>
							</span>
						</div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header"><b>WORKING CAPITAL TURNOVER RATIO</b></div>
                        <div class="collapsible-body">
							<span>
								<p>It indicates the efficiency of management of the company to generate the net revenue by using the invested working capital in the company.</p>
							</span>
							<span>
								<p class="formula"><b>Formula: Working Capital Turnover Ratio = Sales / Working Capital</b></p>
							</span>
						</div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header"><b>FIXED ASSET TURNOVER RATIO</b></div>
                        <div class="collapsible-body">
							<span>
								<p>It is used to measure the operating effectiveness and efficiency of the company to generate net sales from fixed-asset investment.</p>
							</span>
							<span>
								<p class="formula"><b>Formula: Fixed Asset Turnover Ratio = Net Sales / Net Fixed Assets</b></p>
							</span>
						</div>
                      </li>

                    </ul> 
                  </div>
                  <div id="test4" class="col s12 inner_row_container">
                     <div class="row">
						<div class="col s12 m12 l12 info-top">
							<p> <b>Solvency refers to the ability of the company to pay its long-term debt on time.</b></p>
							<p> <b>It indicates the company’s financial strength and sufficient flow of cash to meet paying off its debt and other obligation.</b></p>
						</div>
					</div>
                    <ul class="collapsible">
                      <li class="row document-box-view redborder">
                        <div class="collapsible-header"><b>DEBT-EQUITY RATIO</b></div>
                        <div class="collapsible-body">
							<span>
								<p>It indicates how much of the company’s total assets are financed by debt in comparison with the amount of shareholder’s equity.</p>
							</span>
							<span>
								<p class="formula"><b>Formula: Debt Equity Ratio = Total Debts / Shareholder’s Equity</b></p>
							</span>
						</div>
                      </li>
					  
                      <li class="row document-box-view redborder">
                        <div class="collapsible-header"><b>TIME INTEREST EARNED RATIO</b></div>
                        <div class="collapsible-body">
							<span>
								<p>It shows the company’s ability to generate and sustain sufficient pretax and interest earnings.</p>
							</span>
							<span>
								<p class="formula"><b>Formula: Time Interest Earned Ratio = Earnings before interest and tax/Total Debt</b></p>
							</span>
						</div>
                      </li>
					  
                      <li class="row document-box-view redborder">
                        <div class="collapsible-header"><b>PROPRIETARY RATIO</b></div>
                        <div class="collapsible-body">
							<span>
								<p>It determines the realization percentage of common stockholders in assets of the company in the event of a liquidation. It indicates the security of the common stockholder’s equity.</p>
								<p>Stockholder’s Equity: Sum of common stock, additional paid-in capital and retained earnings</p>
							</span>
							<span>
								<p class="formula"><b>Formula: Return on Common Stockholders Equity Ratio = Total Stockholder’s Equity / Total Assets</b></p>
							</span>
						</div>
                      </li>
                      <li class="row document-box-view redborder">
                        <div class="collapsible-header"><b>FIXED ASSET TO EQUITY RATIO</b></div>
                        <div class="collapsible-body">
							<span>
								<p>It indicates the amount of equity invested by the company in fixed assets. It’s relatively used more as every investor is concerned about the principal amount of investment made by him rather than with the earnings on his investment.</p>
								<p>Net Fixed Assets: Fixed assets net of depreciation (Gross Value – Dep. = Net Block)</p>
							</span>
							<span>
								<p class="formula"><b>Formula: Fixed Asset to Equity Ratio = Net Fixed Assets / Shareholder’s Equity</b></p>
							</span>
						</div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header"><b>CURRENT ASSET TO EQUITY RATIO</b></div>
                        <div class="collapsible-body">
							<span>
								<p>It indicates the amount of shareholder’s equity that’s invested in current assets.</p>
							</span>
							<span>
								<p class="formula"><b>Formula: Current Asset to Equity Ratio = Current Assets / Shareholder’s Equity</b></p>
							</span>
						</div>
                      </li>
					  <li class="row document-box-view redborder">
                        <div class="collapsible-header"><b>CAPITAL GEARING RATIO</b></div>
                        <div class="collapsible-body">
							<span>
								<p>It stages the degree to which a company’s activities are funded by the shareholder’s and how much through borrower’s fund.</p>
							</span>
							<span>
								<p class="formula"><b>Formula: Capital Gearing Ratio = (Long term debt + Short term debt + Bank overdraft) / Shareholder’s Equity</b></p>
							</span>
						</div>
                      </li>
                    </ul>
                  </div>
                  
                  </div>
                </div>
              </div>
        </section>
        <!-- END CONTENT -->
        </div>
        <!-- END WRAPPER -->
      </div>
      <!-- END MAIN -->

    <?php $this->load->view('template/footer'); ?>