<?php $this->load->view('my-community/Cohorts-header');?>

<!-- END HEADER -->
<!-- //////////////////////////////////////////////////////////////////////////// -->
<style type="text/css">
   .add-new {
   margin: 0 0 0 0 !important; 
   }
   .section_li {
   margin: 0 0 0 0 !important; 
   }
   .categories>li {
   float: none;
   display: inline-block;
   }
   a.btn.btn-theme.btn-large.section_li {
   background-color: #f8f9fd;
   color: #7864e9;
   border: 1px solid #7864e9;
   font-size: 12px;
   }
   .btn-theme{
   padding: 0 10px !important;
   border-radius: 6px !important;
   line-height: 40px !important;
   height: 40px !important;
   } 
   ul.categories {
   text-align: center;
   margin:0;
   width: 100%;
   }
   /* li {
   padding: 0 5px 0 5px;
   }*/
   .tabs {
   height:auto;
   background: transparent !important;
   width: 100% !important;
   }
   ul.tabs li {
   border:none !important;
   float: left;
   width: 121px !important;
   }
   li.tab a.active {
   min-height: auto !important;
   }
   a.btn.btn-theme.btn-large.section_li.active {
   background-color: #7864e9 !important;
   color: #f8f9fd;
   border: 0px;
   }
   .tabs .tab{
   padding: 0 2px 0  !important;
   }
   .collapsible {
   border: 0px;
   -webkit-box-shadow: none;
   }
   .document-box-view.redborder.active {
   border-left: 4px solid #ff7c9b;
   }
   .collapsible-header{
   border-bottom:none;
   font-weight:500;
   }
   .collapsible-header.active{
   padding-bottom: 0px;
   font-weight: 600;
   }
   .collapsible-body{
   padding: 0 1rem 1rem 1rem;
   }
   /*.collapsible-body span {
   color: #464646;
   }*/
   .red-bg-right:before {
   top: -32px !important;
   }
   .green-bg-right:after{
   bottom: -25px !important;
   left: 50px;
   }
   .collapsible-body p {
		margin: 0;
	}
	.mt-2{ margin-top:2%; }
</style>
<!-- START MAIN -->
<div id="main" style="padding-left:0px !important;">
   <!-- START WRAPPER -->
   <div class="wrapper">
      <!-- START LEFT SIDEBAR NAV-->
      <?php //$this->load->view('template/sidebar'); ?>
      <!-- END LEFT SIDEBAR NAV-->
      <!-- //////////////////////////////////////////////////////////////////////////// -->
      <!-- START CONTENT -->
      <section id="content" class="bg-cp">
         <div id="breadcrumbs-wrapper">
            <div class="container custom">
               <div class="row">
                  <div class="col s10 m6 l6">
                     <h5 class="breadcrumbs-title my-ex">Government Schemes and Policies</h5>
                     <ol class="breadcrumbs">
                        <li><a href="">RESOURCES / GOVT SCHEMES</a>
                     </ol>
                  </div>
               </div>
            </div>
         </div>
         <div class="container custom">
            <div class="row">
               <div class="col l12 s12 m12 mt-2 before-red-bg red-bg-right after-green-bg green-bg-right">
                  <div id="test1" class="col s12 inner_row_container ">
                     <!--  <div class="row document-box-view blueborder"> -->
                     <ul class="collapsible">
                        <li class="row document-box-view redborder">
                                        <div class="collapsible-header">1)	DPIIT Recognition <!--i class="fa fa-caret-down rotate-icon"></i--></div>
                                        <div class="collapsible-body">
                                            <p>Under the Start-up India drive, qualified organizations can get perceived as Start-ups by DPIIT, to get to a large group of tax breaks, simpler consistence, IPR optimizing and more. The Objective of this plan is to lessen the administrative weight on Start-ups and along these lines permitting them to zero in on their business and follow low expenses.</br>
                                                To know more: <a href="https://www.startupindia.gov.in/content/sih/en/startupgov/startup-recognition-page.html" target="_blank">https://www.startupindia.gov.in/content/sih/en/startupgov/startup-recognition-page.html</a></p>
                                        </div>
                                    </li>
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">2)	The Venture Capital Assistance Scheme: Ministry of Agriculture and Farmers Welfare <!--i class="fa fa-caret-down rotate-icon"></i--></div>
                                        <div class="collapsible-body">
                                            <p>This scheme helps agripreneurs to make interests in setting up agribusiness projects through monetary investment. Provides monetary help for planning of bankable Detailed Project Reports (DPRs) through Project Development Facility (PDF).</br>
                                                To know more: <a href="https://www.startupindia.gov.in/content/sih/en/government-schemes/venture-capital-scheme.html" target="_blank">https://www.startupindia.gov.in/content/sih/en/government-schemes/venture-capital-scheme.html</a></p>
                                        </div>
                                    </li>
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">3)	Support for International Patent Protection in Electronics and & Information Technology (SIP-EIT): Ministry Of Electronics & Information Technology <!--i class="fa fa-caret-down rotate-icon"></i--></div>
                                        <div class="collapsible-body">
                                            <p>SIP-EIT is a plan to offer monetary help to MSMEs and Technology Start-up units for worldwide/international patent filing to encourage advancement and recognise their worth and capabilities of worldwide IP alongside catching development openings in ICTE area. </br>
                                                To know more: <a href="https://www.startupindia.gov.in/content/sih/en/government-schemes/international-patent-protection-sip-eit.html#:~:text=Ministry%20Of%20Electronics%20%26%20Information%20Technology,growth%20opportunities%20in%20ICTE%20sector" target="_blank">https://www.startupindia.gov.in/content/sih/en/government-schemes/international-patent-protection-sip-eit.html#:~:text=Ministry%20Of%20Electronics%20%26%20Information%20Technology,growth%20opportunities%20in%20ICTE%20sector</a></p>
                                        </div>
                                    </li>
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">4)	Stand-Up India for Financing SC/ST and/or Women Entrepreneurs: Small Industries Development Bank of India (SIDBI) <!--i class="fa fa-caret-down rotate-icon"></i--></div>
                                        <div class="collapsible-body">
                                            <p>SIP-EIT is a plan to offer monetary help to MSMEs and Technology Start-up units for worldwide/international patent filing to encourage advancement and recognise their worth and capabilities of worldwide IP alongside catching development openings in ICTE area. </br>
                                                To know more: <a href="https://www.startupindia.gov.in/content/sih/en/government-schemes/international-patent-protection-sip-eit.html#:~:text=Ministry%20Of%20Electronics%20%26%20Information%20Technology,growth%20opportunities%20in%20ICTE%20sector" target="_blank">https://www.startupindia.gov.in/content/sih/en/government-schemes/international-patent-protection-sip-eit.html#:~:text=Ministry%20Of%20Electronics%20%26%20Information%20Technology,growth%20opportunities%20in%20ICTE%20sector</a></p>
                                        </div>
                                    </li>	

                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">4)	Stand-Up India for Financing SC/ST and/or Women Entrepreneurs: Small Industries Development Bank of India (SIDBI) </div>
                                        <div class="collapsible-body">
                                            <p>Stand Up India Scheme facilitate bank loans between 10 lakh and 1 crore to at least one scheduled caste (SC) or Scehduled Tribe, borrower and atleast one women per bank branch for setting up a greenfield enterprise. This enterprise may be in manufacturing, services, or in the trading sector. In case of non-individual enterprises at least 51% of the shareholding and controlling stake should be held by either an SC/ST or Woman entrepreneur. 
                                                </br>
                                                To know more: <a href="https://www.startupindia.gov.in/content/sih/en/government-schemes/stand-up-india.html" target="_blank">https://www.startupindia.gov.in/content/sih/en/government-schemes/stand-up-india.html</a></p>
                                        </div>
                                    </li>

                                    <!-- Card 5 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">5)	Single Point Registration Scheme: Ministry of Micro Small & Medium Enterprises</div>
                                        <div class="collapsible-body">
                                            <p>The Government is the single largest buyer of a variety of goods. With a view to increase the share of purchases from the small-scale sector, the Government Stores Purchase Programme was launched in 1955-56. NSIC registers Micro & small Enterprises (MSEs) under Single Point Registration scheme (SPRS) for participation in Government Purchases.
                                                </br>
                                                To know more: <a href="https://www.startupindia.gov.in/content/sih/en/government-schemes/single-point-registration.html" target="_blank">https://www.startupindia.gov.in/content/sih/en/government-schemes/single-point-registration.html</a></p>
                                        </div>
                                    </li>

                                    <!-- Card 6 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">6)	ExtraMural Research or Core Research Grant: Science and Engineering Research Board (SERB) under Ministry of Science & Technology</div>
                                        <div class="collapsible-body">
                                            <p>Extramural Research (EMR) funding scheme of SERB to academic institution, research laboratories and other R&D organizations to carry out basic research in all frontier areas of Science and Engineering. This scheme encourages emerging and eminent scientist in field of science and engineering for individual centric competitive mode of research funding. The Objective of this scheme provides core research support to active researchers to undertake research and development in frontier areas of Science and Engineering.</br>
                                                To know more: <a href="https://www.startupindia.gov.in/content/sih/en/government-schemes/extra-mural-research.html" target="_blank">https://www.startupindia.gov.in/content/sih/en/government-schemes/extra-mural-research.html</a></p>
                                        </div>
                                    </li>

                                    <!-- Card 7 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">7)	High Risk -High Reward Research: Science and Engineering Research Board (SERB) under Department of Science & Technology </div>
                                        <div class="collapsible-body">
                                            <p>This Scheme is for funding High Risk - High Reward Research aims at supporting proposals that are conceptually new and risky, and if successful, expected to have a paradigm shifting influence on the S&T. This may be in terms of formulating new hypothesis, or scientific breakthroughs which aid in emergence of new technologies.</br>
                                                To know more: <a href="https://www.startupindia.gov.in/content/sih/en/government-schemes/high-risk-high-reward.html" target="_blank">https://www.startupindia.gov.in/content/sih/en/government-schemes/high-risk-high-reward.html</a></p>
                                        </div>
                                    </li>

                                    <!-- Card 8 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">8)	IREDA NCEF Refinance Scheme: Indian Renewable Energy Development Agency (IREDA)</div>
                                        <div class="collapsible-body">
                                            <p>The scheme aims to revive the operations of the existing biomass power & small hydro power projects by bringing down the cost of funds for these projects by providing refinance at concessional rates of interest, with funds sourced from the National Clean Energy Fund (NCEF).</br>
                                                To know more: <a href="https://www.startupindia.gov.in/content/sih/en/government-schemes/national-clean-energy-fund-Refinance.html#:~:text=The%20scheme%20aims%20to%20revive,Clean%20Energy%20Fund%20(NCEF)." target="_blank">https://www.startupindia.gov.in/content/sih/en/government-schemes/national-clean-energy-fund-Refinance.html#:~:text=The%20scheme%20aims%20to%20revive,Clean%20Energy%20Fund%20(NCEF).</a></p>
                                        </div>
                                    </li>

                                    <!-- Card 9 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">9)	Dairy Entrepreneurship Development Scheme: National Bank for Agriculture and Rural Development (NABARD)</div>
                                        <div class="collapsible-body">
                                            The department of Animal Husbandry, dairying and fisheries is implementing Dairy Entrepreneurship Development Scheme (DEDS) for generating self-employment opportunities in the dairy sector, covering activities such as enhancement of milk production, procurement, preservation, transportation, processing, and marketing of milk by providing back ended capital subsidy for bankable projects.</br>
                                            To know more: <a href="https://www.startupindia.gov.in/content/sih/en/government-schemes/dairy-entrepreneurship-development-scheme.html" target="_blank">https://www.startupindia.gov.in/content/sih/en/government-schemes/dairy-entrepreneurship-development-scheme.html</a></p>
                                        </div>
                                    </li>

                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">10)	Revamped Scheme of Fund for Regeneration of Traditional Industries (SFURTI): Khadi and Village Industries Commission under Ministry Of MSME </div>
                                        <div class="collapsible-body">
                                            <p>SFURTI is a Scheme of Fund for Regeneration of Traditional Industries to promote Cluster development. KVIC is the nodal Agency for promotion of Cluster development for Khadi as well as for Village Industries products. 
                                                </br>
                                                To know more: <a href="https://www.startupindia.gov.in/content/sih/en/government-schemes/revamped-scheme.html" target="_blank">https://www.startupindia.gov.in/content/sih/en/government-schemes/revamped-scheme.html</a></p>
                                        </div>
                                    </li>

                                    <!-- Card 11 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">11)	Assistance to Professional Bodies & Seminars/Symposia: Department of Science & Technology </div>
                                        <div class="collapsible-body">
                                            <p>The Programme extends partial support on a selective basis, for organizing seminar / symposia/ training programmes / workshops / conferences at national as well as international level. The support is provided to Research Institutes/ Universities/Medical and Engineering Colleges and other Academic Institutes/ Professional Bodies who organize such events for the scientific community to keep them abreast of the latest developments in their specific areas. The support is generally given for encouraging participation of young scientists and research workers in such events and publication of proceedings / abstracts for wider dissemination. The Programme also supports S&T Professional bodies.</br>
                                                To know more: <a href="https://www.startupindia.gov.in/content/sih/en/government-schemes/assistance-to-professional-bodies.html" target="_blank">https://www.startupindia.gov.in/content/sih/en/government-schemes/assistance-to-professional-bodies.html</a></p>
                                        </div>
                                    </li>

                                    <!-- Card 12 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">12)	Multiplier Grants Scheme: Ministry of Electronics and Information Technology </div>
                                        <div class="collapsible-body">
                                            <p>Department of Electronics and Information Technology (DeitY) is implementing Multiplier Grants Scheme (MGS). MGS aims to encourage collaborative R&D between industry and academics/ R&D institutions for development of products and packages. Under the scheme, if industry supports R&D for development of products that can be commercialized at institution level, then the government will also provide financial support that is up to twice the amount provided by industry. </br>
                                                To know more: <a href="https://www.meity.gov.in/content/multiplier-grants-scheme#:~:text=Department%20of%20Electronics%20and%20Information,development%20of%20products%20and%20packages." target="_blank">https://www.meity.gov.in/content/multiplier-grants-scheme#:~:text=Department%20of%20Electronics%20and%20Information,development%20of%20products%20and%20packages.</a></p>
                                        </div>
                                    </li>

                                    <!-- Card 13 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">13)	Scheme to Support IPR Awareness Workshops/Seminars in E&IT Sector: Ministry Of Electronics & Information Technology </div>
                                        <div class="collapsible-body">
                                            <p>Ministry of Electronics and Information Technology has launched a scheme to Support IPR Awareness Workshops/Seminars for sensitizing and disseminating awareness about Intellectual Property Rights among various stakeholders especially in E&IT sector.
                                                </br>
                                                To know more: <a href="https://www.startupindia.gov.in/content/sih/en/government-schemes/scheme-to-support-ipr-awareness.html" target="_blank">https://www.startupindia.gov.in/content/sih/en/government-schemes/scheme-to-support-ipr-awareness.html</a></p>
                                        </div>
                                    </li>

                                    <!-- Card 14 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">14)	Biotechnology Ignition Grant (BIG): Biotechnology Industry Research Assistance Council (BIRAC) </div>
                                        <div class="collapsible-body">
                                            <p>BIRAC’s strategy is to support the numerous exciting ideas which have an unmet need for funding and mentorship. This strategy is fulfilled through a grant funding scheme called Biotechnology Ignition Grant (BIG) which is available to scientist entrepreneurs from research institutes, academia and start-ups. </br>
                                                To know more: <a href="https://www.startupindia.gov.in/content/sih/en/government-schemes/biotechnology-ignition-grant.html" target="_blank">https://www.startupindia.gov.in/content/sih/en/government-schemes/biotechnology-ignition-grant.html</a></p>
                                        </div>
                                    </li>

                                    <!-- Card 15 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">15)	Small Business Innovation Research Initiative (SBIRI): Biotechnology Industry Research Assistance Council (BIRAC)</div>
                                        <div class="collapsible-body">
                                            <p>This Scheme to provide support for early stage, pre-proof-of-concept research in biotechnology by industry. It supports new indigenous technologies particularly those related to societal needs in the healthcare, food and nutrition, agriculture, and other sectors. It nurtures and mentor innovative and emerging technologies/entrepreneurs, to assist new enterprises to forge appropriate linkages with academia and government</br>
                                                To know more: <a href="https://www.startupindia.gov.in/content/sih/en/government-schemes/Small-business-innovation-research-initiative.html" target="_blank">https://www.startupindia.gov.in/content/sih/en/government-schemes/Small-business-innovation-research-initiative.html</a></p>
                                        </div>
                                    </li>

                                    <!-- Card 16 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">16)	Biotechnology Industry Partnership Programme (BIPP): Biotechnology Industry Research Assistance Council (BIRAC)</div>
                                        <div class="collapsible-body">
                                            <p>Biotechnology Industry Partnership Programme (BIPP) is a government partnership with Industries for support on a cost sharing basis for path-breaking research in frontier futuristic technology areas having major economic potential and making the Indian industry globally competitive. It is focused on IP creation with ownership retained by Indian industry and wherever relevant, by collaborating scientists.</br>
                                                To know more: <a href="https://www.startupindia.gov.in/content/sih/en/government-schemes/biotechnology-industry-partnership-programme.html" target="_blank">https://www.startupindia.gov.in/content/sih/en/government-schemes/biotechnology-industry-partnership-programme.html</a> </p>
                                        </div>
                                    </li>

                                    <!-- Card 17 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">17)	Pradhan Mantri Mudra Yojana: Govt. Of India </div>
                                        <div class="collapsible-body">
                                            <p>MUDRA provides refinance to micro business under the Scheme of Pradhan Mantri MUDRA Yojana. The other products are for development support to the sector. The offerings are being targeted across the spectrum of beneficiary segments.</br>
                                                To know more: <a href="https://www.startupindia.gov.in/content/sih/en/government-schemes/micro-units-development-refinance-agency.html" target="_blank">https://www.startupindia.gov.in/content/sih/en/government-schemes/micro-units-development-refinance-agency.html</a></p>
                                        </div>
                                    </li>

                                    <!-- Card 18 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">18)	4E (End to End Energy Efficiency): Small Industries Development Bank of India (SIDBI)</div>
                                        <div class="collapsible-body">
                                            <p>SIDBI launched the 4E Intervention on the occasion of “World Environment Day” to provide technical backstopping and support MSME clients for reducing their power & fuel cost.</br>
                                                To know more: <a href="https://www.startupindia.gov.in/content/sih/en/government-schemes/end-to-end-energy-efficiency.html" target="_blank">https://www.startupindia.gov.in/content/sih/en/government-schemes/end-to-end-energy-efficiency.html</a></p>
                                        </div>
                                    </li>

                                    <!-- Card 19 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">19)	Sustainable Finance Scheme: Small Industries Development Bank of India (SIDBI)</div>
                                        <div class="collapsible-body">
                                            <p>SIDBI has introduced a new scheme called the Sustainable Finance Scheme for funding sustainable development projects that contribute energy efficiency and cleaner production but not covered under the international or bilateral lines of credit. All sustainable development projects such as renewable energy projects, Bureau of Energy Efficiency (BEE) star rating, green microfinance, green buildings and eco-friendly labelling, etc. are applicable for the scope of this scheme.</br>
                                                To know more: <a href="https://www.startupindia.gov.in/content/sih/en/government-schemes/sustainable-finance-scheme.html" target="_blank">https://www.startupindia.gov.in/content/sih/en/government-schemes/sustainable-finance-scheme.html</a></p>
                                        </div>
                                    </li>

                                    <!-- Card 20 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">20)	SIDBI make in india soft loan fund for micro small and medium enterprises (smile): small industries development bank of india (SIDBI)</div>
                                        <div class="collapsible-body">
                                            <p>SIDBI Make in India Loan for Enterprises (SMILE) was also launched by Mr. Jaitley. The Scheme is intended to take forward Government of India’s ‘Make in India’ campaign and help MSMEs take part in the campaign. The objective of the Scheme is to provide soft loan, in the nature of quasi-equity and term loan on relatively soft terms to MSMEs to meet the required debt-equity ratio for establishment of an MSME as also for pursuing opportunities for growth for existing MSMEs.
                                                <!--</p> 
                                                <p>To know more: <a href="https://www.startupindia.gov.in/content/sih/en/government-schemes/sidbi-make-in-india-soft-loan-fund-for-MSME.html" target="_blank">https://www.startupindia.gov.in/content/sih/en/government-schemes/sidbi-make-in-india-soft-loan-fund-for-MSME.html</a>
                                                </p>-->
                                        </div>
                                    </li>

                                    <!-- Card 21 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">21)	Promoting Innovations in Individuals, Start-ups and MSMEs (PRISM): Department of Science & Industrial Research </div>
                                        <div class="collapsible-body">
                                            <p>PRISM scheme aims to support individual innovators which will enable to achieve the agenda of inclusive development. It would provide support to institutions or organizations set up as Autonomous Organization under a specific statute or as a society registered under the Societies Registration Act, 1860 or Indian Trusts Act, 1882 leading to development of state-of-art new technology solutions aimed at helping MSME clusters. 
                                                </br>To know more: <a href=" https://www.startupindia.gov.in/content/sih/en/government-schemes/promoting-innovations-MSME.html" target="_blank"> https://www.startupindia.gov.in/content/sih/en/government-schemes/promoting-innovations-MSME.html</a></p>
                                        </div>
                                    </li>

                                    <!-- Card 22 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">22)	NewGen Innovation and Entrepreneurship Development Centre: Department Of Science & Technology (DST) </div>
                                        <div class="collapsible-body">
                                            <p>The NewGen Innovation and Entrepreneurship Development Centre (NewGen IEDC) is being promoted in educational institutions to develop institutional mechanism to create entrepreneurial culture in S&T academic institutions and to foster techno-entrepreneurship for generation of wealth and employment by S&T persons. The NewGen IEDCs are established in academic institutions (science colleges, engineering colleges, universities, management institutes) having requisite expertise and infrastructure.</br>To know more: <a href="  https://www.startupindia.gov.in/content/sih/en/government-schemes/newgen-innovation-and-entrepreneurship-development-centre.html" target="_blank">  https://www.startupindia.gov.in/content/sih/en/government-schemes/newgen-innovation-and-entrepreneurship-development-centre.html</a></p>
                                        </div>
                                    </li>

                                    <!-- Card 23 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">23)	International Cooperation (IC) Scheme: Ministry of Micro, Small & Medium Enterprises (MSME) </div>
                                        <div class="collapsible-body">
                                            <p>The Scheme, being implemented by the Ministry of MSME. The objective of this scheme are:
                                                Technology infusion and/or upgradation of Indian micro, small and medium enterprises (MSMEs). Promotion and modernisation of MSME</br>To know more: <a href="   https://www.startupindia.gov.in/content/sih/en/government-schemes/international-cooperation-scheme.html" target="_blank">   https://www.startupindia.gov.in/content/sih/en/government-schemes/international-cooperation-scheme.html</a></p>
                                        </div>
                                    </li>

                                    <!-- Card 24 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">24)	Enhancement of Competitiveness in the Indian Capital Goods Sector: Department of Heavy Industry </div>
                                        <div class="collapsible-body">
                                            <p>1.	To encourage technology development through joint participation with Academia, Industry R&D institute and Government and facilitate transfer/acquiring the critical technologies.                                                                                                       </br>2.	To create common physical infrastructure for enhancing the competitiveness of the local industry, enabling it to withstand the import penetration.</br>To know more: <a href="   https://www.startupindia.gov.in/content/sih/en/government-schemes/enhancement-Competitiveness-in-the-capital-goods-sector.html " target="_blank">   https://www.startupindia.gov.in/content/sih/en/government-schemes/enhancement-Competitiveness-in-the-capital-goods-sector.html </a></p>
                                        </div>
                                    </li>

                                    <!-- Card 25 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">25)	Bridge Loan Against Generation-Based Incentive (GBI) Claims: Indian Renewable Energy Development Agency (IREDA) </div>
                                        <div class="collapsible-body">
                                            <p>Generation Based Incentive (GBI) was announced by the Ministry of New and Renewable Energy (MNRE) for Grid Interactive Wind and Solar Power Projects with the main aim to broaden the investor base, facilitate the entry of large Independent Power Producers and to provide level playing field to various classes of investors. The GBI is provided over and above the feed in tariff approved by State utilities</br>To know more: <a href="   https://www.startupindia.gov.in/content/sih/en/government-schemes/bridge-loans-against-generation-scheme.html" target="_blank">   https://www.startupindia.gov.in/content/sih/en/government-schemes/bridge-loans-against-generation-scheme.html</a></p>
                                        </div>
                                    </li>

                                    <!-- Card 26 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">26)	Aspire - A Scheme for Promotion of Innovation, Rural Industries and Entrepreneurship</div>
                                        <div class="collapsible-body">
                                            <p>ASPIRE- was launched to set up a network of technology centres and to set up incubation centres to accelerate entrepreneurship and to promote start-ups for innovation in agricultural industry</br>To know more: <a href="   https://www.startupindia.gov.in/content/sih/en/government-schemes/aspire-scheme-promotion-innovation-entrepreneurship-and-agro-industry.html" target="_blank">   https://www.startupindia.gov.in/content/sih/en/government-schemes/aspire-scheme-promotion-innovation-entrepreneurship-and-agro-industry.html </a>
                                            </p>
                                        </div>
                                    </li>

                                    <!-- Card 27 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">27)	Ayurvedic Biology Program: Science and Engineering Research Board (SERB) under Ministry of Science & Technology </div>
                                        <div class="collapsible-body">
                                            <p>The programme on Ayurvedic Biology was initiated primarily by the office of the Principal Scientific Advisor to the Government of India. The programme, now reconceived, is being implemented by the Science and Engineering Research Board (SERB). Against this background, the SERB constituted a Task Force to promote the application of basic sciences in the investigation of Ayurvedic concepts, procedures, and products, and nurture the discipline of Ayurvedic Biology.</br>
                                                To know more: <a href="   https://www.startupindia.gov.in/content/sih/en/government-schemes/ayurvedic-biology-program.html" target="_blank">  https://www.startupindia.gov.in/content/sih/en/government-schemes/ayurvedic-biology-program.html </a>
                                            </p>
                                        </div>
                                    </li>

                                    <!-- Card 28 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">28) Technology Development Programme: Department of Science and Technology Under Ministry of Science & Technology</div>
                                        <div class="collapsible-body">
                                            <p>Technology Development Programmes (TDP) is to convert proof-of-concepts for development of pre-competitive/commercial technologies/ techniques/ processes. Proposals of incremental R&D over the existing technologies may be considered for support. Only full-fledged complete proposals for development of technology/process/product will be considered under TSDP. Projects related to design and development of Software/IT, as required for products and processes, as a part of technology development project shall be considered.
                                                </br>To know more: <a href="   https://www.startupindia.gov.in/content/sih/en/government-schemes/technology-development-programme.html" target="_blank">   https://www.startupindia.gov.in/content/sih/en/government-schemes/technology-development-programme.html </a>
                                            </p>
                                        </div>
                                    </li>

                                    <!-- Card 29 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">29)	Software Technology Park Scheme: Software Technology Parks of India Under Ministry of Electronics and Information Technology </div>
                                        <div class="collapsible-body">
                                            <p>The Software Technology Park (STP) Scheme is a 100 percent Export Oriented Scheme for the development and export of computer software, including export of professional services using communication links or physical media
                                                This scheme is unique in its nature as it focuses on one product / sector, i.e. computer software. The scheme integrates the government concept of 100 percent Export Oriented Units (EOU) and Export Processing Zones (EPZ) and the concept of Science Parks / Technology Parks, as operating elsewhere in the world</br>
                                                To know more: <a href="    https://www.startupindia.gov.in/content/sih/en/government-schemes/software-technology-park-scheme.html" target="_blank">https://www.startupindia.gov.in/content/sih/en/government-schemes/software-technology-park-scheme.html </a>
                                            </p>
                                        </div>
                                    </li>

                                    <!-- Card 30 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">30)	Virasat - A Credit Scheme for Craftpersons </div>
                                        <div class="collapsible-body">
                                            <p>The purpose of this scheme is to help Artisans and Craftsmen in meeting their working capital (making products as per the demand in the market) and fixed capital requirement (machineries, equipment, and tools).</br>
                                                To know more: <a href="    https://www.startupindia.gov.in/content/sih/en/government-schemes/virasat_credit_scheme_for_craftpersons.html" target="_blank">https://www.startupindia.gov.in/content/sih/en/government-schemes/virasat_credit_scheme_for_craftpersons.html </a>
                                            </p>
                                        </div>
                                    </li>

                                    <!-- Card 31 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">31)	Coir Udyami Yojana (CUY): Coir Board </div>
                                        <div class="collapsible-body">
                                            <p>This is a credit linked subsidy scheme for setting up of coir units with project cost upto Rs.10 lakhs plus one cycle of working capital, which shall not exceed 25% of the project cost. Working capital will not be considered for subsidy. The scheme main objective is to facilitate sustainable development of the Coir Industry in the country and is under the ambit of Minsitry of MSME.</br>
                                                To know more: <a href="     https://www.startupindia.gov.in/content/sih/en/government-schemes/coir_udyami_yojana.html" target="_blank"> https://www.startupindia.gov.in/content/sih/en/government-schemes/coir_udyami_yojana.html </a>
                                            </p>
                                        </div>
                                    </li>

                                    <!-- Card 32 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">32)	Infomediary Services National Small Industries Corporation (Ministry of Micro, Small and Medium Enterprises)</div>
                                        <div class="collapsible-body">
                                            <p>The start-ups require intensive networking and information about other business ventures. This scheme provides information on business, technology, and finance to MSMEs, and exhibit core competencies of Indian SMEs. This is done through MSME Global mart - www.msmemart.com. This information is available through annual membership of the portal.
                                                A free membership plan is also provided with limited access and services and a Gold membership plan for SC/STs is also provided with full access and services, which is free of charge.</br>
                                                To know more: <a href="     https://www.startupindia.gov.in/content/sih/en/government-schemes/informediary_services_msme.html" target="_blank"> https://www.startupindia.gov.in/content/sih/en/government-schemes/informediary_services_msme.html </a>
                                            </p>
                                        </div>
                                    </li>

                                    <!-- Card 33 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">33)	Raw Material Assistance Scheme National Small Industries Corporation (Ministry of Micro, Small and Medium Enterprises)</div>
                                        <div class="collapsible-body">
                                            <p>Raw Material Assistance Scheme aims at helping MSMEs by way of financing the purchase of Raw Material (both indigenous & imported). This gives an opportunity to MSMEs to focus better on manufacturing quality products.</br>
                                                To know more: <a href="     https://www.startupindia.gov.in/content/sih/en/government-schemes/raw_material_assistance.html" target="_blank"> https://www.startupindia.gov.in/content/sih/en/government-schemes/raw_material_assistance.html </a>
                                            </p>
                                        </div>
                                    </li>

                                    <!-- Card 34 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">34)	Coir Vikas Yojana Coir Board (Ministry of Micro, Small and Medium Enterprises) </div>
                                        <div class="collapsible-body">
                                            <p>The scheme facilitates development of domestic and export markets, skill development and training, empowerment of women, employment/entrepreneurship creation and development, enhanced rawcmaterial utilization, trade related services, welfare activities for the coir workers, etc. This is undertaken through following 6 schemes:</br>
                                                •	Skill Upgradation and Mahila Coir Yojana</br>
                                                •	Export Market Promotion (EMP)</br>
                                                •	Development of Production Infrastructure (DPI) </br>
                                                •	Domestic Market Promotion (DMP)</br>
                                                •	Trade and Industry Related Functional Support Services (TIRFSS)</br>
                                                •	Welfare Measure (Group Personal Accident Insurance Scheme)</br>
                                            </p>
                                            <p>To know more: <a href="     https://www.startupindia.gov.in/content/sih/en/government-schemes/coir_vikas_yojana.html" target="_blank"> https://www.startupindia.gov.in/content/sih/en/government-schemes/coir_vikas_yojana.html </a>
                                            </p>
                                        </div>
                                    </li>

                                    <!-- Card 35 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">35)	Credit Facilitation Through Bank National Small Industries Corporation (Ministry of Micro, Small and Medium Enterprises)</div>
                                        <div class="collapsible-body">
                                            <p>To meet the credit requirements of MSME units, NSIC has entered into a Memorandum of Understanding with various Nationalized and Private Sector Banks. Through association with these banks, NSIC arranges for credit support (fund or non fund based limits) from banks for the MSMEs.</p>
                                            <p>To know more: <a href="     startupindia.gov.in/content/sih/en/government-schemes/sidbi-make-in-india-soft-loan-fund-for-MSME.html" target="_blank"> startupindia.gov.in/content/sih/en/government-schemes/sidbi-make-in-india-soft-loan-fund-for-MSME.html </a>
                                            </p>
                                        </div>
                                    </li>

                                    <!-- Card 36 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">36)	NSIC Infrastructure Scheme - IT Incubator National Small Industries Corporation (Ministry of Micro, Small and Medium Enterprises) </div>
                                        <div class="collapsible-body">
                                            <p>The scheme aims at creating sustainable entrepreneurship development in the area of Information and Communication Technology (ICT) especially first-generation entrepreneurs by fostering nurturing the innovative ideas to commercially viable business prepositions. Entrepreneurs harness the expertise of NSIC in extending hand holding of start-up companies to become successful small enterprises. The scheme also caters to networking between R&D and Industry beneficiaries to create successful commercial ventures.</p>
                                            <p>To know more: <a href="     https://www.startupindia.gov.in/content/sih/en/government-schemes/infrastructure_scheme_incubator.html" target="_blank"> https://www.startupindia.gov.in/content/sih/en/government-schemes/infrastructure_scheme_incubator.html </a>
                                            </p>
                                        </div>
                                    </li>

                                    <!-- Card 37 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">37)	National Awards (Individual MSEs) Ministry of Micro, Small & Medium Enterprises</div>
                                        <div class="collapsible-body">
                                            <p>The Ministry of MSME with a view to recognising the efforts and contribution of MSMEs, givesNational Awards annually to selectedentrepreneurs and enterprises under the scheme of National Awards.</p>
                                            <p>To know more: <a href="     https://www.startupindia.gov.in/content/sih/en/government-schemes/national_awards_for_mse.html" target="_blank"> https://www.startupindia.gov.in/content/sih/en/government-schemes/national_awards_for_mse.html </a>
                                            </p>
                                        </div>
                                    </li>

                                    <!-- Card 38 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">38)	Marketing Assistance Scheme Ministry of Micro, Small and Medium Enterprises </div>
                                        <div class="collapsible-body">
                                            <p>The various objectives of the scheme are as follow:</br>
                                                •	To enhance marketing capabilities & competitiveness of the MSMEs. </br>
                                                •	To showcase the competencies of MSMEs.</br>
                                                •	To update MSMEs about the prevalent market scenario and its impact on their activities. </br>
                                                •	To facilitate the formation of consortia of MSMEs for marketing of their products and services.</br> 
                                                •	To provide platform to MSMEs for interaction with large institutional buyers.</br>
                                                •	To disseminate/ propagate various programmes of the Government. </br>
                                                •	To enrich the marketing skills of the micro, small & medium entrepreneurs. </br>
                                            </p>
                                            <p>To know more: <a href="     https://www.startupindia.gov.in/content/sih/en/government-schemes/marketing_assistance_scheme.html" target="_blank"> https://www.startupindia.gov.in/content/sih/en/government-schemes/marketing_assistance_scheme.html </a>
                                            </p>
                                        </div>
                                    </li>

                                    <!-- Card 39 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">39)	Procurement and Marketing Support Scheme (P&MS) Ministry of Micro, Small and Medium Enterprises </div>
                                        <div class="collapsible-body">
                                            <p>The Procurement and Marketing support Scheme would promote and educate MSMEs by organising trade fairs/awareness programs about GeM portal, Online services, and other services. The scheme also encourages Micro and Small Enterprises to develop domestic markets and find new ways of promotion of new market access initiatives. It will also cover activities required to facilitate market linkages for effective implementation of Public Procurement Policy for MSEs Order of 2012.</p>
                                            <p>To know more: <a href="     https://www.startupindia.gov.in/content/sih/en/government-schemes/procurement_and_marketing_support_scheme.html" target="_blank"> https://www.startupindia.gov.in/content/sih/en/government-schemes/procurement_and_marketing_support_scheme.html </a>
                                            </p>
                                        </div>
                                    </li>

                                    <!-- Card 40 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">40)	National Manufacturing Competitiveness Programme (NMCP) Ministry of Micro, Small & Medium Enterprises</div>
                                        <div class="collapsible-body">
                                            <p>The National Manufacturing Competitiveness Council (NMCC) has finalised a five-year national manufacturing programme. Ten schemes have been drawn up including schemes for promotion of ICT, mini tool room, design clinics, and marketing support for SMEs. Implementation will be in PPP mode, and financing will be tied up. New ZED certification (Zero Defect and Zero Effect Financial support) is also provided under this scheme.</p>
                                            <p>To know more: <a href="     https://www.startupindia.gov.in/content/sih/en/government-schemes/national_manufacturing_competitiveness_programme.html" target="_blank"> https://www.startupindia.gov.in/content/sih/en/government-schemes/national_manufacturing_competitiveness_programme.html </a>
                                            </p>
                                        </div>
                                    </li>

                                    <!-- Card 41 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">41)	Performance & Credit Rating Scheme Ministry of Micro, Small & Medium Enterprises</div>
                                        <div class="collapsible-body">
                                            <p>This scheme seeks to establish independent, trusted third party opinion on capabilities and creditworthiness of MSEs, and makes credit available at attractive interest rates and will ensure better productivity. Under this scheme (as per the turnover of the MSE) a percent of Rating Agency charges is reimbursed by Ministry of SSI.</p>
                                            <p>To know more: <a href="     https://www.startupindia.gov.in/content/sih/en/government-schemes/performance_and_credit_rating_scheme.html" target="_blank"> https://www.startupindia.gov.in/content/sih/en/government-schemes/performance_and_credit_rating_scheme.html </a>
                                            </p>
                                        </div>
                                    </li>

                                    <!-- Card 42 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">42)	Marketing Development Assistance Ministry of Tourism </div>
                                        <div class="collapsible-body">
                                            <p>Ministry of tourism through this particular aims to project India as a multidimensional tourist destination by inviting the foreign media (stakeholders) to promote the Brand 'India'. This scheme also provides financial assistance to local tourist operators/agencies/travel startups/hoteliers to promote by various activities in NE states and Jammu and Kashmir. The scheme also caters to promotion of unexploited and less visited places. </p>
                                            <p>To know more: <a href="     https://www.startupindia.gov.in/content/sih/en/government-schemes/marketing_development_assistance.html" target="_blank"> https://www.startupindia.gov.in/content/sih/en/government-schemes/marketing_development_assistance.html </a>
                                            </p>
                                        </div>
                                    </li>

                                    <!-- Card 43 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">43)	MARKETING DEVELOPMENT ASSISTANCE (MDA) SCHEME for Medical Tourism/Wellness Tourism Service Providers </div>
                                        <div class="collapsible-body">
                                            <p>This scheme can be leveraged by small enterprises that are venturing into Medical Tourism and Wellness Industry. Financial support under the MDA Scheme will be provided to approved medical tourism service providers, i.e. Representatives of Hospitals accredited by Joint Commission for International Accredited Hospitals (JCI) and National Accreditation Board of Hospitals (NABH) and Medical Tourism facilitators (Travel Agents/Tour Operators approved by Ministry of Tourism, Government of India and engaged in Medical Tourism. </p>
                                            <p>To know more: <a href="      https://www.startupindia.gov.in/content/sih/en/government-schemes/marketing_development_assistance_for_medical_tourism.html" target="_blank">  https://www.startupindia.gov.in/content/sih/en/government-schemes/marketing_development_assistance_for_medical_tourism.html </a>
                                            </p>
                                        </div>
                                    </li>

                                    <!-- Card 44 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">44)	Assistance under Scheme for setting up Tea Boutiques in India for Domestic Promotion Tea Board India (Ministry of Commerce and Industry) </div>
                                        <div class="collapsible-body">
                                            <p>In order to showcase Tea's wide varieties at one place and excite the customers especially youth about the qualitative nuances and the correct brewing method in a magnificent & aesthetic ambience, the concept of “Tea Boutique” has been proposed by the tea board which is under the Ministry of Commerce and Industry. The Tea Board  will handhold a prospective entrepreneur for three (3) years in terms of financial assistance for setting up the Tea Boutique.</p>
                                            <p>To know more: <a href="       https://www.startupindia.gov.in/content/sih/en/government-schemes/assistance_for_setting_up_tea_boutiques.html" target="_blank">   https://www.startupindia.gov.in/content/sih/en/government-schemes/assistance_for_setting_up_tea_boutiques.html </a>
                                            </p>
                                        </div>
                                    </li>

                                    <!-- Card 45 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">45)	Swarojgar Credit Card NABARD with Ministry of Finance </div>
                                        <div class="collapsible-body">
                                            <p>Swarojgar Credit Card (SCC) Scheme was introduced in September 2003 for providing adequate, timely and uninterrupted credit, i.e., working capital including consumption needs and/or block capital requirements to the small artisans, handloom weavers and other self -employed persons including micro-entrepreneurs, SHGs, etc., from the banking system in a flexible, hassle free and cost-effective manner.</p>
                                            <p>To know more: <a href="       https://www.startupindia.gov.in/content/sih/en/government-schemes/swarojgar_credit_card.html" target="_blank">   https://www.startupindia.gov.in/content/sih/en/government-schemes/swarojgar_credit_card.html </a>
                                            </p>
                                        </div>
                                    </li>

                                    <!-- Card 46 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">46)	Material Testing Labs - National Small Industries Corporation (Ministry of Micro, Small and Medium Enterprises) </div>
                                        <div class="collapsible-body">
                                            <p>Since MSMEs and entrepreneurs are involved in pleathora of testing new products and are always in a quest to achieve international standards of excellence. To facilitate their endeavours, NSIC launched Testing Labs Scheme by setting various testing laboratories accredited by NABL / BIS, provides material and product testing, energy audit, facilities, thus, enhancing their overall competitiveness. These Centre are equipped with the State of the Art indigenous equipments for carrying out performance and acceptance tests in the fields of chemical, material, electrical, motor & pumps, metallurgical etc.</p>
                                            <p>To know more: <a href="       https://www.startupindia.gov.in/content/sih/en/government-schemes/material_testing_labs.html" target="_blank">   https://www.startupindia.gov.in/content/sih/en/government-schemes/material_testing_labs.html </a>
                                            </p>
                                        </div>
                                    </li>

                                    <!-- Card 47 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">47)	Self Employment and Talent Utilisation (SETU) Niti Aayog </div>
                                        <div class="collapsible-body">
                                            <p>SETU will be a Techno-Financial, Incubation and Facilitation Programme to support all aspects of start up businesses, and other self-employment activities, particularly in technology-driven areas. It aims to create around 100,000 jobs through start-ups.</p>
                                            <p>To know more: <a href="       https://www.startupindia.gov.in/content/sih/en/government-schemes/setu_scheme.html" target="_blank">   https://www.startupindia.gov.in/content/sih/en/government-schemes/setu_scheme.html </a>
                                            </p>
                                        </div>
                                    </li>

                                    <!-- Card 48 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">48)	Micro Exporters Policy (MEP) ECGC Ltd. (Ministry of Commerce & Industry)</div>
                                        <div class="collapsible-body">
                                            <p>The Small Exporter's Policy is basically the Standard Policy, incorporating certain improvements in terms of cover, in order to encourage small exporters to obtain and operate freely without the different type of risks like Political and Commercial. It is issued to exporters whose anticipated export turnover for the period of one year does not exceed INR 1 crores. </p>
                                            <p>To know more: <a href="       https://www.startupindia.gov.in/content/sih/en/government-schemes/micro_exporters_policy.html" target="_blank">   https://www.startupindia.gov.in/content/sih/en/government-schemes/micro_exporters_policy.html </a>
                                            </p>
                                        </div>
                                    </li>

                                    <!-- Card 49 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">49)	Export Development & Promotion of Spices Spices Board (Ministry of Commerce & Industry) </div>
                                        <div class="collapsible-body">
                                            <p>The programmes under the scheme ‘Export Development and Promotion’ aims to support exporters to adopt high tech processing technologies or to upgrade existing level of technologies for high end value addition and to develop capabilities to meet the changing food safety standards in the importing countries. The scheme also caters to promoting the Indian spices exporter by assisting them in promotion of Indian Goods in International Trade Fairs, Seminars, etc. by reimbursing their  expenditure on the same.</p>
                                            <p>To know more: <a href="       https://www.startupindia.gov.in/content/sih/en/government-schemes/export_development_promotion_of_spices.html" target="_blank">   https://www.startupindia.gov.in/content/sih/en/government-schemes/export_development_promotion_of_spices.html </a>
                                            </p>
                                        </div>
                                    </li>

                                    <!-- Card 50 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">50)	ISO 9000/ISO 14001 Certification Reimbursement Ministry of Micro, Small and Medium Enterprises </div>
                                        <div class="collapsible-body">
                                            <p>In order to enhance the competitive strength of SMEs, the scheme provides financial incentives to those SMEs/ancillary undertakings that have acquired ISO 9000/ISO 14001/HACCP certification through reimbursement of the expenditure incurred. The scheme is enlarged so as to include reimbursement of expenses in the acquisition of ISO 14001 certification.</p>
                                            <p>To know more: <a href="       https://www.startupindia.gov.in/content/sih/en/government-schemes/reimbursement_iso_standards.html" target="_blank">   https://www.startupindia.gov.in/content/sih/en/government-schemes/reimbursement_iso_standards.html </a>
                                            </p>
                                        </div>
                                    </li>

                                    <!-- Card 51 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">51)	Marketing Support/Assistance to MSMEs (Bar Code) Ministry of Micro, Small and Medium Enterprises</div>
                                        <div class="collapsible-body">
                                            <p>Under this scheme the Ministry conducts seminars and reimburses registration fees for bar coding in order to encourage MSEs to use bar-codes.
                                                The basic objective of this scheme is to enhance the Marketing competitiveness of Micro & Small Enterprises (MSEs) by way of: </br>
                                                1.	Providing 75% of one time registration fee and annual recurring fee (for first three years) paid by MSEs to GS1 India (GS1 India, an autonomous body under Ministry of Commerce & Industry, Government of India is a solution provider for registration for use of Bar Coding. To become a subscriber of GS1 India, all one has to do is fill up the subscription enquiry or registration form and make the necessary payments as registration fee. Details about registration with GS1 India for use of Bar Coding are available on their website www.gs1india.org).</br>
                                                2.	Popularising the adoption of bar codes on large scale amongst MSEs, </br>
                                                3.	Motivating and encouraging MSEs for use of bar codes through conducting seminars on Bar Code, etc.
                                            </p>
                                            <p>To know more: <a href="        https://www.startupindia.gov.in/content/sih/en/government-schemes/bar_code.html" target="_blank">    https://www.startupindia.gov.in/content/sih/en/government-schemes/bar_code.html </a>
                                            </p>
                                        </div>
                                    </li>

                                    <!-- Card 52 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">52)	Lean Manufacturing Competitiveness for MSMEs Ministry of Micro, Small and Medium Enterprises</div>
                                        <div class="collapsible-body">
                                            <p>Under the Scheme, MSMEs will be assisted in reducing their manufacturing costs, through proper personnel management, better space utilization, scientific inventory management, improved processed flows, reduced engineering time and so on. LMCS (Lean Manufacturing Competitiveness Scheme) also brings improvement in the quality of products and lowers costs, which are essential for competing in national and international markets. </p>
                                            <p>To know more: <a href="        https://www.startupindia.gov.in/content/sih/en/government-schemes/lean_manufacturing_competitiveness.html" target="_blank"> https://www.startupindia.gov.in/content/sih/en/government-schemes/lean_manufacturing_competitiveness.html </a>
                                            </p>
                                        </div>
                                    </li>

                                    <!-- Card 53 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">53)	Financial Support to MSMEs in ZED Certification Scheme Ministry of Micro, Small and Medium Enterprises </div>
                                        <div class="collapsible-body">
                                            <p>The scheme envisages promotion of Zero Defect and Zero Effect (ZED) manufacturing amongst MSMEs and ZED Assessment for their certification so as to:</br>
                                                •	Develop an Ecosystem for Zero Defect Manufacturing in MSMEs.</br>
                                                •	Promote adaptation of Quality tools/systems and Energy Efficient manufacturing.</br>
                                                •	Enable MSMEs for manufacturing of quality products.</br>
                                                •	Encourage MSMEs to constantly upgrade their quality standards in products and processes.</br>
                                                •	Drive manufacturing with adoption of Zero Defect production processes and without impacting the environment</br>
                                                •	Support ‘Make in India’ campaign.</br>
                                                •	Develop professionals in the area of ZED manufacturing and certification.
                                            </p>
                                            <!--<p>To know more: <a href="         https://www.startupindia.gov.in/content/sih/en/government-schemes/zed_certification.html" target="_blank">  https://www.startupindia.gov.in/content/sih/en/government-schemes/zed_certification.html </a>
                                                    </p>-->
                                        </div>
                                    </li>

                                    <!-- Card 54 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">54)	Entrepreneurial and Managerial Development of SMEs through Incubators Ministry of Micro, Small and Medium Enterprises</div>
                                        <div class="collapsible-body">
                                            <p>There are multiple objectives of the scheme, some of which are:</br>
                                                •	To promote emerging technological and knowledge based innovative ventures that seek the nurturing of ideas from professionals.</br>
                                                •	To promote and support untapped creativity of individual innovators and also to assist individual innovators to become technology based entrepreneurs.</br>
                                                •	To promote networking and forging of linkages with other constituents of the innovation chain for commercialization of their developments.
                                            </p>
                                            <p>To know more: <a href="         https://www.startupindia.gov.in/content/sih/en/government-schemes/entrepreneurial_managerial_development.html" target="_blank"> https://www.startupindia.gov.in/content/sih/en/government-schemes/entrepreneurial_managerial_development.html </a>
                                            </p>
                                        </div>
                                    </li>

                                    <!-- Card 55 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">55)	Amended Technology Upgradation Fund Scheme (ATUFS) Ministry of Textiles </div>
                                        <div class="collapsible-body">
                                            <p>With Aim of 'Make in India' and 'Zero Defect and Zero Effect' in manufacturing, the government provides credit linked capital investment subsidy. This scheme would facilitate augmenting of investment, productivity, quality, employment, exports and import subsitution in textile industry. It will also indirectly promote investment in textile machinery manufacturing.
                                            </p>
                                            <p>To know more: <a href="         https://www.startupindia.gov.in/content/sih/en/government-schemes/amended_technology_upgradation_fund_scheme.html" target="_blank"> https://www.startupindia.gov.in/content/sih/en/government-schemes/amended_technology_upgradation_fund_scheme.html </a>
                                            </p>
                                        </div>
                                    </li>

                                    <!-- Card 56 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">56)	Composite Loan Ministry of Micro, Small and Medium Entreprises </div>
                                        <div class="collapsible-body">
                                            <p>The Scheme envisages sanction and disbursement of working capital and term loan together from a single agency. The limit for composite loans has been enhanced to Rs. 25 lakhs in the Comprehensive Policy Package.
                                                The Scheme is operated both by banks and financial institutions. State Financial Corporations under Single Window Scheme provide working capital loan along with term loan to new tiny and small- scale sector units to overcome the initial difficulties and delays faced by them to start production expeditiously.
                                            </p>
                                            <p>To know more: <a href="          https://www.startupindia.gov.in/content/sih/en/government-schemes/composite_loan.html" target="_blank">  https://www.startupindia.gov.in/content/sih/en/government-schemes/composite_loan.html </a>
                                            </p>
                                        </div>
                                    </li>

                                    <!-- Card 57 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">57)	Venture Capital by SIDBI Venture Capital Ltd. (SVLC): Small Industries Development Bank of India (SIDBI)</div>
                                        <div class="collapsible-body">
                                            <p>The objective of the scheme is to work for best returns by investing in deserving entrepreneurial teams using a combination of capital, strategic mentoring, skills, and our vast network of relationships. Under this scheme Investment is made by way of equity and equity type instruments. Financial structuring is done on a case-to-case basis keeping in view factors like risk perception, growth potential, equity base and market condition. 
                                            </p>
                                            <p>To know more: <a href="https://www.startupindia.gov.in/content/sih/en/government-schemes/SVLC.html" target="_blank">  https://www.startupindia.gov.in/content/sih/en/government-schemes/SVLC.html </a>
                                            </p>
                                        </div>
                                    </li>

                                    <!-- Card 58 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">58)	Export Promotion Capital Goods (EPCG) scheme Ministry of Electronics and Information Technology </div>
                                        <div class="collapsible-body">
                                            <p>The Zero duty EPCG Scheme is available to exporters of electronic products. It allows import of capital goods for pre-production, production and post-production (including CKD/SKD thereof as well as computer software systems) at zero% customs duty, subject to an export obligation equivalent to 6 times of duty saved on capital goods imported under EPCG scheme, to be fulfilled in 6 years reckoned from Authorization issue-date. 
                                            </p>
                                            <p>To know more: <a href="https://www.startupindia.gov.in/content/sih/en/government-schemes/epcg.html" target="_blank">  https://www.startupindia.gov.in/content/sih/en/government-schemes/epcg.html </a>
                                            </p>
                                        </div>
                                    </li>

                                    <!-- Card 59 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">59)	Central Sector Scheme for Promotion of International Co-operation in AYUSH Ministry of AYUSH </div>
                                        <div class="collapsible-body">
                                            <p>To promote and strengthen awareness and interest about AYUSH Systems of Medicine and to facilitate International promotion, development and recognition of Ayurveda, Yoga, Naturopathy, Unani, Siddha, Sowa-Rigpa and Homoeopathy.  
                                            </p>
                                            <p>To know more: <a href="https://www.startupindia.gov.in/content/sih/en/government-schemes/ayush_international_cooperation.html" target="_blank">  https://www.startupindia.gov.in/content/sih/en/government-schemes/ayush_international_cooperation.html </a>
                                            </p>
                                        </div>
                                    </li>

                                    <!-- Card 60 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">60)	Credit Guarantee Fund Trust for Micro and Small Enterprises (CGTMSE) Small Industries Development Bank of India (SIDBI) and Ministry of Micro, Small and Medium Enterprises </div>
                                        <div class="collapsible-body">
                                            <p>The Credit Guarantee Fund Scheme for Micro and Small Enterprises (CGS) was launched by the Government of India (GoI) to make available collateral-free credit to the micro and small enterprise sector. Both the existing and the new enterprises are eligible to be covered under the scheme. The Ministry of Micro, Small and Medium Enterprises, GoI and Small Industries Development Bank of India (SIDBI), established a Trust named Credit Guarantee Fund Trust for Micro and Small Enterprises (CGTMSE) to implement the Credit Guarantee Fund Scheme for Micro and Small Enterprises. </p>
                                            <p>To know more: <a href=" https://www.startupindia.gov.in/content/sih/en/government-schemes/cgtsme.html" target="_blank"> https://www.startupindia.gov.in/content/sih/en/government-schemes/cgtsme.html </a>
                                            </p>
                                        </div>
                                    </li>

                                    <!-- Card 61 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">61)	Interest Subsidy Eligibility Certificate (ISEC) for Khadi Institutions Ministry of Micro, Small and Medium Enterprises (KVIC) </div>
                                        <div class="collapsible-body">
                                            <p>The Interest Subsidy Eligibility Certificate (ISEC) Scheme is an important mechanism of funding khadi programme undertaken by khadi institutions. It was introduced to mobilise funds from banking institutions for filling the gap between the actual fund requirements and availability of funds from budgetary sources.</p>
                                            <p>To know more: <a href="https://www.startupindia.gov.in/content/sih/en/government-schemes/isec_khadi.html" target="_blank"> https://www.startupindia.gov.in/content/sih/en/government-schemes/isec_khadi.html </a>
                                            </p>
                                        </div>
                                    </li>

                                    <!-- Card 62 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">62)	The Design Clinic Scheme for MSMEs Ministry of Micro, Small and Medium Enterprises </div>
                                        <div class="collapsible-body">
                                            <p>Design Clinic Scheme for Design Expertise to MSME manufacturing sector.
                                                The main objectives of the scheme are:</br>
                                                • To create a sustainable design eco system for the MSME sector through continuous learning and skill development </br>
                                                • Bring the industrial design fraternity closer to the MSME Sector </br>
                                                • Develop an institutional base for the industry’s design requirement; </br>
                                                • Increase the awareness of the value of design and establish design learning in the MSME </br>
                                                • Increase the competitiveness of local products and services through design.
                                            </p>
                                            <p>To know more: <a href="https://www.startupindia.gov.in/content/sih/en/government-schemes/design_msme.html" target="_blank"> https://www.startupindia.gov.in/content/sih/en/government-schemes/design_msme.html </a>
                                            </p>
                                        </div>
                                    </li>

                                    <!-- Card 63 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">63)	Science and Technology (S&T) for Coir Institutions Ministry of Micro, Small and Medium Enterprises </div>
                                        <div class="collapsible-body">
                                            <p>Technology Transfer, Incubation, Testing, Training Entrepreneurs and Service Facilities for the coir MSMEs through extension of the outcomes of research (done at research institutes under the scheme) at the laboratory level for application at the field level and extension of testing and service facility are the objectives of the scheme. </p>
                                            <p>To know more: <a href="https://www.startupindia.gov.in/content/sih/en/government-schemes/Science_and_technology.html" target="_blank"> https://www.startupindia.gov.in/content/sih/en/government-schemes/Science_and_technology.html </a>
                                            </p>
                                        </div>
                                    </li>

                                    <!-- Card 64 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">64)	Agro Processing Cluster Scheme Ministry of Food Processing Industries </div>
                                        <div class="collapsible-body">
                                            <p>The scheme aims at development of modern infrastructure and common facilities to encourage group of entrepreneurs to set up food processing units based on cluster approach by linking groups of producers/ farmers to the processors and markets through well-equipped supply chain with modern infrastructure. </p>
                                            <p>To know more: <a href="https://www.startupindia.gov.in/content/sih/en/government-schemes/agro_scheme.html" target="_blank"> https://www.startupindia.gov.in/content/sih/en/government-schemes/agro_scheme.html </a>
                                            </p>
                                        </div>
                                    </li>

                                    <!-- Card 65 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">65)	Research & Development In Processed Food Sector Ministry of Food Processing Industries </div>
                                        <div class="collapsible-body">
                                            <p>Under the scheme, the Ministry of Food Processing Industries has been extending financial assistance to undertake demand driven R&D work for the benefit food processing industry in terms of product and process development, efficient technologies, improved packaging, value addition etc. with commercial value along with standardization of various factors viz. additives, colouring agents, preservatives, pesticide residues, chemical contaminants, microbiological contaminants and naturally occurring toxic substances within permissible limits.</p>
                                            <p>To know more: <a href="https://www.startupindia.gov.in/content/sih/en/government-schemes/research_development_foodprocessing.html" target="_blank"> https://www.startupindia.gov.in/content/sih/en/government-schemes/research_development_foodprocessing.html </a>
                                            </p>
                                        </div>
                                    </li>

                                    <!-- Card 66 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">66)	Creation / Expansion of Food Processing & Preservation Capacities Ministry of Food Processing Industries </div>
                                        <div class="collapsible-body">
                                            <p>The main objective of the scheme is creation of processing and preservation capacities and modernisation/ expansion of existing food processing units with a view to increasing the level of processing, value addition leading to reduction of wastage. </p>
                                            <p>To know more: <a href=" https://www.startupindia.gov.in/content/sih/en/government-schemes/cefppc.html" target="_blank">  https://www.startupindia.gov.in/content/sih/en/government-schemes/cefppc.html </a>
                                            </p>
                                        </div>
                                    </li>

                                    <!-- Card 67 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">67)	The Women Entrepreneurship Platform (WEP) Niti Aayog </div>
                                        <div class="collapsible-body">
                                            <p>NITI Aayog has launched a Women Entrepreneurship Platform (WEP) for providing an ecosystem for budding & existing women entrepreneurs across the country. SIDBI has partnered with NITI Aayog to assist in this initiative.</p>
                                            <p>To know more: <a href=" https://www.startupindia.gov.in/content/sih/en/government-schemes/Wep.html" target="_blank">  https://www.startupindia.gov.in/content/sih/en/government-schemes/Wep.html </a>
                                            </p>
                                        </div>
                                    </li>

                                    <!-- Card 67 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">67)	The Women Entrepreneurship Platform (WEP) Niti Aayog </div>
                                        <div class="collapsible-body">
                                            <p>NITI Aayog has launched a Women Entrepreneurship Platform (WEP) for providing an ecosystem for budding & existing women entrepreneurs across the country. SIDBI has partnered with NITI Aayog to assist in this initiative.</p>
                                            <p>To know more: <a href=" https://www.startupindia.gov.in/content/sih/en/government-schemes/Wep.html" target="_blank">  https://www.startupindia.gov.in/content/sih/en/government-schemes/Wep.html </a>
                                            </p>
                                        </div>
                                    </li>

                                    <!-- Card 68 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">68)	National Urban Livelihoods Mission Ministry of Hosing and Urban Affairs </div>
                                        <div class="collapsible-body">
                                            <p>NULM means National Urban Livelihoods Mission. To reduce poverty and vulnerability of the urban poor households by enabling them to access gainful self-employment and skilled wage employment opportunities, resulting in an appreciable improvement in their livelihoods on a sustainable basis, through building strong grassroots level institutions of the poor and ultimately linking them to the entrepreneurial and the startup revolution of India. </p>
                                            <p>To know more: <a href=" https://www.startupindia.gov.in/content/sih/en/government-schemes/nulm.html" target="_blank">  https://www.startupindia.gov.in/content/sih/en/government-schemes/nulm.html </a>
                                            </p>
                                        </div>
                                    </li>

                                    <!-- Card 69 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">69)	Hardware Technology Park Scheme Software Technology Parks of India Under Ministry of Electronics and Information Technology</div>
                                        <div class="collapsible-body">
                                            <p>Units undertaking to export their entire production of goods and services may be set up under the Electronic Hardware Technology Park (E.H.T.P.) Scheme. Such units may be engaged in manufacture and services.
                                                Commensurate with the policy to give a special thrust to export of electronic hardware, such units would be encouraged to be set up under the aforementioned export oriented scheme.</p>
                                            <p>To know more: <a href="https://www.startupindia.gov.in/content/sih/en/government-schemes/hardware-technology-park-scheme.html" target="_blank">  https://www.startupindia.gov.in/content/sih/en/government-schemes/hardware-technology-park-scheme.html </a>
                                            </p>
                                        </div>
                                    </li>

                                    <!-- Card 70 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">70)	Duty Exemption and Remission Scheme Ministry of Commerce and Industry </div>
                                        <div class="collapsible-body">
                                            <p>Duty exemption schemes enable duty free import of inputs required for export production. Duty exemption schemes consist of:</br>
                                                a) Advance Authorisation scheme</br>
                                                b) Duty Free Import Authorisation (DFIA) scheme</br>
                                                2. A Duty Remission Scheme enables post export replenishment / remission of duty on inputs used in export product. Duty Remission Schemes consist of :</br>
                                                a) Duty Entitlement Passbook (DEPB) Scheme</br>
                                                b) Duty Drawback (DBK) Scheme</br>
                                            </p>
                                            <p>To know more: <a href="https://www.startupindia.gov.in/content/sih/en/government-schemes/duty_exemption_and_remission_scheme.html" target="_blank">  https://www.startupindia.gov.in/content/sih/en/government-schemes/duty_exemption_and_remission_scheme.html </a>
                                            </p>
                                        </div>
                                    </li>

                                    <!-- Card 71 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">71)	Support to training and employment programme for women Ministry of Women & Child Development </div>
                                        <div class="collapsible-body">
                                            <p>STEP Scheme aims to provide skills that give employability to women and to provide competencies and skill that enable women to become self-employed/entrepreneurs. The Scheme is intended to benefit women who are in the age group of 16 years and above across the country.</p>
                                            <p>To know more: <a href="https://www.startupindia.gov.in/content/sih/en/government-schemes/support_to_training_and_employment_programme.html" target="_blank">  https://www.startupindia.gov.in/content/sih/en/government-schemes/support_to_training_and_employment_programme.html </a>
                                            </p>
                                        </div>
                                    </li>

                                    <!-- Card 72 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">72)	Comprehensive Handloom Cluster Development Scheme (CHCDS) Ministry of Textiles </div>
                                        <div class="collapsible-body">
                                            <p>The objective is to develop Mega Handloom Clusters that are located in clearly identifiable geographical locations that specialize in specific products, with close linkages and inter dependents amongst the key players in the cluster by improving the infrastructure facilities, with better storage facilities, technology up-gradation in pre-loom/on-loom/post-loom operations, weaving shed, skill up-gradation, design inputs, health facilities etc. which would eventually be able to meet the discerning and changing market demands both at domestic and at the international level and raise living standards of the millions of weavers engaged in the handloom industry.</p>
                                            <p>To know more: <a href="https://www.startupindia.gov.in/content/sih/en/government-schemes/mega_cluster_scheme.html" target="_blank">  https://www.startupindia.gov.in/content/sih/en/government-schemes/mega_cluster_scheme.html </a>
                                            </p>
                                        </div>
                                    </li>

                                    <!-- Card 73 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">73)	Trade Related Entrepreneurship Assistance and Development (TREAD) Scheme for Women Ministry of Micro, Small and Medium Enterprises </div>
                                        <div class="collapsible-body">
                                            <p>This scheme envisages economic empowerment of women by providing credit (through NGOs), training, development and counseling extension activities related to trades, products, services etc.</p>
                                            <p>To know more: <a href="https://www.startupindia.gov.in/content/sih/en/government-schemes/tread.html" target="_blank"> https://www.startupindia.gov.in/content/sih/en/government-schemes/tread.html </a>
                                            </p>
                                        </div>
                                    </li>

                                    <!-- Card 74 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">74)	Cent Kalyani Scheme Central Bank of India </div>
                                        <div class="collapsible-body">
                                            <p>This scheme envisages empowerment of women to start new project or expand or mordenise the existing unit. This scheme provides assistance in the form of capital expenditure (Plant/Machinery) and also meeting day to day expenditure (working capital). This scheme is provided by the Central Bank of India. </p>
                                            <p>To know more: <a href=" https://www.startupindia.gov.in/content/sih/en/government-schemes/cent.html" target="_blank">  https://www.startupindia.gov.in/content/sih/en/government-schemes/cent.html </a>
                                            </p>
                                        </div>
                                    </li>

                                    <!-- Card 75 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">75)	Infomediary Services MINISTRY OF MICRO, SMALL AND MEDIUM ENTERPRISES (NSIC Schemes) </div>
                                        <div class="collapsible-body">
                                            <p>NSIC offers Infomediary Services, which is onestop and a one-window bouquet of aids that will provide information on business, technology and finance, and also exhibit core competencies of Indian SMEs. The corporation is offering services through its MSME E GLOBAL MART www.msmemart.com; which is a Business to Business (B2B) and Business to Customer (B2C) compliant web portal. Services are available through annual membership. </p>
                                            <p>To know more: <a href="https://www.startupindia.gov.in/content/sih/en/government-schemes/infomediary_services.html" target="_blank">  https://www.startupindia.gov.in/content/sih/en/government-schemes/infomediary_services.html </a>
                                            </p>
                                        </div>
                                    </li>

                                    <!-- Card 76 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">76)	Marketing Intelligence Services MINISTRY OF MICRO, SMALL AND MEDIUM ENTERPRISES (NSIC Schemes) </div>
                                        <div class="collapsible-body">
                                            <p>Marketing Intelligence Cell acquires and analyses information for both the existing and potential customers, to understand the market, determine current and future needs and preferences, attitudes and behaviour of market; and to assess changes in business environment that may affect the size and nature of the market</p>
                                            <p>To know more: <a href=" https://www.startupindia.gov.in/content/sih/en/government-schemes/marketingintel_services.html" target="_blank">   https://www.startupindia.gov.in/content/sih/en/government-schemes/marketingintel_services.html </a>
                                            </p>
                                        </div>
                                    </li>

                                    <!-- Card 77 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">77)	Prime Minister’s Employment Generation Programme (PMEGP) MINISTRY OF MICRO, SMALL AND MEDIUM ENTERPRISES (ARI Division Schemes) </div>
                                        <div class="collapsible-body">
                                            <p>The scheme is implemented by Khadi and Village Industries Commission (KVIC) as the nodal agency at the national level. At the state level, the scheme is implemented through State KVIC Directorates, State Khadi and Village Industries Boards (KVIBs) and District Industries Centres (DICs) and banks. The Government subsidy under the scheme is routed by KVIC through the identified banks for eventual distribution to the beneficiaries/entrepreneurs into their bank accounts.</p>
                                            <p>To know more: <a href=" https://www.startupindia.gov.in/content/sih/en/government-schemes/pmemploymentgen_prog.html" target="_blank">   https://www.startupindia.gov.in/content/sih/en/government-schemes/pmemploymentgen_prog.html </a>
                                            </p>
                                        </div>
                                    </li>

                                    <!-- Card 78 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">78)	Udaan training programme for unemployed youth of J&K (SII J&K) Ministry of Skill Development and Entrepreneurship </div>
                                        <div class="collapsible-body">
                                            <p>This scheme provides employment oriented training to the youth from the state over five years covering various sectors like business management, software, BPO.</p>
                                            <p>To know more: <a href=" https://www.startupindia.gov.in/content/sih/en/government-schemes/udaan_trainingprog.html" target="_blank">   https://www.startupindia.gov.in/content/sih/en/government-schemes/udaan_trainingprog.html </a>
                                            </p>
                                        </div>
                                    </li>

                                    <!-- Card 79 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">79)	National Skill Certification & Monetary Reward (STAR scheme) Ministry of Skill Development and Entrepreneurship </div>
                                        <div class="collapsible-body">
                                            <p>The scheme is for encouraging skill development among the youth by providing monetary rewards for successful completion of approved training programmes.</p>
                                            <p>To know more: <a href=" https://www.startupindia.gov.in/content/sih/en/government-schemes/star_scheme.html" target="_blank"> https://www.startupindia.gov.in/content/sih/en/government-schemes/star_scheme.html </a>
                                            </p>
                                        </div>
                                    </li>

                                    <!-- Card 80 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">80)	Pradhan Mantri Kaushal Vikas Yojana Ministry of Skill Development and Entrepreneurship </div>
                                        <div class="collapsible-body">
                                            <p>Pradhan Mantri Kaushal Vikas Yojana (PMKVY) is the flagship scheme of the Ministry of Skill Development & Entrepreneurship (MSDE) implemented by National Skill Development Corporation. The objective of this Skill Certification Scheme is to enable a large number of Indian youth to take up industry-relevant skill training that will help them in securing a better livelihood. Individuals with prior learning experience or skills will also be assessed and certified under Recognition of Prior Learning (RPL). </p>
                                            <p>To know more: <a href=" https://www.startupindia.gov.in/content/sih/en/government-schemes/pmkvy_scheme.html" target="_blank">  https://www.startupindia.gov.in/content/sih/en/government-schemes/pmkvy_scheme.html </a>
                                            </p>
                                        </div>
                                    </li>

                                    <!-- Card 81 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">81)	Apprenticeship Training Ministry of Labour and Employment </div>
                                        <div class="collapsible-body">
                                            <p>This scheme aims to provide facilities available in industry for imparting practical training with a view to meeting the requirements for skilled manpower of Industry.</p>
                                            <p>To know more: <a href="https://www.startupindia.gov.in/content/sih/en/government-schemes/apprenticeship_training.html" target="_blank">  https://www.startupindia.gov.in/content/sih/en/government-schemes/apprenticeship_training.html </a>
                                            </p>
                                        </div>
                                    </li>

                                    <!-- Card 82 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">82)	Craftsmen Training Ministry of Labour and Employment </div>
                                        <div class="collapsible-body">
                                            <p>The scheme is formulated to impart skills in various vocational trades to meet the skilled manpowerrequirements for technology and industrial growth by way of Industrial Training Institutes (ITIs).</p>
                                            <p>To know more: <a href=" https://www.startupindia.gov.in/content/sih/en/government-schemes/craftsmenship_training.html" target="_blank">   https://www.startupindia.gov.in/content/sih/en/government-schemes/craftsmenship_training.html </a>
                                            </p>
                                        </div>
                                    </li>

                                    <!-- Card 83 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">83)	Skill Development in 34 Districts Affected by Left Wing Extremism Ministry of Labour and Employment </div>
                                        <div class="collapsible-body">
                                            <p>The aim is to create skill development infrastructure in the 34 districts closer to the people of left wing extremism (LWE) affected districts. The objective is to establish one ITI & two Skill Development Centers (SDCs) in each of the 34 districts and to run demand driven vocational training courses, both long and short term, to meet the requirement of skilled manpower of various sectors. Under the “skill training” component of the scheme, 30, 120 and 10 youth per district will be trained in long term, short term and instructor training courses respectively.</p>
                                            <p>To know more: <a href=" https://www.startupindia.gov.in/content/sih/en/government-schemes/skilldevelop_.html" target="_blank">   https://www.startupindia.gov.in/content/sih/en/government-schemes/skilldevelop_.html </a>
                                            </p>
                                        </div>
                                    </li>

                                    <!-- Card 84 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">84)	Skill Development Initiative (SDI) Ministry of Labour and Employment </div>
                                        <div class="collapsible-body">
                                            <p>The main aim is to provide vocational training to school dropouts, existing workers, ITI graduates, etc., to improve their employability by optimally utilising the infrastructure available in Government, Private Institutions, and Industry.</p>
                                            <p>To know more: <a href=" https://www.startupindia.gov.in/content/sih/en/government-schemes/SDI_.html" target="_blank"> https://www.startupindia.gov.in/content/sih/en/government-schemes/SDI_.html </a>
                                            </p>
                                        </div>
                                    </li>

                                    <!-- Card 85 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">85)	Upgradation of 1396 ITIs through PPP Ministry of Labour and Employment </div>
                                        <div class="collapsible-body">
                                            <p>The objective of the Scheme is to improve the quality of vocational training in the country and make it demand driven so as to ensure better employability of the graduates.</p>
                                            <p>To know more: <a href="https://www.startupindia.gov.in/content/sih/en/government-schemes/upgradationof1396_ITIs.html" target="_blank"> https://www.startupindia.gov.in/content/sih/en/government-schemes/upgradationof1396_ITIs.html </a>
                                            </p>
                                        </div>
                                    </li>

                                    <!-- Card 86 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">86)	Apparel Park Ministry of Textiles </div>
                                        <div class="collapsible-body">
                                            <p>Government of India has launched 'Apparel Park for Exports' scheme for imparting focussed thrust for setting up of apparel manufacturing units of international standards at potential growth centres.</p>
                                            <p>To know more: <a href="https://www.startupindia.gov.in/content/sih/en/government-schemes/apparel_park.html" target="_blank"> https://www.startupindia.gov.in/content/sih/en/government-schemes/apparel_park.html </a>
                                            </p>
                                        </div>
                                    </li>

                                    <!-- Card 87 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">87)	Research and Development Ministry of Textiles </div>
                                        <div class="collapsible-body">
                                            <p>The office of the Development Commissioner (Handicrafts), an attached office of Ministry of Textiles, Government of India has been implementing the scheme called Research & Development on all India basis to conduct surveys and studies of important crafts and make in-depth analysis of specific aspects and problems of Handicrafts in order to generate useful inputs to aid policy, planning and fine tune the ongoing initiatives; and to have independent evaluation of the schemes implemented.</p>
                                            <p>To know more: <a href="https://www.startupindia.gov.in/content/sih/en/government-schemes/Research_development.html" target="_blank"> https://www.startupindia.gov.in/content/sih/en/government-schemes/Research_development.html </a>
                                            </p>
                                        </div>
                                    </li>

                                    <!-- Card 88 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">88)	Rental for Warehousing Ministry of Textiles</div>
                                        <div class="collapsible-body">
                                            <p>This scheme aims for providing financial support for renting warehouses abroad. </p>
                                            <p>To know more: <a href="https://www.startupindia.gov.in/content/sih/en/government-schemes/rentalfor_warehousing.html" target="_blank"> https://www.startupindia.gov.in/content/sih/en/government-schemes/rentalfor_warehousing.html </a>
                                            </p>
                                        </div>
                                    </li>

                                    <!-- Card 89 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">89)	Promotion for packaged Tea of Indian origin Tea Board of India </div>
                                        <div class="collapsible-body">
                                            <p>The scheme aims to to help Indian exporters for marketing teas of Indian origin in overseas markets on a sustained basis, the sub-component is intended to promote teas of Indian origin therein.</p>
                                            <p>To know more: <a href="https://www.startupindia.gov.in/content/sih/en/government-schemes/promotionforpackaged_tea.html" target="_blank"> https://www.startupindia.gov.in/content/sih/en/government-schemes/promotionforpackaged_tea.html </a>
                                            </p>
                                        </div>
                                    </li>

                                    <!-- Card 90 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">90)	Participation in International Fairs and Exhibitions with Tea Board Tea Board of India </div>
                                        <div class="collapsible-body">
                                            <p>The scheme aims to provide a platform to the exporters to show case their products in international events for promotion and facilitate generation of trade opportunities through organising interactive sessions between buyers and sellers.</p>
                                            <p>To know more: <a href="https://www.startupindia.gov.in/content/sih/en/government-schemes/internationalfairandexhibition_tea.html" target="_blank"> https://www.startupindia.gov.in/content/sih/en/government-schemes/internationalfairandexhibition_tea.html </a>
                                            </p>
                                        </div>
                                    </li>

                                    <!-- Card 91 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">91)	Bridge Loan against MNRE Capital Subsidy Ministry of New and Renewable Energy </div>
                                        <div class="collapsible-body">
                                            <p>The credit under the bill discounting scheme will be available to MNRE Accredited Channel Partners (ACP), State Nodal Agencies (SNA) and other stake holders for purchase and installation of Solar Water Heating System (SWHS) as approved by MNRE
                                                The amount of loan assistance/ Bill Discounting Shall be within the unutilized funds of Government Budget/MNRE Scheme for installations of Solar Water Heating Systems (SWHS). In case it is felt that the recovery/payment of subsidy amount against which loan assistance has been provided is doubtful, borrower will be liable to pay on demand entire such amount including interest and other charges to IREDA</p>
                                            <p>To know more: <a href="https://www.startupindia.gov.in/content/sih/en/government-schemes/bridge_loan.html" target="_blank"> https://www.startupindia.gov.in/content/sih/en/government-schemes/bridge_loan.html </a>
                                            </p>
                                        </div>
                                    </li>

                                    <!-- Card 92 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">92)	Atal Mission For Rejuvenation and Urban Transformation (AMRUT) Ministry of Housing and Urban Affairs </div>
                                        <div class="collapsible-body">
                                            <p>The purpose of Atal Mission for Rejuvenation and Urban Transformation (AMRUT) is to </br>
                                                (i)	ensure that every household has access to a tap with assured supply of water and a sewerage connection; </br>
                                                (ii)	increase the amenity value of cities by developing greenery and well-maintained open spaces (e.g. parks); and </br>
                                                (iii)	reduce pollution by switching to public transport or constructing facilities for non-motorized transport (e.g. walking and cycling). All these outcomes are valued by citizens, particularly women, and indicators and standards have been prescribed by the Ministry of Urban Development (MoUD) in the form of Service Level Benchmarks (SLBs).
                                            </p>	
                                            <p>To know more: <a href="https://www.startupindia.gov.in/content/sih/en/government-schemes/AMRUT_.html" target="_blank"> https://www.startupindia.gov.in/content/sih/en/government-schemes/AMRUT_.html </a>
                                            </p>
                                        </div>
                                    </li>

                                    <!-- Card 93 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">93)	Drugs & Pharmaceutical Research Department of Science and Technology </div>
                                        <div class="collapsible-body">
                                            <p>Recognising the profound influence of R&D on the prospects and opportunities for the growth of the Indian Drug Industry, Department of Science and Technology (DST). The objectives for this scheme are: </br>
                                                •	To synergise the strengths of publicly funded R&D institutions and Indian Pharmaceutical Industry.</br>
                                                •	To create an enabling infrastructure, mechanisms and linkages to facilitate new drug development.</br>
                                                •	To stimulate skill development of human resources in R&D for drugs and pharmaceuticals; and</br>
                                                •	To enhance the nation's self-reliance in drugs and pharmaceuticals especially in areas critical to national health requirements.</br>
                                            </p>	
                                            <p>To know more: <a href="https://www.startupindia.gov.in/content/sih/en/government-schemes/drugandpharma_research.html" target="_blank"> https://www.startupindia.gov.in/content/sih/en/government-schemes/drugandpharma_research.html </a>
                                            </p>
                                        </div>
                                    </li>

                                    <!-- Card 94 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">94)	Mega Cluster Ministry of Textiles </div>
                                        <div class="collapsible-body">
                                            <p> The prospect of this sector lies in infrastructural improvement, modernization of the tools, machinery, process and product diversification and creating strong brands. Innovative designs as well as technical know-how, furthered by brand building of the native products hold the key to creating a niche market for the products manufactured by the clusters. The proposed programme is expected to support the Up gradation of infrastructural facilities coupled with market linkages and product development & diversification.
                                            </p>	
                                            <p>To know more: <a href="https://www.startupindia.gov.in/content/sih/en/government-schemes/Mega_cluster.html" target="_blank"> https://www.startupindia.gov.in/content/sih/en/government-schemes/Mega_cluster.html </a>
                                            </p>
                                        </div>
                                    </li>

                                    <!-- Card 95 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">95)	Development/Upgradation of Watermills and setting up Micro Hydel Projects (up to 100 KW capacity) </div>
                                        <div class="collapsible-body">
                                            <p> Ministry of New and Renewable Resources The Watermills (WM) and Micro Hydel Projects (MHP) have the potential to meet the power requirements of remote areas in a decentralized manner. The scheme provides grant of Central Financial Assistance for development/ upgradation of WM and setting up of MHP. The scheme also envisages support for specialised studies/survey, strengthening of database, training and capacity building relating to WM and MHP.
                                            </p>	
                                            <p>To know more: <a href="https://www.startupindia.gov.in/content/sih/en/government-schemes/watermills_upgrade.html" target="_blank"> https://www.startupindia.gov.in/content/sih/en/government-schemes/watermills_upgrade.html </a>
                                            </p>
                                        </div>
                                    </li>

                                    <!-- Card 96 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">96)	Research, Design, Development, Demonstration (RDD&D) and Manufacture of New and Renewable Energy Ministry of New and Renewable Resources</div>
                                        <div class="collapsible-body">
                                            <p> The purpose of this scheme is to make the industry competitive and renewable; energy generation supply self-sustainable/ profitable.
                                                RDD&D activities would focus on research, design and development that would lead to eventual manufacturing of complete systems, even if those activities are required to be shared among different institutions. Thus, there would be a need for system integration broadly covering, inter-alia, the following areas:</p>	
                                            <p>To know more: <a href="https://www.startupindia.gov.in/content/sih/en/government-schemes/RDD_D.html" target="_blank"> https://www.startupindia.gov.in/content/sih/en/government-schemes/RDD_D.html </a>
                                            </p>
                                        </div>
                                    </li>

                                    <!-- Card 97 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">97)	Wind Resource Assessment in Uncovered/ New Areas under NCEF Scheme Ministry of New and Renewable Resources </div>
                                        <div class="collapsible-body">
                                            <p> The Ministry has initiated a new scheme on implementation of Wind Resource Assessment in Uncovered/New Areas with an aim to assess the realistic potential at 100 m level in 500 new stations under the National Clean Energy Fund (NCEF) also to be implemented through C-WET. SNAs along with Private developers shall invest the entire project cost initially and carry out all necessary works to establish the Wind monitoring stations (WMS).</p>	
                                            <p>To know more: <a href="https://www.startupindia.gov.in/content/sih/en/government-schemes/Windresource_asses.html" target="_blank"> https://www.startupindia.gov.in/content/sih/en/government-schemes/Windresource_asses.html </a>
                                            </p>
                                        </div>
                                    </li>

                                    <!-- Card 98 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">98)	Capital Investment Subsidy Scheme under National Bamboo Mission National Bamboo Mission </div>
                                        <div class="collapsible-body">
                                            <p> There are a number of activities under National Bamboo Mission which are to be financed by a credit linked back ended subsidy by Financial Institutions (FIs) including banks. Promoters can submit their project proposals to the eligible financial institutions for the grant of subsidy.</p>	
                                            <p>To know more: <a href="https://www.startupindia.gov.in/content/sih/en/government-schemes/capitalsubsidy__bamboo.html" target="_blank"> https://www.startupindia.gov.in/content/sih/en/government-schemes/capitalsubsidy__bamboo.html </a>
                                            </p>
                                        </div>
                                    </li>

                                    <!-- Card 99 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">99)	Capacity Building and Technical Assistance (CB&TA) MINISTRY OF DEVELOPMENT OF NORTH EASTERN REGION </div>
                                        <div class="collapsible-body">
                                            <p> The main aim of the Ministry of Development of North Eastern Region is strengthening of its human resources, particularly the youth, to make them skilled and employable. The skill and knowledge base of administrators in the state governments also needs strengthening to ensure good governance.</p>	
                                            <p>To know more: <a href=" https://www.startupindia.gov.in/content/sih/en/government-schemes/CB_TA.html" target="_blank">  https://www.startupindia.gov.in/content/sih/en/government-schemes/CB_TA.html </a>
                                            </p>
                                        </div>
                                    </li>

                                    <!-- Card 100 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">100)	Corporate Finance (North Eastern Development Finance Corporation Ltd Schemes) MINISTRY OF DEVELOPMENT OF NORTH EASTERN REGION </div>
                                        <div class="collapsible-body">
                                            <p> Providing finance such as normal capital expenditure, working capital margin, short fall in working capital, repayment of high cost debt and general corporate purpose like funding of business acquisition or for brand building, etc., where no tangible asset creation may be envisaged.</p>	
                                            <p>To know more: <a href="https://www.startupindia.gov.in/content/sih/en/government-schemes/NED_FL.html" target="_blank"> https://www.startupindia.gov.in/content/sih/en/government-schemes/NED_FL.html </a>
                                            </p>
                                        </div>
                                    </li>

                                    <!-- Card 101 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">101)	Equipment Finance (North Eastern Development Finance Corporation Ltd Schemes) MINISTRY OF DEVELOPMENT OF NORTH EASTERN REGION </div>
                                        <div class="collapsible-body">
                                            <p> This scheme intends to provide financial assistance for acquiring specific machinery/ equipment by financially sound and profit making companies having good credit record. The proposed unit should be located in any of the eight North Eastern States.</p>	
                                            <p>To know more: <a href="https://www.startupindia.gov.in/content/sih/en/government-schemes/Equip_finance.html" target="_blank"> https://www.startupindia.gov.in/content/sih/en/government-schemes/Equip_finance.html </a>
                                            </p>
                                        </div>
                                    </li>

                                    <!-- Card 102 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">102)	Initiative for Development of Entrepreneurs in Agriculture (IDEA) (NEDFL Schemes) MINISTRY OF DEVELOPMENT OF NORTH EASTERN REGION </div>
                                        <div class="collapsible-body">
                                            <p>The scheme intends to promote agri-business ventures in the North-East Region and assist in establishing agri-business as a profitable venture. It also provides gainful employment opportunities and makes available supplementary sources of input supply and services.</p>	
                                            <p>To know more: <a href="https://www.startupindia.gov.in/content/sih/en/government-schemes/IDEA_.html" target="_blank"> https://www.startupindia.gov.in/content/sih/en/government-schemes/IDEA_.html </a>
                                            </p>
                                        </div>
                                    </li>

                                    <!-- Card 103 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">103)	Micro Finance Scheme (NEDFL Schemes) MINISTRY OF DEVELOPMENT OF NORTH EASTERN REGION </div>
                                        <div class="collapsible-body">
                                            <p>The scheme envisages meeting micro credit needsSmall and Medium size agriculturists, selfemployed personnel and entrepreneurs can bereached much more effectively by involving the services of intermediaries, who can understand needs, demand and local situations. Developing and supporting NGOs/ Voluntary Agencies (VAs) with good track record for on-lending to the “needy” for taking up any income generating activities in the rural areas.</p>	
                                            <p>To know more: <a href="https://www.startupindia.gov.in/content/sih/en/government-schemes/micro_finance.html" target="_blank"> https://www.startupindia.gov.in/content/sih/en/government-schemes/micro_finance.html </a>
                                            </p>
                                        </div>
                                    </li>

                                    <!-- Card 104 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">104)	NEDFi Equity Fund (NEDFL Schemes) MINISTRY OF DEVELOPMENT OF NORTH EASTERN REGION </div>
                                        <div class="collapsible-body">
                                            <p>The schemes intends to invest in projects promoted by entrepreneurs in North-Eastern Region having sound business ideas with potential for high growth and more than normal returns on investment.</p>	
                                            <p>To know more: <a href="https://www.startupindia.gov.in/content/sih/en/government-schemes/NEDFL_equity.html" target="_blank"> https://www.startupindia.gov.in/content/sih/en/government-schemes/NEDFL_equity.html </a>
                                            </p>
                                        </div>
                                    </li>

                                    <!-- Card 105 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">105)	NEDFi Opportunity Scheme for Small Enterprises (NoSSE) (NEDFL Schemes) MINISTRY OF DEVELOPMENT OF NORTH EASTERN REGION </div>
                                        <div class="collapsible-body">
                                            <p>The scheme aims at providing long term financial assistance for setting up new industrial and infrastructure projects as well as for expansion, diversification or modernisation of existing industrial enterprises, excluding commercial real estates.</p>	
                                            <p>To know more: <a href=" https://www.startupindia.gov.in/content/sih/en/government-schemes/NoSSE_.html" target="_blank">  https://www.startupindia.gov.in/content/sih/en/government-schemes/NoSSE_.html </a>
                                            </p>
                                        </div>
                                    </li>

                                    <!-- Card 106 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">106)	NEDFi Opportunity Scheme for Small Enterprises (NoSSE) (NEDFL Schemes) MINISTRY OF DEVELOPMENT OF NORTH EASTERN REGION </div>
                                        <div class="collapsible-body">
                                            <p>The scheme has been formulated to help first generation entrepreneurs who are short of equity. New projects in Micro and Small Enterprises, expansion, modernisation of existing units. Technical qualification of the promoter in the relevant field is a pre-requisite.</p>	
                                            <p>To know more: <a href=" https://www.startupindia.gov.in/content/sih/en/government-schemes/NEED_.html" target="_blank">  https://www.startupindia.gov.in/content/sih/en/government-schemes/NEED_.html </a>
                                            </p>
                                        </div>
                                    </li>

                                    <!-- Card 107 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">107)	NEDFi Opportunity Scheme for Small Enterprises (NoSSE) (NEDFL Schemes) MINISTRY OF DEVELOPMENT OF NORTH EASTERN REGION </div>
                                        <div class="collapsible-body">
                                            <p>The scheme has been formulated to help first generation entrepreneurs who are short of equity. New projects in Micro and Small Enterprises, expansion, modernisation of existing units. Technical qualification of the promoter in the relevant field is a pre-requisite.</p>	
                                            <p>To know more: <a href=" https://www.startupindia.gov.in/content/sih/en/government-schemes/NEED_.html" target="_blank"> https://www.startupindia.gov.in/content/sih/en/government-schemes/NEED_.html </a>
                                            </p>
                                        </div>
                                    </li>

                                    <!-- Card 108 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">108)	North-East Handloom Handicrafts (NEHH) (NEDFL Schemes) MINISTRY OF DEVELOPMENT OF NORTH EASTERN REGION </div>
                                        <div class="collapsible-body">
                                            <p>Promoting enterprises in handlooms and handicrafts sector and providing a platform for sustainable economic growth by promoting the local artisans.</p>	
                                            <p>To know more: <a href="https://www.startupindia.gov.in/content/sih/en/government-schemes/NEEHH_.html" target="_blank"> https://www.startupindia.gov.in/content/sih/en/government-schemes/NEEHH_.html </a>
                                            </p>
                                        </div>
                                    </li>

                                    <!-- Card 109 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">109)	Rupee Term Loan (RTL) (NEDFL Schemes) MINISTRY OF DEVELOPMENT OF NORTH EASTERN REGION </div>
                                        <div class="collapsible-body">
                                            <p>Providing medium to long term financial assistance for setting up of new expansion, diversification or modernisation of projects in manufacturing or services sectors.</p>	
                                            <p>To know more: <a href="https://www.startupindia.gov.in/content/sih/en/government-schemes/RTL_.html" target="_blank"> https://www.startupindia.gov.in/content/sih/en/government-schemes/RTL_.html </a>
                                            </p>
                                        </div>
                                    </li>

                                    <!-- Card 110 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">110)	Working Capital Term Loan (WCTL) (NEDFL Schemes) MINISTRY OF DEVELOPMENT OF NORTH EASTERN REGION </div>
                                        <div class="collapsible-body">
                                            <p>The scheme aims to provide one-time core working capital assistance to deserving units in the form of working capital term loan.</p>	
                                            <p>To know more: <a href="https://www.startupindia.gov.in/content/sih/en/government-schemes/WCTL_.html" target="_blank"> https://www.startupindia.gov.in/content/sih/en/government-schemes/WCTL_.html </a>
                                            </p>
                                        </div>
                                    </li>

                                    <!-- Card 111 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">111)	WCTL for Contract Finance (NEDFL Schemes) MINISTRY OF DEVELOPMENT OF NORTH EASTERN REGION </div>
                                        <div class="collapsible-body">
                                            <p>NEDFi provides financial assistance in the form of gap funding to eligible contractor firms/companies for contract work execution.</p>	
                                            <p>To know more: <a href="https://www.startupindia.gov.in/content/sih/en/government-schemes/WCTL_ContractFin.html" target="_blank"> https://www.startupindia.gov.in/content/sih/en/government-schemes/WCTL_ContractFin.html </a>
                                            </p>
                                        </div>
                                    </li>

                                    <!-- Card 112 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">112)	Women Enterprise Development (WED) (NEDFL Schemes) MINISTRY OF DEVELOPMENT OF NORTH EASTERN REGION </div>
                                        <div class="collapsible-body">
                                            <p>The scheme provides financial assistance to woman entrepreneurs for taking up business ventures. Existing businesses will also be eligible for expansion, modernisation and diversification.</p>	
                                            <p>To know more: <a href="https://www.startupindia.gov.in/content/sih/en/government-schemes/WED_.html" target="_blank"> https://www.startupindia.gov.in/content/sih/en/government-schemes/WED_.html </a>
                                            </p>
                                        </div>
                                    </li>

                                    <!-- Card 113 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">113)	North-Eastern Region Urban Development Programme MINISTRY OF DEVELOPMENT OF NORTH EASTERN REGION </div>
                                        <div class="collapsible-body">
                                            <p>The aim is to improve quality of life of urban residents and enhance urban productivity through improved infrastructure and services, with a view to building capacity for enhanced urban governance, finance and service delivery systems through institutional and financial reforms.</p>	
                                            <p>To know more: <a href="https://www.startupindia.gov.in/content/sih/en/government-schemes/NEUDP_.html" target="_blank"> https://www.startupindia.gov.in/content/sih/en/government-schemes/NEUDP_.html </a>
                                            </p>
                                        </div>
                                    </li>

                                    <!-- Card 114 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">114)	North-East Rural Livelihoods Project (NERLP) MINISTRY OF DEVELOPMENT OF NORTH EASTERN REGION </div>
                                        <div class="collapsible-body">
                                            <p>The development objective of the project is to improve rural livelihoods, especially that of women, unemployed youths and the most disadvantaged, in the participating North-Eastern States. There are four components to the project.</br>
                                                i) Social Empowerment </br>
                                                ii) Economic Empowerment</br>
                                                iii) Partnership Development </br>
                                                iv) Project Management</p>	
                                            <p>To know more: <a href="https://www.startupindia.gov.in/content/sih/en/government-schemes/NERLP_.html" target="_blank"> https://www.startupindia.gov.in/content/sih/en/government-schemes/NERLP_.html </a>
                                            </p>
                                        </div>
                                    </li>

                                    <!-- Card 115 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">115)	Advertising and Publicity by the Ministry of Development of North-Eastern Region  MINISTRY OF DEVELOPMENT OF NORTH EASTERN REGION </div>
                                        <div class="collapsible-body">
                                            <p>The scheme aims to showcase the inherent economic, social and cultural strength of the North-Eastern Region as well as to mainstream the region with the country to move forward in its entirety.</p>	
                                            <p>To know more: <a href="https://www.startupindia.gov.in/content/sih/en/government-schemes/Adv_publicity.html" target="_blank"> https://www.startupindia.gov.in/content/sih/en/government-schemes/Adv_publicity.html </a>
                                            </p>
                                        </div>
                                    </li>

                                    <!-- Card 116 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">116)	Ministry of Defence Technology Development Fund - DRDO </div>
                                        <div class="collapsible-body">
                                            <p>Technology Development Fund (TDF) has been established to promote self-reliance in Defence Technology as a part of the 'Make in India' initiative. It is a programme of MoD (Ministry of Defence) executed by DRDO meeting the requirements of Tri-Services, Defence Production and DRDO.
                                                The scheme encourages participation of public/private industries especially MSMEs so as to create an eco-system for enhancing cutting edge technology capability for defence application by inculcating R&D culture in industry.</p>	
                                            <p>To know more: <a href="https://www.startupindia.gov.in/content/sih/en/government-schemes/technology_development_fund_drdo.html" target="_blank"> https://www.startupindia.gov.in/content/sih/en/government-schemes/technology_development_fund_drdo.html </a>
                                            </p>
                                        </div>
                                    </li>

                                    <!-- Card 117 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">117)	The Samridhi Fund Small Industries Development Bank of India (SIDBI) </div>
                                        <div class="collapsible-body">
                                            <p>The Samridhi Fund is an approx. ₹430 crore social venture capital fund. SIDBI has envisaged the creation of the Samridhi Fund to provide capital to social enterprises which can deliver both financial and social returns, in Bihar, Uttar Pradesh, Madhya Pradesh, Odisha , Chattisgarh, Jharkhand, Rajasthan and West Bengal.</p>	
                                            <p>To know more: <a href="https://www.startupindia.gov.in/content/sih/en/government-schemes/samridhi_fund.html" target="_blank"> https://www.startupindia.gov.in/content/sih/en/government-schemes/samridhi_fund.html </a>
                                            </p>
                                        </div>
                                    </li>

                                    <!-- Card 118 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">118) Technology Based Entrepreneurship Development Programme (TEDP) </div>
                                        <div class="collapsible-body">
                                            <p>TEDP is a structured training programme of 6-weeks duration designed to motivate and develop entrepreneurs in specific products / technologies / processes developed by CSIR labs, R&D institutions, universities etc</p>
                                        </div>
                                    </li>

                                    <!-- Card 119 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">119) Aatmanirbhar Bharat ARISE-ANIC</div>
                                        <div class="collapsible-body">
                                            <p>The Aatmanirbhar Bharat ARISE-ANIC program is a national initiative to promote research & innovation and increase competitiveness of Indian startups and MSMEs. The objective is to proactively collaborate with esteemed Ministries and the associated industries to catalyse research, innovation and facilitate innovative solutions to sectoral problems. The objective is also to provide a steady stream of innovative products & solutions where the Central Government Ministries / Departments will become the potential first buyers.</p>
                                        </div>
                                    </li>

                                    <!-- Card 120 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">120) Abdul Kalam Technology Innovation National Fellowship</div>
                                        <div class="collapsible-body">
                                            <p>Abdul Kalam Technology Innovation National Fellowship recognize, encourage and support translational research by Indian Nationals to achieve excellence in engineering, innovation and technology development. Indian National Academy of Engineering (INAE) will co-ordinate and award Abdul Kalam Technology Innovation National Fellowships in association with SERB-DST</p>
                                        </div>
                                    </li>

                                    <!-- Card 121 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">121) Accelerate Vigyan</div>
                                        <div class="collapsible-body">
                                            <p>"Accelerate Vigyan" (AV) strives to provide a big push to high-end scientific research and prepare scientific manpower which can venture into research careers and knowledge-based economy. This scheme is primarily focusing on young potential researchers with an aim to give an opportunity to them to spend quality time in the pre-identified premier institution, labs / organizations and empower them through best practices and environment</p>
                                        </div>
                                    </li>

                                    <!-- Card 122 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">122) Advanced Hydrogen and Fuel Cell Programme (AHFC)</div>
                                        <div class="collapsible-body">
                                            <p>The Advanced Hydrogen and Fuel Cell Programme aims to promote and support activities related to indigenous development of new and existing material in large quantities, catalysts, membrane, components for fuel cells, electrolysers, hydrogen storage materials, materials for type IV cylinders and prototypes for implementation of various applications of hydrogen and fuel cell in the country.</p>
                                        </div>
                                    </li>

                                    <!-- Card 123 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">123) Agriculture Biotechnology</div>
                                        <div class="collapsible-body">
                                            <p>The scheme involves genomic level research for effectively tackling problems of biotic/abiotic stresses and aims towards enhancement of crop productivity, and for improvement of their nutritional quality.</p>
                                        </div>
                                    </li>

                                    <!-- Card 124 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">124) Agro Meteorology</div>
                                        <div class="collapsible-body">
                                            <p>Agrometeorological services programme of the Ministry has a direct impact on agricultural production. The services are available in 550 districts. Farmers receive advisories before various stages of farming. Currently, about 25 lakh farmers are using this information through mobiles. This programme would be continued to have larger coverage with improved advisories, and having closer coordination with State government authorities.</p>
                                        </div>
                                    </li>

                                    <!-- Card 125 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">125) AICTE- Research Promotion Scheme (RPS)</div>
                                        <div class="collapsible-body">
                                            <p>RPS Promotes Research in identified thrust areas of in Technical Education. RPS is aimed to create research ambience in the institutes by promoting research in engineering sciences and innovations in established and newer technologies; and to generate Master’s and Doctoral degree candidates to augment the quality of faculty and research personnel in the country.</p>
                                        </div>
                                    </li>

                                    <!-- Card 126 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">126) Aquaculture & Fisheries Biotechnology</div>
                                        <div class="collapsible-body">
                                            <p>The aim of the programme is to enhance aquaculture production, productivity and development of useful products and processes from marine resources for the benefit of the society. The department has made concerted efforts towards developing newer diagnostics and therapeutics, improvement in aqua feed, developing novel cell lines etc. through the adoption of molecular tools and techniques.</p>
                                        </div>
                                    </li>

                                    <!-- Card 127 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">127) Building Energy Efficiency Research Initiative (BERI)</div>
                                        <div class="collapsible-body">
                                            <p>The focus of the Initiative is to promote R&D activities to improve energy performance of buildings and cities.The programme also supports enhancement of knowledge and practice to save energy indesign, construction and operation of human habitats.</p>
                                        </div>
                                    </li>

                                    <!-- SCHEMES FOR WOMEN Card 128 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">128) Biotechnology based Programme for Women</div>
                                        <div class="collapsible-body">
                                            <p>In women sector, large number of women including SHGs benefitted directly through biotech packages for floriculture, horticulture, cultivation of mushrooms, medicinal and aromatic plants, bio-fertilisers, organic farming, vermicomposting, sericulture, aquaculture, animal husbandry, poultry farming and making of bio-crafts.</p>
                                        </div>
                                    </li>

                                    <!-- Card 129 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">129) Biotechnology Career Advancement & Re-orientation Programme (BioCARe)</div>
                                        <div class="collapsible-body">
                                            <p>The programme is for Career Development of employed/ unemployed women Scientists upto 45 years of age for whom it is the first extramural research grant.</p>
                                        </div>
                                    </li>

                                    <!-- Card 130 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">130) Consolidation of University Research For Innovation And Excellence in Women (CURIE)</div>
                                        <div class="collapsible-body">
                                            <p>The objective is to to develop state-of-the-art infrastructure in women universities in order to attract, train and retain promising girls students in S&T domain.</p>
                                        </div>
                                    </li>

                                    <!-- Card 131 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">131) Biotechnology Career Advancement and Re-orientation Programme (Bio-CARe) for Women Scientists</div>
                                        <div class="collapsible-body">
                                            <p>In an attempt to enhance the participation of Women Scientists in Biotechnology Research, the Department of Biotechnology launched a Biotechnology Career Advancement and Re-orientation Programme (Bio-CARe) for women Scientists.</p>
                                        </div>
                                    </li>

                                    <!-- Card 132 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">132) Knowledge Involvement in Research Advancement through Nurturing (KIRAN)</div>
                                        <div class="collapsible-body">
                                            <p>Under this scheme, women scientists are being encouraged to pursue research in frontier areas of science and engineering, on problems of societal relevance and to take up S&T-based internship followed by self-employment. A large number of well-qualified women get left out of the S&T activities due to various circumstances which are usually typical to the gender.</p>
                                        </div>
                                    </li>

                                    <!-- Card 133 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">133) Mobility Scheme</div>
                                        <div class="collapsible-body">
                                            <p>The Mobility Scheme is aimed to provide an opportunity to women scientists who are facing difficulties in their present job due to relocation (marriage, transfer of husband to any other location within the country, attending ailing parents, and accompanying children studying in different city) and will act as filler while searching other career option at new place.</p>
                                        </div>
                                    </li>
									
									<!-- Card 134 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">134) SAMRIDH Scheme</div>
                                        <div class="collapsible-body">
                                            <p>The SAMRIDH initiative is designed to provide funding support to startups along with helping them bring skill sets together which will help them grow successful. The newly launched SAMRIDH program aims to focus on the acceleration of around 300 start-ups by extending them with customer connect, investor connect, and other opportunities for international expansion in the upcoming three years that will follow.
											For more : <a target="_blank" href="https://pmmodiyojana.in/samridh-scheme/">https://pmmodiyojana.in/samridh-scheme/</a></p>
                                        </div>
                                    </li>
									
									<!-- Card 135 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">135) Pradhan Mantri Mudra Yojana (PMMY)</div>
                                        <div class="collapsible-body">
                                            <p>Micro Units Development Refinance Agency (MUDRA) banks has been created to enhance credit facility and boost the growth of small business in rural areas. The government has introduced this scheme to support small businesses in India. In 2015, the government allocated INR 10,000 crores to promote startup culture in the country. The MUDRA banks provide startup loans of up to INR 10 lakhs to small enterprises, business, which are non-corporate, and non-farm small/micro-enterprises.
											For more : <a target="_blank" href="https://www.mudra.org.in/">https://www.mudra.org.in/</a></p>
                                        </div>
                                    </li>
									
									<!-- Card 136 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">136) Ministry of Skill Development and Entrepreneurship</div>
                                        <div class="collapsible-body">
                                            <p>The task of promoting entrepreneurship was earlier given to different departments and government agencies. In 2014, the Prime Minister decided to dedicate an entire ministry to build this sector as he felt that skill development required greater push from the government's side. Furthermore, the idea is to reach 500 million people by the year 2022 through gap-funding and skill development initiatives.
											For more : <a target="_blank" href="https://www.msde.gov.in/en">https://www.msde.gov.in/en</a></p>
                                        </div>
                                    </li>
									
									<!-- Card 137 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">137) eBiz Portal</div>
                                        <div class="collapsible-body">
                                            <p>eBiz was the first electronic government-to-business(G2B) portal, which was founded in January 2013. The main purpose of the portal was to transform and develop a conducive business environment in the country. eBiz Portal was developed by Infosys in a public-private partnership model. It was designed as a communication center for investors and business communities in India. The portal had launched 29+ services in over 5 states of India, viz., Andhra Pradesh, Delhi, Haryana, Maharashtra, and Tamil Nadu.
											For more : <a target="_blank" href="https://digitalindia.gov.in/content/ebiz">https://digitalindia.gov.in/content/ebiz</a></p>
                                        </div>
                                    </li>
									
									<!-- Card 138 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">138) Multiplier Grants Scheme (MGS)</div>
                                        <div class="collapsible-body">
                                            <p>Department of Electronics and Information Technology (DeitY) started the Multiplier Grants Scheme (MGS). This scheme aims to encourage collaborative Research & Development (R&D) between industry and academics/institutions for the development of products and packages. Under the scheme, if the industry supports the R&D of products that can be commercialized at the institutional level, the government shall provide financial support which will be up to twice the amount provided by industry.
											For more : <a target="_blank" href="https://www.meity.gov.in/content/multiplier-grants-scheme">https://www.meity.gov.in/content/multiplier-grants-scheme</a></p>
                                        </div>
                                    </li>
									
									<!-- Card 139 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">139) Credit Guarantee Fund Trust for Micro and Small Entreprises (CGTMSE)</div>
                                        <div class="collapsible-body">
                                            <p>The Credit Guarantee Fund Trust for Micro and Small Enterprises (CGTMSE ) was set up by the government of India and had been put to effect from 1st January 2000 onwards to provide business loans to micro-level businesses, small-scale industries, and startups with zero collateral. It allows businesses to avail loans at highly subsidized interest rates without requiring security.
											For more : <a target="_blank" href="https://www.finline.in/resource/cgtmse-loans/">https://www.finline.in/resource/cgtmse-loans/</a></p>
                                        </div>
                                    </li>
									
									<!-- Card 140 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">140) Software Technology Park (STP)</div>
                                        <div class="collapsible-body">
                                            <p>The Software Technology Park (STP) scheme is a totally export-oriented scheme for the development and export of computer software. This includes the export of professional services using communication links or media.
											For more : <a target="_blank" href="https://www.startupindia.gov.in/content/sih/en/government-schemes/software-technology-park-scheme.html">https://www.startupindia.gov.in/content/sih/en/government-schemes/software-technology-park-scheme.html</a></p>
                                        </div>
                                    </li>
									
									<!-- Card 141 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">141) The Venture Capital Assistance Scheme (VCA)</div>
                                        <div class="collapsible-body">
                                            <p>Small Farmer’s Agri-Business Consortium (SFAC) has launched the Venture Capital Assistance (VCA) scheme for the welfare of farmer-entrepreneurs and to develop their agri-business. The scheme is approved by the banks and financial institutions regulated by the RBI. It intends to provide assistance in the form of term loans to farmers so that the latter can meet the capital requirements for their project's implementation. VCA promotes the training and nurturing of agri-entrepreneurs.
											For more : <a target="_blank" href="https://www.ynos.in/products/vcs">https://www.ynos.in/products/vcs</a></p>
                                        </div>
                                    </li>
									
									<!-- Card 142 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">142) Loan For Rooftop Solar Pv Power Projects</div>
                                        <div class="collapsible-body">
                                            <p>To build reliance on non-conventional sources of power, the government of India has decided to set up 40,000 MWp of Grid-Interactive Rooftop Solar PV Plants in the next five years. These rooftop solar PV plants will be set up in residential, commercial, industrial, and institutional sectors in the country and shall range from 1 kWp to 500 kWp in terms of capacity.
											For more : <a target="_blank" href="https://solarrooftop.gov.in/notification/Notification-01092017-145851.pdf">https://solarrooftop.gov.in/notification/Notification-01092017-145851.pdf</a></p>
                                        </div>
                                    </li>
									
									<!-- Card 143 -->
                                    <li class="row document-box-view redborder">
                                        <div class="collapsible-header">143) NewGen Innovation and Entrepreneurship Development Centre (NewGen IEDC)</div>
                                        <div class="collapsible-body">
                                            <p>NewGen IEDC is an initiative launched by the National Science and Technology Entrepreneurship Development Board under the Department of Science and Technology, Government of India. The initiative aims to inculcate the spirit of innovation and entrepreneurship among the Indian youth. It also endeavors to support and encourage entrepreneurship through guidance, mentorship, and support.
											For more : <a target="_blank" href="http://www.newgeniedc-edii.in/">http://www.newgeniedc-edii.in/</a></p>
                                        </div>
                                    </li>
                     </ul>
                     <!-- </div> -->
                  </div>
                  
               </div>
            </div>
         </div>
      </section>
      <!-- END CONTENT -->
   </div>
   <!-- END WRAPPER -->
</div>
<!-- END MAIN -->
<!-- ================================================
   Scripts
   ================================================ -->
<?php $this->load->view('template/footer'); ?>