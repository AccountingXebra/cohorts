<div id="digitalmaket-modal" class="modal modal-set">
   <img class="geen" src="<?php echo base_url(); ?>asset/images/green.png" alt="green">
   <div class="modal-body confirma" style="margin-top:0px !important;">
      <p style="font-size:17px !important; text-align: center;" class="ml-20">You will be redirected to an external website of Windchimes Communications that is a specialist in digital marketing having worked with 300+ Indian and International clients.</p>
   </div>
   <div class="modal-content">
      <div class="modal-footer">
         <div class="row">
            <!--   <div class="col l4 s12 m12"></div> text-right-->
            <div class="col l12 s12 m12 cancel-deactiv">
               <a class=" modal-close btn-flat theme-flat-btn theme-btn theme-btn-large model-cancel " type="button">CANCEL</a>
               <a target="_blank" href="https://windchimes.co.in" class="btn-flat theme-primary-btn theme-btn theme-btn-large  modal-trigger dea modal-close centertext">OK</a>
            </div>
         </div>
      </div>
   </div>
</div>
<div id="buildcommunity-modal" class="modal modal-set">
   <img class="geen" src="<?php echo base_url(); ?>asset/images/green.png" alt="green">
   <div class="modal-body confirma" style="margin-top:0px !important;">
      <p style="font-size:17px !important; text-align: center;" class="ml-20">You will be redirected to an external website of One Cohort where you can make a community for your customers, vendors or employees.</p>
   </div>
   <div class="modal-content">
      <div class="modal-footer">
         <div class="row">
            <!--   <div class="col l4 s12 m12"></div> text-right-->
            <div class="col l12 s12 m12 cancel-deactiv ">
               <a class=" modal-close btn-flat theme-flat-btn theme-btn theme-btn-large model-cancel " type="button">CANCEL</a>
               <a target="_blank" href="https://onecohort.in" class="btn-flat theme-primary-btn theme-btn theme-btn-large  modal-trigger dea modal-close centertext">OK</a>
            </div>
         </div>
      </div>
   </div>
</div>
<div id="check_list" class="modal modal-md" style="max-height:auto; max-width:425px !important;">
	<style>
		@media only screen and (max-width: 600px) {
			.modal-width {
				width: 300px !important;
			}
		}	
		#check_list{
			position: fixed;
			top: 50% !important;
			left: 68%;
		}
		#check_list h5{
			font-size:22px;
			color:#fff;
		}

		#check_list h6{
			font-size:16px;
			color:#fff;
		}

		#check_list p{
		    margin: -28px 0% 0px 45px !important;
			font-size:15px;
			color:#000;
		}

		#check_list .modal-header{
			padding:0 15px;
			margin: 0px 0 20px 0 !important;
		}

		#check_list .modal-body{
			margin-top: 0px !important;
		}

		#check_list .top-head{
			background-color:#ff85a1 !important;
		}

		#progressbar {
		  background-color: transparent;
		  border-radius: 20px;
		  /* (height of inner div) / 2 + padding */
		  padding: 0;
		}

		#progressbar>.user_progress{
			background-color: #fff;
			width: 50%;
			height: 10px;
			border-radius: 10px;
			margin: -16.5px 0 0 40px;
		}

		.progress-per{
			color:#fff;
		}

		.check_point{
			padding:15px 20px;
		}

		.diss-miss{
			color:#000 !important;
			font-size:13px;
		}
		.without-progress{
			display:none !important;
		}
		.with-progress .hide{
			display:none !important;
		}
		.with-progress .hide{
			display:none !important;
		}
	</style>
	<?php 

 $company_profile=$this->Adminmaster_model->selectData('businesslist', '*',array('bus_id'=>$this->user_session['bus_id']));
 $personal_profile=$this->Adminmaster_model->selectData('registration', '*',array('bus_id'=>$this->user_session['bus_id'],'reg_admin_type !='=>3));
 $invoice=$this->Adminmaster_model->selectData('sales_invoices', '*',array('bus_id'=>$this->user_session['bus_id']));
 $expense=$this->Adminmaster_model->selectData('company_expense', '*',array('bus_id'=>$this->user_session['bus_id']));

 $progress=0;
 if(count($company_profile)>0){
$progress+=25;
 }
 if(count($personal_profile)>0){
$progress+=25;
 }
 if(count($invoice)>0){
$progress+=25;
 }
 if(count($expense)>0){
$progress+=25;
 }

	?>

	<div class="modal-content top-head">
      <div class="modal-header without-progress">
		 <h6>Congratulations! You are now ready to use Xebra.</h6>
      </div>
	  <div class="modal-header with-progress">
         <h5>Getting Started with Xebra</h5>
		 <div id="progressbar">
			<span class="progress-per"><?=$progress?>%</span><div class="user_progress"></div>
		 </div>
      </div>
	</div>

	
	<div class="modal-body with-progress">
		<?php if(count($company_profile)>0){?>
		<div class="check_point">
			<img width="25" height="25" src="<?php echo base_url(); ?>public/images/ok-48.png" class="gifblank" alt="correct">
			<p style="text-decoration: line-through;" class="check-label" for="check_2">Update Company Profile</p>
		</div>
	<?php } else{ ?>

        <div class="check_point">
			<img width="25" height="25" src="<?php echo base_url(); ?>public/images/grey-dots.png" class="gifblank" alt="correct">
			<p  class="check-label" for="check_2">Update Company Profile</p>
		</div>
	<?php } ?>


       <?php if(count($personal_profile)>0){?>
		<div class="check_point">
			<img width="25" height="25" src="<?php echo base_url(); ?>public/images/ok-48.png" class="gifblank" alt="correct">
			<p style="text-decoration: line-through;" class="check-label" for="check_2">Create Personal Profile</p>
		</div>
       <?php } else{ ?>
          <div class="check_point">
			<img width="25" height="25" src="<?php echo base_url(); ?>public/images/grey-dots.png" class="gifblank" alt="correct">
			<p class="check-label" for="check_2">Create Personal Profile</p>
		</div>
       	<?php } ?>

       	<?php if(count($invoice)>0){?>

		<div class="check_point">
			<img width="25" height="25" src="<?php echo base_url(); ?>public/images/ok-48.png" class="gifblank" alt="correct">
			<p style="text-decoration: line-through;" class="check-label" for="check_3">Prepare Invoice (My Sales / Billing Documents)</p>
		</div>
         <?php } else{ ?>


		<div class="check_point">
			<img width="25" height="25" src="<?php echo base_url(); ?>public/images/grey-dots.png" class="gifblank" alt="correct">
			<p class="check-label" for="check_3">Prepare Invoice (My Sales / Billing Documents)</p>
		</div>
         	<?php } ?>

         		<?php if(count($expense)>0){?>
		<div class="check_point">
			<img width="25" height="25" src="<?php echo base_url(); ?>public/images/ok-48.png" class="gifblank" alt="correct">
			<p style="text-decoration: line-through;"class="check-label" for="check_4">Record Company Expense (My Expenses)</p>
		</div>
		 <?php } else{ ?>
         <div class="check_point">
			<img width="25" height="25" src="<?php echo base_url(); ?>public/images/grey-dots.png" class="gifblank" alt="correct">
			<p class="check-label" for="check_4">Record Company Expense (My Expenses)</p>
		</div>
         <?php } ?>


	</div>
	<div class="modal-content">
      <div class="modal-footer">
         <div class="row">
            <div class="col l4 s12 m12"></div>
            <div class="col l12 s12 m12 cancel-deactiv text-right">
               <a class="diss-miss modal-close model-cancel" type="button">Dismiss Checklist</a>
            </div>
         </div>
      </div>
   </div>
</div>

<div id="add_historical_data" class="modal modal-md" style="max-height:auto; max-width:45% !important;">
	<style>
		#add_historical_data .close-pop{
			margin-top:-33px;
		}
		#add_historical_data h5{
			font-size:22px;
			color:#000;
			margin-top: 0px !important;
		}

		#add_historical_data .modal-header{
			padding:0 15px;
			margin: 0px 0 20px 0 !important;
		}

		#add_historical_data .modal-body{
			margin-top: 0px !important;
		}

		.progress-per{
			color:#fff;
		}

		.check_point{
			padding:15px 20px;
		}

		.diss-miss{
			color:#000 !important;
			font-size:13px;
		}
		#set_start_period{
			width: 25%;
    	margin: 10px -5px 10px 24px !important;
    	height: 34px !important;
		}
		#set_end_period{
			width: 25%;
    	margin: 10px 10px 10px 24px !important;
    	height: 34px !important;
		}
		.fa-plus{
			color:#7864e9;
			font-size: 16px;
		}
		.label-one{
			font-size: 14px !important;
			color:#000 !important;
			margin: 10px 5px 5px 0px !important;
		}
		.hist_info{
			margin-left: 23px !important;
		}
		#hist-data{
			width:96%;
		}
		#hist-data tbody td{
			border:1px solid #eef2fe;
		}
		.setmonth{
			font-size: 13px;
		}
		.setrevenue{
			background-color: transparent;
    	border: none;
			font-size: 15px;
			color:#000;
			width:95% !important;
		}
	</style>
	<div class="modal-content">
	  	<div class="modal-header">
         <h5><strong>Add Historical Data</strong></h5>
				 <a class="modal-close close-pop"><img src="<?php echo base_url();?>asset/images/popupdelete.png" alt="delete"></a>
      </div>
	</div>
	<div class="modal-body">
		 <input placeholder="START PERIOD" autocomplete="off" name="set_start_period" id="set_start_period" class="date-picker btn-date-bill bil-date icon-calendar-green"/><input placeholder="END PERIOD" autocomplete="off" name="set_end_period" id="set_end_period" class="date-picker btn-date-bill bil-date icon-calendar-red"/>&nbsp;<i class="fa fa-plus" aria-hidden="true"/></i>
		 <div class="row hist_info">
		 		<p class="label-one">Please enter the revenue value in given month</p>
				<table id="hist-data">
					<tbody>
						<tr>
						<td><label class="setmonth">APR-19</label></br><input type="input" class="setrevenue" value="140000"></input></td>
						<td><label class="setmonth">MAY-19</label></br><input type="input" class="setrevenue" value="140000"></input></td>
						<td><label class="setmonth">JUN-19</label></br><input type="input" class="setrevenue" value="140000"></input></td>
						<td><label class="setmonth">JUL-19</label></br><input type="input" class="setrevenue" value="140000"></input></td>
						<td><label class="setmonth">AUS-19</label></br><input type="input" class="setrevenue" value="140000"></input></td>
						<td><label class="setmonth">SEP-19</label></br><input type="input" class="setrevenue" value="140000"></input></td>
						</tr>
						<tr>
						<td><label class="setmonth">OCT-19</label></br><input type="input" class="setrevenue" value="140000"></input></td>
						<td><label class="setmonth">NOV-19</label></br><input type="input" class="setrevenue" value="140000"></input></td>
						<td><label class="setmonth">DEC-19</label></br><input type="input" class="setrevenue" value="140000"></input></td>
						<td><label class="setmonth">JAN-19</label></br><input type="input" class="setrevenue" value="140000"></input></td>
						<td><label class="setmonth">FEB-19</label></br><input type="input" class="setrevenue" value="140000"></input></td>
						<td><label class="setmonth">MAR-19</label></br><input type="input" class="setrevenue" value="140000"></input></td>
						</tr>
					</tbody>
				</table>
		 </div>
	</div>
	<div class="modal-content">
      <div class="modal-footer">
         <div class="row">
            <div class="col l4 s12 m12"></div>
            <div class="col l12 s12 m12 cancel-deactiv text-right">
							<a class=" modal-close btn-flat theme-flat-btn theme-btn theme-btn-large model-cancel" type="button">CANCEL</a>
							<button class="btn-flat theme-primary-btn theme-btn theme-btn-large modal-trigger dea modal-close">SAVE</button>
            </div>
         </div>
      </div>
   </div>
	 <script type="text/javascript">
	   $(document).ready(function(){
			 	$( function() {
     			$( "#set_end_period" ).datepicker({
						format: "MM-yyyy",
		        viewMode: "months",
		        minViewMode: "months",
		        numberOfMonths: 2,
     			});
					$( "#set_start_period" ).datepicker({
						format: "MM-yyyy",
		        viewMode: "months",
		        minViewMode: "months",
		        numberOfMonths: 2,
     			});
				});
		});
		</script>
</div>

<div id="deactive_ev" class="modal" style="width:40% !important;">
   <style>
	#deactive_ev .modal-body p{
		font-size: 17px;
		color: #000;
		font-weight: 400;
	}

	#deactive_ev .modal-body {
		width:97% !important;
		margin-left:12px !important;
	}
    #deactive_ev .input-field label.select-label{
		margin: 0 0 0 12px !important;
    }
   </style>
   <img class="geen" src="<?php echo base_url();?>asset/images/green.png" alt="green">
   <div class="modal-content">
      <div class="modal-header">
         <h4>Delete Expense Voucher</h4>
         <input type="hidden" id="ev" name="ev" value="" />
         <a class="modal-close close-pop"><img src="<?php echo base_url();?>asset/images/popupdelete.png" alt="delete"></a>
      </div>
   </div>
   <div class="modal-body">
      <p>Are you sure you want to Delete this Expense Voucher?</p>
   </div>
   <div class="modal-content">
      <div class="modal-footer">
         <div class="row">
            <div class="col l4 s12 m12"></div>
            <div class="col l12 s12 m12 cancel-deactiv text-center">
               <a class=" modal-close btn-flat theme-flat-btn theme-btn theme-btn-large model-cancel " type="button">CANCEL</a>
               <button class="btn-flat theme-primary-btn theme-btn theme-btn-large  modal-trigger dea modal-close del_exp_vou">Delete</button>
            </div>
         </div>
      </div>
   </div>
</div>
<div id="activate_exp" class="modal">
   <img class="geen" src="<?php echo base_url();?>asset/images/green.png" alt="green">
   <div class="modal-content">
      <div class="modal-header">
         <h4>Activate Expense</h4>
         <input type="hidden" id="act_expid" name="act_expid" value="" />
         <a class="modal-close close-pop"><img src="<?php echo base_url();?>asset/images/popupdelete.png" alt="delete"></a>
      </div>
   </div>
   <div class="modal-body">
      <p>Are you sure you want to activate this Expense?</p>
   </div>
   <div class="modal-content">
      <div class="modal-footer">
         <div class="row">
            <div class="col l4 s12 m12"></div>
            <div class="col l12 s12 m12 cancel-deactiv text-center">
               <a class=" modal-close btn-flat theme-flat-btn theme-btn theme-btn-large model-cancel " type="button">CANCEL</a>
               <button class="btn-flat theme-primary-btn theme-btn theme-btn-large  modal-trigger dea modal-close active_exp_type">Activate</button>
            </div>
         </div>
      </div>
   </div>
</div>
<div id="deactivate_exp" class="modal">
   <img class="geen" src="<?php echo base_url();?>asset/images/green.png" alt="green">
   <div class="modal-content">
      <div class="modal-header">
         <h4>Deactivate Expense</h4>
         <input type="hidden" id="expid" name="expid" value="" />
         <a class="modal-close close-pop"><img src="<?php echo base_url();?>asset/images/popupdelete.png" alt="delete"></a>
      </div>
   </div>
   <div class="modal-body">
      <p>Are you sure you want to deactivate this Expense?</p>
   </div>
   <div class="modal-content">
      <div class="modal-footer">
         <div class="row">
            <div class="col l4 s12 m12"></div>
            <div class="col l12 s12 m12 cancel-deactiv text-center">
               <a class=" modal-close btn-flat theme-flat-btn theme-btn theme-btn-large model-cancel " type="button">CANCEL</a>
               <button class="btn-flat theme-primary-btn theme-btn theme-btn-large  modal-trigger dea modal-close remove_exp_type">Deactivate</button>
            </div>
         </div>
      </div>
   </div>
</div>
<div id="da_expenseform" class="modal">
   <img class="geen" src="<?php echo base_url();?>asset/images/green.png" alt="green">
   <div class="modal-content">
      <div class="modal-header">
         <h4><span class="status_type ei-title"></span> Expense</h4>
         <input type="hidden" id="eform_id" name="eform_id" value="" />
         <a class="modal-close close-pop"><img src="<?php echo base_url();?>asset/images/popupdelete.png" alt="delete"></a>
      </div>
   </div>
   <div class="modal-body">
      <p>Are you sure you want to <span class="status_type"></span> this Expense Form?</p>
   </div>
   <div class="modal-content">
      <div class="modal-footer">
         <div class="row">
            <div class="col l4 s12 m12"></div>
            <div class="col l12 s12 m12 cancel-deactiv text-center">
               <a class=" modal-close btn-flat theme-flat-btn theme-btn theme-btn-large model-cancel " type="button">CANCEL</a>
               <button class="btn-flat theme-primary-btn theme-btn theme-btn-large  modal-trigger dea modal-close active_deactive"><span  class="status_type" id="status_type"></span></button>
            </div>
         </div>
      </div>
   </div>
</div>
<div id="deactive_profile" class="modal modal-set">
   <img class="geen" src="<?php echo base_url();?>asset/images/green.png" alt="green">
   <div class="modal-content">
      <div class="modal-header">
         <h4>Deactivate Profile</h4>
         <input type="hidden" id="profile_id" name="profile_id" value="" />
         <a class="modal-close close-pop"><img src="<?php echo base_url();?>asset/images/popupdelete.png" alt="delete"></a>
      </div>
   </div>
   <div class="modal-body confirma">
      <p>Are you sure you want to deactivate this profile?</p>
   </div>
   <div class="modal-content">
      <div class="modal-footer">
         <div class="row">
            <div class="col l4 s12 m12"></div>
            <div class="col l12 s12 m12 cancel-deactiv text-center">
               <a class=" modal-close btn-flat theme-flat-btn theme-btn theme-btn-large model-cancel " type="button">CANCEL</a>
               <button class="btn-flat theme-primary-btn theme-btn theme-btn-large  modal-trigger dea modal-close deactive_user">Deactivate</button>
            </div>
         </div>
      </div>
   </div>
</div>
<div id="deactive_profile_2" class="modal modal-set">
   <img class="geen" src="<?php echo base_url();?>asset/images/green.png" alt="green">
   <div class="modal-content">
      <div class="modal-header">
         <h4>Deactivate Profile</h4>
         <input type="hidden" id="personal_profile_id_2" name="personal_profile_id_2" value="" />
         <a class="modal-close close-pop"><img src="<?php echo base_url();?>asset/images/popupdelete.png" alt="delete"></a>
      </div>
   </div>
   <div class="modal-body confirma">
      <p>Are you sure you want to deactivate this profile?</p>
   </div>
   <div class="modal-content">
      <div class="modal-footer">
         <div class="row">
            <div class="col l12 s12 m12 cancel-deactiv text-center">
               <a class=" modal-close btn-flat theme-flat-btn theme-btn theme-btn-large model-cancel " type="button">CANCEL</a>
               <button class="btn-flat theme-primary-btn theme-btn theme-btn-large  modal-trigger dea modal-close deactive_user_2">Deactivate</button>
            </div>
         </div>
      </div>
   </div>
</div>
<div id="deactive_company_modal" class="modal modal-set">
   <img class="geen" src="<?php echo base_url();?>asset/images/green.png" alt="green">
   <div class="modal-content">
      <div class="modal-header">
         <h4>Deactivate Company Profile</h4>
         <input type="hidden" id="company_profile" name="company_profile" value="" />
         <a class="modal-close close-pop"><img src="<?php echo base_url();?>asset/images/popupdelete.png" alt="delete"></a>
      </div>
   </div>
   <div class="modal-body confirma">
      <p>Are you sure you want to deactivate this Company profile?</p>
   </div>
   <div class="modal-content">
      <div class="modal-footer">
         <div class="row">
            <div class="col l12 s12 m12 cancel-deactiv text-center">
               <a class=" modal-close btn-flat theme-flat-btn theme-btn theme-btn-large model-cancel " type="button">CANCEL</a>
               <button class="btn-flat theme-primary-btn theme-btn theme-btn-large  modal-trigger dea modal-close deactive_company_status">Deactivate</button>
            </div>
         </div>
      </div>
   </div>
</div>
<div id="active_company_profile" class="modal modal-set">
   <img class="geen" src="<?php echo base_url();?>asset/images/green.png" alt="green">
   <div class="modal-content">
      <div class="modal-header">
         <h4>Re-Activate Company Profile</h4>
         <input type="hidden" id="activate_cmp_profile_id" name="activate_cmp_profile_id" value="" />
         <a class="modal-close close-pop"><img src="<?php echo base_url();?>asset/images/popupdelete.png" alt="delete"></a>
      </div>
   </div>
   <div class="modal-body confirma">
      <p>Are you sure you want to re-activate this Company profile?</p>
   </div>
   <div class="modal-content">
      <div class="modal-footer">
         <div class="row">
            <div class="col l12 s12 m12 cancel-deactiv text-center">
               <a class=" modal-close btn-flat theme-flat-btn theme-btn theme-btn-large model-cancel " type="button">CANCEL</a>
               <button class="btn-flat theme-primary-btn theme-btn theme-btn-large  modal-trigger dea modal-close active_cmp_profile">Re-Activate</button>
            </div>
         </div>
      </div>
   </div>
</div>
<div id="active_profile_2" class="modal modal-set">
   <img class="geen" src="<?php echo base_url();?>asset/images/green.png" alt="green">
   <div class="modal-content">
      <div class="modal-header">
         <h4>Re-activate Profile</h4>
         <input type="hidden" id="activate_personal_profile_id_2" name="activate_personal_profile_id_2" value="" />
         <a class="modal-close close-pop"><img src="<?php echo base_url();?>asset/images/popupdelete.png" alt="delete"></a>
      </div>
   </div>
   <div class="modal-body confirma">
      <p>Are you sure you want to re-activate the profile?</p>
   </div>
   <div class="modal-content">
      <div class="modal-footer">
         <div class="row">
            <div class="col l12 s12 m12 cancel-deactiv text-center">
               <a class=" modal-close btn-flat theme-flat-btn theme-btn theme-btn-large model-cancel " type="button">CANCEL</a>
               <button class="btn-flat theme-primary-btn theme-btn theme-btn-large  modal-trigger dea modal-close active_user_2">Re - activate</button>
            </div>
         </div>
      </div>
   </div>
</div>
<div id="active_profile" class="modal modal-set">
   <img class="geen" src="<?php echo base_url();?>asset/images/green.png" alt="green">
   <div class="modal-content">
      <div class="modal-header">
         <h4>Re-activate Profile</h4>
         <input type="hidden" id="activate_profile_id" name="activate_profile_id" value="" />
         <a class="modal-close close-pop"><img src="<?php echo base_url();?>asset/images/popupdelete.png" alt="delete"></a>
      </div>
   </div>
   <div class="modal-body confirma">
      <p>Are you sure you want to re-activate the profile?</p>
   </div>
   <div class="modal-content">
      <div class="modal-footer">
         <div class="row">
            <div class="col l4 s12 m12"></div>
            <div class="col l12 s12 m12 cancel-deactiv text-center">
               <a class=" modal-close btn-flat theme-flat-btn theme-btn theme-btn-large model-cancel " type="button">CANCEL</a>
               <button class="btn-flat theme-primary-btn theme-btn theme-btn-large  modal-trigger dea modal-close active_user">Re - activate</button>
            </div>
         </div>
      </div>
   </div>
</div>
<div id="select_business" class="modal">
   <img class="geen" src="<?php echo base_url();?>asset/images/green.png" alt="green">
   <div class="modal-content">
      <div class="modal-header">
         <h4>Welcome To Xebra!</h4>
      </div>
   </div>
   <div class="modal-body change-model">
      <p> You can begin customizing by selecting the type of business you are into</p>
   </div>
   <div class="modal-body change-model">
      <div class="row pass-row renter">
         <form class="col s12" name="select_businesstype" id="select_businesstype" method="post">
			<?php $csrf = array(
		'name' => $this->security->get_csrf_token_name(),
		'hash' => $this->security->get_csrf_hash()
	);
	?>
	<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
            <p>
               <input name="radio_type_user" type="radio" id="service" value="1" />
               <label for="service">Service</label>
            </p>
            <p>
               <input name="radio_type_user" type="radio" id="manufacturing" value="2"/>
               <label for="manufacturing">Manufacturing</label>
            </p>
            <p>
               <input name="radio_type_user" type="radio" id="retail" value="3" />
               <label for="retail">Retail/Trading/Franchising</label>
            </p>
         </form>
      </div>
   </div>
   <div class="modal-content">
      <div class="modal-footer">
         <div class="row">
            <div class="col l4 s12 m12"></div>
            <div class="col l12 s12 m12 cancel-deactiv text-center">
               <button class="btn-flat theme-primary-btn theme-btn theme-btn-large  modal-trigger dea modal-close update_businesstype">Save And Countinue</button>
            </div>
         </div>
      </div>
   </div>
</div>
<div id="businesstype_errormsg" class="modal error">
   <img class="geen" src="<?php echo base_url();?>asset/images/green.png" alt="green">
   <div class="modal-content success-msg">
      <p>Select any Business</p>
      <a class="modal-close close-pop pass-suc modal-trigger" href="#select_business" type="button"><img src="<?php echo base_url();?>asset/images/popupdelete.png" alt="delete"></a>
   </div>
</div>
<div id="password-modal" class="modal modal-width">
    <style>
		#change_password_frm .modal-body{
			margin-left: 0px !important;
			width: 100% !important;
		}
		#change_password_frm .error{
			top:-12px !important;
		}
		#password-modal .modal-header {
			padding: 15px 11px 30px 11px !important;
		}

		#change_password_frm .toolattr{
			position: absolute;
			top: 5px;
			right: 7px;
		}
		
		#password-modal input[type=text]:not(.browser-default) {
			font-size: 13px !important;
		}
	</style>
   <img class="geen" src="<?php echo base_url(); ?>asset/images/green.png" alt="green">
   <div class="modal-content">
      <div class="modal-header">
         <h4>Change Password</h4>
         <a class="modal-close close-pop"><img src="<?php echo base_url(); ?>asset/images/popupdelete.png" alt="delete"></a>
      </div>
   </div>
   <form id="change_password_frm" name="change_password_frm" novalidate="novalidate">
		<?php $csrf = array(
			'name' => $this->security->get_csrf_token_name(),
			'hash' => $this->security->get_csrf_hash()
		);
		?>
		<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
		<div class="modal-body deactive-acc">
         <!-- <div class="row border-field account-row">
            <label class="full-bg-label active">Enter 6 DIGIT OTP</label>
            <input id="acc_otp" name="acc_otp" class="full-bg adjust-width" type="text" pl>
            </div> -->
         <div class="col l6 s12 m12 fieldset">
            <div class="input-field" style="border-bottom: 1px solid #eef2fe;">
				<label class="full-bg-label active">Enter Current Password<span class="required_field"> *</span></label>
				<input style="width:80% !important; border:none !important;" name="old_password" id="old_password" class="full-bg adjust-width" type="password" autocomplete="off" maxlength="24">
				<div class="hide-show-1">
					<span style="position:absolute; left:90%; top:30%; cursor: pointer;"><img width="29" height="30" src="<?php echo base_url(); ?>public/images/no-monkey.png" alt="small-eazy"></img></span>
				</div>
            </div>
         </div>
         <div class="row input-set account-row">
            <div class="col l6 s12 m6 fieldset">
			   <div class="input-field" style="border-right: 1px solid #eef2fe;">
				  <div class="toolattr">
					<a class="info-ref tooltipped info-tooltipped" data-position="bottom" data-delay="50" data-tooltip="Minimum 8 Characters, 1 in Uppercase, 1 Number & 1 Special Character"></a>
				  </div>
                  <label class="full-bg-label active">Enter New Password<span class="required_field"> *</span></label>
                  <input style="width:65% !important; border:none !important;" name="new_password" id="new_password" class="full-bg adjust-width gstin" type="password" autocomplete="off" maxlength="24">
				  <div class="hide-show-2">
					<span style="position:absolute; left:80%; top:30%; cursor: pointer;"><img width="29" height="30" src="<?php echo base_url(); ?>public/images/no-monkey.png" alt="small-eazy"></img></span>
				  </div>
               </div>
            </div>
            <div class="col l6 s12 m6 fieldset">
               <div class="input-field">
                  <label class="full-bg-label active">Confirm New Password<span class="required_field"> *</span></label>
                  <input style="width:65% !important; border:none !important;" name="confirm_password" id="confirm_password" class="full-bg adjust-width" type="password" autocomplete="off" maxlength="24">
				  <div class="hide-show-3">
					<span style="position:absolute; left:80%; top:30%; cursor: pointer;"><img width="29" height="30" src="<?php echo base_url(); ?>public/images/no-monkey.png" alt="small-eazy"></img></span>
				  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="modal-content">
         <div class="modal-footer">
            <div class="row">
               <div class="col l4 s12 m12"></div>
               <div class="col l12 s12 m12 cancel-deactiv text-right">
                  <a class=" modal-close btn-flat theme-flat-btn theme-btn theme-btn-large model-cancel " type="button">CANCEL</a>
                  <input type="submit" name="submit" value="CHange Password" class="btn-flat theme-primary-btn theme-btn theme-btn-large modal-trigger dea enter_otp_submition">
               </div>
            </div>
         </div>
      </div>
   </form>
</div>
<div id="password-otp-modal" class="modal modal-width">
   <img class="geen" src="<?php echo base_url(); ?>asset/images/green.png" alt="green">
   <div class="modal-content">
      <div class="modal-header">
         <h4>Change Password</h4>
         <a class="modal-close close-pop"><img src="<?php echo base_url(); ?>asset/images/popupdelete.png" alt="delete"></a>
      </div>
   </div>
   <!--  <form id="send_otp_mail" name="send_otp_mail" novalidate="novalidate"> -->
   <div class="modal-body deactive-acc">
      <div class="input-field mb-20">
         <p class="ml-20">Send OTP Via</p>
         <!-- <label for="company_name" class="full-bg-label">Company Name</label>
            <input id="company_name" name="bus_company_name" class="full-bg adjust-width" type="text" value=""> -->
      </div>
      <div class="row border-field account-row">
         <div class="col s12 m12 l12">
            <div class="col s12 m12 l6 account-checkbox">
               <div class="row chkbox">
                  <input type="checkbox" id="mobile_no_pwd" name="mobile_no_pwd" value="Yes" class="sent-otp filled-in not_taxable  full-bg adjust-width gstin">
                  <label for="mobile_no_pwd"  class="account-lable" style="padding-left: 30px;">Registered Mobile Number</label>
               </div>
            </div>
            <div class="col s12 m12 l6 account-checkbox-right">
               <div class="row chkbox">
                  <input type="checkbox" id="email_address_pwd" name="email_address_pwd" value="Yes" class="sent-otp filled-in not_taxable ">
                  <label for="email_address_pwd" class="account-lable" style="padding-left: 30px;">Registered Email Address</label>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="modal-content">
      <div class="modal-footer">
         <div class="row">
            <div class="col l4 s12 m12"></div>
            <div class="col l12 s12 m12 cancel-deactiv text-right">
               <a class=" modal-close btn-flat theme-flat-btn theme-btn theme-btn-large model-cancel " type="button">CANCEL</a>
               <a class="btn-flat theme-primary-btn theme-btn theme-btn-large modal-trigger dea sent_changepwd_otp">SENT OTP</a>
            </div>
         </div>
      </div>
   </div>
   <!-- </form> -->
</div>
<div id="password_enter_otp_modal" class="modal modal-width">
   <img class="geen" src="<?php echo base_url(); ?>asset/images/green.png" alt="green">
   <div class="modal-content">
      <div class="modal-header">
         <h4>Change Password</h4>
         <a class="modal-close close-pop"><img src="<?php echo base_url(); ?>asset/images/popupdelete.png" alt="delete"></a>
      </div>
   </div>
   <form id="pwd_otp_mail" name="pwd_otp_mail" novalidate="novalidate">
	<?php $csrf = array(
		'name' => $this->security->get_csrf_token_name(),
		'hash' => $this->security->get_csrf_hash()
	);
	?>
	<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
      <div class="modal-body deactive-acc">
         <div class="input-field mb-20">
            <p class="par-otp ml-20" id="otp_pwd_sentense"></p>
         </div>
         <div class="col l6 s12 m12 fieldset">
            <div class="input-field">
               <label class="full-bg-label active">Enter 6 DIGIT OTP</label>
               <input name="acc_pwdotp" id="acc_pwdotp" class="full-bg adjust-width" type="text">
            </div>
         </div>
      </div>
      <div class="modal-content">
         <div class="modal-footer">
            <div class="row">
               <div class="col l4 s12 m12"></div>
               <div class="col l12 s12 m12 cancel-deactiv text-right">
                  <a class=" modal-close btn-flat theme-flat-btn theme-btn theme-btn-large model-cancel " type="button">CANCEL</a>
                  <input type="submit" name="submit" value="CONFIRM" class="btn-flat theme-primary-btn theme-btn theme-btn-large modal-trigger dea pwd_otp_submition">
               </div>
            </div>
         </div>
      </div>
   </form>
</div>
<div id="change_pass_success" class="modal">
   <img class="geen" src="<?php echo base_url();?>asset/images/green.png" alt="green">
   <div class="modal-content error-msg" style="text-align:center;">
      <p>Your password has been changed. Please re-login to Xebra</p>
      <a class="modal-close close-pop pass-suc modal-trigger" href="#password-modal" type="button"><img src="<?php echo base_url();?>asset/images/popupdelete.png" alt="delete"></a>
   </div>
</div>

<div id="change_pass_error" class="modal error" style="width:30% !important;">
   <img class="geen" src="<?php echo base_url();?>asset/images/green.png" alt="green">
   <div class="modal-content error-msg">
      <p style="font-size:13px !important;">Your current password doesn't match. Try Again</p>
      <a class="modal-close close-pop pass-suc modal-trigger" href="#password-modal" type="button"><img src="<?php echo base_url();?>asset/images/popupdelete.png" alt="delete"></a>
   </div>
</div>
<div id="otp_error" class="modal error">
   <img class="geen" src="<?php echo base_url();?>asset/images/green.png" alt="green">
   <div class="modal-content error-msg">
      <p>You might have entered wrong OTP.Try Again!</p>
      <a class="modal-close close-pop pass-suc"><img src="<?php echo base_url();?>asset/images/popupdelete.png" alt="delete"></a>
   </div>
</div>
<div id="change-password-notification" class="modal success">
   <img class="geen" src="<?php echo base_url();?>asset/images/green.png" alt="green">
   <div class="modal-content success-msg">
      <p>Password Changed Successfully</p>
      <a class="modal-close close-pop pass-suc"><img src="<?php echo base_url();?>asset/images/popupdelete.png" alt="delete"></a>
   </div>
</div>
<div id="pass-succ" class="modal Password-change">
   <img class="geen" src="<?php echo base_url();?>asset/images/green.png" alt="green">
   <div class="modal-content">
      <div class="modal-header">
         <h4>OTP Confirmation</h4>
         <a class="modal-close close-pop"><img src="<?php echo base_url();?>asset/images/popupdelete.png" alt="delete"></a>
      </div>
   </div>
   <div class="modal-body change-model">
      <div class="row pass-row otp">
         <p>An OTP has successfully send to you registered Address, Enter it below to Change Password</p>
      </div>
   </div>
   <form class="col s12" name="otp_password_frm" id="otp_password_frm" method="post">
	<?php $csrf = array(
		'name' => $this->security->get_csrf_token_name(),
		'hash' => $this->security->get_csrf_hash()
	);
	?>
	<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
      <div class="modal-body change-model">
         <div class="row pass-row renter">
            <div class="input-field">
               <label for="password_otp" class="Password-chan">ENTER 6 DIGIT OTP</label>
               <input id="password_otp" name="password_otp" class="enter" type="text">
            </div>
         </div>
         <div class="row pass-row renter">
         </div>
      </div>
      <div class="modal-content">
         <div class="modal-footer">
            <div class="row">
               <div class="col l4 s12 m12"></div>
               <div class="col l12 s12 m12 cancel-deactiv text-center">
                  <a class=" modal-close btn-flat theme-flat-btn theme-btn theme-btn-large model-cancel " type="button">CANCEL</a>
                  <button class="btn-flat theme-primary-btn theme-btn theme-btn-large  modal-trigger"  type="submit">Change Password</button>
               </div>
            </div>
         </div>
      </div>
   </form>
</div>
<div id="feedback_modal" class="modal modal-feedback" style="max-height:100%;height:auto; width:56%;">
   <img class="geen" src="<?php echo base_url();?>asset/images/green.png" alt="green">
   <form name="feedback_frm" id="feedback_frm" method="post">
      <div class="modal-content">
         <div class="modal-header">
            <a class="modal-close close-pop"><img src="<?php echo base_url();?>asset/images/popupdelete.png" alt="delete"></a>
         </div>
      </div>
      <div class="modal-content" style="padding: 24px 0 0 0;">
         <div class="row">
            <h4 style="width: unset;margin: 0 0 0 0px;font-size: 20px;font-weight: 600;text-align: center;">How likely are you to recommend Xebra Cohorts to a friend or colleague?</h4>
         </div>
         <div class="row">
            <div class="col l10 s12 m12 offset-l1">
               <!-- offset-s6 -->
               <div class="rating-feedback">
                  <label class="radio-lable">
                  <input name="rate" class="rate" type="radio" value="1"/>
                  <span>1</span>
                  </label>
                  <label class="radio-lable">
                  <input name="rate" class="rate" type="radio" value="2"/>
                  <span>2</span>
                  </label>
                  <label class="radio-lable">
                  <input name="rate" class="rate" type="radio"  value="3"/>
                  <span>3</span>
                  </label>
                  <label class="radio-lable">
                  <input name="rate" class="rate" type="radio"value="4" />
                  <span>4</span>
                  </label>
                  <label class="radio-lable">
                  <input name="rate" class="rate" type="radio"   value="5"/>
                  <span>5</span>
                  </label>
                  <div class="">
                     <i class="Medium material-icons" style="    margin-top: -8px;">thumb_down</i>
                     <i class="Medium material-icons" style=" float:right;margin-top: -8px;">thumb_up</i>
                  </div>
               </div>
               <span id="feedback_error"></span>
            </div>
         </div>



         <div class="row">
            <div class="col l11 s12 m12" style="margin-left: 8.333333%; width:82.5%;">
               <p class="title-note" style="margin-bottom: 15px;">Please tell us why you rated Xebra Cohorts <span id="rate_ratio"></span> out of 5?</p>
               <div class="row notess note-mor">
                  <div class="s12 m12 l12">
                     <div class="note">
                        <textarea id="bill1" placeholder="WRITE YOUR FEEDBACK HERE" type="text" name="feedback" class="bill-box ex-noss materialize-textarea feedbacktextbox" style="height: 54px; min-height: 6rem; font-size:15.5px !important; margin-bottom:0;"></textarea>
                        <label for="bill1"></label>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="modal-footer" style="margin-top:-15px;">
         <div class="row" style="margin-bottom:0px !important;">
            <div class="col l11 s12 m12 cancel-deactiv pull-right" style="margin-left: -9px;">
               <a class=" modal-close btn-flat theme-flat-btn theme-btn theme-btn-large model-cancel " type="button">CANCEL</a>
               <button class="btn-flat theme-primary-btn theme-btn theme-btn-large  modal-trigger dea invite_customer">SUBMIT</button>
            </div>
         </div>
      </div>
   </form>
</div>



</div>
<!-- <div id="add_gst_frm" class="modal add_gst_modal">
   <img class="geen" src="<?php //echo base_url();?>asset/images/green.png">

   <div class="modal-content">

     <div class="modal-header">

       <h4>GSTIN Information</h4>

       <a class="modal-close close-pop"><img src="<?php //echo base_url();?>asset/images/popupdelete.png"></a>

     </div></div>

     <div class="modal-body change-model">

       <div class="row pass-row otp">

         <p>Insert your Branch and GST Information</p>

       </div>

     </div>
         <form class="col s12" name="add_gst_from" id="add_gst_from" method="post">

             <div class="modal-body change-model">

               <div class="row pass-row renter">

                 <div class="input-field">

                   <label for="branch_name" class="branch_name">Branch Name</label>

                   <input id="branch_name" name="branch_name" class="enter" type="text">

                 </div>

               </div>

            <div class="row pass-row renter">

                     <div class="input-field">

                       <label for="gst_no" class="gst_no">GST NO</label>

                       <input id="gst_no" name="gst_no" class="enter" type="text">

                   </div>

               </div>

           <div class="row pass-row renter">

                     <div class="input-field">

                       <label for="gst_perc" class="gst_perc">GST %</label>

                       <input id="gst_perc" name="gst_perc" class="enter" type="text">

                   </div>

               </div>

               <div class="row pass-row renter">
               </div>

             </div>

             <div class="modal-content">

             <div class="modal-footer">

               <div class="row">

                 <div class="col l4 s12 m12"></div>

                 <div class="col l8 s12 m12 cancel-deactiv">

                   <a class=" modal-close btn-flat theme-flat-btn theme-btn theme-btn-large model-cancel " type="button">CANCEL</a>

                   <button class="btn-flat theme-primary-btn theme-btn theme-btn-large  modal-trigger"  type="submit">Submit</button>

                 </div>

               </div>

             </div>

           </div>
         </form>
   </div> -->
<div id="add_gst_frm" class="modal add_gst_modal modal-md">
   <img class="geen" src="<?php echo base_url();?>asset/images/green.png" alt="green">
   <div class="modalheader">
      <h4>Add new GSTIN</h4>
      <a class="modal-close close-pop"><img src="<?php echo base_url();?>asset/images/popupdelete1.png" alt="delete"></a>
   </div>
   <div class="modalbody">
      <form class="addgstin" name="add_gst_from" id="add_gst_from" method="post">
		<?php $csrf = array(
		'name' => $this->security->get_csrf_token_name(),
		'hash' => $this->security->get_csrf_hash()
	);
	?>
	<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
         <div class="row gstrow">
            <div class="col l10 s10 m10 fieldset">
               <div class="row">
                  <div class="col l6 s12 m6 fieldset">
                     <div class="input-field">
                        <a class="tooltipped info-tooltipped" data-position="bottom" data-delay="50" data-tooltip="Will be update, Coming Soon..!"></a>
                        <label class="full-bg-label active">GSTIN</label>
                        <input type="hidden" name="gst_array[]" value="1" />
                        <input name="gstin1" id="gstin1" class="full-bg adjust-width gstin" type="text">
                     </div>
                  </div>
                  <div class="col l6 s12 m6 fieldset">
                     <div class="input-field">
                        <a class="tooltipped info-tooltipped" data-position="bottom" data-delay="50" data-tooltip="Will be update, Coming Soon..!"></a>
                        <label class="full-bg-label active">PLACE OF SUPPLY</label>
                        <input id="location1" name="location1" class="full-bg adjust-width" type="text">
                     </div>
                  </div>
               </div>
            </div>
            <div class="col l2 s2 m2 fieldset">
               <div class="col l12 s12 m12 fieldset">
                  <a class="add-remove-btn"><i class="material-icons  rem-red">remove</i></a>
               </div>
            </div>
         </div>
         <div id="rep-element">
         </div>
         <div class="row">
            <div class="col l10 s10 m10 fieldset empty-add-set"></div>
            <div class="col l2 s2 m2 fieldset">
               <div class="col l12 s12 m12 fieldset">
                  <a class="add-remove-btn add-icon addgstrow"><i class="material-icons add-bloue">add</i></a>
               </div>
            </div>
         </div>
         <div class="row">
            <div class="col l6 m6 s12 fieldset">
               <a href="#" class="findgstlink">Find GSTIN (India)</a>
            </div>
            <div class="col l6 m6 s12 fieldset buttonset">
               <div class="right">
                  <button class="btn-flat theme-primary-btn theme-btn theme-btn-large right">Save</button>
                  <button type="button" class="modal-close btn-flat theme-flat-btn theme-btn theme-btn-large">CANCEL</button>
               </div>
            </div>
         </div>
      </form>
   </div>
</div>
<div id="add_gst_frm_2" class="modal add_gst_modal">
   <img class="geen" src="<?php echo base_url();?>asset/images/green.png" alt="green">
   <div class="modal-content">
      <div class="modal-header">
         <h4>GSTIN Information</h4>
         <a class="modal-close close-pop"><img src="<?php echo base_url();?>asset/images/popupdelete.png" alt="delete"></a>
      </div>
   </div>
   <div class="modal-body change-model">
      <div class="row pass-row otp">
         <p>Insert your Branch and GST Information</p>
      </div>
   </div>
   <form class="col s12" name="add_gst_from_2" id="add_gst_from_2" method="post">
	<?php $csrf = array(
		'name' => $this->security->get_csrf_token_name(),
		'hash' => $this->security->get_csrf_hash()
	);
	?>
	<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
      <div class="modal-body change-model">
         <div class="row pass-row renter">
            <div class="input-field">
               <input type="hidden" name="company_id_gst_2" id="company_id_gst_2" value=""/>
               <!--<input type="hidden" name="user_id_gst_2" id="user_id_gst_2" value=""/>-->
               <label for="branch_name_gst_2" class="branch_name">Branch Name</label>
               <input id="branch_name_gst_2" name="branch_name_gst_2" class="enter" type="text">
            </div>
         </div>
         <div class="row pass-row renter">
            <div class="input-field">
               <label for="gst_no_gst_2" class="gst_no">GST NO</label>
               <input id="gst_no_gst_2" name="gst_no_gst_2" class="enter" type="text">
            </div>
         </div>
         <div class="row pass-row renter">
            <div class="input-field">
               <label for="gst_perc_gst_2" class="gst_perc">GST %</label>
               <input id="gst_perc_gst_2" name="gst_perc_gst_2" class="enter" type="text">
            </div>
         </div>
         <div class="row pass-row renter">
         </div>
      </div>
      <div class="modal-content">
         <div class="modal-footer">
            <div class="row">
               <div class="col l4 s12 m12"></div>
               <div class="col l8 s12 m12 cancel-deactiv">
                  <a class=" modal-close btn-flat theme-flat-btn theme-btn theme-btn-large model-cancel " type="button">CANCEL</a>
                  <button class="btn-flat theme-primary-btn theme-btn theme-btn-large  modal-trigger"  type="submit">Submit</button>
               </div>
            </div>
         </div>
      </div>
   </form>
</div>
<div id="edit_gst_frm" class="modal edit_gst_modal">
   <img class="geen" src="<?php echo base_url();?>asset/images/green.png" alt="green">
   <div class="modal-content">
      <div class="modal-header">
         <h4>GSTIN Information</h4>
         <a class="modal-close close-pop"><img src="<?php echo base_url();?>asset/images/popupdelete.png" alt="delete"></a>
      </div>
   </div>
   <div class="modal-body change-model">
      <div class="row pass-row otp">
         <p>Update your Branch and GST Information</p>
      </div>
   </div>
   <form class="col s12" name="edit_gst_from" id="edit_gst_from" method="post">
	<?php $csrf = array(
		'name' => $this->security->get_csrf_token_name(),
		'hash' => $this->security->get_csrf_hash()
	);
	?>
	<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
      <div class="modal-body change-model">
         <div class="row pass-row renter">
            <div class="input-field">
               <input type="hidden"  name="id_edit_gst" id="id_edit_gst" value=""/>
               <input type="hidden"  name="company_id" id="company_id" value=""/>
               <input type="hidden"  name="user_id" id="user_id" value=""/>
               <label for="branch_name" class="branch_name">Branch Name</label>
               <input id="branch_name_edit_gst" name="branch_name_edit_gst" class="enter" type="text">
            </div>
         </div>
         <div class="row pass-row renter">
            <div class="input-field">
               <label for="gst_no" class="gst_no">GST NO</label>
               <input id="gst_no_edit_gst" name="gst_no_edit_gst" class="enter" type="text">
            </div>
         </div>
         <div class="row pass-row renter">
            <div class="input-field">
               <label for="gst_perc" class="gst_perc">GST %</label>
               <input id="gst_perc_edit_gst" name="gst_perc_edit_gst" class="enter" type="text">
            </div>
         </div>
         <div class="row pass-row renter">
         </div>
      </div>
      <div class="modal-content">
         <div class="modal-footer">
            <div class="row">
               <div class="col l4 s12 m12"></div>
               <div class="col l8 s12 m12 cancel-deactiv">
                  <a class=" modal-close btn-flat theme-flat-btn theme-btn theme-btn-large model-cancel " type="button">CANCEL</a>
                  <button class="btn-flat theme-primary-btn theme-btn theme-btn-large  modal-trigger"  type="submit">Submit</button>
               </div>
            </div>
         </div>
      </div>
   </form>
</div>
<div id="da_customer_model" class="modal modal-set">
   <img class="geen" src="<?php echo base_url();?>asset/images/green.png" alt="green">
   <div class="modal-content">
      <div class="modal-header">
         <h4><span class="cstatus_type ei-title"></span> Client</h4>
         <a class="modal-close close-pop"><img src="<?php echo base_url();?>asset/images/popupdelete.png" alt="delete"></a>
      </div>
   </div>
   <div class="modal-body confirma">
      <p>Are you sure you want to <span class="cstatus_type"></span> this Client?</p>
   </div>
   <div class="modal-content">
      <div class="modal-footer">
         <div class="row">
            <input type="hidden" id="ccustomer_id" name="ccustomer_id" value="" />
            <div class="col l4 s12 m12"></div>
            <div class="col l12 s12 m12 cancel-deactiv text-center">
               <a class=" modal-close btn-flat theme-flat-btn theme-btn theme-btn-large model-cancel " type="button">CANCEL</a>
               <button class="btn-flat theme-primary-btn theme-btn theme-btn-large  modal-trigger dea modal-close active_deactive_customer"><span class="cstatus_type" id="cstatus_type"></span></button>
            </div>
         </div>
      </div>
   </div>
</div>
<div id="remove_cust_legal_doc" class="modal modal-set">
   <img class="geen" src="<?php echo base_url();?>asset/images/green.png" alt="green">
   <div class="modal-content">
      <div class="modal-header">
         <h4>Remove Legal Document</h4>
         <input type="hidden" id="cust_legal_doc_id" name="cust_legal_doc_id" value="" />
         <a class="modal-close close-pop"><img src="<?php echo base_url();?>asset/images/popupdelete.png" alt="delete"></a>
      </div>
   </div>
   <div class="modal-body confirma">
      <p>Are you sure you want to remove this legal document?</p>
   </div>
   <div class="modal-content">
      <div class="modal-footer">
         <div class="row">
            <div class="col l12 s12 m12 cancel-deactiv text-center">
               <a class=" modal-close btn-flat theme-flat-btn theme-btn theme-btn-large model-cancel " type="button">CANCEL</a>
               <button class="btn-flat theme-primary-btn theme-btn theme-btn-large  modal-trigger dea modal-close remove_legal_doc">Remove</button>
            </div>
         </div>
      </div>
   </div>
</div>
<div id="remove_cust_po_doc" class="modal modal-set">
   <img class="geen" src="<?php echo base_url();?>asset/images/green.png" alt="green">
   <div class="modal-content">
      <div class="modal-header">
         <h4>Remove Purchase Order Document</h4>
         <input type="hidden" id="cust_po_doc_id" name="cust_po_doc_id" value="" />
         <a class="modal-close close-pop"><img src="<?php echo base_url();?>asset/images/popupdelete.png" alt="delete"></a>
      </div>
   </div>
   <div class="modal-body confirma">
      <p>Are you sure you want to remove this Purchase Order document?</p>
   </div>
   <div class="modal-content">
      <div class="modal-footer">
         <div class="row">
            <div class="col l12 s12 m12 cancel-deactiv text-center">
               <a class=" modal-close btn-flat theme-flat-btn theme-btn theme-btn-large model-cancel " type="button">CANCEL</a>
               <button class="btn-flat theme-primary-btn theme-btn theme-btn-large  modal-trigger dea modal-close remove_po_doc">Remove</button>
            </div>
         </div>
      </div>
   </div>
</div>
<div id="remove_cust_other_doc" class="modal modal-set">
   <img class="geen" src="<?php echo base_url();?>asset/images/green.png" alt="green">
   <div class="modal-content">
      <div class="modal-header">
         <h4>Remove Other Document</h4>
         <input type="hidden" id="cust_other_doc_id" name="cust_other_doc_id" value="" />
         <a class="modal-close close-pop"><img src="<?php echo base_url();?>asset/images/popupdelete.png" alt="delete"></a>
      </div>
   </div>
   <div class="modal-body confirma">
      <p>Are you sure you want to remove this document?</p>
   </div>
   <div class="modal-content">
      <div class="modal-footer">
         <div class="row">
            <div class="col l12 s12 m12 cancel-deactiv text-center">
               <a class=" modal-close btn-flat theme-flat-btn theme-btn theme-btn-large model-cancel " type="button">CANCEL</a>
               <button class="btn-flat theme-primary-btn theme-btn theme-btn-large  modal-trigger dea modal-close remove_other_doc">Remove</button>
            </div>
         </div>
      </div>
   </div>
</div>

<div id="addbranch" class="modal modal-md" style="margin-top:-3% !important;">
   <style>
	#addbranch .select-wrapper {
		height:55px !important;
	}

	.branch-add .dropdown-content{
		height:40vh !important;
		overflow-y: scroll !important;
	}

	.add-branch-curr .dropdown-content{
		height:30vh !important;
		overflow-y: scroll !important;
	}

	#addbranch .label-active.input-field label.select-label {
		padding-bottom: 4px !important;
	}
   </style>


   <div class="modalheader">
      <h4>Add New Bank Details</h4>
      <a class="modal-close close-pop"><img src="<?php echo base_url();?>asset/images/popupdelete1.png" alt="delete"></a>
   </div>
   <div class="modalbody">
      <form class="addgstin" id="bank_details" name="bank_details" method="post">
		<?php $csrf = array(
		'name' => $this->security->get_csrf_token_name(),
		'hash' => $this->security->get_csrf_hash()
	);
	?>
	<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
         <div class="row gstrow">
            <div class="col l12 s12 m12 fieldset">
               <div class="col l6 s12 m6 fieldset">
                  <div class="input-field">
                     <label for="ac_number" class="full-bg-label">Enter Bank Account Number <span class="required_field">*</span></label>
                     <input name="ac_number" id="ac_number" class="full-bg adjust-width gstin numeric_number" type="text">
                  </div>
               </div>
               <div class="col l6 s12 m6 fieldset">
                  <div class="input-field">
                     <label class="full-bg-label" for="bank_name">Enter Bank Name <span class="required_field">*</span></label>
                     <input id="bank_name" name="bank_name" class="full-bg adjust-width" type="text">
                  </div>
               </div>
            </div>
         </div>
         <div class="row gstrow">
            <div class="col l12 s12 m12 fieldset">
				<div class="col l6 s12 m6 input-set border-bottom">
					<div class="input-field label-active" style="margin-left:5px;">
						<label for="acc_type" class="full-bg-label select-label">Select Account Type<span class="required_field">*</span></label>
						<select id="acc_type" class="country-dropdown check-label" name="acc_type">
                     <option></option>
							<option value="Current">Current</option>
							<option value="Savings">Savings</option>
							<option value="Loan">Loan</option>
							<option value="Cash Credit">Cash Credit</option>
						</select>
					</div>
				</div>
				<div class="col l6 s12 m6 fieldset">
				   <div class="input-field">
					  <label for="bank_branch_name" class="full-bg-label">Enter Branch Name</label>
					  <input id="bank_branch_name" name="bank_branch_name" class="full-bg adjust-width border-top-none " type="text">
				   </div>
				</div>
			</div>
         </div>
         <div class="row gstrow">
            <div class="col l12 s12 m12 fieldset">
			   <div class="col l6 s12 m6 input-set border-bottom">
               <div class="input-field" >
                   <label for="bank_country" class="full-bg-label select-label">COUNTRY<span class="required_field">*</span></label>
                   <select class="country-dropdown branch-add check-label" name="bus_bank_country" id="bank_country">
                      <option value=""></option>
                     <?php foreach ($countries as $country) { ?>
                         <option value="<?php echo $country->country_id; ?>"><?php echo $country->country_name; ?></option>
                      <?php } ?>
                   </select>
               </div>
			</div>
            <div class="col l6 s12 m6 fieldset">
               <div class="input-field">
                 <label class="full-bg-label" for="ifsc_code">Enter IFSC Code<span class="required_field">*</span></label>
                 <input id="ifsc_code" name="ifsc_code" class="full-bg adjust-width" type="text">
               </div>
            </div>
			   </div>
         </div>
         <div class="row gstrow">
            <div class="col l12 s12 m12 fieldset">
				<div class="col l6 s12 m6 input-set border-bottom">
				   <div class="input-field" >
					   <label for="bank_country" class="full-bg-label select-label">Currency<span class="required_field">*</span></label>
					   <select class="country-dropdown add-branch-curr check-label" name="bank_cur_check" id="bank_cur_check">
						  <option value=""></option>
					   </select>
				   </div>
				</div>
            <div class="col l6 s12 m6 fieldset">
               <div class="input-field">
                 <label class="full-bg-label" for="swift_code">Enter Swift Code</label> <input id="swift_code" name="swift_code" class="full-bg adjust-width" type="text">
               </div>
            </div>
			</div>
		 </div>
		 <?php if ($this->router->fetch_method() == "edit_company_profile" || $this->router->fetch_method() == "add_company_profile") { ?>
		 <div class="row gstrow">
			<div class="col s12 m12 l12 fieldset">
                <div class="col l6 s12 m6 fieldset">
                  <div class="input-field" style="border-right:1px solid #eef2fe;">
                     <label for="opening_bank_balance" class="full-bg-label">Opening Bank Balance<span class="required_field">*</span></label>
                     <input id="opening_bank_balance" name="opening_bank_balance" class="full-bg adjust-width border-top-none" type="text" autocomplete="off">
                  </div>
				</div>
				<div class="col l6 s12 m6 fieldset border-bottom border-right-none">
					<div class="input-field">
						<label for="opening_balance_date" class="full-bg-label">Opening Bank Balance Date <span class="required_field">*</span></label>
						<input id="opening_balance_date" name="opening_balance_date" class="full-bg adjust-width border-top-none bdatepicker icon-calendar-gray" type="text" autocomplete="off">
					</div>
				</div>
            </div>
		 </div>
		 <?php } ?>

         <div class="row gstrow">
            <div class="col l12 s12 m12 fieldset">

            </div>
         </div>
         <div class="row">
            <div class="col l6 m6 s12 fieldset">
            </div>
            <div class="col l6 m6 s12 fieldset buttonset">
               <div class="right">
                  <button type="submit" class="btn-flat theme-primary-btn theme-btn theme-btn-large right">Save</button>
                  <button type="button" class="modal-close btn-flat theme-flat-btn theme-btn theme-btn-large">CANCEL</button>
               </div>
            </div>
         </div>
      </form>
   </div>
</div>

<div id="addbank_with_existing" class="modal modal-md" style="margin-top:-3% !important;">
   <style>
	#addbank_with_existing .select-wrapper {
		height:60px !important;
	}

	.branch-add .dropdown-content{
		height:40vh !important;
		overflow-y: scroll !important;
	}

	.add-branch-curr .dropdown-content{
		height:30vh !important;
		overflow-y: scroll !important;
	}
   </style>
   <div class="modalheader">
      <h4>Add New Bank Details</h4>
      <a class="modal-close close-pop"><img src="<?php echo base_url();?>asset/images/popupdelete1.png" alt="delete"></a>
   </div>
   <div class="modalbody">
      <form class="addgstin" id="addbank_with_existing_frm" name="addbank_with_existing_frm" method="post">
		<?php $csrf = array(
		'name' => $this->security->get_csrf_token_name(),
		'hash' => $this->security->get_csrf_hash()
	);
	?>
	<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
         <input name="bank_bus_id" id="bank_bus_id" type="hidden">
         <input name="bank_reg_id" id="bank_reg_id" type="hidden">
         <div class="row gstrow">
            <div class="col l12 s12 m12 fieldset">
               <div class="col l6 s12 m6 fieldset">
                  <div class="input-field">
                     <label for="ac_number" class="full-bg-label">Enter Bank Account Number <span class="required_field">*</span></label>
                     <input name="ac_number" id="ac_number" class="full-bg adjust-width gstin numeric_number" type="text">
                  </div>
               </div>
               <div class="col l6 s12 m6 fieldset">
                  <div class="input-field">
                     <label class="full-bg-label" for="bank_name">Enter Bank Name <span class="required_field">*</span></label>
                     <input id="bank_name" name="bank_name" class="full-bg adjust-width" type="text">
                  </div>
               </div>
            </div>
         </div>
         <div class="row gstrow">
            <div class="col l6 s12 m6 input-set border-bottom">
               <div class="row">
                  <div class="input-field label-active">
                     <label for="acc_type" class="full-bg-label select-label">Select Account Type<span class="required_field">*</span></label>
                     <select id="acc_type" class="country-dropdown check-label" name="acc_type">
                        <option value="Current">Current</option>
                        <option value="Savings">Savings</option>
                        <option value="Loan">Loan</option>
                        <option value="Cash Credit">Cash Credit</option>
                     </select>
                  </div>
               </div>
            </div>
            <div class="col l6 s12 m6 fieldset">
               <div class="input-field">
                  <label for="bank_branch_name" class="full-bg-label">Enter Branch Name</label>
                  <input id="bank_branch_name" name="bank_branch_name" class="full-bg adjust-width border-top-none " type="text" style="height:46px !important;">
               </div>
            </div>
         </div>
         <div class="row gstrow">
            <div class="col l6 s12 m6 input-set border-bottom">
               <div class="input-field">
                   <label for="bank_country" class="full-bg-label select-label">COUNTRY<span class="required_field">*</span></label>
                   <select class="country-dropdown branch-add check-label" name="bus_bank_country" id="bus_bank_country">
                      <option value=""></option>
                     <?php foreach ($countries as $country) { ?>
                         <option value="<?php echo $country->country_id; ?>"><?php echo $country->country_name; ?></option>
                      <?php } ?>
                   </select>
               </div>
            </div>
            <div class="col l6 s12 m6 fieldset">
               <div class="input-field">
                 <label class="full-bg-label" for="ifsc_code">Enter IFSC Code<span class="required_field">*</span></label>
                 <input id="ifsc_code" name="ifsc_code" class="full-bg adjust-width" type="text">
               </div>
            </div>
         </div>
         <div class="row gstrow">
            <div class="col l12 s12 m12 fieldset">
               <div class="col l6 s12 m6 input-set border-bottom">

                  <div class="input-field" >
                      <label for="bank_country" class="full-bg-label select-label">Currency<span class="required_field">*</span></label>
                      <select class="country-dropdown add-branch-curr check-label full-bg-label" name="bank_cur_check" id="bank_cur_check_edit">
                         <option value=""></option>
                      </select>
                  </div>

               </div>
               <div class="col l6 s12 m6 fieldset">
                  <div class="input-field">
                    <label class="full-bg-label" for="swift_code">Enter Swift Code</label> <input id="swift_code" name="swift_code" class="full-bg adjust-width" type="text" style="height:49px !important;">
                  </div>
               </div>
            </div>
         </div>

		 <?php if ($this->router->fetch_method() == "edit_company_profile" || $this->router->fetch_method() == "add_company_profile") { ?>
		 <div class="row gstrow">
			<div class="col s12 m12 l12 fieldset">
                <div class="col l6 s12 m6 fieldset">
                  <div class="input-field" style="border-right:1px solid #eef2fe;">
                     <label for="opening_bank_balance" class="full-bg-label">Opening Bank Balance<span class="required_field">*</span></label>
                     <input id="opening_bank_balance" name="opening_bank_balance" class="full-bg adjust-width border-top-none" type="text" autocomplete="off">
                  </div>
				</div>
				<div class="col l6 s12 m6 fieldset border-bottom border-right-none">
					<div class="input-field">
						<label for="opening_balance_date" class="full-bg-label">Opening Bank Balance Date <span class="required_field">*</span></label>
						<input id="opening_balance_date" name="opening_balance_date" class="full-bg adjust-width border-top-none bdatepicker icon-calendar-gray" type="text" autocomplete="off">
					</div>
				</div>
            </div>
		 </div>
		 <!--div class="row gstrow">
            <div class="col l12 s12 m12 fieldset">
				<div class="col s12 m12 l6 input-set">
                    <div class="row">
                    <div class="input-field">
                    <label style="padding-left:8px !important;" for="opening_cash_balance" class="full-bg-label">Opening Cash Balance</label>
                    <input id="opening_cash_balance" name="opening_cash_balance" class="full-bg adjust-width border-top-none" type="text" autocomplete="off" readonly>
                    </div>
                    </div>
                    </div>
				<div class="col l6 s12 m6 fieldset">
					<div class="input-field">
					<label style="padding-left:8px !important;" for="cash_balance_date" class="full-bg-label">Date of Cash Balance</label>
                    <input id="cash_balance_date" name="cash_balance_date" class="full-bg adjust-width border-top-none bdatepicker icon-calendar-gray border-right" type="text" autocomplete="off">
                   </div>
				</div>
			</div>
		 </div-->
		 <?php } ?>

         <div class="row">
            <div class="col l6 m6 s12 fieldset">
            </div>
            <div class="col l6 m6 s12 fieldset buttonset">
               <div class="right">
                  <button type="submit" class="btn-flat theme-primary-btn theme-btn theme-btn-large right">Save</button>
                  <button type="button" class="modal-close btn-flat theme-flat-btn theme-btn theme-btn-large">CANCEL</button>
               </div>
            </div>
         </div>
      </form>
   </div>
</div>

<div id="edit_bank_branch" class="modal modal-md" style="margin-top:-3%;">
	<style>
   .branch-add .dropdown-content{
		height:40vh !important;
		overflow-y: scroll !important;
	}

	.branch-status .dropdown-content{
		height:13vh !important;
		overflow-y: scroll !important;
	}

	.add-branch-curr .dropdown-content{
		height:30vh !important;
		overflow-y: scroll !important;
	}

	.ed-bank label.full-bg-label:not(.label-icon).active {
		margin:2px 0 !important;
	}

	#edit_bank_branch .modalheader{
		padding: 15px 30px !important;
	}

	#edit_bank_branch .select-wrapper {
		height:60px !important;
	}
	</style>
   <div class="modalheader">
      <h4>Edit Bank Details</h4>
      <a class="modal-close close-pop"><img src="<?php echo base_url();?>asset/images/popupdelete1.png" alt="delete"></a>
   </div>
   <div class="modalbody">
      <form class="addgstin" id="edit_bank_details" name="edit_bank_details" method="post">
		<?php $csrf = array(
		'name' => $this->security->get_csrf_token_name(),
		'hash' => $this->security->get_csrf_hash()
	);
	?>
	<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
         <div class="row gstrow">
            <div class="col l12 s12 m12 fieldset">
               <div class="col l6 s12 m6 fieldset">
                  <div class="input-field">
                     <input name="edit_cmpid" id="edit_cmpid" type="hidden">
                     <input name="edit_cbank_id" id="edit_cbank_id" type="hidden">
                     <label for="ac_number" class="full-bg-label">Enter Bank Account Number <span class="required_field">*</span></label>
                     <input name="edit_ac_number" id="edit_ac_number" class="full-bg adjust-width gstin numeric_number" type="text">
                  </div>
               </div>
               <div class="col l6 s12 m6 fieldset">
                  <div class="input-field">
                     <label class="full-bg-label active" for="bank_name">Enter Bank Name <span class="required_field">*</span></label>
                     <input id="edit_bank_name" name="edit_bank_name" class="full-bg adjust-width" type="text">
                  </div>
               </div>
            </div>
         </div>
         <div class="row gstrow">
            <div class="col l12 s12 m12 fieldset border-bottom">
               <div class="col l6 s12 m6 input-set">
                  <!-- <div class="row"> -->
                  <div class="input-field border-bottom">
                     <label for="edit_acc_type" class="full-bg-label select-label" style="margin: 0 !important;">Select Account Type <span class="required_field">*</span></label>
                     <select id="edit_acc_type" class="country-dropdown check-label" name="edit_acc_type">
                        <option value=""></option>
                        <option value="Current">Current</option>
                        <option value="Savings">Savings</option>
                        <option value="Loan">Loan</option>
                        <option value="Cash Credit">Cash Credit</option>
                     </select>
                     <!--   </div> -->
                  </div>
               </div>
               <div class="col l6 s12 m6 fieldset">
                  <div class="input-field">
                     <label for="bank_branch_name active" class="full-bg-label">Enter Branch Name</label>
                     <input id="edit_bank_branch_name" name="edit_bank_branch_name" class="full-bg adjust-width border-top-none " type="text" style="height:46px !important;">
                  </div>
               </div>
            </div>
         </div>
         <div class="row gstrow">
            <div class="col l12 s12 m12 fieldset">
             <div class="col l6 s12 m6 input-set border-bottom" >
                <div class="input-field ed-bank" style="height:68px !important;">
                    <label for="bank_country" class="full-bg-label select-label">COUNTRY <span class="required_field">*</span></label>
                    <select class="country-dropdown branch-add check-label" name="bus_bank_country" id="bus_bank_country_change">
                       <option value="">COUNTRY</option>
                      <?php foreach ($countries as $country) { ?>
                          <option value="<?php echo $country->country_id; ?>"><?php echo $country->country_name; ?></option>
                       <?php } ?>
                    </select>
                </div>
             </div>
             <div class="col l6 s12 m6 fieldset">
                <div class="input-field">
                    <label class="full-bg-label active" for="ifsc_code">Enter IFSC Code <span class="required_field">*</span></label>
                    <input id="ifsc_code" name="ifsc_code" class="full-bg adjust-width" type="text">
				</div>
            </div>
           </div>
         </div>
         <div class="row gstrow">
            <div class="col l12 s12 m12 fieldset border-bottom">
             <div class="col l6 s12 m6 input-set fieldset border-bottom">
                <div class="input-field ed-bank label-active" style="height:74px !important; width:236px; padding-left:10px !important;">
                    <label for="bank_country" class="full-bg-label select-label">Currency <span class="required_field">*</span></label>
                    <select class="country-dropdown add-branch-curr check-label" name="bank_cur_check" id="bank_cur_check_change">
                     <option value="">CURRENCY</option>
                     <!-- <?php //foreach ($currency as $curr) { ?>
                          <option value="<?php //echo $curr->currency_id; ?>"><?php //echo $curr->currency;?> (<?php //echo $curr->currencycode; ?>)</option>
                       <?php //} ?> -->
                    </select>
                </div>
             </div>
             <div class="col l6 s12 m6 fieldset">
                  <div class="input-field">
                    <label class="full-bg-label" for="swift_code">Enter Swift Code</label> <input id="swift_code" name="swift_code" class="full-bg adjust-width" type="text" style="height:46px !important; border-bottom:none !important;">
                  </div>
               </div>
            </div>
         </div>

		 <?php if ($this->router->fetch_method() == "edit_company_profile" || $this->router->fetch_method() == "add_company_profile") { ?>
		 <div class="row gstrow">
			<div class="col s12 m12 l12 fieldset">
                <div class="col l6 s12 m6 fieldset">
                  <div class="input-field" style="border-right:1px solid #eef2fe;">
                     <label for="opening_bank_balance" class="full-bg-label">Opening Bank Balance<span class="required_field">*</span></label>
                     <input id="opening_bank_balance" name="opening_bank_balance" class="full-bg adjust-width border-top-none" type="text" autocomplete="off">
                  </div>
				</div>
				<div class="col l6 s12 m6 fieldset border-right-none">
					<div class="input-field">
						<label for="opening_balance_date" class="full-bg-label">Opening Bank Balance Date <span class="required_field">*</span></label>
						<input id="opening_balance_date" name="opening_balance_date" class="full-bg adjust-width border-top-none bdatepicker icon-calendar-gray" type="text" autocomplete="off">
					</div>
				</div>
            </div>

            <div class="col s12 m12 l12 fieldset">
                <div class="col l6 s12 m6 input-set border-bottom" >
                <div class="input-field ed-bank" style="height:68px !important;">
                    <label for="status" class="full-bg-label select-label">Status</label>
                    <select class="country-dropdown branch-status check-label" name="status" id="status">
						<option value="">Status</option>
						<option value="Active">Active</option>
						<option value="Inactive">Inactive</option>
                    </select>
                </div>
				</div>
				<div class="col l6 s12 m6 fieldset border-bottom border-right-none">
				<div class="input-field">

				</div>
				</div>
            </div>
		 </div>
		 <!--div class="row gstrow">
            <div class="col l12 s12 m12 fieldset">
				<div class="col s12 m12 l6 input-set">
                    <div class="row">
                    <div class="input-field">
                    <label style="padding-left:8px !important;" for="opening_cash_balance" class="full-bg-label">Opening Cash Balance</label>
                    <input id="opening_cash_balance" name="opening_cash_balance" class="full-bg adjust-width border-top-none" type="text" autocomplete="off" readonly>
                    </div>
                    </div>
                    </div>
				<div class="col l6 s12 m6 fieldset">
					<div class="input-field">
					<label style="padding-left:8px !important;" for="cash_balance_date" class="full-bg-label">Date of Cash Balance</label>
                    <input id="cash_balance_date" name="cash_balance_date" class="full-bg adjust-width border-top-none bdatepicker icon-calendar-gray border-right" type="text" autocomplete="off">
                   </div>
				</div>
			</div>
		 </div-->
		 <?php } ?>
         <div class="row">
            <div class="col l6 m6 s12 fieldset">
            </div>
            <div class="col l6 m6 s12 fieldset buttonset">
               <div class="right">
                  <button type="submit" class="btn-flat theme-primary-btn theme-btn theme-btn-large right">Save</button>
                  <button type="button" class="modal-close btn-flat theme-flat-btn theme-btn theme-btn-large">CANCEL</button>
               </div>
            </div>
         </div>
      </form>
   </div>
</div>

<!-- ----- Start Deactivate Account Section----------- -->
<div id="deactive_myaccount_modal" class="modal modal-set">
   <img class="geen" src="<?php echo base_url(); ?>asset/images/green.png" alt="delete">
   <div class="modal-content">
      <div class="modal-header">
         <h4>Deactivate Account</h4>
         <a class="modal-close close-pop"><img src="<?php echo base_url(); ?>asset/images/popupdelete.png" alt="delete"></a>
      </div>
   </div>
   <div class="modal-body confirma">
      <p>Are you sure you want to deactivate your account?</p>
   </div>
   <div class="modal-content">
      <div class="modal-footer">
         <div class="row">
            <div class="col l12 s12 m12 cancel-deactiv">
               <a class=" modal-close btn-flat theme-flat-btn theme-btn theme-btn-large model-cancel " type="button">CANCEL</a>
               <a class="btn-flat theme-primary-btn theme-btn theme-btn-large  modal-trigger dea modal-close deactive_my_account">DEACTIVATE</a>
            </div>
         </div>
      </div>
   </div>
</div>
<div id="deactive_acc_otp_modal" class="modal modal-width">
   <img class="geen" src="<?php echo base_url(); ?>asset/images/green.png" alt="green">
   <div class="modal-content">
      <div class="modal-header">
         <h4>Deactivate Account</h4>
         <a class="modal-close close-pop"><img src="<?php echo base_url(); ?>asset/images/popupdelete.png" alt="delete"></a>
      </div>
   </div>
   <!--  <form id="send_otp_mail" name="send_otp_mail" novalidate="novalidate"> -->
   <div class="modal-body deactive-acc">
      <div class="input-field mb-20">
         <p class="ml-20">Send OTP Via</p>
         <!-- <label for="company_name" class="full-bg-label">Company Name</label>
            <input id="company_name" name="bus_company_name" class="full-bg adjust-width" type="text" value=""> -->
      </div>
      <div class="row border-field account-row">
         <div class="col s12 m12 l12">
            <div class="col s12 m12 l6 account-checkbox">
               <div class="row chkbox">
                  <input type="checkbox" id="mobile_no_acc" name="acc_mobile_no" value="Yes" class="sent-otp filled-in not_taxable  full-bg adjust-width gstin">
                  <label for="mobile_no_acc"  class="account-lable" style="padding-left: 30px;">Registered Mobile Number</label>
               </div>
            </div>
            <div class="col s12 m12 l6 account-checkbox-right">
               <div class="row chkbox">
                  <input type="checkbox" id="email_address_acc" name="acc_email_address" value="Yes" class="sent-otp filled-in not_taxable ">
                  <label for="email_address_acc" class="account-lable" style="padding-left: 30px;">Registered Email Address</label>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="modal-content">
      <div class="modal-footer">
         <div class="row">
            <div class="col l4 s12 m12"></div>
            <div class="col l12 s12 m12 cancel-deactiv text-right">
               <a class=" modal-close btn-flat theme-flat-btn theme-btn theme-btn-large model-cancel " type="button">CANCEL</a>
               <a class="btn-flat theme-primary-btn theme-btn theme-btn-large modal-trigger dea sent_account_otp">GET OTP</a>
            </div>
         </div>
      </div>
   </div>
   <!-- </form> -->
</div>
<div id="deactivate_enter_otp_modal" class="modal modal-width">
   <img class="geen" src="<?php echo base_url(); ?>asset/images/green.png" alt="green">
   <div class="modal-content">
      <div class="modal-header">
         <h4>Deactivate Account</h4>
         <a class="modal-close close-pop"><img src="<?php echo base_url(); ?>asset/images/popupdelete.png" alt="delete"></a>
      </div>
   </div>
   <form id="enter_otp_mail" name="enter_otp_mail" novalidate="novalidate">
	<?php $csrf = array(
		'name' => $this->security->get_csrf_token_name(),
		'hash' => $this->security->get_csrf_hash()
	);
	?>
	<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
      <div class="modal-body deactive-acc">
         <div class="input-field mb-20">
            <p class="par-otp ml-20" id="otp_sentense"></p>
         </div>
         <!-- <div class="row border-field account-row">
            <label class="full-bg-label active">Enter 6 DIGIT OTP</label>
            <input id="acc_otp" name="acc_otp" class="full-bg adjust-width" type="text" pl>
            </div> -->
         <div class="col l6 s12 m12 fieldset">
            <div class="input-field">
               <label class="full-bg-label active">Enter 6 DIGIT OTP</label>
               <input name="acc_otp" id="acc_otp" class="full-bg adjust-width" type="text">
            </div>
         </div>
      </div>
      <div class="modal-content">
         <div class="modal-footer">
            <div class="row">
               <div class="col l4 s12 m12"></div>
               <div class="col l12 s12 m12 cancel-deactiv text-right">
                  <a class=" modal-close btn-flat theme-flat-btn theme-btn theme-btn-large model-cancel " type="button">CANCEL</a>
                  <input type="submit" name="submit" value="CONFIRM" class="btn-flat theme-primary-btn theme-btn theme-btn-large modal-trigger dea enter_otp_submition">
               </div>
            </div>
         </div>
      </div>
   </form>
</div>
<div id="deactivate-notification" class="modal success">
   <img class="geen" src="<?php echo base_url(); ?>asset/images/green.png" alt="green">
   <div class="modal-content success-msg">
      <p>Account Deactivated Successfully</p>
      <a class="modal-close close-pop pass-suc"><img src="<?php echo base_url(); ?>asset/images/popupdelete.png" alt="delete"></a>
   </div>
</div>
<!-- ----- End Deactivate Account Section----------- -->

<!-- ----- Start Logout----------- -->
<div id="logout_myacc_modal" class="modal modal-set">
   <img class="geen" src="<?php echo base_url(); ?>asset/images/green.png" alt="green">
   <div class="modal-content">
      <div class="modal-header">
         <h4>Sign Out</h4>
         <a class="modal-close close-pop"><img src="<?php echo base_url(); ?>asset/images/popupdelete.png" alt="delete"></a>
      </div>
   </div>
   <div class="modal-body confirma">
      <p style="font-size:17px !important; text-align: center;" class="ml-20">Are you sure you want to sign out?</p>
   </div>
   <div class="modal-content">
      <div class="modal-footer">
         <div class="row">
            <!--   <div class="col l4 s12 m12"></div> text-right-->
            <div class="col l12 s12 m12 cancel-deactiv ">
               <a class=" modal-close btn-flat theme-flat-btn theme-btn theme-btn-large model-cancel " type="button">CANCEL</a>
               <a class="btn-flat theme-primary-btn theme-btn theme-btn-large  modal-trigger dea modal-close logout_my_account centertext">CONFIRM</a>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- ----- End Logout----------- -->
<div id="view_add_modal-popup" class="modal modal-set">
   <img class="geen" src="<?php echo base_url(); ?>asset/images/green.png" alt="green">
   <div class="modal-content">
      <div class="modal-header">
         <h4>Confirmation</h4>
         <a class="modal-close close-pop"><img src="<?php echo base_url(); ?>asset/images/popupdelete.png" alt="delete" class="no_acc_change"></a>
      </div>
   </div>
   <div class="modal-body confirma">
      <!-- <p>Link has been sent to your primary contact person's<br> email id to access client portal</p> -->
      <p>Client Portal access link will be emailed to primary contact person</p>
   </div>
   <div class="modal-content">
      <div class="modal-footer">
         <div class="row">
            <div class="col l5 s12 m12"></div>
            <div class="col l12 s12 m12 cancel-deactiv">
               <a href="javascript:void(0);" class="modal-close btn-flat theme-flat-btn theme-btn theme-btn-large model-cancel portal_access_no" type="button">CANCEL</a>
               <a href="javascript:void(0);" class="btn-flat theme-primary-btn theme-btn theme-btn-large  modal-trigger dea modal-close portal_access_yes"  type="button">YES</a>
            </div>
         </div>
      </div>
   </div>
</div>
<div id="view_modal-popup" class="modal modal-set">
   <img class="geen" src="<?php echo base_url(); ?>asset/images/green.png" alt="green">
   <div class="modal-content">
      <div class="modal-header">
         <h4>Confirmation</h4>
         <a class="modal-close close-pop"><img src="<?php echo base_url(); ?>asset/images/popupdelete.png" alt="delete" class="no_acc_change"></a>
      </div>
   </div>
   <div class="modal-body confirma">
      <!-- <p>Are you sure you want to remove the Link of your primary contact person's email id to access client portal?</p> -->
      <p>Are you sure you want to disable portal access to primary contact person?</p>
   </div>
   <div class="modal-content">
      <div class="modal-footer">
         <div class="row">
            <div class="col l5 s12 m12"></div>
            <div class="col l12 s12 m12 cancel-deactiv">
               <a href="javascript:void(0);" class=" modal-close btn-flat theme-flat-btn theme-btn theme-btn-large model-cancel portal_access_no" type="button">CANCEL</a>
               <a href="javascript:void(0);" class="btn-flat theme-primary-btn theme-btn theme-btn-large  modal-trigger dea modal-close portal_access_yes"  type="button">YES</a>
            </div>
         </div>
      </div>
   </div>
</div>

<div id="view_add_modal_emp-popup" class="modal modal-set">
   <img class="geen" src="<?php echo base_url(); ?>asset/images/green.png" alt="green">
   <div class="modal-content">
      <div class="modal-header">
         <h4>Confirmation</h4>
         <a class="modal-close close-pop emp-close"><img src="<?php echo base_url(); ?>asset/images/popupdelete.png" alt="delete" class="no_acc_change"></a>
      </div>
   </div>
   <div class="modal-body confirma">
      <!-- <p>Link has been sent to your primary contact person's<br> email id to access client portal</p> -->
      <p>Employee Portal access link will be emailed to Employee</p>
   </div>
   <div class="modal-content">
      <div class="modal-footer">
         <div class="row">
            <div class="col l5 s12 m12"></div>
            <div class="col l12 s12 m12 cancel-deactiv">
               <a href="javascript:void(0);" class=" modal-close btn-flat theme-flat-btn theme-btn theme-btn-large model-cancel portal_access_no" type="button">CANCEL</a>
               <a href="javascript:void(0);" class="btn-flat theme-primary-btn theme-btn theme-btn-large  modal-trigger dea modal-close portal_access_yes"  type="button">YES</a>
            </div>
         </div>
      </div>
   </div>
</div>
<div id="view_modal_emp-popup" class="modal modal-set">
   <img class="geen" src="<?php echo base_url(); ?>asset/images/green.png" alt="green">
   <div class="modal-content">
      <div class="modal-header">
         <h4>Confirmation</h4>
         <a class="modal-close close-pop"><img src="<?php echo base_url(); ?>asset/images/popupdelete.png" alt="delete" class="no_acc_change"></a>
      </div>
   </div>
   <div class="modal-body confirma">
      <!-- <p>Are you sure you want to remove the Link of your primary contact person's email id to access client portal?</p> -->
      <p>Are you sure you want to disable portal access to Employee?</p>
   </div>
   <div class="modal-content">
      <div class="modal-footer">
         <div class="row">
            <div class="col l5 s12 m12"></div>
            <div class="col l12 s12 m12 cancel-deactiv">
               <a href="javascript:void(0);" class=" modal-close btn-flat theme-flat-btn theme-btn theme-btn-large model-cancel portal_access_no" type="button">CANCEL</a>
               <a href="javascript:void(0);" class="btn-flat theme-primary-btn theme-btn theme-btn-large  modal-trigger dea modal-close portal_access_yes"  type="button">YES</a>
            </div>
         </div>
      </div>
   </div>
</div>

<div id="add_customer_modal" class="modal modal-md">
   <div class="modalheader">
      <h4>Add New Customer</h4>
      <a class="modal-close close-pop"><img src="<?php echo base_url();?>asset/images/popupdelete1.png" alt="delete"></a>
   </div>
   <div class="modalbody">
      <form class="addgstin" id="add_new_customer_form" name="add_new_customer_form" method="post">
		<?php $csrf = array(
		'name' => $this->security->get_csrf_token_name(),
		'hash' => $this->security->get_csrf_hash()
	);
	?>
	<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
         <div class="row gstrow">
            <div class="col l12 s12 m12 fieldset">
               <div class="input-field">
                  <label for="new_cust_company_name" class="full-bg-label">Company Name</label>
                  <input id="new_cust_company_name" name="new_cust_company_name" class="full-bg adjust-width border-top-none border-bottom-none" type="text">
               </div>
            </div>
         </div>
         <div class="row gstrow">
            <div class="col l12 s12 m12 fieldset">
               <div class="input-field">
                  <label for="new_cust_billing_address" class="full-bg-label">Billing Address</label>
                  <input id="new_cust_billing_address" name="new_cust_billing_address" class="full-bg adjust-width border-top-none border-bottom-none" type="text">
               </div>
            </div>
         </div>
         <div class="row gstrow">
            <div class="col l6 s12 m6 input-set">
               <div class="row">
                  <div class="input-field ">
                     <label for="new_cust_country" class="full-bg-label select-label">Country</label>
                     <select id="new_cust_country" class="country-dropdown check-label" name="new_cust_country">
                        <option value=""></option>
                     </select>
                  </div>
               </div>
            </div>
            <div class="col l6 s12 m6 input-set">
               <div class="row">
                  <div class="input-field ">
                     <label for="new_cust_state" class="full-bg-label select-label">State</label>
                     <select id="new_cust_state" class="country-dropdown check-label" name="new_cust_state">
                        <option value=""></option>
                     </select>
                  </div>
               </div>
            </div>
         </div>
         <div class="row gstrow">
            <div class="col l6 s12 m6 input-set">
               <div class="row">
                  <div class="input-field ">
                     <label for="new_cust_city" class="full-bg-label select-label">City</label>
                     <select id="new_cust_city" class="country-dropdown check-label" name="new_cust_city">
                        <option value=""></option>
                     </select>
                  </div>
               </div>
            </div>
            <div class="col l6 s12 m6 fieldset">
               <div class="input-field">
                  <label class="full-bg-label" for="ifsc_code">Pincode</label>
                  <input id="new_cust_pincode" name="new_cust_pincode" class="full-bg adjust-width" type="text">
               </div>
            </div>
         </div>
         <div class="row gstrow">
            <div class="col l12 s12 m12 fieldset">
               <div class="col l6 s12 m6 fieldset">
                  <div class="input-field">
                     <label for="ac_number" class="full-bg-label">Shipping Address</label>
                     <input name="new_cust_ship_address" id="new_cust_ship_address" class="full-bg adjust-width gstin" type="text">
                  </div>
               </div>
               <div class="col l6 s12 m6 fieldset">
                  <div class="col s12 chkbox2">
                     <input type="checkbox" name="same-bill-customer" id="same-bill-popup">
                     <label class="checkboxx2 greentheme-checkbox" for="same-bill-popup"><span class="check-boxs">Same as billing address</span></label>
                  </div>
               </div>
            </div>
         </div>
         <div class="row gstrow">
            <div class="col l6 s12 m6 input-set">
               <div class="row">
                  <div class="input-field shipping_values">
                     <label for="new_cust_ship_country" class="full-bg-label select-label">Shipping Country</label>
                     <select id="new_cust_ship_country" class="country-dropdown check-label" name="new_cust_ship_country">
                        <option value=""></option>
                     </select>
                  </div>
               </div>
            </div>
            <div class="col l6 s12 m6 input-set">
               <div class="row">
                  <div class="input-field shipping_values">
                     <label for="new_cust_ship_state" class="full-bg-label select-label">Shipping State</label>
                     <select id="new_cust_ship_state" class="country-dropdown check-label" name="new_cust_ship_state">
                        <option value=""></option>
                     </select>
                  </div>
               </div>
            </div>
         </div>
         <div class="row gstrow">
            <div class="col l6 s12 m6 input-set">
               <div class="row">
                  <div class="input-field shipping_values">
                     <label for="new_cust_ship_city" class="full-bg-label select-label">City</label>
                     <select id="new_cust_ship_city" class="country-dropdown check-label" name="new_cust_ship_city">
                        <option value=""></option>
                     </select>
                  </div>
               </div>
            </div>
            <div class="col l6 s12 m6 fieldset">
               <div class="input-field">
                  <label class="full-bg-label" for="ifsc_code">Shipping Pincode</label>
                  <input id="new_cust_ship_pincode" name="new_cust_ship_pincode" class="full-bg adjust-width" type="text">
               </div>
            </div>
         </div>
         <div class="row gstrow">
            <div class="col l12 s12 m12 fieldset">
               <div class="input-field">
                  <label for="bank_branch_name" class="full-bg-label">PAN NUMBER</label>
                  <input id="new_cust_pan_no" name="new_cust_pan_no" class="full-bg adjust-width border-top-none border-bottom-none" type="text">
               </div>
            </div>
         </div>
         <div class="row">
            <div class="col l6 m6 s12 fieldset">
            </div>
            <div class="col l6 m6 s12 fieldset buttonset">
               <div class="right">
                  <button type="submit" class="btn-flat theme-primary-btn theme-btn theme-btn-large right">Save</button>
                  <button type="button" class="modal-close btn-flat theme-flat-btn theme-btn theme-btn-large">CANCEL</button>
               </div>
            </div>
         </div>
      </form>
   </div>
</div>
<!--  ...Customer Profile Access portal...  -->
<div id="edit_modal-popup" class="modal modal-set">
   <img class="geen" src="<?php echo base_url(); ?>asset/images/green.png" class="edit_no_acc_change" alt="green">
   <div class="modal-content">
      <div class="modal-header">
         <h4>Confirmation</h4>
         <a class="modal-close close-pop"><img src="<?php echo base_url(); ?>asset/images/popupdelete.png" class="edit_no_acc_change" alt="delete"></a>
      </div>
   </div>
   <div class="modal-body confirma">
      <p>Are you sure you want to disable portal access to primary contact person?</p>
   </div>
   <div class="modal-content">
      <div class="modal-footer">
         <div class="row">
            <div class="col l5 s12 m12"></div>
            <div class="col l12 s12 m12 cancel-deactiv">
               <a class=" modal-close btn-flat theme-flat-btn theme-btn theme-btn-large model-cancel edit_no_acc_change" type="button">CANCEL</a>
               <a href="javascript:void(0);" class="btn-flat theme-primary-btn theme-btn theme-btn-large  modal-trigger dea modal-close"  type="button">YES</a>
            </div>
         </div>
      </div>
   </div>
</div>
<div id="edit_add_modal-popup" class="modal modal-set">
   <img class="geen" src="<?php echo base_url(); ?>asset/images/green.png" alt="green">
   <div class="modal-content">
      <div class="modal-header">
         <h4>Confirmation</h4>
         <a class="modal-close close-pop"><img src="<?php echo base_url(); ?>asset/images/popupdelete.png" class="edit_no_acc_change" alt="delete"></a>
      </div>
   </div>
   <div class="modal-body confirma">
      <p>Client Portal access link will be emailed to primary contact person</p>
   </div>
   <div class="modal-content">
      <div class="modal-footer">
         <div class="row">
            <div class="col l5 s12 m12"></div>
            <div class="col l12 s12 m12 cancel-deactiv">
               <a class=" modal-close btn-flat theme-flat-btn theme-btn theme-btn-large model-cancel edit_no_acc_change" type="button">CANCEL</a>
               <a href="javascript:void(0);" class="btn-flat theme-primary-btn theme-btn theme-btn-large  modal-trigger dea modal-close"  type="button">YES</a>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- Start View Expense Voucher - Sales Invoice -->
<div id="view-expense-voucher" class="modal modal-lg modal-display">
   <div class="modalheader">
      <h4>View Expense Vouchers</h4>
      <a class="modal-close close-pop "><img src="<?php echo base_url();?>asset/images/popupdelete1.png" alt="delete"></a>
   </div>
   <div class="modalbody">
      <form class="addgstin" id="add_expense_voucher_frm" name="add_expense_voucher_frm" method="post">
		<?php $csrf = array(
		'name' => $this->security->get_csrf_token_name(),
		'hash' => $this->security->get_csrf_hash()
	);
	?>
	<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
         <div class="row gstrow">
            <div class="col l12 s12 m12 fieldset">
               <table id="myTable">
                  <thead>
                     <tr class="header">
                        <th style="width:20%;">Company Name</th>
                        <th style="width:15%;">Date of Expense</th>
                        <th style="width:20%;">Made By</th>
                        <th style="width:15%;">Expense Type</th>
                        <th style="width:20%;text-align: right;">Expense Amount</th>
                        <th style="width:10%;"></th>
                     </tr>
                  </thead>
                  <tbody id="myTablebody">
                  </tbody>
               </table>
            </div>
         </div>
         <div class="row">
            <div class="col l6 m6 s12  border-field fieldset">
            </div>
            <div class="col l6 m6 s12 fieldset buttonset">
               <div class="right">
                  <!-- <button type="submit" class="btn-flat theme-primary-btn theme-btn theme-btn-large right">Save</button>
                     --> <button type="button" class="modal-close btn-flat theme-flat-btn theme-btn theme-btn-large">CANCEL</button>
               </div>
            </div>
         </div>
      </form>
   </div>
</div>

<div id="equiModal" class="modal ps-active-y" style="width:35%;">
        <a style="margin-right:5px; margin-top:27px;" id="equi_modal_close" class="modal-close close-pop" style=""><img width="18" height="18" src="<?php echo base_url(); ?>asset/images/popupdelete1.png" alt="delete"></a>
          <h4 class="modal-title" style='text-align:left;'>Add Equalisation levy amount</h4>

         <div class="modalbody">
            <div class="equ_levy_details" style="margin-top:15px;">
            <form class="" id="equia_levy" name="equia_levy" method="post">
				<?php $csrf = array(
		'name' => $this->security->get_csrf_token_name(),
		'hash' => $this->security->get_csrf_hash()
	);
	?>
	<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
               <div class="row">
                  <div class="col l12 s10 m10 fieldset">
                    <div class="row">
                     <div class="col l6 s12 m6 fieldset">
                       <div class="input-field">
                   <input type="hidden" name="eql_id" id="eql_id">
                        <label class="full-bg-label" style='margin:8px 0 !important;'>SAC NO.</label>
                        <input style='height: 2rem !important;' name="inv_equ_sac_no" id="equ_sac_no" value='' class="full-bg adjust-width border-right" type="text">
                       </div>
                     </div>
                     <div class="col l6 s12 m6 fieldset">
                       <div class="input-field">
                             <label class="full-bg-label" style='margin:8px 0 !important;'> GST % </label>
                             <input style='height: 2rem !important;' name="inv_equ_gst_tax" id="equ_gst_tax" value='' class="full-bg adjust-width border-right" type="text">
                           </div>
                     </div>
                    </div>
                    <div class="row">
                     <div class="col l6 s12 m6 fieldset">
                       <div class="input-field">
                             <label class="full-bg-label" style='margin:8px 0 !important;'>CESS %</label>
                             <input style='height: 2rem !important;' name="inv_equ_cess_tax" id="equ_cess_tax" value='' class="full-bg adjust-width border-right" type="text">
                           </div>
                     </div>
                     <div class="col l6 s12 m6 fieldset">
                       <div class="input-field">
                             <label class="full-bg-label" style='margin:8px 0 !important;'>OTHER TAX %</label>
                             <input style='height: 2rem !important;' name="inv_equ_other_tax" id="equ_other_tax" value='' class="full-bg adjust-width border-right" type="text">
                           </div>
                     </div>
                   </div>
                    <div class="row">
                     <div class="col l6 s12 m6 fieldset">
                       <div class="input-field">
                        <label class="full-bg-label" style='margin:4px 0 !important;'>Equalisation Levy %</label>
                        <input type="text" class="bill-box bill1 full-bg adjust-width border-right numeric_number" style="height:35px;" id="equ_levy_per" name="inv_equ_levy_per" value="" >
                       </div>
                     </div>
                     <div class="col l6 s12 m6 fieldset">
                       <div class="input-field">
                        <label class="full-bg-label" style='margin:4px 0 !important;'>Equalisation Levy Amount</label>
                        <input id="equ_levy_amount" name="inv_equ_levy_amt" type="text" style="height:35px;" class="bill-box bill1 full-bg adjust-width border-right numeric_number" value="">
                       </div>
                     </div>
                    </div>
                    <div class="row" style="margin-top:20px !important;">
                     <div class="col l4 s12 m6 fieldset" style='margin-left:21px; margin-top:6px;'>
                       <input type="checkbox" id="set_default" class="late_fee_chkbox"  name="set_default">
                       <label class="checkboxx2" for="set_default"><span class="check-boxs">Set as default</span></label>
                     </div>
                     <div class="col l8 s12 m6 fieldset" style='text-align:right; margin-left:-50px;'>
                       <button id="equi_close" type="button" class="btn btn-default modal-close" data-dismiss="modal" style="font-size:12px;">CANCEL</button>
                       <button id="equi_save" type="button" class="btn btn-default" data-dismiss="modal" style="font-size:12px; color:#fff;">SAVE</button>
                     </div>
                    </div>
               </div>
            </div>
         </form>
      </div>
   </div>
</div>

<div id="latefeeModal" class="modal ps-active-y" style="width:35%;">
        <a style="margin-right:5px; margin-top:27px;" id="equi_modal_close" class="modal-close close-pop" style=""><img width="18" height="18" src="<?php echo base_url(); ?>asset/images/popupdelete1.png" alt="delete"></a>
          <h4 class="modal-title" style='text-align:left;'>Late Fee</h4>

         <div class="modalbody">
            <div class="late_fee_details" style="margin-top:15px;">
            <form class="" id="late_fee" name="late_fee" method="post">
				<?php $csrf = array(
		'name' => $this->security->get_csrf_token_name(),
		'hash' => $this->security->get_csrf_hash()
	);
	?>
	<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
               <div class="row">
                  <div class="col l12 s10 m10 fieldset">
                    <div class="row">
                     <div class="col l6 s12 m6 fieldset">
                       <div class="input-field">
                   <input type="hidden" name="latfee_id" id="latfee_id">
                        <label class="full-bg-label late-ck" style='margin:8px 0 !important;'>SAC NO.</label>
                        <input style='height: 2rem !important;' name="inv_late_sac_no" id="late_sac_no" value='' class="full-bg adjust-width border-right" type="text">
                       </div>
                     </div>
                     <div class="col l6 s12 m6 fieldset">
                       <div class="input-field">
                             <label class="full-bg-label late-ck" style='margin:8px 0 !important;'> GST % </label>
                           <input style='height: 2rem !important;' name="inv_late_gst_tax" id="late_gst_tax" value='' class="full-bg adjust-width border-right" type="text">
                           </div>
                     </div>
                    </div>
                    <div class="row">
                     <div class="col l6 s12 m6 fieldset">
                       <div class="input-field">
                             <label class="full-bg-label late-ck" style='margin:8px 0 !important;'>CESS %</label>
                             <input style='height: 2rem !important;' name="inv_late_cess_tax" id="late_cess_tax" value='' class="full-bg adjust-width border-right" type="text">
                           </div>
                     </div>
                     <div class="col l6 s12 m6 fieldset">
                       <div class="input-field">
                             <label class="full-bg-label late-ck" style='margin:8px 0 !important;'>OTHER TAX %</label>
                             <input style='height: 2rem !important;' name="inv_late_other_tax" id="late_other_tax" value='' class="full-bg adjust-width border-right" type="text">
                           </div>
                     </div>
                   </div>
                    <div class="row">
                     <!--<div class="col l6 s12 m6 fieldset">
                       <div class="input-field">
                        <label class="full-bg-label" style='margin:4px 0 !important;'>Equalisation Levy %</label>
                        <input type="text" class="bill-box bill1 full-bg adjust-width border-right numeric_number" style="height:35px;" id="equ_levy_per" name="inv_equ_levy_per" value="" >
                       </div>
                     </div>-->
                     <div class="col l6 s12 m6 fieldset">
                       <div class="input-field">
                      <label class="full-bg-label" style='margin:4px 0 !important;'>Late Fee Amount</label>
                        <input id="late_fee_amt" name="inv_late_fee_amt" type="text" style="height:35px;" class="bill-box bill1 full-bg adjust-width border-right numeric_number" value="">
                       </div>
                     </div>
                    </div>
                    <div class="row" style="margin-top:20px !important;">
                     <div class="col l4 s12 m6 fieldset" style='margin-left:21px; margin-top:6px;'>
                       <input type="checkbox" id="set_default_late" class="late_fee_chkbox"  name="set_default">
                       <label class="checkboxx2" for="set_default_late"><span class="check-boxs">Set as default</span></label>
                     </div>
                     <div class="col l8 s12 m6 fieldset" style='text-align:right; margin-left:-50px;'>
                       <button id="late_close" type="button" class="btn btn-default modal-close" data-dismiss="modal" style="font-size:12px;">CANCEL</button>
                       <button id="late_save" type="button" class="btn btn-default" data-dismiss="modal" style="font-size:12px;">SAVE</button>
                     </div>
                    </div>
               </div>
            </div>
         </form>
      </div>
   </div>
</div>
<!-- End View Expense Voucher - Sales Invoice -->

<!-- Add New Vendor -->

<div id="view-vendor" class="modal modal-md modal-display" style="margin-top:-3% !important;">
   <style>
	#view-vendor.ps-container > .ps-scrollbar-x-rail > .ps-scrollbar-x {
		display: none !important;
	}

	#vendor_name:focus{
		border-left:3px solid #ff85a1 !important;
	}

	#pan_no:focus{
		border-left:3px solid #ff85a1 !important;
	}

	#gstin:focus{
		border-left:3px solid #ff85a1 !important;
	}

	#bill_add:focus{
		border-left:3px solid #ff85a1 !important;
	}

	#pin_code:focus{
		border-left:3px solid #ff85a1 !important;
	}

	#view-vendor #vendor_save.btn.btn-default {
		border: 1px solid #7864e9;
		text-transform: capitalize;
		color: #fff;
		box-shadow: none;
		position: relative;
		min-width: 100px;
		background: #7864e9;
	}
	#view-vendor .modalbody {
		overflow-x: hidden;
	}

	#vendor_details_form .error{
		margin-top:-25px !important;
	}

	#vendor_details_form .error.active{
		margin-top:0px !important;
	}
   </style>
   <div class="modalheader" style="padding-bottom:7px;">
      <h4>Add New Vendor</h4>
      <a class="modal-close close-pop vendeselect"><img src="<?php echo base_url();?>asset/images/popupdelete1.png" alt="delete"></a>
   </div>
   <div class="modalbody">
				<div class="client_details" style="margin-top:15px;">
				<form class="" id="vendor_details_form" name="client_details_form" method="post">
					<?php $csrf = array(
		'name' => $this->security->get_csrf_token_name(),
		'hash' => $this->security->get_csrf_hash()
	);
	?>
	<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
					<div class="row">
						<div class="col l12 s10 m10 fieldset">
						  <div class="row">
							<div class="col l6 s12 m6 fieldset">
							  <div class="input-field">
								<label class="full-bg-label" style='margin:8px 0 !important;'>VENDOR NO.</label>
								<input style='height: 3rem !important; border-right:1px solid #C0C0C0 !important; padding-left:20px !important;' name="vendor_no" id="vendor_no" value='<?//=$vendorNo?>' class="readonly-bg-grey full-bg adjust-width" type="text" readonly="readonly">
							  </div>
							</div>
							<div class="col l6 s12 m6 fieldset" style="border-left:1px solid #ccc;">
								<div class="input-field">
									<label class="full-bg-label" style="margin: 7px 0 !important;">VENDOR DATE</label>
									<input type="text" id="vendor_date" name="vendor_date" style="height:3rem !important; padding-left:20px !important;" class="readonly-bg-grey full-bg adjust-width border-top-none" value="<?= date('d/m/Y'); ?>" readonly="readonly">
									<!--<input name="client_date" style="height:2rem !important;" id="client_date" class="full-bg adjust-width border-top-none bdatepicker icon-calendar-gray border-right" type="text" autocomplete="off">-->
								</div>
							</div>
						  </div>
						  <div class="row">
							<div class="col l12 s12 m12 fieldset">
								<div class="input-field input-set">
									<label class="full-bg-label" style='margin:8px 0 !important;'>VENDOR NAME<span class="required_field"> *</span></label>
									<input style='height: 3rem !important; padding-left:20px !important;' name="vendor_name" id="vendor_name" class="full-bg adjust-width" type="text">
								</div>
							</div>
						  </div>
						  <div class="row">
							<div class="col l6 s12 m6 fieldset">
							  <div class="input-field">
								<label class="full-bg-label" style='margin:8px 0 !important;'>PAN NUMBER</label>
								<input style='width:83% !important; height: 3rem !important; padding-left:20px !important;' name="pan_no" id="pan_no" value='' class="full-bg adjust-width border-right" type="text">
							  </div>
							</div>
							<div class="col l6 s12 m6 fieldset">
								<div class="input-field">
									<label class="full-bg-label" style='margin:8px 0 !important;'>GSTIN<span class="required_field"> *</span></label>
									<input style='height: 3rem !important; padding-left:20px !important;' name="gstin" id="gstin" value='' class="full-bg adjust-width border-right" type="text">
								</div>
							</div>
						 </div>
						 <div class="row">
							<div class="col l12 s12 m12 fieldset">
							  <div class="input-field">
									  <label class="full-bg-label" style='margin:8px 0 !important;'>BILLING ADDRESS</label>
									  <input style='height: 3rem !important; padding-left:20px !important;' name="bill_add" id="bill_add" value='' class="full-bg adjust-width border-right" type="text">
									</div>
							</div>
						  </div>
						  <div class="row">
							<div class="col l6 s12 m6 fieldset" style="height:75px; border:0.5px solid #e6f7ff !important;">
							   <div class="input-field input-set" style="padding-left:10px;">
                               <label class="full-bg-label select-label" for="country">COUNTRY<span class="required_field"> *</span></label>
                                <select id="country" name="country" class="country-dropdown check-label" >
                                         <option value=""></option>
                                             <?php if($countries != '') {
                                      foreach($countries as $country) { ?>
                                        <option class="icon-location" value="<?php echo $country->country_id; ?>"><?php echo strtoupper($country->country_name); ?></option>
                                          <?php }  } ?>
                                </select>
							  </div>
							</div>
							<div class="col l6 s12 m6 fieldset" style="height:75px;">
							  <div class="input-field input-set" style="padding-left:10px;">
                               <label class="full-bg-label select-label" for="state">STATE WITH STATE CODE<span class="required_field"> *</span></label>
                                <select id="state" name="state" class="country-dropdown check-label" >
                                          <option value=""></option>
                                </select>
							  </div>
							</div>
						  </div>
						  <div class="row">
							<div class="col l6 s12 m6 fieldset" style="height:75px; border:0.5px solid #e6f7ff !important;">
							  <div class="input-field input-set" style="padding-left:10px;">
                               <label class="full-bg-label select-label" for="city">CITY<span class="required_field"> *</span></label>
                                <select id="city" name="city" class="country-dropdown check-label" >
                                          <option value=""></option>
                                </select>
							  </div>
							</div>
							<div class="col l6 s12 m6 fieldset">
							  <div class="input-field">
								<label class="full-bg-label" style='margin:8px 0 !important;'>PIN CODE</label>
								<input style='height: 3rem !important;' name="pin_code" id="pin_code" value='' class="full-bg adjust-width border-right numeric_number" type="text">
							  </div>
							</div>
						  </div>
						  <div class="row" style="margin-top:20px !important;">
							<div class="col l12 s12 m6">
							</div>
							<div class="col l12 s12 m6 fieldset" style='text-align:right; margin-left:-20px;'>
							  <button id="client_close" type="button" class="btn btn-default modal-close vendeselect" data-dismiss="modal" style="font-size:12px;">CANCEL</button>
							  <button id="vendor_save" type="button" class="btn btn-default" data-dismiss="modal" style="font-size:12px;">SAVE</button>
							</div>
						  </div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

<!-- END -->
<!-- asset sale modal-->
<div id="asset-sale-add" class="modal modal-md modal-display">
   <style type="text/css">
      #asset-add .modalbody {
      overflow-y: hidden;
      overflow-x: hidden;
   }
   .modal input.select-dropdown {
      margin: 3px 0px 0px 12px !important;
   }
   </style>
   <div class="modalheader" style="padding-bottom:7px;">
      <h4>Add New Asset</h4>
      <a class="modal-close close-pop cldeselect"><img src="<?php echo base_url();?>asset/images/popupdelete1.png" alt="delete"></a>
   </div>
   <div class="modalbody">
            <div class="client_details" style="margin-top:15px;">
            <form class="" id="add_new_assets_modal" name="add_new_assets_modal" method="post">
				<?php $csrf = array(
		'name' => $this->security->get_csrf_token_name(),
		'hash' => $this->security->get_csrf_hash()
	);
	?>
	<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
               <div class="row">
                  <div class="col l12 s10 m10 fieldset">
                    <div class="row">
                     <div class="col l6 s12 m6 fieldset">
                       <div class="input-field">
                        <label for="new_asset_code" class="full-bg-label" style='margin:8px 0 !important;'>ASSET NO.<span class="required_field"> *</span></label>
                        <input style='height: 3rem !important; border-right:1px solid #C0C0C0 !important;' name="new_asset_code" id="new_asset_code" value="<?//= $assetNo; ?>" class="readonly-bg-grey full-bg adjust-width" type="text" readonly="readonly">
                       </div>
                     </div>
                     <div class="col l6 s12 m6 fieldset">
                        <div class="input-field">
                           <label for="new_asset_date" class="full-bg-label" style="margin: 7px 0 !important;">ASSET DATE<span class="required_field"> *</span></label>
                           <input type="text" id="new_asset_date" name="new_asset_date" style="height:3rem !important;" class="readonly-bg-grey full-bg  adjust-width border-top-none" value="<?= date('d/m/Y'); ?>" readonly="readonly">
                           <!--<input name="client_date" style="height:2rem !important;" id="client_date" class="full-bg adjust-width border-top-none bdatepicker icon-calendar-gray border-right" type="text" autocomplete="off">-->
                        </div>
                     </div>
                    </div>
                    <div class="row">
                     <div class="col l6 s12 m6 fieldset">
                       <div class="input-field">
                             <label for="new_asset_name" class="full-bg-label" style='margin:18px 0 !important;'>ASSET NAME<span class="required_field"> *</span></label>
                             <input style='height: 3rem !important;' name="new_asset_name" id="new_asset_name" maxlength="30" class="full-bg adjust-width border-right" type="text">
                        </div>
                     </div>

                     <div class="col l6 s12 m6 fieldset">
                       <div class="input-field">
                             <label for="new_asset_description" class="full-bg-label" style='margin:18px !important;'>ASSET DESCRIPTION</label>
                             <input style='height: 3rem !important;' name="new_asset_description" id="new_asset_description" maxlength="200" class="full-bg adjust-width border-right" type="text">
                        </div>
                     </div>
                    </div>

                    <div class="row">
                     <div class="col l6 s12 m6 fieldset">
                       <div class="input-field input-set ">
                           <label class="full-bg-label select-label" for="new_nature_asset">NATURE OF ASSET<span class="required_field"> *</span></label>
                           <select id="new_nature_asset" name="new_nature_asset" class="country-dropdown check-label">
                              <option value=""></option>
                              <option value="tangible">TANGIBLE</option>
                              <option value="intangible">INTANGIBLE</option>
                           </select>
                       </div>
                     </div>

                     <div class="col l6 s12 m6 fieldset">
                       <div class="input-field input-set ">
                           <label class="full-bg-label select-label" for="new_asset_type">ASSET TYPE<span class="required_field"> *</span></label>
                           <select id="new_asset_type" name="new_asset_type" class="country-dropdown check-label">
                              <option value=""></option>
                              <?php //foreach($asset_type as $key=>$value){ ?>
                                  <option value="<?//=$value->atl_id?>" ><?//= strtoupper($value->asset_type_name);?></option>
                              <?php //} ?>
                           </select>
                       </div>
                     </div>
                   </div>

                  <div class="row">
                     <div class="col l6 s12 m6 fieldset">
                       <div class="input-field">
                           <label for="new_sac_no" class="full-bg-label" style='margin:18px 0 !important;'>SAC NUMBER</label>
                           <input style='height: 3rem !important;' name="new_sac_no" id="new_sac_no" value='' class="full-bg numeric_number adjust-width border-right" type="text">
                        </div>
                     </div>
                     <div class="col l6 s12 m6 fieldset">
                       <div class="input-field">
                             <label for="new_asset_gst" class="full-bg-label" style='margin:18px 0 !important;'>GOODS & SERVICE TAX (%)</label>
                             <input style='height: 3rem !important;' name="new_asset_gst" id="new_asset_gst" value='' class="full-bg numeric_number adjust-width border-right" type="text">
                           </div>
                     </div>
                   </div>

                   <div class="row">
                     <div class="col l12 s12 m12 fieldset">
                       <div class="input-field">
                             <label for="asset_cess_tax" class="full-bg-label" style='margin:18px 0 !important;'>CESS TAX (%)</label>
                             <input style='height: 3rem !important;' name="new_asset_cess_tax" id="new_asset_cess_tax" value='' class="full-bg numeric_number adjust-width border-right" type="text">
                           </div>
                     </div>
                    </div>
                    <!--
                    <div class="row">
                     <div class="col l6 s12 m6 fieldset">
                       <div class="input-field">
                           <label for="vendor_name" class="full-bg-label" style='margin:8px 0 !important;'>VENDOR NAME<span class="required_field"> *</span></label>
                           <input style='height: 3rem !important;' name="new_vendor_name" id="new_vendor_name" value='' class="full-bg adjust-width border-right" type="text">
                        </div>
                     </div>
                     <div class="col l6 s12 m6 fieldset">
                       <div class="input-field">
                             <label for="gstin" class="full-bg-label" style='margin:8px 0 !important;'>GSTIN</label>
                             <input style='height: 3rem !important;' name="new_gstin" id="new_gstin" value='' class="full-bg adjust-width border-right" type="text">
                           </div>
                     </div>
                   </div>
                  -->

                  <!--
                  <div class="row">
                     <div class="col l12 s12 m12 fieldset">
                       <div class="input-field">
                             <label for="new_place_of_supply" class="full-bg-label" style='margin:8px 0 !important;'>PLACE OF SUPPLY</label>
                             <input style='height: 3rem !important;' name="new_place_of_supply" id="new_place_of_supply" value='' class="full-bg adjust-width border-right" type="text">
                           </div>
                     </div>
                  </div>
                  -->

                  <!--
                  <div class="row">
                     <div class="col l6 s12 m6 fieldset" style="height:75px; border:0.5px solid #e6f7ff !important;">
                        <div class="input-field input-set ">
                               <label class="full-bg-label select-label" for="country">COUNTRY<span class="required_field"> *</span></label>
                                <select id="country" name="country" class="country-dropdown check-label" >
                                         <option value=""></option>
                                             <?php //if($countries != '') {
                                      //foreach($countries as $country) { ?>
                                        <option class="icon-location" value="<?php //echo $country->country_id; ?>"><?php //echo $country->country_name; ?></option>
                                          <?php //}  } ?>
                                </select>
                       </div>
                     </div>
                     <div class="col l6 s12 m6 fieldset" style="height:75px;">
                       <div class="input-field input-set ">
                               <label class="full-bg-label select-label" for="state">STATE WITH STATE CODE<span class="required_field"> *</span></label>
                                <select id="state" name="state" class="country-dropdown check-label" >
                                          <option value=""></option>
                                </select>
                       </div>
                     </div>
                    </div>

                    <div class="row">
                     <div class="col l6 s12 m6 fieldset" style="height:75px; border:0.5px solid #e6f7ff !important;">
                       <div class="input-field input-set ">
                               <label class="full-bg-label select-label" for="city">CITY<span class="required_field"> *</span></label>
                                <select id="city" name="city" class="country-dropdown check-label" >
                                          <option value=""></option>
                                </select>
                       </div>
                     </div>
                     <div class="col l6 s12 m6 fieldset">
                       <div class="input-field">
                        <label class="full-bg-label" style='margin:8px 0 !important;'>PIN CODE</label>
                        <input style='height: 3rem !important;' name="pin_code" id="pin_code" value='' class="full-bg adjust-width border-right" type="text">
                       </div>
                     </div>
                    </div>
!-->

                    <div class="row" style="margin-top:20px !important;">
                     <div class="col l12 s12 m6">
                     </div>
                     <div class="col l12 s12 m6 fieldset" style='text-align:right; margin-left:-20px;'>
                       <button id="client_close" type="button" class="btn btn-default modal-close cldeselect" data-dismiss="modal" style="font-size:12px; color: black !important;">CANCEL</button>
                       <button id="sales_save" type="button" class="btn btn-default" data-dismiss="modal" style="font-size:12px; color: white !important; background: #7864E9 !important;">SAVE</button>
                     </div>
                    </div>
               </div>
            </div>
         </form>
      </div>
   </div>
</div>

<!-- create Asset purchase (create new asset) modal -->
<div id="asset-add" class="modal modal-md modal-display">
   <style type="text/css">
      #asset-add .modalbody {
      overflow-y: hidden;
      overflow-x: hidden;
   }
   .modal input.select-dropdown {
      margin: 3px 0px 0px 12px !important;
   }
   </style>
   <div class="modalheader" style="padding-bottom:7px;">
      <h4>Add New Asset</h4>
      <a class="modal-close close-pop cldeselect"><img src="<?php echo base_url();?>asset/images/popupdelete1.png" alt="delete"></a>
   </div>
   <div class="modalbody" style="margin: 0 0 0 10px !important;">
            <div class="client_details" style="margin-top:15px; width: 96% !important;">
            <form class="" id="add_new_assets_modal" name="add_new_assets_modal" method="post">
				<?php $csrf = array(
		'name' => $this->security->get_csrf_token_name(),
		'hash' => $this->security->get_csrf_hash()
	);
	?>
	<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
               <div class="row">
                  <div class="col l12 s10 m10 fieldset">

                    <div class="row">
                     <div class="col l6 s12 m6 fieldset">
                       <div class="input-field">
                        <label for="new_new_asset_code" class="full-bg-label" style='margin:8px 0 !important;'>ASSET NO.<span class="required_field"> *</span></label>
                        <input style='height: 3rem !important; border-right:1px solid #C0C0C0 !important;' name="new_new_asset_code" id="new_new_asset_code" value="<?//= $assetNo; ?>" class="readonly-bg-grey full-bg adjust-width" type="text" readonly="readonly">
                       </div>
                     </div>

                     <div class="col l6 s12 m6 fieldset">
                        <div class="input-field">
                           <label for="new_new_asset_date" class="full-bg-label" style="margin: 7px 0 !important;">ASSET DATE<span class="required_field"> *</span></label>
                           <input type="text" id="new_new_asset_date" name="new_new_asset_date" style="height:3rem !important;" class="readonly-bg-grey full-bg  adjust-width border-top-none" value="<?= date('d/m/Y'); ?>" readonly="readonly">
                        </div>
                     </div>
                    </div>

                    <div class="row">
                     <div class="col l6 s12 m6 fieldset">
                       <div class="input-field">
                             <label for="new_new_asset_name" class="full-bg-label" style='margin:18px 0 !important;'>ASSET NAME<span class="required_field"> *</span></label>
                             <input style='height: 3rem !important;' name="new_new_asset_name" id="new_new_asset_name" maxlength="30" class="full-bg adjust-width border-right" type="text">
                        </div>
                     </div>

                     <div class="col l6 s12 m6 fieldset">
                       <div class="input-field">
                             <label for="new_new_asset_description" class="full-bg-label" style='margin:18px 0 !important;'>ASSET DESCRIPTION</label>
                             <input style='height: 3rem !important;' name="new_new_asset_description" id="new_new_asset_description" maxlength="200" class="full-bg adjust-width border-right" type="text">
                        </div>
                     </div>
                    </div>

                    <div class="row">
                     <div class="col l6 s12 m6 fieldset">
                       <div class="input-field input-set ">
                           <label class="full-bg-label select-label" for="new_new_nature_asset">NATURE OF ASSET<span class="required_field"> *</span></label>
                           <select id="new_new_nature_asset" name="new_new_nature_asset" class="country-dropdown check-label">
                              <option value=""></option>
                              <option value="tangible">TANGIBLE</option>
                              <option value="intangible">INTANGIBLE</option>
                           </select>
                       </div>
                     </div>

                     <div class="col l6 s12 m6 fieldset">
                       <div class="input-field input-set ">
                           <label class="full-bg-label select-label" for="new_new_asset_type">ASSET TYPE<span class="required_field"> *</span></label>
                           <select id="new_new_asset_type" name="new_new_asset_type" class="country-dropdown check-label">
                              <option value=""></option>
                              <?php //foreach($asset_type as $key=>$value){ ?>
                                  <option value="<?//=$value->atl_id?>" ><?//= strtoupper($value->asset_type_name);?></option>
                              <?php //} ?>
                           </select>
                       </div>
                     </div>
                   </div>

                  <div class="row">
                     <div class="col l6 s12 m6 fieldset">
                       <div class="input-field">
                           <label for="new_new_sac_no" class="full-bg-label" style='margin:18px 0 !important;'>SAC NUMBER</label>
                           <input style='height: 3rem !important;' name="new_new_sac_no" id="new_new_sac_no" value='' class="numeric_number full-bg adjust-width border-right" type="text">
                        </div>
                     </div>

                     <div class="col l6 s12 m6 fieldset">
                       <div class="input-field">
                             <label for="new_new_gstin" class="full-bg-label" style='margin:18px 0 !important;'>GOODS & SERVICE TAX (%)</label>
                             <input style='height: 3rem !important;' name="new_new_gstin" id="new_new_gstin" value='' class="numeric_number full-bg adjust-width border-right" type="text">
                           </div>
                     </div>
                   </div>

                   <div class="row">
                     <div class="col l12 s12 m12 fieldset">
                       <div class="input-field">
                             <label for="new_new_asset_cess_tax" class="full-bg-label" style='margin:20px 0 !important;'>CESS TAX (%)</label>
                             <input style='height: 3rem !important;' name="new_new_asset_cess_tax" id="new_new_asset_cess_tax" value='' class="numeric_number full-bg adjust-width border-right" type="text">
                           </div>
                     </div>
                    </div>

                    <div class="row">

                     <div class="col l6 s12 m6 fieldset">
                       <div class="input-field">
                           <label for="new_new_vendor_name" class="full-bg-label" style='margin:18px 0 !important;'>VENDOR NAME<span class="required_field"> *</span></label>
                           <input style='height: 3rem !important;' name="new_new_vendor_name" id="new_new_vendor_name" value='' class="full-bg adjust-width border-right" type="text">
                        </div>
                     </div>

                     <div class="col l6 s12 m6 fieldset">
                       <div class="input-field">
                             <label for="new_new_gstin" class="full-bg-label" style='margin:18px 0 !important;'>GSTIN</label>
                             <input style='height: 3rem !important;' name="new_new_gstin" id="new_new_gstin" value='' class="full-bg adjust-width border-right" type="text">
                           </div>
                     </div>


                   </div>

                  <div class="row">

                     <div class="col l6 s12 m6 fieldset" style="height: 75px;">
                        <div class="input-field input-set">
                           <label class="full-bg-label select-label" for="new_new_country">COUNTRY<span class="required_field"> *</span></label>

                           <select id="new_new_country" name="new_new_country" class="country-dropdown check-label" >
                              <option value=""></option>
                              <?php if($countries != '') {
                                 foreach($countries as $country) { ?>
                                 <option class="icon-location" value="<?php echo $country->country_id; ?>"><?php echo strtoupper($country->country_name); ?></option>
                              <?php }  } ?>
                           </select>
                        </div>
                     </div>

                     <div class="col l6 s12 m6 fieldset" style="height: 75px;">
                        <div class="input-field input-set">
                           <label class="full-bg-label select-label" for="new_new_state">PLACE OF SUPPLY<span class="required_field"> *</span></label>

                           <select class="fullcol js-example-basic-single country-dropdown check-label" name="new_new_state" id="new_new_state" style="width:98% !important; height:56px;">

                           </select>
                        </div>
                     </div>

                  </div>




                    <div class="row" style="margin-top:20px !important;">
                     <div class="col l12 s12 m6">
                     </div>
                     <div class="col l12 s12 m6 fieldset" style='text-align:right; margin-left:-20px;'>
                       <button id="client_close" type="button" class="btn btn-default modal-close cldeselect" data-dismiss="modal" style="font-size:12px; color: black !important;">CANCEL</button>
                       <button id="asset_save" type="button" class="btn btn-default" data-dismiss="modal" style="font-size:12px; color: white !important; background: #7864E9 !important;">SAVE</button>
                     </div>
                    </div>
               </div>
            </div>
         </form>
      </div>
   </div>
</div>


<div id="view-client" class="modal modal-md modal-display">
   <div class="modalheader" style="padding-bottom:7px;">
      <h4>Add New Client</h4>
      <a class="modal-close close-pop cldeselect"><img src="<?php echo base_url();?>asset/images/popupdelete1.png" alt="delete"></a>
   </div>
   <div class="modalbody">
				<div class="client_details" style="margin-top:15px;">
				<form class="" id="create_new_assets_modal" name="create_new_assets_modal" method="post">
					<?php $csrf = array(
		'name' => $this->security->get_csrf_token_name(),
		'hash' => $this->security->get_csrf_hash()
	);
	?>
	<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
					<div class="row">
						<div class="col l12 s10 m10 fieldset">
						  <div class="row">
							<div class="col l6 s12 m6 fieldset">
							  <div class="input-field">
								<label class="full-bg-label" style='margin:8px 0 !important;'>CLIENT NO.</label>
								<input style='height: 3rem !important; border-right:1px solid #C0C0C0 !important;' name="client_no" id="client_no" value='<?=@$custNo?>' class="readonly-bg-grey full-bg adjust-width" type="text" readonly="readonly">
							  </div>
							</div>
							<div class="col l6 s12 m6 fieldset">
								<div class="input-field">
									<label class="full-bg-label" style="margin: 7px 0 !important;">CLIENT DATE</label>
									<input type="text" id="client_date" name="client_date" style="height:3rem !important;" class="readonly-bg-grey full-bg  adjust-width border-top-none" value="<?= date('d/m/Y'); ?>" readonly="readonly">
									<!--<input name="client_date" style="height:2rem !important;" id="client_date" class="full-bg adjust-width border-top-none bdatepicker icon-calendar-gray border-right" type="text" autocomplete="off">-->
								</div>
							</div>
						  </div>
						  <div class="row">
							<div class="col l12 s12 m12 fieldset">
							  <div class="input-field">
									  <label class="full-bg-label" style='margin:8px 0 !important;'>CLIENT NAME<span class="required_field"> *</span></label>
									  <input style='height: 3rem !important;' name="cust_name" id="cust_name" class="full-bg adjust-width border-right" type="text">
									</div>
							</div>
						  </div>
						  <div class="row">
							<div class="col l6 s12 m6 fieldset">
							  <div class="input-field">
									  <label class="full-bg-label" style='margin:8px 0 !important;'>PAN NUMBER</label>
									  <input style='height: 3rem !important;' name="pan_no" id="pan_no" value='' class="full-bg adjust-width border-right" type="text">
									</div>
							</div>
							<div class="col l6 s12 m6 fieldset">
							  <div class="input-field">
									  <label class="full-bg-label" style='margin:8px 0 !important;'>GSTIN</label>
									  <input style='height: 3rem !important;' name="gstin" id="gstin" value='' class="full-bg adjust-width border-right" type="text">
									</div>
							</div>
						 </div>
						 <div class="row">
							<div class="col l12 s12 m12 fieldset">
							  <div class="input-field">
									  <label class="full-bg-label" style='margin:8px 0 !important;'>BILLING ADDRESS</label>
									  <input style='height: 3rem !important;' name="bill_add" id="bill_add" value='' class="full-bg adjust-width border-right" type="text">
									</div>
							</div>
						  </div>
						  <div class="row">
							<div class="col l6 s12 m6 fieldset" style="height:75px; border:0.5px solid #e6f7ff !important;">
							   <div class="input-field input-set ">
                               <label class="full-bg-label select-label" for="country">COUNTRY<span class="required_field"> *</span></label>
                                <select id="country" name="country" class="country-dropdown check-label" >
                                         <option value=""></option>
                                             <?php if($countries != '') {
                                      foreach($countries as $country) { ?>
                                        <option class="icon-location" value="<?php echo $country->country_id; ?>"><?php echo $country->country_name; ?></option>
                                          <?php }  } ?>
                                </select>
							  </div>
							</div>
							<div class="col l6 s12 m6 fieldset" style="height:75px;">
							  <div class="input-field input-set ">
                               <label class="full-bg-label select-label" for="state">STATE WITH STATE CODE<span class="required_field"> *</span></label>
                                <select id="state" name="state" class="country-dropdown check-label" >
                                          <option value=""></option>
                                </select>
							  </div>
							</div>
						  </div>
						  <div class="row">
							<div class="col l6 s12 m6 fieldset" style="height:75px; border:0.5px solid #e6f7ff !important;">
							  <div class="input-field input-set ">
                               <label class="full-bg-label select-label" for="city">CITY<span class="required_field"> *</span></label>
                                <select id="city" name="city" class="country-dropdown check-label" >
                                          <option value=""></option>
                                </select>
							  </div>
							</div>
							<div class="col l6 s12 m6 fieldset">
							  <div class="input-field">
								<label class="full-bg-label" style='margin:8px 0 !important;'>PIN CODE</label>
								<input style='height: 3rem !important;' name="pin_code" id="pin_code" value='' class="full-bg adjust-width border-right" type="text">
							  </div>
							</div>
						  </div>
						  <div class="row" style="margin-top:20px !important;">
							<div class="col l12 s12 m6">
							</div>
							<div class="col l12 s12 m6 fieldset" style='text-align:right; margin-left:-20px;'>
							  <button id="client_close" type="button" class="btn btn-default modal-close cldeselect" data-dismiss="modal" style="font-size:12px;">CANCEL</button>
							  <button id="client_save" type="button" class="btn btn-default" data-dismiss="modal" style="font-size:12px;">SAVE</button>
							</div>
						  </div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

<div id="asset-add1" class="modal modal-md modal-display">
   <div class="modalheader" style="padding-bottom:7px;">
      <h4>Add New Asset</h4>
      <a class="modal-close close-pop cldeselect"><img src="<?php echo base_url();?>asset/images/popupdelete1.png" alt="delete"></a>
   </div>
   <div class="modalbody">
            <div class="client_details" style="margin-top:15px;">
            <form class="" id="client_details_form" name="client_details_form" method="post">
				<?php $csrf = array(
		'name' => $this->security->get_csrf_token_name(),
		'hash' => $this->security->get_csrf_hash()
	);
	?>
	<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
               <div class="row">
                  <div class="col l12 s10 m10 fieldset">
                    <div class="row">
                     <div class="col l6 s12 m6 fieldset">
                       <div class="input-field">
                        <label class="full-bg-label" style='margin:8px 0 !important;'>CLIENT NO.</label>
                        <input style='height: 3rem !important; border-right:1px solid #C0C0C0 !important;' name="client_no" id="client_no" value='<?=@$custNo?>' class="readonly-bg-grey full-bg adjust-width" type="text" readonly="readonly">
                       </div>
                     </div>
                     <div class="col l6 s12 m6 fieldset">
                        <div class="input-field">
                           <label class="full-bg-label" style="margin: 7px 0 !important;">CLIENT DATE</label>
                           <input type="text" id="client_date" name="client_date" style="height:3rem !important;" class="readonly-bg-grey full-bg  adjust-width border-top-none" value="<?= date('d/m/Y'); ?>" readonly="readonly">
                           <!--<input name="client_date" style="height:2rem !important;" id="client_date" class="full-bg adjust-width border-top-none bdatepicker icon-calendar-gray border-right" type="text" autocomplete="off">-->
                        </div>
                     </div>
                    </div>
                    <div class="row">
                     <div class="col l12 s12 m12 fieldset">
                       <div class="input-field">
                             <label class="full-bg-label" style='margin:8px 0 !important;'>CLIENT NAME<span class="required_field"> *</span></label>
                             <input style='height: 3rem !important;' name="cust_name" id="cust_name" class="full-bg adjust-width border-right" type="text">
                           </div>
                     </div>
                    </div>
                    <div class="row">
                     <div class="col l6 s12 m6 fieldset">
                       <div class="input-field">
                             <label class="full-bg-label" style='margin:8px 0 !important;'>PAN NUMBER</label>
                             <input style='height: 3rem !important;' name="pan_no" id="pan_no" value='' class="full-bg adjust-width border-right" type="text">
                           </div>
                     </div>
                     <div class="col l6 s12 m6 fieldset">
                       <div class="input-field">
                             <label class="full-bg-label" style='margin:8px 0 !important;'>GSTIN</label>
                             <input style='height: 3rem !important;' name="gstin" id="gstin" value='' class="full-bg adjust-width border-right" type="text">
                           </div>
                     </div>
                   </div>
                   <div class="row">
                     <div class="col l12 s12 m12 fieldset">
                       <div class="input-field">
                             <label class="full-bg-label" style='margin:8px 0 !important;'>BILLING ADDRESS</label>
                             <input style='height: 3rem !important;' name="bill_add" id="bill_add" value='' class="full-bg adjust-width border-right" type="text">
                           </div>
                     </div>
                    </div>
                    <div class="row">
                     <div class="col l6 s12 m6 fieldset" style="height:75px; border:0.5px solid #e6f7ff !important;">
                        <div class="input-field input-set ">
                               <label class="full-bg-label select-label" for="country">COUNTRY<span class="required_field"> *</span></label>
                                <select id="country" name="country" class="country-dropdown check-label" >
                                         <option value=""></option>
                                             <?php if($countries != '') {
                                      foreach($countries as $country) { ?>
                                        <option class="icon-location" value="<?php echo $country->country_id; ?>"><?php echo $country->country_name; ?></option>
                                          <?php }  } ?>
                                </select>
                       </div>
                     </div>
                     <div class="col l6 s12 m6 fieldset" style="height:75px;">
                       <div class="input-field input-set ">
                               <label class="full-bg-label select-label" for="state">STATE WITH STATE CODE<span class="required_field"> *</span></label>
                                <select id="state" name="state" class="country-dropdown check-label" >
                                          <option value=""></option>
                                </select>
                       </div>
                     </div>
                    </div>
                    <div class="row">
                     <div class="col l6 s12 m6 fieldset" style="height:75px; border:0.5px solid #e6f7ff !important;">
                       <div class="input-field input-set ">
                               <label class="full-bg-label select-label" for="city">CITY<span class="required_field"> *</span></label>
                                <select id="city" name="city" class="country-dropdown check-label" >
                                          <option value=""></option>
                                </select>
                       </div>
                     </div>
                     <div class="col l6 s12 m6 fieldset">
                       <div class="input-field">
                        <label class="full-bg-label" style='margin:8px 0 !important;'>PIN CODE</label>
                        <input style='height: 3rem !important;' name="pin_code" id="pin_code" value='' class="full-bg adjust-width border-right" type="text">
                       </div>
                     </div>
                    </div>
                    <div class="row" style="margin-top:20px !important;">
                     <div class="col l12 s12 m6">
                     </div>
                     <div class="col l12 s12 m6 fieldset" style='text-align:right; margin-left:-20px;'>
                       <button id="client_close" type="button" class="btn btn-default modal-close cldeselect" data-dismiss="modal" style="font-size:12px;">CANCEL</button>
                       <button id="client_save" type="button" class="btn btn-default" data-dismiss="modal" style="font-size:12px;">SAVE</button>
                     </div>
                    </div>
               </div>
            </div>
         </form>
      </div>
   </div>
</div>

<div id="subscription_modal" class="modal modal-md modal-display" style="max-width: 385px !important;">
		<div class="modalheader border-bottom" style="padding-bottom:7px; padding-top:18px !important; text-align:center;">
			<h4 style="padding-left:0px; font-size:17px !important;	">Subscription & Buy Add-Ons</h4>
			<a class="modal-close close-pop" style="margin-right:-15px !important;"><img src="<?php echo base_url();?>asset/images/popupdelete1.png" alt="delete"></a>
		</div>
		<div class="modalbody">
			<div class="choose_subscription" style="margin-top:15px;">
				<form class="" id="subscription_details_form" name="subscription_form" action="<?php echo base_url();?>subscription/modal_redirect" method="post">
					<?php $csrf = array(
		'name' => $this->security->get_csrf_token_name(),
		'hash' => $this->security->get_csrf_hash()
	);
	?>
	<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
					<div class="row border-bottom">
					<div class="col s12 m12 l12">
						 <div class="form-check col s12 m12 l8" style="margin-bottom:5px;">
							<input type="radio" class="form-check-input" id="renew_sub" name="sub_add_ons" value="renew" checked>
							<label class="form-check-label" for="renew_sub">Renew Subscription</label>
						 </div>
						</div>

						<div class="col s12 m12 l12">
						 <div class="form-check col s12 m12 l8" style="margin-bottom:5px;">
							<input type="radio" class="form-check-input" id="add-on" name="sub_add_ons" value="addons">
							<label class="form-check-label" for="add-on">Buy Add-Ons</label>
						 </div>
						</div>

						<div class="col s12 m12 l12">
						 <div class="form-check col s12 m12 l8" style="margin-bottom:15px;">
							<input type="radio" class="form-check-input" id="subsc_addons" name="sub_add_ons" value="both">
							<label class="form-check-label" for="subsc_addons">Both</label>
						 </div>
						</div>
					</div>
					<div class="row" style="margin-bottom:8px !important; margin-top:-10px !important;">
						<div class="col l12 s12 m6"></div>
						<div class="col l12 s12 m6 fieldset" style='text-align:right; margin-left:-20px;'>
							<input id="choose_close" type="button" class="btn btn-default modal-close" data-dismiss="modal" style="font-size:12px; color:black;" value="CANCEL"></input>
							<input id="choose_proceed" type="submit" class="btn btn-default modal-close" data-dismiss="modal" style="font-size:12px; background-color:#7864e9; color:white;" value="PROCEED"></input>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>

<!-- Add new Expense -->

<div id="new-expense" class="modal modal-md modal-display" style="margin-top:-3% !important;">
   <style>
	#new-expense.ps-container > .ps-scrollbar-x-rail > .ps-scrollbar-x {
		display: none !important;
	}

	#exp_name:focus{
		border-left:3px solid #ff85a1 !important;
	}

	#sac_no:focus{
		border-left:3px solid #ff85a1 !important;
	}

	#gs_tax:focus{
		border-left:3px solid #ff85a1 !important;
	}

	#cess_tax:focus{
		border-left:3px solid #ff85a1 !important;
	}

	#other_tax:focus{
		border-left:3px solid #ff85a1 !important;
	}

	#other_per:focus{
		border-left:3px solid #ff85a1 !important;
	}

	#new-expense .modalbody {
		overflow-x: hidden;
	}

	#new-expense input.full-bg {
		padding: 20px 19px 8px 19px !important;
	}


   </style>
   <div class="modalheader" style="padding-bottom:7px;">
      <h4>Add New Expense</h4>
      <a class="modal-close close-pop deselect"><img src="<?php echo base_url();?>asset/images/popupdelete1.png" alt="delete"></a>
   </div>
   <div class="modalbody">
				<div class="client_details" style="margin-top:15px;">
				<form class="" id="expense_details_form" name="equia_levy" method="post">
					<?php $csrf = array(
		'name' => $this->security->get_csrf_token_name(),
		'hash' => $this->security->get_csrf_hash()
	);
	?>
	<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
					<div class="row">
						<div class="col l12 s10 m10 fieldset">
						  <div class="row">
							<div class="col l6 s12 m6 fieldset">
							  <div class="input-field">
								<label class="full-bg-label" style='margin:8px 0 !important;'>EXPENSE NO.</label>
								<input style='height: 3rem !important; padding-left: 18px !important; border-right:1px solid #C0C0C0 !important;' name="exp_no" id="exp_no" value='<?//=$exp_code?>' class="readonly-bg-grey full-bg adjust-width border-right" type="text">
							  </div>
							</div>
							<div class="col l6 s12 m6 fieldset">
								<div class="input-field">
									<label class="full-bg-label">EXPENSE DATE</label>
									<input type="text" id="exp_date" name="exp_date" style="padding-left: 18px !important; height:3rem !important;" class="readonly-bg-grey full-bg adjust-width border-top-none" value="<?= date('d/m/Y'); ?>" readonly="readonly">
									<!--<input name="item_date" style="height:2rem !important;" id="item_date" class="full-bg adjust-width border-top-none bdatepicker icon-calendar-gray border-right" type="text" autocomplete="off">-->
								</div>
							</div>
						  </div>
						  <div class="row">
							<div class="col l12 s12 m12 fieldset">
							  <div class="input-field">
								<label class="full-bg-label" style='margin:8px 0 !important;'>EXPENSE NAME<span class="required_field"> *</span></label>
								<input style='height: 3rem !important;' name="exp_name" id="exp_name" value='' class="full-bg adjust-width border-right" type="text">
							  </div>
							</div>
						  </div>
						  <div class="row">
							<div class="col l12 s12 m12 fieldset">
							  <div class="input-field input-set" style="padding-left:10px;">
                               <label class="full-bg-label select-label" for="exp_category">EXPENSE CATEGORY<span class="required_field"> *</span></label>
                               <select id="exp_category" name="exp_category" class="country-dropdown check-label">
									<option value=""></option>
									  <option value="Marketing & Advertising">MARKETING & ADVERTISING</option>
									  <option value="Automobile Expense">AUTOMOBILE EXPENSE</option>
									  <option value="Bank Fees & Charges">BANK FEES & CHARGES</option>
									  <option value="Consultant Fees">CONSULTANT FEES</option>
									  <option value="Courier Expense">COURIER EXPENSE</option>
									  <option value="Credit Card Charges">CREDIT CARD CHARGES</option>
									  <option value="IT & Internet Expense">IT & INTERNET EXPENSE</option>
									  <option value="Janitorial Expense">HANITORIAL EXPENSE</option>
									  <option value="Lodging">LODGING</option>
									  <option value="Meals & Entertainment">MEALS & ENTERTAINMENT</option>
									  <option value="Office Supplies">OFFICE SUPPLIES</option>
									  <option value="Printing & Stationery">PRINTING & STATIONERY</option>
									  <option value="Rent Expenses">RENT EXPENSES</option>
									  <option value="Repairs & Maintenance">REPAIRS & MAINTENANCE</option>
									  <option value="Telephone Expense">TELEPHONE EXPENSE</option>
									  <option value="Travel Expense"> TRAVEL EXPENSE</option>
									  <option value="Other Expenses">OTHER EXPENSE</option>
                                </select>
							  </div>
							</div>
						  </div>
						  <div class="row">
							<div class="col l12 s12 m12 fieldset">
							  <div class="input-field">
								<label class="full-bg-label" style='margin:8px 0 !important;'>SAC NO.</label>
								<input style='height: 3rem !important;' name="sac_no" id="sac_no" value='' class="full-bg adjust-width border-right numeric_number" type="text">
							  </div>
							</div>
						  </div>
						  <div class="row">
							<div class="col l6 s12 m6 fieldset">
							  <div class="input-field">
									  <label class="full-bg-label" style='margin:8px 0 !important;'>GOOD & SERVICE TAX (%)</label>
									  <input style='height: 3rem !important;' name="gs_tax" id="gs_tax" value='' class="full-bg adjust-width border-right numeric_number" type="text">
									</div>
							</div>
							<div class="col l6 s12 m6 fieldset">
							  <div class="input-field">
									  <label class="full-bg-label" style='margin:8px 0 !important;'>CESS TAX (%)</label>
									  <input style='height: 3rem !important;' name="cess_tax" id="cess_tax" value='' class="full-bg adjust-width border-right numeric_number" type="text">
									</div>
							</div>
						 </div>
						 <div class="row">
							<div class="col l6 s12 m6 fieldset">
							  <div class="input-field">
									  <label class="full-bg-label" style='margin:8px 0 !important;'>OTHER TAX NAME</label>
									  <input style='height: 3rem !important;' name="other_tax" id="other_tax" value='' class="full-bg adjust-width border-right" type="text">
									</div>
							</div>
							<div class="col l6 s12 m6 fieldset">
							  <div class="input-field">
								<label class="full-bg-label" style='margin:8px 0 !important;'>OTHER TAX (%)</label>
								<input style='height: 3rem !important;' name="other_per" id="other_per" value='' class="full-bg adjust-width border-right numeric_number" type="text">
							  </div>
							</div>
						 </div>
						  <div class="row" style="margin-top:20px !important;">
							<div class="col l12 s12 m6">
							</div>
							<div class="col l12 s12 m6 fieldset" style='text-align:right; margin-left:-20px;'>
							  <button id="item_close" type="button" class="btn btn-default modal-close deselect" data-dismiss="modal" style="font-size:12px; height:41px !important;">CANCEL</button>
							  <input id="item_save" type="submit" name="submit" value="SAVE" class="btn-flat theme-primary-btn theme-btn theme-btn-large pays  modal-trigger modal-close" style="padding: 0px 8px !important;">
							  <!--button id="item_save" type="button" class="btn btn-default modal-close" data-dismiss="modal" style="font-size:12px;">SAVE</button-->
							</div>
						  </div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

<!-- END -->
<div id="new-item" class="modal modal-md modal-display">
   <div class="modalheader" style="padding-bottom:7px;">
      <h4>Add New Item</h4>
      <a class="modal-close close-pop itdeselect"><img src="<?php echo base_url();?>asset/images/popupdelete1.png" alt="delete"></a>
   </div>
   <div class="modalbody">
				<div class="client_details" style="margin-top:15px;">
				<form class="" id="item_details_form" name="equia_levy" method="post">
					<?php $csrf = array(
		'name' => $this->security->get_csrf_token_name(),
		'hash' => $this->security->get_csrf_hash()
	);
	?>
	<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
					<div class="row">
						<div class="col l12 s10 m10 fieldset">
						  <div class="row">
							<div class="col l6 s12 m6 fieldset">
							  <div class="input-field">
								<label class="full-bg-label" style='margin:8px 0 !important;'>ITEM NO.</label>
								<input style='height: 3rem !important; border-right:1px solid #C0C0C0 !important;' name="item_no" id="item_no" value='<?=@$itemNo?>' class="readonly-bg-grey full-bg adjust-width border-right" type="text">
							  </div>
							</div>
							<div class="col l6 s12 m6 fieldset">
								<div class="input-field">
									<label class="full-bg-label">ITEM DATE</label>
									<input type="text" id="item_date" name="item_date" style="height:3rem !important;" class="readonly-bg-grey full-bg  adjust-width border-top-none" value="<?= date('d/m/Y'); ?>" readonly="readonly">
									<!--<input name="item_date" style="height:2rem !important;" id="item_date" class="full-bg adjust-width border-top-none bdatepicker icon-calendar-gray border-right" type="text" autocomplete="off">-->
								</div>
							</div>
						  </div>
						  <div class="row">
							<div class="col l12 s12 m12 fieldset">
							  <div class="input-field">
								<label class="full-bg-label" style='margin:8px 0 !important;'>ITEM NAME<span class="required_field"> *</span></label>
								<input style='height: 3rem !important;' name="item_name" id="item_name" value='' class="full-bg adjust-width border-right" type="text">
							  </div>
							</div>
						  </div>
						  <div class="row">
							<div class="col l12 s12 m12 fieldset">
							  <div class="input-field">
								<label class="full-bg-label" style='margin:8px 0 !important;'>SAC NO.<span class="required_field"> *</span></label>
								<input style='height: 3rem !important;' name="sac_no" id="sac_no" value='' class="full-bg adjust-width border-right" type="text">
							  </div>
							</div>
						  </div>
						  <div class="row">
							<div class="col l6 s12 m6 fieldset">
							  <div class="input-field">
									  <label class="full-bg-label" style='margin:8px 0 !important;'>GOOD & SERVICE TAX (%)<span class="required_field"> *</span></label>
									  <input style='height: 3rem !important;' name="gs_tax" id="gs_tax" value='' class="full-bg adjust-width border-right" type="text">
									</div>
							</div>
							<div class="col l6 s12 m6 fieldset">
							  <div class="input-field">
									  <label class="full-bg-label" style='margin:8px 0 !important;'>CESS TAX (%)</label>
									  <input style='height: 3rem !important;' name="cess_tax" id="cess_tax" value='' class="full-bg adjust-width border-right" type="text">
									</div>
							</div>
						 </div>
						 <div class="row">
							<div class="col l6 s12 m6 fieldset">
							  <div class="input-field">
									  <label class="full-bg-label" style='margin:8px 0 !important;'>OTHER TAX NAME</label>
									  <input style='height: 3rem !important;' name="other_tax" id="other_tax" value='' class="full-bg adjust-width border-right" type="text">
									</div>
							</div>
							<div class="col l6 s12 m6 fieldset">
							  <div class="input-field">
								<label class="full-bg-label" style='margin:8px 0 !important;'>OTHER TAX PERCENTAGE</label>
								<input style='height: 3rem !important;' name="other_per" id="other_per" value='' class="full-bg adjust-width border-right" type="text">
							  </div>
							</div>
						 </div>
						  <div class="row" style="margin-top:20px !important;">
							<div class="col l12 s12 m6">
							</div>
							<div class="col l12 s12 m6 fieldset" style='text-align:right; margin-left:-20px;'>
							  <button id="item_close" type="button" class="btn btn-default modal-close itdeselect" data-dismiss="modal" style="font-size:12px; height:41px !important;">CANCEL</button>
							  <input id="item_save" type="submit" name="submit" value="SAVE" class="btn-flat theme-primary-btn theme-btn theme-btn-large pays  modal-trigger modal-close" style="padding: 0px 8px !important;">
							  <!--button id="item_save" type="button" class="btn btn-default modal-close" data-dismiss="modal" style="font-size:12px;">SAVE</button-->
							</div>
						  </div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>


	<div id="view_access_list" class="modal modal-md modal-display" style="max-width:1200px !important; min-width:1130px !important; max-height:90% !important; margin-top:-3% !important;">
		<div class="modalheader border-bottom" style="padding-bottom:7px; padding-top:18px !important; text-align:;">
			<h4 style="padding-left:0px; font-size:17px !important;	">Access List</h4>
			<a class="modal-close close-pop" style="margin-right:-15px !important;"><img src="<?php echo base_url();?>asset/images/popupdelete1.png" alt="delete"></a>
		</div>
		<div class="modalbody">
			<div class="view_expense_1" style="margin-top:0px;">
				<form class="" id="expense_details_view" name="expense_form" method="post">
					<?php $csrf = array(
		'name' => $this->security->get_csrf_token_name(),
		'hash' => $this->security->get_csrf_hash()
	);
	?>
	<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
					<table border="2" id="tab-table" class="tabs-class">
                           <tbody>
                              <tr class="bg-row tabss" id="over-boder" style="background: #e0e2e8;">
                                 <th class="tab1" style="width:25%;">
                                     <p style="color:#000000 !important;">Modules</p>
                                 </th>
								 <!--th class="tab1" style="width:50px;">
                                     <p style="text-align:center; color:#000000 !important;">Client</p>
                                 </th>
								 <th class="tab1" style="width:50px;">
                                     <p style="text-align:center; color:#000000 !important;">Vendor</p>
                                 </th-->
								 <th class="tab1" style="width:15%;">
                                     <p style="text-align:center; color:#000000 !important;">Employee</p>
                                 </th>
								 <th class="tab1" style="width:15%;">
                                     <p style="text-align:center; color:#000000 !important;">HR</p>
                                 </th>
								 <th class="tab1" style="width:15%;">
                                     <p style="text-align:center; color:#000000 !important;">CA/Tax <br>Specialist</p>
                                 </th>
								 <th class="tab1" style="width:15%;">
                                     <p style="text-align:center; color:#000000 !important;">Accountant</p>
                                 </th>
								 <th class="tab1" style="width:15%">
                                     <p style="text-align:center; color:#000000 !important;">Admin</p>
                                 </th>
							  </tr>
							  <tr class="boder-tr">
                                 <td>
                                    <p class="txt_pnt">My Business Analytics</p>
								 </td>
								 <!--td>
                                    <p class="txt_pnt"></p>
								 </td>
								 <td>
                                    <p class="txt_pnt"></p>
								 </td-->
								 <td>
                                    <p class="txt_pnt"></p>
								 </td>
								 <td>
                                    <p class="txt_pnt"></p>
								 </td>
								 <td>
                                    <p class="txt_pnt"></p>
								 </td>
								 <td>
                                    <p class="txt_pnt"></p>
								 </td>
								 <td>
                                    <p class="txt_pnt" style="text-align:center;">&#10004;</p>
								 </td>
							  </tr>
							  <tr class="boder-tr">
                                 <td>
                                    <p class="txt_pnt">My Sales</p>
								 </td>
								 <!--td>
                                    <p class="txt_pnt"></p>
								 </td>
								 <td>
                                    <p class="txt_pnt"></p>
								 </td-->
								 <td>
                                    <p class="txt_pnt"></p>
								 </td>
								 <td>
                                    <p class="txt_pnt"></p>
								 </td>
								 <td>
                                    <p class="txt_pnt"></p>
								 </td>
								 <td>
                                    <p class="txt_pnt" style="text-align:center;">&#10004;</p>
								 </td>
								 <td>
                                    <p class="txt_pnt" style="text-align:center;">&#10004;</p>
								 </td>
							  </tr>
							  <tr class="boder-tr">
                                 <td>
                                    <p class="txt_pnt">Client Portal</p>
								 </td>
								 <!--td>
                                    <p class="txt_pnt">Individual Entries</p>
								 </td>
								 <td>
                                    <p class="txt_pnt"></p>
								 </td-->
								 <td>
                                    <p class="txt_pnt"></p>
								 </td>
								 <td>
                                    <p class="txt_pnt"></p>
								 </td>
								 <td>
                                    <p class="txt_pnt"></p>
								 </td>
								 <td>
                                    <p class="txt_pnt" style="text-align:center;"> All Clients </p>
								 </td>
								 <td>
                                    <p class="txt_pnt" style="text-align:center;"> All Clients </p>
								 </td>
							  </tr>
							  <tr class="boder-tr">
                                 <td>
                                    <p class="txt_pnt">Expense Voucher</p>
								 </td>
								 <!--td>
                                    <p class="txt_pnt"></p>
								 </td>
								 <td>
                                    <p class="txt_pnt"></p>
								 </td-->
								 <td>
                                    <p class="txt_pnt" style="text-align:center;">Individual Entries</p>
								 </td>
								 <td>
                                    <p class="txt_pnt" style="text-align:center;">Individual Entries</p>
								 </td>
								 <td>
                                    <p class="txt_pnt"></p>
								 </td>
								 <td>
                                    <p class="txt_pnt" style="text-align:center;"> All Entries </p>
								 </td>
								 <td>
                                    <p class="txt_pnt" style="text-align:center;"> All Entries </p>
								 </td>
							  </tr>
							  <tr class="boder-tr">
                                 <td>
                                    <p class="txt_pnt">My Expenses</p>
								 </td>
								 <!--td>
                                    <p class="txt_pnt"></p>
								 </td>
								 <td>
                                    <p class="txt_pnt"></p>
								 </td-->
								 <td>
                                    <p class="txt_pnt"></p>
								 </td>
								 <td>
                                    <p class="txt_pnt"></p>
								 </td>
								 <td>
                                    <p class="txt_pnt"></p>
								 </td>
								 <td>
                                    <p class="txt_pnt" style="text-align:center;">&#10004;</p>
								 </td>
								 <td>
                                    <p class="txt_pnt" style="text-align:center;">&#10004;</p>
								 </td>
							  </tr>
							  <tr class="boder-tr">
                                 <td>
                                    <p class="txt_pnt">Vendor Portal</p>
								 </td>
								 <!--td>
                                    <p class="txt_pnt"></p>
								 </td>
								 <td>
                                    <p class="txt_pnt">Individual Entries</p>
								 </td-->
								 <td>
                                    <p class="txt_pnt"></p>
								 </td>
								 <td>
                                    <p class="txt_pnt"></p>
								 </td>
								 <td>
                                    <p class="txt_pnt"></p>
								 </td>
								 <td>
                                    <p class="txt_pnt" style="text-align:center;">All Vendors</p>
								 </td>
								 <td>
                                    <p class="txt_pnt" style="text-align:center;">All Vendors</p>
								 </td>
							  </tr>
							  <tr class="boder-tr">
                                 <td>
                                    <p class="txt_pnt">My Assets & Depreciation</p>
								 </td>
								 <!--td>
                                    <p class="txt_pnt"></p>
								 </td>
								 <td>
                                    <p class="txt_pnt"></p>
								 </td-->
								 <td>
                                    <p class="txt_pnt"></p>
								 </td>
								 <td>
                                    <p class="txt_pnt"></p>
								 </td>
								 <td>
                                    <p class="txt_pnt"></p>
								 </td>
								 <td>
                                    <p class="txt_pnt" style="text-align:center;">&#10004;</p>
								 </td>
								 <td>
                                    <p class="txt_pnt" style="text-align:center;">&#10004;</p>
								 </td>
							  </tr>
							  <tr class="boder-tr">
                                 <td>
                                    <p class="txt_pnt">My Receipts & Payments</p>
								 </td>
								 <!--td>
                                    <p class="txt_pnt"></p>
								 </td>
								 <td>
                                    <p class="txt_pnt"></p>
								 </td-->
								 <td>
                                    <p class="txt_pnt"></p>
								 </td>
								 <td>
                                    <p class="txt_pnt"></p>
								 </td>
								 <td>
                                    <p class="txt_pnt"></p>
								 </td>
								 <td>
                                    <p class="txt_pnt" style="text-align:center;">&#10004;</p>
								 </td>
								 <td>
                                    <p class="txt_pnt" style="text-align:center;">&#10004;</p>
								 </td>
							  </tr>
							  <tr class="boder-tr">
                                 <td>
                                    <p class="txt_pnt">My Employees</p>
								 </td>
								 <!--td>
                                    <p class="txt_pnt"></p>
								 </td>
								 <td>
                                    <p class="txt_pnt"></p>
								 </td-->
								 <td>
                                    <p class="txt_pnt"></p>
								 </td>
								 <td>
                                    <p class="txt_pnt" style="text-align:center;">All Employees</p>
								 </td>
								 <td>
                                    <p class="txt_pnt"></p>
								 </td>
								 <td>
                                    <p class="txt_pnt" style="text-align:center;">All Employees</p>
								 </td>
								 <td>
                                    <p class="txt_pnt" style="text-align:center;">All Employees</p>
								 </td>
							  </tr>
							  <tr class="boder-tr">
                                 <td>
                                    <p class="txt_pnt">My Cash & Bank</p>
								 </td>
								 <!--td>
                                    <p class="txt_pnt"></p>
								 </td>
								 <td>
                                    <p class="txt_pnt"></p>
								 </td-->
								 <td>
                                    <p class="txt_pnt"></p>
								 </td>
								 <td>
                                    <p class="txt_pnt"></p>
								 </td>
								 <td>
                                    <p class="txt_pnt"></p>
								 </td>
								 <td>
                                    <p class="txt_pnt" style="text-align:center;">&#10004;</p>
								 </td>
								 <td>
                                    <p class="txt_pnt" style="text-align:center;">&#10004;</p>
								 </td>
							  </tr>
							  <tr class="boder-tr">
                                 <td>
                                    <p class="txt_pnt">My Comapany Profile</p>
								 </td>
								 <!--td>
                                    <p class="txt_pnt"></p>
								 </td>
								 <td>
                                    <p class="txt_pnt"></p>
								 </td-->
								 <td>
                                    <p class="txt_pnt"></p>
								 </td>
								 <td>
                                    <p class="txt_pnt"></p>
								 </td>
								 <td>
                                    <p class="txt_pnt"></p>
								 </td>
								 <td>
                                    <p class="txt_pnt" style="text-align:center;">&#10004;</p>
								 </td>
								 <td>
                                    <p class="txt_pnt" style="text-align:center;">&#10004;</p>
								 </td>
							  </tr>
							  <tr class="boder-tr">
                                 <td>
                                    <p class="txt_pnt">My Tax Summary</p>
								 </td>
								 <!--td>
                                    <p class="txt_pnt"></p>
								 </td>
								 <td>
                                    <p class="txt_pnt"></p>
								 </td-->
								 <td>
                                    <p class="txt_pnt"></p>
								 </td>
								 <td>
                                    <p class="txt_pnt"></p>
								 </td>
								 <td>
                                    <p class="txt_pnt" style="text-align:center;">&#10004;</p>
								 </td>
								 <td>
                                    <p class="txt_pnt" style="text-align:center;">&#10004;</p>
								 </td>
								 <td>
                                    <p class="txt_pnt" style="text-align:center;">&#10004;</p>
								 </td>
							  </tr>
							  <tr class="boder-tr">
                                 <td>
                                    <p class="txt_pnt">My Statement of Accounts</p>
								 </td>
								 <!--td>
                                    <p class="txt_pnt"></p>
								 </td>
								 <td>
                                    <p class="txt_pnt"></p>
								 </td-->
								 <td>
                                    <p class="txt_pnt"></p>
								 </td>
								 <td>
                                    <p class="txt_pnt"></p>
								 </td>
								 <td>
                                    <p class="txt_pnt"></p>
								 </td>
								 <td>
                                    <p class="txt_pnt" style="text-align:center;">&#10004;</p>
								 </td>
								 <td>
                                    <p class="txt_pnt" style="text-align:center;">&#10004;</p>
								 </td>
							  </tr>
							  <tr class="boder-tr">
                                 <td>
                                    <p class="txt_pnt">My Resource Center</p>
								 </td>
								 <!--td>
                                    <p class="txt_pnt"></p>
								 </td>
								 <td>
                                    <p class="txt_pnt"></p>
								 </td-->
								 <td>
                                    <p class="txt_pnt" style="text-align:center;">&#10004;</p>
								 </td>
								 <td>
                                    <p class="txt_pnt" style="text-align:center;">&#10004;</p>
								 </td>
								 <td>
                                    <p class="txt_pnt" style="text-align:center;">&#10004;</p>
								 </td>
								 <td>
                                    <p class="txt_pnt" style="text-align:center;">&#10004;</p>
								 </td>
								 <td>
                                    <p class="txt_pnt" style="text-align:center;">&#10004;</p>
								 </td>
							  </tr>
							  <tr class="boder-tr">
                                 <td>
                                    <p class="txt_pnt">Personal Profile</p>
								 </td>
								 <!--td>
                                    <p class="txt_pnt"></p>
								 </td>
								 <td>
                                    <p class="txt_pnt"></p>
								 </td-->
								 <td>
                                    <p class="txt_pnt" style="text-align:center;">Individual Profile</p>
								 </td>
								 <td>
                                    <p class="txt_pnt" style="text-align:center;">Individual Profile</p>
								 </td>
								 <td>
                                    <p class="txt_pnt" style="text-align:center;">Individual Profile</p>
								 </td>
								 <td>
                                    <p class="txt_pnt" style="text-align:center;">Individual Profile</p>
								 </td>
								 <td>
                                    <p class="txt_pnt" style="text-align:center;">All Profile</p>
								 </td>
							  </tr>
							  <tr class="boder-tr">
                                 <td>
                                    <p class="txt_pnt">My Settings</p>
								 </td>
								 <!--td>
                                    <p class="txt_pnt"></p>
								 </td>
								 <td>
                                    <p class="txt_pnt"></p>
								 </td-->
								 <td>
                                    <p class="txt_pnt"></p>
								 </td>
								 <td>
                                    <p class="txt_pnt"></p>
								 </td>
								 <td>
                                    <p class="txt_pnt"></p>
								 </td>
								 <td>
                                    <p class="txt_pnt" style="text-align:center;">&#10004;</p>
								 </td>
								 <td>
                                    <p class="txt_pnt" style="text-align:center;">&#10004;</p>
								 </td>
							  </tr>
							  <tr class="boder-tr">
                                 <td>
                                    <p class="txt_pnt">My Billing & Subscription</p>
								 </td>
								 <!--td>
                                    <p class="txt_pnt"></p>
								 </td>
								 <td>
                                    <p class="txt_pnt"></p>
								 </td-->
								 <td>
                                    <p class="txt_pnt"></p>
								 </td>
								 <td>
                                    <p class="txt_pnt"></p>
								 </td>
								 <td>
                                    <p class="txt_pnt"></p>
								 </td>
								 <td>
                                    <p class="txt_pnt" style="text-align:center;">&#10004;</p>
								 </td>
								 <td>
                                    <p class="txt_pnt" style="text-align:center;">&#10004;</p>
								 </td>
							  </tr>
							  <tr class="boder-tr">
                                 <td>
                                    <p class="txt_pnt">My Document Locker</p>
								 </td>
								 <!--td>
                                    <p class="txt_pnt"></p>
								 </td>
								 <td>
                                    <p class="txt_pnt"></p>
								 </td-->
								 <td>
                                    <p class="txt_pnt"></p>
								 </td>
								 <td>
                                    <p class="txt_pnt"></p>
								 </td>
								 <td>
                                    <p class="txt_pnt"></p>
								 </td>
								 <td>
                                    <p class="txt_pnt" style="text-align:center;">&#10004;</p>
								 </td>
								 <td>
                                    <p class="txt_pnt" style="text-align:center;">&#10004;</p>
								 </td>
							  </tr>
							  <tr class="boder-tr">
                                 <td>
                                    <p class="txt_pnt">My Refer & Earn</p>
								 </td>
								 <!--td>
                                    <p class="txt_pnt"></p>
								 </td>
								 <td>
                                    <p class="txt_pnt"></p>
								 </td-->
								 <td>
                                    <p class="txt_pnt"></p>
								 </td>
								 <td>
                                    <p class="txt_pnt"></p>
								 </td>
								 <td>
                                    <p class="txt_pnt"></p>
								 </td>
								 <td>
                                    <p class="txt_pnt" style="text-align:center;">&#10004;</p>
								 </td>
								 <td>
                                    <p class="txt_pnt" style="text-align:center;">&#10004;</p>
								 </td>
							  </tr>
							  <tr class="boder-tr">
                                 <td>
                                    <p class="txt_pnt">My Activity History</p>
								 </td>
								 <!--td>
                                    <p class="txt_pnt"></p>
								 </td>
								 <td>
                                    <p class="txt_pnt"></p>
								 </td-->
								 <td>
                                    <p class="txt_pnt"></p>
								 </td>
								 <td>
                                    <p class="txt_pnt"></p>
								 </td>
								 <td>
                                    <p class="txt_pnt"></p>
								 </td>
								 <td>
                                    <p class="txt_pnt"></p>
								 </td>
								 <td>
                                    <p class="txt_pnt" style="text-align:center;">&#10004;</p>
								 </td>
							  </tr>
							  <tr class="boder-tr">
                                 <td>
                                    <p class="txt_pnt">Feedback</p>
								 </td>
								 <!--td>
                                    <p class="txt_pnt"></p>
								 </td>
								 <td>
                                    <p class="txt_pnt"></p>
								 </td-->
								 <td>
                                    <p class="txt_pnt"></p>
								 </td>
								 <td>
                                    <p class="txt_pnt"></p>
								 </td>
								 <td>
                                    <p class="txt_pnt"></p>
								 </td>
								 <td>
                                    <p class="txt_pnt"></p>
								 </td>
								 <td>
                                    <p class="txt_pnt" style="text-align:center;">&#10004;</p>
								 </td>
							  </tr>
							  <tr class="boder-tr">
                                 <td>
                                    <p class="txt_pnt">Deactivate Account</p>
								 </td>
								 <!--td>
                                    <p class="txt_pnt"></p>
								 </td>
								 <td>
                                    <p class="txt_pnt"></p>
								 </td-->
								 <td>
                                    <p class="txt_pnt"></p>
								 </td>
								 <td>
                                    <p class="txt_pnt"></p>
								 </td>
								 <td>
                                    <p class="txt_pnt"></p>
								 </td>
								 <td>
                                    <p class="txt_pnt"></p>
								 </td>
								 <td>
                                    <p class="txt_pnt" style="text-align:center;">&#10004;</p>
								 </td>
							  </tr>
						   </tbody>
					</table>
				</form>
			</div>
		</div>
	</div>

	<div id="view_expense" class="modal modal-md modal-display" style="max-width:80% !important; min-width:55% !important;">
		<style>
			.important{
				border:1px solid #ff0000 !important;
			}
		</style>
		<script>
			$(document).ready(function(){
				$("#exp_vou_gst").focus();
				$(".gstfield").addClass("important");
			});
		</script>
		<div class="modalheader border-bottom" style="padding-bottom:7px; padding-top:18px !important; text-align:;">
			<h4 style="padding-left:0px; font-size:17px !important;">View Expense Vouchers </h4>
			<!--<div class="col l6 s12 m12 searchbtn">
            <a class="filter-search btn-search btn" style="margin-left:-145px !important; margin-top:-10px;">
            <input type="text" name="search" id="search_dc" class="search-hide-show" style="display:none" />
            <i class="material-icons ser search-btn-field-show">search</i>
            </a>
            </div>-->
			<a class="exp_close modal-close close-pop" style="margin-right:-15px !important;"><img src="<?php echo base_url();?>asset/images/popupdelete1.png" alt="delete"></a>
		</div>
		<div class="modalbody">
			<div class="view_expense" style="margin-top:0px;">
				<form class="" id="expense_details_view" name="expense_form" method="post">
					<?php $csrf = array(
		'name' => $this->security->get_csrf_token_name(),
		'hash' => $this->security->get_csrf_hash()
	);
	?>
	<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
					<div style="overflow-y:scroll !important; height:350px; padding:0 5px;">
						<table border="2" id="tab-table" class="tabs-class">
                              <tr class="bg-row tabss" id="over-boder">
                                 <th class="tab1" style="width:12% !important;">
									 <p>Expense No.</p>
                                 </th>
                                 <th class="tab1" style="width:13% !important;">
                                    <p>Expense Date</p>
                                 </th>
                                 <th class="tab2" style="width:29% !important;">
                                    <p>Client Name</p>
                                 </th>
								 <th class="tab2" style="width:20% !important;">
                                    <p>Made By</p>
                                 </th>
								 <th class="tab2" style="width:13% !important;">
                                    <p>Expense Amt</p>
                                 </th>
								 <th class="tab2" style="width:12% !important;">
                                    <p>Action</p>
                                 </th>
							  </tr>
                            <tbody id="myTable1">
							  <tr class="boder-tr">
                                 <td>
                                    <p class="txt_pnt">Windchimes Communications</p>
								 </td>
								 <td>
                                    <p class="txt_pnt">16, sep 2017</p>
								 </td>
								 <td>
                                    <p class="txt_pnt">Xyz name</p>
								 </td>
								 <td>
                                    <p class="txt_pnt">Travel</p>
								 </td>
								 <td>
                                    <p class="txt_pnt">2000</p>

								 </td>
								 <td>
                                    <a style="padding-right:5px;" id="add_exp_inv" href="#" class="add_to_invoice" id="1" data-ev_id="1" data-ev_amount="2000">add to invoice</a>
								 </td>
							  </tr>
							</tbody>
					</table>
					</div>
					<div class="row" style="margin-top:10px !important; margin-bottom:0px !important;">
						<div class="col l3 s3 m3 fieldset">
							  <div class="input-field">
								<label class="full-bg-label" style='margin:8px 0 !important;'>SAC NO.<span class="required_field">* </span></label>
								<input style='height: 2rem !important;' name="exp_vou_sac" id="exp_vou_sac" value='' class="full-bg adjust-width border-right" type="text">
							  </div>
						</div>
							<div class="col l3 s3 m3 fieldset">
							  <div class="input-field gstfield">
									  <label class="full-bg-label gact" style='margin:8px 0 !important;'>GOOD & SERVICE TAX (%)<span class="required_field">* </span></label>
									  <input style='height: 2rem !important;' name="exp_vou_gst" id="exp_vou_gst" value='' class="full-bg adjust-width" type="text">
									</div>
							</div>
							<div class="col l3 s3 m3 fieldset" style="border-left:1px solid #eef2fe;">
							  <div class="input-field">
									  <label class="full-bg-label" style='margin:8px 0 !important;'>CESS TAX (%)</label>
									  <input style='height: 2rem !important;' name="exp_vou_cess" id="exp_vou_cess" value='' class="full-bg adjust-width border-right" type="text">
									</div>
							</div>
							<!--<div class="col l3 s3 m3 fieldset" style="width:132px !important;">
							  <div class="input-field">
									  <label class="full-bg-label" style='margin:8px 0 !important;'>OTHER TAX NAME</label>
									  <input style='height: 2rem !important;' name="exp_vou_other" id="exp_vou_other" value='' class="full-bg adjust-width border-right" type="text">
									</div>
							</div>-->
							<div class="col l3 s3 m3 fieldset" style="width:179px !important;">
							  <div class="input-field">
								<label class="full-bg-label" style='margin:8px 0 !important;'>OTHER TAX (%)</label>
								<input style='height: 2rem !important;' name="exp_vou_other" id="exp_vou_other" value='' class="full-bg adjust-width border-right" type="text">
							  </div>
							</div>
						</div>
						<div class="row" style="margin:0px 0px 0px 10px !important;">
							<div class="col l2 s2 m2 fieldset" style="padding-top:0px !important;">
							   <!--<div class="input-field">-->
							   <input type="checkbox" id="exp_vouc" class="exp_vou_chkbox" value="1" name="exp_vouc">
							   <label style="margin: 15px 0 0 0 !important;" class="checkboxx2 checkboxx-in " for="exp_vouc"><span class="check-boxs">Set default</span></label>
							   <!--</div>-->
							</div>
						</div>
				</form>
			</div>
		</div>
	</div>


	<div id="add_bad_debts" class="modal modal-md modal-display" style="margin-top:-10px !important;">
	<style>
	#add_bad_debts .usd_val{
		background-color:#fff !important;
		height:35px !important;
		line-height:35px !important;
		border-radius:5px !important;
		padding-left:10px!important;
	}

	#add_bad_debts .usd_val[type=text]:not(.browser-default):disabled{
		border-bottom:none !important;
	}
	#add_bad_debts .inr_val[type=text]:not(.browser-default):disabled{
		border-bottom:none !important;
	}

	#add_bad_debts .inr_val{
		background-color:#fff!important;
		height:35px!important;
		line-height:35px!important;
		border-radius:5px!important;
		padding-left:10px!important;
	}

	#add_bad_debts .ex-i{
		position:unset !important;
		margin:-32px -5px 0 0;
	}
	</style>
   <div class="modalheader" style="padding-bottom:0px;">
      <h4>Bad Debts</h4>
      <a class="modal-close close-pop"><img src="<?php echo base_url();?>asset/images/popupdelete1.png" alt="delete"></a>
   </div>
   <div class="modalbody">
		<div class="baddebts_details" style="margin-top:10px;">
			<form action="" class="" id="baddebts_details_form" name="bad_debts" method="post">
				<?php $csrf = array(
		'name' => $this->security->get_csrf_token_name(),
		'hash' => $this->security->get_csrf_hash()
	);
	?>
	<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
				<div class="row ">
                    <div class="input-field col s12 m12 l6 padd-n" style="border-right:1px solid #696969 !important;">
                        <label for="srec_code" class="full-bg-label">Receipt No.<span class="required_field"> *</span></label>
                        <input id="srec_code" name="srec_code" class="readonly-bg-grey full-bg adjust-width first border-right" type="text"  value="0" readonly="readonly" style="margin-bottom:0px !important;">
                    </div>
                    <div class="input-field col s12 m12 l6 padd-n">
						<label for="srec_date" class="full-bg-label">Receipt Date<span class="required_field"> *</span></label>
                        <input id="srec_date" name="srec_date" class="readonly-bg-grey full-bg adjust-width first border-right" type="text" value="0" readonly="readonly" style="margin-bottom:0px !important;">
                    </div>
					<div class="col l6 s12 m12 input-field padd-n" style="border-right:1px solid #EEF2FE !important;">
						<label class="full-bg-label" for="bd_client_name">CLIENT NAME<span class="required_field"> *</span></label>
						<input id="bd_client_name" name="bd_client_name" class="full-bg adjust-width border-top-none" type="text" value="0" readonly="readonly" style="margin-bottom:0px !important;">
						<input type="hidden" id="bd_client_id" name="bd_client_id">
					</div>
					<div class="col l6 s12 m12 input-field padd-n">
						<label class="full-bg-label" for="bd_inv_name">INVOICE NO.<span class="required_field"> *</span></label>
						<input id="bd_inv_name" name="bd_inv_name" value="0" class="full-bg adjust-width border-top-none" type="text" readonly="readonly" style="margin-bottom:0px !important;">
						<input type="hidden"id="bd_inv_id" name="bd_inv_id">
					</div>
					<div class="col s12 m12 l12">
                        <div class="col s12 m12 l4 input-set border-field">
                                <div class="input-field">
                                    <label style="margin-left:-25px !important;" for="srecl_inv_amt0" class="full-bg-label">Invoice Amount</label>
                                    <span id="int_currency"></span><input id="srecl_inv_amt0" name="srecl_inv_amt[]" class="full-bg adjust-width inv_amount" type="text" value="0" readonly="readonly" style="margin-bottom:0px !important; margin-left:-20px !important; width:125px !important;">
                                </div>
                        </div>
                        <div class="col s12 m12 l4 input-set border-field">
                            <div class="input-field" style="padding-right:10px !important;">
                                <label style="margin-left:-15px !important;" for="srecl_conv_amt0" class="full-bg-label">Pending Amount(INR) </label>
                                <span id="ind_currency"></span><input id="srecl_conv_amt0" name="srecl_conv_amt[]" class="full-bg adjust-width conv_amount" type="text" value="0" readonly="readonly" style="margin-bottom:0px !important; margin-left:-12px !important; width:115px !important;">
                            </div>
                        </div>
                        <div class="col s12 m12 l4 input-set border-field" style="border-right:none !important;">
                            <div class="input-field">
                                <label style="margin-left:-15px !important;" for="srecl_inv_date0" class="full-bg-label">Invoice Date</label>
                                <input id="srecl_inv_date0" name="srecl_inv_date[]" class="full-bg adjust-width border-top-none" type="text" readonly="readonly" value="0" style="margin-bottom:0px !important; margin-left:-12px !important; width:115px !important;">
                            </div>
                        </div>
						<div class="input-field col s12 m12 l6 padd-n">
                            <label for="srec_baddebts_amt" class="full-bg-label" style="margin-left:-11px !important;">BAD DEBTS AMOUNT<span class="required_field"> *</span></label>
                            <input id="srec_baddebts_amt" name="srec_baddebts_amt" class="full-bg adjust-width numeric_number border-right" type="text"  value="" autocomplete="off" style="margin-left:-11px !important;">
                            <label id="bd_cust_error" for="bd_cust_error"></label>
                        </div>
                        <div class="col l6 s12 m12 input-field padd-n" style="margin-left:-11px !important;">
							<label class="full-bg-label" for="srec_status">RECEIPT STATUS<span class="required_field"> *</span></label>
							<input id="srec_status" name="srec_status" class="full-bg adjust-width border-top-none" type="text" readonly="readonly" value="Bad Debts">
							<input type="hidden" id="bd_status" name="bd_status" value="bad_debts">
						</div>
						<!--<div class="input-field col s12 m12 l6 padd-n">
                            <label for="srec_status" class="full-bg-label" style="margin-left:-11px !important;">RECEIPT STATUS<span class="required_field"> *</span></label>
							<input id="srec_status" name="srec_status" class="full-bg adjust-width numeric_number" type="text" readonly="readonly" value="Bad Debts" style="margin-left:-11px !important;">
                            <input type="hidden" id="bd_status" name="bd_status" value="Bad Debts">
                        </div>-->
                    </div>
					<div class="row" id="sr_currency_con">
						<div class="optional-row">
							<div id="sr_currencyconvert">
								<div class='col s12 m12 l7' style='margin:0px -45px 0 5px;'><div style='width:25%; float:right;'><input class='usd_val' type='text' id='sr_dollar_number' name='sr_dollar_number' value='1' style='margin:-15px 0 0 -25px !important;'></input></div><div style='width:12%; float:right; padding-top:3px;'><label style='font-size:12px; float:right;'></label></div><div style='width:63%; float:right; padding-top:3px;'><label style='font-size:12px; float:right;'>CURRENCY EXCHANGE RATE:<a class='sac info-ref tooltipped info-tooltipped ex-i' data-html='true' data-position='bottom' data-delay='50' data-tooltip='Mention the current exchange rate.'></a></label></div></div><div class='col s12 m12 l5' style='margin:0px -15px 0 15px;'><div style='width:75%; float:right;'><input class='inr_val' type='text' id='sr_inr_value' name='sr_inr_value' value="" style='margin:-15px 0 0 10px !important;'></input></div><div style='width:20%; float:right; padding-top:3px;'><label style='font-size:12px; float:right;'> = INR </label></div></div>
							</div>
						</div>
					</div>
					<div class="row" style="margin-top:20px !important;">
						<div class="col l12 s12 m6"></div>
						<div class="col l12 s12 m6 fieldset" style='text-align:right; margin-left:-20px;'>
							<input value="CANCEL" id="bad_debts_close" type="button" class="btn btn-default modal-close" data-dismiss="modal" style="font-size:12px;">
							<input type="submit" value="SAVE" id="bad_debts_save" type="button" class="btn btn-default" data-dismiss="modal" style="font-size:12px; background-color:#7864e9 !important;">
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

<!-- Receiving Invoice View -->
<div id="receive_inv" class="modal modal-md modal-display">
	<style>
		.modal .modal-content {
			padding: 10px !important;
		}

		.modal-header {
			padding-top: 10px !important;
		}

		.model-cancel{
			margin-right:0px !important;
		}
	</style>
   <img class="geen" src="<?php echo base_url();?>asset/images/green.png" alt="green">
   <div class="modal-content">
      <div class="modal-header">
         <h4>Receive Invoice</h4>
         <a class="modal-close close-pop"><img src="<?php echo base_url();?>asset/images/popupdelete.png" alt="delete"></a>
      </div>
   </div>
   <div class="modal-body">
      <p>HERE IS A INVOICE...!</p>
   </div>
   <div class="modal-content">
      <div class="modal-footer">
         <div class="row">
            <div class="col l4 s12 m12"></div>
            <div class="col l12 s12 m12 cancel-deactiv text-center">
               <a class=" modal-close btn-flat theme-flat-btn theme-btn theme-btn-large model-cancel" type="button">CANCEL</a>
               <button class="btn-flat theme-primary-btn theme-btn theme-btn-large  modal-trigger dea modal-close del_exp_vou">VIEW</button>
			   <button class="btn-flat theme-primary-btn theme-btn theme-btn-large  modal-trigger dea modal-close del_exp_vou">SAVE</button>
            </div>
         </div>
      </div>
   </div>
</div>

<div id="add-card" class="modal modal-md" style="max-height: 100%;">
  <style>
	.card-drop .select-dropdown {
		height:15vh !important;
		overflow-y: none !important;
	}

	#add-card input.select-dropdown {
		padding: 0 10px;
		margin: -30px 0 0 0 !important;
	}
  </style>
  <div class="modalheader">
    <h4>Add Card Details</h4>
    <a class="modal-close close-pop"><img src="<?php echo base_url();?>asset/images/popupdelete1.png" alt="delete"></a>
  </div>

  <div class="modalbody">
    <form class="addgstin" id="add_new_cards" name="add_new_cards" method="post">
		<?php $csrf = array(
		'name' => $this->security->get_csrf_token_name(),
		'hash' => $this->security->get_csrf_hash()
	);
	?>
	<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
      <div class="row gstrow">
        <div class="col l12 s10 m10 fieldset">
          <div class="row">
            <div class="col l6 s12 m6 fieldset" style="height:75px; border:0.5px solid #e6f7ff !important;">
				<div class="input-field input-set">
					<label class="full-bg-label select-label" for="card">SELECT CARD<span class="required_field">*</span></label>
                    <select id="card" name="card[]" class="card-drop country-dropdown check-label" >
						<option value=""></option>
						<option value="Debit Card">DEBIT CARD</option>
						<option value="Credit Card">CREDIT CARD</option>
					</select>
				</div>
			</div>
            <div class="col l6 s12 m6 fieldset">
              <div class="input-field">
                <label class="full-bg-label">ISSUER BANK</label>
                <input id="issue_bank" name="iss_bank[]" class="full-bg adjust-width" type="text">
              </div>
            </div>
          </div>
        </div>
        <div class="col l12 s10 m10 fieldset">
          <div class="row">
            <div class="col l6 s12 m6 fieldset" style="height:75px; border:0.5px solid #e6f7ff !important;">
				<div class="input-field input-set">
					<label class="full-bg-label select-label" for="card_1">SELECT CARD<span class="required_field">*</span></label>
                    <select id="card_1" name="card[]" class="card-drop country-dropdown check-label" >
						<option value=""></option>
						<option value="Debit Card">DEBIT CARD</option>
						<option value="Credit Card">CREDIT CARD</option>
					</select>
				</div>
			</div>
            <div class="col l6 s12 m6 fieldset">
              <div class="input-field">
                <label class="full-bg-label">ISSUER BANK</label>
                <input id="iss_bank_1" name="iss_bank[]" class="full-bg adjust-width" type="text">
              </div>
            </div>
          </div>
        </div>
        <div class="col l12 s10 m10 fieldset">
          <div class="row">
            <div class="col l6 s12 m6 fieldset" style="height:75px; border:0.5px solid #e6f7ff !important;">
				<div class="input-field input-set">
					<label class="full-bg-label select-label" for="card_2">SELECT CARD<span class="required_field">*</span></label>
                    <select id="card_2" name="card[]" class="card-drop country-dropdown check-label" >
						<option value=""></option>
						<option value="Debit Card">DEBIT CARD</option>
						<option value="Credit Card">CREDIT CARD</option>
					</select>
				</div>
			</div>
            <div class="col l6 s12 m6 fieldset">
              <div class="input-field">
                <label class="full-bg-label">ISSUER BANK</label>
                <input id="iss_bank_2" name="iss_bank[]" class="full-bg adjust-width" type="text">
              </div>
            </div>
          </div>
        </div>
        <div class="col l12 s10 m10 fieldset">
          <div class="row">
            <div class="col l6 s12 m6 fieldset" style="height:75px; border:0.5px solid #e6f7ff !important;">
				<div class="input-field input-set">
					<label class="full-bg-label select-label" for="card_3">SELECT CARD<span class="required_field">*</span></label>
                    <select id="card_3" name="card[]" class="card-drop country-dropdown check-label" >
						<option value=""></option>
						<option value="Debit Card">DEBIT CARD</option>
						<option value="Credit Card">CREDIT CARD</option>
					</select>
				</div>
			</div>
            <div class="col l6 s12 m6 fieldset">
              <div class="input-field">
                <label class="full-bg-label">ISSUER BANK</label>
                <input id="iss_bank_3" name="iss_bank[]" class="full-bg adjust-width" type="text">
              </div>
            </div>
          </div>
        </div>
        <div class="col l12 s10 m10 fieldset">
          <div class="row">
            <div class="col l6 s12 m6 fieldset" style="height:75px; border:0.5px solid #e6f7ff !important;">
				<div class="input-field input-set">
					<label class="full-bg-label select-label" for="card_4">SELECT CARD<span class="required_field">*</span></label>
                    <select id="card_4" name="card[]" class="card-drop country-dropdown check-label" >
						<option value=""></option>
						<option value="Debit Card">DEBIT CARD</option>
						<option value="Credit Card">CREDIT CARD</option>
					</select>
				</div>
			</div>
            <div class="col l6 s12 m6 fieldset">
              <div class="input-field">
                <label class="full-bg-label">ISSUER BANK</label>
                <input id="iss_bank_4" name="iss_bank[]" class="full-bg adjust-width" type="text">
              </div>
            </div>
          </div>
        </div>

      </div>
      <div id="rep-element">
      </div>
      <div class="row">
        <div class="col l6 m6 s12 fieldset">
        </div>
        <div class="col l6 m6 s12 fieldset buttonset">
          <div class="right">
            <button type="submit" class="btn-flat theme-primary-btn theme-btn theme-btn-large right">Save</button>
            <button type="button" class="modal-close btn-flat theme-flat-btn theme-btn theme-btn-large modal-close">CANCEL</button>
          </div>
        </div>
      </div>
    </form>
  </div>
</div>

<div id="edit-card" class="modal modal-md" style="max-height: 100%;">
  <style>
	.card-drop .select-dropdown {
		height:15vh !important;
		overflow-y: none !important;
	}

	#edit-card .country-dropdown span.caret {
		margin: 10px 15px 0 0 !important;
	}

	#edit-card input.select-dropdown {
		padding: 0 10px;
		margin: -30px 0 0 0 !important;
	}

	#edit-card .input-field label.select-label {
		padding: 0px 0 12px 10px !important;
	}
  </style>
  <div class="modalheader">
    <h4>Edit Card Details</h4>
    <a class="modal-close close-pop"><img src="<?php echo base_url();?>asset/images/popupdelete1.png" alt="delete"></a>
  </div>

  <div class="modalbody">
    <form class="addgstin" id="edit_new_cards_ext" name="edit_new_cards_ext" method="post">
		<?php $csrf = array(
		'name' => $this->security->get_csrf_token_name(),
		'hash' => $this->security->get_csrf_hash()
	);
	?>
	<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
      <div class="row gstrow">
        <div class="col l12 s10 m10 fieldset">
          <div class="row">
            <div class="col l6 s12 m6 fieldset" style="height:75px; border:0.5px solid #e6f7ff !important;">
				<div class="input-field input-set label-active">
				<input id="ccard_id" name="ccard_id" class="full-bg adjust-width" type="hidden">
				<label class="card-first full-bg-label select-label" for="card_3">SELECT CARD<span class="required_field">*</span></label>
                <select id="card" name="card" class="card-drop country-dropdown check-label" >
					<option value=""></option>
					<option value="Debit Card">DEBIT CARD</option>
					<option value="Credit Card">CREDIT CARD</option>
				</select>
				</div>
			</div>
            <div class="col l6 s12 m6 fieldset">
				<div class="input-field label-active">
					<label class="full-bg-label">ISSUER BANK</label>
					<input id="iss_bank" name="iss_bank" class="full-bg adjust-width" type="text">
				</div>
            </div>
          </div>
        </div>

		<div class="col s12 m12 l12 fieldset">
			<div class="col l6 s12 m6 input-set border-bottom" >
				<div class="input-field ed-bank" style="height:68px !important;">
					<label for="status" class="full-bg-label select-label">Status</label>
                    <select class="card-drop country-dropdown check-label" name="status" id="status">
						<option value="">Status</option>
						<option value="Active">Active</option>
						<option value="Inactive">Inactive</option>
                    </select>
                </div>
            </div>
            <div class="col l6 s12 m6 fieldset border-bottom border-right-none">
				<div class="input-field">

				</div>
            </div>
		</div>

      <div class="row">
        <div class="col l6 m6 s12 fieldset"></div>
        <div class="col l6 m6 s12 fieldset buttonset">
          <div class="right">
            <button type="submit" class="btn-flat theme-primary-btn theme-btn theme-btn-large right">Save</button>
            <button type="button" class="modal-close btn-flat theme-flat-btn theme-btn theme-btn-large modal-close">CANCEL</button>
          </div>
        </div>
      </div>
    </form>
  </div>
</div>
</div>

<div id="edit-card-new" class="modal modal-md" style="max-height: 100%;">
  <style>
   .card-drop .select-dropdown {
      height:15vh !important;
      overflow-y: none !important;
   }
   
   #edit-card .country-dropdown span.caret {
      margin: 10px 15px 0 0 !important;
   }
   
   #edit-card input.select-dropdown {
      padding: 0 10px;
      margin: -30px 0 0 0 !important;
   }
   
   #edit-card .input-field label.select-label {
      padding: 0px 0 12px 10px !important;
   }
  </style>
  <div class="modalheader">
    <h4>Add Card Details</h4>
    <a class="modal-close close-pop"><img src="<?php echo base_url();?>asset/images/popupdelete1.png" alt="delete"></a>
  </div>

  <div class="modalbody">
    <form class="addgstin" id="edit_new_cards" name="edit_new_cards" method="post">
		<?php $csrf = array(
		'name' => $this->security->get_csrf_token_name(),
		'hash' => $this->security->get_csrf_hash()
	);
	?>
	<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
      <div class="row gstrow">
        
        <div class="col l12 s10 m10 fieldset">
          <div class="row">
            <div class="col l6 s12 m6 fieldset" style="height:75px; border:0.5px solid #e6f7ff !important;">
            <div class="input-field input-set label-active">
               <input id="ccard_id" name="ccard_id" class="full-bg adjust-width" type="hidden">
               <label class="card-first full-bg-label select-label" for="card_3">SELECT CARD<span class="required_field">*</span></label>
                 <select id="card" name="card" class="card-drop country-dropdown check-label" >
               <option value=""></option>
               <option value="Debit Card">DEBIT CARD</option>
               <option value="Credit Card">CREDIT CARD</option>
            </select>
         </div>
         </div>
            <div class="col l6 s12 m6 fieldset">
              <div class="input-field label-active">
                <label class="full-bg-label">ISSUER BANK</label>
                <input id="iss_bank" name="iss_bank" class="full-bg adjust-width" type="text">
              </div>
            </div>
          </div>
        </div>


           <div class="col s12 m12 l12 fieldset">
                <div class="col l6 s12 m6 input-set border-bottom" >
                <div class="input-field ed-bank" style="height:68px !important;">
                    <label for="status" class="full-bg-label select-label">Status</label>
                    <select class="card-drop country-dropdown check-label" name="status" id="status">
                  <option value="">Status</option>
                  <option value="Active">Active</option>
                  <option value="Inactive">Inactive</option>
                    </select>
                </div>
            </div>
            <div class="col l6 s12 m6 fieldset border-bottom border-right-none">
            <div class="input-field">
                 
            </div>
            </div>
            </div>
       
      <div class="row">
        <div class="col l6 m6 s12 fieldset">
        </div>
        <div class="col l6 m6 s12 fieldset buttonset">
          <div class="right">
            <button type="submit" class="btn-flat theme-primary-btn theme-btn theme-btn-large right">Save</button>
            <button type="button" class="modal-close btn-flat theme-flat-btn theme-btn theme-btn-large modal-close">CANCEL</button>
          </div>
        </div>
      </div>
    </form>
  </div>
</div>
</div>
<div id="thanks_you" class="modal modal-md modal-display">
	<style>
		#thanks_you .modal{
			background-color:#fff !important;
		}
		#thanks_you .modal .modal-content {
			padding: 10px !important;
		}

		#thanks_you .modal-header {
			padding-top: 10px !important;
		}

		#thanks_you .model-cancel{
			margin-right:0px !important;
		}

		#thanks_you .modal-body{
			border:none !important;
			text-align:center;
			padding:1% 0 8% 0;
		}

		#thanks_you .modal-body img{
			margin:0.5% 0 0 25%;
		}
	</style>
   <img class="geen" src="<?php echo base_url();?>asset/images/green.png" alt="green">
   <div class="modal-content">
      <div class="modal-header">
         <a class="modal-close close-pop"><img src="<?php echo base_url();?>asset/images/popupdelete.png" alt="delete"></a>
      </div>
   </div>
   <div class="modal-body">
		<h5><strong>Your Payment is Successful..!</strong></h5></br>
		<img width="200" height="150" class="geen" src="<?php echo base_url();?>public/images/success.PNG" alt="green">
   </div>
</div>

<!-- --------------CALCULATOR MODAL CODE----------- -->
<div id="calculator" class="modal modal-sm modal-display">
   <div class="modalheader">
      <h4>Calculator</h4>
      <a class="modal-close close-pop"><img src="<?php echo base_url();?>asset/images/popupdelete1.png" alt="delete"></a>
   </div>
   <div class="modalbody" style="text-align: center;">
       <div class="calrow" id="result">
         <form name="calc" style="padding: 0 10px;">
			<?php $csrf = array(
		'name' => $this->security->get_csrf_token_name(),
		'hash' => $this->security->get_csrf_hash()
	);
	?>
	<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
           <input type="text" class="screen text-center result" name="result" readonly>
         </form>
       </div>
       <div class="calrow">
         <button id="allClear" type="button" class="btn btn-danger cal-btn" onclick="clearScreen()">AC</button>
         <button id="clear" type="button" class="btn btn-warning cal-btn" onclick="clearScreen()">CE</button>
         <button id="%" type="button" class="btn btn-info cal-btn" onclick="myFunction(this.id)">%</button>
         <button id="/" type="button" class="btn btn-info cal-btn" onclick="myFunction(this.id)">÷</button>
       </div>
       <div class="calrow">
         <button id="7" type="button" class="btn btn-info cal-btn" onclick="myFunction(this.id)">7</button>
         <button id="8" type="button" class="btn btn-info cal-btn" onclick="myFunction(this.id)">8</button>
         <button id="9" type="button" class="btn btn-info cal-btn" onclick="myFunction(this.id)">9</button>
         <button id="*" type="button" class="btn btn-info cal-btn" onclick="myFunction(this.id)">x</button>
       </div>
       <div class="calrow">
         <button id="4" type="button" class="btn btn-info cal-btn" onclick="myFunction(this.id)">4</button>
         <button id="5" type="button" class="btn btn-info cal-btn" onclick="myFunction(this.id)">5</button>
         <button id="6" type="button" class="btn btn-info cal-btn" onclick="myFunction(this.id)">6</button>
         <button id="-" type="button" class="btn btn-info cal-btn" onclick="myFunction(this.id)">-</button>
       </div>
       <div class="calrow">
         <button id="1" type="button" class="btn btn-info cal-btn" onclick="myFunction(this.id)">1</button>
         <button id="2" type="button" class="btn btn-info cal-btn" onclick="myFunction(this.id)">2</button>
         <button id="3" type="button" class="btn btn-info cal-btn" onclick="myFunction(this.id)">3</button>
         <button id="+" type="button" class="btn btn-info cal-btn" onclick="myFunction(this.id)">+</button>
       </div>
       <div class="calrow">
         <button id="0" type="button" class="btn btn-info cal-btn" onclick="myFunction(this.id)">0</button>
         <button id="." type="button" class="btn btn-info cal-btn" onclick="myFunction(this.id)">.</button>
         <button id="equals" type="button" class="btn btn-success cal-btn" onclick="calculate()">=</button>
         <button id="blank" type="button" class="btn btn-info cal-btn">&nbsp;</button>
       </div>
   </div>
</div>
<!-- --------------END CALCULATOR MODAL CODE----------- -->

<!-- ================================================
   Scripts

   ================================================ -->
<!-- jQuery Library -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-migrate/1.4.1/jquery-migrate.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>asset/materialize/js/materialize.min.js"></script>
<!-- <script src="//oss.maxcdn.com/jquery.trip.js/3.3.3/trip.min.js"></script> -->
<!--prism-->

<script type="text/javascript" src="<?php echo base_url();?>asset/js/prism.js"></script>
<!--scrollbar-->

<script type="text/javascript" src="<?php echo base_url();?>asset/js/perfect-scrollbar.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>asset/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>public/js/index.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>asset/js/data-tables.js"></script>
<script src="<?=public_path()?>js/additional-methods.min.js" type="text/javascript"></script>
<script src="<?=public_path()?>js/page/jquery.validate.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>asset/js/jquery.pwdMeter.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>asset/js/bootstrap-datepicker.js" type="text/javascript"></script>
<script src="<?=public_path()?>js/select2.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>public/js/page/community.js" type="text/javascript"></script>

 <?php if ($this->router->fetch_class() == "admin_dashboard") { ?>
<script src="<?=public_path()?>js/page/feedback.js" type="text/javascript"></script>
<?php } ?>

<?php if ($this->router->fetch_class() == "profile") { ?>
<script src="<?=public_path()?>js/page/profile.js" type="text/javascript"></script>
<script src="<?=public_path()?>js/page/personal_profile.js" type="text/javascript"></script>
<?php } ?>

<?php if ($this->router->fetch_class() == "settings") { ?>
<script src="<?=public_path()?>js/page/alerts.js" type="text/javascript"></script>
<?php } ?>

<?php if ($this->router->fetch_class() == "module_tracker") { ?>
<script src="<?=public_path()?>js/page/module_tracker.js" type="text/javascript"></script>
<?php } ?>

<?php if ($this->router->fetch_class() == "refer_earn") { ?>
	<script type="text/javascript" src="<?php echo base_url();?>public/js/page/refer-earn.js"></script>
<?php }?>

<script src="<?=public_path()?>js/page/common.js" type="text/javascript"></script>


<!-- chartjs -->
<!-- <script type="text/javascript" src="../../vendors/chartjs/chart.min.js"></script> -->
<!--plugins.js - Some Specific JS codes for Plugin Settings-->
<?php
   /* if(isset($scripts)){
    if(count($scripts)>0){
      foreach($scripts as $script){
        echo '<script src="'.$script.'" type="text/javascript"></script>'."\n";
      }
    }
   }  */
   ?>

	<?php if($this->router->fetch_method()=="add_company_expense" || $this->router->fetch_method()=="edit_company_expense"){?>
		<script>
		if ($('.menu-icon').hasClass('open')) {
			$('.menu-icon').removeClass('open');
			$('.menu-icon').addClass('close');
			$('#left-sidebar-nav').removeClass('nav-lock');
			$('.header-search-wrapper').removeClass('sideNav-lock');
			$('#header').addClass('header-collapsed');
			$('#logo_img').show();
			$('#eazy_logo').hide();
			$('#left-sidebar-nav').toggleClass('nav-expanded nav-collapsed');
			$('#main').toggleClass('main-full');
		}
		</script>
	<?php } ?>

	<?php if($this->router->fetch_method()=="welcome" || $this->router->fetch_method()=="no_connection"){?>
			<script>
			//$(".sh-show").show();
			if ($('.menu-icon').hasClass('open')) {
				$('.menu-icon').removeClass('open');
				$('.menu-icon').addClass('close');
				$('#left-sidebar-nav').removeClass('nav-lock');
				$('.header-search-wrapper').removeClass('sideNav-lock');
				$('#header').addClass('header-collapsed');
				$('#logo_img').show();
				$('#eazy_logo').hide();
				$('#left-sidebar-nav').toggleClass('nav-expanded nav-collapsed');
				$('#main').toggleClass('main-full');
			}
			</script>
	<?php } ?>



   <?php if($this->router->fetch_method()=="add_sales_invoice" || $this->router->fetch_method()=="edit_sales_invoice" || $this->router->fetch_method()=="add_proforma_invoice" || $this->router->fetch_method()=="edit_proforma_invoice" || $this->router->fetch_method()=="add_estimate_invoice" || $this->router->fetch_method()=="edit_estimate_invoice" || $this->router->fetch_method()=="tax_summary_gst"){?>
		<script>
			if ($('.menu-icon').hasClass('open')) {
				$('.menu-icon').removeClass('open');
				$('.menu-icon').addClass('close');
				$('#left-sidebar-nav').removeClass('nav-lock');
				$('.header-search-wrapper').removeClass('sideNav-lock');
				$('#header').addClass('header-collapsed');
				$('#logo_img').show();
				$('#eazy_logo').hide();
				$('#left-sidebar-nav').toggleClass('nav-expanded nav-collapsed');
				$('#main').toggleClass('main-full');
			}
		</script>
	<?php } ?>

	<?php if($this->router->fetch_method()=="add_purchase_order" || $this->router->fetch_method()=="edit_purchase_order"){?>
		<script>
			if ($('.menu-icon').hasClass('open')) {
				$('.menu-icon').removeClass('open');
				$('.menu-icon').addClass('close');
				$('#left-sidebar-nav').removeClass('nav-lock');
				$('.header-search-wrapper').removeClass('sideNav-lock');
				$('#header').addClass('header-collapsed');
				$('#logo_img').show();
				$('#eazy_logo').hide();
				$('#left-sidebar-nav').toggleClass('nav-expanded nav-collapsed');
				$('#main').toggleClass('main-full');
			}
		</script>
	<?php } ?>

	<!--<?php //if($this->router->fetch_method()=="add_company_expense" || $this->router->fetch_method()=="edit_company_expense"){?>
		<script>
			if ($('.menu-icon').hasClass('open')) {
				$('.menu-icon').removeClass('open');
				$('.menu-icon').addClass('close');
				$('#left-sidebar-nav').removeClass('nav-lock');
				$('.header-search-wrapper').removeClass('sideNav-lock');
				$('#header').addClass('header-collapsed');
				$('#logo_img').show();
				$('#eazy_logo').hide();
				$('#left-sidebar-nav').toggleClass('nav-expanded nav-collapsed');
				$('#main').toggleClass('main-full');
			}
		</script>
	<?php //} ?>-->

	<?php if($this->router->fetch_method()=="add_debit_note" || $this->router->fetch_method()=="edit_debit_note"){?>
		<script>
			if ($('.menu-icon').hasClass('open')) {
				$('.menu-icon').removeClass('open');
				$('.menu-icon').addClass('close');
				$('#left-sidebar-nav').removeClass('nav-lock');
				$('.header-search-wrapper').removeClass('sideNav-lock');
				$('#header').addClass('header-collapsed');
				$('#logo_img').show();
				$('#eazy_logo').hide();
				$('#left-sidebar-nav').toggleClass('nav-expanded nav-collapsed');
				$('#main').toggleClass('main-full');
			}
		</script>
	<?php } ?>

	<?php if($this->router->fetch_method()=="expense_payments"){?>
		<script>
			if ($('.menu-icon').hasClass('open')) {
				$('.menu-icon').removeClass('open');
				$('.menu-icon').addClass('close');
				$('#left-sidebar-nav').removeClass('nav-lock');
				$('.header-search-wrapper').removeClass('sideNav-lock');
				$('#header').addClass('header-collapsed');
				$('#logo_img').show();
				$('#eazy_logo').hide();
				$('#left-sidebar-nav').toggleClass('nav-expanded nav-collapsed');
				$('#main').toggleClass('main-full');
			}
		</script>
	<?php } ?>

	<?php if($this->router->fetch_method()=="add_asset_purchase" || $this->router->fetch_method()=="asset_purchase_edit" || $this->router->fetch_method()=="add_asset_sale" || $this->router->fetch_method()=="sales_edit_data"){?>
		<script>
			if ($('.menu-icon').hasClass('open')) {
				$('.menu-icon').removeClass('open');
				$('.menu-icon').addClass('close');
				$('#left-sidebar-nav').removeClass('nav-lock');
				$('.header-search-wrapper').removeClass('sideNav-lock');
				$('#header').addClass('header-collapsed');
				$('#logo_img').show();
				$('#eazy_logo').hide();
				$('#left-sidebar-nav').toggleClass('nav-expanded nav-collapsed');
				$('#main').toggleClass('main-full');
			}
		</script>
	<?php } ?>

<script type="text/javascript" src="<?php echo base_url();?>asset/js/plugins.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>asset/js/dev-custom.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>asset/js/techybirds.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>asset/js/my_purchases.js"></script>
<script type="text/javascript">
   function base_path(){
      return '<?=base_path()?>';
   }
</script>

<?php if ($this->router->fetch_class() == "business_analytics") { ?>
	<?php if ($this->router->fetch_method() == "credit_history_list" || $this->router->fetch_method() == "credit_history_invoice") { ?>
		<script src="<?=public_path()?>js/page/ba-credit-history.js" type="text/javascript"></script>
<?php } } ?>


<?php if ($this->router->fetch_class() == "business_analytics") { ?>
	<?php if ($this->router->fetch_method() == "credit_history_list") { ?>
		<script type="text/javascript">
		$(document).ready(function() {
		  window.onscroll = function() {myFunction()};

			var header = document.getElementById("fixedHeader");
			//var sticky = header.offsetTop;

			function myFunction() {
				if (window.pageYOffset > 950) {
				header.classList.add("sticky");
			  } else {
				header.classList.remove("sticky");
			  }
			}
		});
		</script>
	<?php } ?>

	<?php if ($this->router->fetch_method() == "credit_history_invoice") { ?>
		<script type="text/javascript">
		$(document).ready(function() {
		  window.onscroll = function() {myFunction()};

			var header = document.getElementById("fixedHeader");
			//var sticky = header.offsetTop;

			function myFunction() {
				if (window.pageYOffset > 550) {
				header.classList.add("sticky");
			  } else {
				header.classList.remove("sticky");
			  }
			}
		});
		</script>
	<?php } ?>
<?php } ?>

<?php if ($this->router->fetch_class() == "employee") { ?>
	<?php if ($this->router->fetch_method() == "manage_employee_master" || $this->router->fetch_method() == "manage_employee_payment") { ?>
		<script type="text/javascript">
		$(document).ready(function() {
			console.log("Good");
			window.onscroll = function() {myFunction()};
			var header = document.getElementById("fixedHeader");
			function myFunction() {
				if (window.pageYOffset > 220) {
				header.classList.add("sticky");
			  } else {
				header.classList.remove("sticky");
			  }
			}
		  });
		</script>
	<?php } ?>
<?php } ?>

<?php if ($this->router->fetch_class() == "my_payroll") { ?>
	<?php if ($this->router->fetch_method() == "manage_appraisal" || $this->router->fetch_method() == "manage_salary_expense") { ?>
		<script type="text/javascript">
		$(document).ready(function() {
			console.log("Good");
			window.onscroll = function() {myFunction()};
			var header = document.getElementById("fixedHeader");
			function myFunction() {
				if (window.pageYOffset > 250) {
				header.classList.add("sticky");
			  } else {
				header.classList.remove("sticky");
			  }
			}
		  });
		</script>
	<?php } ?>
<?php } ?>

<?php if ($this->router->fetch_class() == "refer_earn") { ?>
	<script type="text/javascript">
		$(document).ready(function() {
			window.onscroll = function() {myFunction()};
			var header = document.getElementById("fixedHeader");
			//var sticky = header.offsetTop;

			function myFunction() {
				if (window.pageYOffset > 360) {
				header.classList.add("sticky");
			  } else {
				header.classList.remove("sticky");
			  }
			}
		});
	</script>
<?php } ?>

<?php if ($this->router->fetch_class() == "account") { ?>
	<?php if ($this->router->fetch_method() == "manage_account" || $this->router->fetch_method() == "manage_jv") { ?>
		<script type="text/javascript">
		$(document).ready(function() {
			console.log("Good");
			window.onscroll = function() {myFunction()};
			var header = document.getElementById("fixedHeader");
			function myFunction() {
				if (window.pageYOffset > 250) {
				header.classList.add("sticky");
			  } else {
				header.classList.remove("sticky");
			  }
			}
		  });
		</script>
	<?php } ?>
<?php } ?>

<?php if ($this->router->fetch_class() == "cash_bank" || $this->router->fetch_class() == "Cash_bank") { ?>
	<?php if ($this->router->fetch_method() == "petty_cash" || $this->router->fetch_method() == "list_cash_statement" || $this->router->fetch_method() == "list_bank_statement"|| $this->router->fetch_method() == "manage_petty_cash") { ?>
		<script type="text/javascript">
		$(document).ready(function() {
			console.log("Good");
			window.onscroll = function() {myFunction()};
			var header = document.getElementById("fixedHeader");
			function myFunction() {
				if (window.pageYOffset > 250) {
				header.classList.add("sticky");
			  } else {
				header.classList.remove("sticky");
			  }
			}
		  });
		</script>
	<?php } ?>
<?php } ?>

<?php if ($this->router->fetch_class() == "Assets" || $this->router->fetch_class() == "assets") { ?>
	<?php if ($this->router->fetch_method() == "asset_register" || $this->router->fetch_method() == "asset_sales" || $this->router->fetch_method() == "asset_tracker" || $this->router->fetch_method() == "depreciation_schedule" || $this->router->fetch_method() == "asset_purchase") { ?>
		<script type="text/javascript">
		$(document).ready(function() {
			console.log("Good");
			window.onscroll = function() {myFunction()};
			var header = document.getElementById("fixedHeader");
			function myFunction() {
				if (window.pageYOffset > 250) {
				header.classList.add("sticky");
			  } else {
				header.classList.remove("sticky");
			  }
			}
		  });
		</script>
	<?php } ?>
<?php } ?>

<?php if ($this->router->fetch_class() == "expense") { ?>
	<?php if ($this->router->fetch_method() == "manage_expense_master" || $this->router->fetch_method() == "purchase_order" || $this->router->fetch_method() == "manage_company_expense" || $this->router->fetch_method() == "debit_note") { ?>
		<script type="text/javascript">
		$(document).ready(function() {
			window.onscroll = function() {myFunction()};
			var header = document.getElementById("fixedHeader");
			function myFunction() {
				if (window.pageYOffset > 250) {
				header.classList.add("sticky");
			  } else {
				header.classList.remove("sticky");
			  }
			}
		  });
		</script>
	<?php } ?>
<?php } ?>

<?php if ($this->router->fetch_class() == "sales") { ?>
	<?php if ($this->router->fetch_method() == "manage_clients" || $this->router->fetch_method() == "manage-clients" || $this->router->fetch_method() == "billing_documents" || $this->router->fetch_method() == "credit_notes" || $this->router->fetch_method() == "manage_expense_voucher" || $this->router->fetch_method() == "manage_appraisal") { ?>
		<script type="text/javascript">
		$(document).ready(function() {
			window.onscroll = function() {myFunction()};

			var header = document.getElementById("fixedHeader");
			//var sticky = header.offsetTop;
			function myFunction() {
			if (window.pageYOffset > 260) {
				header.classList.add("sticky");
			}else {
				header.classList.remove("sticky");
			}
			}
		  });
		</script>
	<?php } ?>
<?php } ?>

<?php if ($this->router->fetch_class() == "receipts_payments") { ?>
	<script type="text/javascript">
		$(document).ready(function() {
			window.onscroll = function() {myFunction()};
			var header = document.getElementById("fixedHeader");
			//var sticky = header.offsetTop;

			function myFunction() {
				if (window.pageYOffset > 160) {
				header.classList.add("sticky");
			  } else {
				header.classList.remove("sticky");
			  }
			}
		});
	</script>
<?php } ?>

<?php if ($this->router->fetch_class() == "receipts_payments") { ?>

	<?php if ($this->router->fetch_method() == "tax_payment_gst_list" || $this->router->fetch_method() == "tds_payment_list" || $this->router->fetch_method() == "equa_levy_list" || $this->router->fetch_method() == "asset_sales_receipt" || $this->router->fetch_method() == "expense_payments" || $this->router->fetch_method() == "asset_payments" || $this->router->fetch_method() == "manage_tds_payment" || $this->router->fetch_method() == "manage_tds_employee" || $this->router->fetch_method() == "manage_prof_tax") { ?>
		<script type="text/javascript">
		$(document).ready(function() {
					  window.onscroll = function() {myFunction()};

			var header = document.getElementById("fixedHeader");
			//var sticky = header.offsetTop;

			function myFunction() {
				if (window.pageYOffset > 250) {
				header.classList.add("sticky");
			  } else {
				header.classList.remove("sticky");
			  }
			}
		});
		</script>
	<?php } ?>
<?php } ?>

<?php if ($this->router->fetch_class() == "tax_summary") { ?>
	<?php if ($this->router->fetch_method() == "clients_tds" || $this->router->fetch_method() == "tax_summary_gst" || $this->router->fetch_method() == "tax_summary_credit_gst" || $this->router->fetch_method() == "equalisation_levy" || $this->router->fetch_method() == "prof_tax" || $this->router->fetch_method() == "tax_sum_tds_vendors" || $this->router->fetch_method() == "tax_summary_tds_employee" || $this->router->fetch_method() == "pur_gst_unreg") { ?>
		<script type="text/javascript">
		$(document).ready(function() {
			console.log("GOOD");
		  window.onscroll = function() {myFunction()};

			var header = document.getElementById("fixedHeader");
			//var sticky = header.offsetTop;

			function myFunction() {
				if (window.pageYOffset >= 250) {
				header.classList.add("sticky");
			  } else {
				header.classList.remove("sticky");
			  }
			}
		});
		</script>
	<?php } ?>
<?php } ?>

<!-- $this->router->fetch_class() == "subscription" || -->
<?php if ($this->router->fetch_class() == "services" || $this->router->fetch_class() == "statement_account" || $this->router->fetch_class() == "profile" || $this->router->fetch_class() == "dashboard" || $this->router->fetch_class() == "module_tracker" || $this->router->fetch_class() == "settings" ) { ?>

<?php if ($this->router->fetch_class() == "statement_account" ) { ?>
	<script type="text/javascript">
		$(document).ready(function() {
		  window.onscroll = function() {myFunction()};

			var header = document.getElementById("fixedHeader");
			//var sticky = header.offsetTop;

			function myFunction() {
				if (window.pageYOffset > 300) {
				header.classList.add("sticky");
			  } else {
				header.classList.remove("sticky");
			  }
			}
		});
	</script>
<?php } }?>

<?php if ($this->router->fetch_class() == "services" || $this->router->fetch_class() == "profile" || $this->router->fetch_class() == "dashboard" || $this->router->fetch_class() == "module_tracker" || $this->router->fetch_class() == "settings") { ?>

	<script type="text/javascript">
		$(document).ready(function() {
		  window.onscroll = function() {myFunction()};

			var header = document.getElementById("fixedHeader");
			//var sticky = header.offsetTop;

			function myFunction() {
				if (window.pageYOffset > 160) {
				header.classList.add("sticky");
			  } else {
				header.classList.remove("sticky");
			  }
			}
		});
	</script>
<?php } ?>
<?php if ( $this->router->fetch_class() == "expense" ) { ?>
<?php if ($this->router->fetch_method() == "purchase_order" || $this->router->fetch_method() == "manage_vendors" || $this->router->fetch_method() == "manage-vendors" || $this->router->fetch_method() == "debit_note" || $this->router->fetch_method() == "manage_expense_master") { ?>
	<script type="text/javascript">
		$(document).ready(function() {
		  window.onscroll = function() {myFunction()};

			var header = document.getElementById("fixedHeader");
			var sticky = header.offsetTop;

			function myFunction() {
				if (window.pageYOffset > 220) {
				header.classList.add("sticky");
			  } else {
				header.classList.remove("sticky");
			  }
			}
		});
	</script>
<?php } } ?>

<?php if ( $this->router->fetch_class() == "my_payroll" ) { ?>
<?php if ($this->router->fetch_method() == "manage_appraisal" || $this->router->fetch_method() == "manage_salary_expense") { ?>
	<script type="text/javascript">
		$(document).ready(function() {
		  window.onscroll = function() {myFunction()};

			var header = document.getElementById("fixedHeader");
			//var sticky = header.offsetTop;

			function myFunction() {
				if (window.pageYOffset > 160) {
				header.classList.add("sticky");
			  } else {
				header.classList.remove("sticky");
			  }
			}
		});
	</script>
<?php } } ?>

<?php if ( $this->router->fetch_class() == "My_payroll" ) { ?>
<?php if ($this->router->fetch_method() == "create_appraisal") { ?>
	<script type="text/javascript">
		$(document).ready(function() {
			$(".emp").addClass("active");
			$(".emp-col").css("display","block","!important");
		});
	</script>
<?php } } ?>

<?php if ( $this->router->fetch_class() == "my_payroll" ) { ?>
<?php if ($this->router->fetch_method() == "manage_appraisal" || $this->router->fetch_method() == "create_appraisal" || $this->router->fetch_method() == "edit_appraisal" || $this->router->fetch_method() == "view_appraisal") { ?>
	<script type="text/javascript">
		$(document).ready(function() {
			$(".my-exp").removeClass("active");
			$(".col-exp").css("display","none","!important");
		});
	</script>
<?php } } ?>

<?php if ( $this->router->fetch_class() == "my_payroll" || $this->router->fetch_class() == "My_payroll" ) { ?>
<?php if ($this->router->fetch_method() == "manage_salary_expense" || $this->router->fetch_method() == "add_salary_expense" || $this->router->fetch_method() == "edit_salary_expense" || $this->router->fetch_method() == "view_salary_expense") { ?>
	<script type="text/javascript">
		$(document).ready(function() {
			$(".emp").removeClass("active");
			$(".emp-col").css("display","none","!important");
		});
	</script>
<?php } } ?>

<?php if ( $this->router->fetch_class() == "employee" ) { ?>
<?php if ($this->router->fetch_method() == "manage_employee_payment" || $this->router->fetch_method() == "add_employee_pay" || $this->router->fetch_method() == "edit_employee_pay" || $this->router->fetch_method()=="view_payment_details") { ?>
	<script type="text/javascript">
		$(document).ready(function() {
			$(".emp").removeClass("active");
			$(".emp-col").css("display","none","!important");
		});
	</script>
<?php } } ?>

<?php if ( $this->router->fetch_class() == "employee" ) { ?>
<?php if ($this->router->fetch_method() == "manage_employee_master" || $this->router->fetch_method() == "edit_employee_master" || $this->router->fetch_method() == "add_employee_master" || $this->router->fetch_method()=="view_employee_details") { ?>
	<script type="text/javascript">
		$(document).ready(function() {
			$(".rp").removeClass("active");
			$(".rp-col").css("display","none","!important");
		});
	</script>
<?php } } ?>

<?php if ( $this->router->fetch_class() == "Cash_bank" ) { ?>
<?php if ($this->router->fetch_method() == "list_cash_statement" || $this->router->fetch_method() == "list_bank_statement") { ?>
	<script type="text/javascript">
		$(document).ready(function() {
			$(".my-exp").removeClass("active");
			$(".col-exp").css("display","none","!important");
		});
	</script>
<?php } } ?>

<?php if ( $this->router->fetch_class() == "Cash_bank" ) { ?>
<?php if ($this->router->fetch_method() == "manage_petty_cash") { ?>
	<script type="text/javascript">
		$(document).ready(function() {
			$(".cash").removeClass("active");
			$(".cash-col").css("display","none","!important");
		});
	</script>
<?php } } ?>

<?php if ($this->router->fetch_method() == "add_service" || $this->router->fetch_method()=="edit_service" ||  $this->router->fetch_method()=="view_services") { ?>
	<script type="text/javascript">
		$(document).ready(function() {
			$(".ba").removeClass("active");
			$(".ba-body").css("display","none","!important");
		});
	</script>
<?php } ?>

<?php if ( $this->router->fetch_class() == "services" ) { ?>
	<script type="text/javascript">
		$(document).ready(function() {
			$(".ba").removeClass("active");
			$(".ba-body").css("display","none","!important");
		});
	</script>
<?php } ?>

<script type="text/javascript">
	$('.hide-show-1').show();
	$('.hide-show-1 span').addClass('show')
				  
	$('.hide-show-1 span').click(function(){
		if( $(this).hasClass('show') ) {
			//$(this).text('Hide');
			$(this).html('<img width="29" height="28" src="<?php echo base_url(); ?>public/images/off-monkey.png" alt="small-eazy"></img>');
			$('#old_password').attr('type','text');
			$(this).removeClass('show');
		} else {
			$(this).html('<img width="29" height="28" src="<?php echo base_url(); ?>public/images/no-monkey.png" alt="small-eazy"></img>');
			$('#old_password').attr('type','password');
			$(this).addClass('show');
		}
	});
	
	$('.hide-show-2').show();
	$('.hide-show-2 span').addClass('show')
				  
	$('.hide-show-2 span').click(function(){
		if( $(this).hasClass('show') ) {
			//$(this).text('Hide');
			$(this).html('<img width="29" height="28" src="<?php echo base_url(); ?>public/images/off-monkey.png" alt="small-eazy"></img>');
			$('#new_password').attr('type','text');
			$(this).removeClass('show');
		} else {
			$(this).html('<img width="29" height="28" src="<?php echo base_url(); ?>public/images/no-monkey.png" alt="small-eazy"></img>');
			$('#new_password').attr('type','password');
			$(this).addClass('show');
		}
	});
	
	$('.hide-show-3').show();
	$('.hide-show-3 span').addClass('show')
				  
	$('.hide-show-3 span').click(function(){
		if( $(this).hasClass('show') ) {
			//$(this).text('Hide');
			$(this).html('<img width="29" height="28" src="<?php echo base_url(); ?>public/images/off-monkey.png" alt="small-eazy"></img>');
			$('#confirm_password').attr('type','text');
			$(this).removeClass('show');
		} else {
			$(this).html('<img width="29" height="28" src="<?php echo base_url(); ?>public/images/no-monkey.png" alt="small-eazy"></img>');
			$('#confirm_password').attr('type','password');
			$(this).addClass('show');
		}
	});
</script>

<script type="text/javascript">
   $(document).ready(function () {
	 $('.bold').on('click', function(){
		//$('.active').removeClass('active');
		//$(this).addClass('active');
		//$(".auto-shut").css("display","none");
	});
	 /*$(".buzz_in").click(function(){
		$(".sales-body").css("display","none");
		$(".my_sales").removeClass('active');
	 });
     $(".my_sales").click(function(){
		$(".ba-body").css("display","none");
		$(".buzz_in").removeClass('active');
	 });*/

	/*
    var company_id = $('#header_company_profiles').val();
    $.ajax({
      url:base_url+'Dashboard/get_branch_gst_info',
      type:"POST",
      data:{'company_id':company_id},
      success:function(res){
        $("#branch_gst_list").html(res);
        $("#branch_gst_list").parents('.input-field').addClass('label-active');
         $('#branch_gst_list').material_select();

        $("#create_invoice #company_gst").val($('#branch_gst_list').val());
        $("#company_gst").val($('#branch_gst_list').val());
      },
    }); */

   $('#datepickerd').datepicker({
            todayBtn: "linked",
            format: 'dd-mm-yyyy'
        });
        $('.input-daterange').datepicker({
            todayBtn: "linked",
            format: 'dd-mm-yyyy',
        });
   $('.input-date').datepicker({
            todayBtn: "linked",
            format: 'yyyy-mm-dd'
        });

   $(".password_strength").pwdMeter();

   /* $.ajax({

   url:base_url+'Dashboard/get_login_check',

   type:"POST",

   data:{},

   success:function(res){

    if(res==true)

    {
       $('#select_business').modal({
        dismissible: false, // Modal can be dismissed by clicking outside of the modal
        opacity: 1, // Opacity of modal background
        inDuration: 700, // Transition in duration
        outDuration: 200, // Transition out duration
        startingTop: '4%', // Starting top style attribute
        endingTop: '10%', // Ending top style attribute
        });

          $('#select_business').modal('open');

    }

   },

   }); */

   $('.update_businesstype').on('click',function(){

      console.log(this);

      if (!$("input[name='radio_type_user']:checked").val()) {

         $('#businesstype_errormsg').modal('open');

      }

      else {

        var business_type =  $('input[name=radio_type_user]:checked', '#select_businesstype').val();

        $.ajax({

            url:base_url+'Dashboard/update_user_business',

            type:"POST",

            data:{'csrf_test_name':csrf_hash,"business_type":business_type,},

            success:function(res){

                if(res!=false){

                  window.location.href=base_url+'Dashboard';

                }

                else

                {

                  window.location.href=base_url+'Dashboard';

                }

              },
        });

      }

   });
   // Materialize.toast('I am a toast!', 4000,'rounded')



    <?php $msg1="";
      if($this->session->flashdata('success')!=''){

      $msg1=$this->session->flashdata('success');

      $msg1 = str_replace("\n", '', $msg1);

      ?>

   Materialize.toast('<?php echo trim($msg1); ?>', 2000,'green rounded');

   <?php }  ?>

   <?php $msg ="";
      if($this->session->flashdata('error')!=''){

      $msg=$this->session->flashdata('error');

      $msg = str_replace("\n", '', $msg);

      ?>

   Materialize.toast('<?php echo trim($msg); ?>', 2000,'red rounded');

   <?php } ?>

   $.validator.addMethod("urlcheck", function(value, element) {
        return  this.optional(element) || /^(http|https)?:\/\/[a-zA-Z0-9-\.]+\.[a-z]{2,4}/.test(value);
   }, "Please Enter a valid URL.");

   $.validator.addMethod("gstregex", function(value, element) {
		return this.optional(element) || /^[a-zA-Z0-9]+$/.test(value);
   }, "GSTIN must contain only letters and numbers.");
   $.validator.addMethod("pannoregex", function(value, element) {
    return this.optional(element) || /^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$/.test(value);
   }, "Enter PAN no in a valid format");
    $.validator.addMethod("email", function(value, element) {
    return this.optional(element) || /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i.test(value);
   }, "Invalid Email Id");
   /*$("#header_company_profiles").off().on("change",function(){
    var company_id = $(this).val();
    $('#company_profile').val(company_id);
    $.ajax({
      url:base_url+'Dashboard/get_branch_gst_info',
      type:"POST",
      data:{'company_id':company_id},
      success:function(res){
        $("#branch_gst_list").html(res);
        $("#branch_gst_list").parents('.input-field').addClass('label-active');
        $('#branch_gst_list').material_select();
      },
      complete:function(res){
        $('#company_gst').val($('#branch_gst_list').val());
      },
    });
   });*/

   $("#branch_gst_list").off().on("change",function(){
    $('#company_gst').val($(this).val());
    $("#create_invoice #branch_name").val($(this).val());
   });

      $(".select2-selection").on("click",function(){
      $('.select2-results__options li:first-child').css("display","none");

      $(".select2-search__field").on("keypress",function(){
           $(".select2-results__option:has('li'):contains('*')").remove();
        });
      });


       $(".select2-selection").on("keydown",function(objEvent){
         if (objEvent.keyCode == 13) {
           // console.log('fas gya');
           $(".select2-results__option:has('li'):contains('*')").remove();
        }
        });



   $(document).on('focus', '.select2', function (e) {
   if($(this).hasClass('select2-container--focus')) {
      $(this).parent('.input-field').parent('div').addClass('bodrpinkright');
      $(".select2-results__option:has('li'):contains('*')").remove();
   }


   });
   $(document).on('click', '.select2', function (e) {

     $(this).parent('.input-field').parent('div').removeClass('bodrpinkright');
      $(".select2-results__option:has('li'):contains('*')").remove();
   if($(this).hasClass('select2-container--open')) {

      $(this).parent('.input-field').parent('div').addClass('bodrpinkright');
   }


   });
   $(document).on('focusout', '.select2', function (e) {
   if(!$(this).hasClass('select2-container--open')) {
      $(this).parent('.input-field').parent('div').removeClass('bodrpinkright');
      $(".select2-results__option:has('li'):contains('*')").remove();
   }

   });



   /*

   $("#countrysec").on("click",function(){
   $('#countrysec').addClass('bodrpinkright');
   $('#statesec').removeClass('bodrpinkright');
   $('#citysec').removeClass('bodrpinkright');
   $('#shipstatesec').removeClass('bodrpinkright');
   $('#shipcitysec').removeClass('bodrpinkright');
   $('#shipcountrysec').removeClass('bodrpinkright');
   });
   $("#statesec").on("click",function(){
   $('#statesec').addClass('bodrpinkright');
   $('#countrysec').removeClass('bodrpinkright');
   $('#citysec').removeClass('bodrpinkright');
   $('#shipstatesec').removeClass('bodrpinkright');
   $('#shipcitysec').removeClass('bodrpinkright');
   $('#shipcountrysec').removeClass('bodrpinkright');
   });
   $("#citysec").on("click",function(){
   $('#citysec').addClass('bodrpinkright');
   $('#countrysec').removeClass('bodrpinkright');
   $('#statesec').removeClass('bodrpinkright');
   $('#shipstatesec').removeClass('bodrpinkright');
   $('#shipcitysec').removeClass('bodrpinkright');
   $('#shipcountrysec').removeClass('bodrpinkright');
   });

   $("#shipcountrysec").on("click",function(){
   $('#shipcountrysec').addClass('bodrpinkright');
   $('#shipstatesec').removeClass('bodrpinkright');
   $('#shipcitysec').removeClass('bodrpinkright');
   $('#countrysec').removeClass('bodrpinkright');
   $('#statesec').removeClass('bodrpinkright');
   $('#citysec').removeClass('bodrpinkright');
   });
   $("#shipstatesec").on("click",function(){
   $('#shipstatesec').addClass('bodrpinkright');
   $('#shipcountrysec').removeClass('bodrpinkright');
   $('#shipcitysec').removeClass('bodrpinkright');
   $('#countrysec').removeClass('bodrpinkright');
   $('#statesec').removeClass('bodrpinkright');
   $('#citysec').removeClass('bodrpinkright');
   });
   $("#shipcitysec").on("click",function(){
   $('#shipcitysec').addClass('bodrpinkright');
   $('#shipstatesec').removeClass('bodrpinkright');
   $('#shipcountrysec').removeClass('bodrpinkright');
   $('#countrysec').removeClass('bodrpinkright');
   $('#statesec').removeClass('bodrpinkright');
   $('#citysec').removeClass('bodrpinkright');
   });

   */




















   function makeredcolor(id) {
   $('#'+id).each(function () {
      $('#'+id).html($('#'+id).html().replace(/(\*)/g, '<span style="color: red;">$1</span>'));
    });

   }

   function removeredstar(id) {
   $('#'+id).each(function () {
      $('#'+id).html($('#'+id).html().replace(/(\*)/g, '<span style="color: transparent;">$1</span>'));
    });

   }

	$("#country").on("change",function(){
		var country_id = $(this).val();
		if(country_id!=''){
			$("#select2-country-container").css("font-size","14px");
			$("#select2-country-container").css("color","#000");
			$("#select2-country-container").css("font-weight","500");
			//$('#countrysec').removeClass('bodrpinkright');
			if(csrf_hash===""){
				csrf_hash=csrf_hash;
			}
			$.ajax({
				url:base_url+'profile/get_states',
				type:"POST",
				data:{'csrf_test_name':csrf_hash,'country_id':country_id},
				success:function(res){
					var myArray = res.split("@");
					//$(".state_label").hasClass('select-dropdown').find('ul').html(res);
					$("#state").html(myArray[0]);
					$("#state").parents('.input-field').addClass('label-active');
					$('#state').material_select();
					//  $('#statesec').addClass('bodrpinkright');
					if(myArray[1]){
						csrf_hash=myArray[1];
					}
				},
			});
			
			if(csrf_hash===""){
				csrf_hash=csrf_hash;
			}
			$.ajax({
				url:base_url+'profile/bank_swift_check',
				type:"POST",
				data:{'csrf_test_name':csrf_hash,'country_id':country_id},
				success:function(res){
					var myArray = res.split("@");
					$("#currency").html(myArray[0]);
					$("#currency").parents('.input-field').addClass('label-active');
					$('#currency').material_select();
					if(myArray[1]){
						csrf_hash=myArray[1];
					}
				},
			});
	
	if(csrf_hash===""){
		csrf_hash=csrf_hash;
	}
    $.ajax({
      url:base_url+'profile/timezone',
      type:"POST",
      data:{'csrf_test_name':csrf_hash,'country_id':country_id},
      success:function(res){
		var myArray = res.split("@");	
        $("#timezone").html(myArray[0]);
        $("#timezone").parents('.input-field').addClass('label-active');
        $('#timezone').material_select();
		if(myArray[1]){
			csrf_hash=myArray[1];
		}
      },
    });

	if(csrf_hash===""){
		csrf_hash=csrf_hash;
	}
    $.ajax({
      url:base_url+'profile/date_format',
      type:"POST",
      data:{'csrf_test_name':csrf_hash,'country_id':country_id},
      success:function(res){
		var myArray = res.split("@");		
        $("#date_format").html(myArray[0]);
        $("#date_format").parents('.input-field').addClass('label-active');
        $('#date_format').material_select();
		if(myArray[1]){
			csrf_hash=myArray[1];
		}
      },
    });


        setTimeout(function(){
          makeredcolor("select2-state-container");
        }, 400);

     }else{
      // Materialize.toast('Please select the country ', 7000,'red rounded');
     }

   });


    $("#country1").on("change",function(){

    var country_id = $(this).val();
   if(country_id!=''){
    $("#select2-country-container").css("font-size","14px");
    $("#select2-country-container").css("color","#000");
    $("#select2-country-container").css("font-weight","500");
        //$('#countrysec').removeClass('bodrpinkright');

    $.ajax({
      url:base_url+'profile/get_states',
      type:"POST",
      data:{'csrf_test_name':csrf_hash,'country_id':country_id},
      success:function(res){
        //$(".state_label").hasClass('select-dropdown').find('ul').html(res);
        $("#state1").html(res);
        $("#state1").parents('.input-field').addClass('label-active');
        $('#state1').material_select();
           //  $('#statesec').addClass('bodrpinkright');
      },
    });


        setTimeout(function(){
          makeredcolor("select2-state-container");
        }, 400);

     }else{
      // Materialize.toast('Please select the country ', 7000,'red rounded');
     }

   });

   $("#country2").on("change",function(){

    var country_id = $(this).val();
   if(country_id!=''){
    $("#select2-country-container").css("font-size","14px");
    $("#select2-country-container").css("color","#000");
    $("#select2-country-container").css("font-weight","500");
        //$('#countrysec').removeClass('bodrpinkright');

    $.ajax({
      url:base_url+'profile/get_states',
      type:"POST",
      data:{'csrf_test_name':csrf_hash,'country_id':country_id},
      success:function(res){
        //$(".state_label").hasClass('select-dropdown').find('ul').html(res);
        $("#state2").html(res);
        $("#state2").parents('.input-field').addClass('label-active');
        $('#state2').material_select();
           //  $('#statesec').addClass('bodrpinkright');
      },
    });


        setTimeout(function(){
          makeredcolor("select2-state-container");
        }, 400);

     }else{
      // Materialize.toast('Please select the country ', 7000,'red rounded');
     }

   });

	$("#bus_bank_country_change").on("change",function(){

    var country_id = $(this).val();

    if(country_id!=''){

       $.ajax({
         url:base_url+'profile/bank_swift_check',
         type:"POST",
         data:{'csrf_test_name':csrf_hash,'country_id':country_id},
         success:function(res){
           $("#bank_cur_check_change").html(res);
           $("#bank_cur_check_change").parents('.input-field').addClass('label-active');
           $('#bank_cur_check_change').material_select();
         },
		});
		setTimeout(function(){
			makeredcolor("select2-state-container");
		}, 400);
     }else{
      // Materialize.toast('Please select the country ', 7000,'red rounded');
     }

   });

   $("#bus_bank_country").on("change",function(){

    var country_id = $(this).val();

    // if(country_id == 101)
    // {
    //   res = '<label class="full-bg-label" for="ifsc_code">Enter IFSC Code<span class="required_field">*</span></label> <input id="ifsc_code" name="ifsc_code" class="full-bg adjust-width" type="text">';
    //   $("#ifscORswift_edit").html(res);

    // }else{
    //   res = '<label class="full-bg-label" for="swift_code">Enter Swift Code<span class="required_field">*</span></label> <input id="swift_code" name="swift_code" class="full-bg adjust-width" type="text">';
    //   $("#ifscORswift_edit").html(res);
    // }

    if(country_id!=''){

       $.ajax({
         url:base_url+'profile/bank_swift_check',
         type:"POST",
         data:{'csrf_test_name':csrf_hash,'country_id':country_id},
         success:function(res){
           //$(".state_label").hasClass('select-dropdown').find('ul').html(res);
           $("#bank_cur_check_edit").html(res);
           $("#bank_cur_check_edit").parents('.input-field').addClass('label-active');
            $('#bank_cur_check_edit').material_select();
              //  $('#statesec').addClass('bodrpinkright');
         },
       });


        setTimeout(function(){
          makeredcolor("select2-state-container");
        }, 400);

     }else{
      // Materialize.toast('Please select the country ', 7000,'red rounded');
     }

   });

   $("#bank_country").on("change",function(){

    var country_id = $(this).val();

    if(country_id == 101)
    {
      res = '<label class="full-bg-label" for="ifsc_code">Enter IFSC Code<span class="required_field">*</span></label> <input id="ifsc_code" name="ifsc_code" class="full-bg adjust-width" type="text">';
      $("#ifscORswift").html(res);

    }else{
      res = '<label class="full-bg-label" for="swift_code">Enter Swift Code<span class="required_field">*</span></label> <input id="swift_code" name="swift_code" class="full-bg adjust-width" type="text">';
      $("#ifscORswift").html(res);
    }

    if(country_id!=''){
		if(csrf_hash===""){
			csrf_hash=csrf_hash;
		}
		$.ajax({
         url:base_url+'profile/bank_swift_check',
         type:"POST",
         data:{'csrf_test_name':csrf_hash,'country_id':country_id},
         success:function(res){
			 var myArray = res.split("@");
			if(myArray[1]){
				csrf_hash=myArray[1];
			}
           //$(".state_label").hasClass('select-dropdown').find('ul').html(res);
           $("#bank_cur_check").html(myArray[0]);
           $("#bank_cur_check").parents('.input-field').addClass('label-active');
            $('#bank_cur_check').material_select();
              //  $('#statesec').addClass('bodrpinkright');
         },
       });


        setTimeout(function(){
          makeredcolor("select2-state-container");
        }, 400);

     }else{
      // Materialize.toast('Please select the country ', 7000,'red rounded');
     }

   });

   $("#state").on("change",function(){
    var state_id = $(this).val();
        if(state_id!=''){
    $("#select2-state-container").css("font-size","14px");
    $("#select2-state-container").css("color","#000");
    $("#select2-state-container").css("font-weight","500");
        //$('#statesec').removeClass('bodrpinkright');
	if(csrf_hash===""){
		csrf_hash=csrf_hash;
	}
    $.ajax({
      url:base_url+'profile/get_cities',
      type:"POST",
      data:{'csrf_test_name':csrf_hash,'state_id':state_id},
      success:function(res){
		  var myArray = res.split("@");
			$("#city").html(myArray[0]);
                 $("#city").parents('.input-field').addClass('label-active');
             $('#city').material_select();
                // $('#citysec').addClass('bodrpinkright');
				if(myArray[1]){
			csrf_hash=myArray[1];
		}
      },
    });
              setTimeout(function(){
                makeredcolor("select2-city-container");
              }, 400);
        }else{
          //Materialize.toast('Please select the state', 7000,'red rounded');
        }

   });
      $("#city").on("change",function(){
        var city_id = $(this).val();
        if(city_id!=''){
          $("#select2-city-container").css("font-size","14px");
          $("#select2-city-container").css("color","#000");
          $("#select2-city-container").css("font-weight","500");
          //$('#citysec').removeClass('bodrpinkright');
        }
      });

       $("#state1").on("change",function(){
    var state_id = $(this).val();
        if(state_id!=''){
    $("#select2-state-container").css("font-size","14px");
    $("#select2-state-container").css("color","#000");
    $("#select2-state-container").css("font-weight","500");
        //$('#statesec').removeClass('bodrpinkright');
    $.ajax({
      url:base_url+'profile/get_cities',
      type:"POST",
      data:{'csrf_test_name':csrf_hash,'state_id':state_id},
      success:function(res){
                 $("#city1").html(res);
                 $("#city1").parents('.input-field').addClass('label-active');
             $('#city1').material_select();
                // $('#citysec').addClass('bodrpinkright');
      },
    });
              setTimeout(function(){
                makeredcolor("select2-city-container");
              }, 400);
        }else{
          //Materialize.toast('Please select the state', 7000,'red rounded');
        }

   });
       $("#state2").on("change",function(){
    var state_id = $(this).val();
        if(state_id!=''){
    $("#select2-state-container").css("font-size","14px");
    $("#select2-state-container").css("color","#000");
    $("#select2-state-container").css("font-weight","500");
        //$('#statesec').removeClass('bodrpinkright');
    $.ajax({
      url:base_url+'profile/get_cities',
      type:"POST",
      data:{'csrf_test_name':csrf_hash,'state_id':state_id},
      success:function(res){
                 $("#city2").html(res);
                 $("#city2").parents('.input-field').addClass('label-active');
             $('#city2').material_select();
                // $('#citysec').addClass('bodrpinkright');
      },
    });
              setTimeout(function(){
                makeredcolor("select2-city-container");
              }, 400);
        }else{
          //Materialize.toast('Please select the state', 7000,'red rounded');
        }

   });
      $("#city1").on("change",function(){
        var city_id = $(this).val();
        if(city_id!=''){
          $("#select2-city-container").css("font-size","14px");
          $("#select2-city-container").css("color","#000");
          $("#select2-city-container").css("font-weight","500");
          //$('#citysec').removeClass('bodrpinkright');
        }
      });
   $("#city2").on("change",function(){
        var city_id = $(this).val();
        if(city_id!=''){
          $("#select2-city-container").css("font-size","14px");
          $("#select2-city-container").css("color","#000");
          $("#select2-city-container").css("font-weight","500");
          //$('#citysec').removeClass('bodrpinkright');
        }
      });

    $("#shipping_country").on("change",function(){

    var country_id = $(this).val();

    if(country_id!=''){
          $("#select2-shipping_country-container").css("font-size","14px");
          $("#select2-shipping_country-container").css("color","#000");
          $("#select2-shipping_country-container").css("font-weight","500");

           $('#shipcountrysec').removeClass('bodrpinkright');

      $.ajax({
      url:base_url+'profile/get_states',
      type:"POST",
      data:{'csrf_test_name':csrf_hash,'country_id':country_id},
      success:function(res){
        $("#shipping_state").html(res);
        $("#shipping_state").parents('.input-field').addClass('label-active');
        $('#shipping_state').material_select();
             //$('#shipstatesec').addClass('bodrpinkright');
      },
    });

              setTimeout(function(){

                makeredcolor("select2-shipping_state-container");
               // removeredstar("select2-shipping_state-container");

              }, 400);

        }else{
         // Materialize.toast('Please select the country ', 7000,'red rounded');
        }
   });

   $("#shipping_state").on("change",function(){
    var state_id = $(this).val();

        $('#select2-shipping_state-container').html($('#select2-shipping_state-container').html().replace(/(\*)/g, '<span style="color: transparent;">$1</span>'));
        if(state_id!=''){
          $("#select2-shipping_state-container").css("font-size","14px");
          $("#select2-shipping_state-container").css("color","#000");
          $("#select2-shipping_state-container").css("font-weight","500");



   $('#shipstatesec').removeClass('bodrpinkright');
          $.ajax({
          url:base_url+'profile/get_cities',
          type:"POST",
          data:{'csrf_test_name':csrf_hash,'state_id':state_id},
          success:function(res){
            $("#shipping_city").html(res);
            $("#shipping_city").parents('.input-field').addClass('label-active');
            $('#shipping_city').material_select();
        //      $('#shipcitysec').addClass('bodrpinkright');
          },
        });
              setTimeout(function(){
                makeredcolor("select2-shipping_city-container");
              //  removeredstar("select2-shipping_city-container");
              }, 500);

          }else{
         // Materialize.toast('Please select the state', 7000,'red rounded');
        }
   });
      $("#shipping_city").on("change",function(){
        var city_id = $(this).val();
          $('#select2-shipping_city-container').html($('#select2-shipping_city-container').html().replace(/(\*)/g, '<span style="color: transparent;">$1</span>'));
        if(city_id!=''){
          $("#select2-shipping_city-container").css("font-size","14px");
          $("#select2-shipping_city-container").css("color","#000");
          $("#select2-shipping_city-container").css("font-weight","500");
           $('#shipcitysec').removeClass('bodrpinkright');
        }
      });

   });



    /*~~~~~~~~~~~ Country & STATE LIST WITH ALL OPTION~~~~~~~~~~~~~~~*/

    $("#allcountry").on("change",function(){

    var country_id = $(this).val();
   if(country_id!=''){
    $("#select2-country-container").css("font-size","14px");
    $("#select2-country-container").css("color","#000");
        $("#select2-country-container").css("font-weight","500");
        //$('#countrysec').removeClass('bodrpinkright');

    $.ajax({
      url:base_url+'profile/get_all_states',
      type:"POST",
      data:{'csrf_test_name':csrf_hash,'country_id':country_id},
      success:function(res){
        //$(".state_label").hasClass('select-dropdown').find('ul').html(res);
        $("#allstate").html(res);
        $("#allstate").parents('.input-field').addClass('label-active');
            $('#allstate').material_select();
           //  $('#statesec').addClass('bodrpinkright');
      },
    });


        setTimeout(function(){
        //  makeredcolor("select2-state-container");
        }, 400);

     }else{
      // Materialize.toast('Please select the country', 7000,'red rounded');
     }

   });



   $("#allstate").on("change",function(){
    var state_id = $(this).val();
        if(state_id!=''){
    $("#select2-state-container").css("font-size","14px");
    $("#select2-state-container").css("color","#000");
    $("#select2-state-container").css("font-weight","500");
        //$('#statesec').removeClass('bodrpinkright');
    $.ajax({
      url:base_url+'profile/get_all_cities',
      type:"POST",
      data:{'csrf_test_name':csrf_hash,'state_id':state_id},
      success:function(res){
                 $("#allcity").html(res);
                 $("#allcity").parents('.input-field').addClass('label-active');
                 $('#allcity').material_select();
                // $('#citysec').addClass('bodrpinkright');
      },
    });
              setTimeout(function(){
               // makeredcolor("select2-city-container");
              }, 400);
        }else{
          //Materialize.toast('Please select the state', 7000,'red rounded');
        }

   });
      $("#allcity").on("change",function(){
        var city_id = $(this).val();
        if(city_id!=''){
          $("#select2-city-container").css("font-size","14px");
          $("#select2-city-container").css("color","#000");
          $("#select2-city-container").css("font-weight","500");
          //$('#citysec').removeClass('bodrpinkright');
        }
      });


    /*~~~~~~~~~~~ END Country & STATE LIST WITH ALL OPTION~~~~~~~~~~~~~~~*/



/*~~~~~~~~~~~~~~~~~CALCULATOR~~~~~~~~~~~~~~*/
function myFunction(id) {
  document.calc.result.value+=id;
}
function clearScreen() {
  document.calc.result.value="";
}
function calculate() {
try {
    var input = eval(document.calc.result.value);
    document.calc.result.value=input;
  } catch(err){
      document.calc.result.value="Error";
    }
}
/*~~~~~~~~~~~~~~~~~END CALCULATOR~~~~~~~~~~~~~~*/



   function alphanumeric(txt)
   {
    var letters = /^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%*#?&])[A-Za-z\d$@$!%*#?&]{8,}$/;
    if(txt.value.match(letters))
    {

      $("#invalid_password_alpha").hide();

      return true;

    }

    else

    {
      document.create_user.contact_password.focus();

      $("#invalid_password_alpha").show();

      return false;

    }

   }

   function alphanumeric_password_change(txt)
   {
        console.log('ff');
    var letters = /^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%*#?&])[A-Za-z\d$@$!%*#?&]{8,}$/;
    if(txt.value.match(letters))
    {
      $("#invalid_password_alpha").hide();
      return true;
    }
    else
    {
      document.change_password_frm.new_password.focus();
      $("#invalid_password_alpha").show();
      return false;
    }
   }

   function Popup(data)
   {
    var data = jQuery(data).html()
    //alert(data);
     var mywindow = window.open('', 'PRINT', 'height=400,width=600');
    try {

   mywindow.document.write('<html><head><title></title>');
          mywindow.document.write('<link rel="stylesheet" href="<?php echo base_url(); ?>asset/css/style-form.css" type="text/css" />');
          mywindow.document.write('<link rel="stylesheet" href="<?php echo base_url(); ?>asset/css/style.css" type="text/css" />');
      mywindow.document.write('</head><body>');
      mywindow.document.write('<h1>' + document.title  + '</h1>');
      mywindow.document.write(data);
      mywindow.document.write('</body></html>');
      mywindow.document.close();
      setTimeout(function(){ mywindow.print(); }, 2000);

    } catch (e) {
    alert("Allow this site in popup blocker in you browser setting.");
    }
   }

   function showAlertMore(){
     $('.alertmore').toggleClass('active');
     if($('.alertmore').hasClass('active')){
    $('#showalertboxs i').html('keyboard_arrow_up');
     }else{
    $('#showalertboxs i').html('keyboard_arrow_down');
     }
   }
</script>
<script type="text/javascript">
   $(document).ready(function () {

     var image = $('#image_signature_info').val();
     var img_folder = $('#sign_img_folder').val();

       if(image != '' && image != undefined)
       {
       $('.uploader-placeholder-sign').css('background-image', 'url('+base_url+'public/upload/'+img_folder+'/'+image+')');
       }
       else
       {
         $('.uploader-placeholder-sign').css('background-image','url('+base_url+'asset/css/img/icons/placeholder.png)');
       }

      var image = $('#image_info').val();
      var img_folder = $('#img_folder').val();

       if(image != '' && image != undefined)
       {
       $('.uploader-placeholder').css('background-image', 'url('+base_url+'public/upload/'+img_folder+'/'+image+')');
       }
       else
       {
         $('.uploader-placeholder').css('background-image','url('+base_url+'asset/css/img/icons/placeholder.png)');
       }

			 var image_event = $('#image_info_event').val();
			 var image_id = $('#image_id_event').val();
			 if(image_event != '' && image_event != undefined)
			 {
					//$('.uploader-placeholder-event').css('background-image', 'url('+base_url+'upload/event_image/'+image_id+'/'+image_event+')');
               $('.uploader-placeholder-event').css('background-image', 'url('+base_upload+'/event_image/'+image_id+'/'+image_event+')');
					$('.uploader-placeholder-event').css('width','133px');
					$('.uploader-placeholder-event').css('height','75px');
					$('.uploader-placeholder-event').css('background-position', 'center');
					//$('.uploader-placeholder-event').css('background-repeat', 'round');
			 }
			 else
			 {
				 $('.uploader-placeholder-event').css('width','133px');
				 $('.uploader-placeholder-event').css('height','75px');
				 $('.uploader-placeholder-event').css('background-image','url('+base_url+'asset/css/img/icons/placeholder.png)');
				 $('.uploader-placeholder-event').css('background-position', 'center');
				 //$('.uploader-placeholder-event').css('background-repeat', 'round');
			 }

			 var image_deal = $('#image_info_deal').val();
			 var image_deal_id = $('#image_id_deal').val();
			 if(image_deal != '' && image_deal != undefined)
			 {
				$('.uploader-placeholder-deal').css('background-image', 'url('+base_url+'upload/offer_image/'+image_deal_id+'/'+image_deal+')');
				$('.uploader-placeholder-deal').css('width','138px');
				$('.uploader-placeholder-deal').css('height','75px');
				$('.uploader-placeholder-deal').css('background-repeat', 'round');
				//$('.uploader-placeholder-deal').css('background-position', 'center');
			 }
			 else
			 {
				 $('.uploader-placeholder-deal').css('width','138px');
				 $('.uploader-placeholder-deal').css('height','75px');
				 $('.uploader-placeholder-deal').css('background-image','url('+base_url+'asset/css/img/icons/placeholder.png)');
				 $('.uploader-placeholder-deal').css('background-repeat', 'round');
				 //$('.uploader-placeholder-deal').css('background-position', 'center');
			 }



			 var image_profile = $('#image_info_profile').val();
			 var image_profile_id = $('#img_folder_profile').val();
			 
			 if(image_profile != '' && image_profile != undefined)
			 {
					$('.uploader-placeholder_profile').css('background-image', 'url('+base_p+'/'+image_profile_id+'/'+image_profile+')');
					$('.uploader-placeholder_profile').css('width','110px');
					$('.uploader-placeholder_profile').css('height','104px');
					$('.uploader-placeholder_profile').css('background-repeat', 'round');
					$('.uploader-placeholder_profile').css('background-position', 'inherit');
			 }
			 else
			 {
				$('.uploader-placeholder_profile').css('width','110px');
				$('.uploader-placeholder_profile').css('height','104px');
				$('.uploader-placeholder_profile').css('background-image','url('+base_url+'asset/css/img/icons/placeholder.png)');
				$('.uploader-placeholder_profile').css('background-repeat', 'round');
				$('.uploader-placeholder_profile').css('background-position', 'inherit');
			 }

			  var comp_profile = $('#image_info_company').val();
			 var comp_profile_id = $('#img_folder_company').val();
			 if(comp_profile != '' && comp_profile != undefined)
			 {
					$('.uploader-placeholder-company').css('background-image', 'url('+base_p+'/'+comp_profile_id+'/'+comp_profile+')');
					$('.uploader-placeholder-company').css('width','110px');
					$('.uploader-placeholder-company').css('height','88px');
					//$('.uploader-placeholder-company').css('background-repeat', 'round');
					$('.uploader-placeholder-company').css('background-repeat', 'round');
					$('.uploader-placeholder-company').css('background-position', 'inherit');
			 }
			 else
			 {
				 $('.uploader-placeholder-company').css('width','110px');
				 $('.uploader-placeholder-company').css('height','88px');
				 $('.uploader-placeholder-company').css('background-image','url('+base_url+'asset/css/img/icons/placeholder.png)');
				 //$('.uploader-placeholder-company').css('background-repeat', 'no-repeat');
				 //$('.uploader-placeholder-company').css('background-position', 'center');
				 $('.uploader-placeholder-company').css('background-repeat', 'round');
				 $('.uploader-placeholder-company').css('background-position', 'inherit');
			 }

     /*var profile_img = $('#image_info').val();
     if(profile_img != '' && profile_img != undefined)

     {
       $('.uploader-placeholder').css('background-image', 'url('+base_url+'uploads/profile_images/'+profile_img+')');
     }
     else
     {
       $('.uploader-placeholder').css('background-image','url('+base_url+'asset/css/img/icons/placeholder.png)');
     }*/


   $(".cancel_btn").on("click",function(){
      window.location.href=base_url+'Company_profile/manage_company_profile';
   });

   $("#CheckForExistingBus").on("click",function(){

      $.ajax({
      url:base_url+'profile/check_for_Bus',
      type:"POST",
      datatype: 'json',
      data:{'csrf_test_name':csrf_hash, },
      success:function(res){


        if(res == 'exist')
        {
          $("#logout_for_new_bus_modal").show();
          return false;
        }
        else if(res == 'notexist')
        {
          window.location.href="<?php echo base_url();?>profile/add_company_profile";
        }
        else
        {
          Materialize.toast('Something went wrong. Try again', 2000,'green rounded');
        }

      },
    });

   });

   $(document).on("change",".hide-file",function(){
   var filesize = (this.files[0].size);
   var filename = (this.files[0].name);
   if(filename !=""){
		var file = this.files[0];
		var fileType = file["type"];
		var validImageTypes = ["image/jpeg", "image/jpg", "image/png"];
		if ($.inArray(fileType, validImageTypes) < 0) {
			Materialize.toast('Image format is incorrect.', 2000,'red rounded');
			$(".hide-file-profile").val('');
			return false;
		}
		/*else if(filesize_mb > 100) {
			Materialize.toast('Upload image size should not be more than 50mb.', 2000,'red rounded');
			$("#video_up").val('');
		}*/	
	}
   if(csrf_hash===""){
		csrf_hash=csrf_hash;
	}
   $.ajax({
      url:base_url+'profile/check_for_allowedsize',
      type:"POST",
      datatype: 'json',
      data:{
		  'csrf_test_name':csrf_hash,
		  "filesize":filesize,
          "filename":filename,
          "doctype":"image",
      },
      success:function(res){
        //alert(res);

        var res1 = JSON.parse(res);
		var myArray = res1.split("/");
		if(myArray[1]){
			csrf_hash=myArray[1];
		}
        if(myArray[0]=="sizeexc"){
          Materialize.toast('Document size exceeds 10MB limit', 3000,'red rounded');
                $('.hide-file').val("");
          return false;
        }
        else if(myArray[0] == 'error')
        {

          Materialize.toast('Document format is incorrect', 2000,'red rounded');
              $('.hide-file').val("");
          return false;
        }
          else if(myArray[0] == 'subscription')
        {

          Materialize.toast('You can’t use more than 1 GB space in the trial period. Do subscribe to an appropriate pack for more storage space');
              $('.hide-file').val("");
          return false;
        }
        else if(myArray[0] == 'subscription_1')
        {

          Materialize.toast('Your allotted space of 1 GB is utilised. Click on Buy Add-ons in accounts module in the profile dropdown to buy more space');
              $('.hide-file').val("");
          return false;
        } else if(myArray[0] == 'subscription_2')
        {

          Materialize.toast('Your allotted space of 5 GB is utilised. Click on Buy Add-ons in accounts module in the profile dropdown to buy more space');
              $('.hide-file').val("");
          return false;
        } else if(myArray[0] == 'subscription_3')
        {

          Materialize.toast('Your allotted space of 10 GB is utilised. Click on Buy Add-ons in accounts module in the profile dropdown to buy more space');
              $('.hide-file').val("");
          return false;
        }
        else
        {

          Materialize.toast('Document has been uploaded', 2000,'green rounded');
        }
      },
    });
	readURL(this);
	readCOURL(this);
   });
     
   $(document).on("change",".hide-file-deal",function(){
   var filesize = (this.files[0].size);
   var filename = (this.files[0].name);
   $.ajax({
      url:base_url+'profile/check_for_allowedsize',
      type:"POST",
      datatype: 'json',
      data:{
		  'csrf_test_name':csrf_hash,
		  "filesize":filesize,
          "filename":filename,
          "doctype":"image",
      },
      success:function(res){
        //alert(res);

        var res1 = JSON.parse(res);

        if(res1=="sizeexc"){
          Materialize.toast('Document size exceeds 10MB limit', 3000,'red rounded');
                $('.hide-file-deal').val("");
          return false;
        }
        else if(res1 == 'error')
        {

          Materialize.toast('Document format is incorrect', 2000,'red rounded');
              $('.hide-file-deal').val("");
          return false;
        }
          else if(res1 == 'subscription')
        {

          Materialize.toast('You can’t use more than 1 GB space in the trial period. Do subscribe to an appropriate pack for more storage space');
              $('.hide-file-deal').val("");
          return false;
        }
        else if(res1 == 'subscription_1')
        {

          Materialize.toast('Your allotted space of 1 GB is utilised. Click on Buy Add-ons in accounts module in the profile dropdown to buy more space');
              $('.hide-file-deal').val("");
          return false;
        } else if(res1 == 'subscription_2')
        {

          Materialize.toast('Your allotted space of 5 GB is utilised. Click on Buy Add-ons in accounts module in the profile dropdown to buy more space');
              $('.hide-file-deal').val("");
          return false;
        } else if(res1 == 'subscription_3')
        {

          Materialize.toast('Your allotted space of 10 GB is utilised. Click on Buy Add-ons in accounts module in the profile dropdown to buy more space');
              $('.hide-file-deal').val("");
          return false;
        }
        else
        {

          Materialize.toast('Document has been uploaded', 2000,'green rounded');
        }
      },
    });
	readDELURL(this);
   });
   
   $(document).on("change",".hide-file-event",function(){
   var filesize = (this.files[0].size);
   var filename = (this.files[0].name);
   $.ajax({
      url:base_url+'profile/check_for_allowedsize',
      type:"POST",
      datatype: 'json',
      data:{
		  'csrf_test_name':csrf_hash,
		  "filesize":filesize,
          "filename":filename,
          "doctype":"image",
      },
      success:function(res){
        //alert(res);

        var res1 = JSON.parse(res);

        if(res1=="sizeexc"){
          Materialize.toast('Document size exceeds 10MB limit', 3000,'red rounded');
                $('.hide-file-deal').val("");
          return false;
        }
        else if(res1 == 'error')
        {

          Materialize.toast('Document format is incorrect', 2000,'red rounded');
              $('.hide-file-deal').val("");
          return false;
        }
          else if(res1 == 'subscription')
        {

          Materialize.toast('You can’t use more than 1 GB space in the trial period. Do subscribe to an appropriate pack for more storage space');
              $('.hide-file-deal').val("");
          return false;
        }
        else if(res1 == 'subscription_1')
        {

          Materialize.toast('Your allotted space of 1 GB is utilised. Click on Buy Add-ons in accounts module in the profile dropdown to buy more space');
              $('.hide-file-deal').val("");
          return false;
        } else if(res1 == 'subscription_2')
        {

          Materialize.toast('Your allotted space of 5 GB is utilised. Click on Buy Add-ons in accounts module in the profile dropdown to buy more space');
              $('.hide-file-deal').val("");
          return false;
        } else if(res1 == 'subscription_3')
        {

          Materialize.toast('Your allotted space of 10 GB is utilised. Click on Buy Add-ons in accounts module in the profile dropdown to buy more space');
              $('.hide-file-deal').val("");
          return false;
        }
        else
        {

          Materialize.toast('Document has been uploaded', 2000,'green rounded');
        }
      },
    });
	readEVENTURL(this);
   });
   
   $(document).on("change",".hide-file-co",function(){
   var filesize = (this.files[0].size);
   var filename = (this.files[0].name);
   if(filename !=""){
		var file = this.files[0];
		var fileType = file["type"];
		var validImageTypes = ["image/jpeg", "image/jpg", "image/png"];
		if ($.inArray(fileType, validImageTypes) < 0) {
			Materialize.toast('Image format is incorrect.', 2000,'red rounded');
			$(".hide-file-profile").val('');
			return false;
		}
		/*else if(filesize_mb > 100) {
			Materialize.toast('Upload image size should not be more than 50mb.', 2000,'red rounded');
			$("#video_up").val('');
		}*/	
	}
   if(csrf_hash===""){
		csrf_hash=csrf_hash;
	}
   $.ajax({
      url:base_url+'profile/check_for_allowedsize',
      type:"POST",
      datatype: 'json',
      data:{
		  'csrf_test_name':csrf_hash,
		  "filesize":filesize,
          "filename":filename,
          "doctype":"image",
      },
      success:function(res){
        //alert(res);

        var res1 = JSON.parse(res);
		var myArray = res1.split("/");
		if(myArray[1]){
			csrf_hash=myArray[1];
		}
        if(myArray[0]=="sizeexc"){
          Materialize.toast('Document size exceeds 10MB limit', 3000,'red rounded');
                $('.hide-file').val("");
          return false;
        }
        else if(myArray[0] == 'error')
        {

          Materialize.toast('Document format is incorrect', 2000,'red rounded');
              $('.hide-file').val("");
          return false;
        }
          else if(myArray[0] == 'subscription')
        {

          Materialize.toast('You can’t use more than 1 GB space in the trial period. Do subscribe to an appropriate pack for more storage space');
              $('.hide-file').val("");
          return false;
        }
        else if(myArray[0] == 'subscription_1')
        {

          Materialize.toast('Your allotted space of 1 GB is utilised. Click on Buy Add-ons in accounts module in the profile dropdown to buy more space');
              $('.hide-file').val("");
          return false;
        } else if(myArray[0] == 'subscription_2')
        {

          Materialize.toast('Your allotted space of 5 GB is utilised. Click on Buy Add-ons in accounts module in the profile dropdown to buy more space');
              $('.hide-file').val("");
          return false;
        } else if(myArray[0] == 'subscription_3')
        {

          Materialize.toast('Your allotted space of 10 GB is utilised. Click on Buy Add-ons in accounts module in the profile dropdown to buy more space');
              $('.hide-file').val("");
          return false;
        }
        else
        {

          Materialize.toast('Document has been uploaded', 2000,'green rounded');
        }
      },
    });
	readCOCURL(this);
   });
      
   function readDELURL(input) {

  if (input.files && input.files[0]) {

      //var filename = input.files[0].name;

    //$('#image_info').val(filename);

      var reader = new FileReader();

      reader.onload = function (e) {

          $('.uploader-placeholder-deal').css('background-image', 'url('+e.target.result+')');

      };

      reader.readAsDataURL(input.files[0]);

  }
   }
   
   function readEVENTURL(input) {

  if (input.files && input.files[0]) {

      //var filename = input.files[0].name;

    //$('#image_info').val(filename);

      var reader = new FileReader();

      reader.onload = function (e) {

          $('.uploader-placeholder-event').css('background-image', 'url('+e.target.result+')');

      };

      reader.readAsDataURL(input.files[0]);

  }
   }
   
   function readCOURL(input) {

  if (input.files && input.files[0]) {

      //var filename = input.files[0].name;

    //$('#image_info').val(filename);

      var reader = new FileReader();

      reader.onload = function (e) {

          $('.uploader-placeholder_profile').css('background-image', 'url('+e.target.result+')');

      };

      reader.readAsDataURL(input.files[0]);

  }

}

function readCOCURL(input) {

  if (input.files && input.files[0]) {

      //var filename = input.files[0].name;

    //$('#image_info').val(filename);

      var reader = new FileReader();

      reader.onload = function (e) {

          $('.uploader-placeholder-company').css('background-image', 'url('+e.target.result+')');

      };

      reader.readAsDataURL(input.files[0]);

  }

}

   $(document).on("change",".docup",function(){
	   var file_id=$(this).data("val");

   var filesize = (this.files[0].size);
   var filename = (this.files[0].name);
   $.ajax({
      url:base_url+'profile/check_for_allowedsize',
      type:"POST",
      datatype: 'json',
      data:{
		  'csrf_test_name':csrf_hash,
		  "filesize":filesize,
          "filename":filename,
           "doctype":"file",
      },
      success:function(res){
        //alert(res);
        var res1 = JSON.parse(res);

         if(res1=="sizeexc"){

            Materialize.toast('Document size exceeds 10MB limit', 3000,'red rounded');
          $('.docup').val("");
          return false;
        }
        else if(res1 == 'error')
        {

          Materialize.toast('Document format is incorrect', 2000,'red rounded');
           $('.docup').val("");
          return false;
        }
         else if(res1 == 'subscription')
        {

          Materialize.toast('You can’t use more than 1 GB space in the trial period. Do subscribe to an appropriate pack for more storage space');
              $('.hide-file').val("");
          return false;
        }
        else if(res1 == 'subscription_1')
        {

          Materialize.toast('Your allotted space of 1 GB is utilised. Click on Buy Add-ons in accounts module in the profile dropdown to buy more space');
              $('.hide-file').val("");
          return false;
        } else if(res1 == 'subscription_2')
        {

          Materialize.toast('Your allotted space of 5 GB is utilised. Click on Buy Add-ons in accounts module in the profile dropdown to buy more space');
              $('.hide-file').val("");
          return false;
        } else if(res1 == 'subscription_3')
        {

          Materialize.toast('Your allotted space of 10 GB is utilised. Click on Buy Add-ons in accounts module in the profile dropdown to buy more space');
              $('.hide-file').val("");
          return false;
        }
        else if(res1 == 'true')
        {

         Materialize.toast('Document has been uploaded', 2000,'green rounded');

		  $(".f_name").text(filename);
		  $("#f_name"+file_id).text(filename);
		  $("#purchase_bill").val(filename);
        }
        else{
         return false;
        }
      },
    });
   });

   });
     function pad (str, max) {
       str = str.toString();
       return str.length < max ? pad("0" + str, max) : str;
     }
   /*$("select").change(function () {
   if($(this).val()!=''){
     $(this).valid();
   }
   });*/
</script>
<script type="text/javascript">
   $(window).load(function() {
       $(".loading_data").fadeOut();
   });
   var somethingChanged=false;
     $('form input').change(function() {
         somethingChanged = true;
    });
      $('form').on('submit', function(event) {
       omethingChanged = false;
       $(window).unbind( "beforeunload" );
    });
     $('button,.go-back').click(function() {
       omethingChanged = false;
       $(window).unbind( "beforeunload" );
    });
     $(window).bind('beforeunload', function(e){
         if(somethingChanged)
             return "You made some changes and it's not saved?";
         else
             e=null; // i.e; if form state change show warning box, else don't show it.
     });
   // Enable navigation prompt
   /*$(function () {
       $("input, textarea, select").on("input change", function() {
           window.onbeforeunload = window.onbeforeunload || function (e) {
               return "You have unsaved changes.  Do you want to leave this page and lose your changes?";
           };
       });
       $("form").on("submit", function() {
           window.onbeforeunload = null;
       });
   })*/

</script>
<script type="text/javascript">
  $(document).ready(function() {

    $('.notificationStatus').click(function(e) {
      //alert($(this).attr("id"));
      var id = $(this).attr("id");
      var link = $('#link').val();

            $.ajax({
              url:base_url+'Notification/read_notification',
              type:"POST",
              data:{'csrf_test_name':csrf_hash,'noti_id':id},
              success:function(res){
               if(res = 'Done'){
                  window.location.replace(link);
               }else{

               }

              },
            });
    });
});
</script>
<script type="text/javascript">

   var TrialExpireStatus = '<?php  if(@$this->user_session["reg_trial_expire"]){ echo $this->user_session["reg_trial_expire"]; } else{ echo 0;} ?>'
   if(TrialExpireStatus == 1)
   {
      $(".hide_on_tial_expire").css("pointer-events", "none");
   }else{
      $(".hide_on_tial_expire").css("pointer-events", "auto");
   }
   //alert(TrialExpireStatus);

</script>
<script type="text/javascript">
		$('textarea').keypress(function (e) {
			var regex = new RegExp("^[A-Za-z0-9 ]*$");
			var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
			if (regex.test(str)) {
				return true;
			}else{
				e.preventDefault();
				return false;
			}
		});
		$.fn.regexMask = function(mask) {
			$(this).keypress(function (event) {
				if (!event.charCode) return true;
				var part1 = this.value.substring(0, this.selectionStart);
				var part2 = this.value.substring(this.selectionEnd, this.value.length);
				if (!mask.test(part1 + String.fromCharCode(event.charCode) + part2))
					return false;
			});
		};
		var mask = new RegExp('^[A-Za-z0-9_@.,-/&: ]*$')	
		$("input[type='text']").regexMask(mask);
		//$("input[type='text']").bind("cut copy paste",function(e) {
			//e.preventDefault();
		//});
		$("input[type='password']").bind("cut copy paste",function(e) {
			e.preventDefault();
		});
		$('textarea').bind("cut copy paste",function(e) {
			e.preventDefault();
		});	
	</script>
<!--custom-script.js - Add your own theme custom JS-->
<!-- <script type="text/javascript" src="../../js/custom-script.js"></script> -->
<!-- <script type="text/javascript" src="../../js/scripts/dashboard-ecommerce.js"></script> -->
</body>
</html>
