
<aside id="left-sidebar-nav" class="nav-expanded nav-lock nav-collapsible hide_on_tial_expire">

    <div class="brand-sidebar">

        <!--<h1 class="logo-wrapper">
              <a href="<?php echo base_url();?>dashboard" class="brand-logo darken-1">
                <span class="logo-text hide-on-med-and-down"><span class="text-hide-collapsible"><span class="bold">Eazy</span>Invoices</span></span>
              </a>
        </h1>-->
        <a href="<?php echo base_url();?>" class="brand-logo darken-1">
        <div id='logo_img' style="padding-right:58px; padding-bottom:8px; padding-top:13px; padding-left:5px; background-color:white; border-right:1px solid #e0ebeb;" hidden>
            <img width="50" height="40" src="<?php echo base_url(); ?>public/images/logo dia-1.jpg" alt="small-eazy"></img>
        </div><!-- #f8f9fd -->
		<div id="eazy_logo" style="background-color:white !important; width:252px !important; height:68px !important;">
			<img width="205" height="55" style="margin:7px 10px 5px 20px !important;" src="<?php echo base_url(); ?>public/images/xebra-logo.png" alt="Logo" class="text-hide-collapsible logo_style_2"/>
		</div>
        </a>

        <a href="#" class="navbar-toggler" id="hide_sidebar">

            <i class="menu-icon open"></i>

        </a>

    </div>

<?php if($this->session->userdata['user_session']['reg_admin_type'] == 5 || $this->session->userdata['user_session']['reg_admin_type'] == 4 || $this->session->userdata['user_session']['reg_admin_type'] == 3 ||$this->session->userdata['user_session']['reg_admin_type'] == 2 || $this->session->userdata['user_session']['reg_admin_type'] == 1 || $this->session->userdata['user_session']['reg_admin_type'] == -1) 
{ ?>
  
    <ul id="slide-out" class="side-nav fixed leftside-navigation" style="background-color: inherit !important">

        <?php if($this->session->userdata['user_session']['reg_admin_type'] == 3 || $this->session->userdata['user_session']['reg_admin_type'] == -1) 
                  { ?>
		
		<!-- Business Analytics -->
        <li class="no-padding">

            <ul class="collapsible" data-collapsible="accordion">

                <li class="bold ba <?= get_active_tab_main('business_analytics'); ?><?= get_active_tab_main('services'); ?>">

                    <a class="collapsible-header ba waves-effect waves-cyan <?= get_active_tab_main('business_analytics'); ?><?= get_active_tab_main('services'); ?>">

                        <i class="menu dashboard-icon"></i>

                        <span class="nav-text">My Business Analytics</span>

                    </a>

                    <div class="collapsible-body ba-body">

                        <ul>

                            <li class="<?php if($this->router->fetch_class()=="business_analytics"){ get_active_tab('index');  if($this->router->fetch_method()=="index" || $this->router->fetch_method()=="customers_revenue" ) { echo "tree"; } else { echo "notree"; } } ?> ">

                                <a id="cust_rev" href="<?php echo base_url();?>business_analytics/customers-revenue">

                                    <i class="menu-round-icon"></i>

                                    <span>Clients & Revenue</span>

                                </a>

                            </li>
							
							<ul class="collapsible" data-collapsible="accordion" style="margin-top:0px !important; margin-bottom:20px !important;">
                            <li class=" <?php if($this->router->fetch_class()=="business_analytics"){ if($this->router->fetch_method()=="credit_history_invoice" || $this->router->fetch_method()=="credit_history_list" || $this->router->fetch_method()=="index" ){ echo 'tree' ; }else { echo 'notree'; } } ?>">
                              <!-- <a href="#"><i class="menu-round-icon"></i><span>TDS</span></a> -->
                                    <a class="collapsible-header waves-effect waves-cyan <?php if($this->router->fetch_class()=="business_analytics"){ if($this->router->fetch_method()=="credit_history_list" || $this->router->fetch_method()=="credit_history_invoice" || $this->router->fetch_method()=="index" ){ echo 'active';} } ?>"><span class="nav-text"><i class="menu-round-icon"></i>Credit History</span></a>
                                    <div class="collapsible-body">
                                        <ul style="margin-left: 30px;">
                                            <li class="<?php if($this->router->fetch_class()=="business_analytics"){ if($this->router->fetch_method()=="credit_history_list" || $this->router->fetch_method()=="index" ){ echo 'tree' ; }else { echo 'notree'; }  } ?>">

                                            <a href="<?= base_url(); ?>business_analytics/credit_history_list">

                                                <i class="menu-round-icon"></i>

                                                <span>Client Wise</span>

                                            </a>

                                            </li>
                                        <li class="<?php if($this->router->fetch_class()=="business_analytics"){ if($this->router->fetch_method()=="credit_history_invoice" || $this->router->fetch_method()=="index" ){ echo 'tree' ; }else { echo 'notree'; }  } ?>">

                                            <a href="<?= base_url(); ?>business_analytics/credit_history_invoice">

                                                <i class="menu-round-icon"></i>

                                                <span>Invoice Wise</span>

                                            </a>

                                        </li>       
                                        </ul>
                                    </div>
								</li>
							</ul>
							<li class="<?php if($this->router->fetch_class()=="business_analytics"){ get_active_tab('index');  if($this->router->fetch_method()=="index" || $this->router->fetch_method()=="vendor_expenses" ) { echo "tree"; } else { echo "notree"; } } ?> ">

                                <a id="cust_rev" href="<?php echo base_url();?>business_analytics/vendor-expenses">

                                    <i class="menu-round-icon"></i>

                                    <span>Vendors & Expenses</span>

                                </a>

                            </li>
                            
							<li class="<?php if($this->router->fetch_class()=="business_analytics"){ get_active_tab('index');  if($this->router->fetch_method()=="index" || $this->router->fetch_method()=="graph_employees" ) { echo "tree"; } else { echo "notree"; } } ?> ">

                                <a id="cust_rev" href="<?php echo base_url();?>business_analytics/employee_analytiics">

                                    <i class="menu-round-icon"></i>

                                    <span>Employee Analytics</span>

                                </a>

                            </li>
                       
                        </ul>
                    </div>
                    <!--<div class="collapsible-body">
                        
                    </div>-->

                </li>

            </ul>

        </li>
        <?php }?>

        <!-- My Sales -->
		<?php if($this->session->userdata['user_session']['reg_admin_type'] == 3 || $this->session->userdata['user_session']['reg_admin_type'] == 1 || $this->session->userdata['user_session']['reg_admin_type'] == 4 || $this->session->userdata['user_session']['reg_admin_type'] == 5 || $this->session->userdata['user_session']['reg_admin_type'] == -1) { ?>
		<li class="no-padding">

            <ul class="collapsible" data-collapsible="accordion">

                <li class="bold <?= get_active_tab_main('sales'); ?><?= get_active_tab_main('services'); ?>">

                    <a class="collapsible-header waves-effect waves-cyan <?= get_active_tab_main('sales'); ?><?= get_active_tab_main('services'); ?>">

                        <i class="menu sales-icon"></i>

                        <span class="nav-text">My Sales</span>

                    </a>

                    <div class="collapsible-body">

                        <ul>
                          <?php if($this->session->userdata['user_session']['reg_admin_type'] == 3 || $this->session->userdata['user_session']['reg_admin_type'] == 1 || $this->session->userdata['user_session']['reg_admin_type'] == -1) { ?>
                            <li class="<?php if($this->router->fetch_class()=="sales"){ get_active_tab('index');  if($this->router->fetch_method()=="index" || $this->router->fetch_method()=="manage_clients" || $this->router->fetch_method()=="add_client" || $this->router->fetch_method()=="edit_client" || $this->router->fetch_method()=="view_client" ) { echo "tree"; } else { echo "notree"; } } ?> ">

                                <a href="<?php echo base_url();?>sales/my-clients">

                                    <i class="menu-round-icon"></i>

                                    <span>Client Master</span>

                                </a>

                            </li>
                            <li class="<?php if($this->router->fetch_class()=="services"){  get_active_tab('index'); if($this->router->fetch_method()=="index" || $this->router->fetch_method()=="add_service" || $this->router->fetch_method()=="edit_service" ||  $this->router->fetch_method()=="view_services")  { echo "tree"; } else { echo "notree"; } } ?>">

                                <a href="<?php echo base_url();?>services">

                                    <i class="menu-round-icon"></i>

                                    <span>Items Master</span>

                                </a>

                            </li>
                            <li class="<?php if($this->router->fetch_method()=="billing_documents" || $this->router->fetch_method()=="add_sales_invoice" || $this->router->fetch_method()=="edit_sales_invoice" || $this->router->fetch_method()=="add_proforma_invoice" || $this->router->fetch_method()=="edit_proforma_invoice" || $this->router->fetch_method()=="add_estimate_invoice" || $this->router->fetch_method()=="edit_estimate_invoice") { echo "tree"; } else { echo "notree"; }   ?>">

                                <a href="<?php echo base_url();?>sales/billing-documents">

                                    <i class="menu-round-icon"></i>

                                    <span>Billing Documents</span>

                                </a>

                            </li>

                            <li class="<?php if($this->router->fetch_method()=="credit_notes" || $this->router->fetch_method()=="add_credit_note" || $this->router->fetch_method()=="edit_credit_note" || $this->router->fetch_method()=="view_credit_note") { echo "tree"; }else { echo "notree"; }   ?>">

                                <a href="<?php echo base_url(); ?>sales/credit-notes">

                                    <i class="menu-round-icon"></i>

                                    <span>Credit Notes</span>

                                </a>

                            </li>
                        <?php } ?>
						
						<!--li class="<?php //if($this->router->fetch_class()=="receipts_payments"){ if($this->router->fetch_method()=="add_sales_receipt" || $this->router->fetch_method()=="edit_sales_receipt" || $this->router->fetch_method()=="view_sales_receipt" || $this->router->fetch_method()=="index" ){ echo 'tree' ; }else { echo 'notree'; }  } ?>">
							<a href="<?//= base_url(); ?>receipts-payments">
                                <i class="menu-round-icon"></i>
								<span>Sales Receipts</span>
							</a>
						</li-->
						
                    <?php if($this->session->userdata['user_session']['reg_admin_type'] == 3 || $this->session->userdata['user_session']['reg_admin_type'] == 1 || $this->session->userdata['user_session']['reg_admin_type'] == 4 ||$this->session->userdata['user_session']['reg_admin_type'] == 5|| $this->session->userdata['user_session']['reg_admin_type'] == -1) { ?>
                  <li class="<?php if($this->router->fetch_method()=="expense_voucher" || $this->router->fetch_method()=="add_expense_voucher" || $this->router->fetch_method()=="manage_expense_voucher"  || $this->router->fetch_method()=="edit_expense_voucher" ) { echo "tree"; }else { echo "notree"; }   ?>">

                    <a href="<?php echo base_url(); ?>sales/expense_voucher">

                      <i class="menu-round-icon"></i>

                      <span>Expense Vouchers</span>

                    </a>

                  </li>
                     <?php } ?>
                        </ul>

                    </div>

                </li>

            </ul>

        </li>
		<?php } ?>
		
		<!-- My Expense -->
         <?php if($this->session->userdata['user_session']['reg_admin_type'] == 3 || $this->session->userdata['user_session']['reg_admin_type'] == 1  || $this->session->userdata['user_session']['reg_admin_type'] == -1) { ?>
		<li class="no-padding">
            <ul class="collapsible" data-collapsible="accordion">
				<li class="my-exp bold <?= get_active_tab_main('expense'); ?><?= get_active_tab_main('my_payroll'); ?><?= get_active_tab_main('My_payroll'); ?><?= get_active_tab_main('cash_bank'); ?><?= get_active_tab_main('Cash_bank'); ?>">
                    <a class="my-exp collapsible-header waves-effect waves-cyan <?= get_active_tab_main('expense'); ?><?= get_active_tab_main('my_payroll'); ?><?= get_active_tab_main('My_payroll'); ?><?= get_active_tab_main('cash_bank'); ?><?= get_active_tab_main('Cash_bank'); ?>">
                        <i class="menu expense-icon"></i>
                        <span class="nav-text">My Expenses</span>
                    </a>
                    <div class="collapsible-body col-exp">
                        <ul>
                            <?php if($this->session->userdata['user_session']['reg_admin_type'] == 3 || $this->session->userdata['user_session']['reg_admin_type'] == 1 || $this->session->userdata['user_session']['reg_admin_type'] == 5 || $this->session->userdata['user_session']['reg_admin_type'] == -1) { ?>
							<li class="<?php if($this->router->fetch_class()=="expense"){ get_active_tab('index');  if($this->router->fetch_method()=="index" || $this->router->fetch_method()=="manage_vendors" || $this->router->fetch_method()=="add_vendor" || $this->router->fetch_method()=="edit_vendor" || $this->router->fetch_method()=="view_vendor") { echo "tree"; } else { echo "notree"; } } ?> ">
                                <a href="<?php echo base_url(); ?>expense/manage_vendors">
                                    <i class="menu-round-icon"></i>
                                    <span>Vendor Master</span>
                                </a>
                            </li>
							
							<li class="<?php if($this->router->fetch_class()=="expense"){ get_active_tab('index');  if($this->router->fetch_method()=="index" || $this->router->fetch_method()=="manage_expense_master" || $this->router->fetch_method()=="add_expense_master" || $this->router->fetch_method()=="edit_expense_master" || $this->router->fetch_method()=="view_expense_master") { echo "tree"; } else { echo "notree"; } } ?> ">
                                <a href="<?= base_url(); ?>expense/manage_expense_master">
                                    <i class="menu-round-icon"></i>
                                    <span>Expense Master</span>
                                </a>
                            </li>
							
							<li class="<?php if($this->router->fetch_class()=="expense"){ get_active_tab('index');  if($this->router->fetch_method()=="index" || $this->router->fetch_method()=="purchase_order" || $this->router->fetch_method()=="add_purchase_order" || $this->router->fetch_method()=="edit_purchase_order" || $this->router->fetch_method()=="view_purchase_order") { echo "tree"; } else { echo "notree"; } } ?> ">
                                <a href="<?= base_url(); ?>expense/purchase_order">
                                    <i class="menu-round-icon"></i>
                                    <span>Purchase Orders</span>
                                </a>
                            </li>
						
							<li class="<?php if($this->router->fetch_class()=="expense"){ get_active_tab('index');  if($this->router->fetch_method()=="index" || $this->router->fetch_method()=="manage_company_expense" || $this->router->fetch_method()=="add_company_expense" ) { echo "tree"; } else { echo "notree"; } } ?> ">
                                <a href="<?= base_url(); ?>expense/manage_company_expense">
                                    <i class="menu-round-icon"></i>
                                    <span>Company Expenses</span>
                                </a>
                            </li>
                        <?php } ?>
                            <?php if($this->session->userdata['user_session']['reg_admin_type'] == 3 || $this->session->userdata['user_session']['reg_admin_type'] == 1 || $this->session->userdata['user_session']['reg_admin_type'] == 4 || $this->session->userdata['user_session']['reg_admin_type'] == 5 || $this->session->userdata['user_session']['reg_admin_type'] == -1) { ?>
							<li class="salary <?php if($this->router->fetch_class()=="my_payroll" || $this->router->fetch_class()=="My_payroll"){ get_active_tab('index');  if($this->router->fetch_method()=="index" || $this->router->fetch_method()=="manage_salary_expense" ||$this->router->fetch_method()=="add_salary_expense" || $this->router->fetch_method()=="edit_salary_expense" || $this->router->fetch_method()=="view_salary_expense") { echo "tree"; } else { echo "notree"; } } ?> ">
                                <a href="<?= base_url(); ?>my_payroll/manage_salary_expense">
                                    <i class="menu-round-icon"></i>
                                    <span>Salary Expenses</span>
                                </a>
                            </li>
							<?php } ?>
                            <?php if($this->session->userdata['user_session']['reg_admin_type'] == 3 || $this->session->userdata['user_session']['reg_admin_type'] == 1|| $this->session->userdata['user_session']['reg_admin_type'] == -1) { ?>
							<li class="<?php if($this->router->fetch_class()=="expense"){ get_active_tab('index');  if($this->router->fetch_method()=="index" || $this->router->fetch_method()=="debit_note" || $this->router->fetch_method()=="edit_debit_note" || $this->router->fetch_method()=="add_debit_note" || $this->router->fetch_method()=="view_debit_note" ) { echo "tree"; } else { echo "notree"; } } ?> ">
                                <a href="<?= base_url(); ?>expense/debit_note">
                                    <i class="menu-round-icon"></i>
										<span>Debit Notes</span>
                                </a>
                            </li>

                            
							<li class="<?php if($this->router->fetch_class()=="cash_bank" || $this->router->fetch_class()=="Cash_bank"){ get_active_tab('index');  if($this->router->fetch_method()=="index" || $this->router->fetch_method()=="petty_cash" || $this->router->fetch_method()=="manage_petty_cash" || $this->router->fetch_method()=="insert_petty_cash") { echo "tree"; } else { echo "notree"; } } ?> ">
                                <a href="<?php echo base_url(); ?>cash_bank/petty_cash">
									<i class="menu-round-icon"></i>
									<span>Petty Cash</span>
                                </a>
                            </li>
                        <?php } ?>
                        </ul>
                    </div>
                </li>
            </ul>
        </li>

    <?php } ?>
	
		<!-- Assets -->
        <?php if($this->session->userdata['user_session']['reg_admin_type'] == 3 || $this->session->userdata['user_session']['reg_admin_type'] == 1  || $this->session->userdata['user_session']['reg_admin_type'] == -1) { ?>
		<li class="no-padding">
            <ul class="collapsible" data-collapsible="accordion">
				<li class="bold <?= get_active_tab_main('Assets'); ?>">
                    <a class="collapsible-header waves-effect waves-cyan <?= get_active_tab_main('Assets'); ?>">
                        <i class="menu asset-icon"></i>
                        <span class="nav-text">My Assets & Depreciation</span>
                    </a>
                    <div class="collapsible-body">
                        <ul>
							<li class="<?php if($this->router->fetch_class()=="Assets"){ get_active_tab('index');  if($this->router->fetch_method()=="index" || $this->router->fetch_method()=="asset_register" || $this->router->fetch_method()=="create_asset_register" || $this->router->fetch_method()=="asset_register_edit") { echo "tree"; } else { echo "notree"; } } ?> ">
                                <a href="<?php echo base_url(); ?>Assets/asset_register">

                                    <i class="menu-round-icon"></i>

                                    <span>Vendor & Asset Master</span>
                                </a>
                            </li>
							
							<li class="<?php if($this->router->fetch_class()=="Assets"){ get_active_tab('index');  if($this->router->fetch_method()=="index" || $this->router->fetch_method()=="purchase_blank" || $this->router->fetch_method()=="asset_purchase" || $this->router->fetch_method()=="add_asset_purchase" || $this->router->fetch_method()=="asset_purchase_edit" || $this->router->fetch_method()=="purchase_view") { echo "tree"; } else { echo "notree"; } } ?> ">
                                <a href="<?php echo base_url(); ?>Assets/asset_purchase">
                                    <i class="menu-round-icon"></i>
                                    <span>Asset Purchase</span>
                                </a>
                            </li>
							
							<li class="<?php if($this->router->fetch_class()=="Assets"){ get_active_tab('index');  if($this->router->fetch_method()=="index" || $this->router->fetch_method()=="asset_sales" || $this->router->fetch_method()=="add_asset_sale" || $this->router->fetch_method()=="view_asset_sales"  || $this->router->fetch_method()=="sales_view") { echo "tree"; } else { echo "notree"; } } ?> ">
                                <a href="<?php echo base_url(); ?>Assets/asset_sales">
                                    <i class="menu-round-icon"></i>
                                    <span>Asset Sales</span>
                                </a>
                            </li>
							
                            <li class="<?php if($this->router->fetch_class()=="Assets"){ get_active_tab('index');  if($this->router->fetch_method()=="index" || $this->router->fetch_method()=="asset_tracker" || $this->router->fetch_method()=="create_asset_tracker" || $this->router->fetch_method()=="asset_tracker_edit" || $this->router->fetch_method()=="tracker_view" ) { echo "tree"; } else { echo "notree"; } } ?> ">
                                <a href="<?php echo base_url(); ?>Assets/asset_tracker">

                                    <i class="menu-round-icon"></i>

                                    <span>Asset Tracker</span>
                                </a>
                            </li>
							
							<li class="<?php if($this->router->fetch_class()=="Assets"){ get_active_tab('index');  if($this->router->fetch_method()=="index" || $this->router->fetch_method()=="depreciation_schedule" ) { echo "tree"; } else { echo "notree"; } } ?> ">
                                <a href="<?php echo base_url(); ?>Assets/depreciation_schedule">

                                    <i class="menu-round-icon"></i>

                                    <span>Depreciation Schedule</span>
                                </a>
                            </li>
							
                        </ul>
                    </div>
                </li>
            </ul>
        </li>
	<?php } ?>
		
		<!-- My Receipts & Payments -->
        <?php if($this->session->userdata['user_session']['reg_admin_type'] == 3 || $this->session->userdata['user_session']['reg_admin_type'] == 1 || $this->session->userdata['user_session']['reg_admin_type'] == -1) { ?>
        <li class="no-padding">

                <ul class="collapsible" data-collapsible="accordion">

                    <li class="bold rp <?= get_active_tab_main('receipts_payments'); ?><?= get_active_tab_main('employee'); ?>">
                        <a class="collapsible-header rp waves-effect waves-cyan <?= get_active_tab_main('receipts_payments'); ?><?= get_active_tab_main('employee'); ?>">

                            <i class="menu receipt-icon"></i>

                            <span class="nav-text">My Receipts & Payments</span>

                        </a>
                        
                        <div class="collapsible-body rp-col">
                        
                            <ul class="collapsible" data-collapsible="accordion">

                                <li class=" <?php if($this->router->fetch_class()=="receipts_payments"){ if($this->router->fetch_method()=="add_sales_receipt" || $this->router->fetch_method()=="edit_sales_receipt" || $this->router->fetch_method()=="view_sales_receipt" || $this->router->fetch_method()=="add_asset_sales_receipt" || $this->router->fetch_method()=="edit_asset_sales_receipt" || $this->router->fetch_method()=="view_asset_sales_receipt" || $this->router->fetch_method()=="asset_sales_receipt" || $this->router->fetch_method()=="manage_employee_payment" || $this->router->fetch_method()=="index" ){ echo 'tree' ; }else { echo 'notree'; }  } ?> ">
                                    <a class="collapsible-header waves-effect waves-cyan <?php if($this->router->fetch_class()=="receipts_payments"){ if($this->router->fetch_method()=="add_sales_receipt" || $this->router->fetch_method()=="edit_sales_receipt" || $this->router->fetch_method()=="view_sales_receipt" || $this->router->fetch_method()=="add_asset_sales_receipt" || $this->router->fetch_method()=="edit_asset_sales_receipt" || $this->router->fetch_method()=="view_asset_sales_receipt" || $this->router->fetch_method()=="asset_sales_receipt" || $this->router->fetch_method()=="index" ){ echo 'active';} } ?>"><span class="nav-text"><i class="menu-round-icon"></i>Receipts</span></a>
                                    <div class="collapsible-body">

                                    <ul style="margin-left: 30px; ">

                                        <li class="<?php if($this->router->fetch_class()=="receipts_payments"){ if($this->router->fetch_method()=="add_sales_receipt" || $this->router->fetch_method()=="edit_sales_receipt" || $this->router->fetch_method()=="view_sales_receipt" || $this->router->fetch_method()=="index" ){ echo 'tree' ; }else { echo 'notree'; }  } ?>">

                                            <a href="<?= base_url(); ?>receipts-payments">

                                                <i class="menu-round-icon"></i>

                                                <span>Sales Receipts</span>

                                            </a>

                                        </li>
										
										<li class="<?php if($this->router->fetch_class()=="receipts_payments"){ if($this->router->fetch_method()=="add_asset_sales_receipt" || $this->router->fetch_method()=="edit_asset_sales_receipt" || $this->router->fetch_method()=="view_asset_sales_receipt" || $this->router->fetch_method()=="asset_sales_receipt" ){ echo 'tree' ; }else { echo 'notree'; }  } ?>">

                                            <a href="<?= base_url(); ?>receipts-payments/asset_sales_receipt">

                                                <i class="menu-round-icon"></i>

                                                <span>Asset Sales Receipts</span>

                                            </a>

                                        </li>

                                    </ul>

                                </div>

                              </li>
							  
								<li class=" <?php if($this->router->fetch_class()=="receipts_payments" ){ if($this->router->fetch_method()=="tax_payment_blank" || $this->router->fetch_method()=="tax_payment_gst_list" || $this->router->fetch_method()=="equilisation_levy" || $this->router->fetch_method()=="manage_tds_payment" || $this->router->fetch_method()=="manage_prof_tax" || $this->router->fetch_method()=="tax_payment_add_gst" || $this->router->fetch_method()=="view_gst_details" || $this->router->fetch_method()=="equa_levy_blank" || $this->router->fetch_method()=="expense_payments" || $this->router->fetch_method()=="asset_payments" || $this->router->fetch_method()=="manage_employee_payment" || $this->router->fetch_method()=="create_expense_payment" || $this->router->fetch_method()=="manage_tds_employee" || $this->router->fetch_method()=="create_asset_payments" || $this->router->fetch_method()=="edit_asset_payments" || $this->router->fetch_method()=="view_asset_payments" || $this->router->fetch_method()=="add_employee_pay" || $this->router->fetch_method()=="edit_employee_pay" || $this->router->fetch_method()=="view_payment_details" || $this->router->fetch_method()=="add_tds_payment" || $this->router->fetch_method()=="edit_tds_payment" || $this->router->fetch_method()=="tds_payment_view" || $this->router->fetch_method()=="manage_tds_employee" || $this->router->fetch_method()=="add_tds_employee" || $this->router->fetch_method()=="edit_tds_employee" || $this->router->fetch_method()=="view_tds_employee" || $this->router->fetch_method()=="add_equa_levy" || $this->router->fetch_method()=="tax_payment_edit_equi" || $this->router->fetch_method()=="view_equalevy_details" || $this->router->fetch_method()=="add_prof_tax" || $this->router->fetch_method()=="edit_expense_payment" || $this->router->fetch_method()=="edit_prof_tax" || $this->router->fetch_method()=="view_expense_payments" || $this->router->fetch_method()=="view_prof_tax" || $this->router->fetch_method()=="index" ){ echo 'tree' ; }else { echo 'notree'; } } ?>">
                                    <a class="collapsible-header waves-effect waves-cyan <?php if($this->router->fetch_class()=="receipts_payments" || $this->router->fetch_class()=="employee"){ if($this->router->fetch_method()=="tax_payment_blank" || $this->router->fetch_method()=="tax_payment_gst_list" || $this->router->fetch_method()=="equilisation_levy" || $this->router->fetch_method()=="manage_prof_tax" || $this->router->fetch_method()=="manage_tds_payment" || $this->router->fetch_method()=="tax_payment_add_gst" || $this->router->fetch_method()=="view_gst_details" || $this->router->fetch_method()=="equa_levy_blank" || $this->router->fetch_method()=="expense_payments" || $this->router->fetch_method()=="asset_payments" || $this->router->fetch_method()=="manage_employee_payment" || $this->router->fetch_method()=="edit_tds_employee" || $this->router->fetch_method()=="create_asset_payments" || $this->router->fetch_method()=="create_expense_payment" || $this->router->fetch_method()=="edit_asset_payments" || $this->router->fetch_method()=="view_asset_payments" || $this->router->fetch_method()=="add_employee_pay" || $this->router->fetch_method()=="edit_employee_pay" || $this->router->fetch_method()=="add_tds_payment" || $this->router->fetch_method()=="edit_tds_payment" || $this->router->fetch_method()=="view_payment_details" || $this->router->fetch_method()=="tds_payment_view"|| $this->router->fetch_method()=="manage_tds_employee" || $this->router->fetch_method()=="add_tds_employee" || $this->router->fetch_method()=="edit_tds_employee" || $this->router->fetch_method()=="view_tds_employee" || $this->router->fetch_method()=="add_equa_levy" || $this->router->fetch_method()=="tax_payment_edit_equi" || $this->router->fetch_method()=="view_equalevy_details" || $this->router->fetch_method()=="add_prof_tax" || $this->router->fetch_method()=="edit_prof_tax" || $this->router->fetch_method()=="edit_expense_payment" || $this->router->fetch_method()=="view_prof_tax" || $this->router->fetch_method()=="view_expense_payments" || $this->router->fetch_method()=="index" ){ echo 'active';} } ?>"><span class="nav-text"><i class="menu-round-icon"></i>Payments</span></a>
                                    <div class="collapsible-body">

                                    <ul style="margin-left: 30px;">
									
                                        <li class="<?php if($this->router->fetch_class()=="receipts_payments" || $this->router->fetch_class()=="employee"){ if($this->router->fetch_method()=="expense_payments" || $this->router->fetch_method()=="create_expense_payment" || $this->router->fetch_method()=="edit_expense_payment" || $this->router->fetch_method()=="view_expense_payments" || $this->router->fetch_method()=="index" ){ echo 'tree' ; }else { echo 'notree'; }  } ?>">
                                                        
                                            <a href="<?= base_url(); ?>receipts_payments/expense_payments">

                                                <i class="menu-round-icon"></i>

                                                    <span>Expense Payments</span>

                                            </a>

                                        </li>
										<li class="<?php if($this->router->fetch_class()=="receipts_payments"){ if($this->router->fetch_method()=="add_asset_payments" || $this->router->fetch_method()=="asset_payments" || $this->router->fetch_method()=="edit_asset_payments" || $this->router->fetch_method()=="view_asset_payments" || $this->router->fetch_method()=="create_asset_payments" || $this->router->fetch_method()=="edit_asset_payments" || $this->router->fetch_method()=="view_asset_payments"){ echo 'tree' ; }else { echo 'notree'; }  } ?>">
                                                        
                                            <a href="<?= base_url(); ?>receipts_payments/asset_payments">

                                                <i class="menu-round-icon"></i>

													<span>Asset Payments</span>

											</a>

                                        </li>
										
										<li class="<?php if($this->router->fetch_class()=="receipts_payments" || $this->router->fetch_class()=="employee"){ get_active_tab("manage_employee_payment"); if($this->router->fetch_method()=="manage_employee_payment" || $this->router->fetch_method()=="add_employee_pay" || $this->router->fetch_method()=="edit_employee_pay" || $this->router->fetch_method()=="view_payment_details") { echo "tree"; } else { echo "notree"; } } ?>">
                                                        
                                            <a href="<?= base_url(); ?>employee/manage_employee_payment">

                                                <i class="menu-round-icon"></i>

                                                    <span>Salary Payments</span>

                                            </a>

                                        </li>
										<ul class="collapsible" data-collapsible="accordion" style="margin-top:0px !important; margin-bottom:20px !important;">
                                        <li class="<?php if($this->router->fetch_class()=="receipts_payments"){ if($this->router->fetch_method()=="tax_payment_blank" || $this->router->fetch_method()=="tax_payment_gst_list" || $this->router->fetch_method()=="tax_payment_add_gst" || $this->router->fetch_method()=="equilisation_levy" || $this->router->fetch_method()=="manage_tds_payment" || $this->router->fetch_method()=="manage_prof_tax" || $this->router->fetch_method()=="view_gst_details" || $this->router->fetch_method()=="equa_levy_blank" || $this->router->fetch_method()=="manage_tds_employee" || $this->router->fetch_method()=="tds_payment_view" || $this->router->fetch_method()=="add_tds_payment" || $this->router->fetch_method()=="add_tds_employee" || $this->router->fetch_method()=="edit_tds_employee" || $this->router->fetch_method()=="view_tds_employee" || $this->router->fetch_method()=="add_equa_levy" || $this->router->fetch_method()=="tax_payment_edit_equi" || $this->router->fetch_method()=="view_equalevy_details" || $this->router->fetch_method()=="add_prof_tax" || $this->router->fetch_method()=="edit_prof_tax" || $this->router->fetch_method()=="view_prof_tax" || $this->router->fetch_method()=="edit_tds_payment" || $this->router->fetch_method()=="index" ){ echo 'tree' ; }else { echo 'notree'; }  } ?>">
                                        <a class="collapsible-header waves-effect waves-cyan <?php if($this->router->fetch_class()=="receipts_payments"){ if($this->router->fetch_method()=="tax_payment_blank" || $this->router->fetch_method()=="tax_payment_gst_list" || $this->router->fetch_method()=="manage_tds_payment" || $this->router->fetch_method()=="equilisation_levy" || $this->router->fetch_method()=="manage_prof_tax" || $this->router->fetch_method()=="tax_payment_add_gst" || $this->router->fetch_method()=="view_gst_details" || $this->router->fetch_method()=="equa_levy_blank" || $this->router->fetch_method()=="manage_tds_employee"  || $this->router->fetch_method()=="tds_payment_view" || $this->router->fetch_method()=="add_tds_payment" || $this->router->fetch_method()=="add_tds_employee" || $this->router->fetch_method()=="edit_tds_employee" || $this->router->fetch_method()=="view_tds_employee" || $this->router->fetch_method()=="add_equa_levy" || $this->router->fetch_method()=="tax_payment_edit_equi" || $this->router->fetch_method()=="view_equalevy_details" || $this->router->fetch_method()=="add_prof_tax" || $this->router->fetch_method()=="edit_prof_tax" || $this->router->fetch_method()=="edit_tds_payment" || $this->router->fetch_method()=="view_prof_tax" || $this->router->fetch_method()=="index" ){ echo 'active';} } ?>"><span class="nav-text"><i class="menu-round-icon"></i>Tax Payments</span></a>
                                        <div class="collapsible-body">

                                        <ul style="margin-left: 30px;">
											<li class="<?php if($this->router->fetch_class()=="receipts_payments"){ if($this->router->fetch_method()=="tax_payment_blank" || $this->router->fetch_method()=="tax_payment_gst_list" || $this->router->fetch_method()=="tax_payment_add_gst" || $this->router->fetch_method()=="view_gst_details" || $this->router->fetch_method()=="index" ){ echo 'tree' ; }else { echo 'notree'; }  } ?>">
                                                        
                                            <a href="<?= base_url(); ?>receipts_payments/tax_payment_gst_list">

												<i class="menu-round-icon"></i>

                                                <span>GST</span>

                                            </a>

                                            </li>
											
											<ul class="collapsible" data-collapsible="accordion" style="margin-top:0px !important; margin-bottom:20px !important;">
											<li class=" <?php if($this->router->fetch_class()=="receipts_payments"){ if($this->router->fetch_method()=="manage_tds_payment" || $this->router->fetch_method()=="manage_tds_employee"|| $this->router->fetch_method()=="manage_tds_employee" || $this->router->fetch_method()=="edit_tds_payment" || $this->router->fetch_method()=="tds_payment_view" || $this->router->fetch_method()=="add_tds_payment" || $this->router->fetch_method()=="add_tds_employee" || $this->router->fetch_method()=="edit_tds_employee" || $this->router->fetch_method()=="view_tds_employee" || $this->router->fetch_method()=="edit_tds_payment"){ echo 'tree' ; }else { echo 'notree'; }  } ?> "><!-- <a href="#"><i class="menu-round-icon"></i><span>TDS</span></a> -->
											<a class="collapsible-header waves-effect waves-cyan <?php if($this->router->fetch_class()=="receipts_payments"){ if($this->router->fetch_method()=="manage_tds_payment" || $this->router->fetch_method()=="manage_tds_employee" || $this->router->fetch_method()=="manage_tds_employee" || $this->router->fetch_method()=="tds_payment_view" || $this->router->fetch_method()=="add_tds_payment" || $this->router->fetch_method()=="add_tds_employee" || $this->router->fetch_method()=="edit_tds_employee" || $this->router->fetch_method()=="view_tds_employee" || $this->router->fetch_method()=="edit_tds_payment"){ echo 'active';} } ?>"><span class="nav-text"><i class="menu-round-icon"></i>TDS</span></a>
											<div class="collapsible-body">

											<ul style="margin-left: 30px;">

											<li class="<?php if($this->router->fetch_class()=="receipts_payments"){ if($this->router->fetch_method()=="manage_tds_payment" || $this->router->fetch_method()=="add_tds_payment" || $this->router->fetch_method()=="edit_tds_payment" || $this->router->fetch_method()=="tds_payment_view" ){ echo 'tree' ; }else { echo 'notree'; } } ?>">

                                            <a href="<?= base_url(); ?>receipts_payments/manage_tds_payment">

                                                <i class="menu-round-icon"></i>

                                                <span>Expenses</span>

                                            </a>

											</li>
											<li class="<?php if($this->router->fetch_class()=="receipts_payments"){ if($this->router->fetch_method()=="manage_tds_employee" || $this->router->fetch_method()=="add_tds_employee" || $this->router->fetch_method()=="edit_tds_employee" ||$this->router->fetch_method()=="view_tds_employee"){ echo 'tree' ; }else { echo 'notree'; }  } ?>">

                                            <a href="<?= base_url(); ?>receipts_payments/manage_tds_employee">

                                                <i class="menu-round-icon"></i>

                                                <span>Employees</span>

                                            </a>

											</li>
											<!--<li>

                                            <a href="#">

                                                <i class="menu-round-icon"></i>

                                                <span>for Employees</span>

                                            </a>

											</li> -->
											</ul>
											</div>
											</li>    
											</ul>
                                            <li class="<?php if($this->router->fetch_class()=="receipts_payments"){ if($this->router->fetch_method()=="equilisation_levy" || $this->router->fetch_method()=="equa_levy_list" || $this->router->fetch_method()=="add_equa_levy" || $this->router->fetch_method()=="tax_payment_edit_equi" || $this->router->fetch_method()=="view_equalevy_details" || $this->router->fetch_method()=="index" ){ echo 'tree' ; }else { echo 'notree'; }  } ?>">
												<a href="<?= base_url(); ?>receipts_payments/equilisation_levy">
												<i class="menu-round-icon"></i>
												<span>Equalisation Levy</span>
                                                </a>
											</li>

                                            <li class="<?php if($this->router->fetch_class()=="receipts_payments"){ if($this->router->fetch_method()=="manage_prof_tax" || $this->router->fetch_method()=="add_prof_tax" || $this->router->fetch_method()=="edit_prof_tax" || $this->router->fetch_method()=="view_prof_tax" || $this->router->fetch_method()=="index" ){ echo 'tree' ; }else { echo 'notree'; }  } ?>">
                                                        
												<a href="<?= base_url(); ?>receipts_payments/manage_prof_tax">

                                                <i class="menu-round-icon"></i>

                                                <span>Professsional Tax</span>

                                                </a>

                                            </li>

                                            </ul>
                                            </div>
											</li>
										</ul>
                                    </ul>
                                    </div>

                              </li>
                              
                        </ul>
                        </div>
                        
                    </li>
                </ul>
        </li>
  <?php } ?>
		
		<!-- My Employee -->
         <?php if($this->session->userdata['user_session']['reg_admin_type'] == 3 || $this->session->userdata['user_session']['reg_admin_type'] == 1  || $this->session->userdata['user_session']['reg_admin_type'] == -1 ||  $this->session->userdata['user_session']['reg_admin_type'] == 4) { ?>
		<li class="no-padding">
            <ul class="collapsible" data-collapsible="accordion">
                <li class="bold emp <?= get_active_tab_main('employee'); ?><?= get_active_tab_main('my_payroll'); ?><?= get_active_tab_main('My_payroll'); ?>">
                    <a class="collapsible-header emp waves-effect waves-cyan <?= get_active_tab_main('employee'); ?><?= get_active_tab_main('my_payroll'); ?><?= get_active_tab_main('My_payroll'); ?>">
                        <i class="menu employee-icon"></i>
                        <span class="nav-text">My Employees</span>
                    </a>

                    <div class="collapsible-body emp-col">
                        <ul>

                            <?php if($this->session->userdata['user_session']['reg_admin_type'] == 3 || $this->session->userdata['user_session']['reg_admin_type'] == 1  || $this->session->userdata['user_session']['reg_admin_type'] == -1 || $this->session->userdata['user_session']['reg_admin_type'] == 4) { ?>

							<li class="bold <?php if($this->router->fetch_class()=="employee"){ if($this->router->fetch_method()=="index" || $this->router->fetch_method()=="manage_employee_master" ||$this->router->fetch_method()=="add_employee_master" || $this->router->fetch_method()=="edit_employee_master" || $this->router->fetch_method()=="view_employee_master" || $this->router->fetch_method()=="view_employee_details") { echo "tree"; }else { echo "notree"; } } ?>">	
                                <a href="<?= base_url(); ?>employee/manage_employee_master">
                                    <i class="menu-round-icon"></i>
                                    <span>Employee Master</span>
                                </a>
							</li>
                            <?php } ?>
                            <?php if($this->session->userdata['user_session']['reg_admin_type'] == 3 || $this->session->userdata['user_session']['reg_admin_type'] == 1 || $this->session->userdata['user_session']['reg_admin_type'] == 5 || $this->session->userdata['user_session']['reg_admin_type'] == -1 || $this->session->userdata['user_session']['reg_admin_type'] == 4) { ?>
							<li class="bold appraisal <?php if($this->router->fetch_class()=="My_payroll" || $this->router->fetch_class()=="my_payroll" ){ get_active_tab('index'); if($this->router->fetch_method()=="index" || $this->router->fetch_method()=="manage_appraisal" || $this->router->fetch_method()=="create_appraisal" || $this->router->fetch_method()=="edit_appraisal" || $this->router->fetch_method()=="view_appraisal") { echo "tree"; }else { echo "notree"; } } ?>">	
								<a class="app" href="<?= base_url(); ?>my_payroll/manage_appraisal">
                                    <i class="menu-round-icon"></i>
                                    <span>Appraisal</span>
                                </a>
							</li>
                        <?php } ?>
                        </ul>
                    </div>
                </li>
            </ul>
        </li>
		<?php } ?>
		<!-- My Bank & Cash -->
        <?php if($this->session->userdata['user_session']['reg_admin_type'] == 3 || $this->session->userdata['user_session']['reg_admin_type'] == -1) { ?>
		<li class="no-padding">
            <ul class="collapsible" data-collapsible="accordion">
				<li class="bold cash <?= get_active_tab_main('Cash_bank'); ?>"	>
					<a class="collapsible-header cash waves-effect waves-cyan <?= get_active_tab_main('Cash_bank'); ?>">
                        <i class="menu cashbank-icon"></i>
                        <span class="nav-text">MY CASH & BANK </span>
                    </a>

                    <div class="collapsible-body cash-col">
						<ul>
							
							
							<li class="<?php if($this->router->fetch_class()=="Cash_bank"){ get_active_tab('index');  if($this->router->fetch_method()=="index" || $this->router->fetch_method()=="list_cash_statement") { echo "tree"; } else { echo "notree"; } } ?> ">
								<a href="<?php echo base_url(); ?>Cash_bank/list_cash_statement">
								<i class="menu-round-icon"></i>
								<span> Cash Statement </span>
								</a>
                            </li>
							
							<li class="<?php if($this->router->fetch_class()=="Cash_bank"){ get_active_tab('index');  if($this->router->fetch_method()=="index" || $this->router->fetch_method()=="list_bank_statement") { echo "tree"; } else { echo "notree"; } } ?> ">
								<a href="<?php echo base_url(); ?>Cash_bank/list_bank_statement">
								<i class="menu-round-icon"></i>
								<span> Bank Statement </span>
								</a>
                            </li>
						</ul>
                   </div>
                </li>
            </ul>
        </li>
   <?php } ?>
        
		<!--<li class="no-padding">

            <ul class="collapsible" data-collapsible="accordion">

                <li class="bold <?php //if($this->router->fetch_class()=="receipts_payments"){ if($this->router->fetch_method()=="index" || $this->router->fetch_method()=="add_sales_receipt" || $this->router->fetch_method()=="edit_sales_receipt" || $this->router->fetch_method()=="view_sales_receipt" ) { echo "tree"; }else { echo "notree"; } } ?>">

                    <a class="collapsible-header waves-effect waves-cyan <?//= get_active_tab_main('receipts_payments'); ?>">

                        <i class="menu purchase-icon"></i>

                        <span class="nav-text">My Receipts</span>

                    </a>

                    <div class="collapsible-body">

                        <ul>
                            <li class="<?php //if($this->router->fetch_class()=="receipts_payments"){ echo get_active_tab('index'); } ?><?//= get_active_tab('add_sales_receipt'); ?><?//= get_active_tab('edit_sales_receipt'); ?><?//= get_active_tab('view_sales_receipt'); ?>">

                                <a href="<?//= base_url(); ?>receipts-payments">

                                    <i class="menu-round-icon"></i>

                                    <span>Sales Receipts</span>

                                </a>
                                

                            </li>
                            
                            <!--<li class="<?php //if($this->router->fetch_class()=="receipts_payments"){ echo get_active_tab('index'); } ?><?//= get_active_tab('add_sales_receipt'); ?><?//= get_active_tab('edit_sales_receipt'); ?><?//= get_active_tab('view_sales_receipt'); ?>">

                                <a href="<?//= base_url(); ?>receipts-payments/tax_payment_blank">

                                    <i class="menu-round-icon"></i>

                                    <span>Tax Payment</span>

                                </a>
                                

                            </li>

                        </ul>
                    </div>
                </li>
            </ul>
        </li>-->

		<!-- My Tax Summary -->
        <?php if($this->session->userdata['user_session']['reg_admin_type'] == 3 ||$this->session->userdata['user_session']['reg_admin_type'] == 2  || $this->session->userdata['user_session']['reg_admin_type'] == 1 || $this->session->userdata['user_session']['reg_admin_type'] == -1) { ?>
		<li class="no-padding">

                <ul class="collapsible" data-collapsible="accordion">

                    <li class="bold <?= get_active_tab_main('tax_summary'); ?>">

                        <a class="collapsible-header waves-effect waves-cyan <?= get_active_tab_main('tax_summary'); ?>">

                            <i class="menu gst-icon"></i>

                            <span class="nav-text">My Tax Summary</span>

                        </a>

                        <div class="collapsible-body">
                        
                            <ul class="collapsible" data-collapsible="accordion">

                                <li class=" <?php if($this->router->fetch_class()=="tax_summary"){ if( $this->router->fetch_method()=="tax_sum_tds_vendors" || $this->router->fetch_method()=="tax_summary_tds_employee" || $this->router->fetch_method()=="clients_tds" || $this->router->fetch_method()=="index" ){ echo 'tree' ; }else { echo 'notree'; }  } ?> ">
                                    <a class="collapsible-header waves-effect waves-cyan <?php if($this->router->fetch_class()=="tax_summary"){ if($this->router->fetch_method()=="tax_sum_tds_vendors" || $this->router->fetch_method()=="tax_summary_tds_employee" || $this->router->fetch_method()=="clients_tds" || $this->router->fetch_method()=="index" ){ echo 'active';} } ?>"><span class="nav-text"><i class="menu-round-icon"></i>TDS</span></a>
                                    <div class="collapsible-body">

                                    <ul style="margin-left: 30px; ">

                                        <li class="<?php if($this->router->fetch_class()=="tax_summary"){ if($this->router->fetch_method()=="clients_tds" || $this->router->fetch_method()=="index" ){ echo 'tree' ; }else { echo 'notree'; }  } ?>">

                                            <a href="<?= base_url(); ?>tax_summary/clients_tds">

                                                <i class="menu-round-icon"></i>

                                                <span>For Clients</span>

                                            </a>

                                        </li>
                                        <li class="<?php if($this->router->fetch_class()=="tax_summary"){ if($this->router->fetch_method()=="tax_sum_tds_vendors" || $this->router->fetch_method()=="index" ){ echo 'tree' ; }else { echo 'notree'; }  } ?>">

                                             <a href="<?= base_url(); ?>tax_summary/tax_sum_tds_vendors">

                                                <i class="menu-round-icon"></i>

                                                <span>For Vendors</span>

                                            </a>

                                        </li>
                                        <li class="<?php if($this->router->fetch_class()=="tax_summary"){ if($this->router->fetch_method()=="tax_summary_tds_employee" || $this->router->fetch_method()=="index" ){ echo 'tree' ; }else { echo 'notree'; }  } ?>">

                                             <a href="<?= base_url(); ?>tax_summary/tax_summary_tds_employee">

                                                <i class="menu-round-icon"></i>

                                                <span>For Employees</span>

                                            </a>

                                        </li>

                                    </ul>

                                </div>

                              </li>


								<li class=" <?php if($this->router->fetch_class()=="tax_summary"){ if($this->router->fetch_method()=="tax_summary_gst" || $this->router->fetch_method()=="tax_summary_credit_gst" || $this->router->fetch_method()=="tax_summary_gst_3b" || $this->router->fetch_method()=="tax_summary_credit_3b" || $this->router->fetch_method()=="pur_gst_reg" || $this->router->fetch_method()=="pur_gst_unreg" || $this->router->fetch_method()=="clients_tds" || $this->router->fetch_method()=="index" ){ echo 'tree' ; }else { echo 'notree'; } } ?>">
								<!-- <a href="#"><i class="menu-round-icon"></i><span>TDS</span></a> -->
                                    <a class="collapsible-header waves-effect waves-cyan <?php if($this->router->fetch_class()=="tax_summary"){ if($this->router->fetch_method()=="tax_summary_gst" || $this->router->fetch_method()=="tax_summary_credit_gst" || $this->router->fetch_method()=="clients_tds" || $this->router->fetch_method()=="tax_summary_gst_3b" || $this->router->fetch_method()=="tax_summary_credit_3b" || $this->router->fetch_method()=="pur_gst_reg" || $this->router->fetch_method()=="pur_gst_unreg" || $this->router->fetch_method()=="index" ){ echo 'active';} } ?>"><span class="nav-text"><i class="menu-round-icon"></i>GST</span></a>
                                    <div class="collapsible-body">
										
                                    <ul class="collapsible" data-collapsible="accordion" style="margin-left: 30px;">
										<li class=" <?php if($this->router->fetch_class()=="tax_summary"){ if($this->router->fetch_method()=="tax_summary_gst" || $this->router->fetch_method()=="tax_summary_credit_gst" || $this->router->fetch_method()=="index" ){ echo 'tree' ; }else { echo 'notree'; } } ?>">
										<a class="collapsible-header waves-effect waves-cyan <?php if($this->router->fetch_class()=="tax_summary"){ if($this->router->fetch_method()=="tax_summary_gst" || $this->router->fetch_method()=="tax_summary_credit_gst" || $this->router->fetch_method()=="index" ){ echo 'active';} } ?>"><span class="nav-text"><i class="menu-round-icon"></i>GSTR1</span></a>
										<div class="collapsible-body">

										<ul style="margin-left: 30px;">
									
									
                                        <li class="<?php if($this->router->fetch_class()=="tax_summary"){ if($this->router->fetch_method()=="tax_summary_gst" || $this->router->fetch_method()=="index" ){ echo 'tree' ; }else { echo 'notree'; }  } ?>">

                                            <a href="<?= base_url(); ?>tax-summary/tax-summary-gst">

                                                <i class="menu-round-icon"></i>

                                                <span>Sales Invoice</span>

                                            </a>

                                        </li>
                                        <li class="<?php if($this->router->fetch_class()=="tax_summary"){ if($this->router->fetch_method()=="tax_summary_credit_gst" || $this->router->fetch_method()=="index" ){ echo 'tree' ; }else { echo 'notree'; }  } ?>">

                                            <a href="<?= base_url(); ?>tax-summary/tax-summary-credit-gst">

                                                <i class="menu-round-icon"></i>

                                                <span>Credit Note</span>

                                            </a>

                                        </li>
										</ul>
										</div>
										</li>
										<!--li class="<?php //if($this->router->fetch_class()=="tax_summary"){ if($this->router->fetch_method()=="pur_gst_reg" || $this->router->fetch_method()=="index" ){ echo 'tree' ; }else { echo 'notree'; }  } ?>">

                                            <a href="<?//= base_url(); ?>tax-summary/pur_gst_reg">

                                                <i class="menu-round-icon"></i>

                                                <span>GSTR2</span>

                                            </a>

                                        </li-->
										
										<li class="<?php if($this->router->fetch_class()=="tax_summary"){ if($this->router->fetch_method()=="pur_gst_unreg" || $this->router->fetch_method()=="index" ){ echo 'tree' ; }else { echo 'notree'; }  } ?>">

                                            <a href="<?= base_url(); ?>tax-summary/pur_gst_unreg">

                                                <i class="menu-round-icon"></i>

                                                <span>GSTR3B</span>

                                            </a>

                                        </li>
										
										<!--ul class="collapsible" data-collapsible="accordion" style="margin-top: 0px;">
										<li class=" <?php //if($this->router->fetch_class()=="tax_summary"){ if($this->router->fetch_method()=="tax_summary_gst_3b" || $this->router->fetch_method()=="tax_summary_credit_3b" || $this->router->fetch_method()=="pur_gst_unreg" || $this->router->fetch_method()=="index" ){ echo 'tree' ; }else { echo 'notree'; } } ?>">
										<a class="collapsible-header waves-effect waves-cyan <?php //if($this->router->fetch_class()=="tax_summary"){ //if($this->router->fetch_method()=="tax_summary_gst_3b" || $this->router->fetch_method()=="tax_summary_credit_3b" || $this->router->fetch_method()=="pur_gst_unreg" || $this->router->fetch_method()=="index" ){ echo 'active';} } ?>"><span class="nav-text"><i class="menu-round-icon"></i>GSTR3B</span></a>
										<div class="collapsible-body">

										<ul style="margin-left: 30px;">
									
									
                                        <li class="<?php //if($this->router->fetch_class()=="tax_summary"){ if($this->router->fetch_method()=="tax_summary_gst_3b" || $this->router->fetch_method()=="index" ){ echo 'tree' ; }else { echo 'notree'; }  } ?>">

                                            <a href="<?//= base_url(); ?>tax-summary/tax-summary-gst-3b">

                                                <i class="menu-round-icon"></i>

                                                <span>Sales Invoice</span>

                                            </a>

                                        </li>
                                        <li class="<?php //if($this->router->fetch_class()=="tax_summary"){ if($this->router->fetch_method()=="tax_summary_credit_3b" || $this->router->fetch_method()=="index" ){ echo 'tree' ; }else { echo 'notree'; }  } ?>">

                                            <a href="<?//= base_url(); ?>tax-summary/tax-summary-credit-3b">

                                                <i class="menu-round-icon"></i>

                                                <span>Credit Note</span>

                                            </a>

                                        </li>
										<li class="<?php //if($this->router->fetch_class()=="tax_summary"){ if($this->router->fetch_method()=="pur_gst_unreg" || $this->router->fetch_method()=="index" ){ echo 'tree' ; }else { echo 'notree'; }  } ?>">

                                            <a href="<?//= base_url(); ?>tax-summary/pur_gst_unreg">

                                                <i class="menu-round-icon"></i>

                                                <span>RD Company Expense</span>

                                            </a>

                                        </li>
										</ul>
										</div>
										</li>
										</ul-->
										
                                    </ul>

                                </div>

                              </li>

							  <li class="<?php if($this->router->fetch_class()=="tax_summary"){ if($this->router->fetch_method()=="equalisation_levy" || $this->router->fetch_method()=="index" ){ echo 'tree' ; }else { echo 'notree'; }  } ?>">

                                <a href="<?= base_url(); ?>tax_summary/equalisation-levy">

                                  <i class="menu-round-icon"></i>

                                  <span>Equalisation Levy</span>

                                </a>

                              </li>

                              <li class="<?php if($this->router->fetch_class()=="tax_summary"){ if($this->router->fetch_method()=="prof_tax" || $this->router->fetch_method()=="index" ){ echo 'tree' ; }else { echo 'notree'; }  } ?>">

                                <a href="<?= base_url(); ?>tax-summary/prof_tax">

                                  <i class="menu-round-icon"></i>

                                  <span>Professional Tax</span>

                                </a>

                              </li>

                            </ul>

                        </div>

                    </li>

                </ul>

            </li>
        <?php } ?>
		
		<!-- Statement Of Account -->
        <?php //if($this->session->userdata['user_session']['reg_admin_type'] == 3 || $this->session->userdata['user_session']['reg_admin_type'] == 1 || $this->session->userdata['user_session']['reg_admin_type'] == -1) { ?>
		<!--li class="no-padding">

            <ul class="collapsible" data-collapsible="accordion">

                <li class="bold <?//= get_active_tab_main('statement_account'); ?>">

                    <a class="collapsible-header waves-effect waves-cyan <?//= get_active_tab_main('statement_account'); ?>" href="<?php //echo base_url(); ?>statement_account">

                        <i class="menu soa-icon"></i>

                        <span class="nav-text">Statement Of Accounts</span>

                    </a>

                      <!--div class="collapsible-body">

                            <ul>

                                <li class="<?php //if($this->router->fetch_class()=="statement_account"){  get_active_tab('index'); } ?><?php //if(get_active_tab('statement_account')=='tree' || get_active_tab('index')=='tree' ){ echo "tree"; } else { echo "notree"; } ?> ">

                                    <a href="<?php //echo base_url(); ?>statement_account">

                                        <i class="menu-round-icon"></i>

                                        <span>Clients</span>

                                    </a>

                                </li>
                                

                            </ul>

                        </div-->

                <!--/li>
            </ul>
        </li-->
          <?php //} ?>
		
		<!-- My Account -->
        <!--<?php //if($this->session->userdata['user_session']['reg_admin_type'] == 3 || $this->session->userdata['user_session']['reg_admin_type'] == 1  || $this->session->userdata['user_session']['reg_admin_type'] == -1) { ?>
		<li class="no-padding">
            <ul class="collapsible" data-collapsible="accordion">
				<li class="bold <?//= get_active_tab_main('account'); ?>"	>
					<a class="collapsible-header waves-effect waves-cyan <?//= get_active_tab_main('account'); ?>">
                        <i class="menu account-icon"></i>
                        <span class="nav-text">MY ACCOUNTS</span>
                    </a>

                    <div class="collapsible-body">
						<ul>
							<li class="<?php //if($this->router->fetch_class()=="account"){ get_active_tab('index');  if($this->router->fetch_method()=="index" || $this->router->fetch_method()=="manage_account" ||$this->router->fetch_method()=="newaccount" ||$this->router->fetch_method()=="editaccount" ||$this->router->fetch_method()=="viewacc" ||$this->router->fetch_method()=="get_account_details") { echo "tree"; } else { echo "notree"; } } ?> ">
								<a href="<?php //echo base_url(); ?>account/manage_account">
								<i class="menu-round-icon"></i>
								<span>Account</span>
								</a>
                            </li>
							<li class="<?php //if($this->router->fetch_class()=="account"){ get_active_tab('index');  if($this->router->fetch_method()=="index" || $this->router->fetch_method()=="manage_jv" || $this->router->fetch_method()=="create_jv" || $this->router->fetch_method()=="edit_jv" || $this->router->fetch_method()=="view_jv" || $this->router->fetch_method()=="get_jv_details") { echo "tree"; } else { echo "notree"; } } ?> ">
								<a href="<?php //echo base_url(); ?>account/manage_jv">
								<i class="menu-round-icon"></i>
								<span>JV Account</span>
								</a>
                            </li>
							<li class="<?php //if($this->router->fetch_class()=="account"){  get_active_tab('index'); } ?><?php //if(get_active_tab('report_page')=='tree' || get_active_tab('index')=='tree' ){ echo "tree"; } else { echo "notree"; } ?> ">
								<a href="<?php //echo base_url(); ?>account/report_page">
									<i class="menu-round-icon"></i>
									<span>Accounting Reports</span>
                                </a>
                            </li>
							<li class="<?php //if($this->router->fetch_class()=="account"){  get_active_tab('index'); } ?><?php //if(get_active_tab('ledger_page')=='tree' || get_active_tab('index')=='tree' ){ echo "tree"; } else { echo "notree"; } ?> ">
								<a href="<?php //echo base_url(); ?>account/ledger_page">
									<i class="menu-round-icon"></i>
									<span>Ledger</span>
                                </a>
                            </li>
							<li class="<?php //if($this->router->fetch_class()=="account"){  get_active_tab('index'); } ?><?php //if(get_active_tab('trial_balance')=='tree' || get_active_tab('index')=='tree' ){ echo "tree"; } else { echo "notree"; } ?> ">
								<a href="<?php //echo base_url(); ?>account/trial_balance">
									<i class="menu-round-icon"></i>
									<span>Trial Balance</span>
                                </a>
                            </li>
						</ul>
                   </div>
                </li>
            </ul>
        </li>
		<?php //} ?>-->
		
		<!-- Marketplace -->
		<!--li class="no-padding">
            <ul class="collapsible" data-collapsible="accordion">
                <li class="bold <?php //if($this->router->fetch_class()=="Market_place"){ echo get_active_tab('index'); } ?> ">
                    <a href="<?//= base_url(); ?>market_place/search_company">
                        <i class="menu market-icon"></i>
						<span class="nav-text">MY MARKETPLACE</span>
                    </a>        
                </li>
            </ul>
        </li-->
 
		<!-- My Resource Center -->
		<li class="no-padding">

                <ul class="collapsible" data-collapsible="accordion">

                    <li class="bold <?= get_active_tab_main('resource_center'); ?>">

                        <a class="collapsible-header waves-effect waves-cyan <?= get_active_tab_main('resource_center'); ?>">

                            <i class="menu resource-icon"></i>

                            <span class="nav-text">My Resource Center</span>

                        </a>

                        <div class="collapsible-body">

                            <ul>

                                <li class="<?php if($this->router->fetch_class()=="resource_center"){  get_active_tab('index'); } ?><?php if(get_active_tab('faqs')=='tree' || get_active_tab('index')=='tree' ){ echo "tree"; } else { echo "notree"; } ?>">

                                    <a href="<?php echo base_url(); ?>resource_center/faqs">

                                        <i class="menu-round-icon"></i>

                                        <span>FAQs</span>

                                    </a>

                                </li>
                                <li class="<?php if($this->router->fetch_class()=="resource_center"){  get_active_tab('index'); } ?><?php if(get_active_tab('accounting-terms')=='tree' || get_active_tab('index')=='tree' ){ echo "tree"; } else { echo "notree"; } ?>">

                                    <a href="<?php echo base_url(); ?>resource_center/accounting-terms">

                                        <i class="menu-round-icon"></i>

                                        <span>Accounting Terms</span>

                                    </a>

                                </li>
								
								<li class="<?php if($this->router->fetch_class()=="resource_center"){  get_active_tab('index'); } ?><?php if(get_active_tab('financial_ratios')=='tree' || get_active_tab('index')=='tree' ){ echo "tree"; } else { echo "notree"; } ?>">

                                    <a href="<?php echo base_url(); ?>resource_center/financial_ratios">

                                        <i class="menu-round-icon"></i>

                                        <span>Financial Ratios</span>
 
                                    </a>

                                </li>
                            </ul>

                        </div>

                    </li>

                </ul>

            </li>
    </ul>
		<?php }?>

		<?php
		/*if($this->session->userdata['user_session']['reg_admin_type'] == 2)
		{
		?>
			<ul id="slide-out" class="side-nav fixed leftside-navigation">

            <li class="no-padding">

                <ul class="collapsible" data-collapsible="accordion">

                    <li class="bold <?= get_active_tab_main('tax_summary'); ?>">

                        <a class="collapsible-header waves-effect waves-cyan <?= get_active_tab_main('tax_summary'); ?>">

                            <i class="menu gst-icon"></i>

                            <span class="nav-text">My Tax Summary</span>

                        </a>

                        <div class="collapsible-body">

                            <ul class="collapsible" data-collapsible="accordion">

                                <li class=" <?php if($this->router->fetch_class()=="tax_summary"){ if($this->router->fetch_method()=="clients_tds" || $this->router->fetch_method()=="index" ){ echo 'tree' ; }else { echo 'notree'; }  } ?> "><!-- <a href="#"><i class="menu-round-icon"></i><span>TDS</span></a> -->
                                    <a class="collapsible-header waves-effect waves-cyan <?php if($this->router->fetch_class()=="tax_summary"){ if($this->router->fetch_method()=="clients_tds" || $this->router->fetch_method()=="index" ){ echo 'active';} } ?>"><span class="nav-text"><i class="menu-round-icon"></i>TDS</span></a>
                                    <div class="collapsible-body">

                                    <ul style="margin-left: 30px; ">

                                        <li class="<?php if($this->router->fetch_class()=="tax_summary"){ if($this->router->fetch_method()=="clients_tds" || $this->router->fetch_method()=="index" ){ echo 'tree' ; }else { echo 'notree'; }  } ?>">

                                            <a href="<?= base_url(); ?>tax-summary/clients-tds">

                                                <i class="menu-round-icon"></i>

                                                <span>For Clients</span>

                                            </a>

                                        </li>

                                    </ul>

                                </div>

                              </li>
                              <li class="<?= get_active_tab('tax_summary_gst'); ?>">

                                <a href="<?= base_url(); ?>tax-summary/tax-summary-gst">

                                  <i class="menu-round-icon"></i>

                                  <span>GST</span>

                                </a>

                              </li>
                              <li class="<?= get_active_tab('equalisation_levy'); ?>">

                                <a href="<?= base_url(); ?>tax-summary/equalisation-levy">

                                  <i class="menu-round-icon"></i>

                                  <span>Equalisation Levy</span>

                                </a>

                              </li>

                            </ul>

                        </div>

                    </li>

                </ul>

            </li>
            <li class="no-padding">

                <ul class="collapsible" data-collapsible="accordion">

                    <li class="bold <?= get_active_tab_main('resource_center'); ?>">

                        <a class="collapsible-header waves-effect waves-cyan <?= get_active_tab_main('resource_center'); ?>">

                            <i class="menu resource-icon"></i>

                            <span class="nav-text">My Resource Center</span>

                        </a>

                        <div class="collapsible-body">

                            <ul>

                                <li class="<?php if($this->router->fetch_class()=="resource_center"){  get_active_tab('index'); } ?><?php if(get_active_tab('faqs')=='tree' || get_active_tab('index')=='tree' ){ echo "tree"; } else { echo "notree"; } ?> ">

                                    <a href="<?php echo base_url(); ?>resource_center/faqs">

                                        <i class="menu-round-icon"></i>

                                        <span>FAQs</span>

                                    </a>

                                </li>
                                <li class="<?= get_active_tab('accounting_terms'); ?>">

                                    <a href="<?php echo base_url(); ?>resource_center/accounting-terms">

                                        <i class="menu-round-icon"></i>

                                        <span>Accounting Terms</span>

                                    </a>

                                </li>

                            </ul>

                        </div>

                    </li>

                </ul>

            </li>

      </ul>
    <?php 
    }*/
    ?>

		<?php
		/*if($this->session->userdata['user_session']['reg_admin_type'] == 2)
		{
		?>
		<ul id="slide-out" class="side-nav fixed leftside-navigation">

            <li class="no-padding">

                <ul class="collapsible" data-collapsible="accordion">

                    <li class="bold <?= get_active_tab_main('tax_summary'); ?>">

                        <a class="collapsible-header waves-effect waves-cyan <?= get_active_tab_main('tax_summary'); ?>">

                            <i class="menu gst-icon"></i>

                            <span class="nav-text">My Tax Summary</span>

                        </a>

                        <div class="collapsible-body">

                            <ul class="collapsible" data-collapsible="accordion">

                                <li class=" <?php if($this->router->fetch_class()=="tax_summary"){ if($this->router->fetch_method()=="clients_tds" || $this->router->fetch_method()=="index" ){ echo 'tree' ; }else { echo 'notree'; }  } ?> "><!-- <a href="#"><i class="menu-round-icon"></i><span>TDS</span></a> -->
                                    <a class="collapsible-header waves-effect waves-cyan <?php if($this->router->fetch_class()=="tax_summary"){ if($this->router->fetch_method()=="clients_tds" || $this->router->fetch_method()=="index" ){ echo 'active';} } ?>"><span class="nav-text"><i class="menu-round-icon"></i>TDS</span></a>
                                    <div class="collapsible-body">

                                    <ul style="margin-left: 30px; ">

                                        <li class="<?php if($this->router->fetch_class()=="tax_summary"){ if($this->router->fetch_method()=="clients_tds" || $this->router->fetch_method()=="index" ){ echo 'tree' ; }else { echo 'notree'; }  } ?>">

                                            <a href="<?= base_url(); ?>tax-summary/clients-tds">

                                                <i class="menu-round-icon"></i>

                                                <span>For Clients</span>

                                            </a>

                                        </li>

                                    </ul>

                                </div>

                              </li>
                              <li class="<?= get_active_tab('tax_summary_gst'); ?>">

                                <a href="<?= base_url(); ?>tax-summary/tax-summary-gst">

                                  <i class="menu-round-icon"></i>

                                  <span>GST</span>

                                </a>

                              </li>
                              <li class="<?= get_active_tab('equalisation_levy'); ?>">

                                <a href="<?= base_url(); ?>tax-summary/equalisation-levy">

                                  <i class="menu-round-icon"></i>

                                  <span>Equalisation Levy</span>

                                </a>

                              </li>

                            </ul>

                        </div>

                    </li>

                </ul>   

            </li>
            <li class="no-padding">

                <ul class="collapsible" data-collapsible="accordion">

                    <li class="bold <?= get_active_tab_main('resource_center'); ?>">

                        <a class="collapsible-header waves-effect waves-cyan <?= get_active_tab_main('resource_center'); ?>">

                            <i class="menu resource-icon"></i>

                            <span class="nav-text">My Resource Center</span>

                        </a>

                        <div class="collapsible-body">

                            <ul>

                                <li class="<?php if($this->router->fetch_class()=="resource_center"){  get_active_tab('index'); } ?><?php if(get_active_tab('faqs')=='tree' || get_active_tab('index')=='tree' ){ echo "tree"; } else { echo "notree"; } ?> ">

                                    <a href="<?php echo base_url(); ?>resource_center/faqs">

                                        <i class="menu-round-icon"></i>

                                        <span>FAQs</span>

                                    </a>

                                </li>
                                <li class="<?= get_active_tab('accounting_terms'); ?>">

                                    <a href="<?php echo base_url(); ?>resource_center/accounting-terms">

                                        <i class="menu-round-icon"></i>

                                        <span>Accounting Terms</span>

                                    </a>

                                </li>

                            </ul>

                        </div>

                    </li>

                </ul>

            </li>

      </ul>
    <?php 
    }*/
    ?>

		


    <a href="#" data-activates="slide-out" class="sidebar-collapse btn-floating btn-medium waves-effect waves-light hide-on-large-only gradient-45deg-light-blue-cyan gradient-shadow">

        <i class="material-icons">menu</i>

    </a>


</aside>
