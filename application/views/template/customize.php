<head>
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
	<style>

	/* NEW CSS BY WCID-366 */
		
		#inv_customisemodal p.white-color{
			color:#fff;
		}
		
		#inv_customisemodal label.white-color{
			color:#fff;
		}
		
		#inv_customisemodal .big-w{
			font-size:13px;
			color:#000;
		}
		#inv_customisemodal .small-w{
			font-size:12px;
			color:#000;
		}
		
		#inv_customisemodal .small-sw{
			font-size:11px;
			color:#000;
		}
		
		.inv-title{
			font-size:18px !important;
		}
		
		.eoe_div{
			width:50px;
		}
		
		/* Table CSS */
		#default_1 td{
			border:none !important;
			border-top:1px solid #595959 !important;
		}
		
		#default_1 th{
			border:none !important;
			padding:10px 0 !important;
		}
		
		#default_1 .amttable{
			text-align:center;
		}
		
		#default_2 td{
			border:none !important;
		}
		
		#default_2 th{
			border:none !important;
			padding:10px 0 !important;
		}
		
		#default_2 .amttable{
			text-align:center;
		}
		
		#default_3 td{
			border:none !important;
			border-bottom:1px solid #ccc !important;
		}
		
		#default_3 th{
			border:none !important;
			padding:10px 0 !important;
		}
		
		#default_3 .amttable{
			text-align:center;
		}
		
		.total_box{
			background-color:#000;
			color:#fff;
			border-radius:5px;
			height:auto !important;
			padding:15px 10px !important;
		}
		
		#inv_customisemodal footer {
			margin-top: 50px;
			border-bottom: 23px solid #000;
			padding:15px 10% 10px 16%;
		}
		
		.se_tem_tr{
			background-color:#000 !important;
			color:#fff;
			font-size:13px;
		}
		
		.ls_tem_tr{
			background-color:#8cd98c !important;
			color:#fff;
			font-size:13px;
		}
	/*END*/
	
	#inv_customisemodal p{
		font-size:13px;
	}
	
	*{
		margin: 0;
		padding: 0;
		outline: none;
		-webkit-box-sizing: border-box;
		-moz-box-sizing: border-box;
		box-sizing: border-box;
	}

	.ps-container.ps-active-y > .ps-scrollbar-y-rail {
		display: none !important;
	}

	.tabs {
		margin: 0;
		padding: 0;
		list-style: none;
		overflow: hidden;
		border-bottom: 1px solid #999;
	}

	.tabs li {
		float: left;
		border: 1px solid #999;
		border-bottom: none;
		background: #e0e0e0;
	}

	.tabs li a {
		display: block;
		padding: 10px 20px;
		font-size: 16px;
		color: #000;
		text-decoration: none;
	}

	.tabs li a:hover {
		background: #ccc;
	}

	.tabs li.active,.tabs li.active a:hover {
		font-weight: bold;
		background: #fff;
		border:none;
	}

.tab_container {
  border: 1px #999;
  border-top: none;
  background: #fff;
  width:364px;
  margin-left:10px;
}

.tab_content {
  padding: 20px;
  font-size: 16px;
}

.tabs {
	 margin-left:10px;
	 border-bottom:none;
	 height:40px;
}

/*#paper_size, #orientation ,#hdr_logo{
  position: absolute;
  opacity:1;
  content:none;
  pointer-events: none;
}*/

/*[type="checkbox"] + label:before, [type="checkbox"]:not(.filled-in) + label:after*/
#hdr_logo{
    width: 14px;
    height: 14px;
    margin: 0 0 0 0;
    border: 2px solid #B0B7CA;
}

.caret{
	border-right:0px;
	border-left:0px;
}

#inv_tbl, th, td {
  border: 1px solid black;
  border-collapse: collapse;
}

#tab-table_1 th{
	background-color:#7864e9 !important;
}

p{
	font-size:13px;
}

#inv_tbl #total_amt{
	background-color:#ffffe6;
}

	#b_name, #br_name, #ifsc, #ac_no{
		height:1rem;
		/*margin-top:15px;*/
	}
	
#inv_tbl .head_details{
	text-align:center;
}

[type="checkbox"] + label {
    padding-left: 26px;
    height: 25px;
    line-height: 17px;
}


/*.ps-container > .ps-scrollbar-x-rail > .ps-scrollbar-x {

	display:none;
}*/

.modal input.select-dropdown {
    margin: -10px 0 0 -21px !important;
}

.up_lbl1, .pan-no, .gst-no, .cin-no, .txt_pnt{
	color:#000000;
}

.advance-pay1 {
    width: calc(100% - 0px) !important;
}

	#tab-table .amttable{
		text-align:center;
	}
	
	th.dis1 p {
		padding: 0 0px 0 15px;
	}
	
	p{
		font-size:11px;
	}
	
	.zebra-logo{
		margin-top:20px;
	}
	
	.tem-name{
		text-align:center;
		font-size:13px;
	}
	
	.template_div{
		margin-bottom:4%;
	}
	
	.template_div.active{
		/*border:1px solid #000;*/
	}

	.zoom:hover {
		transform: scale(1.1); /* (150% zoom - Note: if the zoom is too large, it will go outside of the viewport) */
		text-align:center;
	}
	
	.al-right{
			text-align:right;
		}
		
		.big-inv{
			color:#000;
			margin-top:0px !important;
		}
		
		.inv-to{
			font-size:15px;
			color:#000;
		}
		
		.mod-pay{
			color:#000;
			font-size:14px;
			line-height:15px;
		}
		
		.foot{
			background-color:#7864e9;
			margin:-4% 0 0 0;
			padding:5px 0;
		}
		
		.foot_lbl{
			font-size:13px;
			color:#fff;
		}
		
		.xebra{
			color:#fff;
			margin:3% -2%;
		}
		
		.add{
			margin-top:10px;
			float:left;
		}
		
		.add label{
			font-size:13px;
			color:#fff;
		}
		
		.pan{
			margin-top:-10%;
			float:right !important;
		}
		
		.term{
			color:#fff;
			font-size:13px;
		}
		
		.up_lbl, .txt_pnt{
			color:#000;
			font-size:13px;
		}
		
		.paybtn{
		 	color: #fff;
		    padding: 5px 8%;
		    background: #0059b3;
		    border-radius: 5px;
		    border: 1px solid #0059b3;
		    margin: 6px -3px;
		 }
		 
		 hr.bottom-hr {
		  border-color: grey;
		  background: none;
		  padding: 4px 0 0 39px !important;
		  border-top: none;
		  border-left: none;
		  margin: 0 0 10px 3px;
		  border-right: none;
		  float: initial;
		  width: 95%;
		}
		
		.txt_ftr{
			font-size:13px;
			color:#000;
		}
		
		#layout_showbtn{
			height: 38px !important;
			width: 100% !important;
			line-height: 20px !important;
			margin: 1px 0 12px 0 !important;
		}
		
		.note-cust{
			padding-bottom:10px;
		}
		
		.note-cust p{
			font-size:13px !important;
			color:#595959 !important;
		}
		
		.empty-r{
			height:25px;
		}
		
		.company-name{
			font-size:15px;
			color:#595959;
			margin:0 0 -10px 10px;
		}
		
		.term-list-b:not(.browser-default) > li {
			list-style-type: disc !important;
			color:#fff !important;
		}
		
		.term-list:not(.browser-default) > li {
			list-style-type: disc !important;
			color:#000 !important;
		}
		
		.term1-list:not(.browser-default) > li {
			list-style-type: disc !important;
			color:#595959 !important;
		}
		
		input[class=colorin]::-webkit-color-swatch{ 
			background-color: #fff !important; 
		}
</style>
</head>  

	<div class="row" class="custom-template">
		<div class="col s12 m12 l4">
			<div class="masonry" style="margin-left:-18px;">
				<ul class="tabs">
					<li><a id="cust_all" href="#tab1">Template</a></li>
					<li><a id="cust_tab" href="#tab2">Content</a></li>
					<li><a id="foot_tab" href="#tab3">Footer</a></li>
				</ul>
				<div class="tab_container">
					<div id="tab1" class="tab_content"><!-- TAB 1 (Template) -->
						<div class="row">
							<div class="row border-bottom">
								<div class="col s12 m12 l12">
									<button class="btn-flat theme-primary-btn theme-btn theme-btn-large" id="layout_showbtn">SELECT LAYOUT</button>
								</div>
								<div class="col s12 m12 l12">
								<input type="checkbox" id="set_invoice_no" name="set_invoice_no" value="1">
								<label for="set_invoice_no" style="font-size:13px; margin-bottom:12px;">Set Invoice prefix <a style="margin-top:-7px;" class="note tooltipped info-tooltipped" data-position="top" data-delay="50" data-html="true" data-tooltip="You can customise the invoice number so that all</br> your future invoices will start with this prefix"></a></label>
								</div>
								<div class="col s12 m12 l4 inv_inp" style="padding:8px 8px 0 12px;">
									<input style="border:1px solid #c2d6d6; border-radius:6px; padding:0 0 0 10px; height:2rem;" id="inv_no_pre" type="text" maxlength="6" size="6" class="form-control" name="invoice" placeholder="">
								</div>
								<div class="col s12 m12 l8 inv_inp" style="padding-top:15px !important;">
									<p style="font-size:13px !important; color:#000000 !important;">
										<label style="color:#000000 !important;">For Example:</label> <label style=" padding:4px !important; border:0.5px solid black !important; border-style:dotted !important; color:#000000 !important;">INV10</label><label style="color:#000000 !important;">/2019-20</label>		
									</p>
									<!--<input style="border:1px solid #c2d6d6; border-radius:6px; padding-left:10px;" id="inv_no_2" type="text" class="form-control" name="invoice" placeholder="">-->
								</div>
							</div>
							<div class="row border-bottom" style="margin-left:-5px;">									
								<div class="form-check col s12 m12 l4">
									<input type="radio" class="form-check-input" id="paper_size" name="paper" value="A4" checked="checked">
									<label class="form-check-label" for="paper_size">A4</label>
								</div>

								<div class="form-check col s12 m12 l4" style="margin-bottom:20px;">
									<input type="radio" class="form-check-input" id="letter" name="paper" value="letter">
									<label class="form-check-label" for="letter">Letter</label>
								</div>
							</div>
							<!--<div class="row">  
								<div class=" col s12 m12 l12 logo" style="margin-bottom:20px;">
									<label for="orientation" style="font-size:13px;">Orientation</label><br>
									<div class="col s12 m12 l4" style="margin-top:5px;">
										<input type="radio" id="orientation" name="orientation" value="portrait"><span style="font-size:14px;">Portrait</span></input>
									</div>
									<div class="col s12 m12 l4" style="margin-top:5px;">
										<input type="radio" id="orientation" name="orientation" value="landscape"><span style="font-size:14px;">Landscape</span></input>
									</div>
								</div>
							</div>-->
							<div class="row border-bottom" hidden>
								<div class="col s12 m12 l12">
									<label for="margins" style="font-size:13px;">Margins (in inches)</label><br>
									<input type="hidden" id="margins" name="margins" value="" style="width:20%; height:20px; border:1px solid #ccc; !important;">
									<label style="font-size:13px;">Left/right/bottom<label></input>
								</div>
							</div>
							<div class="row border-bottom">
								<div class="col s12 m12 l12">
								<label for="tmp_back_color" style="font-size:11px;">BACKGROUND COLOR</label><br>		
								<div class="col s12 m12 l3" style="margin-top:0px; height:32px; border:1px solid #c2d6d6;">
									<input style="margin-left:-12px; width:74px; height:30px;" class="colorin" id="tmp_hex_color" type="color" name="tmp_hex_color" value="#ffffff" onchange="changeColortmp();"/>
								</div>	
								<div class="col s12 m12 l5" style="margin-top:0px; margin-bottom:10px !important;">	<!-- class="full-bg" -->
									<input style="width:69px; margin:0px 0px 0px -12px !important; height:30px !important; border:1px solid #c2d6d6;" type="text" id="tmp_back_color" name="tmp_back_color" value="#ffffff" type="text"></input>
								</div>
								</div>
							</div>		
							<div class="row border-bottom">
								<div class="col s12 m12 l12">
									<input type="checkbox" id="set_for_all_tmp" name="set_for_all_tmp" value="1">
									<label for="set_for_all_tmp" style="font-size:13px; margin-bottom:20px;">Set For All Invoices</label>
								</div>
							</div>		
							<div class="row border-bottom note-cust">	
								<p style="margin-top:-10px;">These changes and template will get reflected at the time of taking printout or emailing as an attachment and NOT at the time of making invoice entry</p>
							</div>
							<div class="row">
								   <button class="btn-flat theme-primary-btn theme-btn theme-btn-large ml-5 right" id="layout_btn">Save</button>
								   <button class="btn-flat theme-flat-btn theme-btn theme-btn-large right modal-close" type="button" onclick="">CANCEL</button>
							</div>
						</div>
					</div><!-- TAB 1 CLOSE -->
					
					<div class="row">
							<div id="tab2" class="tab_content">	
								<div class="row">
									<label class="pull-left" style="font-size:14px;"><b>Header Logo</b></label>
								</div>
								<div class="row border-bottom">
									<input type="checkbox" id="hdr_logo" name="hdr_logo" value="1"></input>
									<label for="hdr_logo" style="font-size:13px; margin-bottom:10px;">Logo</label>
									<input type="hidden" id="template_val" name="template_val" value=""></input>
								</div>
								<!--<div class="row">
									<div class="col s12 m12 l12" style="margin-bottom:20px;">
										<div class="col s12 m12 l4">
											<label style="color: #000;">LegalContract.jpg</label><br>
											<span style="font-size:13px;">Image size 20px 20px</span>
										</div>	  
										<div class="col s12 m12 l4">
											<a href="#"><img src="<?php echo base_url(); ?>asset/css/img/icons/delete.png"></a>
										</div>
									<!--</div>
								</div>-->
								<div class="row">
									<label class="" style="font-size:14px;"><b>Document Info</b></p>
								</div>
								<div class="row border-bottom">
									<div class="col s12 m12 l6" style="margin-left:-10px !important;">
										<label for="font_color" style="font-size:13px;">FONT COLOR</label>
										<div class="col s12 m12 l6" style="margin-bottom:10px;"><input type="text" id="font_color" value="#000000" type="text" style="width:69px; margin:0px 0px 0px -12px !important; height:30px !important; border:1px solid #c2d6d6;"></input></div>
										<div class="col s12 m12 l6" style="margin-top:0px; height:32px; border:1px solid #c2d6d6;"><input id="hex_color" style="margin-left:-12px; width:68px; height:30px;" type="color" name="favcolor" value="#000000" onchange="changeColor()"></div>
									</div>
									<div class="col s12 m12 l6" style="margin-bottom:20px;">
										<label for="font_size" style="font-size:13px;">FONT SIZE</label><br>
										<div class="col s12 m12 l12" style="border:1px solid #c2d6d6 !important; padding-left:18px !important; height:33px !important;">
											<select id="font_size" name="font_size">
												<option value="8px">8px</option>
												<option value="9px">9px</option>
												<option value="10px">10px</option>
												<option value="11px">11px</option>
												<option value="12px">12px</option>
												<option value="13px" selected>13px</option>
												<option value="14px">14px</option>
												<option value="15px">15px</option>
												<option value="16px">16px</option>
												<option value="17px">17px</option>
												<option value="18px">18px</option>
												<option value="19px">19px</option>
												<option value="20px">20px</option>
											</select>
										</div>
									</div>
								</div>
								<div class="row border-bottom" style="margin-left:-12px !important;">
										<!--<div class="col s12 m12 l6">
											<input type="checkbox" id="shipping_address" name="shipping_address" value="1">
											<label for="shipping_address" style="font-size:9px;"><b>SHIPPING ADDRESS</b></label>
										</div>
										<div class="col s12 m12 l6">
											<input type="checkbox" id="late_fees" name="late_fees" value="1">
											<label for="late_fees" style="font-size:11px;">LATE FEES</label>
										</div>-->
									<div class="col s12 m12 l6">
										<input type="checkbox" id="bank_details" name="bank_details" value="1">
										<label for="bank_details" style="font-size:11px;">BANK DETAILS</label>
									</div>
									<div class="col s12 m12 l6">
										<input type="checkbox" id="pan_no" name="pan_no" value="1">
										<label for="pan_no" style="font-size:11px;">PAN NO</label>
									</div>
									<div class="col s12 m12 l6">
										<input type="checkbox" id="gst" name="gst" value="1">
										<label for="gst" style="font-size:11px;">GST</label>
									</div>
									<div class="col s12 m12 l6">
										<input type="checkbox" id="payment_btn" name="payment_btn" value="1">
										<label for="payment_btn" style="font-size:11px;">PAYMENT BUTTON</label>
									</div>
									<div class="col s12 m12 l6">
										<input type="checkbox" id="cin_no" name="cin_no" value="1">
										<label for="cin_no" style="font-size:11px;">CIN NO</label>
									</div>
									<div class="col s12 m12 l6">
										<input type="checkbox" id="purchase_order_date" name="purchase_order_date" value="1">
										<label for="purchase_order_date" style="font-size:11px;">PURCHASE ORDER DATE OF ISSUE</label>
									</div>
									<div class="col s12 m12 l6">
										<input type="checkbox" id="purchase_order" name="purchase_order" value="1">
										<label for="purchase_order" style="font-size:11px;">PURCHASE ORDER</label>
									</div>
									<div class="col s12 m12 l6">	
									<input type="checkbox" id="terms_condition" name="terms_condition" value="1">
									<label for="terms_condition" style="font-size:11px;">TERMS & CONDITIONS</label>	 
									</div>
								</div>
								<div class="row" style="margin-bottom:5px !important;">
									<label style="font-size:14px;"><b>Other info</b></label>
									<div class="logo" style="margin-top:5px;">
										<input type="checkbox" id="signature" name="signature" value="1">
										<label for="signature" style="font-size:13px;">Show Signature Box<a style="margin-top:-7px;" class="note tooltipped info-tooltipped" data-position="top" data-delay="50" data-html="true" data-tooltip="Upload signature in personal profile module"></a></label>
									</div>
									<?php if($reg_data[0]->reg_degital_signature!=""){?>
									<div class="col s12 m12 l12" style="margin-left:-18px !important;">
										<img width="180" height="40" src="<?php echo base_url(); ?>public/upload/degital_signature/<?php echo $reg_data[0]->reg_id; ?>/<?php echo $reg_data[0]->reg_degital_signature; ?>" style="margin:0px;" class="signature_img" alt="signature">
									</div>
									<!--div class="col s12 m12 l12" style="margin-left:-18px !important;">
										<img width="180" height="40" src="<?php //echo base_url(); ?>asset/images/signature.png" style="margin:0px;" class="signature_img" alt="signature">
									</div-->
									<?php } ?>
									</div>
									<!--div class="row" style="margin-bottom:5px !important;">
									<div class="col s12 m12 l6" style="margin-left:-15px !important;">
									   <input style="border:1px solid #c2d6d6; border-radius:4px; padding-left:10px;" type="text" name="client_name" id="client_name" value="<?php //echo $reg_data[0]->reg_username; ?>" type="text" placeholder="CLIENT NAME" disabled> </input>
									</div>
									<div class="col s12 m12 l6">
									   <input style="border:1px solid #c2d6d6; border-radius:4px; padding-left:10px;" type="text" name="client_designation"  id="client_designation" value="<?php //echo $reg_data[0]->reg_designation; ?>" type="text" placeholder="DESIGNATION" disabled> </input>
									</div>
									</div-->
								<div class="row border-bottom">
									<div class="logo">
										<input type="checkbox" id="set_for_all_cont" name="set_for_all_cont" value="1">
										<label for="set_for_all_cont" style="font-size:13px; margin-bottom:20px;">Set For All Invoices</label>
									</div>
								</div>
								<div class="row border-bottom note-cust">	
									<p style="margin-top:-10px;">These changes and template will get reflected at the time of taking printout or emailing as an attachment and NOT at the time of making invoice entry</p>
								</div>
								<div class="row">
									<div class="col s12 m12 l12">
										<button class="btn-flat theme-primary-btn theme-btn theme-btn-large ml-5 right" id="content_btn">Save</button>
										<button class="btn-flat theme-flat-btn theme-btn theme-btn-large right modal-close" type="button"  onclick="#">CANCEL</button>
									</div>
								</div>
							</div>
						</div>
					<!-- TAB 3 -->
					<div id="tab3" class="tab_content">  
						<div class="row">
									<div class="row">
										<label class="pull-left" style="font-size:14px;">Document Info</label><br>
										<div class="logo" style="margin-top:5px;">
											<input type="checkbox" id="eoe" name="eoe" value="1">
											<label for="eoe" style="font-size:13px;">E&OE - Errors and Omission Expected</label>
										</div>
									</div>
									<div class="row"> <!-- class="full-bg" -->
										<div class="col s12 m12 l6">
											<label for="ftr_font_color" style="font-size:11px; margin-left:-12px !important;">FONT COLOR</label><br>
											<div class="col s12 m12 l6" style="margin-left:-12px; height:32px; border:1px solid #c2d6d6;"><input style="margin-left:-12px; width:68px; height:30px;" id="ftr_hex_color" type="color" name="ftr_hex_color" value="#000000" onchange="changeColorftr()"></div>
											<div class="col s12 m12 l6" style="margin-left:-12px !important;"><input style="width:69px; margin:0px 0px 0px 0px !important; height:30px !important; border:1px solid #c2d6d6;" type="text" id="ftr_font_color" value="#000000" type="text"></input></div>
										</div>
										<div class="col s12 m12 l6">
											<label for="ftr_font_size" style="font-size:11px;">FONT SIZE</label>
											<div class="col s12 m12 l12" style="border:1px solid #c2d6d6 !important; padding-left:18px !important; height:33px !important;">
												<select id="ftr_font_size" name="ftr_font_size" style="margin-left:5px !important;">
													<option value="8px">8px</option>
													<option value="9px">9px</option>
													<option value="10px">10px</option>
													<option value="11px">11px</option>
													<option value="12px" selected>12px</option>
													<option value="13px">13px</option>
													<option value="14px">14px</option>
													<option value="15px">15px</option>
													<option value="16px">16px</option>
													<option value="17px">17px</option>
													<option value="18px">18px</option>
													<option value="19px">19px</option>
													<option value="20px">20px</option>
												</select>
											</div>
										</div>
									</div>
									<div class="row"><!-- class="full-bg" -->
											<label for="ftr_back_color" style="font-size:11px;">BACKGROUND COLOR</label><br>
											<div class="col s12 m12 l6" style=" height:32px; border:1px solid #c2d6d6; width:70px !important;"><input style="margin-left:-12px; width:68px; height:30px;" id="back_hex_color" type="color" name="back_hex_color" value="#ffffff" class="colorin" onchange="changeColorback()"></div>
											<div class="col s12 m12 l3" style="margin-left:-16px;"><input style="width:69px; margin:0px 0px 0px 0px !important; height:30px !important; border:1px solid #c2d6d6;" type="text" id="ftr_back_color" value="#000000" type="text"> </input></div>
									</div>
									<div class="row" style="margin-left:-10px !important;">
											<div class="col s12 m12 l6">
												<input type="checkbox" id="company_address" name="company_address" value="1">
												<label for="company_address" style="font-size:11px;">SHOW COMPANY ADDRESS</label>
											</div>
											<div class="col s12 m12 l6">
												<input type="checkbox" id="company_email" name="company_email" value="1">
												<label for="company_email" style="font-size:11px;">SHOW COMPANY EMAIL ID</label>
											</div>
											<div class="col s12 m12 l6">
												<input type="checkbox" id="company_contact" name="company_contact" value="1">
												<label for="company_contact" style="font-size:11px;">SHOW COMPANY CONTACT NO</label>
											</div>
											<div class="col s12 m12 l6">
												<input type="checkbox" id="company_website" name="company_website" value="1">
												<label for="company_website" style="font-size:11px;">SHOW COMPANY WEBSITE</label>
											</div>
											<div class="col s12 m12 l6">
												<input type="checkbox" id="fb_link" name="fb_link" value="1">
												<label for="fb_link" style="font-size:11px;">FACEBOOK LINK</label>
											</div>
											<div class="col s12 m12 l6">
												<input type="checkbox" id="twitter_link" name="twitter_link" value="1">
												<label for="twitter_link" style="font-size:11px;">TWITTER LINK</label>
											</div>
											<div class="col s12 m12 l6">
												<input type="checkbox" id="linked_link" name="linked_link" value="1">
												<label for="linked_link" style="font-size:11px;">LINKEDIN LINK</label>	
											</div>
									</div>
									<div class="logo border" style="border-top:1px solid #eef2fe; padding:15px 0;">
										<input type="checkbox" id="set_for_all_ftr" name="set_for_all_ftr" value="1">
										<label for="set_for_all_ftr" style="font-size:13px;">Set For All Invoices</label>
									</div>
									<div class="row border-bottom note-cust" style="border-top:1px solid #eef2fe; padding-top:10px;">	
										<p>These changes and template will get reflected at the time of taking printout or emailing as an attachment and NOT at the time of making invoice entry</p>
									</div>
									<div class="row">
										<div class="col s12 m12 l12">
											<button class="btn-flat theme-primary-btn theme-btn theme-btn-large ml-5 right" id="footer_btn">Save</button>
											<button class="btn-flat theme-flat-btn theme-btn theme-btn-large right modal-close" type="button"  onclick="#">CANCEL</button>
										</div>
									</div>
							</div>
						</div>
					</div>
				</div>
			</div><!-- col 4 close -->
			<a style="padding-left:25px; margin-top:15px;" class="modal-close close-pop" style=""><img width="20" height="20" src="<?php echo base_url(); ?>asset/images/popupdelete1.png" alt="delete"></a>
			<div id="template-gallary" class="col s12 m12 l8" style="margin-bottom:20px; margin-left:-60px;">
			
				<!--div class="row border-bottom zebra-logo">
					<div class="col s12 m12 l12" style="text-align:right"><img src="<?php echo base_url(); ?>public/images/xebra-logo.png" height="40" width="200" alt="eazyinvoices-logo"></div>
				</div-->
				<div class="row zebra-logo">
					<div class="col s12 m12 l6 zoom template_div active default" style="text-align:center;">
						<img src="<?php echo base_url(); ?>public/images/invoice-template/1.jpg" height="375" width="300" alt=""><br>
						<div class="tem-name"><span>Default Template</span></div>
					</div>
					<div class="col s12 m12 l6 zoom template_div first_template" style="text-align:center;">
						<img src="<?php echo base_url(); ?>public/images/invoice-template/2.jpg" height="375" width="300" alt=""><br>
						<div class="tem-name"><span>Template 1</span></div>
					</div>
					<!--div class="col s12 m12 l4 zoom template_div second_template">
						<img src="<?php //echo base_url(); ?>public/images/invoice-template/Template-1.jpg" height="200" width="265" alt=""><br>
						<div class="tem-name"><span>Template 2</span></div>
					</div-->
					<div class="col s12 m12 l6 zoom template_div third_template" style="text-align:center;">
						<img src="<?php echo base_url(); ?>public/images/invoice-template/3.jpg" height="375" width="300" alt=""><br>
						<div class="tem-name"><span>Template 2</span></div>
					</div>
					<!--div class="col s12 m12 l4 zoom template_div fourth_template">
						<img src="<?php //echo base_url(); ?>public/images/invoice-template/Template-3.jpg" height="200" width="265" alt=""><br>
						<div class="tem-name"><span>Template 4</span></div>
					</div>
					<div class="col s12 m12 l4 zoom template_div fifth_template">
						<img src="<?php //echo base_url(); ?>public/images/invoice-template/Template-5.jpg" height="200" width="265" alt=""><br>
						<div class="tem-name"><span>Template 5</span></div>
					</div-->
					<div class="col s12 m12 l6 zoom template_div sixth_template" style="text-align:center;">
						<img src="<?php echo base_url(); ?>public/images/invoice-template/4.jpg" height="375" width="300" alt=""><br>
						<div class="tem-name"><span>Template 3</span></div>
					</div>
				</div>
			</div>
			
			<!-- Default Template Start -->
			<div id="default-template" class="col s12 m12 l8" style="margin-bottom:20px; margin-left:-60px;" hidden>
			<div id="inv_tem" class="box-wrapper bg-white shadow border-radius-6" style="padding:0 25px;">
				<div class="row" style="margin:10px 0 0 0; padding:0 0 10px 0; border-bottom:3px solid #595959;">
					<div class="col s12 m12 l12" style="text-align:right; margin:0 0 0 20px;">
						<a position="top" style="padding-left:5px;" class="close-temp" style=""><img width="20" height="20" src="<?php echo base_url(); ?>asset/images/popupdelete1.png" alt="delete"></a>
					</div>
					<div class="col s12 m12 l6 temp_text"><b><p class="inv-title temp_text">SALES INVOICE</b></p></div>
					<div class="col s12 m12 l6" style="text-align:right;">
						<img id="customize_logo_img" src="<?php echo base_url(); ?>public/images/xebra-logo.png" height="55" width="200" alt="xebra-logo">
					</div>
				</div>
				<div class="row">
				<div class="row" style="padding:15px 0; margin-bottom:5px !important; border-bottom:2px solid #595959;">
				<div class="col s4 m4 l4">
					<p class="big-w eoe_div temp_text"><b> E & OE </b></p>
					<p class="big-w temp_text"><b>BILLED TO:</b></p>
					<p class="big-w temp_text"><b>NEW COMPANY LLP,</b></p>
					<p class="small-w temp_text">1ST FLOOR, ABC CHAMBERS, M.G. ROAD,</p>
					<p class="small-w temp_text">MALAD-WEST, MUMBAI, MAHARASHTRA</p>
					<p class="small-w temp_text">INDIA - 400061</p>
				</div>
				<div class="col s2 m2 l2" style="width:13% !important; padding:4% 0;">
					<p class="big-w temp_text"><b>INVOICE NO: </b></p>
					<p style="padding-top:5px;" class="small-w temp_text">INV1/2019-20</p>
				</div>
				<div class="col s2 m2 l2" style="width:15% !important; padding:4% 0;">
					<p class="big-w temp_text"><b>DATE OF ISSUE: </b></p>
					<p style="padding-top:5px;" class="small-w temp_text">15-08-2019</p>
				</div>
				<div class="col s2 m2 l2" style="width:18.5% !important; padding:4% 0;">
					<p class="pu_no big-w temp_text"><b>ESTIMATE/P.O. NO: </b></p>
					<p style="padding-top:5px;" class="pu_no small-w temp_text">10/2019-20</p>
				</div>
				<div class="col s2 m2 l2" style="width:20% !important; padding:4% 0;">
					<p class="pu_o_date big-w temp_text"><b>ESTIMATE/P.O. DATE: </b></p>
					<p style="padding-top:5px;" class="pu_o_date small-w temp_text">10-05-2019</p>
				</div>	
					<!--label class="up_lbl temp_text"><b>TERMS OF PAYMENT: </b></label><label class="txt_pnt temp_text">15</label-->
				</div>
				<div class="inner advance-pay1" id="scrollbar-restable" style="padding:0px 10px 0px 10px !important;">
                        <table id="default_1" class="">
                           <thead>
                              <tr class="" id="">
                                 <th class="" style="width:180px !important; padding-left:6px !important;">
                                    <p class="big-w temp_text">PARTICULARS</p>
                                 </th>
                                 <th class="amttable">
                                    <p class="big-w temp_text">SAC</p>
                                 </th>
                                 <th class="amttable">
                                    <p class="big-w temp_text">QNTY</p>
                                 </th>
                                 <th class="amttable">
                                    <p class="big-w temp_text" style="">RATE</br>(INR)</p>
                                 </th>
                                 <th class="amttable">
                                    <p class="big-w temp_text" style="">TAXABLE</br>(INR) </p> 
                                 </th>
                                 <th class="amttable">
                                    <p class="big-w temp_text">CGST</br>(INR)(%)</p>
                                 </th>
                                 <th class="amttable">
                                    <p class="big-w temp_text">SGST</br>(INR)(%)</p>
                                 </th>
                                 <th class="amttable">
                                    <p class="big-w temp_text">AMOUNT</br>(INR)</p>
                                 </th>
                              </tr>
							</thead>
							<tbody>
								<tr>
									<td class="" style="width:180px !important;">
										<p class="big-w temp_text"><b>ITEM NAME 01</b></p>
										<p class="small-w temp_text">Post promotion for ABC CCC saving </br>account by XYZ.</p>
									</td>
									<td class="amttable">
										<p class="small-w temp_text">345100</p>
									</td>
									<td class="amttable">
										<p class="big-w temp_text">4</p>
									</td>
									<td class="amttable">
										<p class="big-w temp_text">400000</p>
									</td>
									<td class="amttable">
										<p class="big-w temp_text">1600000</p> 
									</td>
									<td class="amttable">
										<p class="big-w temp_text">3500</br>(9%)</p>
									</td>
									<td class="amttable">
										<p class="big-w temp_text">3500</br>(9%)</p>
									</td>
									<td class="amttable">
										<p class="big-w temp_text">1607000</p>
									</td>
								</tr>
								<tr style="height:55px;">
									<td class="" style="width:180px !important;">
										<p class="big-w temp_text"></p>
										<p class="small-w temp_text"></p>
									</td>
									<td class="amttable">
										<p class="small-w temp_text"></p>
									</td>
									<td class="amttable">
										<p class="big-w temp_text"></p>
									</td>
									<td class="amttable">
										<p class="big-w temp_text"></p>
									</td>
									<td class="amttable">
										<p class="big-w temp_text"></p> 
									</td>
									<td class="amttable">
										<p class="big-w temp_text"></p>
									</td>
									<td class="amttable">
										<p class="big-w temp_text"></p>
									</td>
									<td class="amttable">
										<p class="big-w temp_text"></p>
									</td>
								</tr>
								<tr style="height:55px;">
									<td class="" style="width:180px !important;">
										<p class="big-w temp_text"></p>
										<p class="small-w temp_text"></p>
									</td>
									<td class="amttable">
										<p class="small-w temp_text"></p>
									</td>
									<td class="amttable">
										<p class="big-w temp_text"></p>
									</td>
									<td class="amttable">
										<p class="big-w temp_text"></p>
									</td>
									<td class="amttable">
										<p class="big-w temp_text"></p> 
									</td>
									<td class="amttable">
										<p class="big-w temp_text"></p>
									</td>
									<td class="amttable">
										<p class="big-w temp_text"></p>
									</td>
									<td class="amttable">
										<p class="big-w temp_text"></p>
									</td>
								</tr>
							</tbody>
						</table>
                    </div>
					<div class="row" style="padding-top:3.5%; border-top:1px solid #595959;">
						<div class="col s12 m12 l12">
							<div class="col s7 m7 l7"></div>
							<div class="col s5 m5 l5">
								<p style="float:left;" class="big-w temp_text"><b>SUBTOTAL</b></p><p style="float:right; text-align:right;" class="big-w temp_text">INR 1607000</p>
								</br><p style="float:left;" class="big-w temp_text"><b>DISCOUNT</b></p><p style="float:right; text-align:right;" class="big-w temp_text">INR 7000</p>
							</div>
						</div>
					</div>
					<div class="row" style="padding-top:1%;">
						<div class="col s12 m12 l12">
							<div class="col s4 m4 l4" style="margin-left:-10px;">
								<div class="bank_info">
								<p class="big-w temp_text">BANK NAME: ABCD BANK LIMITED</p>
								<p class="big-w temp_text">ACCOUNT NUMBER: 10000XXX12012</p>
								<p class="big-w temp_text">BRANCH NAME: BANDRA EAST</p>
								<p class="big-w temp_text">IFSC CODE: ABCD00XX</p>
								</div>
							</div>
							<div class="col s4 m4 l3">
								<div style="margin-bottom:10px;">
									<p style="margin-bottom:10px;" class="big-w temp_text"><b>TERMS OF PAYMENT</b></p>
									<p class="small-w temp_text">30</p>
								</div>
								<div>
									<p style="margin-bottom:10px;" class="big-w temp_text"><b>PLACE OF SUPPLY</b></p>
									<p class="small-w temp_text">MAHARASHTRA</p>
								</div>
							</div>
							<div class="col s5 m5 l5 total_box">
								<div class="row" style="margin-bottom:5px;">
									<p style="margin-bottom:10px; color:#fff; float:left;" class="big-w temp_text"><b>GRAND TOTAL</b></p><p style="float:right; color:#fff; text-align:right;" class="big-w temp_text">INR 1600000</p>
								</div>
								<div class="row" style="margin-bottom:5px;">
									<p style="color:#fff; float:left;" class="small-w temp_text">IN WORDS: RUPEES SIXTEEN LAKH ONLY</p>
								</div>
							</div>
						</div>
					</div>
					
					<div class="row" style="margin-top:-1%;">
						<div class="col s12 m12 l12">
							<div class="col s4 m4 l4" style="margin-left:-10px;">
								<p class="big-w temp_text cinno">CIN: XEBRAAXB2008123</p>
								<p class="big-w temp_text pan-1">PAN: XEBRA1234R</p>
								<p class="big-w temp_text gst_1">GSTIN: 27XEBRA1234RZXZ</p>
							</div>
							<div class="col s4 m4 l3">
								<div>
									<p style="margin-bottom:10px;" class="big-w temp_text"><b>GSTIN</b></p>
									<p class="small-w temp_text">27XEBRA1234RZXZ</p>
								</div>
							</div>
							<div class="col s5 m5 l5" style="text-align:right;" id="sign_box">
								<div class="col s12 m12 l12" style="text-align:center;">
									<p class="temp_text" style="font-size:13px; padding-bottom:5px !important; padding-left:40px !important;"><b>FOR <?= strtoupper($company[0]->bus_company_name) ?></b></p>
								</div>			
								<div class="col s12 m12 l12" style="margin-right:-10px !important; height:55px;">
									<div class="col s2 m2 l1" style="margin-left:14px;"></div>
									<?php if($reg_data[0]->reg_degital_signature!=""){?>
									<div class="col s12 m12 l12" style="margin-left:-30px !important;">
										<img width="180" height="50" src="<?php echo base_url(); ?>public/upload/degital_signature/<?php echo $reg_data[0]->reg_id; ?>/<?php echo $reg_data[0]->reg_degital_signature; ?>" style="margin:0px;" class="signature_img" alt="signature">
									</div>
									<?php } ?>
								</div>
								<div class="col s12 m12 l12" style="text-align:center; padding:10px 0 0 50px; border-top:1px solid #ccc;">
									<p style="font-size:13px;"><label class="up_lbl1 temp_text"><b><?php echo strtoupper($reg_data[0]->reg_username) ?></b></label> - <label class="up_lbl1 temp_text"><b><?php echo strtoupper($reg_data[0]->reg_designation) ?></b></label></p>
								</div>
							</div>
						</div>
					</div>
					<div class="row" style="margin-top:5%;">
						<div class="col s12 m12 l12" style="border-bottom:1px solid #ccc;"></div>
						<div class="col s12 m12 l12" style=" margin-top:-18px !important;">
							<button class="make-payment temp_text" class="btn" style="font-size: 13px !important; padding:5px 20px; border-radius:5px; background-color:#000; color:#fff;" type="submit"> MAKE PAYMENT </button>
						</div>
					</div>
					<div class="row" style="margin-top:3%;">
						<div class="col s12 m12 l8" style="padding-top:4%;">
							<p class="big-w temp_text"><b> TERMS & CONDITIONS </b></p>
							<div class="col s12 m12 112" id="cond" style="margin:7% 0 0 0;">
							<div class="col s12 m12 l12">
								<ul class="term-list" style="margin-left:0px;">
									<li><p style="color:#000;" class="small-w temp_text">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p></li>
									<li><p style="color:#000;" class="small-w temp_text">Etiam et dolor ac dolor rutrum ultricies egestas consectetur mauris. Phasellus ut tincidunt dolor, a suscipit lorem.</p></li>
									<li><p style="color:#000;" class="small-w temp_text">Vestibulum ullamcorper nisl in libero condimentum blandit.</p></li>
									<li><p style="color:#000;" class="small-w temp_text"> Sed egestas odio sem, sit amet finibus mauris blandit quis. Donec vel facilisis nunc.</p></li>
									<li><p style="color:#000;" class="small-w temp_text">In ac eleifend urna. Aliquam euismod luctus tortor. Vestibulum sit amet facilisis odio.</p></li>
								</ul>
							</div> 
							</div>
						</div>
						<div class="col s12 m12 l4">
							<div class="col s12 m12 l12 telno border-bottom">
								<div class="col s12 m12 l10" style="text-align:right; padding:10px 0;">
									<p class="big-w footer_p"><b>Phone</b></p>
									<p class="small-w footer_p">880212XX12</p>
								</div>
								<div class="col s12 m12 l2" style="padding:10px 0;">
									<img width="50" height="45" src="<?php echo base_url(); ?>public/images/invoice-template/phone-b.png" alt="phone">
								</div>
							</div>
							
							<div class="col s12 m12 l12 email_id border-bottom">
							<div class="col s12 m12 l10" style="text-align:right; padding:10px 0;">
								<p class="big-w footer_p"><b>Email</b></p>
								<p class="small-w footer_p">businessabc@abc.com</p>
							</div>
							<div class="col s12 m12 l2" style="padding:10px 0;">
								<img width="50" height="45" src="<?php echo base_url(); ?>public/images/invoice-template/email-b.png" alt="email">
							</div>
							</div>
							
							<div class="col s12 m12 l12 addr border-bottom">
							<div class="col s12 m12 l10" style="text-align:right; padding:10px 0;">
								<p class="big-w footer_p"><b>Address</b></p>
								<p class="small-w footer_p">64/a, street point avenue,</br>middle road,city name</p>
							</div>
							<div class="col s12 m12 l2" style="padding:10px 0;">
								<img width="50" height="45" src="<?php echo base_url(); ?>public/images/invoice-template/add-b.png" alt="add">
							</div>
							</div>
							
							<div class="col s12 m12 l12 webst border-bottom">
							<div class="col s12 m12 l10" style="text-align:right; padding:10px 0;">
								<p class="big-w footer_p"><b>Web</b></p>
								<p class="small-w footer_p">www.yourwebsitename.com</p>
							</div>
							<div class="col s12 m12 l2" style="padding:10px 0;">
								<img width="50" height="45" src="<?php echo base_url(); ?>public/images/invoice-template/web-b.png" alt="phone">
							</div>
							</div>
						</div>
					</div>
					<footer style="margin:15px 0px; background-color:#000; padding-bottom:22px;">
						<div class="col s12 m12 l4 facebk">
							<div class="col s3 m3 l3" style="margin:-6px -7px;">
								<img width="50" height="45" src="<?php echo base_url(); ?>public/images/invoice-template/Facebook.png" alt="fb">
							</div>
							<div class="col s9 m9 l9">
								<p style="color:#fff; padding:8px 0;" class="small-w footer_p">http://facebook.com/abc</p>
							</div>
						</div>
						<div class="col s12 m12 l4 twitter">
							<div class="col s3 m3 l3" style="margin:-6px -7px;">
								<img width="50" height="45" src="<?php echo base_url(); ?>public/images/invoice-template/Twitter.png" alt="twit">
							</div>
							<div class="col s9 m9 l9">
								<p style="color:#fff; padding:8px 0;" class="small-w footer_p">http://twitter.com/abc</p>
							</div>
						</div>
						<div class="col s12 m12 l4 link_id">
							<div class="col s3 m3 l3" style="margin:-6px -7px;">
								<img width="50" height="45" src="<?php echo base_url(); ?>public/images/invoice-template/LinkedIn.png" alt="link">
							</div>
							<div class="col s9 m9 l9">
								<p style="color:#fff; padding:8px 0;" class="small-w footer_p">http://linkedin.com/abc</p>
							</div>
						</div>
					</footer>
				</div>
				</div>
			</div><!-- Default Template End -->
		
			<!-- First Template Start -->
			<div id="first-template" class="col s12 m12 l8 inv_tem" style="margin-bottom:20px; margin-left:-60px;" hidden>
			<div id="inv_tem" class="box-wrapper bg-white shadow border-radius-6" style="padding:0 25px;">
				<div class="row" style="margin:10px 0 0 0; padding:0 0 10px 0; border-bottom:3px solid #595959;">
					<div class="col s12 m12 l12" style="text-align:right; margin:0 0 0 20px;">
						<a position="top" style="padding-left:5px;" class="close-temp" style=""><img width="20" height="20" src="<?php echo base_url(); ?>asset/images/popupdelete1.png" alt="delete"></a>
					</div>
					<div class="col s12 m12 l3" style="text-align:left; padding:0px;">
						<img id="customize_logo_img" src="<?php echo base_url(); ?>public/images/xebra-logo.png" height="55" width="200" alt="eazyinvoices-logo">
					</div>
					<div class="col s12 m12 l3" style="">
						<div class="col s12 m12 l10" style="text-align:right; padding:10px 0;">
							<p class="big-w footer_p"><b>Phone</b></p>
							<p class="small-sw footer_p">880212XX12</p>
						</div>
						<div class="col s12 m12 l2" style="padding:10px 0;">
							<img width="50" height="45" src="<?php echo base_url(); ?>public/images/invoice-template/phone-b.png" alt="phone">
						</div>
					</div>
					<div class="col s12 m12 l3" style="">
						<div class="col s12 m12 l10" style="text-align:right; padding:10px 0;">
							<p class="big-w footer_p"><b>Email</b></p>
							<p class="small-sw footer_p">businessabc@abc.com</p>
						</div>
						<div class="col s12 m12 l2" style="padding:10px 0;">
							<img width="50" height="45" src="<?php echo base_url(); ?>public/images/invoice-template/email-b.png" alt="email">
						</div>
					</div>
					<div class="col s12 m12 l3" style="">
						<div class="col s12 m12 l10" style="text-align:right; padding:10px 0;">
							<p class="big-w footer_p"><b>Address</b></p>
							<p class="small-sw footer_p">64/a, street point avenue,</br>middle road,city name</p>
						</div>
						<div class="col s12 m12 l2" style="padding:10px 0;">
							<img width="50" height="45" src="<?php echo base_url(); ?>public/images/invoice-template/add-b.png" alt="add">
						</div>
					</div>
				</div>
				<div class="row">
				<div class="row" style="padding:15px 0; margin-bottom:5px !important; border-bottom:2px solid #595959;">
				<div class="col s3 m3 l3 temp_text" style="width:20%; padding:4.5% 0 4.5% 5px;"><b><p class="inv-title temp_text">SALES INVOICE</b></p></div>
				<div class="col s3 m3 l3" style="width:30%;">
					<p class="big-w eoe_div temp_text"><b> E & OE </b></p>
					<p class="big-w temp_text"><b>BILLED TO:</b></p>
					<p class="big-w temp_text"><b>NEW COMPANY LLP,</b></p>
					<p class="small-sw temp_text">1ST FLOOR, ABC CHAMBERS, M.G. ROAD,</p>
					<p class="small-sw temp_text">MALAD-WEST, MUMBAI, MAHARASHTRA</p>
					<p class="small-sw temp_text">INDIA - 400061</p>
				</div>
				<div class="col s3 m3 l3" style="padding:4% 0; width:25%;">
					<p class="big-w temp_text"><b>INVOICE NO: </b><label style="padding-top:5px;" class="small-w temp_text">INV1/2019-20</label></p>
					<p class="big-w temp_text"><b>DATE OF ISSUE: </b><label style="padding-top:5px;" class="small-w temp_text">15-08-2019</label></p>
				</div>
				<div class="col s3 m3 l3" style="padding:4% 0; width:25%;">
					<p class="pu_no big-w temp_text"><b>ESTIMATE/P.O. NO: </b><label style="padding-top:5px;" class="small-w temp_text">10/2019-20</label></p>
					<p class="pu_o_date big-w temp_text"><b>ESTIMATE/P.O. DATE: </b><label style="padding-top:5px;" class="small-w temp_text">10-05-2019</label></p>
				</div>
				<!--label class="up_lbl temp_text"><b>TERMS OF PAYMENT: </b></label><label class="txt_pnt temp_text">15</label-->
				</div>
				<div class="inner advance-pay1" id="scrollbar-restable" style="padding:0px 0px 0px 0px !important;">
                        <table id="default_2" class="table_1">
                           <thead>
                              <tr class="se_tem_tr" id="">
                                 <th class="" style="width:180px !important; padding-left:6px !important;">
                                    <p class="temp_text">PARTICULARS</p>
                                 </th>
                                 <th class="amttable">
                                    <p class="temp_text">SAC</p>
                                 </th>
                                 <th class="amttable">
                                    <p class="temp_text">QNTY</p>
                                 </th>
                                 <th class="amttable">
                                    <p class="temp_text" style="">RATE</br>(INR)</p>
                                 </th>
                                 <!--th class="amttable">
                                    <p class="big-w temp_text" style="">TAXABLE</br>(INR) </p> 
                                 </th-->
                                 <th class="amttable">
                                    <p class="temp_text">AMOUNT</br>(INR)</p>
                                 </th>
                              </tr>
							</thead>
							<tbody>
								<tr style="background-color:#eef2fe;">
									<td class="" style="width:180px !important;">
										<p class="big-w temp_text"><b>ITEM NAME 01</b></p>
										<p class="small-w temp_text">Post promotion for ABC CCC saving </br>account by XYZ.</p>
									</td>
									<td class="amttable">
										<p class="small-w temp_text">345100</p>
									</td>
									<td class="amttable">
										<p class="big-w temp_text">4</p>
									</td>
									<td class="amttable">
										<p class="big-w temp_text">400000</p>
									</td>
									<!--td class="amttable">
										<p class="big-w temp_text">1600000</p> 
									</td-->
									<td class="amttable">
										<p class="big-w temp_text">1600000</p>
									</td>
								</tr>
								<tr style="background-color:#ccc; height:55px;">
									<td class="" style="width:180px !important;">
										<p class="big-w temp_text"></p>
										<p class="small-w temp_text"></p>
									</td>
									<td class="amttable">
										<p class="small-w temp_text"></p>
									</td>
									<td class="amttable">
										<p class="big-w temp_text"></p>
									</td>
									<td class="amttable">
										<p class="big-w temp_text"></p>
									</td>
									<!--td class="amttable">
										<p class="big-w temp_text"></p> 
									</td-->
									<td class="amttable">
										<p class="big-w temp_text"></p>
									</td>
								</tr>
								<tr style="background-color:#eef2fe; height:55px;">
									<td class="" style="width:300px !important;">
										<p class="big-w temp_text"></p>
										<p class="small-w temp_text"></p>
									</td>
									<td class="amttable">
										<p class="small-w temp_text"></p>
									</td>
									<td class="amttable">
										<p class="big-w temp_text"></p>
									</td>
									<td class="amttable">
										<p class="big-w temp_text"></p>
									</td>
									<!--td class="amttable">
										<p class="big-w temp_text"></p> 
									</td-->
									<td class="amttable">
										<p class="big-w temp_text"></p>
									</td>
								</tr>
								<tr style="background-color:#ccc; height:55px;">
									<td class="" style="width:180px !important;">
										<p class="big-w temp_text"></p>
										<p class="small-w temp_text"></p>
									</td>
									<td class="amttable">
										<p class="small-w temp_text"></p>
									</td>
									<td class="amttable">
										<p class="big-w temp_text"></p>
									</td>
									<td class="amttable">
										<p class="big-w temp_text"></p>
									</td>
									<!--td class="amttable">
										<p class="big-w temp_text"></p> 
									</td-->
									<td class="amttable">
										<p class="big-w temp_text"></p>
									</td>
								</tr>
							</tbody>
						</table>
                    </div>
					<div class="row" style="padding:1% 0; margin:0 1px 0 0; background-color:#eef2fe;">
						<div class="col s12 m12 l12">
							<div class="col s7 m7 l7"></div>
							<div class="col s5 m5 l5">
								<p style="float:left;" class="big-w temp_text"><b>SUBTOTAL</b></p><p style="float:right; text-align:right;" class="big-w temp_text">INR 1607000</p>
								</br><p style="float:left;" class="big-w temp_text"><b>CGST (9%)</b></p><p style="float:right; text-align:right;" class="big-w temp_text">INR 4000</p>
								</br><p style="float:left;" class="big-w temp_text"><b>SGST (9%)</b></p><p style="float:right; text-align:right;" class="big-w temp_text">INR 4000</p>
								</br><p style="float:left;" class="big-w temp_text"><b>DISCOUNT</b></p><p style="float:right; text-align:right;" class="big-w temp_text">INR 7000</p>
							</div>
						</div>
					</div>
					<div class="row" style="padding-top:3%;">
						<div class="col s12 m12 l12">
							<div class="col s2 m2 l2" style="width:19%;">
								<div style="margin-bottom:10px;">
									<p style="margin-bottom:10px;" class="big-w temp_text"><b>MODE OF PAYMENT</b></p>
									<p class="small-w temp_text">INTERNET BANKING</p>
								</div>
							</div>
							<div class="col s3 m3 l3" style="width:18%;">
								<div>
									<p style="margin-bottom:10px;" class="big-w temp_text"><b>GSTIN</b></p>
									<p class="small-w temp_text">27XEBRA1234RZXZ</p>
								</div>
							</div>
							<div class="col s2 m2 l2" style="width:21%;">
								<div>
									<p style="margin-bottom:10px;" class="big-w temp_text"><b>PLACE OF SUPPLY</b></p>
									<p class="small-w temp_text">MAHARASHTRA</p>
								</div>
							</div>
							<div class="col s5 m5 l5 border-bottom" style="padding-bottom:2%;">
								<div class="row" style="margin-bottom:5px;">
									<p style="margin-bottom:10px; color:#000; float:left;" class="big-w temp_text"><b>GRAND TOTAL</b></p><p style="float:right; color:#000; text-align:right;" class="big-w temp_text">INR 1600000</p>
								</div>
								<div class="row" style="margin-bottom:5px;">
									<p style="color:#000; float:left;" class="small-w temp_text">IN WORDS: RUPEES SIXTEEN LAKH ONLY</p>
								</div>
							</div>
						</div>
					</div>
					
					<div class="row" style="margin-top:5%;">
						<div class="col s12 m12 l12" style=" margin:10px 0 !important;">
							<button class="make-payment temp_text" class="btn" style="font-size: 13px !important; padding:5px 20px; border-radius:5px; background-color:#000; color:#fff;" type="submit"> MAKE PAYMENT </button>
						</div>
					</div>
					
					<div class="row" style="padding-top:1%;">
						<div class="col s12 m12 l12">
							<div class="col s4 m4 l4" style="margin-left:-10px;">
								<div class="bank_info">
								<p class="big-w temp_text">BANK NAME: ABCD BANK LIMITED</p>
								<p class="big-w temp_text">ACCOUNT NUMBER: 10000XXX12012</p>
								<p class="big-w temp_text">BRANCH NAME: BANDRA EAST</p>
								<p class="big-w temp_text">IFSC CODE: ABCD00XX</p>
								</div>
							</div>
							<div class="col s4 m4 l4" style="margin-left:-10px;">
								<p class="big-w temp_text cinno">CIN: XEBRAAXB2008123</p>
								<p class="big-w temp_text pan-1">PAN: XEBRA1234R</p>
								<p class="big-w temp_text gst_1">GSTIN: 27XEBRA1234RZXZ</p>
							</div>
							<div class="col s4 m4 l4" style="text-align:right;" id="sign_box">
								<div class="col s12 m12 l12" style="text-align:center;">
									<p class="temp_text" style="font-size:13px; padding-bottom:5px !important; padding-left:40px !important;"><b>FOR <?= strtoupper($company[0]->bus_company_name) ?></b></p>
								</div>			
								<div class="col s12 m12 l12" style="margin-right:-10px !important; height:55px;">
									<div class="col s2 m2 l1" style="margin-left:14px;"></div>
									<?php if($reg_data[0]->reg_degital_signature!=""){?>
									<div class="col s12 m12 l12" style="margin-left:10px !important;">
										<img width="180" height="50" src="<?php echo base_url(); ?>public/upload/degital_signature/<?php echo $reg_data[0]->reg_id; ?>/<?php echo $reg_data[0]->reg_degital_signature; ?>" style="margin:0px;" class="signature_img" alt="signature">
									</div>
									<?php } ?>
								</div>
								<div class="col s12 m12 l12" style="text-align:center; padding:10px 0 0 50px; border-top:1px solid #ccc;">
									<p style="font-size:13px;"><label class="up_lbl1 temp_text"><b><?php echo strtoupper($reg_data[0]->reg_username) ?></b></label> - <label class="up_lbl1 temp_text"><b><?php echo strtoupper($reg_data[0]->reg_designation) ?></b></label></p>
								</div>
							</div>
						</div>
					</div>

					<div class="row" style="background-color:#000 !important; padding:2% 0;">
						<div class="col s12 m12 l8">
							<p style="color:#fff;" class="big-w temp_text"><b> TERMS & CONDITIONS </b></p>
							<div class="col s12 m12 112 cond" style="margin:2% 0 0 0;">
							<div class="col s12 m12 l12">
								<ul class="term-list-b" style="margin-left:0px;">
									<li><p style="color:#fff;" class="small-w temp_text">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p></li>
									<li><p style="color:#fff;" class="small-w temp_text">Etiam et dolor ac dolor rutrum ultricies egestas consectetur mauris. Phasellus ut tincidunt dolor, a suscipit lorem.</p></li>
									<li><p style="color:#fff;" class="small-w temp_text">Vestibulum ullamcorper nisl in libero condimentum blandit.</p></li>
									<li><p style="color:#fff;" class="small-w temp_text"> Sed egestas odio sem, sit amet finibus mauris blandit quis. Donec vel facilisis nunc.</p></li>
									<li><p style="color:#fff;" class="small-w temp_text">In ac eleifend urna. Aliquam euismod luctus tortor. Vestibulum sit amet facilisis odio.</p></li>
								</ul>
							</div> 
							</div>
						</div>
						<div class="col s12 m12 l4" style="padding-top:30px;">
							<div class="col s12 m12 l12 facebk border-bottom">
								<div class="col s12 m12 l2" style="padding:4px 0 0 0;">
									<img width="50" height="45" src="<?php echo base_url(); ?>public/images/invoice-template/Facebook.png" alt="add">
								</div>
								<div class="col s12 m12 l10" style="padding:18px 0;">
									<p style="color:#fff;" class="small-w footer_p">http://facebook.com/abc</p>
								</div>
							</div>
							
							<div class="col s12 m12 l12 twitter border-bottom">
								<div class="col s12 m12 l2" style="padding:4px 0 0 0;">
									<img width="50" height="45" src="<?php echo base_url(); ?>public/images/invoice-template/Twitter.png" alt="add">
								</div>
								<div class="col s12 m12 l10" style="padding:18px 0;">
									<p style="color:#fff;" class="small-w footer_p">http://twitter.com/abc</p>
								</div>
							</div>
							
							<div class="col s12 m12 l12 link_id border-bottom">
								<div class="col s12 m12 l2" style="padding:4px 0 0 0;">
									<img width="50" height="45" src="<?php echo base_url(); ?>public/images/invoice-template/LinkedIn.png" alt="add">
								</div>
								<div class="col s12 m12 l10" style="padding:18px 0;">
									<p style="color:#fff;" class="small-w footer_p">http://linkedin.com/abc</p>
								</div>
							</div>
							
							<div class="col s12 m12 l12 webst border-bottom">
								<div class="col s12 m12 l2" style="padding:4px 0 0 0;">
									<img width="50" height="45" src="<?php echo base_url(); ?>public/images/invoice-template/web.png" alt="add">
								</div>
								<div class="col s12 m12 l10" style="padding:18px 0;">
									<p style="color:#fff;" class="small-w footer_p">www.yourwebsitename.com</p>
								</div>
							</div>
						</div>
					</div>
				</div>
				</div>
			</div>
			<!-- First Template END -->
		
			<!-- Second Template END -->
			<div id="second-template" class="col s12 m12 l8" style="margin-bottom:20px; margin-left:-60px;" hidden>
			<a style="padding-left:25px; margin-top:10px;" class="close-temp" style=""><img width="20" height="20" src="<?php echo base_url(); ?>asset/images/popupdelete1.png" alt="delete"></a>
			<head>
			<style>
				.white-text{
					color:#fff !important;
				}
				
				.small-text{
					color:#000 !important;
					font-size: 13px;
				}   
				.medium-text {
					color:#000 !important;
					font-size: 15px;
				}
				.top-hr{
					margin: 15px 0px 0px -70px;
					height: 8px;
					background-color:#9c27b0;
				}
				
				.purple{
					font-size:13px !important;
				}
				
				#second-template p{
					font-size:13px;
					color:#000 !important;
				}
				
				.cust-name{
					font-size:15px;
				}
				
				.cust-details{
					font-size:13px !important;
				}
				
				.inv-de{
					padding:5px !important;
					margin-bottom:5px !important;
					color:#fff !important;
				}
				
				.inv-info{
					padding:5px !important;
					margin-bottom:5px !important;
					color:#000 !important;
					font-size:13px !important;
				}
				
				#sec-table thead  th{
					background-color:#7979d2 !important; 
					padding:20px !important;
				}
				
				.purple{
					background-color:#7979d2 !important; 
				}
				
				.tot-inr{
					padding:20px !important;
				}
				
				.total-word{
					font-size:15px !important;
				}
			</style>
			</head>
			<body class="container">
			<div class="row" style="margin-bottom:0px !important;">
				<div class="col l4">
					<img src="<?php echo base_url(); ?>public/images/xebra-logo.png" height="40" width="200" alt="eazyinvoices-logo">
				</div>
				<div class="col l8"><hr class="top-hr"></hr></div>
			</div>	
			<div class="row">
            <div class="col l6" style="margin-left:10px;">
                <p style="margin-bottom:-20px !important; margin-top: 20px;">
                    <span class="purple white-text" style="padding-left: 2px;padding-right: 2px;">INVOICE</span><span class="to"> TO <span><br/>
                    <span class="cust-name"><b>Suhas Sawant</b><span> <br/>
                    <span class="cust-com medium-text">Manager, Windchimes Communications Pvt. Ltd.</span>
                </p></br>
                <br/>
                <p class="cust-details small-text">
                    <b>PHONE:</b> +91 9819184721, +91 2226470454   <br/>
                    <b>EMAIL</b> suhas@windchimes.co.in   <br/>
                    <b>WEBSITE</b> www.windchimes.co.in   <br/>
                    <b>ADDRESS:</b> 402, Capri Building, A.K. Marg, Near Bandra Court, <br>    Bandra East, Mumbai - 400051. <br/>
                    <b>GSTIN:</b> 345678 <br/>
                    <b>PAN:</b> 901234 <br/>
                    <b>PLACE OF SUPPLY:</b> DADAR 
                </p>
            </div>
            <div class="col l4 right">
                <h3 class="black-text" style="margin-bottom: 10px; margin-left: 8px;"> INVOICE </h3>
                <div class="col white-text">
                    <p>
                        <span class="purple inv-de">INVOICE DATE</span>  <br/></br>
                        <span class="purple inv-de">P.O. NO.</span>      <br/><br/>
                        <span class="purple inv-de">P.O. DATE</span>     <br/><br/>
                        <span class="purple inv-de">GSTIN</span>         
                    </p>
                    <p style="margin-top:25px;">
                        <span class="inv-de black">DUE DATE</span>
                    </p>
                </div>
                <div class="col">
                    <p>
                        <span class="inv-info">: March 10,2018</span> <br/><br/>
                        <span class="inv-info">: 123456789 </span>    <br/><br/>
                        <span class="inv-info">: March 10, 2018</span>  <br/><br/>
                        <span class="inv-info">: 1234567890</span>      
                    </p>
                    <p style="margin-top:25px;">
                        <span class="inv-info">: April 10, 2018</span>
                    </p>
                </div>
                <div class="col"></div>
            </div>
			</div>
			<div class="row">
            <table id="sec-table" class="centered striped">
                <thead class="white-text">
                    <tr>
                        <th style="text-align: left; padding-left:5%; ">ITEM NAME & DESCRIPTION</th>
                        <th>SAC</th>
                        <th>QUANTITY</th>
                        <th>RATE</th>
                        <th>DISCOUNT</th>
                        <th>TOTAL</th>
                    </tr>
                </thead>
                <tr>
                    <td style="text-align: left; padding-left: 5%;">
                        <p><span class="purple white-text" style="padding: 2px;">Web</span> Design <br/>
                            <span class="small-text">This is a sample letter that has been placed <br/> 
                                to demonstrate the typing format</span>
                        </p>
                    </td>
                    <td>342</td>
                    <td>3</td>
                    <td>INR 653.00</td>
                    <td>15%</td>
                    <td>INR 1959.00</td>
                </tr>
                <tr>
                    <td style="text-align: left; padding-left: 5%;">
                        <p><span class="purple white-text" style="padding: 2px;">Typography</span> Idea <br/>
                            <span class="small-text">This is a sample letter that has been placed <br/> 
                                to demonstrate the typing format</span>
                        </p>
                    </td>
                    <td>658</td>
                    <td>4</td>
                    <td>INR 125.00</td>
                    <td>30%</td>
                    <td>INR 500</td>
                </tr>
            </table>
            <hr class="purple" style="height: 2px;"/>
			</div>
			<div class="row">
            <div class="col l2 right" style="margin-left: 5px;">
                    <span style="color:#7979d2 !important;">INR 4429.00 </span><br/>
                    <p class="medium-text">
                        INR 664.35 <br/> INR 664.35 <br/>
                        INR 250    <br/> INR 200  <br/>
                    </p>
                </div>
                <div class="col l2 right" style="text-align: right; margin-right: 5px;">
                    <span style="color:#7979d2 !important; ">SUB TOTAL</span> <br/>
                    <p class="medium-text">
                        +SGST(09%) <br/> +CGST(09%) <br/> 
                        +CESS <br/> +OTHERS <br/>
                    </P>
                </div>
			</div>
			<div class="row">
				<div class="col right" style="margin-right: 70px; margin-top: -25px; padding:10px;">
                    <p class="right tot-inr card-panel purple white-text" style="color: #fff !important;">TOTAL DUE: <span class="white-text tot-inr" style="margin-left: 25px; color:#fff !important;">INR 5757.7</span></p> <br/>
					<P class="right total-word">FIVE THOUSAND SEVEN HUNDRED AND FIFTY SEVEN POINT SEVEN</P>
                </div>
            </div>
			<div class="row">
                <div class="col l4">
                    <p><span class="purple white-text" style="padding: 8px; margin-bottom:10px;"> Payment </span> </br></br>
                        <span class="small-text grey-text" style="margin-top:10px !important; font-size:13px !important;">
                            <b>PAYMENT METHOD:</b> Bank Transfer <br/>
                            <b>A/C NUMBER:</b> 23568974123568 <br/>
                            <b>BANK NAME:</b> HDFC Bank   <br/>
                            <b>IFSC CODE:</b> ICIC0012354 </br>
                        </span>
                    </p>
                    <button class = "btn purple white-text" style="margin-top:10px; font-size: 20px;" type = "submit"> MAKE PAYMENT </button>
                </div>
            <div class="col l4 right" style="margin-right: 70px;">
                <br/><br/>
                    <p style="text-align: right; font-size:13px !important;">Mr. Amit Shah <br/>  <span class="small-text">Account Manager</span> </p>
                </div>
            </div>
			<div class="row" style="border-top:1px solid #000 !important; margin-top:20px;">
                <br/>
                <p class="medium-text"><b>TERMS & CONDITIONS</b> <br/>
                    <span style="font-size:13px !important;" class="small-text">Payment Should be made within 30 days by cheque, <i>Paypal</i> or bank transfer.</span>
                </p>
            </div>
            <!--div class="row">
                <p class="small-text col l1">
                    FACEBOOK    <br/>
                    WEBSITE     <br/> 
                    TWITTER     <br/>
                    BLOG        <br/>
                </p>
                <p class="small-text col l4">
                    : https://facebook.com/ABCFINANCE           <br/> 
                    : https://ABCFINANCE                        <br/>
                    : https://twitter.com/ABCFINANCE            <br/>
                    : https://in.linkedin.com/in/ABCFINANCE     <br/>
                </p>
                <p class="col l4">
                    <span class="medium-text">
                        Address:
                    </span> <br/>
                    ABC Finance Private Limited. <br/>
                    110/112, B Wing, Kalpataru Enclave, S.V. Road,  <br/>
                    Santacruz - west    <br/>
                    TEL NO. :9769976482 <br/>
                    EMAIL ID :suhas@windchimes.co.in <br/>
                    
                </p>
            </div-->
		</body>
		</div>
		<!-- Second Template END -->
		
		<!-- Third Template Start -->
		<div id="third-template" class="col s12 m12 l8 inv_tem" style="margin-bottom:20px; margin-left:-60px;" hidden>
			<div id="inv_tem" class="box-wrapper bg-white shadow border-radius-6" style="padding:0 25px;">
				<div class="row" style="margin:10px 0 0 0; padding:0 0 10px 0;">
					<div class="col s12 m12 l12" style="text-align:right; margin:0 0 0 20px;">
						<a position="top" style="padding-left:5px;" class="close-temp" style=""><img width="20" height="20" src="<?php echo base_url(); ?>asset/images/popupdelete1.png" alt="delete"></a>
					</div>
					<div class="col s12 m12 l4" style="text-align:left; padding:0px;">
						<img id="customize_logo_img" src="<?php echo base_url(); ?>public/images/xebra-logo.png" height="55" width="200" alt="eazyinvoices-logo">
					</div>
					<div class="col s12 m12 l8" style="background-color:#7687b8; height:61px;">
						
					</div>
				</div>
				<div class="row" style="margin-top:-10px;">
					<div class="col s12 m12 l4">
						<div class="col s6 m6 l6" style="padding:6.5% 0; width:50%; border-bottom:3px solid #99d5c9;">
							<p class="big-w temp_text"><b>INVOICE NO: </b></p>
							<p style="padding-top:5px;" class="small-w temp_text">INV1/2019-20</p>
						</div>
						<div class="col s6 m6 l6" style="padding:6.5% 0; width:50%; border-bottom:3px solid #7687b8;">
							<p class="big-w temp_text"><b>DATE OF ISSUE: </b></p>
							<p style="padding-top:5px;" class="small-w temp_text">15-08-2019</p>
						</div>
					</div>
					<div class="col s12 m12 l8" style="background-color:#7687b8; border-bottom:3px solid #99d5c9;">
						<div class="col s12 m12 l12 temp_text" style="text-align:center; padding:4.5% 0 4% 5px; color:#fff;"><b><p class="inv-title temp_text">SALES INVOICE</b></p></div>
					</div>
				</div>
				<div class="row" style="margin:10px 0 0 0; padding:0 0 10px 0;">
					<div class="col s12 m12 l4" style="">
						<div class="col s12 m12 l12" style="text-align:left; padding:10px 0;">
							<p class="small-w footer_p">64/a, street point avenue,</br>middle road,city name</p>
							<p class="small-w footer_p">880212XX12</p>
							<p class="small-w footer_p">businessabc@abc.com</p>
							<p style="color:#000;" class="small-w footer_p">www.yourwebsitename.com</p>
						</div>
					</div>
					<div class="col s12 m12 l4" style="padding:0 0;">
						<p class="big-w eoe_div temp_text"><b> E & OE </b></p>
						<p class="big-w temp_text"><b>BILLED TO:</b></p>
						<p class="big-w temp_text"><b>NEW COMPANY LLP,</b></p>
						<p class="small-sw temp_text">1ST FLOOR, ABC CHAMBERS, M.G. ROAD,</p>
						<p class="small-sw temp_text">MALAD-WEST, MUMBAI, MAHARASHTRA</p>
						<p class="small-sw temp_text">INDIA - 400061</p>
					</div>
					<div class="col s12 m12 l4" style="padding:0 0;">
						<p class="pu_no big-w temp_text"><b>ESTIMATE/P.O. NO: </b></p>
						<p style="padding-top:5px;" class="small-w temp_text">10/2019-20</p>
						</br>
						<p class="pu_o_date big-w temp_text"><b>ESTIMATE/P.O. DATE: </b></p>
						<p style="padding-top:5px;" class="small-w temp_text">10-05-2019</p>
					</div>
					<!--label class="up_lbl temp_text"><b>TERMS OF PAYMENT: </b></label><label class="txt_pnt temp_text">15</label-->
				</div>
				<div class="row">
				<div class="inner advance-pay1" id="scrollbar-restable" style="padding:0px 0px 0px 0px !important;">
                        <table id="default_3" class="">
                           <thead>
                              <tr class="se_tem_tr" id="">
                                 <th class="" style="width:160px !important; padding-left:6px !important; background-color:#99d5c9;">
                                    <p class="temp_text">PARTICULARS</p>
                                 </th>
                                 <th class="amttable" style="background-color:#7687b8;">
                                    <p class="temp_text">SAC</p>
                                 </th>
                                 <th class="amttable" style="background-color:#7687b8;">
                                    <p class="temp_text">QNTY</p>
                                 </th>
                                 <th class="amttable" style="background-color:#7687b8;">
                                    <p class="temp_text" style="">RATE</br>(INR)</p>
                                 </th>
                                 <!--th class="amttable">
                                    <p class="big-w temp_text" style="">TAXABLE</br>(INR) </p> 
                                 </th-->
                                 <th class="amttable" style="background-color:#7687b8;">
                                    <p class="temp_text">AMOUNT</br>(INR)</p>
                                 </th>
                              </tr>
							</thead>
							<tbody>
								<tr style="background-color:#eef2fe;">
									<td class="" style="width:273px !important; background-color:#fff;">
										<p class="big-w temp_text"><b>ITEM NAME 01</b></p>
										<p class="small-w temp_text">Post promotion for ABC CCC saving </br>account by XYZ.</p>
									</td>
									<td class="amttable">
										<p class="small-w temp_text">345100</p>
									</td>
									<td class="amttable">
										<p class="big-w temp_text">4</p>
									</td>
									<td class="amttable">
										<p class="big-w temp_text">400000</p>
									</td>
									<!--td class="amttable">
										<p class="big-w temp_text">1600000</p> 
									</td-->
									<td class="amttable">
										<p class="big-w temp_text">1600000</p>
									</td>
								</tr>
								<tr style="background-color:#eef2fe; height:55px;">
									<td class="" style="width:273px !important; background-color:#fff;">
										<p class="big-w temp_text"></p>
										<p class="small-w temp_text"></p>
									</td>
									<td class="amttable">
										<p class="small-w temp_text"></p>
									</td>
									<td class="amttable">
										<p class="big-w temp_text"></p>
									</td>
									<td class="amttable">
										<p class="big-w temp_text"></p>
									</td>
									<!--td class="amttable">
										<p class="big-w temp_text"></p> 
									</td-->
									<td class="amttable">
										<p class="big-w temp_text"></p>
									</td>
								</tr>
								<tr style="background-color:#eef2fe; height:55px;">
									<td class="" style="width:273px !important; background-color:#fff;">
										<p class="big-w temp_text"></p>
										<p class="small-w temp_text"></p>
									</td>
									<td class="amttable">
										<p class="small-w temp_text"></p>
									</td>
									<td class="amttable">
										<p class="big-w temp_text"></p>
									</td>
									<td class="amttable">
										<p class="big-w temp_text"></p>
									</td>
									<!--td class="amttable">
										<p class="big-w temp_text"></p> 
									</td-->
									<td class="amttable">
										<p class="big-w temp_text"></p>
									</td>
								</tr>
							</tbody>
						</table>
                    </div>
					<div class="row" style="padding:1% 0; margin:0 1px 0 0; background-color:#eef2fe;">
						<div class="col s12 m12 l12">
							<div class="col s4 m4 l4" style="">
								<p style="margin-bottom:10px;" class="big-w temp_text"><b>MODE OF PAYMENT</b> <label class="small-w temp_text">INTERNET BANKING</label></p>
								<p style="margin-bottom:10px;" class="big-w temp_text"><b>GSTIN</b> <label class="small-w temp_text">27XEBRA1234RZXZ</label></p>
								<p style="margin-bottom:10px;" class="big-w temp_text"><b>PLACE OF SUPPLY</b> <label class="small-w temp_text">MAHARASHTRA</label></p>
							</div>
							<div class="col s8 m8 l8">
								<div class="col s8 m8 l8" style="float:right;">
								<div>
								<p style="float:left;" class="big-w temp_text"><b>SUBTOTAL</b></p><p style="float:right; text-align:right;" class="big-w temp_text">INR 1607000</p>
								</div>
								<div style="border-bottom:1px solid #ccc;">
								</br><p style="float:left;" class="big-w temp_text"><b>CGST (9%)</b></p><p style="float:right; text-align:right;" class="big-w temp_text">INR 4000</p></br>
								</div>
								<div style="border-bottom:1px solid #ccc;">
								<p style="float:left;" class="big-w temp_text"><b>SGST (9%)</b></p><p style="float:right; text-align:right;" class="big-w temp_text">INR 4000</p></br>
								</div>
								<div style="border-bottom:1px solid #ccc;">
								<p style="float:left;" class="big-w temp_text"><b>DISCOUNT</b></p><p style="float:right; text-align:right;" class="big-w temp_text">INR 7000</p></br>
								</div>
								<div class="row" style="border-bottom:1px solid #ccc; margin-bottom:5px;">
								<p style="margin-bottom:10px; color:#000; float:left;" class="big-w temp_text"><b>GRAND TOTAL</b></p><p style="color:#000; text-align:right;" class="big-w temp_text">1600000</p>
								</div>
								<div class="row" style="margin-bottom:5px;">
								<p style="color:#000; font-size:14px;" class="small-w temp_text"><b>IN WORDS: RUPEES SIXTEEN LAKH ONLY</b></p>
								</div>
								</div>
							</div>
						</div>
					</div>
					
					<div class="row" style="margin-top:5%;">
						<div class="col s4 m4 l4" style="margin-left:0px;">
						<div class="bank_info">
							<p class="big-w temp_text">BANK NAME: ABCD BANK LIMITED</p>
							<p class="big-w temp_text">ACCOUNT NUMBER: 10000XXX12012</p>
							<p class="big-w temp_text">BRANCH NAME: BANDRA EAST</p>
							<p class="big-w temp_text">IFSC CODE: ABCD00XX</p>
						</div>
						</div>
						<div class="col s4 m4 l4" style="margin-left:0px;">
							<p class="big-w temp_text cinno">CIN: XEBRAAXB2008123</p>
							<p class="big-w temp_text pan-1">PAN: XEBRA1234R</p>
							<p class="big-w temp_text gst_1">GSTIN: 27XEBRA1234RZXZ</p>
						</div>
						<div class="col s4 m4 l4" style=" margin:10px 0 !important; text-align:center;">
							<button class="make-payment temp_text" class="btn" style="font-size: 13px !important; padding:5px 20px; border-radius:5px; background-color:#7687b8; color:#fff;" type="submit"> MAKE PAYMENT </button>
						</div>
					</div>
					
					<div class="row" style="padding-top:1%; margin-left:-12px;">
						<div class="col s12 m12 l12">
							<div class="col s12 m12 l8">
							<p style="color:#000;" class="big-w temp_text"><b> TERMS & CONDITIONS </b></p>
							<div class="col s12 m12 112 cond" style="margin:2% 0 0 0;">
							<div class="col s12 m12 l12">
								<ul class="term-list" style="margin-left:0px;">
									<li><p style="color:#000;" class="small-w temp_text">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p></li>
									<li><p style="color:#000;" class="small-w temp_text">Etiam et dolor ac dolor rutrum ultricies egestas consectetur mauris. Phasellus ut tincidunt dolor, a suscipit lorem.</p></li>
									<li><p style="color:#000;" class="small-w temp_text">Vestibulum ullamcorper nisl in libero condimentum blandit.</p></li>
									<li><p style="color:#000;" class="small-w temp_text"> Sed egestas odio sem, sit amet finibus mauris blandit quis. Donec vel facilisis nunc.</p></li>
									<li><p style="color:#000;" class="small-w temp_text">In ac eleifend urna. Aliquam euismod luctus tortor. Vestibulum sit amet facilisis odio.</p></li>
								</ul>
							  </div> 
							</div>
							</div>
							<div class="col s4 m4 l4" style="text-align:right; margin-top:3%;" id="sign_box">
								<div class="col s12 m12 l12" style="text-align:center;">
									<p class="temp_text" style="font-size:13px; padding-bottom:5px !important; padding-left:40px !important;"><b>FOR <?= strtoupper($company[0]->bus_company_name) ?></b></p>
								</div>			
								<div class="col s12 m12 l12" style="margin:12px 0 !important; height:55px;">
									<div class="col s2 m2 l1" style="margin-left:14px;"></div>
									<?php if($reg_data[0]->reg_degital_signature!=""){?>
									<div class="col s12 m12 l12" style="margin-left:10px !important;">
										<img width="180" height="50" src="<?php echo base_url(); ?>public/upload/degital_signature/<?php echo $reg_data[0]->reg_id; ?>/<?php echo $reg_data[0]->reg_degital_signature; ?>" style="margin:0px;" class="signature_img" alt="signature">
									</div>
									<?php } ?>
								</div>
								<div class="col s12 m12 l12" style="text-align:center; padding:10px 0 0 50px; border-top:1px solid #ccc;">
									<p style="font-size:13px;"><label class="up_lbl1 temp_text"><b><?php echo strtoupper($reg_data[0]->reg_username) ?></b></label> - <label class="up_lbl1 temp_text"><b><?php echo strtoupper($reg_data[0]->reg_designation) ?></b></label></p>
								</div>
							</div>
						</div>
					</div>

					<div class="row" style="background-color:#fff !important; padding:2% 0;">
						<div class="col s12 m12 l12">
							<div class="col s12 m12 l3" style="background-color:#eef2fe;">
								<div class="col s12 m12 l12" style="padding:37px 0;">
									
								</div>
							</div>
							<div class="col s12 m12 l3 facebk border-bottom" style="background-color:#99d5c9;">
								<div class="col s12 m12 l12" style="padding:1px 0; text-align:center;">
									<img style="margin-top:0px;" width="50" height="45" src="<?php echo base_url(); ?>public/images/invoice-template/Facebook.png" alt="fb">
									<p style="color:#fff;" class="small-w footer_p">http://facebook.com/abc</p>
								</div>
							</div>
							
							<div class="col s12 m12 l3 twitter border-bottom" style="background-color:#7687b8;">
								<div class="col s12 m12 l12" style="padding:1px 0; text-align:center;">
									<img style="margin-top:0px;" width="50" height="45" src="<?php echo base_url(); ?>public/images/invoice-template/Twitter.png" alt="twit">
									<p style="color:#fff;" class="small-w footer_p">http://twitter.com/abc</p>
								</div>
							</div>
							
							<div class="col s12 m12 l3 link_id border-bottom" style="background-color:#99d5c9;">
								<div class="col s12 m12 l12" style="padding:1px 0; text-align:center;">
									<img style="margin-top:0px;" width="50" height="45" src="<?php echo base_url(); ?>public/images/invoice-template/LinkedIn.png" alt="link">
									<p style="color:#fff;" class="small-w footer_p">http://linkedin.com/abc</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Third Template END -->
		
		<!-- Fourth Template Start -->
		<div id="fourth-template" class="col s12 m12 l8" style="margin-bottom:20px; margin-left:-60px;" hidden>
			<a style="padding-left:25px; margin-top:10px;" class="close-temp" style=""><img width="20" height="20" src="<?php echo base_url(); ?>asset/images/popupdelete1.png" alt="delete"></a>
			<style>
				#fourth-template p{
					margin-bottom: 0px;
					font-size: 13px;
				}
				.address{
					line-height: 1em;
				}
				#invoice{
					background: url(ColourBand.png) no-repeat center;
					background-size: cover;
					font-size: 24px;
					letter-spacing: 0.4em;
					font-weight: 7px;
				}
				#logo{
					margin-bottom: 80px;
				}
				.mx-auto{
					width:100% !important;
				}
				.mx-auto th{
					height:20px;
					background-color: rgb(117, 117, 117);
					color: white;
					padding: 13px 10px;
					font-weight: 500;
					font-size: 0.8em;
				}
				.mx-auto td{
					padding:13px 10px;
					border-bottom: 1px solid black;
					font-weight: 500;
					font-size: 0.8em;
				}
				#total-cost{
					padding-right:98px;
				}
				#total-cost p{
					padding:13px 0px;
					border-bottom: 1px solid black;
				}
				#item-desc-head{
					background: url(ColourBand.png) no-repeat center;
					background-size:cover;
					width:300px;
				}
				#sign{
					width:80%;
					text-align:right !important;
					margin-top:15% !important;
				}
				
				footer{
					margin-top: 50px;
					border-bottom: 10px solid #50e3c2;
				}
				.footer p{
					background-color: #dddee2;
					font-size: 13px;
					margin-bottom: 0px;
				}
				/*
				#social p, #company p{
					margin-bottom: 0px;
					padding: 0px 10px;;
				}*/
				
				.first-head{
					background-color:#dddee2 !important;
					color: #000 !important;
				}
				
				#item-desc-head{
					background-color:#ff6666 !important;
				}
				
				.bnk-de{
					font-size:13px !important;
				}
				
				.pr-3, .pr-5, .mb-0, .pb-2{
					font-size:13px !important;
				}
				
				#companydetails ,.inv_tail{
					text-align:right !important;
				}
			</style>
		</head>
		<body>
		<div class="container" style="width:790px">
		<div class="row">
			<div class="col s12 m12 l7">
				<p class="" id="logo">LOGO</p>
			</div>	
			<div class="col s12 m12 l5">
				<div id="companydetails" class="mb-5">
					<p class="address">405, Capri Building, A K Marg</p>
					<p class="address">Near Bandra Court, Bandra East,</p>
					<p class="address">Mumbai - 400051</p>
					<p class="py-1"><i class="fa fa-phone" aria-hidden="true"></i> +91 22 2647 0475</p>
					<p class="pb-1"><i class="fa fa-envelope" aria-hidden="true"></i> help@eazyinvoice.co.in</p>
					<p class="pb-1"><i class="fas fa-adjust"></i>www.eazyinvoice.co.in</p>
				</div>
			</div>	
		</div>
		<div class="row">
			<div class="col s12 m12 l7">
				<p class="font-weight-bold">To,</p>
                <p class="h4 mb-0"><b>SUHAS SAWANT</b></p>
                <p class="font-weight-bold"><b>Manager, Windchimes Communication Pvt. Ltd.</b></p>
                <p>A 02, Capri Building, A K Marg, </br>Near Bandra Court, Bandra East, Mumbai - 400051</p>
                <p>GSTIN: 9002929890</p>
                <p>PAN: 0390101HJS89</p>
                <p>PLACE OF SUPPLY: Maharashtra</p>
			</div>
			<div class="col s12 m12 l5 inv_tail">
				<div class="text-light p-1 pl-3 mb-4" id="invoice">
					<b>Invoice</b>
				</div>
				<div id="invoice-details" class="pl-3">
					<p><b>INVOICE NO.</b></p>
					<p class="h3" style="font-size:18px !important;"><b>#23698720</b></p>
				<div class="d-inline-block" style="text-align:left !important; margin-left:48%;">
					<p><b>INVOICE DATE</b><span style="margin-left:5px;"></span>: 30 April 2018</p>
					<p><b>ISSUE DATE</b><span style="margin-left:17px;"></span>: 2 April 2018</p>
					<p><b>P.O. NUMBER</b><span style="margin-left:10px;"></span>: 0598297392</p>
					<p><b>P.O. DATE</b><span style="margin-left:28px;"></span>: 25 April 2018</p>
					<p><b>GSTIN</b><span style="margin-left:46px;"></span>: 7292829291</p>
					<p><b>PAN</b><span style="margin-left:56px;"></span>: 8923929JH82H</p>
					<p><b>CIN</b><span style="margin-left:59px;"></span>: 823929</p>
				</div>
				</div>
		  </div>
		</div>
		<div class="row">
        <div class="col s12 m12 l12">
        <table class="mx-auto">
            <tr class="bg-light text-center">
                <th class="bg-light text-dark first-head" style="background-color:#dddee2">NO.</th>
                <th class="text-left" id="item-desc-head">ITEM DESCRIPTION</th>
				<th style="background-color:#00b300 !important">UNIT PRICE</th>
                <th class="text-dark" style="background-color:#dddee2 !important;">QNTY</th>
                <th style="background-color:#0080ff !important;">TOTAL</th>
            </tr>
            <tr class="text-center">
                <td>1</td>
                <td class="text-left">Lorem ipsum dolor sit amet consectetur </br>adipisicing elit. Modi</td>
				<td>200,000</td>
                <td>23</td>
                <td>150,000</td>
            </tr>
            <tr class="text-center">
                <td class="">1</td>
                <td class="text-left">Lorem ipsum dolor sit amet consectetur </br>adipisicing elit. Modi</td>
                <td>200,000</td>
                <td>23</td>
                <td>150,000</td>
            </tr>
            <tr class="text-center">
                <td class="">1</td>
                <td class="text-left">Lorem ipsum dolor sit amet consectetur </br>adipisicing elit. Modi</td>
                <td>200,000</td>
                <td>23</td>
                <td>150,000</td>
            </tr>
            <tr class="text-center">
                <td>1</td>
                <td class="text-left">Lorem ipsum dolor sit amet consectetur </br>adipisicing elit. Modi</td>
                <td>200,000</td>
                <td>23</td>
                <td>150,000</td>
            </tr>
            <tr class="text-center">
                <td>1</td>
                <td class="text-left">Lorem ipsum dolor sit amet consectetur </br>adipisicing elit. Modi</td>
                <td>200,000</td>
                <td>23</td>
                <td>150,000</td>
            </tr>
            <tr class="text-center">
                <td>1</td>
                <td class="text-left">Lorem ipsum dolor sit amet consectetur </br>adipisicing elit. Modi</td>
                <td>200,000</td>
                <td>23</td>
                <td>150,000</td>
            </tr>
            <tr class="text-center">
                <td>1</td>
                <td class="text-left">Lorem ipsum dolor sit amet consectetur </br>adipisicing elit. Modi</td>
                <td>200,000</td>
                <td>23</td>
                <td>150,000</td>
            </tr>
            <tr class="text-center">
                <td>1</td>
                <td class="text-left">Lorem ipsum dolor sit amet consectetur </br>adipisicing elit. Modi</td>
                <td>200,000</td>
                <td>23</td>
                <td>150,000</td>
            </tr>
        </table>
		</div>
		</div>
		<div class="row">
            <div class="col s12 m12 l6 pl-5" id="">
                
            </div>
            <div class="col s12 m12 l6 text-right" id="total-cost">
                <p class="pr-3"><span class="pr-5 text-left">SUB TOTAL:</span>INR 16,900</p>
                <p class="pr-3"><span class="pr-5">TAX: GST(15%)</span>INR 22,780</p>
                <p class="pr-3"><span class="pr-5">GRAND TOTAL:</span>INR 18,228.0</p>
            </div>
        </div>
        <div class="row">
            <div class="col s12 m12 l6 pl-5" id="payment">
                <a href="#" class="btn purple white-text btn-lg mb-2 mt-2">MAKE PAYMENT</a></br></br>
                <p class="bnk-de" id="ac-number">A/C NUMBER: 01992823738</p></br></br>
                <p class="bnk-de" id="bank-name">BANK NAME: ICICI Bank</p></br></br>
                <p class="bnk-de" id="branch">BRANCH: Bandra East</p></br></br>
                <p class="bnk-de" id="ifsc">IFSC CODE: ICCI202928</p></br></br>
            </div>
            <div class="col s12 m12 l6 pr-3 pt-2 d-flex justify-content-end">
                <div id="sign">
                <p class="h5 mb-0">Smako Atson</p>
                <p class="text-secondary h6 mb-0">Creative head</p>
                <p class="h1 pb-2">SIGN</p>
                </div>
            </div>
        </div>
        <div class="footer">
            <p>TERMS & CONDITIONS:</p> 
			<p>Payment should be made within 30 days of the invoice date and either made via bank transfer or cheque</p>
        </div>
        <footer class="container mt-0">
            <!--<div class="row py-4 bg-secondary">
                <div class="col-md-6 text-left" id="social">
                    <p>FACEBOOK: <span id="facebook">https://facebook.com/ABCFINANCE</span></p>
                    <p>WEBSITE:  <span id="website">https://facebook.com/ABCFINANCE</span></p>
                    <p>TWITTER:  <span id="twitter">https://facebook.com/ABCFINANCE</span></p>                       
                    <p>BLOG:   <span id="blog">https://facebook.com/ABCFINANCE</span></p>
                </div>
                <div class="col-md-6 text-right " id="company">
                    <p>COMPANY: <span id="comp">https://facebook.com/ABCFINANCE</span></p>
                    <p>ADDRESS:  <span id="address">https://facebook.com/ABCFINANCE</span></p>
                    <p>TEL NO.:  <span id="tel-no">https://facebook.com/ABCFINANCE</span></p>                       
                    <p>EMAIL ID:   <span id="emailid">https://facebook.com/ABCFINANCE</span></p>
                    
                </div>
            </div>-->
        </footer>
		</div>
		</body>
		</div>
		
		<!-- Fourth Template END -->
		
		
		<!-- Fifth Template Start -->
		<div id="fifth-template" class="col s12 m12 l8" style="margin-bottom:20px; margin-left:-60px;" hidden>
			<a style="padding-left:25px; margin-top:10px;" class="close-temp" style=""><img width="20" height="20" src="<?php echo base_url(); ?>asset/images/popupdelete1.png" alt="delete"></a>
		<head>	
		<style>
        /*.container{
            background: url(Temp-2-1.jpg) no-repeat center;
            background-size: cover;
            min-height: 1000px;
        }
        #fifth-template p{
            margin:0;
            line-height: 1em;
            font-size: 15px;
        }
        .invoice-dets{
            width: 280px;
            height:150px;
        }
        .invoice-dets > p{
            font-size: 14px !important;
        }
        #inv .pl-5{
            font-size: 50px !important;
            font-weight: 400;
            letter-spacing: 0.1em;
        }
        .sidebar{
            height:1000px; 
            position: relative;
        }
        .to{
            position:absolute;
            bottom:0;
        }
        #fifth-template #tem-fifth-table {
            margin-top: 250px !important;
			width:80% !important;
			margin-left:5% !important;
        }
        #fifth-template #tem-fifth-table td,th{
            max-width: 280px;
            min-width: 80px;
            padding: 13px 10px 13px 0;
        }
        #fifth-template #tem-fifth-table tr{
            border-bottom: 1px solid black;
        }
        #payment{
            line-height: 0.5em;
            font-size: 13px;
        }
        #gst{
            width:200px;
			background-color:#ffcc00 !important;
        }
        #gst p{
            margin-bottom: 0px !important;
            padding-bottom: 0px !important ;
            padding-right: 4px;
            
        }
        #total{
            width:250px;
            border-top:1px black solid;
            font-size: 18px;
        }*/

    </style>
		</head>
		<body>
    <div class="container" style="width:837px;">
        <div class="row">
            <div class="col s12 m12 l4 sidebar">
                <p class="pl-5" style="font-size:40px !important; margin-bottom:0px !important; margin-left:-4px;" id="inv">I N V O I C E</p>
                <div class="col s12 m12 l6 float-left pl-4 text-left invoice-det font-weight-bold">
                    <p>INVOICE NO.</p>
                    <p>INVOICE DATE</p>
                    <p>P.O. NUMBER</p>
                    <p>P.O. DATE</p>
                    <p>GSTIN</p>
                    <p>PAN</p>
					<p>CIN</p>
                </div>
                <div class="col s12 m12 l6 float-right text-right invoice-det-value font-weight-bold pr-3">
                        <p>89077853</p>
                        <p>07, April,2018</p>
                        <p>#13245</p>
                        <p>25 October,2018</p>
                        <p>112233255</p>
                        <p>898789JH89</p>
                        <p>8909898</p>
                </div>
                <!--div class="to pb-5">
                    <p class="m-1 font-weight-bold">INVOICE TO:</p>
                    <p class="font-weight-bold m-1">Suhas Sawant</p>
                    <p class="font-weight-bold m-1">Accounts Dept.</p>
                    <p class="m-1">Windchimes Communications</p>
                    <p class="m-1">Bandra East</p>
                    <p class="m-1">Mumbai-400051</p>
                </div-->
            </div>
			<div class="col s12 m12 l8" style="text-align:right;">
				LOGO HERE
			</div>
		</div>
		<div class="row">
            <div class="col s12 m12 l12">
                <table id="tem-fifth-table">
                    <tr class="text-center" style="border-bottom:none;">
                        <th class="text-left"><b>SL NO.</b></th>
						<th class="text-left"><b>ITEM DESCRIPTION</b></th>
                        <th><b>PRICE</b></th>
                        <th><b>QNTY</b></th>
                        <th><b>TOTAL</b></th>
                    </tr>
                    <tr class="text-center">
                        <td>1</td>
						<td class="text-left"><span class="font-weight-bold d-block heading">Website Designing</span><span class="desc">Lorem ipsum dolor sit amet consectetur</span></td>
                        <td>&#x20b9; 25,00,000</td>
                        <td>1</td>
                        <td>&#x20b9; 25,00,000</td>
                    </tr>
                    <tr class="text-center">
						<td>2</td>
                        <td class="text-left"><span class="font-weight-bold d-block heading">Website Designing</span><span class="desc">Lorem ipsum dolor sit amet consectetur</span></td>
                        <td>&#x20b9; 25,00,000</td>
                        <td>1</td>
                        <td>&#x20b9; 25,00,000</td>
                    </tr>
                    <tr class="text-center">
						<td>3</td>
                        <td class="text-left"><span class="font-weight-bold d-block heading">Website Designing</span><span class="desc">Lorem ipsum dolor sit amet consectetur</span></td>
                        <td>&#x20b9; 25,00,000</td>
                        <td>1</td>
                        <td>&#x20b9; 25,00,000</td>
                    </tr>
                    <tr class="text-center">
						<td>4</td>
                        <td class="text-left"><span class="font-weight-bold d-block heading">Website Designing</span><span class="desc">Lorem ipsum dolor sit amet consectetur</span></td>
                        <td>&#x20b9; 25,00,000</td>
                        <td>1</td>
                        <td>&#x20b9; 25,00,000</td>
                    </tr>
                    <tr class="text-center">
						<td>5</td>
                        <td class="text-left"><span class="font-weight-bold d-block heading">Website Designing</span><span class="desc">Lorem ipsum dolor sit amet consectetur</span></td>
                        <td>&#x20b9; 25,00,000</td>
                        <td>1</td>
                        <td>&#x20b9; 25,00,000</td>
                    </tr>
                </table>
			</div>
		</div>
        <div class="row pt-4">
			<div class="col s12 m12 l4">
				<div class="to pb-5">
                    <p class="m-1 font-weight-bold" style="color:#000 !important;"><b>INVOICE TO:</b></p>
                    <label style="color:#000 !important; font-size:13px !important;" class="font-weight-bold m-1">Suhas Sawant</label><br>
                    <label style="color:#000 !important; font-size:13px !important;" class="font-weight-bold m-1">Accounts Dept.</label><br>
                    <label style="color:#000 !important; font-size:13px !important;" class="m-1">Windchimes Communications</label><br>
                    <label style="color:#000 !important; font-size:13px !important;" class="m-1">Bandra East</label><br>
                    <label style="color:#000 !important; font-size:13px !important;" class="m-1">Mumbai-400051</label><br>
                </div>
			</div>
			<div class="col s12 m12 l4" id="payment">
				<a href="#" class="btn purple white-text btn-lg my-2" style="background-color:#dddee2">MAKE PAYMENT</a></br></br></br></br>
				<p id="ac-number"><b>A/C NUMBER:</b> 01992823738</p></br></br>
				<p id="bank-name"><b>BANK NAME:</b> ICICI Bank</p></br></br>
				<p id="branch"><b>BRANCH:</b> Bandra East</p></br></br>
				<p id="ifsc"><b>IFSC CODE:</b> ICCI202928</p></br></br>
			</div>
            <div class="col s12 m12 l4 text-right pr-0">
				<p class="font-weight-bold h6 text-right pr-3"><b>SUB TOTAL:</b> 1,250,000</p>
                <div class="pr-3 float-right text-right" id="gst">
					<p><b>+SGST(9%):</b> 112,500</p>
                    <p><b>+CGST(9%):</b> 112,500</p>
				</div>
                <div class="clearfix"></div>
				<div class="font-weight-bold float-right text-left pl-3 pt-2" id="total">
					<h6 class="h4"><b>TOTAL:</b> 1,475,000</h6>
				</div>
            </div>
       </div>
        
        <div class="footer pb-3">
            <p style="font-size:14px;"><b>TERMS & CONDITIONS:</b></p> 
			<p>Payment should be made within 30 days of the invoice date and either made via bank transfer or cheque</p>
        </div>
    </div>
	</body>
		</div>
		
		<!-- Fifth Template END -->		
		<div id="sixth-template" class="col s12 m12 l8 inv_tem" style="margin-bottom:20px; margin-left:-60px;" hidden>
			<div id="inv_tem" class="box-wrapper bg-white shadow border-radius-6" style="padding:0 25px;">
				<div class="row" style="margin:10px 0 0 0; padding:0 0 10px 0;">
					<div class="col s12 m12 l12" style="text-align:right; margin:0 0 0 20px;">
						<a position="top" style="padding-left:5px;" class="close-temp" style=""><img width="20" height="20" src="<?php echo base_url(); ?>asset/images/popupdelete1.png" alt="delete"></a>
					</div>
					<div class="col s12 m12 l3" style="text-align:left; padding:0px;">
						<img id="customize_logo_img" src="<?php echo base_url(); ?>public/images/xebra-logo.png" height="55" width="200" alt="eazyinvoices-logo">
					</div>
					<div class="col s12 m12 l3" style="">
						<div class="col s12 m12 l10" style="text-align:right; padding:10px 0;">
							<p class="big-w footer_p"><b>Phone</b></p>
							<p class="small-sw footer_p">880212XX12</p>
						</div>
						<div class="col s12 m12 l2" style="padding:10px 0;">
							<img width="50" height="45" src="<?php echo base_url(); ?>public/images/invoice-template/phone-b.png" alt="phone">
						</div>
					</div>
					<div class="col s12 m12 l3" style="">
						<div class="col s12 m12 l10" style="text-align:right; padding:10px 0;">
							<p class="big-w footer_p"><b>Email</b></p>
							<p class="small-sw footer_p">businessabc@abc.com</p>
						</div>
						<div class="col s12 m12 l2" style="padding:10px 0;">
							<img width="50" height="45" src="<?php echo base_url(); ?>public/images/invoice-template/email-b.png" alt="email">
						</div>
					</div>
					<div class="col s12 m12 l3" style="">
						<div class="col s12 m12 l10" style="text-align:right; padding:10px 0;">
							<p class="big-w footer_p"><b>Address</b></p>
							<p class="small-sw footer_p">64/a, street point avenue,</br>middle road,city name</p>
						</div>
						<div class="col s12 m12 l2" style="padding:10px 0;">
							<img width="50" height="45" src="<?php echo base_url(); ?>public/images/invoice-template/add-b.png" alt="add">
						</div>
					</div>
				</div>
				<div class="row">
				<div class="row" style="padding:15px 0 50px 0; margin-bottom:5px !important; background-color:#298048;">
				<div class="col s3 m3 l3 temp_text" style="color:#fff; width:20%; padding:4.5% 0 4.5% 5px;"><b><p class="inv-title temp_text white-color">SALES INVOICE</b></p></div>
				<div class="col s3 m3 l3" style="width:30%;">
					<p style="color:#fff;" class="white-color big-w eoe_div temp_text"><b> E & OE </b></p>
					<p style="color:#fff;" class="white-color big-w temp_text"><b>BILLED TO:</b></p>
					<p style="color:#fff;" class="white-color big-w temp_text"><b>NEW COMPANY LLP,</b></p>
					<p style="color:#fff;" class="white-color small-sw temp_text">1ST FLOOR, ABC CHAMBERS, M.G. ROAD,</p>
					<p  style="color:#fff;"class="white-color small-sw temp_text">MALAD-WEST, MUMBAI, MAHARASHTRA</p>
					<p  style="color:#fff;"class="white-color small-sw temp_text">INDIA - 400061</p>
				</div>
				<div class="col s3 m3 l3" style="padding:4% 0; width:25%;">
					<p style="color:#fff;" class="white-color big-w temp_text"><b>INVOICE NO: </b><label style="color:#fff; padding-top:5px;" class="white-color small-w temp_text">INV1/2019-20</label></p>
					<p style="color:#fff;" class="white-color big-w temp_text"><b>DATE OF ISSUE: </b><label style="color:#fff; padding-top:5px;" class="white-color small-w temp_text">15-08-2019</label></p>
				</div>
				<div class="col s3 m3 l3" style="padding:4% 0; width:25%;">
					<p style="color:#fff;" class="white-color pu_no big-w temp_text"><b>ESTIMATE/P.O. NO: </b><label style="color:#fff; padding-top:5px;" class="white-color small-w temp_text">10/2019-20</label></p>
					<p style="color:#fff;" class="pu_o_date big-w temp_text white-color"><b>ESTIMATE/P.O. DATE: </b><label style="color:#fff; padding-top:5px;" class="white-color small-w temp_text">10-05-2019</label></p>
				</div>
				<!--label class="up_lbl temp_text"><b>TERMS OF PAYMENT: </b></label><label class="txt_pnt temp_text">15</label-->
				</div>
				<div class="inner advance-pay1" id="scrollbar-restable" style="padding:0px 0px 0px 0px !important; width: calc(92% - 0px) !important; margin:-36px 3% 0 4%;">
                        <table id="default_1" class="table_1">
                           <thead>
                              <tr class="ls_tem_tr" id="" style="background-color:#abdea5;">
                                 <th class="" style="width:180px !important; padding-left:6px !important;">
                                    <p style="color:#000; class="temp_text">PARTICULARS</p>
                                 </th>
                                 <th class="amttable">
                                    <p style="color:#000; class="temp_text">SAC</p>
                                 </th>
                                 <th class="amttable">
                                    <p style="color:#000; class="temp_text">QNTY</p>
                                 </th>
                                 <th class="amttable">
                                    <p style="color:#000; class="temp_text" style="">RATE</br>(INR)</p>
                                 </th>
                                 <th class="amttable">
                                    <p style="color:#000; class="big-w temp_text" style="">CGST</br>(INR)(%)</p> 
                                 </th>
								 <th class="amttable">
                                    <p style="color:#000; class="big-w temp_text" style="">SGST</br>(INR)(%)</p> 
                                 </th>
                                 <th class="amttable">
                                    <p style="color:#000; class="temp_text">AMOUNT</br>(INR)</p>
                                 </th>
                              </tr>
							</thead>
							<tbody>
								<tr>
									<td class="" style="width:180px !important;">
										<p class="big-w temp_text"><b>ITEM NAME 01</b></p>
										<p class="small-w temp_text">Post promotion for ABC CCC saving </br>account by XYZ.</p>
									</td>
									<td class="amttable">
										<p class="small-w temp_text">345100</p>
									</td>
									<td class="amttable">
										<p class="big-w temp_text">4</p>
									</td>
									<td class="amttable">
										<p class="big-w temp_text">400000</p>
									</td>
									<td class="amttable">
										<p class="big-w temp_text">1600</p> 
									</td>
									<td class="amttable">
										<p class="big-w temp_text">1600</p> 
									</td>
									<td class="amttable">
										<p class="big-w temp_text">1600000</p>
									</td>
								</tr>
								<tr style="height:55px;">
									<td class="" style="width:180px !important;">
										<p class="big-w temp_text"></p>
										<p class="small-w temp_text"></p>
									</td>
									<td class="amttable">
										<p class="small-w temp_text"></p>
									</td>
									<td class="amttable">
										<p class="big-w temp_text"></p>
									</td>
									<td class="amttable">
										<p class="big-w temp_text"></p>
									</td>
									<td class="amttable">
										<p class="big-w temp_text"></p> 
									</td>
									<td class="amttable">
										<p class="big-w temp_text"></p> 
									</td>
									<td class="amttable">
										<p class="big-w temp_text"></p>
									</td>
								</tr>
								<tr style="height:55px;">
									<td class="" style="width:300px !important;">
										<p class="big-w temp_text"></p>
										<p class="small-w temp_text"></p>
									</td>
									<td class="amttable">
										<p class="small-w temp_text"></p>
									</td>
									<td class="amttable">
										<p class="big-w temp_text"></p>
									</td>
									<td class="amttable">
										<p class="big-w temp_text"></p>
									</td>
									<td class="amttable">
										<p class="big-w temp_text"></p> 
									</td>
									<td class="amttable">
										<p class="big-w temp_text"></p> 
									</td>
									<td class="amttable">
										<p class="big-w temp_text"></p>
									</td>
								</tr>
								<tr style="height:25px;">
									<td class="" style="width:180px !important;">
										<p class="big-w temp_text"></p>
										<p class="small-w temp_text"></p>
									</td>
									<td class="amttable">
										<p class="small-w temp_text"></p>
									</td>
									<td class="amttable">
										<p class="big-w temp_text"></p>
									</td>
									<td class="amttable">
										<p class="big-w temp_text"></p>
									</td>
									<td class="amttable">
										<p class="big-w temp_text"></p> 
									</td>
									<td class="amttable">
										<p class="big-w temp_text"></p> 
									</td>
									<td class="amttable">
										<p class="big-w temp_text"></p>
									</td>
								</tr>
							</tbody>
						</table>
                    </div>
					<div class="row" style="padding-top:3%; margin:0 1.5%;">
						<div class="col s12 m12 l12">
							<div class="col s2 m2 l2" style="width:19%;">
								<div style="margin-bottom:10px;">
									<p style="margin-bottom:10px;" class="big-w temp_text"><b>MODE OF PAYMENT</b></p>
									<p class="small-w temp_text">INTERNET BANKING</p>
								</div>
							</div>
							<div class="col s3 m3 l3" style="width:18%;">
								<div>
									<p style="margin-bottom:10px;" class="big-w temp_text"><b>GSTIN</b></p>
									<p class="small-w temp_text">27XEBRA1234RZXZ</p>
								</div>
							</div>
							<div class="col s2 m2 l2" style="width:21%;">
								<div>
									<p style="margin-bottom:10px;" class="big-w temp_text"><b>PLACE OF SUPPLY</b></p>
									<p class="small-w temp_text">MAHARASHTRA</p>
								</div>
							</div>
							<div class="col s5 m5 l5 border-bottom" style="padding-bottom:2%;">
								<p style="float:left;" class="big-w temp_text"><b>SUBTOTAL</b></p><p style="float:right; text-align:right;" class="big-w temp_text">INR 1607000</p>
								</br><p style="float:left;" class="big-w temp_text"><b>DISCOUNT</b></p><p style="float:right; text-align:right;" class="big-w temp_text">INR 7000</p>
								</br>
								<div class="row" style="margin-bottom:5px;">
									<p style="margin-bottom:10px; color:#000; float:left;" class="big-w temp_text"><b>GRAND TOTAL</b></p><p style="float:right; color:#000; text-align:right;" class="big-w temp_text">1600000</p>
								</div>
								<div class="row" style="margin-bottom:0px;">
									<p style="color:#000; float:left;" class="small-w temp_text">IN WORDS: RUPEES SIXTEEN LAKH ONLY</p>
								</div>
							</div>
						</div>
					</div>
					
					<div class="row" style="margin-top:2%; margin:2% 1.5% 0 1.5%;">
						<div class="col s12 m12 l12" style=" margin:10px 0 !important;">							 <div class="col s4 m4 l4" style="margin-left:-10px;">
							<div class="bank_info">
							<p class="big-w temp_text">BANK NAME: ABCD BANK LIMITED</p>
							<p class="big-w temp_text">ACCOUNT NUMBER: 10000XXX12012</p>
							<p class="big-w temp_text">BRANCH NAME: BANDRA EAST</p>
							<p class="big-w temp_text">IFSC CODE: ABCD00XX</p>
							</div>
						</div>
						<div class="col s4 m4 l4"></div>
						<div class="col s4 m4 l4" style="margin:20px 0; text-align:center;">
							<button class="make-payment temp_text" class="btn" style="font-size: 13px !important; padding:5px 20px; border-radius:5px; background-color:#000; color:#fff;" type="submit"> MAKE PAYMENT </button>
						</div>
						</div>
					</div>
					
					<div class="row" style="padding-top:1%; margin:0 1.5% 0 1.5%;"">
						<div class="col s12 m12 l12">
							<div class="col s4 m4 l4" style="margin-left:-10px;">
								<p class="big-w temp_text cinno">CIN: XEBRAAXB2008123</p>
								<p class="big-w temp_text pan-1">PAN: XEBRA1234R</p>
								<p class="big-w temp_text gst_1">GSTIN: 27XEBRA1234RZXZ</p>
							</div>
							<div class="col s4 m4 l4"></div>
							<div class="col s4 m4 l4" style="text-align:right;" id="sign_box">
								<div class="col s12 m12 l12" style="text-align:center;">
									<p class="temp_text" style="font-size:13px; padding-bottom:5px !important; padding-left:40px !important;"><b>FOR <?= strtoupper($company[0]->bus_company_name) ?></b></p>
								</div>			
								<div class="col s12 m12 l12" style="margin-right:-10px !important; height:55px;">
									<div class="col s2 m2 l1" style="margin-left:14px;"></div>
									<?php if($reg_data[0]->reg_degital_signature!=""){?>
									<div class="col s12 m12 l12" style="margin-left:10px !important;">
										<img width="180" height="50" src="<?php echo base_url(); ?>public/upload/degital_signature/<?php echo $reg_data[0]->reg_id; ?>/<?php echo $reg_data[0]->reg_degital_signature; ?>" style="margin:0px;" class="signature_img" alt="signature">
									</div>
									<?php } ?>
								</div>
								<div class="col s12 m12 l12" style="text-align:center; padding:10px 0 0 50px; border-top:1px solid #ccc;">
									<p style="font-size:13px;"><label class="up_lbl1 temp_text"><b><?php echo strtoupper($reg_data[0]->reg_username) ?></b></label> - <label class="up_lbl1 temp_text"><b><?php echo strtoupper($reg_data[0]->reg_designation) ?></b></label></p>
								</div>
							</div>
						</div>
					</div>

					<div class="row" style="padding:2% 0; margin:0 1.5% 0 1.5%;"">
						<div class="col s12 m12 l12">
							<p style="color:#000;" class="big-w temp_text"><b> TERMS & CONDITIONS </b></p>
							<div class="col s12 m12 112 cond" style="margin:2% 0 0 0;">
							<div class="col s12 m12 l12">
								<ul class="term-list" style="margin-left:0px;">
									<li><p style="color:#000;" class="small-w temp_text">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p></li>
									<li><p style="color:#000;" class="small-w temp_text">Etiam et dolor ac dolor rutrum ultricies egestas consectetur mauris. Phasellus ut tincidunt dolor, a suscipit lorem.</p></li>
									<li><p style="color:#000;" class="small-w temp_text">Vestibulum ullamcorper nisl in libero condimentum blandit.</p></li>
									<li><p style="color:#000;" class="small-w temp_text"> Sed egestas odio sem, sit amet finibus mauris blandit quis. Donec vel facilisis nunc.</p></li>
									<li><p style="color:#000;" class="small-w temp_text">In ac eleifend urna. Aliquam euismod luctus tortor. Vestibulum sit amet facilisis odio.</p></li>
								</ul>
							</div> 
							</div>
						</div>
					</div>
					<div class="row" style="margin-top:-1%; background-color:#298048;">
						<div class="col s12 m12 l12">
							<div class="col s12 m12 l3 facebk border-bottom">
								<div class="col s2 m2 l2" style="margin:3px 25px 0 -25px;">
									<img width="50" height="45" src="<?php echo base_url(); ?>public/images/invoice-template/Facebook.png" alt="fb">
								</div>
								<div class="col s10 m10 l10" style="padding:18px 0;">
									<p style="color:#fff;" class="small-w footer_p">http://facebook.com/abc</p>
								</div>
							</div>
							
							<div class="col s12 m12 l3 twitter border-bottom">
								<div class="col s2 m2 l2" style="margin:3px 25px 0 -25px;">
									<img width="50" height="45" src="<?php echo base_url(); ?>public/images/invoice-template/Twitter.png" alt="tw">
								</div>
								<div class="col s10 m10 l10" style="padding:18px 0;">
									<p style="color:#fff;" class="small-w footer_p">http://twitter.com/abc</p>
								</div>
							</div>
							
							<div class="col s12 m12 l3 link_id border-bottom">
								<div class="col s2 m2 l2" style="margin:3px 25px 0 -25px;">
									<img width="50" height="45" src="<?php echo base_url(); ?>public/images/invoice-template/LinkedIn.png" alt="ln">
								</div>
								<div class="col s10 m10 l10" style="padding:18px 0;">
									<p style="color:#fff;" class="small-w footer_p">http://linkedin.com/abc</p>
								</div>
							</div>
							
							<div class="col s12 m12 l3 webst border-bottom">
								<div class="col s2 m2 l2" style="margin:3px 25px 0 -25px;">
									<img width="50" height="45" src="<?php echo base_url(); ?>public/images/invoice-template/web.png" alt="web">
								</div>
								<div class="col s10 m10 l10" style="padding:18px 0;">
									<p style="color:#fff;" class="small-w footer_p">www.yourwebsitename.com</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- END -->
	</div>
	
	</div>
	
	<script type="text/javascript">
		//After click on template full page will display
		$("#first-template").hide();
		$("#layout_showbtn").click(function(){
			$("#template-gallary").show();
			$("#default-template").hide();
			$("#first-template").hide();
			$("#second-template").hide();
			$("#third-template").hide();
			$("#fourth-template").hide();
			$("#fifth-template").hide();
			$("#sixth-template").hide();
			$("#template_val").val(0);
		});
		
		
		$(".default").click(function(){
			$("#template-gallary").hide();
			$("#default-template").show();
			$("#first-template").hide();
			$("#second-template").hide();
			$("#third-template").hide();
			$("#fourth-template").hide();
			$("#fifth-template").hide();
			$("#sixth-template").hide();
			$("#template_val").val(0);
		});
		
		$(".first_template").click(function(){
			$("#template-gallary").hide();
			$("#default-template").hide();
			$("#first-template").show();
			$("#second-template").hide();
			$("#third-template").hide();
			$("#fourth-template").hide();
			$("#fifth-template").hide();
			$("#sixth-template").hide();
			$("#template_val").val(1);
		});
		
		$(".second_template").click(function(){
			$("#template-gallary").hide();
			$("#second-template").show();
			$("#default-template").hide();
			$("#first-template").hide();
			$("#third-template").hide();
			$("#fourth-template").hide();
			$("#fifth-template").hide();
			$("#sixth-template").hide();
			$("#template_val").val(2);
		});
		
		$(".third_template").click(function(){
			$("#template-gallary").hide();
			$("#third-template").show();
			$("#second-template").hide();
			$("#default-template").hide();
			$("#first-template").hide();
			$("#fourth-template").hide();
			$("#fifth-template").hide();
			$("#sixth-template").hide();
			$("#template_val").val(3);
		});
		
		$(".fourth_template").click(function(){
			$("#template-gallary").hide();
			$("#fourth-template").show();
			$("#third-template").hide();
			$("#second-template").hide();
			$("#default-template").hide();
			$("#first-template").hide();
			$("#fifth-template").hide();
			$("#sixth-template").hide();
			$("#template_val").val(4);
		});
		
		$(".fifth_template").click(function(){
			$("#template-gallary").hide();
			$("#fourth-template").hide();
			$("#third-template").hide();
			$("#second-template").hide();
			$("#default-template").hide();
			$("#first-template").hide();
			$("#fifth-template").show();
			$("#sixth-template").hide();
			$("#template_val").val(5);
		});
		
		$(".sixth_template").click(function(){
			$("#template-gallary").hide();
			$("#fourth-template").hide();
			$("#third-template").hide();
			$("#second-template").hide();
			$("#default-template").hide();
			$("#first-template").hide();
			$("#fifth-template").hide();
			$("#sixth-template").show();
			$("#template_val").val(6);
		});
		
		$(".close-temp").click(function(){
			$("#template-gallary").show();
			$("#default-template").hide();
			$("#fourth-template").hide();
			$("#third-template").hide();
			$("#second-template").hide();
			$("#first-template").hide();
			$("#fifth-template").hide();
			$("#sixth-template").hide();
			
		});
	
		$("#custom-foot").click(function(){
			var data_val = $(this).data("value");
			if(data_val=="3"){
				$("#foot_tab").click();
			}
		});
		
		$("#inv_custmise_fields, #est_custmise_fields, #pro_custmise_fields, #cust_fields").click(function(){
			var data_val = $(this).data("value");
			if(data_val=="2"){
				$("#cust_tab").click();
			}
		});
		
		$("#custom-all, #est_customise_btn").click(function(){
			var data_val = $(this).data("value");
			if(data_val=="1"){
				$("#cust_all").click();
			}
		});
		
		$("#est_ftr_customise").click(function(){
			var data_val = $(this).data("value");
			if(data_val=="3"){
				$("#foot_tab").click();
			}
		});
	</script>
	
	<script>		
	$(document).ready(function($) {
	
	$("#font_size").change(function() {
		var f_size = $("#font_size").val();
		$('.temp_text').css("font-size", f_size);	
	});	
	
	$("#ftr_font_size").change(function() {
		var footer_size = $("#ftr_font_size").val();
		$('.footer_p').css("font-size", footer_size);	
	});
	
	$(".inv_inp").hide();
	$("#set_invoice_no").change(function() {
		if(this.checked) {
			$(".inv_inp").show();
		}else{
			$(".inv_inp").hide();
		}
	});
	
	//Logo Image hide and show
	$("#customize_logo_img").hide();
	$(".customize_logo_img").hide();
	$("#hdr_logo").change(function() {
		if(this.checked) {
			$("#customize_logo_img").show().focus();
			$("#customize_logo_img").css("border", "2px solid #ffcc66");
			$(".customize_logo_img").show().focus();
			$(".customize_logo_img").css("border", "2px solid #ffcc66");
		}else{
			$("#customize_logo_img").hide();
			$(".customize_logo_img").hide();
		}
	});
	
	$("#eoe_div").hide();
	$(".eoe_div").hide();
	$("#eoe").change(function(){
		if(this.checked) {
			$("#eoe_div").show().focus();
			$("#eoe_div").css("border", "2px solid #ffcc66");
			$(".eoe_div").show();
			$(".eoe_div").css("border", "2px solid #ffcc66");
		}else{
			$("#eoe_div").hide();
			$(".eoe_div").hide();
		}
	});
	
	//Bank Deatils Hide and show
	$(".bank_info").hide();
	$("#bank_de").hide();
	$("#bank_de1").hide();
	$("#bank_details").change(function() {
		if(this.checked) {
			$(".bank_info").show().focus();
			$(".bank_info").css("border", "2px solid #ffcc66");
			$("#bank_de").show();
			$("#bank_de1").show();
			$("#bank_de").css("border", "2px solid #ffcc66");
			$("#bank_de1").css("border", "2px solid #ffcc66");
		}else{
			$(".bank_info").hide();
			$("#bank_de1").hide();
			$("#bank_de").hide();
		}
	});
	
	//Make Payment button hide and show
	$("#make-payment").hide();
	$(".make-payment").hide();
	$("#payment_btn").change(function() {
		if(this.checked){
			$("#make-payment").show().focus();
			$("#make-payment").css("border", "2px solid #ffcc66");
			$(".make-payment").show().focus();
			$(".make-payment").css("border", "2px solid #ffcc66");
		}else{
			$("#make-payment").hide();
			$(".make-payment").hide();
		}
	});
	
	//Pan No Hide and show.
	$("#pan").hide();
	$(".pan-1").hide();
	$("#pan_no").change(function() {
		if(this.checked){
			$("#pan").show().focus();
			$("#pan").css("border", "2px solid #ffcc66");
			$(".pan-1").show().focus();
			$(".pan-1").css("border", "2px solid #ffcc66");
		}else{
			$(".pan-1").hide();
		}
	});
	
	//CIN field hide and show
	$(".cinno").hide();
	$("#cin_no").change(function() {
		if(this.checked){
			$(".cinno").show().focus();
			$(".cinno").css("border", "2px solid #ffcc66");
		}else{
			$(".cinno").hide();
		}
	});
	
	//Terms_Condition
	$("#cond").hide();
	$(".cond").hide();
	$("#terms_condition").change(function() {
		if(this.checked){
			$("#cond").show().focus();
			$("#cond").css("border", "2px solid #ffcc66");
			$(".cond").show().focus();
			$(".cond").css("border", "2px solid #ffcc66");
		}else{
			$("#cond").hide();
			$(".cond").hide();
		}
	});
	
	//GST
	$("#gst_no").hide();
	$(".gst_1").hide();
	$("#gst").change(function() {
		if(this.checked){
			$("#gst_no").show().focus();
			$("#gst_no").css("border", "2px solid #ffcc66");
			$(".gst_1").show().focus();
			$(".gst_1").css("border", "2px solid #ffcc66");
		}else{
			$("#gst_no").hide();
			$(".gst_1").hide();
		}
	});
	
	//Purchase Order date of issue
	$(".pu_o_date").hide();
	$("#purchase_order_date").change(function() {
		if(this.checked){
			$(".pu_o_date").show().focus();
			$(".pu_o_date").css("border", "2px solid #ffcc66");
		}else{
			$(".pu_o_date").hide();
		}
	});
	
	//Purchase Order date of issue
	$(".pu_no").hide();
	$("#purchase_order").change(function() {
		if(this.checked){
			$(".pu_no").show().focus();
			$(".pu_no").css("border", "2px solid #ffcc66");
		}else{
			$(".pu_no").hide();
		}
	});
	
	//Company Address
	$(".addr").hide();
	$("#company_address").change(function() {
		if(this.checked){
			$(".addr").show().focus();
			$(".addr").css("border", "2px solid #ffcc66");
		}else{
			$(".addr").hide();
		}
	});

	//Company EmailId
	$(".email_id").hide();
	$("#company_email").change(function() {
		if(this.checked){
			$(".email_id").show().focus();
			$(".email_id").css("border", "2px solid #ffcc66");
		}else{
			$(".email_id").hide();
		}
	});

	//Company Contact No
	$(".telno").hide();
	$("#company_contact").change(function() {
		if(this.checked){
			$(".telno").show().focus();
			$(".telno").css("border", "2px solid #ffcc66");
		}else{
			$(".telno").hide();
		}
	});

	//Company Website
	$(".webst").hide();
	$("#company_website").change(function() {
		if(this.checked){
			$(".webst").show().focus();
			$(".webst").css("border", "2px solid #ffcc66");
		}else{
			$(".webst").hide();
		}
	});

	//Company facebook
	$(".facebk").hide();
	$("#fb_link").change(function() {
		if(this.checked){
			$(".facebk").show().focus();
			$(".facebk").css("border", "2px solid #ffcc66");
		}else{
			$(".facebk").hide();
		}
	});
	
	//Company twitter
	$(".twitter").hide();
	$("#twitter_link").change(function() {
		if(this.checked){
			$(".twitter").show().focus();
			$(".twitter").css("border", "2px solid #ffcc66");
		}else{
			$(".twitter").hide();
		}
	});

	//Company twitter
	$(".link_id").hide();
	$("#linked_link").change(function() {
		if(this.checked){
			$(".link_id").show().focus();
			$(".link_id").css("border", "2px solid #ffcc66");
		}else{
			$(".link_id").hide();
		}
	});

	//Signature Box
	$("#sign_box").hide();
	$(".sign_box").hide();
	$("#signature").change(function() {
		if(this.checked){
			$("#sign_box").show().focus();
			$("#sign_box").css("border", "2px solid #ffcc66");
			$(".sign_box").show().focus();
			$(".sign_box").css("border", "2px solid #ffcc66");
		}else{
			$("#sign_box").hide();
			$(".sign_box").hide();
		}
	});
	
  $('.tab_content').hide();
  $('.tab_content:first').show();
  $('.tabs li:first').addClass('active');
  $('.tabs li').click(function(event) {
    $('.tabs li').removeClass('active');
    $(this).addClass('active');
    $('.tab_content').hide();

    var selectTab = $(this).find('a').attr("href");

    $(selectTab).fadeIn();
  });
  
  ////////////////////////////////// data fetching for customisation///////////////////////////////////////////////////////////
  var prefix=$('#prefix_sales').data('prefix');
   renderData(prefix);
  
 
  
  $('#layout_btn').click(function(){
	if(prefix=="est"){
		var pref="EST"
	}
	if(prefix=="pro"){
		var pref="PRV"
	}
	if(prefix=="inv"){
		var pref="INV"
	}
	var bus_id=$('#'+prefix+'_bus_id').val();
	var reg_id=$('#'+prefix+'_reg_id').val();
	
	var inv_no= pref+""+$('#inv_invoice_no').val()+"/"+$('#inv_financial_yr').val();
	var customer_id=$('#'+prefix+'_customer_id').val();
	
	////////////////////////////////////////////////
	if($("#paper_size").is(":checked")){
	var paper_size=$("#paper_size").val();
    }
    if($("#letter").is(":checked")){
	var paper_size=$("#letter").val();
    }
	//var orientation=$("#orientation").val();
	var margins=$("#margins").val();
	var tmp_back_color=$("#tmp_back_color").val();
	var layout=$("#template_val").val();
	
	var checked=$("#set_for_all_tmp").is(":checked");

	if(checked){
		$("#set_for_all_tmp").val(1);
	}else{
		$("#set_for_all_tmp").val(0);
	}
	var set_for_all_tmp=$("#set_for_all_tmp").val();

	
	var checkedPref=$("#set_invoice_no").is(":checked");
	if(checkedPref){
		var set_inv=$("#inv_no_pre").val();
		var set_invoice_no=1;
	}else{
		var set_inv=pref;
		var set_invoice_no=0;
	}
	
	////////////////////////////////////////////////
	
	
	if(customer_id==""){
	Materialize.toast('Please select client first', 2000,'red rounded');
	return false;
	}else{

		 $.ajax({
							type: "POST",
							dataType: 'json',
							
							url: base_url+'sales/get_template_customisation',
							data:{
								'csrf_test_name':csrf_hash,
								"bus_id":bus_id,
							      "reg_id":reg_id,
								  "cust_id":customer_id,
								  "inv_invoice_no":inv_no,
								  "set_invoice_no":set_invoice_no,
								  "inv_no_pre":set_inv,
								   "inv_pref":set_inv,
								  "paper_size":paper_size,
								//  "inv_orientation":orientation,
                                  "inv_margins":margins,
                                  "tmp_back_color":tmp_back_color,
                                  "inv_layout":layout,
                  				  "set_for_all_tmp":set_for_all_tmp

								  
							
							},
							cache: false,
			                success:function(result)
							{
								if(result['customisation'][0].set_invoice_no==1){
                                 $("#set_invoice_no").prop("checked",true);
                                 $("#inv_no_pre").val(result['customisation'][0].inv_no_pre);
                                 $("#inv_letter").val(result['customisation'][0].inv_no_pre);
                                  //$("#invoice_letter").html(result['customisation'][0].inv_no_pre);
                                  if(result['customisation'][0].inv_no_pre!=""){
                                  $("#invoice_letter").html(result['customisation'][0].inv_no_pre);
                                 }else
                                 {
                                 	$("#invoice_letter").html(prefix);
                                 }
                                
                                 
								}else{
									 $("#set_invoice_no").prop("checked",false);
                                 $("#inv_no_pre").val(result['customisation'][0].inv_no_pre);
                                 $("#inv_letter").val(result['customisation'][0].inv_no_pre);
                                  //$("#invoice_letter").html(result['customisation'][0].inv_no_pre);
                                  if(result['customisation'][0].inv_no_pre!=""){
                                  $("#invoice_letter").html(result['customisation'][0].inv_no_pre);
                                 }else
                                 {
                                 	$("#invoice_letter").html(prefix);
                                 }
								}

								if(result['customisation'][0].paper_size=='A4'){
                                 $("#paper_size").prop("checked",true);
								}
								if(result['customisation'][0].paper_size=='letter'){
                                 $("#letter").prop("checked",true);
								}							

								$("#margins").val(result['customisation'][0].inv_margins);
								$("#tmp_back_color").val(result['customisation'][0].tmp_back_color);
								$("#tmp_hex_color").val(result['customisation'][0].tmp_back_color);
								
                                if(result['customisation'][0].inv_layout==1){
                                 $("#first-template").show();
								 $("#template-gallary").hide();
								}else if(result['customisation'][0].inv_layout==2){
                                  $("#second-template").show();
								  $("#template-gallary").hide();
								}else if(result['customisation'][0].inv_layout==3){
                                  $("#third-template").show();
								  $("#template-gallary").hide();
								}else if(result['customisation'][0].inv_layout==4){
                                  $("#fourth-template").show();
								  $("#template-gallary").hide();
								}else if(result['customisation'][0].inv_layout==5){
                                  $("#fifth-template").show();
								  $("#template-gallary").hide();
								}else if(result['customisation'][0].inv_layout==6){
                                  $("#sixth-template").show();
								  $("#template-gallary").hide();
								}else{
                                  $("#default-template").show();
								  $("#template-gallary").hide();
								}

									


								if(result['customisation'][0].set_for_all_tmp==1){
                                 $("#set_for_all_tmp").prop("checked",true);
								}
								Materialize.toast('Custom options saved successfully', 2000,'red rounded');

							},
						});
	}
});

  $('#content_btn').click(function(){
	if(prefix=="est"){
		var pref="EST"
	}
	if(prefix=="pro"){
		var pref="PRV"
	}
	if(prefix=="inv"){
		var pref="INV"
	}
	var bus_id=$('#'+prefix+'_bus_id').val();
	var reg_id=$('#'+prefix+'_reg_id').val();
	
	var inv_no= pref+""+$('#inv_invoice_no').val()+"/"+$('#inv_financial_yr').val();
	var customer_id=$('#'+prefix+'_customer_id').val();
	
	////////////////////////////////////////////////
	var hdr_logo_checked=$("#hdr_logo").is(":checked");
	if(hdr_logo_checked){
		$("#hdr_logo").val(1);
	}else{
		$("#hdr_logo").val(0);
	}
	var hdr_logo=$("#hdr_logo").val();
	var font_color=$("#font_color").val();
	var font_size=$("#font_size").val();
	/*var shipping_address_checked=$("#shipping_address").is(":checked");
	if(shipping_address_checked){
		$("#shipping_address").val(1);
	}else{
		$("#shipping_address").val(0);
	}
	var shipping_address=$("#shipping_address").val();*/
	/*var late_fees_checked=$("#late_fees").is(":checked");
	if(late_fees_checked){
		$("#late_fees").val(1);
	}else{
		$("#late_fees").val(0);
	}
	var late_fees=$("#late_fees").val();*/
	
	var bank_details_checked=$("#bank_details").is(":checked");
	if(bank_details_checked){
		$("#bank_details").val(1);
	}else{
		$("#bank_details").val(0);
	}
	var bank_details=$("#bank_details").val();
	var pan_no_checked=$("#pan_no").is(":checked");
	if(pan_no_checked){
		$("#pan_no").val(1);
	}else{
		$("#pan_no").val(0);
	}
	var pan_no=$("#pan_no").val();
	var gst_checked=$("#gst").is(":checked");
	if(gst_checked){
		$("#gst").val(1);
	}else{
		$("#gst").val(0);
	}
	var gst=$("#gst").val();
	var payment_btn_checked=$("#payment_btn").is(":checked");
	if(payment_btn_checked){
		$("#payment_btn").val(1);
	}else{
		$("#payment_btn").val(0);
	}
	var payment_btn=$("#payment_btn").val();
	var cin_no_checked=$("#cin_no").is(":checked");
	if(cin_no_checked){
		$("#cin_no").val(1);
	}else{
		$("#cin_no").val(0);
	}
	var cin_no=$("#cin_no").val();
	var purchase_order_date_checked=$("#purchase_order_date").is(":checked");
	if(purchase_order_date_checked){
		$("#purchase_order_date").val(1);
	}else{
		$("#purchase_order_date").val(0);
	}
	var purchase_order_date=$("#purchase_order_date").val();
	var purchase_order_checked=$("#purchase_order").is(":checked");
	if(purchase_order_checked){
		$("#purchase_order").val(1);
	}else{
		$("#purchase_order").val(0);
	}
	var purchase_order=$("#purchase_order").val();
	var terms_condition_checked=$("#terms_condition").is(":checked");
	if(terms_condition_checked){
		$("#terms_condition").val(1);
	}else{
		$("#terms_condition").val(0);
	}
     var terms_condition=$("#terms_condition").val();
	var signature_checked=$("#signature").is(":checked");
	if(signature_checked){
		$("#signature").val(1);
	}else{
		$("#signature").val(0);
	}
	var signature=$("#signature").val();
	var client_name=$("#client_name").val();
	var client_designation=$("#client_designation").val();
	
	
	var checked=$("#set_for_all_cont").is(":checked");

	if(checked){
		$("#set_for_all_cont").val(1);
	}else{
		$("#set_for_all_cont").val(0);
	}
	var set_for_all_cont=$("#set_for_all_cont").val();
	
	////////////////////////////////////////////////
	
	
	if(customer_id==""){
	Materialize.toast('Please select client first', 2000,'red rounded');
	return false;
	}else{
		
		 $.ajax({
							type: "POST",
							dataType: 'json',
							
							url: base_url+'sales/get_content_customisation',
							data:{
								'csrf_test_name':csrf_hash,
								"bus_id":bus_id,
							      "reg_id":reg_id,
								  "cust_id":customer_id,
								  "inv_invoice_no":inv_no,
								  "hdr_logo":hdr_logo,
								  "font_color":font_color,
                                  "font_size":font_size,
                                 // "shipping_address":shipping_address,
                                 // "late_fees":late_fees,
                  				  "bank_details":bank_details,
                  				  "pan_no":pan_no,
                                  "gst":gst,
                                  "payment_btn":payment_btn,
                                  "cin_no":cin_no,
                  				  "purchase_order_date":purchase_order_date,
                  				  "purchase_order":purchase_order,
                  				  "terms_condition":terms_condition,
                                  "inv_signature":signature,
                                  "client_name":client_name,
                                  "client_designation":client_designation,
                  				  "set_for_all_cont":set_for_all_cont

								  
							
							},
							cache: false,
			                success:function(result)
							{
								
								if(result['customisation'][0].hdr_logo==1){
                                 $("#hdr_logo").prop("checked",true);
								}
								$("#font_color").val(result['customisation'][0].font_color);
								$("#hex_color").val(result['customisation'][0].font_color);
								$("#font_size").val(result['customisation'][0].font_size);
								

								/*if(result['customisation'][0].shipping_address==1){
                                 $("#shipping_address").prop("checked",true);
								}

								if(result['customisation'][0].late_fees==1){
                                 $("#late_fees").prop("checked",true);
								}*/
								if(result['customisation'][0].bank_details==1){
                                 $("#bank_details").prop("checked",true);

                                 $("#"+prefix+"_bank_details").show();

								}else{
									$("#bank_details").prop("checked",false);
									 $("#"+prefix+"_bank_details").hide();
								}

								if(result['customisation'][0].pan_no==1){
                                 $("#pan_no").prop("checked",true);
                                 $("#"+prefix+"_pan_no").show();
								}
								if(result['customisation'][0].gst==1){
                                 $("#gst").prop("checked",true);
                                 $("#"+prefix+"_gst").show();
								}
								if(result['customisation'][0].payment_btn==1){
                                 $("#payment_btn").prop("checked",true);
								}
								if(result['customisation'][0].cin_no==1){
                                 $("#cin_no").prop("checked",true);
                                 $("#"+prefix+"_cin_no").show();
								}
								if(result['customisation'][0].purchase_order_date==1){
                                 $("#purchase_order_date").prop("checked",true);
                                 $("#"+prefix+"_reference").show();
								}
								if(result['customisation'][0].purchase_order==1){
                                 $("#purchase_order").prop("checked",true);
                                 $("#"+prefix+"_reference_ref_date").show();
								}
								if(result['customisation'][0].terms_condition==1){
                                 $("#terms_condition").prop("checked",true);
                                  $("#"+prefix+"terms_condition").show();
								}
								if(result['customisation'][0].inv_signature==1){
                                 $("#signature").prop("checked",true);
                                  $("#"+prefix+"_signature_box").show();
                                  $("#"+prefix+"_sign_name").val(result['customisation'][0].client_name);
                                  $("#"+prefix+"_sign_designation").val(result['customisation'][0].client_designation);
                                  
								}else{
                                   $("#signature").prop("checked",false);
                                  $("#"+prefix+"_signature_box").hide();
                                  $("#"+prefix+"_sign_name").val(result['customisation'][0].client_name);
                                  $("#"+prefix+"_sign_designation").val(result['customisation'][0].client_designation);

								}

                                 $("#client_name").val(result['customisation'][0].client_name);							
						
                                 $("#client_designation").val(result['customisation'][0].client_designation);
								
                                 if(result['customisation'][0].set_for_all_cont==1){
                                 $("#set_for_all_cont").prop("checked",true);
								}


								Materialize.toast('Custom options saved successfully', 2000,'red rounded');



							},
						});
	}
});


 $('#footer_btn').click(function(){
	if(prefix=="est"){
		var pref="EST"
	}
	if(prefix=="pro"){
		var pref="PRV"
	}
	if(prefix=="inv"){
		var pref="INV"
	}
	var bus_id=$('#'+prefix+'_bus_id').val();
	var reg_id=$('#'+prefix+'_reg_id').val();
	
	var inv_no= pref+""+$('#inv_invoice_no').val()+"/"+$('#inv_financial_yr').val();
	var customer_id=$('#'+prefix+'_customer_id').val();
	
	////////////////////////////////////////////////
	var eoe_checked=$("#eoe").is(":checked");
	if(eoe_checked){
		$("#eoe").val(1);
	}else{
		$("#eoe").val(0);
	}
	var eoe=$("#eoe").val();
	var ftr_font_color=$("#ftr_font_color").val();
	var ftr_font_size=$("#ftr_font_size").val();
	var ftr_back_color=$("#ftr_back_color").val();
	var company_address_checked=$("#company_address").is(":checked");
	if(company_address_checked){
		$("#company_address").val(1);
	}else{
		$("#company_address").val(0);
	}
	var company_address=$("#company_address").val();

	var company_email_checked=$("#company_email").is(":checked");
	if(company_email_checked){
		$("#company_email").val(1);
	}else{
		$("#company_email").val(0);
	}
	var company_email=$("#company_email").val();
	var company_contact_checked=$("#company_contact").is(":checked");
	if(company_contact_checked){
		$("#company_contact").val(1);
	}else{
		$("#company_contact").val(0);
	}
	var company_contact=$("#company_contact").val();

	var company_website_checked=$("#company_website").is(":checked");
	if(company_website_checked){
		$("#company_website").val(1);
	}else{
		$("#company_website").val(0);
	}
	var company_website=$("#company_website").val();
	var fb_link_checked=$("#fb_link").is(":checked");
	if(fb_link_checked){
		$("#fb_link").val(1);
	}else{
		$("#fb_link").val(0);
	}
	var fb_link=$("#fb_link").val();
	var twitter_link_checked=$("#twitter_link").is(":checked");
	if(twitter_link_checked){
		$("#twitter_link").val(1);
	}else{
		$("#twitter_link").val(0);
	}
	var twitter_link=$("#twitter_link").val();
	var linked_link_checked=$("#linked_link").is(":checked");
	if(linked_link_checked){
		$("#linked_link").val(1);
	}else{
		$("#linked_link").val(0);
	}
	var linked_link=$("#linked_link").val();
	
	var checked=$("#set_for_all_ftr").is(":checked");

	if(checked){
		$("#set_for_all_ftr").val(1);
	}else{
		$("#set_for_all_ftr").val(0);
	}
	var set_for_all_ftr=$("#set_for_all_ftr").val();
	
	////////////////////////////////////////////////
	
	
	if(customer_id==""){
	Materialize.toast('Please select client first', 2000,'red rounded');
	return false;
	}else{
		
		 $.ajax({
							type: "POST",
							dataType: 'json',
							
							url: base_url+'sales/get_footer_customisation',
							data:{
								'csrf_test_name':csrf_hash,
								"bus_id":bus_id,
							      "reg_id":reg_id,
								  "cust_id":customer_id,
								  "inv_invoice_no":inv_no,
								  "eoe_ftr":eoe,
								  "ftr_font_color":ftr_font_color,
                                  "ftr_font_size":ftr_font_size,
                                  "ftr_back_color":ftr_back_color,
                                  "company_address":company_address,
                  				  "company_email":company_email,
                  				  "company_contact":company_contact,
                                  "company_website":company_website,
                                  "fb_link":fb_link,
                                  "twitter_link":twitter_link,
                  				  "linked_link":linked_link,
                  				  "set_for_all_ftr":set_for_all_ftr							  
							
							},
							cache: false,
			                success:function(result)
							{
								

								if(result['customisation'][0].eoe_ftr==1){
                                 $("#eoe").prop("checked",true);
								}
								$("#ftr_font_color").val(result['customisation'][0].ftr_font_color);
								$("#ftr_font_size").val(result['customisation'][0].ftr_font_size);
								$("#ftr_back_color").val(result['customisation'][0].ftr_back_color);

								if(result['customisation'][0].company_address==1){
                                 $("#company_address").prop("checked",true);
                                 $("#"+prefix+"_ftr_address").show();
								}
								if(result['customisation'][0].company_email==1){
                                 $("#company_email").prop("checked",true);
                                 $("#"+prefix+"_ftr_email").show();
								}

								if(result['customisation'][0].company_website==1){
                                 $("#company_website").prop("checked",true);
                                 $("#"+prefix+"_ftr_website").show();
								}

								if(result['customisation'].company_contact==1){
                                 $("#company_contact").prop("checked",true);

                                 $("#"+prefix+"_ftr_contact").show();
                                 
								}

								if(result['customisation'][0].fb_link==1){
                                 $("#fb_link").prop("checked",true);
                                 $("#"+prefix+"_ftr_fb").show();
								}

								if(result['customisation'][0].twitter_link==1){
                                 $("#twitter_link").prop("checked",true);
                                 $("#"+prefix+"_ftr_tw").show();
								}

								if(result['customisation'][0].linked_link==1){
                                 $("#linked_link").prop("checked",true);
                                 $("#"+prefix+"_ftr_linkedin").show();
								}
								
                                 if(result['customisation'][0].set_for_all_ftr==1){
                                 $("#set_for_all_ftr").prop("checked",true);
								}


								Materialize.toast('Custom options saved successfully', 2000,'red rounded');

							},
						});
	}
});


$("#"+prefix+"_customer_id").on("change",function(){

	renderData(prefix);
});
  
 
  //////////////////////////////////// data fetching for customisation////////////////////////////////////////////////////////
 
});



function renderData(prefix){
	
	if(prefix=="est"){
		var pref="EST"
	}
	if(prefix=="pro"){
		var pref="PRV"
	}
	if(prefix=="inv"){
		var pref="INV"
	}
	var bus_id=$('#'+prefix+'_bus_id').val();
	var reg_id=$('#'+prefix+'_reg_id').val();
	
	var inv_no= pref+""+$('#inv_invoice_no').val()+"/"+$('#inv_financial_yr').val();
     	var customer_id=$('#'+prefix+'_customer_id').val();


	$.ajax({
							type: "POST",
							dataType: 'json',
							
							url: base_url+'sales/get_customisation',
							data:{
								'csrf_test_name':csrf_hash,
								"bus_id":bus_id,
							      "reg_id":reg_id,
								  "cust_id":customer_id,
								  "inv_invoice_no":inv_no,
								  
							},
							cache: false,
			                success:function(result)
							{
                               if(result['customisation'].set_invoice_no==1){
                                 $("#set_invoice_no").prop("checked",true);
                                 $("#inv_no_pre").val(result['customisation'].inv_no_pre);
                                
                                 // $("#invoice_letter").html(result['customisation'].inv_no_pre);
                                 if(result['customisation'].inv_no_pre!=""){
                                  $("#invoice_letter").html(result['customisation'].inv_no_pre);
                                   $("#inv_letter").val(result['customisation'].inv_no_pre);
                                 }else
                                 {
                                 	$("#invoice_letter").html(prefix);
                                 	$("#inv_letter").val(pref);
                                 }
                                
                                 
								}else{
									 $("#set_invoice_no").prop("checked",false);
                                 $("#inv_no_pre").val(result['customisation'].inv_no_pre);
                                 $("#inv_letter").val(result['customisation'].inv_no_pre);
                                 if(result['customisation'].inv_no_pre!=""){
                                  $("#invoice_letter").html(result['customisation'].inv_no_pre);
                                 }else
                                 {
                                 	$("#invoice_letter").html(prefix);
                                 }
                                  
								}

								if(result['customisation'].paper_size=='A4'){
                                 $("#paper_size").prop("checked",true);
								}
								if(result['customisation'].paper_size=='letter'){
                                 $("#letter").prop("checked",true);
								}


                                if(result['customisation'].inv_layout==1){
                                 $("#first-template").show();
								 $("#template-gallary").hide();
								}else if(result['customisation'].inv_layout==2){
                                  $("#second-template").show();
								  $("#template-gallary").hide();
								}else if(result['customisation'].inv_layout==3){
                                  $("#third-template").show();
								  $("#template-gallary").hide();
								}else if(result['customisation'].inv_layout==4){
                                  $("#fourth-template").show();
								  $("#template-gallary").hide();
								}else if(result['customisation'].inv_layout==5){
                                  $("#fifth-template").show();
								  $("#template-gallary").hide();
								}else if(result['customisation'].inv_layout==6){
                                  $("#sixth-template").show();
								  $("#template-gallary").hide();
								}else{
                                  $("#default-template").show();
								  $("#template-gallary").hide();
								}


								
								$("#margins").val(result['customisation'].inv_margins);
								$("#tmp_back_color").val(result['customisation'].tmp_back_color);
								$("#tmp_hex_color").val(result['customisation'].tmp_back_color);
                                  

								

								

								if(result['customisation'].set_for_all_tmp==1){
                                 $("#set_for_all_tmp").prop("checked",true);
								}
                                 

								if(result['customisation'].hdr_logo==1){
                                 $("#hdr_logo").prop("checked",true);
								 $("#hdr_logo").trigger('change');
								}
								$("#font_color").val(result['customisation'].font_color);
								$("#hex_color").val(result['customisation'].font_color);
								$("#font_size").val(result['customisation'].font_size);
								

								
								if(result['customisation'].bank_details==1){
                                 $("#bank_details").prop("checked",true);
                                 $("#"+prefix+"_bank_details").show();
								 $("#bank_details").trigger('change');
								}

								if(result['customisation'].pan_no==1){
                                 $("#pan_no").prop("checked",true);
                                  $("#"+prefix+"_pan_no").show();
								  $("#pan_no").trigger('change');
								  
								}
								if(result['customisation'].gst==1){
                                 $("#gst").prop("checked",true);
                                 $("#"+prefix+"_gst").show();
								 $("#gst").trigger('change');
								}
								if(result['customisation'].payment_btn==1){
                                 $("#payment_btn").prop("checked",true);
								 $("#payment_btn").trigger('change');
								}
								if(result['customisation'].cin_no==1){
                                 $("#cin_no").prop("checked",true);
                                 $("#"+prefix+"_cin_no").show();
								 $("#cin_no").trigger('change');
								}
								if(result['customisation'].purchase_order_date==1){
                                 $("#purchase_order_date").prop("checked",true);
                                  $("#"+prefix+"_reference_ref_date").show();
                                  $("#purchase_order_date").trigger('change');
								}
								if(result['customisation'].purchase_order==1){
                                 $("#purchase_order").prop("checked",true);
                                  $("#"+prefix+"_reference").show();
								  $("#purchase_order").trigger('change');
								}
								if(result['customisation'].terms_condition==1){
                                 $("#terms_condition").prop("checked",true);
                                  $("#"+prefix+"terms_condition").show();
								  $("#terms_condition").trigger('change');
								}
								if(result['customisation'].inv_signature==1){
                                 $("#signature").prop("checked",true);
                                  $("#"+prefix+"_signature_box").show();
                                  $("#"+prefix+"_sign_name").val(result['customisation'].client_name);
                                  $("#"+prefix+"_sign_designation").val(result['customisation'].client_designation);
								  $("#signature").trigger('change');
								}								
                                  $("#client_name").val(result['customisation'].client_name);							
						
                                 $("#client_designation").val(result['customisation'].client_designation);
								
                                 if(result['customisation'].set_for_all_cont==1){
                                 $("#set_for_all_cont").prop("checked",true);
								}

								if(result['customisation'].eoe_ftr==1){
                                 $("#eoe").prop("checked",true);
								 $("#eoe").trigger('change');
								}
								$("#ftr_font_color").val(result['customisation'].ftr_font_color);
								$("#ftr_font_size").val(result['customisation'].ftr_font_size);
								$("#ftr_back_color").val(result['customisation'].ftr_back_color);

								if(result['customisation'].company_address==1){
                                 $("#company_address").prop("checked",true);
                                 $("#"+prefix+"_ftr_address").show();
								 $("#company_address").trigger('change');
								}
								if(result['customisation'].company_email==1){
                                 $("#company_email").prop("checked",true);
                                 $("#"+prefix+"_ftr_email").show();
								 $("#company_email").trigger('change');
								}

								if(result['customisation'].company_website==1){
                                 $("#company_website").prop("checked",true);
                                 $("#"+prefix+"_ftr_website").show();
								 $("#company_website").trigger('change');
                                 
								}

								if(result['customisation'].company_contact==1){
                                 $("#company_contact").prop("checked",true);

                                 $("#"+prefix+"_ftr_contact").show();
                                 $("#company_contact").trigger('change');
								}

								if(result['customisation'].fb_link==1){
                                 $("#fb_link").prop("checked",true);
                                 $("#"+prefix+"_ftr_fb").show();
								 $("#fb_link").trigger('change');
								}

								if(result['customisation'].twitter_link==1){
                                 $("#twitter_link").prop("checked",true);
                                 $("#"+prefix+"_ftr_tw").show();
								 $("#twitter_link").trigger('change');
								}

								if(result['customisation'].linked_link==1){
                                 $("#linked_link").prop("checked",true);
                                  $("#"+prefix+"_ftr_linkedin").show();
								  $("#linked_link").trigger('change');
								}
								
                                 if(result['customisation'].set_for_all_ftr==1){
                                 $("#set_for_all_ftr").prop("checked",true);
								}


								//Materialize.toast('Your custom options saved successfully', 3000,'red rounded');

							},
						});
	
	
	
}

function changeColor(){
	
	
	  var hex=$('#hex_color').val();
	  $('#font_color').val(hex);
	  //document.getElementByClassName("temp_text").style.color = hex;
	  $('.temp_text').css("color", hex);
}

function changeColorftr(){
	
	
	  var hex=$('#ftr_hex_color').val();
	  $('#ftr_font_color').val(hex);
	  $('.footer_p').css("color", hex);
	  $('#address').css("color", hex);
}
function changeColorback(){
	
	
	  var hex=$('#back_hex_color').val();
	  $('#ftr_back_color').val(hex);
	  //to Set Background color.
	  $('#footer_box').css("background-color", hex);
	  $('.footer_box').css("background-color", hex);
}
var tmp_hex_color;
var defaultColor = "#ffffff";
function changeColortmp(){	
	  var hex=$('#tmp_hex_color').val();
	  //tmp_hex_color.value = defaultColor;
	  $('#tmp_back_color').val(hex);
	  $('#inv_tem').css("background-color", hex);
	  $('.inv_tem').css("background-color", hex);
	  //$('#templa').css("background-color", hex);
}


</script>