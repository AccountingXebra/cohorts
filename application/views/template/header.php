<!DOCTYPE html>

<html lang="en">

<head>

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

	<meta name="viewport" content="width=device-width, initial-scale=1">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta name="msapplication-tap-highlight" content="no">

	<meta name="description" content="">

	<meta name="keywords" content="">
	<!--<meta http-equiv="refresh" content="1200;url=<?php echo base_url();?>logoff" />-->

	<title>Xebra.in</title>

	<!-- Favicons-->

	<!-- <link rel="icon" href="../../images/favicon/favicon-32x32.png" sizes="32x32"> -->

	<!-- Favicons-->

	<!-- <link rel="apple-touch-icon-precomposed" href="../../images/favicon/apple-touch-icon-152x152.png"> -->

	<!-- For iPhone -->

	<meta name="msapplication-TileColor" content="#00bcd4">

	<!-- <meta name="msapplication-TileImage" content="images/favicon/mstile-144x144.png"> -->

	<!-- For Windows Phone -->

	<!-- CORE CSS-->

	<link rel="stylesheet" href="<?php echo base_url(); ?>public/css/select2.min.css" type="text/css"/>

	<link href="<?php echo base_url();?>asset/materialize/css/materialize.css" type="text/css" rel="stylesheet">

	<link href="<?php echo base_url();?>asset/css/style.css" type="text/css" rel="stylesheet">

	<!-- <link href="/your-path-to-fontawesome/css/solid.css" rel="stylesheet"> -->

    <link rel="stylesheet" href="<?php echo base_url('asset/datetimepick/css/bootstrap-datetimepicker.css'); ?>" />




    <?php /*?><link href="<?php echo base_url();?>asset/css/css/style-form.css" type="text/css" rel="stylesheet"><?php */?>

    <link href="<?php echo base_url();?>asset/css/custom.css" type="text/css" rel="stylesheet">

     <link href="<?php echo base_url();?>asset/css/userprofiletable.css" type="text/css" rel="stylesheet">

     <link href="<?php echo base_url();?>asset/css/jquery.dataTables.min.css" type="text/css" rel="stylesheet">
     <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.5.3/css/bootstrapValidator.css" type="text/css" rel="stylesheet">

  <!--Import Google Icon Font-->

  <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">

  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

  <link href="https://fonts.googleapis.com/css?family=Fira+Sans:100,400,500,600,700" rel="stylesheet">



    <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->


    <link href="<?php echo base_url();?>asset/css/perfect-scrollbar.css" type="text/css" rel="stylesheet">
    <link href="<?php echo base_url();?>asset/css/customei.css" type="text/css" rel="stylesheet">
	<script>var base_url = '<?php echo base_url(); ?>';</script>
  <script>var base_p = '<?php echo DOC_ROOT_DOWNLOAD_PATH; ?>';</script>
	<script>var csrf_token = '<?php echo $this->security->get_csrf_token_name(); ?>';</script>
	<script>var csrf_hash = '<?php echo $this->security->get_csrf_hash(); ?>';</script>
	
	<script type="text/javascript" src="<?php echo base_url();?>asset/js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript"  src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>

	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.1/js/all.min.js"></script>
    <style type="text/css">
	.notify h6{
		font-weight:500 !important;
	}

	.community-button:hover .community-icons{
	  background: url(<?=base_url('asset/css/img/icons/teamworks.png');?>) no-repeat center;
	}
	.community-button.active .community-icons{
	  background: url(<?=base_url('asset/css/img/icons/teamworks.png');?>) no-repeat center;
	}
	.community-icons {
	  background: url(<?=base_url('asset/css/img/icons/teamworks.png');?>) no-repeat center;
	  width: 40px;
	  float: left;
	  height: 68px !important;
	}
	/*...................................................*/
	#community-dropdown li {
		border-bottom: none !important;
	}
	i.settings-icons {
	  background-image: url(<?=base_url('asset/css/img/icons/settings-active.png');?>);
	  background-repeat: no-repeat;
	  background-position: center;
	  width: 20px;
	  height: 68px !important;
	}
	.setting-button:hover i.settings-icons {
	  background-image: url(<?=base_url('asset/css/img/icons/settings-active.png');?>);
	}
	.setting-button.active i.settings-icons {
	  background-image: url(<?=base_url('asset/css/img/icons/settings-active.png');?>);
	}
	.loading_data {
		position: fixed;
		left: 0px;
		top: 0px;
		width: 100%;
		height: 100%;
		z-index: 9999;
		background: url(<?=base_url('public/images/loading-ani.gif');?>) 50% 50% no-repeat rgb(249,249,249);
		background-size: 6%;
		opacity: .9;
	}
.notification-msg{
      font-size: 13px;
    font-weight: 300;
    color: #616161 !important;
}
.notifi-dropdown {
  white-space: normal !important;
  max-width: 400px !important;
  min-width: 400px !important;
}
.notisec {
    display: flex !important;
}
.notisec span {
   margin-right: 8px;
}

	#logout_for_new_bus_modal{
		left:150px;
		width:41% !important;
	}

	#logout_for_new_bus_modal .modal-body{
		margin: 0px;
	}

	#logout_for_new_bus_modal .modal-content {
		padding:0px;
	}

	#setting-dropdown li{
		background:  #7864e9 !important;
	}

	#community-dropdown li{
		background:  #7864e9 !important;
	}

	.gst_dropdown_select .dropdown-content li {
		background: #7864e9 !important;
		border-bottom: none  !important;
		overflow: hidden;
	}

	.gst_dropdown_select li{
		background:  #7864e9 !important;
	}

	.gst_dropdown_select span {
		color:#D0D3D4 !important;
	}

	.gst_dropdown_select span:hover {
		background:  #7864e9 !important;
		color:white !important;
		border-left:3px solid #50f1ae !important;
	}

	/*#setting-dropdown li:hover{
		border-left:4px solid #85e085 !important;
		background-repeat: no-repeat;
        background-position: left center;
	}*/

	.notify{
		padding-top:0px !important;
		margin-bottom:-15px !important;
	}

	.notify h6{
		font-size:11px !important;
	}

	.main_menu_options, .sub_menu_opt{
		color:#D0D3D4 !important;
	}

	.main_menu_options:hover{
		color:white !important;
		border-left: 3px solid #50e3c2 !important;
	}

	/*After active Set Alert Dropdown*/
	.main_menu_options.active{
		background-color: rgba(0,0,0,0.12);
		color:white !important;
		border-left: 3px solid #50e3c2 !important;
	}

	.setalert-submenu > li > a > .sub_menu_opt:hover{
		color:white !important;
	}

	.company_dropdown span.caret {
		display:none;
	}

	ul#notifications-dropdown {
		overflow-y: scroll !important;
		/*height:400px !important;*/
	}

	.mod-name{
		font-size:8px;
	}

	input:-webkit-autofill,
	input:-webkit-autofill:hover,
	input:-webkit-autofill:focus,
	textarea:-webkit-autofill,
	textarea:-webkit-autofill:hover,
	textarea:-webkit-autofill:focus,
	select:-webkit-autofill,
	select:-webkit-autofill:hover,
	select:-webkit-autofill:focus {
		-webkit-text-fill-color: #595959;
		-webkit-box-shadow: 0 0 0px 1000px #000 inset;
		transition: background-color 5000s ease-in-out 0s;
	}

	.el-badge .el-badge__content.is-fixed {
		top: 5px;
		right: -3px;
		padding: 0 4px;
		font-size: 10px;
		font-weight: 500;
		line-height: 14px;
		background-color: #db2124;
		border-width: 2px;
		-webkit-transform: none;
		transform: none;
	}
	.el-badge__content.is-fixed {
		position: absolute;
		top: 28px;
		right: 29px;
		-webkit-transform: translateY(-50%) translateX(100%);
		transform: translateY(-50%) translateX(100%);
	}
	.el-badge__content{
		background-color: #ff85a1 !important;
		border-radius: 10px;
		color: #fff;
		display: inline-block;
		font-size: 9px;
		font-weight:500;
		height: 17px;
		line-height: 15px;
		padding: 0 6px;
		text-align: center;
		white-space: nowrap;
		border: 1px solid #fff;
	}

	nav ul.hide-on-med-and-down > li {
		margin: 2px 0 0 25px !important;
	}

	.nav-wrapper .one_gst span.caret {
		display:none !important;
	}

	.one_gst .gst_dropdown_select{
		pointer-events: none !important;
	}
    </style>
</head>

<body class="layout-semi-dark white">
<div class="loading_data"></div>


    <?php

        $reg_id=$this->session->userdata['user_session']['reg_id'];
        $bus_id=$this->session->userdata['user_session']['bus_id'];

        if($reg_id) {

        $notifications = $this->Adminmaster_model->selectData('notifications','*',array('reg_id'=>$reg_id,'bus_id'=>$bus_id,'status'=>'Active', 'clear_noti' => 0),'noti_id','DESC');
        $newNotif = $this->Adminmaster_model->selectData('notifications','ref_id',array('reg_id'=>$reg_id,'bus_id'=>$bus_id,'status'=>'Active','noti_read_status'=>0 ,'clear_noti' => 0),'noti_id','DESC');

      }

    ?>

    <!-- Start Page Loading -->

   <!-- <div id="loader-wrapper">

		<div id="loader"></div>

		<div class="loader-section section-left"></div>

		<div class="loader-section section-right"></div>

    </div> -->

		<?php
      	  //$current_user =  $this->session->userdata('aduserData');
        ?>

    <header id="header" class="page-topbar">

      <!-- start header nav-->

      <div class="navbar-fixed">

        <nav class="navbar-color gradient-45deg-purple-deep-orange gradient-shadow">

          <div class="nav-wrapper">

            <div class="dropdown-select-wrapper">

              <div class="company_dropdown" style="/*border-right:1px solid rgb(229,235,247);*/">

                <select class="company_dropdown_select companyname read" id="header_company_profiles" name="header_company_profiles" disabled="true">
               		  <!-- <option value="" >Entire Company<option> -->

        					<?php if($this->session->userdata['user_session']['reg_id']) {


        						$company_profiles = $this->Adminmaster_model->selectData('businesslist','*',array('bus_id'=>$this->session->userdata['user_session']['bus_id'],'status'=>'Active'),'bus_id','ASC');

        						  foreach($company_profiles as $company) {
        						  ?>

                  <option value="<?php echo $company->bus_id; ?>" <?php echo ($company->bus_id == $this->session->userdata['user_session']['bus_id'])?'selected':''; ?>><?php echo $company->bus_company_name; ?></option>

                  <?php } }  ?>

                </select>

              </div>

              <?php
				//Remaining Time period
                if($this->session->userdata['user_session']['reg_istrial'] == 1){
					$now = time();
                    $created_date = strtotime($this->session->userdata['user_session']['createdat']);
                    $datediff = $now - $created_date;
                    $remaining_day = round($datediff / (60 * 60 * 24));
                    if($remaining_day>15){
                      //Activation Part ditails Come here
                      //echo 'trial_period_expired';
                    }
                    else{
                      ?>
                      <!--<div style="z-index: 10000; position: relative; top: 70px; left: 300px;" class="ex-date">
                        <a href="<?php echo base_url();?>subscription/my_subscription" class="grey-text text-darken-2 notisec" style="float:left; font-size:14px !important;position: fixed; border:1px solid #ff85a1  !important; border-radius:20px !important; background-color: #ff85a1; color:#ffffff !important; height:22px !important; line-height:21px !important; margin:0px 0px 0px 0px !important;">
                        &nbsp;&nbsp; You have <?php echo 15-$remaining_day; ?> days left in your trial. Upgrade Your Account. &nbsp
                        <a id="close_msg" style="float:right !important; font-size:12px !important;position: fixed; border:1px solid #ff85a1  !important; border-radius:20px !important; background-color: #ff85a1; color:#ffffff !important; height:20px !important; line-height:21px !important; padding:0 5px; cursor:pointer; margin:1.2px 0 0 7.7%">X</a></a>
                      </div>-->
                      <?php
                    }
                  }
              ?>
			  <?php
				$gst_number = $this->Adminmaster_model->selectData('gst_number','*',array('bus_id'=>$this->session->userdata['user_session']['bus_id'],'type'=>'business','status'=>'Active'),'gst_id','ASC');
			  ?>
              <?php if(count($gst_number) > 1){?>
				<div class="gst_dropdown hide_on_tial_expire">
			  <? }else{ ?>
				<div class="gst_dropdown one_gst hide_on_tial_expire">
			  <?php } ?>
                <?php if($this->session->userdata['user_session']['reg_admin_type'] == 5){?>
				<select class="gst_dropdown_select companyname" id="branch_gst_list" name="branch_gst_list" onchange="change_gst_header();" disabled>
				<?} else {?>
				<select class="gst_dropdown_select companyname" id="branch_gst_list" name="branch_gst_list" onchange="change_gst_header();">
				<?php } ?>
                  <?php if($this->session->userdata['user_session']['bus_id']) {
                    if($gst_number!=false){
                      foreach($gst_number as $gst) {
                       /*$result = mb_substr($gst->gst_no, 0, 7).'...';*/
                      ?>
					  <?php if($gst->status=="Active"){
						  $class = 'gstactive';
					  } else {
						  $class = 'gstinactive';
					  }?>

					  <option class="<?php echo $class; ?>" value="<?php echo $gst->gst_id; ?>" <?= ($this->session->userdata['user_session']['ei_gst']==$gst->gst_id)?'selected':''; ?>><?php echo "GSTIN: ".strtoupper($gst->gst_no).' -'.$gst->place; ?></option>

					  <?php } ?>
                      <option value="0" <?= ($this->session->userdata['user_session']['ei_gst']==0)?'selected':''; ?>>All Combined</option>
					<?php } }  ?>

                </select>

              </div>

              <div class="header-search-wrapper hide sideNav-lock hide_on_tial_expire">

                <i class="material-icons">search</i>

                <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Search" />

              </div>

            </div>

      <ul class="right hide-on-med-and-down">
              <li>
                <a href="javascript:void(0);" class="waves-effect waves-block waves-light profile-button" data-activates="profile-dropdown">
                  <span class="avatar-status avatar-online">
                    <!--<?php // if($current_user['aduserprofilephoto'] != '') {?>
                      <img src="<?php echo base_url();?>uploads/profile_images/<?php // echo $current_user['aduserprofilephoto']; ?>" alt="">
                    <?php //} ?>-->

                     <?php if($this->session->userdata['user_session']['user_photo']==""){

                      ?>
  				        	 <img src="<?php echo base_url();?>asset/css/img/icons/client.png" alt="profile-pic">
                    <?php }else{
                         $imgUrl=base_url().'public/upload/personal_images/'.$this->session->userdata['user_session']['reg_id'].'/'.$this->session->userdata['user_session']['user_photo'];
                      ?>
                     <img src="<?php echo $imgUrl;?>" alt="profile-pic">
                   <?php } ?>

                  </span>
                 <!-- substr($this->session->userdata['user_session']['ei_username'],0,8) -->
				 <?php $arr = explode(' ',trim($this->session->userdata['user_session']['ei_username'])); ?>
                  <span class="user-name">Hi
					<span class="user-display-name" style="width:2px;"><?php echo ucfirst($arr[0]); ?>
					</span>
				  </span>
				  <span class="arrow-icon down">&#9660;</span>
                  <span class="arrow-icon up">&#x25B2;</span>
                </a>
              </li>
              <li>
							<?php if(count($newNotif) == 0){ ?>
								<a style="width: 145%;" href="javascript:void(0);" class="waves-effect waves-block waves-light notification-button" data-activates="notifications-dropdown">
									<i class="notifications-icons"></i>
									<!--sup class="el-badge__content notiCount el-badge__content--undefined is-fixed"><?//= ($newNotif!='')?count($newNotif):''; ?></sup-->
								</a>
                <?php }else{ ?>
									<a style="width: 145%;" href="javascript:void(0);" class="waves-effect waves-block waves-light set notification-button" data-activates="notifications-dropdown">
										<i class="notifications-icons"></i>
										<sup class="el-badge__content notiCount el-badge__content--undefined is-fixed"><?= ($newNotif!='')?count($newNotif):''; ?></sup>
									</a>
                <?php } ?>
              </li>
              <?php if($this->session->userdata['user_session']['reg_admin_type'] == 1 || $this->session->userdata['user_session']['reg_admin_type'] == 3 || $this->session->userdata['user_session']['reg_admin_type'] == 4)
                  { ?>
              <li>
                <a href="javascript:void(0);" class="waves-effect waves-block waves-light setting-button" data-activates="setting-dropdown">
                  <i class="settings-icons"></i>
                </a>
              </li>
            <?php } ?>

              <li>
				<a href="#" class="waves-effect waves-block waves-light community-button" data-activates="community-dropdown">
                  <i class="community-icons"></i>
                </a>
              </li>

			  <!-- Marketplace -->
              <!--li class="sh-show">
                <a href="<?= base_url(); ?>community/my-marketplace" class="waves-effect waves-block waves-light community-button">
                  <i class="community-icons"></i>
                </a>
              </li-->
         	  <!-- Learning -->
			  <!--li class="sh-show" hidden>
                <a class="waves-effect waves-block waves-light community-button" data-activates="learning-dropdown">
                  <i class="community-icons"></i>
                </a>
              </li>
			  <!-- Activate Gift -->
			  <!--li class="sh-show" hidden>
                <a href="<?= base_url(); ?>connection/view_gift" class="waves-effect waves-block waves-light community-button">
                  <i class="community-icons"></i>
                </a>
              </li-->
            </ul>

            <!-- translation-button -->

            <ul id="translation-dropdown" class="dropdown-content">

              <li>

                <a href="#!" class="grey-text text-darken-1">

                  <i class="flag-icon flag-icon-gb"></i> English</a>

              </li>

              <li>

                <a href="#!" class="grey-text text-darken-1">

                  <i class="flag-icon flag-icon-fr"></i> French</a>

              </li>

              <li>

                <a href="#!" class="grey-text text-darken-1">

                  <i class="flag-icon flag-icon-cn"></i> Chinese</a>

              </li>

              <li>

                <a href="#!" class="grey-text text-darken-1">

                  <i class="flag-icon flag-icon-de"></i> German</a>

              </li>

            </ul>


          <!-- notifications-dropdown -->

          <?php  if(@$this->user_session["reg_trial_expire"] != 1){ ?>
          <ul id="notifications-dropdown" class="dropdown-content notifi-dropdown">

            <li class="notify">

              <h6>NOTIFICATIONS
                    <?php
                    if(count($newNotif) == 0)
                    {
                      ?>

                      <?php
                    }else{
                      ?>
                        <span class="new badge notiCount"><?= ($newNotif!='')?count($newNotif):''; ?></span>
                      <?php
                    }

                    ?>
              </h6>

            </li>

            <li class="divider"></li>

            <?php

            $reg_id=$this->session->userdata['user_session']['reg_id'];
		$bus_id=$this->session->userdata['user_session']['bus_id'];


			$subscription_bus=$this->Adminmaster_model->selectData('businesslist', '*', array('bus_id'=>$bus_id));
			$plan= $this->Adminmaster_model->selectData('subscription', '*',array('bus_id'=>$bus_id ,'subscription_id'=>@$subscription_bus[0]->subscription_id));
              // Remaining Time period
                //if($this->session->userdata['user_session']['reg_istrial'] == 1)
			if($plan[0]->subscription_plan=="Walk")
                {
                  $now = time();
                  $created_date = strtotime($this->session->userdata['user_session']['createdat']);
                  $datediff = $now - $created_date;
                  $remaining_day = round($datediff / (60 * 60 * 24));
                  if($remaining_day>15){
                    // Activation Part ditails Come here
                    //echo 'trial_period_expired';
                  }
                  else
                  {
                    ?>
                   <!-- <li style="background-color: #f9e8e8 !important;">
                      <a href="<?php echo base_url();?>subscription/my_subscription" class="grey-text text-darken-2 notisec" style="font-size:12px !important;">

                        <div><span class="material-icons icon-bg-circle yellow">warning</span></div><div style="word-break: break-word;">You have <?php echo 15-$remaining_day; ?> days left for trial period to end. UPGRADE NOW! </div>
                      </a>

                      <time class="media-meta" datetime="2015-06-12T20:50:48+08:00"></time>

                    </li>-->
                    <?php
                  }
                }

      			?>

            <?php
              // Show Notification of user
              if($notifications!=''){
              foreach($notifications as $notif) {
                  $today=date('Y-m-d');
                  $created_date = $notif->createdat;
                  $date_diff=strtotime($today) - strtotime(date("Y-m-d", strtotime($created_date)));
                  $days=floor($date_diff / (60 * 60 * 24));
                  $left_time='';

                  if($notif->noti_read_status==1)
                    {

                      ?>
                        <li class="notificationStatus" id="<?php echo $notif->noti_id ?>">
                      <?php

                    }else{

                      ?>
                        <li class="notificationStatus" id="<?php echo $notif->noti_id ?>" style="background-color: #f9e8e8 !important;">
                      <?php

                    }

                    ?>
                          <a class="grey-text text-darken-2 notisec">
                            <input type="hidden" name="link" id="link" value="<?php echo base_url();?><?=$notif->noti_url; ?>">
                            <div><span class="material-icons icon-bg-circle red">event_available</span></div><div style="word-break: break-word;"> <?=  wordwrap($notif->noti_message,60); ?>
                            </div>
                          </a>

                          <time class="media-meta" datetime="2015-06-12T20:50:48+08:00"><?= $days; ?> days ago</time>

                        </li>
                    <?php

                  }
                }
              ?>
			  <?php if(count($newNotif) == 0) { ?>

			  <?php }else{ ?>
				<li class="notify">
				<p style="text-align:center; font-size:14px !important;"><a style="text-decoration: underline; color:#50E2C2 !important;" onclick="change_status();">Clear notifications</a></p>
				</li>
              <?php } ?>
          </ul>
          <?php }else{} ?>


          <?php if(@$this->user_session["reg_trial_expire"] != 1){ ?>

          <!-- Profile Dropdown -->
			    <ul id="profile-dropdown" class="dropdown-content user-profile-down">
              <li>
                <div class="user-pofile-image">
                 <!--  <img src="<?php echo base_url(); ?>asset/images/user.jpg" class="user-img"> -->
                  <div class="user-image-layout"></div>
                  <div class="user-short-details">
                    <span class="name"><?php echo ucfirst($this->session->userdata['user_session']['ei_username']); ?></span>
                    <span class="mail"><?php echo $this->session->userdata['user_session']['email']; ?></span>
                  </div>
                </div>
              </li>
              <li class="user-profile-fix">
                <ul>

                  <?php if($this->session->userdata['user_session']['reg_admin_type'] == 3 || $this->session->userdata['user_session']['reg_admin_type'] == 1 || $this->session->userdata['user_session']['reg_admin_type'] == -1)
                  { ?>

                  <li class='my-comp'>
                    <a style="font-size:12px;" id='com_pro' href="<?php echo base_url();?>profile/company-profile">
                        MY COMPANY PROFILE
					</a>
                    <!--ul class="userprofile-submenu">
						<li id="CheckForExistingBus">
							<a class="modal-trigger modal-close" href="#logout_for_new_bus_modal">
								<i class="menu-round-icon"></i>
								<span>Add new business</span>
							</a>
						</li>
                    </ul-->
                  </li>
				<?php }?>
                  <?php if($this->session->userdata['user_session']['reg_admin_type'] == 3 || $this->session->userdata['user_session']['reg_admin_type'] == -1)
                  { ?>
                  <li>
                    <a style="font-size:12px;" href="<?php echo base_url(); ?>profile/personal-profile">
                      MY PERSONAL PROFILE
                    </a>
                  </li>
                  <?php }?>


                  <?php if($this->session->userdata['user_session']['reg_admin_type'] == 3 || $this->session->userdata['user_session']['reg_admin_type'] == 1 || $this->session->userdata['user_session']['reg_admin_type'] == -1)
                  { ?>
                  <li>
                    <a style="font-size:12px;" href="<?= base_url(); ?>dashboard/document-locker">
                      MY DOCUMENT LOCKER
                    </a>
                  </li>
                  <!-- <li>
                    <a href="#">
                      My Products / Services
                    </a>
                  </li> -->
                  <li>
                    <a style="font-size:12px;" href="#" id="ac-menu" class="open">
                      MY ACCOUNT
                    </a>
                    <ul class="userprofile-submenu my_ac">
                      <?php if($this->session->userdata['user_session']['reg_istrial'] == 0){ ?>
                      <li>
						<a style="font-size:12px;" class="renew_sub modal-trigger modal-close" href="#subscription_modal">
                        <!--<a href="<?php //echo base_url(); ?>subscription">-->
                        <i class="menu-round-icon"></i>
                        <span>RENEW SUBSCRIPTION & <br>&nbsp&nbsp&nbsp&nbspBUY ADD-ONS</span>
                        </a>
                      </li>
                    <?php } ?>

                      <li>
            <!--<a class="modal-trigger modal-close" href="#subscription_modal">-->
                        <a style="font-size:12px;" href="<?php echo base_url(); ?>subscription">
                          <i class="menu-round-icon"></i>
                          <span>MY BILLING HISTORY</span>
                        </a>
                      </li>
                      <?php if($this->session->userdata['user_session']['reg_admin_type'] == 3){ ?>

                      <!-- <li>
                        <a href="#">
                          <i class="menu-round-icon"></i>
                          <span>Referrals</span>
                        </a>
                      </li>-->
                      <li>
                        <a style="font-size:12px;" href="<?php echo base_url(); ?>module_tracker">
                          <i class="menu-round-icon"></i>
                          <span>MY ACTIVITY HISTORY</span>
                        </a>
                      </li>
                      <li>
                        <a style="font-size:12px;" class=" Password-change modal-trigger" href="#password-modal">
                          <i class="menu-round-icon"></i>
                          <span>CHANGE PASSWORD</span>
                        </a>
                      </li>
                      <!-- <li>
                        <a class=" deactivate modal-trigger" href="<?= base_url(); ?>dashboard/document-locker">
                          <i class="menu-round-icon"></i>
                          <span>Document Locker</span>
                        </a>
                      </li> -->
                      <li>
                        <a style="font-size:12px;" class=" deactivate modal-trigger" href="#deactive_myaccount_modal">
                          <i class="menu-round-icon"></i>
                          <span>DEACTIVATE ACCOUNT</span>
                        </a>
                      </li>
                    <?php } ?>

                    </ul>
                  </li>

                  <?php }?>

                  <li>
                    <a style="font-size:12px;" href="#feedback_modal" class="modal-trigger">
                      MY FEEDBACK
                    </a>
                  </li>

				  <?php if($this->session->userdata['user_session']['reg_admin_type'] == 3 || $this->session->userdata['user_session']['reg_admin_type'] == -1 || $this->session->userdata['user_session']['reg_admin_type'] == 2 || $this->session->userdata['user_session']['reg_admin_type'] == 4 || $this->session->userdata['user_session']['reg_admin_type'] == 5 || $this->session->userdata['user_session']['reg_admin_type'] == 1){ ?>
				  <li class='my-comp'>
                    <a style="font-size:12px;" id="refer_earn" href="<?php echo base_url();?>refer_earn">
                      MY REFERRAL PROGRAM
                    </a>

					<!--<ul class="userprofile-submenu">
						<li>
						<a>
						<i class="menu-round-icon"></i>
						<span>Add new Referal</span>
						</a>

						</li>
					</ul>-->
                  </li>
				  <?php } ?>
                    <li class="divider"></li>
                   <!--li>
                        <a class=" Password-change modal-trigger" href="#password-modal">

                          <span>Change Password</span>
                        </a>
                      </li-->


                  <li class="divider"></li>
                  <li class="signout-link">
                    <a href="#logout_myacc_modal" class="singout modal-trigger">
                      Sign out
                    </a>
                  </li>
                </ul>
              </li>
          </ul>
        <?php }else{ ?>

          <!-- Profile Dropdown -->
          <ul id="profile-dropdown" class="dropdown-content user-profile-down">
              <li>
                <div class="user-pofile-image">
                 <!--  <img src="<?php echo base_url(); ?>asset/images/user.jpg" class="user-img"> -->
                  <div class="user-image-layout"></div>
                  <div class="user-short-details">
                    <span class="name"><?php echo ucfirst($this->session->userdata['user_session']['ei_username']); ?></span>
                    <span class="mail"><?php echo $this->session->userdata['user_session']['email']; ?></span>
                  </div>
                </div>
              </li>
              <li class="user-profile-fix">
                <ul>

                  <?php if($this->session->userdata['user_session']['reg_admin_type'] == 3 || $this->session->userdata['user_session']['reg_admin_type'] == -1)
                  { ?>
                  <li>
                    <a href="#">
                      My Personal Profile
                    </a>
                  </li>
                  <?php }?>

                  <?php if($this->session->userdata['user_session']['reg_admin_type'] == 3 || $this->session->userdata['user_session']['reg_admin_type'] == 1 || $this->session->userdata['user_session']['reg_admin_type'] == -1)
                  { ?>

                  <li>
                      <a href="#">
                        My Company Profile
                          </a>
                        <!-- <ul class="userprofile-submenu">
                        <li id="CheckForExistingBus">
                            <a data-target="#">
                            <i class="menu-round-icon"></i>
                            <span>Add new business</span>
                          </a>
                      </li>
                      </ul> -->
                  </li>

                  <?php }?>

                  <?php if($this->session->userdata['user_session']['reg_admin_type'] == 3 || $this->session->userdata['user_session']['reg_admin_type'] == 1 || $this->session->userdata['user_session']['reg_admin_type'] == -1)
                  { ?>

                  <li>
                    <a href="#">
                      Document Locker
                    </a>
                  </li>
                  <!-- <li>
                    <a href="#">
                      My Products / Services
                    </a>
                  </li> -->
                  <li>
                    <a href="#">
                      My Account
                    </a>
                  </li>

                  <?php }?>

                  <li>
                    <a href="#" class="modal-trigger">
                      My Feedback
                    </a>
                  </li>


                  <li class="divider"></li>
                  <li class="signout-link">
                    <a href="#logout_myacc_modal" class="singout modal-trigger">
                      Sign out
                    </a>
                  </li>
                </ul>
              </li>
          </ul>

        <?php } ?>

        <?php $bus_id = $this->user_session['bus_id']; ?>

		  	<!-- Community Dropdown -->
          	<ul id="community-dropdown" class="dropdown-content user-profile-down">
              	<li class="user-profile-fix">
	                <ul>
	                	<li>
	                    	<a class="main_menu_options" href="<?= base_url(); ?>community/manage-profile?token=<?php echo $bus_id; ?>">
	                    		<i class="manageprofile-icons" style="margin: -25px 10px -20px 0px;"></i>
	                    	MANAGE PROFILE
	                    	</a>
	                  	</li>

	                  	<li>
	                    	<a class="main_menu_options" href="<?= base_url(); ?>community/my-connections">
	                    		<i class="comm-icons" style="margin: -25px 10px -20px 0px;"></i>
	                    	CONNECTIONS
	                    	</a>
	                  	</li>

					  	<li>
	                    	<a class="main_menu_options" href="<?= base_url(); ?>community/my-marketplace">
	                    		<i class="marketplace-icons" style="margin: -25px 10px -20px 0px;"></i>
	                      	MARKET PLACE
	                    	</a>
	                  	</li>

	                  	<li>
	                    	<a class="main_menu_options" href="<?= base_url(); ?>community/deals">
	                    		<i style="font-size:16px; margin:0 8px 0 4px;" class="fas fa-gift"></i>&nbsp;DEALS
	                    	</a>
	                  	</li>

						<li>
	                    	<a class="main_menu_options" href="<?= base_url(); ?>community/events">
								<i style="margin: -25px 10px -20px 0px;" class="events-icons" aria-hidden="true"></i>EVENTS
	                    	</a>
	                  	</li>
					</ul>
			  	</li>
		  	</ul>

		  	<ul id="learning-dropdown" class="dropdown-content user-profile-down">
              <li class="user-profile-fix">
                <ul>
                  <li>
                    <a class="main_menu_options" href="<?= base_url(); ?>resource_center/accounting-terms">
                      Accounting Terms
                    </a>
                  </li>
				  <li>
                    <a class="main_menu_options" href="<?= base_url(); ?>resource_center/financial_ratios">
                      Financial Ratios
                    </a>
                  </li>
				</ul>
			  </li>
		  	</ul>

          <?php if(@$this->user_session["reg_trial_expire"] != 1 ){ ?>
          <!-- Settings Dropdown -->
          <ul id="setting-dropdown" class="dropdown-content user-profile-down">

              <li class="user-profile-fix">
                <ul>
				<li>
                    <a class="main_menu_options" href="<?php echo base_url(); ?>business_analytics/import_data">
                      <i style="font-size:16px; margin-right:10px;" class="fas fa-file-import"></i>IMPORT DATA
                    </a>
                  </li>
									<li>
                    <a class="main_menu_options" href="<?php echo base_url(); ?>customise-emails">
                      <i style="font-size:16px; margin-right:10px;" class="fas fa-envelope"></i>CUSTOMIZE EMAILS
                    </a>
                  </li>

                  <li>
                    <a class="main_menu_options open" id="set-alert" href="#">
					  <!--i style="font-size:16px; margin-right:8px;" class="fas fa-alarm-clock"></i--><i style="font-size:16px; margin-right:10px;" class="fas fa-bell"></i>SET ALERT FOR
                    </a>
                    <ul class="setalert-submenu">
                      <?php if($this->session->userdata['user_session']['reg_admin_type'] !=4)
					 { ?>
					  <li>
                        <a href="<?= base_url(); ?>settings">
                          <i class="menu-round-icon"></i>
                          <span class="sub_menu_opt">CLIENT / VENDOR</span>
                        </a>
                      </li>
                       <li>
                        <a href="<?= base_url(); ?>settings/item-alerts">
                          <i class="menu-round-icon"></i>
                          <span class="sub_menu_opt">ITEM / EXPENSE</span>
                        </a>
                      </li>
                      <!--li>
                        <a href="<?//= base_url(); ?>settings/vendor_alert_list">
                          <i class="menu-round-icon"></i>
                          <span class="sub_menu_opt">For Vendor</span>
                        </a>
                      </li>
                      <li>
                        <a href="<?//= base_url(); ?>settings/exp_alert">
                          <i class="menu-round-icon"></i>
                          <span class="sub_menu_opt">For Expense</span>
                        </a>
                      </li-->
                      <li>
                        <a href="<?= base_url(); ?>settings/credit-period-alerts">
                          <i class="menu-round-icon"></i>
                          <span class="sub_menu_opt">CREDIT PERIOD</span>
                        </a>
                      </li>
					 <?php } ?>
                      <li>
                        <a href="<?= base_url(); ?>settings/birthday-anniversary-alerts">
                          <i class="menu-round-icon"></i>
                          <span class="sub_menu_opt">BIRTHDAY & ANNIVERSARY</span>
                        </a>
                      </li>
					  <li>
                        <a href="<?= base_url(); ?>settings/activity-alerts">
                          <i class="menu-round-icon"></i>
                          <span class="sub_menu_opt">ACTIVITY / TASK</span>
                        </a>
                      </li>

                    </ul>
                  </li>

                </ul>
              </li>
          </ul>
          <?php }else{} ?>

          </div>

        </nav>

      </div>

      <!-- ----- Start Logout For New Company ----------- -->

      <div id="logout_for_new_bus_modal" class="modal modal-md modal-display" aria-hidden="true">
         <style>
			#logout_for_new_bus_modal .modal-header h4 {
				width: 90%;
			}
		 </style>
		 <img class="geen" src="<?php echo base_url(); ?>asset/images/green.png" alt="green-cir">
         <div class="modal-content">
            <div class="modal-header">
               <h4 class="text-center">Add New Company</h4>
               <a class="modal-close close-pop log_close"><img style="margin-left:-5px;" src="<?php echo base_url(); ?>asset/images/popupdelete.png" alt="delete"></a>
            </div>
         </div>
         <div class="modal-body confirma">
            <p class="ml-20">You will be signed out from your existing session & taken to subscription page for adding another company
			</p><!-- You will be signed out from your existing session and subscribe again for new company -->
         </div>
         <div class="modal-content">
            <div class="modal-footer">
               <div class="row">
                  <!--   <div class="col l4 s12 m12"></div> text-right-->
                  <div class="col l12 s12 m12 cancel-deactiv" style='text-align:right;'>
                     <a class="close modal-close btn-flat theme-primary-btn theme-btn theme-btn-large model-cancel" type="button" data-dismiss="modal" style="margin-top:-5px !important; background-color:white !important; color:black !important;">CANCEL</a>
					 <a style='margin-top:0px; margin-right:5px;' class="btn-flat theme-primary-btn theme-btn theme-btn-large dea modal-trigger modal-close logout_my_account centertext">CONFIRM</a>

                  </div>
               </div>
            </div>
         </div>
      </div>

    </header>

 <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-145159571-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-145159571-1');
</script>

    <!-- Start of  Zendesk Widget script -->
<!--<script id="ze-snippet" src="https://static.zdassets.com/ekr/snippet.js?key=3c42f582-437d-4c63-8bba-8e08f246232d"> </script>-->
<!-- End of  Zendesk Widget script -->

<!--<script type="text/javascript">(function() {var walkme = document.createElement('script'); walkme.type = 'text/javascript'; walkme.async = true; walkme.src = 'https://cdn.walkme.com/users/9d0d877be77b4684904a1bc20f005dbd/test/walkme_9d0d877be77b4684904a1bc20f005dbd_https.js'; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(walkme, s); window._walkmeConfig = {smartLoad:true}; })();</script>-->

	<script type="text/javascript">

		function change_status() {

			$.ajax({
				type: "post",
				dataType: "json",
				data: {'csrf_test_name':csrf_hash,'status':1},
				url: '<?php echo base_url();?>notification/change_status',
				cache: false,
				success: function(data) {

					if(data==true) {
						Materialize.toast('Notification cleared successfully.', 2000,'green rounded');
						$('#notifications-dropdown').html('');
						$('.notiCount').html(0);
					} else {
						//
					}
				}
			});

		}


		$(document).ready(function() {

			if(navigator.onLine) {
        //alert("Hurray! You're online!!!");
    } else {
        //alert("Oops! You're offline. Please check your network connection...");
        location.href=base_url+'index/no_internet';
    }

		$("#close_msg").click(function(){
			$(".ex-date").css("display","none");
		});

		////////////////



        ////////////////////

var unloaded = false;
//$(window).on('beforeunload', unload);
//$(window).on('unload', unload);





// Attach the event click for all links in the page
	if($("#branch_gst_list option").hasClass("gstinactive")){
		$("#branch_gst_list .gstinactive").css("color","#000 !important");
	}

  $("a").on("click", function() {

     unloaded = true;
  });
   $("select").on("change", function() {

     unloaded = true;
  });
    $("input").on("click", function() {

     unloaded = true;
  });
    $("button").on("click", function() {

     unloaded = true;
  });
$('.select-dropdown').on("change",function(){

    unloaded = true;
});

   $("p").on("click", function() {
     unloaded = true;
  });
    $("span").on("click", function() {

     unloaded = true;
  });
    $("body").on("click", function() {

     unloaded = true;
  });
     $("div").on("click", function() {

     unloaded = true;
  });
      $("img").on("click", function() {

     unloaded = true;
  });
$("ul").on("click", function() {

     unloaded = true;
  });
$("li").on("click", function() {

     unloaded = true;
  });
$("ol").on("click", function() {

     unloaded = true;
  });
$("h1").on("click", function() {

     unloaded = true;
  });
$("h2").on("click", function() {

     unloaded = true;
  });
$("h3").on("click", function() {

     unloaded = true;
  });
$("h4").on("click", function() {

     unloaded = true;
  });
$("h5").on("click", function() {

     unloaded = true;
  });
$("h6").on("click", function() {

     unloaded = true;
  });
$("hr").on("click", function() {

     unloaded = true;
  });



  // Attach the event submit for all forms in the page
  $("form").on("submit", function() {
     unloaded = true;
  });

  $("input[type=submit]").on("click", function() {
unloaded = true;
});
$(window).on("keydown",function(){
  unloaded = true;
});

window.onhashchange = function() {
 //blah blah blah
 //alert('refresh');
 unloaded = true;
}


window.onbeforeunload = function(e) {

    if(window.screenLeft >screen.width){
            unloaded = false;

        }

  else{
  unloaded = true;

}
unload();
}

window.unload = function(e) {


        unload();
}

function unload(){
    if(!unloaded){
        $('body').css('cursor','wait');
        $.ajax({
            type: 'get',
            async: false,
            url: base_url+'index/logout/',
            success:function(){
                unloaded = true;
                $('body').css('cursor','default');
            },
            timeout: 50000
        });
    }
}
				//Hide and Show for 'MY ACCOUNT'
				$('#ac-menu').click(function(){
					if($(this).hasClass('open')){
						$('.my_ac').css('visibility','visible').css('opacity','1').css('height','auto');
						$('#ac-menu').removeClass("open");
						$('#ac-menu').addClass("close");
						//$('#profile-dropdown').css('');
						return false;
					}else if($(this).hasClass('close')){
						$('.my_ac').css('visibility','hidden').css('opacity','0').css('height',0);
						$('#ac-menu').removeClass("close");
						$('#ac-menu').addClass("open");
						return false;
					}
				});

				//Hide and Show for 'SET ALERTS'
				$('#set-alert').click(function(){
					if($(this).hasClass('open')){
						$('.setalert-submenu').css('visibility','visible').css('opacity','1').css('height','auto');
						$('#set-alert').removeClass("open");
						$('#set-alert').addClass("active");
						$('#set-alert').addClass("close");
						//$('#profile-dropdown').css('');
						return false;
					}else if($(this).hasClass('close')){
						$('.setalert-submenu').css('visibility','hidden').css('opacity','0').css('height',0);
						$('#set-alert').removeClass("close");
						$('#set-alert').removeClass("active");
						$('#set-alert').addClass("open");
						return false;
					}
				});

			});
	</script>

    <!-- END HEADER -->
