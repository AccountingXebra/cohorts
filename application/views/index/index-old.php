
    <?php include('header.php'); ?>
	<style type="text/css">
        .forgot_email label.error{
          padding-top: 12px;
        }
        .modal-loader {
          position: fixed;
          left: 0px;
          top: 0px;
          width: 100%;
          height: 100%;
          z-index: 9999;
          background: url(./public/images/loader.gif) center no-repeat;
      }
       .dataTables_length {
  
    margin-left: 500px;
       }
    .eazy{
      font-weight:900%;
    }
    
    /*.dot {
      height: 100px;
      width: 100px;
      background-color: #FF7D9A;
      border-radius: 50%;
      display: inline-block;
    }*/
  
    input#login_submit{
      width:60% !important;
      padding-right:20px !important;
      background: #8533ff !important;
      color:white !important;
      border:none !important;
    }
  
    input#login_submit:hover{
      background: #8533ff !important;
      color:white !important;
      border:none !important;
    }
    
    .logo_style_2{
      height:55px !important;
    }
      </style>
      <!-- Pop up Login form start -->
   
            <!-- Modal content-->
      <span class="dot"></span>
            <div class="sign_up container " style="margin: 50px 0; width: 100%;">
        <img class="geen" src="<?php echo base_url();?>asset/images/off-circle.png" style="margin-left:495px !important; position:absolute; margin-top:-30px;" alt="off-cir">
              <div class="col-md-12">
                <div class="col-md-4">
                </div>
                  <div class="row">

                    <?php
                 $user=$this->input->cookie('remember_me_u',true);
                 $pass=$this->input->cookie('remember_me_pass',true);

                    ?>
          <!--<img class="geen" style="margin:0px 0px 0px 0px;" src="<?php echo base_url(); ?>asset/images/greenCir_2.jpg">-->
                  <div class="col-md-7 login_page" style="margin-left: 0; padding-left: 40px; padding-right: 40px;">
                      <div class="modal-content" style="box-shadow: none;">
                          <div style="text-align:center; margin-top:10px;"  >
                              <!--<h4 class="modal-title"><img src="<?php echo base_url(); ?>public/images/header_1.png" alt="Logo" class="logo_style_2"/></h4>-->
                              <!--<label class="logo_line_2">Most Social Invoicing & Business Intelligence Platform</label>-->
                <img width="205" height="55" src="<?php echo base_url(); ?>public/images/xebra-logo.png" alt="xebra-logo" class="logo_style_2"/>
                <label class="logo_line_2" style="padding-left:2px; padding-right:12px; padding-top:12px;">To log in, enter your email address and password</label>
                          </div>
                          <div class="modal-body" style="margin-top:-10px !important;">
                           <form action="" id="login_frm" name="login_frm" class="" method="post" accept-charset="utf-8" novalidate="true">
							<?php $csrf = array(
					'name' => $this->security->get_csrf_token_name(),
					'hash' => $this->security->get_csrf_hash()
				);
				?>
				<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
                              <div class="row">
                                      <div class="col-lg-12 error_cls">
                                        <div id="error_popup" name="error_popup">
                                    </div>
                                      </div>
                                  <div class="col-lg-12 login_form_textbox">
                                      <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-envelope-o"></i></span>
                                        <input class="form-control" name="email" id="email" type="text" placeholder="EMAIL" value="<?=$user?>">
                                      </div>
                                      <!--<div class="req_star_textbx"><font style="color:red">*</font></div>-->
                                  </div>
                                  <div class="col-lg-12 login_form_textbox"> 
                   <div class="input-group" style="border:1px solid #ababab; border-radius:4px;">
                                        <span class="input-group-addon" style="padding: 6px 13px !important; border:none !important; border-right:1px solid #ababab !important;"><i class="fa fa-lock fa-lg"></i></span>
                                        <input style="width:78%; border:none !important;" class="form-control show_password" name="password" id="password" type="password" placeholder="PASSWORD" value=""/>
                    <div class="hide-show">
                      <span style="position:absolute; left:248px; top:5px; cursor: pointer;"><img width="29" height="30" src="<?php echo base_url(); ?>public/images/no-monkey.png" alt="small-eazy"></img></span>
                    </div>
                   </div>
					<!--<div class="req_star_textbx"><font style="color:red">*</font></div>-->
                  </div>
                  <div class="col-lg-12 login_form_textbox" style="margin-top:-14px;">
                  <!--<label style="font-size:11px !important; text-align:left;">Minimum 8 Characters, 1 in Uppercase, 1 Number & 1 Special Character</label>-->
                  </div>
                  <!--<div class="col-lg-12 login_form_textbox" style="display:block;">
                                    <div class="col-lg-6 login_rem_2">
                                          <input type="checkbox" class="form_checkbox show_pass_check" id="show_paas"> 
                                          <div class="login_rem">Show password</div>
                                      </div>
                                  </div>-->
                                      
                                <div class="col-lg-12 login_form_textbox recaptcha" id="recaptcha" style="display:none;">
                    <div class="g-recaptcha" data-sitekey="6LfYOTQUAAAAAIrUmLcbkZdfQEzH8HKxhgpT7CHo"></div>
                  </div>
                  <div class="col-lg-12">
                  <label id="g-recaptcha-response-error" class="error" for="g-recaptcha-response"></label>
                  </div>
                                   <div class="col-lg-12 login_form_textbox" style="display:block;">
                                      <div class="col-lg-6 login_rem_2">
                                          <input style="margin:0px !important;" type="checkbox" class="form_checkbox" name="remember_me" id="remember_me"/>
                                          <div style="padding-top:0px !important;" class="login_rem">Remember Me</div>
                                      </div>
                                      <div class="col-lg-6">
                                          <div><a style="text-decoration:none; font-family: 'Roboto', sans-serif;" class="login_forgotpass_link" href="#" onclick="open_forgotpassword()">Forgot Password?</a></div>
                                      </div>
                                   </div>
                           
                                  <div class="col-lg-12" style="margin-bottom:-20px !important;">
                                      <div class="form-group login_btn_1">
                                          <input type="submit" class="btn login_form_btn" value="SECURE LOGIN" name="login_submit" id="login_submit"><i class="fa fa-lock fa-2x" style="color:#fff; margin-left:-35px;"></i>
                                          <!--<div class="login_signup_link"><a href="<?= base_url(); ?>client-portal">Login as Client</a></div>--> <!-- onclick="open_client_login()" -->
                                      </div>
                                  </div>
                               </div>
                              </form>
                          </div>
              <div class="modal-footer" style="background-color:#f0f0f5; padding:20px 10px; border-radius:5px;">
                <!--<div class="col-lg-12 login_signup_link">Don't have an account?   
                <a href="#" onclick="open_signup()"> <b>Sign Up</b></a></div>-->
                <div class="col-lg-12">
                  <!--<a class="btn btn-welcome" href="<?php echo base_url(); ?>index/emailmsg">Subscribe Now</a>-->
                  <div class="col-lg-12 login_signup_link" style="font-size:13px;">Don't have an account?   
                  <a href="#" style="color:#7965E9;" onclick="open_signup()"><b>Sign Up</b></a></div>
                </div>
              </div>
                      </div>
                  </div>
              </div>
          </div>
      <!--<img class="geen" src="<?php echo base_url();?>asset/images/green-circless.png" style="margin-left:502px">-->
        </div>  
    <div class="sign_footer footer" style="margin-top:75px;">
      <div class="row" style="text-align:center">
        <div style="margin-right:45px; font-size:13px;">
          <p style="color:white;"><a style="color:white;" href="https://www.xebra.in/terms-conditions/" target="_blank"> Terms & Conditions </a> | <a style="color:white;" href="https://www.xebra.in/privacy-policy/" target="_blank"> Privacy Policy </a> | <a style="color:white;" href="https://www.xebra.in/faqs/" target="_blank"> FAQs </a></p>
        </div>
        <div style="margin-right:45px; margin-top:5px;">
          <label style="font-size:13px; color:#B0B7CA;">Copyright &copy; <?php echo date('Y');?> Xebra. All Right Reserved</label>
        </div>
      </div>
    </div>
   
    <script>
 
    $( document ).ready(function() {

      var em = '<?php 
            if(@$web_email){
              echo $web_email;
            }else{
              echo "";
            }
           ?>'

      //alert(em);
      if(em)
      {
        //alert(em);
        open_signup();
        $('.Web_email').val(em);
      }
 var signup='<?php echo $open_signup;?>';
      if(signup){
         open_signup();
      }

      var otp='<?php echo $open_otp;?>';
      if(otp){
         open_otp();
      }
        /*$('.hide-show').show();
        $('.hide-show span').addClass('show')
          
        $('.hide-show span').click(function(){
        if( $(this).hasClass('show') ) {
          $(this).text('Hide');
          $('#password').attr('type','text');
          $(this).removeClass('show');
        } else {
          $(this).text('Show');
          $('#password').attr('type','password');
          $(this).addClass('show');
        }
        });
          
        $('#password').on('click', function(){
          $('.hide-show span').text('Show').addClass('show');
          $('.hide-show').parent().find('#password').attr('type','password');
        });*/ 
    });
    </script>
    


















      <?php //include('pages/slider.php'); ?>

      <?php //include('pages/about.php'); ?>

      <?php  //include('file:///D|/Krutika/PROJECTS/EasyInvoice/2018/FEB/23 FEB 2018/backup/easy_invoice_system/application/views/pages/template/services.php'); ?>

      <?php // include('pages/portfolio.php'); ?>

      <?php //include('pages/contact.php'); ?>
      <?php include('footer.php'); ?>

  <!--<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=599ece9893086300127f927d&product=sticky-share-buttons"></script>-->
<!--<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-599ed322f4cc9c4b"></script> -->

