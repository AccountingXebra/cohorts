<footer id="ubea-footer" role="contentinfo" >
<style>
	.modal-loader{
		position: fixed;
		left: 29.5%;
		top: 80px;
		width: 42%;
		height: 55%;
		z-index: 9999;
		background: url(<?=base_url('public/images/loading-ani.gif');?>) 50% 50% no-repeat rgb(249,249,249);
		background-size: 6%;
		opacity: .9;
	}
	hr{  
		border: none;
		border-left: 1px solid hsla(200, 10%, 50%,100);
		height: 1vh;
		width: 1px;       
	}

    .login_form_btn {
    border-radius: 4px;
    background-color: #6b36f4;
    color: #FFF;
    width: 180px;
}

.btn {
    /* margin-right: 4px; */
    /* margin-bottom: 4px; */
    font-family: "Droid Sans", Arial, sans-serif;
    font-size: 14px;
    font-weight: 400;
    -webkit-border-radius: 4px;
    -moz-border-radius: 4px;
    -ms-border-radius: 4px;
    border-radius: 4px;
    -webkit-transition: 0.5s;
    -o-transition: 0.5s;
    transition: 0.5s;
    padding: 8px 30px;
}

	.partitioned {
	  padding-left: 15px;
	  letter-spacing: 22px;
	  border: 0;
	  background-image: linear-gradient(to left, black 70%, rgba(255, 255, 255, 0) 0%);
	  background-position: bottom;
	  background-size: 31px 1px;
	  background-repeat: repeat-x;
	  background-position-x: 30px;
	  width: 220px;
	  min-width:220px;
	}

	#divInner{
	  left: 0;
	  position: sticky;
	}

	#divOuter{
	  width:190px; 
	  overflow:hidden
	}
	
	.vl {
	  border-left: 1px solid;
	  height: 100px;
	  position: absolute;
	  left: 49%;
	}
	.sgn-select{
	  margin-top:-1vh;		
	  min-width: 9em;
	  /*position: relative;*/
	  display: inline-block;
	  margin-right: 1em;
	  min-height: 2em;
	  max-height:3em;
	  /*overflow:hidden;*/
	  top: .5em;  
	  cursor: pointer;
	  text-align: left;
	  white-space: nowrap;
	  color: #e0e0d1;
	  outline: none;
	  border: -0.94em solid transparent;
	  border-radius: 0.5em;
	}
	
	#signup_submit:hover{
		background: #8533ff !important;
		color:white !important;
	}
	
	.sign-star{
		margin-top:-10px !important;
	}
	
	.sign-xebra{
		padding-left:20px !important;
	}
	
	.sign-logo{
		margin-left:-6px !important;
	}
	
	.high{
		border:1px solid red !important;
		border-radius:5px !important;
	}
	
	.login_form_textbox label.error {
		margin: 35px 0 0 0 !important;
	}
</style>

	<!-- <div class="container-fluid">
      <div class="row">
        <div class="container">
          <div class="row">
			<div class="col-md-12">
              <div class="row">
				<div class="col-md-3 part1">
					<div id="footer_side_wall">
						<img src="<?php //echo //site_url('public/images/footer_2.png'); ?>" class="logo_style">
						<br><br>
						<p class="para_style">Easyinvoces.com allows you to create invoices, file GST returns and gather bussiness intelligence</p>
					</div>
                    
                    <p class="p_tag">
						<small class="block">Copyrights &copy; 2016 - 17. All Rights Reserved</small>
					</p>
				</div>
				<div class="col-md-3 part2">
                    <h5>EAZY INVOICES<sup>TM</sup></h5>
                    <hr>
                    <ul class="ubea-footer-links">
                        <li><a href="<?php //echo base_url(); ?>Home1/about_us">ABOUT US</a></li>
                        <li><a>REFER AN ENTREPRENEUR</a></li>
                        <li><a href="<?php // echo base_url(); ?>Home1/copyrights">COPYRIGHTS & OUR PARTNERS</a></li>
                    </ul>
				</div>
				<div class="col-md-4 part3">
                    <h5>POLICIES</h5>
                    <hr>
                    <ul class="ubea-footer-links">
                        <li><a href="<?php // echo base_url(); ?>Home1/billing_and_upgrade">PRICING PLANS </a></li>
                        <li><a>DOWNLOAD EAZY INVOICES<sup>TM</sup> TO YOUR SERVER</a></li>
                        <li><a href="<?php // echo base_url(); ?>Home1/data_security">DATA SECURITY AND BACK UP</a></li>
                    </ul>
				</div>
				<div class="col-md-2">
                    <h5>OTHERS</h5>
                    <hr>
                    <ul class="ubea-footer-links">
                        <li><a>FEEDBACK</a></li>
                        <li><a href="<?php // echo base_url(); ?>Home1/faqs">FAQs</a></li>
                        <li><a>SITEMAP</a></li>
                        <li><a href="<?php //echo base_url(); ?>Home1/accounting_terms">ACCOUNTING TERMS</a></li>
                    </ul>
				</div>
              </div>
			</div>
          </div>
        </div>
	  </div>
    </div> -->
	</footer>
 </div> 
</div> 

    
    <!-- Login model for client-->
    
    <div id="client_login_modal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg client_login_form">
            <!-- Modal content-->
            <div class="container">
                <div class="row">
                <div class="col-md-5 login_page_2">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">×</button>
                            <h4 class="modal-title"><img src="<?php echo base_url(); ?>public/images/header_1.png" alt="Logo" class="logo_style_2"/></h4>
                            <label class="logo_line">Most Social Invoicing & Business Intelligence Platform</label>
                        </div>
                        <div class="modal-body">

                            <form action="" id="client_login_frm" name="client_login_frm" class="" method="post" accept-charset="utf-8" novalidate="true">
								<?php $csrf = array(
					'name' => $this->security->get_csrf_token_name(),
					'hash' => $this->security->get_csrf_hash()
				);
				?>
				<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
                            <div class="row">
									<?php //echo $messeges; ?>
                                    <div class="col-lg-12 error_cls">
                                    	<div id="error_popup" name="error_popup">
                            			</div>
                                    </div>
                                <div class="col-lg-12 login_form_textbox">
                                    <div class="input-group">
                                      <span class="input-group-addon"><i class="fa fa-user-o fa-lg"></i></span>
                                      <input class="form-control" name="client_username" id="client_username" type="text" placeholder="example@companyname.com">
                                    </div>
                                    <div class="req_star_textbx"><font style="color:red">*</font></div>
                                </div>
                                <div class="col-lg-12 login_form_textbox">
                                   <div class="input-group">
                                          <span class="input-group-addon"><i class="fa fa-lock fa-2x"></i></span>
                                          <input class="form-control show_password" name="client_password" id="client_password" type="password" placeholder="Password(AlphaNumeric - min. 8 characters)">
                                    </div>
                                     <div class="req_star_textbx"><font style="color:red">*</font></div>
                                </div>
                                
                                <div class="col-lg-12 login_form_textbox">
                                  <div class="col-lg-6 login_rem_2">
                                        <input type="checkbox" class="form_checkbox show_pass_check" id="show_paas"> 
                                        <div class="login_rem">Show password</div>
                                    </div>
                                  </div>
                         
                                  <div class="col-lg-12">
                                	<div class="col-lg-6">
                                    	<div class="form-group login_btn_1">
                                        	 <input type="submit"  class="btn login_form_btn" value="LOG IN" name="login_submit" id="login_submit" >
                                     	</div>
                                    </div>
                                    <div class="col-lg-6">
                                    	 <div class="form-group login_btn_1">
                                        <input type="button"  class="btn login_form_btn" value="Cancel" name="hide_client_login" id="hide_client_login" onclick="hide_client_popup()">
                                    </div>
                                    </div>
										<hr>
                                </div>
                             </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
	</div>
    
    <!-- \ Login model for client-->

 <div id="login_otp_modal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg login_otp_form">
            <!-- Modal content-->
            <div class="container">
                <div class="row">
                <div class="col-md-5 login_page_2">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">×</button>
                            <h4 class="modal-title"><img src="<?php echo base_url(); ?>public/images/header_1.png" alt="Logo" class="logo_style_2"/></h4>
                            <label class="logo_line">Most Social Invoicing & Business Intelligence Platform</label>
                        </div>
                        <div class="modal-body">
                            
                            <form action="" id="login_otp_frm" name="login_otp_frm" class="" method="post" accept-charset="utf-8" novalidate="true">
							<?php $csrf = array(
					'name' => $this->security->get_csrf_token_name(),
					'hash' => $this->security->get_csrf_hash()
				);
				?>
				<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
                            <div class="row">
                            		<div class="popup_top_text">We take data security very seriously</div>
                                     <div class="popup_top_text_2">One-time Password will be shared on your registered email address and mobile number</div>
									<?php //echo $messeges; ?>
                                    <div class="col-lg-12 error_cls">
                                    	<div id="error_popup" name="error_popup">
                            			</div>
                                    </div>
                                <div class="col-lg-12 login_form_textbox">
                                    <div class="input-group">
                                      <span class="input-group-addon"><i class="fa  fa-arrow-right fa-lg"></i></span>
                                      <input class="form-control" name="login_phone_otp" id="login_phone_otp" type="text" placeholder="Enter OTP">
                                    </div>
                                     <div class="req_star_textbx"><font style="color:red">*</font></div>
									<label for="login_otp" class="error" id="login_otp-error" ></label>
                                </div>
                                
									
                                    
                                    <?php /*?><div class="col-lg-12 login_form_textbox">
                                        <div class="input-group">
                                          <span class="input-group-addon"><i class="fa  fa-arrow-right fa-lg"></i></span>
                                          <input class="form-control" name="login_email_otp" id="login_email_otp" type="text" placeholder="Email OTP">
                                        </div>
                                     <div class="req_star_textbx"><font style="color:red">*</font></div>
									<label for="login_otp" class="error" id="login_otp-error" ></label>
                                </div><?php */?>
                         			<span  id="otp_error"  class="error pass_2" style="display:none;">Your OTP did not match. Please enter correct OTP.</span>
                                    
                                <div class="col-lg-12">
                                	<div class="col-lg-6">
                                    	<div class="form-group login_btn_1">
                                        	<input type="submit"  class="btn login_form_btn" value="Confirm" name="login_otp_submit" id="login_otp_submit" >
                                     	</div>
                                    </div>
                                    <div class="col-lg-6">
                                    	 <div class="form-group login_btn_1">
                                        <input type="button"  class="btn login_form_btn" value="Cancel" name="otp_cancel" id="otp_cancel" onclick="hide_popup()">
                                    </div>
                                    </div>

                                </div>
                             </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
	</div>
  <!-- Pop up Login form End -->  

	<!-- OTP for Client -->
    
    <div id="client_login_otp_modal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg client_login_otp_form">
            <!-- Modal content-->
            <div class="container">
                <div class="row">
                <div class="col-md-5 login_page_2">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">×</button>
                            <h4 class="modal-title"><img src="<?php echo base_url(); ?>public/images/header_1.png" alt="Logo" class="logo_style_2"/></h4>
                            <label class="logo_line">Most Social Invoicing & Business Intelligence Platform</label>
                        </div>
                        <div class="modal-body">
                            
                            <form action="" id="client_login_otp_frm" name="client_login_otp_frm" class="" method="post" accept-charset="utf-8" novalidate="true">
                            <div class="row">
                            		<div class="popup_top_text">We take data security very seriously</div>
                                    <div class="popup_top_text_2">One-time Password will be shared on your registered email address and mobile number</div>
									<?php //echo $messeges; ?>
                                    <div class="col-lg-12 error_cls">
                                    	<div id="error_popup" name="error_popup">
                            			</div>
                                    </div>
                                <div class="col-lg-12 login_form_textbox">
                                    <div class="input-group">
                                      <span class="input-group-addon"><i class="fa fa-arrow-right fa-lg"></i></span>
                                      <input class="form-control" name="client_phone_otp" id="client_phone_otp" type="text" placeholder="Enter OTP">
                                    </div>
                                     <div class="req_star_textbx"><font style="color:red">*</font></div>
									<label for="login_otp" class="error" id="login_otp-error" ></label>
                                </div>
                                
									
                                    
                                    <?php /*?><div class="col-lg-12 login_form_textbox">
                                    <div class="input-group">
                                      <span class="input-group-addon"><i class="fa fa-user-o fa-lg"></i></span>
                                      <input class="form-control" name="client_email_otp" id="client_email_otp" type="text" placeholder="Email OTP">
                                    </div>
                                     <div class="req_star_textbx"><font style="color:red">*</font></div>
									<label for="login_otp" class="error" id="login_otp-error" ></label>
                                </div><?php */?>
                         			<span  id="otp_error"  class="error pass_2" style="display:none;">Your OTP did not match. Please enter correct OTP.</span>
                                    
                                <div class="col-lg-12">
                                	<div class="col-lg-6">
                                    	<div class="form-group login_btn_1">
                                        	<input type="submit"  class="btn login_form_btn" value="Confirm" name="login_otp_submit" id="login_otp_submit" >
                                     	</div>
                                    </div>
                                    <div class="col-lg-6">
                                    	 <div class="form-group login_btn_1">
                                        <input type="button"  class="btn login_form_btn" value="Cancel" name="otp_cancel" id="otp_cancel" onclick="hide_client_popup()">
                                    </div>
                                    </div>

                                </div>
                             </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
	</div>
    
    <!--OTP for Client -- >
   
  <!-- Pop up SIGN UP form start -->
    <div id="signup_modal" class="modal fade" role="dialog" style="margin-top:20px !important;">
        <div class="modal-dialog modal-lg signup_form">
            <!-- Modal content-->
            <div class="container">
                <div class="row signup_form">
                <div class="signup_frm" style="width: 45%; padding-left: 120px;">
                    <div class="modal-content" style="box-shadow: none;">
                        <div style="text-align:center; margin-top:10px;">
                            <!--<button type="button" class="close" data-dismiss="modal">×</button>-->
                            <!--<h4 class="modal-title"><img src="<?php echo base_url(); ?>public/images/header_1.png" alt="Logo" class="logo_style_3_popup"/></h4>
                            <label class="logo_line_2">Most Social Invoicing & Business Intelligence Platform</label>
							
							<h4><b>Eazy</b>Invoice </h4>-->
   						    <!--<img width="200" height="500" src="<?php echo base_url(); ?>public/images/Eazy-Invoice-Logo.png"  alt="Logo" class="logo_style_2"/>-->
							<img width="205" height="55" src="<?php echo base_url(); ?>public/images/xebra-logo.png" alt="Logo" class="logo_style_2 sign-logo"/>
							
							<label class="logo_line_2" style="font-size:15px; font-family: 'Droid Sans', Arial, sans-serif; margin-top:5px !important;">No credit card required. Cancel Anytime.</label>
						</div>
                        <div class="modal-body" style="margin-bottom:-30px !important; margin-top:-12px !important;">
                            <!--<div>
                            <label class="logo_line_2">Please register to make invoicing incredibly easy!</label>
                            </div>-->
                            
                            <form action="" id="signup_frm" name="signup_frm" class="" method="post" accept-charset="utf-8" novalidate="true" style="margin:0px 15px 0px 15px !important;">
                            <div class="row">
                           		<div class="col-lg-12 error_cls">
									<div id="error_popup" name="error_popup">
                            		</div>
                                </div>
                                <div class="col-lg-12 login_form_textbox">
                                    <div class="input-group name-re">
                                      <span class="input-group-addon"><i class="fa fa-user-o fa-lg"></i></span>
                                      <input class="form-control" name="name" id="name" type="text" placeholder="NAME">
                                    </div>
                                    <div class="req_star_textbx sign-star"><font style="color:red">*</font></div>
                                </div>
								
                                <div class="col-lg-12 login_form_textbox">
                                    <div class="input-group email-re">
                                      <span class="input-group-addon"><i class="fa fa-envelope-o"></i></span>
                                      <input class="form-control Web_email" name="email" id="email" type="text" placeholder="EMAIL">
                                    </div>
                                    <div class="req_star_textbx sign-star"><font style="color:red">*</font></div>
                                </div>
                                <div class="col-lg-12 login_form_textbox">
                                   <div class="input-group pass-wrng" style="border:1px solid #ababab; border-radius:4px;">
                                          <span class="input-group-addon" style="padding: 6px 13px !important; border:none !important; border-right:1px solid #ababab !important;"><i class="fa fa-lock fa-2x"></i></span>
                                          <input style="width:82%; border:none !important;" class="form-control password_strength show_password" name="password_2" id="password_2" type="password" placeholder="PASSWORD" onblur="alphanumeric(document.signup_frm.password_2)">
                                    </div>
									<div class="hide-show">
										<span style="position:absolute; left:260px; top:5px; cursor: pointer;"><img width="29" height="30" src="<?php echo base_url(); ?>public/images/no-monkey.png" alt="small-eazy"></img></span>
									</div>
                                    <div class="req_star_textbx sign-star"><font style="color:red">*</font></div>
                                </div>
								<!--
								<div class="col-lg-12 login_form_textbox"> 
									 <div class="input-group" style="border:1px solid #ababab; border-radius:4px;">
                                        <span class="input-group-addon" style="padding: 6px 13px !important; border:none !important; border-right:1px solid #ababab !important;"><i class="fa fa-lock fa-2x"></i></span>
                                        <input style="width:82%; border:none !important;" class="form-control show_password" name="password" id="password" type="password" placeholder="PASSWORD"/>
										<div class="hide-show">
											<span style="position:absolute; left:240px; top:10px;">Show</span>
										</div>
									 </div>
                                     <!--<div class="req_star_textbx"><font style="color:red">*</font></div>-->
                                  <!--</div>-->
								  
								  <div class="col-lg-12 login_form_textbox" style="margin-top:-12px; margin-bottom:-12px !important;">
									<label style="font-size:10.2px !important; text-align:left;">Min 8 Characters, 1 Uppercase, 1 Number & 1 Special Character</label>
                                  </div>
								  
								<div class="col-lg-12">
                                 <!--span id="pwdMeter" class="neutral"></span-->
								</div>
									<!--span  id="invalid_password_alpha"  class="error pass_2" style="display:none;">Please Enter Alphanumeric Values and Special Character for Password</span-->
								<!--<div class="col-lg-12 login_form_textbox" style="display: block;">
                                  <div class="col-lg-6 login_rem_2">
                                        <input type="checkbox" class="form_checkbox show_pass_check" id="show_paas"> 
                                        <div class="login_rem">Show password</div>
                                    </div>
                                  </div>-->
                                
								<!--<div class="col-lg-12 input-set border-none">
									<div class="input-field">
										<label for="country" class="full-bg-label select-label">Country<span class="required_field"> *</span></label>
										<select class="js-example-basic-single" name="reg_admin_type" id="reg_admin_type">
										<option value="">Country *</option>
											<?php foreach($access_list as $access){ ?> 
												<option value="<?php echo $access->id; ?>" ><?php echo $access->access; ?></option>
											<?php } ?>
										</select>
									</div>
								</div>-->
															  
                                <div class="col-lg-12 login_form_textbox">
                                   <div class="input-group con-re">
                                          <span class="input-group-addon"><i class="fa fa-refresh fa-lg" style="font-size:15px;"></i></span>
                                          <input class="form-control" name="confirm_password" id="confirm_password" type="password" placeholder="CONFIRM PASSWORD">
                                    </div>
                                    <div class="req_star_textbx sign-star"><font style="color:red">*</font></div>
                                </div>
								
								<div class="col-lg-12 login_form_textbox">
                                   <div class="input-group mb-re">
                                          <span class="input-group-addon"><i class="fa fa-mobile-phone" style="font-size:26px"></i><!--<hr>--></span>
                                          <input class="form-control" name="contact" id="contact" type="text" placeholder="8945612354" maxlength="10">
                                    </div>
                                    <div class="req_star_textbx sign-star"><font style="color:red">*</font></div>
                                </div>
								
								
									<!--<div class="col-sm-5" style="margin-bottom:15px; margin-top:11px;">
										<select class="sgn-select md-form" >
										  <option value="" disabled selected>Choose Country</option>
										  <option value="1">Option 1</option>
										  <option value="2">Option 2</option>
										  <option value="3">Option 3</option>
										</select>
									</div>
									
									<div class="col-sm-5" style="margin-left:22px; margin-top:11px;">
										<select class="sgn-select md-form">
										  <option value="" disabled selected>Choose your State</option>
										  <option value="1">Option 1</option>
										  <option value="2">Option 2</option>
										  <option value="3">Option 3</option>
										</select>
									</div>-->
								

                                
                                    <!--<div class="col-lg-12 login_rem_2 login_form_textbox" style="padding-left:7px !important;">
                                        <input type="checkbox" class="form_checkbox" name="subscribe" id="subscribe" value="1"/>
                                        <div class="signup_newsletter">Subscribe to the newsletter</div>
                                    </div>-->
                                   
                                    <!--<div class="col-lg-12 login_form_textbox">
                                    	<div>By Signing Up, You are agreed to EazyInvoice's <div class="terms_condition"><a href="<?php echo base_url();?>Home1/terms_and_conditions">Terms and Conditions</a></div> and <div class="terms_condition"><a href="<?php echo base_url();?>Home1/privacy_policy">Privacy Policy.</a></div>
                                        </div>
                                    </div>--> 
                                 
                                <div class="col-lg-12 form-group login_btn_1">
                                    <input type="submit" class="btn login_form_btn sign-xebra" value="SECURE SIGNUP" name="signup_submit" id="signup_submit"><i class="fa fa-lock fa-2x" style="color:#fff; margin-left:-35px;"></i>
                                </div>
                                
                             </div>
                            </form>
                        </div>
						<div class="modal-footer" style="background-color:#f0f0f5; margin-top:10px; padding:20px 10px; border-radius:5px;">
							<div class="col-lg-12">
								<div class="login_signup_link" style="font-size:13px;">Already have an account?  <a href="#" onclick="open_login()" style="color:#7965E9;">  <b>Log In</b></a></div>
                            </div>
						</div>
                    </div>
                </div>
            </div>
        </div>
        </div>
	</div>
   <!-- Pop up SIGN UP form start --> 
   	<!--  Forgot Password Page-->
   	<div id="forgotpassword_modal" class="modal fade" role="dialog" style="margin-top:80px;">
        <div class="modal-dialog modal-lg forgotpassword_form">
            <!-- Modal content-->
            <div class="container">
                <div class="row signup_form">
                <div class="col-md-offset-1 col-md-5 forgot_pass">
                    <div class="modal-content">
						<div class="col-lg-12">
                        <div class="" style="text-align:center; margin-top:20px;">
                            <!--<button type="button" class="close" data-dismiss="modal" style="margin-right:9px;">×</button>-->
                            <!--<h4 class="modal-title"><img src="<?php echo base_url(); ?>public/images/header_1.png" alt="Logo" class="logo_style_3_popup"/></h4>
                            <label class="logo_line">Most Social Invoicing & Business Intelligence Platform</label>-->
							<p style="font-size:18px; font-family: Verdana,sans-serif; color:black;"><b> Forgot Password </b></p>
							<p class="logo_line_2" style="font-size:13px; color:#696969;"> To reset the password, enter the email address that you use to log in to <b style="font-size:13px; color:black;">Xebra Cohorts </b> A link will be emailed to this address which will let you reset your password.</p>
                        </div>
						</div>
                        <div class="modal-body" style="margin-top:135px;">
                        <div class="modal-loader" style="display: none;"></div>
                          <form action="" id="forgot_password_frm" name="forgot_password_frm" class="" method="post" accept-charset="utf-8" novalidate="true" style="margin-bottom:-10px !important;">
                            <div class="row">
                                <!--<div class="col-lg-12">
                                	<div class="input-group">
                                    	<label>Please Enter Your Email ID</label>
                                    </div>
                                </div>-->
								<div class="col-lg-12">
									<div class="col-lg-12 login_form_textbox">
                                    <div class="input-group forgot_email">
										<span class="input-group-addon"><i class="fa fa-envelope-o"></i></span>
                                        <input style="height:34px; border:1px solid rgba(0, 0, 0, 0.1); font-size:13px;" class="form-control" name="get_email" id="get_email" type="email" placeholder="EMAIL" required autocomplete="off">
                                    </div>
									</div>
                                </div>
                                <!--<div class="col-lg-12 login_form_textbox">
                                    <div class="input-group forgot_email">
                                      <span class="input-group-addon"><i class="fa fa-at fa-lg"></i></span>
                                      <input class="form-control" name="get_email" id="get_email" type="email" placeholder="example@companyname.com" required autocomplete="off">
                                    </div>
                                    <div class="req_star_textbx"><font style="color:red">*</font></div>
                                </div>-->
                                <div class="col-lg-12 error_cls" style="padding-bottom:25px !important;">
                                    <div id="error_popup" name="error_popup"></div>
                                </div>
                                <div class="col-lg-12">
                                <div class="col-lg-6">
                                    <div class="form-group login_btn_1">
                                        <input style="background-color:#7863e8 !important; font-size:13px !important;" onMouseOver="this.style.color='white'" type="submit" class="btn login_form_btn" value="SEND LINK" name="get_password" id="get_password">
                                    </div>
                                 </div>
                                 <div class="col-lg-6" style="margin-top:4px;">
                                    <div class="form-group login_btn_1">
                                        <b>&nbsp;&nbsp; or</b> &nbsp; <a onclick="open_login()" style="cursor: pointer; font-size:15px; color:#7965E9;"><b>Cancel</b></a>
                                    </div>
                                 </div>   
                                </div>
                               <hr>  
                             </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
	</div>    <!-- send_otp_modelwidth: 75%;
		padding-left: 0px;
		margin-left: 2%;-->
    <div id="send_otp_model" class="modal fade" role="dialog" style="margin-top:20px;">
        <div class="modal-dialog modal-lg otp_form">
            <!-- Modal content-->
            <div class="container">
                <div class="row otp_first">
                <div class="col-md-offset-1 col-md-5 forgot_pass">
                    <div class="modal-content">
                        <div class="" style="text-align:center; margin-top:15px;">
                            <button style="margin-right:10px; margin-top:2px;" type="button" class="close" data-dismiss="modal">×</button>
                            <!--<h4 class="modal-title"><img src="<?php echo base_url(); ?>public/images/header_1.png" alt="Logo" class="logo_style_3_popup"/></h4>
                            <label class="logo_line">Most Social Invoicing & Business Intelligence Platform</label>-->
							<!--<img width="200" height="500" src="<?php echo base_url(); ?>public/images/Eazy-Invoice-Logo.png" alt="Logo" class="logo_style_2"/><br>-->
							<img width="205" height="55" src="<?php echo base_url(); ?>public/images/xebra-logo.png" alt="Logo" class="logo_style_2"/><br>
							<label class="logo_line_2" style="font-size:15px;">We take data security very seriously</label>
						</div>
                        <div class="modal-body">
                            <div class="modal-loader" style="display: none;"></div>
             
                            <form action="" id="otp_frm" name="otp_frm" class="" method="post" accept-charset="utf-8" novalidate="true">
                            <div class="row">
                            <div class="popup_top_text_2" style="color:#8533ff;">Please enter two separate 6-digit OTP Sent on your email Id and your mobile no. We do this double confirmation on your first log-in and every time you change your password 
							<!--One-time Password will be shared on your registered email address and mobile number-->
							</div>
										
								<div class="col-lg-12" style="margin-top:6px;">
									<!--<div class="col-lg-1"></div>-->
									<div class="col-lg-2"></div>
									<div class="col-lg-4">
										<p style="text-align:center;"><i class="fa fa-desktop" style="font-size:20px; margin-bottom:5px !important;"></i></p>
										<div id="divOuter">
											<div id="divInner">
												<input class="partitioned" name="verification_code" id="verification_code" type="text" maxlength="6" autocomplete="off"/>
											</div>
										</div>
									</div>
									<div class="vl"></div>
									<div class="col-lg-4">
										<p style="text-align:center;"><i class="fa fa-mobile" style="font-size:24px;"></i></p>
										<div id="divOuter">
											<div id="divInner">
												<input class="partitioned" id="mbl_code" name="mbl_code" type="text" maxlength="6" />
											</div>
										</div>
									</div>
									<div class="col-lg-2"></div>
									<!--<div class="col-lg-1"></div>-->
								</div>
								<!--<div class="col-lg-12">
                                	<div class="input-group">
                                    	<label>Please Enter Verification Code</label>
                                    </div>
                                </div>
                                <div class="col-lg-12 login_form_textbox">
                                    <div class="input-group forgot_email">
                                      <span class="input-group-addon"><i class="fa fa-edit fa-lg"></i></span>
                                      <input class="form-control" name="verification_code" id="verification_code" type="text" placeholder="Verification Code" required autocomplete="off">
                                    </div>
                                    <div class="req_star_textbx"><font style="color:red">*</font></div>
                                </div>-->
								
								
                                <!--<span  id="otp_error"  class="error pass_2" style="display:none;">OTP Not match. Please Insert valid OTP.</span>-->
                                <div class="col-lg-12 error_cls">
                                    	<div id="error_popup" name="error_popup">
                            			</div>
                                    </div>
                                <div class="col-lg-12">
								<div class="col-lg-3"></div>
                                <div class="col-lg-6">
                                    <div class="form-group login_btn_1" style="text-align:center; margin-right:10px; margin-top:30px;">
                                        <input type="submit"  class="btn login_form_btn" value="RE-SEND EMAIL" name="submit" id="submit">
                                       
                                    </div>
                                    <div class="form-group login_btn_1" style="text-align:center; margin-right:10px; margin-top:30px;">
                                        <input type="submit"  class="btn login_form_btn" value="VERIFY" name="submit" id="submit">
                                       
                                    </div>
                                    <div class="form-group login_btn_1" style="text-align:center; margin-right:10px; margin-top:30px;">
                                        <input type="submit"  class="btn login_form_btn" value="RE-SEND MOBILE" name="submit" id="submit">
                                       
                                    </div>
                                </div>
								<div class="col-lg-3"></div>
                                 <!--<div class="col-lg-6">
                                    <div class="form-group login_btn_1">
                                        <input type="button"  class="btn login_form_btn" value="BACK" onclick="open_login()">
                                    </div>-->
                                 </div>   
                                </div>
                             </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
	</div>
    
	<div id="reset_password_email" class="modal fade" role="dialog" style="height:auto !important; margin-top:30px; margin-left:150px; padding:0px !important;">
		<div class="modal-dialog modal-lg reset_pass_form">
            <div class="container">
                <div class="row conf_pass">
                <div class="col-md-offset-1 col-md-5 forgot_pass">
                    <div class="modal-content">
						<div class="col-lg-12" style="text-align:right; margin-top:20px; font-family: Verdana,sans-serif !important; color:black;">
							<a class="exp_close modal-close close-pop" style="cursor: pointer; margin-right:0px !important;"><img src="<?php echo base_url();?>asset/images/popupdelete1.png" alt="delete"></a>
						</div> 	
						<div class="col-lg-12" style="text-align:center; margin-top:-25px; font-family: Verdana,sans-serif !important; color:black;">
							<p style="font-size:18px;"><b>Please check your email</b></p>
						</div> 	
                        <div class="modal-body">
                            <div class="modal-loader" style="display: none;"></div>
                            <form action="" id="resend_password_frm" name="resend_password_frm" class="" method="post" accept-charset="utf-8" novalidate="true">
								<div class="row" style="text-align:center;">
									<img alt="receive-email" class="columnImage" width="130" height="130" src="<?php echo base_url(); ?>asset/images/email/receive-email.png" border="0"/>	
								</div>
								<div style="text-align:center;">
									<p style="font-size:15px; margin-bottom:8px !important;">A link to reset your password has been set to:<br><label id="email_cust" style="color:#8533ff;"><b></b></label></p>
								</div>
								<div style="text-align:center; font-size:15px; width:336px; margin-left:11px;">
									<p>If you don't receive an email shortly, check your 'bulk email' or 'junk email' folders. To make sure you receive email from <b>Xebra</b> in the future, add the <b>'xebra.in'</b> domain to your email safe list</p>
								</div>
								<div style="text-align:center;">
                                    <input type="hidden" name="get_email" id="get_email">
									<input style="background-color:#7863e8 !important; font-size:13px !important;" onMouseOver="this.style.color='white'" type="submit" class="btn login_form_btn" value="RESEND EMAIL" name="resend-email" id="resend-email">
								</div>
                            </form>
                        </div>
                    </div>
                </div>
				</div>
			</div>
        </div>
	</div>

    <div id="resend_password_email" class="modal fade" role="dialog" style="height:auto !important; margin-top:30px; margin-left:150px; padding:0px !important;">
        <div class="modal-dialog modal-lg reset_pass_form">
            <div class="container">
                <div class="row conf_pass">
                <div class="col-md-offset-1 col-md-5 forgot_pass">
                    <div class="modal-content">
                        <div class="col-lg-12" style="text-align:right; margin-top:20px; font-family: Verdana,sans-serif !important; color:black;">
                            <a class="exp_close modal-close close-pop" style="cursor: pointer; margin-right:0px !important;"><img src="<?php echo base_url();?>asset/images/popupdelete1.png" alt="delete"></a>
                        </div>  
                        <div class="col-lg-12" style="text-align:center; margin-top:-25px; font-family: Verdana,sans-serif !important; color:black;">
                            <p style="font-size:18px;"><b>Please check your email</b></p>
                        </div>  
                        <div class="modal-body">
                            <div class="modal-loader" style="display: none;"></div>
                            <form action="" id="resend_password_frm" name="resend_password_frm" class="" method="post" accept-charset="utf-8" novalidate="true">
                                <div class="row" style="text-align:center;">
                                    <img alt="receive-email" class="columnImage" width="130" height="130" src="<?php echo base_url(); ?>asset/images/email/receive-email.png" border="0"/>  
                                </div>
                                <div style="text-align:center;">
                                    <p style="font-size:15px; margin-bottom:8px !important;">A link to reset your password has been resent to:<br><label id="email_cust" style="color:#8533ff;"><b></b></label></p>
                                </div>
                                <div style="text-align:center; font-size:15px; width:336px; margin-left:11px;">
                                    <p>If you don't receive an email shortly, check your 'bulk email' or 'junk email' folders. To make sure you receive email from <b>Xebra</b> in the future, add the <b>'xebra.in'</b> domain to your email safe list</p>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
	
    
    
    <div id="success_model" class="modal fade" role="dialog" style="margin-top:50px;">
        <div class="modal-dialog modal-lg reset_pass_form">
            <!-- Modal content-->
            <div class="container">
                <div class="row signup_form">
                <div class="col-md-offset-1 col-md-5 forgot_pass">
                    <div class="modal-content">
                        <div class="" style="margin-top:15px; text-align:center;">
                            <button type="button" class="close" data-dismiss="modal" style="margin-right:10px;">×</button>
                            <!--<h4 class="modal-title"><img src="<?php echo base_url(); ?>public/images/header_1.png" alt="Logo" class="logo_style_3_popup"/></h4>-->
							<!--<img width="200" height="500" src="<?php echo base_url(); ?>public/images/Eazy-Invoice-Logo.png" alt="Logo" class="logo_style_2"/>-->
							<img width="205" height="55" src="<?php echo base_url(); ?>public/images/xebra-logo.png" alt="Logo" class="logo_style_2"/>
                            <!--<label class="logo_line">Most Social Invoicing & Business Intelligence Platform</label>-->
                        </div>
                        <div class="modal-body">
                            <form action="" id="success_frm" name="success_frm" class="" method="post" accept-charset="utf-8" novalidate="true">
                            <div class="row">
                           	   <div class="col-lg-12 error_cls">
                                    	<div id="" name="">
                                        	<div>Your Password Has Been Changed Successfully</div>
                            			</div>
                                    </div>
                                <div class="col-lg-12" style="margin-top:15px; text-align:center;">
                                    <div class="form-group login_btn_1">
                                        <input type="button" class="btn login_form_btn" value="GO TO LOGIN" onclick="open_login()" style="font-size:15px;">
                                    </div>
                                </div>
                               <hr>  
                             </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
	</div>
    
    <!-- ----- Start Reactive----------- -->
<!--      <div id="reactive_bus" class="modal modal-set">
       <img class="geen" src="<?php echo base_url(); ?>asset/images/green.png">
       <div class="modal-content">
          <div class="modal-header">
             <h4>Reactivate Account</h4>
             <a class="modal-close close-pop"><img src="<?php echo base_url(); ?>asset/images/popupdelete.png"></a>
          </div>
       </div>
       <div class="modal-body confirma">
          <p class="ml-20">Are you sure you want to Reactivate your Account ?</p>
       </div>
       <div class="modal-content">
          <div class="modal-footer">
             <div class="row">
                 <div class="col l4 s12 m12"></div> text-right
                <div class="col l12 s12 m12 cancel-deactiv ">
                   <a class=" modal-close btn-flat theme-flat-btn theme-btn theme-btn-large model-cancel " type="button">CANCEL</a>
                   <a class="btn-flat theme-primary-btn theme-btn theme-btn-large  modal-trigger dea modal-close reactive_my_account centertext">CONFIRM</a>
                </div>
             </div>
          </div>
       </div>
    </div>  -->

    <div id="reactive_bus" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg forgotpassword_form">
            <!-- Modal content-->
            <div class="container">
                <div class="row signup_form">
                <div class="col-md-offset-1 col-md-5 forgot_pass">
                    <div class="modal-content">
                        <div class="modal-body">
                        <div class="modal-loader" style="display: none;"></div>     
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="input-group">
                                        <label>Are you sure you want to Reactivate your Account ?</label>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                <div class="col-lg-6">
                                    <div class="form-group login_btn_1">
                                        <a class="btn-flat theme-primary-btn theme-btn theme-btn-large  modal-trigger dea modal-close reactive_bus_acc centertext">CONFIRM</a>
                                    </div>
                                 </div>
                                 <div class="col-lg-6">
                                    <div class="form-group login_btn_1">
                                        <a class=" modal-close btn-flat theme-flat-btn theme-btn theme-btn-large model-cancel cancel_reactive_bus_acc">CANCEL</a>
                                    </div>
                                 </div>   
                                </div>
                               <hr>  
                             </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>
    <!-- ----- End Reactivate----------- -->

    <!-- Check For OTP-->
    	<div id="successful_signup_model" class="modal fade" role="dialog" style="margin-top:50px;">
        <style>
			@media screen and (-webkit-min-device-pixel-ratio:0){
				::i-block-chrome,.partitioned{
					border: 1px solid black;
				}
			}	
			@media only screen and (max-width: 600px) {
				#successful_signup_model .modal-content
					width: 330px !important;
					margin: 0 0 0 -65px !important;
				}
			}	
			.login_page_2 {
				padding-left: 54px;
				margin-left: 9.6%;
				width: 57% !important;
			}
		</style>
		<div class="modal-dialog modal-lg otp_check_form">
            <!-- Modal content-->
            <div class="container">
                <div class="row">
                <div class="col-md-5 login_page_2">
                    <div class="modal-content">
                        <div class="" style="text-align:center; margin-top:25px;">
                            <!--<h4 class="modal-title"><img src="<?php echo base_url(); ?>public/images/header_1.png" alt="Logo" class="logo_style_2"/></h4>
                            <label class="logo_line">Most Social Invoicing & Business Intelligence Platform</label>-->
							<!--<img width="200" height="500" src="<?php echo base_url(); ?>public/images/Eazy-Invoice-Logo.png" alt="Logo" class="logo_style_2"/>-->
							<img width="185" height="75" src="<?php echo base_url(); ?>public/images/xebracohorts_c.png" alt="xebra-logo" class="logo_style_2"/>
                        </div>
                        <div class="modal-body">
                            <form action="" id="check_for_otp" name="check_for_otp" class="" method="post" accept-charset="utf-8" novalidate="true">
                            <div class="row">
								<div class="popup_top_text">We take data security very seriously</div>
                                <div class="popup_top_text_2" style="color:#8533ff !important;"> Please enter 6-digit OTP Sent on your email id</span></div>
								<!--One-time Password will be shared on your registered mobile number & Mail Address-->
									<?php //echo $messeges; ?>
                                    <div class="col-lg-12">
                                    	<div id="otp_error_popup" name="error_popup" style="font-size: 14px; color: red; text-align:center;">
                            			</div>
                                    </div>
                                    
                                <!--<div class="col-lg-12 login_form_textbox" style="margin-top:13px;">
                                    <div class="input-group">
                                      <span class="input-group-addon"><i class="fa fa-arrow-right fa-lg"></i></span>
                                      <input style="height:34px; border:1px solid rgba(0, 0, 0, 0.1); font-size:13px;" class="form-control" name="phone_otp" id="phone_otp" type="text" placeholder="Enter 6 Digit OTP" autocomplete="off">
                                      <input type="hidden" name="reg_id" id="reg_id_otp" value="">
                                    </div>
                                    <div class="req_star_textbx"><font style="color:red">*</font></div>-->
                                <!--</div>-->
								<input type="hidden" name="reg_id" id="reg_id_otp" value="">
								<div class="col-lg-12 login_form_textbox" style="margin-top:13px;">
									<!--<div class="col-lg-1"></div>-->
									<div class="col-lg-4"></div>
									<div class="col-lg-4">
										<p style="margin-left: 18%; text-align:center;"><i class="fa fa-desktop" style="font-size:20px; margin-top:4px !important;"></i></p>
										<div id="divOuter">
											<div id="divInner">
												<input class="partitioned" style="letter-spacing:24px;" name="phone_otp" id="phone_otp" type="text" maxlength="6" autocomplete="off"/>
											</div>
										</div>
									</div>
									<div class="vl" hidden></div>
									<div class="col-lg-4" hidden>
										<p style="text-align:center;"><i class="fa fa-mobile" style="font-size:24px;"></i></p>
										<div id="divOuter">
											<div id="divInner">
												<input class="partitioned" style="letter-spacing:24px;" name="mbl_code" id="mbl_code" type="text" maxlength="6" />
											</div>
										</div>
									</div>
									<div class="col-lg-2"></div>
									<!--<div class="col-lg-1"></div>-->
								</div>
                                
                                <?php /*?><div class="col-lg-12 login_form_textbox">
                                   <div class="input-group">
                                          <span class="input-group-addon"><i class="fa fa-lock fa-2x"></i></span>
                                          <input class="form-control" name="email_otp" id="email_otp" type="text" placeholder="Email OTP">
                                    </div>
                                     <div class="req_star_textbx"><font style="color:red">*</font></div>
                                </div><?php */?>
                                <div class="col-md-12 text-center" style="padding: 0px 0 0 0; color: #000; font-weight: 600;">
									OTP valid for <span id="otp_timer"></span>
								</div>									
                                <div class="col-lg-12">
									<div class="col-lg-2"></div>
                                    <div class="col-lg-4" style="text-align: center; margin-top: 25px;">
                                        <div class="form-group login_btn_1">
                                            <input type="button" onMouseOver="this.style.color='white'" style="background-color:#8533ff !important;" class="btn login_form_btn" value="RE-SEND EMAIL" name="resend-email-otp" id="resend-email-otp">
                                            <input type="email" name="resend_email" id="resend_email" value="" hidden>
                                            <input type="password" name="resend_password" id="resend_password" value="" hidden>
                                        </div>
                                     </div>

                                     <div class="col-lg-4" style="text-align: center; margin-top: 25px;">
                                        <div class="form-group login_btn_1">
                                            <input type="submit" onMouseOver="this.style.color='white'" style="background-color:#8533ff !important;" class="btn login_form_btn co-otp-submit" value="VERIFY" name="submit" id="submit">
                                        </div>
                                     </div>

                                     <div class="col-lg-4" style="text-align: center; margin-top: 25px;" hidden>
                                        <div class="form-group login_btn_1">
                                            <input type="submit" onMouseOver="this.style.color='white'" style="background-color:#8533ff !important;" class="btn login_form_btn" value="RE-SEND MOBILE" name="resend-mobile-otp" id="resend-mobile-otp">
                                            <input type="text" class="numeric_number" name="resend_email_mobile" id="resend_email_mobile" value="" hidden>
                                            <input type="password" name="resend_password_mobile" id="resend_password_mobile" value="" hidden>
                                        </div>
                                     </div>
                                </div>
                         
                             </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
	</div>
	
	<div id="successful_signup_model12" class="modal fade" role="dialog" style="margin-top:50px;">
        <style>
			.login_page_co {
				padding-left: 54px;
				margin-left: 9.6%;
				width: 57% !important;
			}
		</style>
		<div class="modal-dialog modal-lg otp_check_form">
            <!-- Modal content-->
            <div class="container">
                <div class="row">
                <div class="col-md-5 login_page_co">
                    <div class="modal-content">
                        <div class="" style="text-align:center; margin-top:15px;">
                            <!--<h4 class="modal-title"><img src="<?php echo base_url(); ?>public/images/header_1.png" alt="Logo" class="logo_style_2"/></h4>
                            <label class="logo_line">Most Social Invoicing & Business Intelligence Platform</label>-->
							<!--<img width="200" height="500" src="<?php echo base_url(); ?>public/images/Eazy-Invoice-Logo.png" alt="Logo" class="logo_style_2"/>-->
							<img width="180" height="75" src="<?php echo base_url(); ?>public/images/cohorts.png" alt="xebra-logo" class="logo_style_2"/>
                        </div>
                        <div class="modal-body">
                            <form action="" id="check_for_otp" name="check_for_otp" class="" method="post" accept-charset="utf-8" novalidate="true">
                            <div class="row">
								<div class="popup_top_text">We take data security very seriously</div>
                                <div class="popup_top_text_2" style="color:#8533ff !important;"> Please enter 6-digit OTP Sent on your email id</span></div>
								<!--One-time Password will be shared on your registered mobile number & Mail Address-->
									<?php //echo $messeges; ?>
                                    <div class="col-lg-12">
                                    	<div id="otp_error_popup" name="error_popup" style="font-size: 14px; color: #f44336; text-align:center;">
                            			</div>
                                    </div>
                                    
								<input type="hidden" name="reg_id" id="reg_id_otp" value="">
								<div class="col-lg-12 login_form_textbox" style="margin-top:13px;">
									<!--<div class="col-lg-1"></div>-->
									<div class="col-lg-4"></div>
									<div class="col-lg-4">
										<p style="margin-left: 18%; text-align:center;"><i class="fa fa-desktop" style="font-size:20px; margin-top:4px !important;"></i></p>
										<div id="divOuter">
											<div id="divInner">
												<input class="partitioned" style="letter-spacing:24px;" name="phone_otp" id="phone_otp" type="text" maxlength="6" autocomplete="off"/>
											</div>
										</div>
									</div>
									<!--div class="vl"></div>
									<div class="col-lg-4">
										<p style="text-align:center;"><i class="fa fa-mobile" style="font-size:24px;"></i></p>
										<div id="divOuter">
											<div id="divInner">
												<input class="partitioned" style="letter-spacing:24px;" name="mbl_code" id="mbl_code" type="text" maxlength="6" />
											</div>
										</div>
									</div-->
									<div class="col-lg-4"></div>
									<!--<div class="col-lg-1"></div>-->
								</div>
                                
                                <div class="col-lg-12">
                                    <div class="col-lg-2"></div>
									<div class="col-lg-4" style="text-align: center; margin-top: 25px;">
                                        <div class="form-group login_btn_1">
                                            <input type="submit" onMouseOver="this.style.color='white'" style="background-color:#8533ff !important;" class="btn login_form_btn" value="RE-SEND EMAIL" name="resend-email-otp" id="resend-email-otp">
                                            <input type="email" name="resend_email" id="resend_email" value="" hidden>
                                            <input type="password" name="resend_password" id="resend_password" value="" hidden>
                                        </div>
                                     </div>

                                     <div class="col-lg-4" style="text-align: center; margin-top: 25px;">
                                        <div class="form-group login_btn_1">
                                            <input type="submit" onMouseOver="this.style.color='white'" style="background-color:#8533ff !important;" class="btn login_form_btn" value="VERIFY" name="submit" id="submit">
                                        </div>
                                     </div>
									 <div class="col-lg-2"></div>

                                     <!--div class="col-lg-4" style="text-align: center; margin-top: 25px;">
                                        <div class="form-group login_btn_1">
                                            <input type="submit" onMouseOver="this.style.color='white'" style="background-color:#8533ff !important;" class="btn login_form_btn" value="RE-SEND MOBILE" name="resend-mobile-otp" id="resend-mobile-otp">
                                            <input type="text" class="numeric_number" name="resend_email_mobile" id="resend_email_mobile" value="" hidden>
                                            <input type="password" name="resend_password_mobile" id="resend_password_mobile" value="" hidden>
                                        </div>
                                     </div-->
                                </div>
                         
                             </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
	</div>

    <div id="forget_pass_success" class="modal" style="left:431px !important; right:465px !important; top:12px !important;">
       <img alt="green" class="geen" src="<?php echo base_url();?>asset/images/green.png">
       <div class="modal-content error-msg">
          <p style="margin:18px 0px 20px 28px !important;">Your Password reset link have been sent on email</p>
          <!--<a class="modal-close close-pop pass-suc modal-trigger" href="#password-modal" type="button"><img src="<?php echo base_url();?>asset/images/popupdelete.png"></a>-->
       </div>
    </div>

    <!-- -->
    <div id="successful_signup_model_2" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg reset_pass_form">
            <!-- Modal content-->
            <div class="container">
                <div class="row signup_form">
                <div class="col-md-offset-1 col-md-5 forgot_pass">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">×</button>
                            <h4 class="modal-title"><img src="<?php echo base_url(); ?>public/images/header_1.png" alt="Logo" class="logo_style_3_popup"/></h4>
                            <label class="logo_line">Most Social Invoicing & Business Intelligence Platform</label>
                        </div>
                        <div class="modal-body">
                            <form action="" id="successful_registeration" name="successful_registeration" class="" method="post" accept-charset="utf-8" novalidate="true">
                            <div class="row">
                           	   <div class="col-lg-12 error_cls">
                                    	<?php /*?><div id="" name="">
                                        	<div>Registration Email Is Sent To You Successfully !!</div>
                                            <div>Please Check Your Mail And Verify Registration.</div>
                            			</div><?php */?>
                                   <div class="col-lg-12 login_form_textbox">
                                    <div class="input-group">
                                      <span class="input-group-addon"><i class="fa fa-edit fa-lg"></i></span>
                                      <input class="form-control" name="phone_otp" id="phone_otp" type="text" placeholder="Phone OTP" required>
                                    </div>
                                    	<div class="req_star_textbx"><font style="color:red">*</font></div>
                                </div>
                                	
                                    <div class="col-lg-12 login_form_textbox">
                                    <div class="input-group">
                                      <span class="input-group-addon"><i class="fa fa-edit fa-lg"></i></span>
                                      <input class="form-control" name="phone_otp" id="phone_otp" type="text" placeholder="Phone OTP" required>
                                    </div>
                                    	<div class="req_star_textbx"><font style="color:red">*</font></div>
                                </div>
                                
                                    </div>
                                    <div class="col-lg-12 error_cls">
                                    	<div id="error_popup" name="error_popup">
                            			</div>
                                    </div>
                                <div class="col-lg-12">
                                    <div class="form-group login_btn_1">
                                        <input type="button"  class="btn login_form_btn" value="OK" name="register_successful" id="register_successful" onclick="hide_popup()">
                                    </div>
                                </div>
                               <hr>  
                             </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
	</div>
    

    
   <!-- -->
    <div class="gototop js-top">
		<a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
	</div>

    <!-- jQuery -->
    <script>var base_url = "<?php echo base_url(); ?>";</script> 
	<script src="<?php echo base_url(); ?>public/js/jquery.min.js"></script>
	<script src="<?php echo base_url(); ?>public/js/pwstrength.js"></script>
    <!--<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.js"></script>-->
	<script src="<?php echo base_url(); ?>public/js/jquery.validate.js"></script>
    <!--<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/additional-methods.min.js"></script>-->
    <script src="<?php echo base_url('public/js/jquery.pwdMeter.js'); ?>"></script>
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
	<!-- jQuery Easing -->
	<script src="<?php echo base_url(); ?>public/js/jquery.easing.1.3.js"></script>
	<!-- Bootstrap -->
	<script src="<?php echo base_url(); ?>public/js/bootstrap.min.js"></script>
	<!-- Waypoints -->
	<script src="<?php echo base_url(); ?>public/js/jquery.waypoints.min.js"></script>
	<!-- Carousel -->
	<script src="<?php echo base_url(); ?>public/js/owl.carousel.min.js"></script>
	<!-- countTo -->
	<script src="<?php echo base_url(); ?>public/js/jquery.countTo.js"></script>
	<!-- Flexslider -->
	<!--<script src="<?php //echo base_url(); ?>public/js/jquery.flexslider-min.js"></script>-->
    <script src="<?php echo base_url(); ?>public/js/jquery.flexslider-min.js"></script>
    <script src="<?php echo base_url(); ?>public/js/jquery.flexslider.js"></script>
	<!-- Magnific Popup -->
	<script src="<?php echo base_url(); ?>public/js/jquery.magnific-popup.min.js"></script>
	<script src="<?php echo base_url(); ?>public/js/magnific-popup-options.js"></script>
	<!-- Main -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script src="<?php echo base_url(); ?>public/js/main.js"></script>
    <!--<script type="text/javascript">
        function Validate(password_2) {
            password_2.value = password_2.value.replace(/[^a-zA-Z 0-9\n\r]+/g, '');
        }
    </script>-->


        <script type="text/javascript"> 
			$(document).ready(function(){

           	 	$(".password_strength").pwdMeter();
				//$("#new_password").pwdMeter();
		         });	
			
			//$("#invalid_password_alpha").hide();
			function alphanumeric(txt) 
			{ 
			var letters = /^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%*#?&])[A-Za-z\d$@$!%*#?&]{8,}$/; 
			if(txt.value.match(letters)) 
			{ 
			$("#invalid_password_alpha").hide();
			return true; 
			} 
			else 
			{ 
			//alert('Please input alphanumeric characters only'); 
			document.signup_frm.password_2.focus();
			$("#invalid_password_alpha").show();
			return false; 
			} 
			} 
		</script>
        
        
        <script type="text/javascript"> 
			function alphanumeric2(txt) 
			{ 
			var letters = /^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%*#?&])[A-Za-z\d$@$!%*#?&]{8,}$/; 
			if(txt.value.match(letters)) 
			{ 
			$("#invalid_password_alpha_2").hide();
			return true; 
			} 
			else 
			{ 
			//alert('Please input alphanumeric characters only');
			document.reset_password_frm.new_password.focus();  
			$("#invalid_password_alpha_2").show();
			return false; 
			} 
			} 
		</script>
        
    	<script type="text/javascript">
		  $(document).ready(function(){
			$('.show_pass_check').on('change',function(){
				$('.show_password').attr('type','Password');
			  var isChecked = $(this).prop('checked');
			  //console.log(isChecked);
			  if (isChecked) {
				$('.show_password').attr('type','text');
			  } else {
				$('.show_password').attr('type','Password');
			  }
			});
		  });
 		 </script>
         <script type="text/javascript">
             $(document).ready(function(){
               $("#login_frm").attr('autocomplete', 'off');
               $("#signup_frm").attr('autocomplete', 'off');
               //$('#email').on("cut copy paste",function(e) {
                  //e.preventDefault();
               //});
               $('#password').on("cut copy paste",function(e) {
                  e.preventDefault();
               });
               $('#confirm_password').on("cut copy paste",function(e) {
                  e.preventDefault();
               });
               //$('#contact').on("cut copy paste",function(e) {
                  //e.preventDefault();
               //});
            });
			
			
				$('.hide-show').show();
				$('.hide-show span').addClass('show')
				  
				$('.hide-show span').click(function(){
				if( $(this).hasClass('show') ) {
					//$(this).text('Hide');
					$(this).html('<img width="29" height="28" src="<?php echo base_url(); ?>public/images/off-monkey.png" alt="small-eazy"></img>');
					$('#password').attr('type','text');
					$('#password_2').attr('type','text');
					$('#new_password').attr('type','text');
					$(this).removeClass('show');
				} else {
					$(this).html('<img width="29" height="28" src="<?php echo base_url(); ?>public/images/no-monkey.png" alt="small-eazy"></img>');
					//$(this).text('Show');
					$('#password').attr('type','password');
					$('#password_2').attr('type','password');
					$('#new_password').attr('type','password');
					$(this).addClass('show');
				}
				});
					
				$('#password').on('click', function(){
					//$('.hide-show span').text('Show').addClass('show');
					//$('.hide-show').parent().find('#password').attr('type','password');
				});
				
				$('#password_2').on('click', function(){
					//$('.hide-show span').text('Show').addClass('show');
					//$('.hide-show').parent().find('#password_2').attr('type','password');
				});
				
				$('#new_password').on('click', function(){
					//$('.hide-show span').text('Show').addClass('show');
					//$('.hide-show').parent().find('#new_password').attr('type','password');
				});
				
         </script>

         <script type="text/javascript">
             
            $(document).ready(function() {

                $('#resend-email-otp').click(function(){
					$(".modal-loader").fadeIn("slow");
					// var resend_email        = $('#resend_email').val();
					// var resend_password     = $('#resend_password').val();
					$("#otp_error_popup").text("OTP email has been sent again.");
                    var  reg_id_otp=$('#reg_id_otp').val();
                    $.ajax({
                        dataType: 'json',
                        type: "POST",
                        url:base_url+'index/resend_email_otp',
                        data: {'csrf_test_name':csrf_hash,'reg_id_otp': reg_id_otp},
                        success: function(data){
							$(".modal-loader").fadeOut("slow");
							timer(180);
							$('.co-otp-submit').removeAttr('disabled', 'disabled');
                            $('#resend_email').val('');
                            $('#resend_password').val('');
                            $("#otp_error_popup").text(" ");
                            Materialize.toast('Your OTP is resend successfully to your email', 4000,'green rounded');
                        }

                        
                        
                    });


                });
            });

         </script>

         <script type="text/javascript">
             
            $(document).ready(function() {

                $('#resend-mobile-otp').click(function(){
                    $("#otp_error_popup").text("OTP sms has been sent again.");
                    $(".modal-loader").fadeIn("slow");
                    //var resend_email_mobile         = $('#resend_email_mobile').val();
                    //var resend_password_mobile      = $('#resend_password_mobile').val();
                    var  reg_id_otp=$('#reg_id_otp').val();

                    $.ajax({

                        dataType: 'json',
                        type: "POST",
                        url:base_url+'index/resend_mob_otp',

                        data: {'csrf_test_name':csrf_hash,'reg_id_otp': reg_id_otp},

                        success: function(data){
                            $(".modal-loader").fadeOut("slow"); 
                            $('#resend_email_mobile').val('');
                            $('#resend_password_mobile').val('');
                            $("#otp_error_popup").text(" ");
                            Materialize.toast('Your OTP is resend successfully to your mobile number', 4000,'green rounded');
                        }

                       
                    });

                });
            });

         </script>
		 
		 <script type="text/javascript">
		$('textarea').keypress(function (e) {
			var regex = new RegExp("^[A-Za-z0-9 ]*$");
			var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
			if (regex.test(str)) {
				return true;
			}else{
				e.preventDefault();
				return false;
			}
		});	
		$.fn.regexMask = function(mask) {
			$(this).keypress(function (event) {
				if (!event.charCode) return true;
				var part1 = this.value.substring(0, this.selectionStart);
				var part2 = this.value.substring(this.selectionEnd, this.value.length);
				if (!mask.test(part1 + String.fromCharCode(event.charCode) + part2))
					return false;
			});
		};
		var mask = new RegExp('^[A-Za-z0-9_@&-/ ]*$')	
		$("input[type='text']").regexMask(mask);
		//$("input[type='text']").bind("cut copy paste",function(e) {
			//e.preventDefault();
		//});
		$("input[type='password']").bind("cut copy paste",function(e) {
			e.preventDefault();
		});
		$('textarea').bind("cut copy paste",function(e) {
			e.preventDefault();
		});	
	</script>
	
	<script type="text/javascript">
		let timerOn = true;

		function timer(remaining) {
		  var m = Math.floor(remaining / 60);
		  var s = remaining % 60;
		  
		  m = m < 10 ? '0' + m : m;
		  s = s < 10 ? '0' + s : s;
		  document.getElementById('otp_timer').innerHTML = m + ':' + s;
		  remaining -= 1;
		  
		  if(remaining >= 0 && timerOn) {
			setTimeout(function() {
				timer(remaining);
			}, 1000);
			return;
		  }

		  if(!timerOn) {
			// Do validate stuff here
			return;
		  }
		  
		  // Do timeout stuff here
		  $('.co-otp-submit').attr('disabled', 'disabled');
		}
	</script>	
  
 </body>
</html>
