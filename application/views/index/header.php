

<html>

<head>

	<meta charset="utf-8">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<? if($this->router->fetch_method()=="signup"){ ?>
		<title>Xebra Cohorts sign-up page into the entrepreneurship community to share ideas</title>
	<? }else{ ?>
		<title><?= translate('HOME_TITLE'); ?></title>
	<? } ?>	

<!-- 	<meta name="viewport" content="width=device-width, initial-scale=1">

	<meta name="description" content="Free HTML5 Website Template by freshdesignweb.com" />

	<meta name="keywords" content="free website templates, free html5, free template, free bootstrap, free website template, html5, css3, mobile first, responsive" />

	<meta name="author" content="freshdesignweb.com" />



  	Facebook and Twitter integration

	<meta property="og:title" content=""/>

	<meta property="og:image" content=""/>

	<meta property="og:url" content=""/>

	<meta property="og:site_name" content=""/>

	<meta property="og:description" content=""/>

	<meta name="twitter:title" content="" />

	<meta name="twitter:image" content="" />

	<meta name="twitter:url" content="" />

	<meta name="twitter:card" content="" /> -->



	<!-- <link href="https://fonts.googleapis.com/css?family=Droid+Sans" rel="stylesheet"> -->

	

	<!-- Animate.css -->

	<link rel="stylesheet" href="<?php echo base_url(); ?>public/css/animate.css">

	<!-- Icomoon Icon Fonts-->

	<link rel="stylesheet" href="<?php echo base_url(); ?>public/css/icomoon.css">

	<!-- Themify Icons-->

	<link rel="stylesheet" href="<?php echo base_url(); ?>public/css/themify-icons.css">

	<!-- Bootstrap  -->

	<link rel="stylesheet" href="<?php echo base_url(); ?>public/css/bootstrap.css">

    <!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">-->
	<link href="<?php echo base_url();?>public/css/bootstrap.min.css" type="text/css" rel="stylesheet">

	<!-- Magnific Popup -->

	<link rel="stylesheet" href="<?php echo base_url(); ?>public/css/magnific-popup.css">

	<!-- Owl Carousel  -->

	<link rel="stylesheet" href="<?php echo base_url(); ?>public/css/owl.carousel.min.css">

	<link rel="stylesheet" href="<?php echo base_url(); ?>public/css/owl.theme.default.min.css">

    

    <!--<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">-->
	<link rel="stylesheet" href="<?php echo base_url(); ?>public/css/w3.css">

	<!-- Font-awesome files--->

	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
	<!--<link href="<?php echo base_url();?>public/css/font-awesome.min.css" type="text/css" rel="stylesheet">-->

    <!-- Montserrat files--->

    <link href="<?php echo base_url(); ?>public/css/montserrat.css" rel="stylesheet">
	<!-- https://fonts.googleapis.com/css?family=Montserrat:300,400,500,700,800 -->
	<!-- Theme style  -->

	<link rel="stylesheet" href="<?php echo base_url(); ?>public/css/style.css">

    <link rel="stylesheet" href="<?php echo base_url(); ?>public/css/style_2.css">



	<!-- Modernizr JS -->
	<script src="<?php echo base_url(); ?>public/js/jquery.min.js"></script>
    <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>-->

	<script src="<?php echo base_url(); ?>public/js/modernizr-2.6.2.min.js"></script>

	<!--<script src='https://www.google.com/recaptcha/api.js'></script>-->
	<!--script src="<?php echo base_url(); ?>public/js/api.js"></script-->

    <script>var base_url = '<?php echo base_url();?>';</script>
	<script>var csrf_token = '<?php echo $this->security->get_csrf_token_name(); ?>';</script>
	<script>var csrf_hash = '<?php echo $this->security->get_csrf_hash(); ?>';</script>


	<!-- FOR IE9 below -->

	<!--[if lt IE 9]>

	<script src="js/respond.min.js"></script>

	<![endif]-->


	<style>.input-group{ width:99%; } </style>
	</head>



	<body>

<script>
/*

  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){

  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),

  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)

  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');



  ga('create', 'UA-101721479-1', 'auto');

  ga('send', 'pageview');


*/
</script>



	<div class="ubea-loader"></div>

	

   


 <script type="text/javascript">

 /*

	$(".newsletter .copy").css("margin-left","-154px");

*/	



	$(".newsletter").hover(function() {

		$(this).children().animate({

			width: "154px"

		}, 300 );

		$(this).children().children().css("margin-left","0px");

	});

	

	$(".newsletter").mouseleave(function() {

		$(this).children().animate({

			width: "0px"

		}, 300 );

		$(this).children().children().css("margin-left","-154px");/*animate({marginLeft: "-=-154px"})*/

	});



 </script>           

       