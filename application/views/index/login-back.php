
	<?php include('header.php'); ?>

    <style type="text/css">
		.forgot_email label.error{
          padding-top: 0px;
        }
        .modal-loader {
			position: fixed;
			left: 0px;
			top: 0px;
			width: 100%;
			height: 100%;
			z-index: 9999;
			background: url(./public/images/loader.gif) center no-repeat;
		}
		.eazy{
			font-weight:900%;
		}

		#login_submit:hover{
			background: #8533ff !important;
			color:white !important;
		}

		body{
			font-family: Verdana,sans-serif !important;
		}

		#login_back{
			border-radius:5px;
			color:#fff;
			font-size:15px;
			background-color:#7965E9 !important;
		}
		body {
    	font-family: "Roboto",sans-serif !important;
		}
      </style>

		<!-- Modal content-->
		<span class="dot"></span>
		<img class="geen" src="<?php echo base_url();?>asset/images/off-circle.png" style="margin-left:37.4% !important; position:absolute; margin-top:-35px;" alt="off-cir">
		<div class="container " style="margin: 10% 0; width: 100%;">
			<div class="col-md-12">
				<div class="col-md-4"></div>
				<div class="row">
					<div class="col-md-7" style="margin-left: 0; padding-left: 35px; padding-right: 35px; width:30% !important;">
						<div class="modal-content" style="box-shadow: none;">
							<div style="text-align:center; margin:30px 25px 0px 30px;">
								<p style="font-size:18px;">As you were away,<br>we have logged you out.</p>
							</div>

							<div class="modal-body" style="margin:-20px 10px 0px 18px !important;">
								<form action="" id="logged_back_frm" name="logged_back_frm" class="" method="post" accept-charset="utf-8" novalidate="true">
									<?php $csrf = array(
					'name' => $this->security->get_csrf_token_name(),
					'hash' => $this->security->get_csrf_hash()
				);
				?>
				<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
									<div class="row" style="text-align:center;">
										<img width="180" height="180" class="columnImage" src="<?php echo base_url(); ?>public/images/logged-off.jpg" border="0"/>
									</div>
									<div class="row" style="text-align:center; padding-top:8%;">
									<?php $url='https://'.$_SERVER['HTTP_HOST'].'/xebra'; ?>
										<a href="<?=$url?>" type="submit" class="btn login_back_btn" value="LOGIN BACK" name="login_back" id="login_back">LOGIN BACK</a>
									</div>
								</form>
							</div>
					  </div>
                  </div>
              </div>
          </div>
        </div>
		<img class="geen" src="<?php echo base_url();?>asset/images/off-circless.png" style="margin-left:51% !important; position:absolute; margin-top:-10.1%;" alt="off-cir">

		<script type="text/javascript">
			$(document).ready(function() {

			});
		</script>
		<?php include('footer.php'); ?>
