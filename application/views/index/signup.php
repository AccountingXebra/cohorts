<!DOCTYPE html>
<html lang="en">
<?php include('header.php'); ?>
<style type="text/css">
	.forgot_email label.error{
		padding-top: 12px;
	}
    .modal-loader {
		position: fixed;
        left: 0px;
        top: 0px;
        width: 100%;
        height: 100%;
        z-index: 9999;
        background: url(./public/images/loader.gif) center no-repeat;
	}
    .dataTables_length {
		margin-left: 500px;
	}
    .eazy{
      font-weight:900%;
    }
    /*.dot {
		height: 100px;
		width: 100px;
		background-color: #FF7D9A;
		border-radius: 50%;
		display: inline-block;
    }*/
    input#login_submit{
		width:60% !important;
		padding-right:20px !important;
		background: #8533ff !important;
		color:white !important;
		border:none !important;
	} 
    input#login_submit:hover{
		background: #8533ff !important;
		color:white !important;
		border:none !important;
    }
    .logo_style_2{
		height:55px !important;
    }
	.login_page{
		width:32% !important;
	}
	.welcom-be{
		font-size:32px;
		font-weight:100 !important;
		color:#fff !important;
		text-align:left;
		line-height:1.2 !important;
	}
	
	.welcom-title{
		font-size:32px;
		font-weight:600;
		color:#fff !important;
	}
	.welcom-content{
		padding:20px 0 10px 0 !important;
	}
	body{
		width:100% !important;
		background-color:#503cb4 !important; 
		/*background: linear-gradient(-180deg, #503cb4 2%, #503cb4 100%) !important;*/
	}
	
	.comm-pic{
		border-top-left-radius:5px;
		border-bottom-left-radius:5px;
	}
	
	.soft-comment{
		background-color:#fff !important;
		height:170px;
		margin-left:-30px;
		border-top-right-radius:5px;
		border-bottom-right-radius:5px;
	}
	
	.person-comment{
		padding: 3% 5px 0 5px;
		text-align: center;
		font-size: 12.5px;
		font-weight: 500;
		color:#000;
		margin-bottom:10px !important;
	}
	
	.person-info{
		padding: 28px 5px 0 5px;
		text-align: center;
		font-size: 12.5px;
		font-weight: 500;
		color:#000;
	}
	
	.high{
		border:1px solid red !important;
		border-radius:5px !important;
	}
	
	.login_form_textbox label.error {
		margin: 37px 0 0 0 !important;
	}
</style>
	<span class="dot"></span>
    <div class="container " style="margin: 20px 0; width: 100%;">
	<div class="col-md-12">
	<div class="sign_up container" style="margin: 20px 0; width: 100%;">
        <div class="col-md-12">
			<div class="col-md-7" style="padding-top:5% !important;">
				<div class="welcom-container">
					<div class="welcom-content">
						<span class="welcom-be">BE A</span>
						<p class="welcom-title">SMART ENTERPRENEUR</p>
					</div>
				</div>
				<img class="geen" height="150" src="<?php echo base_url();?>asset/css/img/icons/Circle-1.png" style="margin-left:-6% !important; position:absolute; text-align:center; margin-top:10px;" alt="off-cir">
				<div class="row">
					<div class="col-md-3">
						<img height="170" width="150" src="<?php echo base_url();?>asset/css/img/icons/blank-stage/ganeshpic.jpg" class="gifblank comm-pic" alt="com-blank">
					</div>
					<div class="col-md-7 soft-comment">
						<p class="person-comment">Xebra provides a detailed financial analysis which allows me to keep a track of commissions, earnings, profits, growth and so on. It has definitely made finances easier. This is one software every business needs.</p>
						<p class="person-info"><strong>Ganesh Gadakh</strong></br>Founder Director - Elixir Integrated Brandcomm Pvt. Ltd.</p>
					</div>
				</div>
				<img class="geen" width="170" src="<?php echo base_url();?>asset/css/img/icons/Circle-2.png" style="margin-left:12% !important; position:absolute; text-align:center; margin-top:0px;" alt="off-cir">
			</div>
            <div class="row">
			<div class="col-md-8 login_page" style="margin-left: 0; padding:0 10px 0 60px;">
				<img class="geen" height="170" src="<?php echo base_url();?>asset/css/img/icons/Circle-1.png" style="margin-left:-12% !important; position:absolute; text-align:center; margin-top:15px;" alt="off-cir">
				<div class="modal-content" style="box-shadow: none;">
					<div style="text-align:center; margin-top:10px;"  >
						<img width="205" height="55" src="<?php echo base_url(); ?>public/images/xebra-logo.png" alt="xebra-logo" class="logo_style_2"/>
						<label class="logo_line_2" style="font-size:15px; font-family: 'Droid Sans', Arial, sans-serif; margin-top:5px !important;">No credit card required. Cancel Anytime.</label>
					</div>
                    <div class="modal-body" style="margin-top:-10px !important;">
						<form action="" id="signup_frm" name="signup_frm" class="" method="post" accept-charset="utf-8" novalidate="true" style="margin:0px 15px 0px 15px !important;">
							<?php $csrf = array(
					'name' => $this->security->get_csrf_token_name(),
					'hash' => $this->security->get_csrf_hash()
				);
				?>
				<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
						<div class="row">
                           		<div class="col-lg-12 error_cls">
									<div id="error_popup" name="error_popup">
                            		</div>
                                </div>
                                <div class="col-lg-12 login_form_textbox">
                                    <div class="input-group name-re">
                                      <span class="input-group-addon"><i class="fa fa-user-o fa-lg"></i></span>
                                      <input class="form-control" name="name" id="name" type="text" placeholder="NAME">
                                    </div>
                                    <div class="req_star_textbx sign-star"><font style="color:red">*</font></div>
                                </div>
								
                                <div class="col-lg-12 login_form_textbox">
                                    <div class="input-group email-re">
                                      <span class="input-group-addon"><i class="fa fa-envelope-o"></i></span>
                                      <input class="form-control Web_email" name="email" id="email" type="text" placeholder="EMAIL" value="<?php if(isset($_GET['ne'])){echo $_GET['ne'];}?>">
                                    </div>
                                    <div class="req_star_textbx sign-star"><font style="color:red">*</font></div>
                                </div>
                                <div class="col-lg-12 login_form_textbox">
                                   <div class="input-group pass-wrng" style="border:1px solid #ababab; border-radius:4px;">
                                          <span class="input-group-addon" style="padding: 6px 13px !important; border:none !important; border-right:1px solid #ababab !important;"><i class="fa fa-lock fa-2x"></i></span>
                                          <input style="width:82%; border:none !important;" class="form-control password_strength show_password" name="password_2" id="password_2" type="password" placeholder="PASSWORD" onblur="alphanumeric(document.signup_frm.password_2)">
                                    </div>
									<div class="hide-show">
										<span style="position:absolute; left:253px; top:5px; cursor: pointer;"><img width="29" height="30" src="<?php echo base_url(); ?>public/images/no-monkey.png" alt="small-eazy"></img></span>
									</div>
                                    <div class="req_star_textbx sign-star"><font style="color:red">*</font></div>
                                </div>
								  <div class="col-lg-12 login_form_textbox" style="margin-top:-10px; margin-bottom:-12px !important;">
									<label style="font-size:10.2px !important; text-align:left;">Min 8 Characters, 1 Uppercase, 1 Number & 1 Special Character</label>
                                  </div>
								<div class="col-lg-12"><!--span id="pwdMeter" class="neutral"></span--></div>
                                <div class="col-lg-12 login_form_textbox">
                                   <div class="input-group con-re">
                                          <span class="input-group-addon"><i class="fa fa-refresh fa-lg" style="font-size:15px;"></i></span>
                                          <input class="form-control" name="confirm_password" id="confirm_password" type="password" placeholder="CONFIRM PASSWORD">
                                    </div>
                                    <div class="req_star_textbx sign-star"><font style="color:red">*</font></div>
                                </div>
								
							
                                <div class="col-lg-12 form-group login_btn_1">
                                    <input type="submit" class="btn login_form_btn sign-xebra" value="SECURE SIGNUP" name="signup_submit" id="signup_submit"><i class="fa fa-lock fa-2x" style="color:#fff; margin-left:-25px;"></i>
                                </div>
                                
                             </div>
					</form>
				</div>
				<div class="modal-footer" style="background-color:#f0f0f5; padding:20px 10px; border-radius:5px;">
					<div class="col-lg-12">
						<div class="login_signup_link" style="font-size:13px;">Already have an account?  <a href="<?php echo base_url(); ?>" style="color:#7965E9;">  <b>Log In</b></a></div>
						<!-- onclick="open_login()" -->
					</div>
				</div>
			</div>
			<img class="geen" width="170" src="<?php echo base_url();?>asset/css/img/icons/Circle-2.png" style="margin-left:25% !important; position:absolute; text-align:center; margin-top:-1px;" alt="off-cir">
		</div>
			</div>
		</div>
	</div>  
	</div>
	</div>
    <div class="footer" style="margin-top:4%; border-top:1px solid #C0C0C0;">
		<div class="row" style="text-align:center; padding-top:10px;">
			<div style="margin-right:-15px; font-size:13px;">
				<p style="color:white;"><a style="color:white;" href="https://www.xebra.in/terms-conditions/" target="_blank"> Terms & Conditions </a>  |  <a style="color:white;" href="https://www.xebra.in/privacy-policy/" target="_blank"> Privacy Policy </a>  |  <a style="color:white;" href="https://www.xebra.in/faqs/" target="_blank"> FAQs </a></p>
			</div>
			<div style="margin-right:-10px; margin-top:5px;">
				<label style="font-size:13px; color:#B0B7CA;">Copyright &copy; <?php echo date('Y');?> Xebra. All Rights Reserved</label>
			</div>
		</div>
	</div>
    <script>
    $( document ).ready(function(){
	var em = '<?php 
	if(@$web_email){
		echo $web_email;
    }else{
		echo "";
    }?>'
	if(em){
        open_signup();
		$('.Web_email').val(em);
    }
	var signup='<?php echo $open_signup;?>';
      if(signup){
         open_signup();
      }
      var otp='<?php echo $open_otp;?>';
      if(otp){
         open_otp();
      }
    });
    </script>
<?php include('footer.php'); ?>
</html>