

    <!-- //////////////////////////////////////////////////////////////////////////// -->

    <!-- START MAIN -->

    <div id="main">

      <!-- START WRAPPER -->

      <div class="wrapper">

       

       	 <!-- START LEFT SIDEBAR NAV-->

         <?php $this->load->view('dashboard_pages/sidebar.php');?>

        

        <!-- END LEFT SIDEBAR NAV-->

        <!-- //////////////////////////////////////////////////////////////////////////// -->

        <!-- START CONTENT -->

        <section id="content">

          <div class="container">

            <div class="plain-page-header">

              <div class="row">

                <div class="col l6 s12 m6">

                  <a class="go-back underline" href="<?php echo base_url();?>Personal_profile/manage_profile">Back to My Personal Profile</a> 

                </div>

                <div class="col l6 s12 m6">

                </div>

              </div>

            </div>

            <div class="page-content">

              <div class="row">

                <div class="col s12 m12 l3"></div>

                <div class="col s12 m12 l6">

                  <form class="create-company-form border-split-form" id="create_user" name="create_user" method="post" action="<?php echo base_url();?>Personal_profile/create_new_user" enctype="multipart/form-data">

                    <div class="box-wrapper bg-white bg-img-green shadow border-radius-6">
					<?php $csrf = array(
					'name' => $this->security->get_csrf_token_name(),
					'hash' => $this->security->get_csrf_hash()
				);
				?>
				<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
                      <div class="box-header">

                        <h3 class="box-title">Create Personal Profile</h3>

                      </div>

                      <div class="box-body">

                        <div class="row">

                          <div class="col s12 m12 l12">

                            <div class="col s12 m12 l9">

                              <div class="row">

                                <div class="input-field">

                                  <label for="company_name" class="full-bg-label">Contact Name</label>

                                  <input id="contact_name" name="contact_name" class="full-bg adjust-width" type="text">

                                </div>

                              </div>

                            </div>

                            <div class="col s12 m12 l3">

                              <div class="row">

                                <div class="input-field">

                                  <div class="uploader-placeholder">

                                    <input type="file" class="hide-file" id="upload_img" name="upload_img">

                                    <input type="hidden" id="image_info" name="image_info" value="" />

                                  </div>

                                </div>

                              </div>

                            </div>

                          </div>

                        </div>

                        <div class="row">

                          <div class="col s12 m12 l12">

                            <div class="col s12 m12 l12">

                              <div class="row">

                                <div class="input-field">

                                  <label for="contact_email" class="full-bg-label">Email ID</label>

                                  <input id="contact_email" name="contact_email" class="full-bg adjust-width border-top-none" type="text">

                                </div>

                              </div>

                            </div>

                          </div>

                        </div>

                        

                        <div class="row">

                          <div class="col s12 m12 l12">

                            <div class="col s12 m12 l6 input-set">

                              <div class="row">

                                <div class="input-field">

                                    <label for="contact_mobile" class="full-bg-label">Password</label>

                                    <input id="contact_password" name="contact_password" class="full-bg adjust-width border-top-none show_password password_strength" type="password" onblur="alphanumeric(document.create_user.contact_password)">

                                    <span id="invalid_password_alpha"  class="error pass_2" style="display:none;font-size:12px;">Please Enter Alphanumeric Values and Special Character for Password</span>

                                    

                                </div>

                              </div>

                            </div>

                             <div class="col s12 m12 l6 input-set">

                              <div class="input-field">

                                <label for="country" class="full-bg-label">Confirm Password</label>

								<input id="contact_confirm_password" name="contact_confirm_password" class="full-bg adjust-width border-top-none" type="password">

                              </div>

                            </div>

                          </div>

                        </div>

                        

                        <div class="row">

                          <div class="col s12 m12 l12">

                            <div class="col s12 m12 l6 input-set">

                            

                             <div class="row">

                                  <div class="col s12 m12 l12">

                                    <div class="col s12 m12 l12">

                                      <div class="row">

                                           <input type="checkbox" id="filled-in1" class="purple show_password_chkbox" />

                                           <label for="filled-in1">Show Password</label>

                                      </div>

                                    </div>

                                  </div>

                                </div>

                           	 </div>

                             <div class="col s12 m12 l6 input-set">

                              <div class="input-field">

                                 <span id="pwdMeter" class="neutral"></span>

                              </div>

                            </div>

                          </div>

                        </div>

    

                  		



                        <div class="row">

                          <div class="col s12 m12 l12">

                            <div class="col s12 m12 l6 input-set">

                              <div class="row">

                                <div class="input-field">

                                    <label for="contact_mobile" class="full-bg-label">Mobile</label>

                                    <input id="contact_mobile" name="contact_mobile" class="full-bg adjust-width border-top-none" type="text">

                                </div>

                              </div>

                            </div>

                             <div class="col s12 m12 l6 input-set">

                              <div class="input-field">

                                <label for="country" class="full-bg-label select-label">Access</label>

                                

                                <select id="access_type" class="country-dropdown check-label" name="access_type">

                                  <option value=""></option>

                                  <option value="1">Accountant</option>

                                  <option value="2">CA</option>

                                  <option value="3">Admin</option>

                                </select>

                              </div>

                            </div>

                          </div>

                        </div>

                      </div>

                    </div>



                    <div class="row">

                      <div class="col s12 m12 l12">

                        <div class="text-center"><i class="material-icons know-more-form">keyboard_arrow_down</i></div>

                      </div>

                    </div>



                    <div class="row">

                      <div class="col s12 m12 l12">

                        <div class="form-botom-divider"></div>

                      </div>

                    </div>

                    <div class="row">

                      <div class="col s12 m12 l6 right">

                        <button class="btn-flat theme-flat-btn theme-btn theme-btn-large">Cancel</button>

                        <input type="submit" class="btn-flat theme-primary-btn theme-btn theme-btn-large right" value="Save">

                      </div>

                    </div>

                  </form>

                </div>

                <div class="col s12 m12 l3"></div>

              </div>

            </div>

          </div>        	 

        </section>

        <!-- END CONTENT -->

        </div>

        

        <!-- END WRAPPER -->

      </div>

      <!-- END MAIN -->

