<!DOCTYPE html>
<html lang="en">
<?php include('header.php'); ?>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="Login to the Xebra Cohorts,  a platform that offers growth and a community to discuss ideas and thoughts. Showcase your work, organise events and offer deals to scale your business. Understand financial and accounting terms to get a better understanding of numbers">
<meta name="keywords" content="Xebra Cohorts Login, Xebra Cohorts sign-in">
<script src="https://www.google.com/recaptcha/api.js"></script>
<style type="text/css">
	/* Mobile View */
	@media only screen and (max-width: 600px) {
		.ubea-nav-toggle {
			display: none;
		}
		.sign_up.container{
			margin:-10% 0 0 -2% !important;
		}
		.welcom-title{ font-size:25px !important; }
		.Welcome-image img{ margin:0 0 0 -10%; }
		.mbb .login_page{ width:125% !important; }  
		.mbb{ margin:0 0 0 -20%; }
		#login_frm .input-group .form-control{ width:120%; }
		.mbbxc{ width:115% !important; }
		.monkeyxc{ left:222px !important; }
		#login_submit{ padding:10px 0 10px 0 !important; }
		a.login_forgotpass_link{ margin:-2% 0 0 0; font-size:15px; }
		.login_rem, .logo_line_2, .login_signup_link { font-size:15px !important; }
	}
	@media screen and (max-width: 768px){
		.ubea-nav-toggle {
			display: none;
		}
		.container {
			margin: 0px 0 15% 0 !important;
		}
	}
	@media screen and (min-width: 1900px) {
		.container.bigScr{ 
            margin: 20px 10% !important;
			width: 76% !important;
		}
	}
	@media screen and (min-width: 1600px) {
		.container.bigScr{ 
            margin: 20px 4% !important;
            width: 86% !important;
		}
	}
	/* Mobile View */
	@media only screen and (max-width: 600px) {
		.ubea-nav-toggle {
			display: none;
		}
		.sign_up.container{
			margin:-10% 0 0 -2% !important;
		}
		.welcom-title{ font-size:25px !important; }
		.Welcome-image img{ margin:0 0 0 -10%; }
		.mbb .login_page{ width:125% !important; }  
		.mbb{ margin:0 0 0 -20%; }
		#login_frm .input-group .form-control{ width:120%; }
		.mbbxc{ width:115%; }
		.monkeyxc{ left:222px !important; }
		#login_submit{ padding:10px 0 10px 0 !important; }
		a.login_forgotpass_link{ margin:-13% 0 0 0; }
	}
	input[type="checkbox"],
	input[type="radio"] {
		-webkit-box-sizing: border-box;
        box-sizing: border-box;
		/* 1 */
		padding: 0;
		/* 2 */
	}
	[type="checkbox"].filled-in:checked + label:before {
		top: 0;
		left: 1px;
		width: 8px;
		height: 13px;
		border-top: 2px solid transparent;
		border-left: 2px solid transparent;
		border-right: 2px solid #fff;
		border-bottom: 2px solid #fff;
		-webkit-transform: rotateZ( 37deg );
		transform: rotateZ( 37deg );
		-webkit-transform-origin: 100% 100%;
		transform-origin: 100% 100%;
	}
	/* Remove default checkbox */
	[type="checkbox"]:not(:checked),
	[type="checkbox"]:checked {
		position: absolute;
		opacity: 0;
		pointer-events: none;
	}

	[type="checkbox"] {
		/* checkbox aspect */
	}

	[type="checkbox"] + label {
		position: relative;
		padding-left: 20px;
		cursor: pointer;
		display: inline-block;
		height: 25px;
		line-height: 25px;
		font-size: 1rem;
		-webkit-user-select: none;
		-moz-user-select: none;
		-ms-user-select: none;
        user-select: none;
	}

	[type="checkbox"] + label:before,
	[type="checkbox"]:not(.filled-in) + label:after {
		content: '';
		position: absolute;
		top: 0;
		left: 0;
		width: 16px;
		height: 16px;
		z-index: 0;
		border: 2px solid #5a5a5a;
		border-radius: 1px;
		margin-top: 2px;
		-webkit-transition: .2s;
		transition: .2s;
	}

	[type="checkbox"]:not(.filled-in) + label:after {
		border: 0;
		-webkit-transform: scale(0);
        transform: scale(0);
	}

	[type="checkbox"]:not(:checked):disabled + label:before {
		border: none;
		background-color: rgba(0, 0, 0, 0.42);
	}

	[type="checkbox"].tabbed:focus + label:after {
		-webkit-transform: scale(1);
        transform: scale(1);
		border: 0;
		border-radius: 50%;
		-webkit-box-shadow: 0 0 0 10px rgba(0, 0, 0, 0.1);
        box-shadow: 0 0 0 10px rgba(0, 0, 0, 0.1);
	}
	
	[type="checkbox"]:checked + label:before {
		top: 1px;
		left: 0px;
		width: 8px;
		height: 14px;
		border-top: 2px solid transparent;
		border-left: 2px solid transparent;
		border-right: 2px solid #26a69a;
		border-bottom: 2px solid #26a69a;
		-webkit-transform: rotate(40deg);
        transform: rotate(40deg);
		-webkit-backface-visibility: hidden;
        backface-visibility: hidden;
		-webkit-transform-origin: 100% 100%;
        transform-origin: 100% 100%;
	}

	[type="checkbox"]:checked:disabled + label:before {
		border-right: 2px solid rgba(0, 0, 0, 0.42);
		border-bottom: 2px solid rgba(0, 0, 0, 0.42);
	}

	/* Indeterminate checkbox */
	[type="checkbox"]:indeterminate + label:before {
		top: -11px;
		left: -12px;
		width: 10px;
		height: 22px;
		border-top: none;
		border-left: none;
		border-right: 2px solid #26a69a;
		border-bottom: none;
		-webkit-transform: rotate(90deg);
        transform: rotate(90deg);
		-webkit-backface-visibility: hidden;
        backface-visibility: hidden;
		-webkit-transform-origin: 100% 100%;
		transform-origin: 100% 100%;
	}

	[type="checkbox"]:indeterminate:disabled + label:before {
		border-right: 2px solid rgba(0, 0, 0, 0.42);
		background-color: transparent;
	}

	[type="checkbox"].filled-in + label:after {
		border-radius: 2px;
	}

	[type="checkbox"].filled-in + label:before,
	[type="checkbox"].filled-in + label:after {
		content: '';
		left: 0;
		position: absolute;
		/* .1s delay is for check animation */
		-webkit-transition: border .25s, background-color .25s, width .20s .1s, height .20s .1s, top .20s .1s, left .20s .1s;
		transition: border .25s, background-color .25s, width .20s .1s, height .20s .1s, top .20s .1s, left .20s .1s;
		z-index: 1;
		border:1px solid red;
	}

	[type="checkbox"].filled-in:not(:checked) + label:before {
		width: 0;
		height: 0;
		border: 3px solid transparent;
		left: 6px;
		top: 10px;
		-webkit-transform: rotateZ(37deg);
        transform: rotateZ(37deg);
		-webkit-transform-origin: 100% 100%;
        transform-origin: 100% 100%;
	}

	[type="checkbox"].filled-in:not(:checked) + label:after {
		border: 2px solid #B0B7CA;
		height: 16px;
		width: 16px;
		background-color: transparent;
		top: 0px;
		z-index: 0;
	}

	[type="checkbox"].filled-in:checked + label:before {
		top: -2px;
		left: 0px;
		width: 8px;
		height: 13px;
		border-top: 2px solid transparent;
		border-left: 2px solid transparent;
		border-right: 2px solid #fff;
		border-bottom: 2px solid #fff;
		-webkit-transform: rotateZ(37deg);
		transform: rotateZ(37deg);
		-webkit-transform-origin: 100% 100%;
		transform-origin: 100% 100%;
	}

		[type="checkbox"].filled-in:checked + label:after {
		  top: 0;
		  width: 16px;
		  height: 16px;
		  border: 2px solid #8533ff;
		  background-color: #8533ff;
		  z-index: 0;
		}

		[type="checkbox"].filled-in.tabbed:focus + label:after {
		  border-radius: 2px;
		  border-color: #5a5a5a;
		  background-color: rgba(0, 0, 0, 0.1);
		}

		[type="checkbox"].filled-in.tabbed:checked:focus + label:after {
		  border-radius: 2px;
		  background-color: #26a69a;
		  border-color: #26a69a;
		}

		[type="checkbox"].filled-in:disabled:not(:checked) + label:before {
		  background-color: transparent;
		  border: 2px solid transparent;
		}

		[type="checkbox"].filled-in:disabled:not(:checked) + label:after {
		  border-color: transparent;
		  background-color: #949494;
		}

		[type="checkbox"].filled-in:disabled:checked + label:before {
		  background-color: transparent;
		}

		[type="checkbox"].filled-in:disabled:checked + label:after {
		  background-color: #949494;
		  border-color: #949494;
		}	

/*...........................................................*/
	.forgot_email label.error{
		padding-top: 12px;
	}
    .modal-loader {
		position: fixed;
        left: 0px;
        top: 0px;
        width: 100%;
        height: 100%;
        z-index: 9999;
        background: url(./public/images/loader.gif) center no-repeat;
	}
    .dataTables_length {
		margin-left: 500px;
	}
    .eazy{
      font-weight:900%;
    }
    /*.dot {
		height: 100px;
		width: 100px;
		background-color: #FF7D9A;
		border-radius: 50%;
		display: inline-block;
    }*/
    input#login_submit{
		width:60% !important;
		padding-right:20px !important;
		background: #8533ff !important;
		color:white !important;
		border:none !important;
	} 
    input#login_submit:hover{
		background: #8533ff !important;
		color:white !important;
		border:none !important;
    }
    .logo_style_2{
		height:80px !important;
    }
	.login_page{
		width:32% !important;
	}
	.welcom-container{
		text-align:center;
	}
	.welcom-title{
		font-size:32px;
		font-weight:600;
		color:#fff !important;
	}
	.welcom-content{
		padding:0px 0 20px 0 !important;
	}
	
	.Welcome-image{
		padding-top:12px;
	}
	
	body{
		width:100% !important;
		background-color:rgb(77, 68, 167) !important;
		/*background-color:#4848AA !important;*/
		/*background: linear-gradient(-180deg, #503CB4 2%, #4848AA 100%) !important;*/
	}
	
	#g-recaptcha-response-error{
		display:none !important;
	}
</style>
	<span class="dot"></span>
    <div class="container bigScr" style="margin: 70px 0; width: 100%;">
	<div class="col-md-12">
	<div class="sign_up container " style="margin: 35px 0; width: 100%;">
		<!--img class="geen" src="<?php echo base_url();?>asset/images/off-circle.png" style="margin-left:495px !important; position:absolute; margin-top:-30px;" alt="off-cir"-->
        <div class="col-md-12">
			<div class="col-md-7">
				<div class="welcom-container">
					<div class="Welcome-image">
						<img height="300" width="300" src="<?php echo base_url();?>asset/css/img/icons/blank-stage/login1.gif" class="gifblank" alt="com-blank">
					</div>
					<div class="welcom-content">
						<h1 class="welcom-title">PROFIT FROM IT</h1><!-- BOOST YOUR BUSINESS -->
					</div>
				</div>
			</div>
            <div class="row mbb">
			<?php $user=$this->input->cookie('remember_me_u',true); $pass=$this->input->cookie('remember_me_pass',true);?>
			<div class="col-md-8 login_page" style="margin-left: 0; padding-left: 40px; padding-right: 40px;">
				<img class="geen" height="170" src="<?php echo base_url();?>asset/css/img/icons/Circle-1.png" style="margin-left:-12% !important; position:absolute; text-align:center; margin-top:15px;" alt="off-cir">
				<div class="modal-content" style="box-shadow: none;">
					<div style="text-align:center; margin-top:10px;">
						<!--<h4 class="modal-title"><img src="<?php echo base_url(); ?>public/images/header_1.png" alt="Logo" class="logo_style_2"/></h4>-->
                        <!--<label class="logo_line_2">Most Social Invoicing & Business Intelligence Platform</label>-->
						<img width="185" height="75" src="<?php echo base_url(); ?>public/images/xebracohorts_c.png" alt="xebra-logo" class="logo_style_2"/>
						<label class="logo_line_2" style="padding-left:2px; padding-right:12px; padding-top:12px;">To log in, enter your email address and password</label>
					</div>
                    <div class="modal-body" style="margin-top:-10px !important;">
						<form action="" id="login_frm" name="login_frm" class="" method="post" accept-charset="utf-8" novalidate="true">
							<?php $csrf = array(
					'name' => $this->security->get_csrf_token_name(),
					'hash' => $this->security->get_csrf_hash()
				);
				?>
				<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
						<div class="row">
							<div class="col-lg-12 error_cls"><div id="error_popup" name="error_popup"></div></div>
							<div class="col-lg-12 login_form_textbox">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-envelope-o"></i></span>
									<input class="form-control" name="email" id="email" type="text" placeholder="EMAIL" value="<?=$user?>">
								</div><!--<div class="req_star_textbx"><font style="color:red">*</font></div>-->
							</div>
                            <div class="col-lg-12 login_form_textbox"> 
								<div class="input-group mbbxc" style="border:1px solid #ababab; border-radius:4px;">
									<span class="input-group-addon" style="padding: 6px 13px !important; border:none !important; border-right:1px solid #ababab !important;"><i class="fa fa-lock fa-lg"></i></span>
                                    <input style="width:78%; border:none !important;" class="form-control show_password" name="password" id="password" type="password" placeholder="PASSWORD" value=""/>
									<div class="hide-show">
										<span class="monkeyxc" style="position:absolute; left:251px; top:5px; cursor: pointer;"><img width="29" height="30" src="<?php echo base_url(); ?>public/images/no-monkey.png" alt="small-eazy"></img></span>
									</div>
								</div><!--<div class="req_star_textbx"><font style="color:red">*</font></div>-->
							</div>
							<div class="col-lg-12 login_form_textbox" style="margin-top:-14px;">
							<!--<label style="font-size:11px !important; text-align:left;">Minimum 8 Characters, 1 in Uppercase, 1 Number & 1 Special Character</label>-->
							</div>
							<!--<div class="col-lg-12 login_form_textbox" style="display:block;">
                            <div class="col-lg-6 login_rem_2">
                            <input type="checkbox" class="form_checkbox show_pass_check" id="show_paas"> 
                            <div class="login_rem">Show password</div>
                            </div>
                            </div>-->
							<div class="g-recaptcha" data-sitekey="<?= recaptcha_invisible ?>" data-callback="onSubmit" data-size="invisible"></div>
							<div class="col-lg-12"><label id="g-recaptcha-response-error" class="error" for="g-recaptcha-response"></label></div>
							<div class="col-lg-12 login_form_textbox" style="display:block;">
								<div class="col-lg-6 login_rem_2">
									<input style="margin:0px !important;" type="checkbox" class="filled-in form_checkbox" name="remember_me" id="remember_me" />
									<label for="remember_me"></label>
                                    <div style="line-height: 17px; padding-top:0px !important;" class="login_rem">Remember Me</div>
								</div>
                                <div class="col-lg-6">
									<div><a style="text-decoration:none; font-family: 'Roboto', sans-serif;" class="login_forgotpass_link" href="#" onclick="open_forgotpassword()">Forgot Password?</a></div>
                                </div>
							</div>
							<div class="col-lg-12" style="margin-bottom:-20px !important;">
								<div class="form-group login_btn_1">
									<input type="submit" class="btn login_form_btn" value="SECURE LOGIN" name="login_submit" id="login_submit"><i class="fa fa-lock fa-2x" style="color:#fff; margin-left:-35px;"></i>
                                    <!--<div class="login_signup_link"><a href="<?= base_url(); ?>client-portal">Login as Client</a></div>--> <!-- onclick="open_client_login()" -->
								</div>
							</div>
						</div>
					</form>
				</div>
				<!-- Recaptcha Here -->
				<!--<div class="g-recaptcha" data-sitekey="6LezsAMfAAAAAMgw6faSmKmz_zCQaABku2Mkn6Pb" data-callback="onSubmit" data-size="invisible"></div>-->
				<div class="modal-footer" style="background-color:#f0f0f5; padding:20px 10px; border-radius:5px;">
                <!--<div class="col-lg-12 login_signup_link">Don't have an account?   
                <a href="#" onclick="open_signup()"> <b>Sign Up</b></a></div>-->
                <div class="col-lg-12">
					<!--<a class="btn btn-welcome" href="<?php echo base_url(); ?>index/emailmsg">Subscribe Now</a>-->
					<div class="col-lg-12 login_signup_link" style="font-size:13px;">Don't have an account?   
					<a href="<?php echo base_url(); ?>signup" style="color:#7965E9;"><b>Sign Up</b></a></div>
					<!-- onclick="open_signup()" -->
                </div>
				</div>
			</div>
			<img class="geen" width="170" src="<?php echo base_url();?>asset/css/img/icons/Circle-2.png" style="margin-left:16% !important; position:absolute; text-align:center; margin-top:-1px;" alt="off-cir">
		</div>
		
			</div>
		</div>
	</div>  
	</div>
	</div>
    <div class="footer" style="margin-top:2%; border-top:1px solid #C0C0C0;">
		<div class="row" style="text-align:center; padding-top:10px;">
			<div style="margin-right:-15px; font-size:15px;">
				<p style="color:white;"><a style="color:white;" href="https://www.xebra.in/terms-conditions/" target="_blank"> Terms & Conditions </a>  |  <a style="color:white;" href="https://www.xebra.in/privacy-policy/" target="_blank"> Privacy Policy </a>  |  <a style="color:white;" href="https://www.xebra.in/faqs/" target="_blank"> FAQs </a></p>
			</div>
			<div style="margin-right:-10px; margin-top:5px;">
				<label style="font-size:13px; color:#B0B7CA;">Copyright &copy; <?php echo date('Y');?> Xebra. All Rights Reserved</label>
			</div>
		</div>
	</div>
	<!--div class="sign_footer footer" style="margin-top:75px;">
      <div class="row" style="text-align:center">
        <div style="margin-right:45px; font-size:13px;">
          <p style="color:white;"><a style="color:white;" href="https://www.xebra.in/terms-conditions/" target="_blank"> Terms & Conditions </a> | <a style="color:white;" href="https://www.xebra.in/privacy-policy/" target="_blank"> Privacy Policy </a> | <a style="color:white;" href="https://www.xebra.in/support-faq/" target="_blank"> FAQs </a></p>
        </div>
        <div style="margin-right:45px; margin-top:5px;">
          <label style="font-size:13px; color:#B0B7CA;">Copyright &copy; <?php //echo date('Y');?> Xebra. All Right Reserved</label>
        </div>
      </div>
    </div-->
   
    <script>
    $( document ).ready(function() {
		setTimeout(function(){
			grecaptcha.execute();
		}, 1000);
		var em = '<?php 
            if(@$web_email){
              echo $web_email;
            }else{
              echo "";
            }
           ?>'

      //alert(em);
      if(em)
      {
        //alert(em);
        open_signup();
        $('.Web_email').val(em);
      }
 var signup='<?php echo $open_signup;?>';
      if(signup){
         open_signup();
      }

     
        /*$('.hide-show').show();
        $('.hide-show span').addClass('show')
          
        $('.hide-show span').click(function(){
        if( $(this).hasClass('show') ) {
          $(this).text('Hide');
          $('#password').attr('type','text');
          $(this).removeClass('show');
        } else {
          $(this).text('Show');
          $('#password').attr('type','password');
          $(this).addClass('show');
        }
        });
          
        $('#password').on('click', function(){
          $('.hide-show span').text('Show').addClass('show');
          $('.hide-show').parent().find('#password').attr('type','password');
        });*/ 
    });
    </script>
	<?php //include('pages/slider.php'); ?>

      <?php //include('pages/about.php'); ?>

      <?php  //include('file:///D|/Krutika/PROJECTS/EasyInvoice/2018/FEB/23 FEB 2018/backup/easy_invoice_system/application/views/pages/template/services.php'); ?>

      <?php // include('pages/portfolio.php'); ?>

      <?php //include('pages/contact.php'); ?>
      <?php include('footer.php'); ?>

  <!--<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=599ece9893086300127f927d&product=sticky-share-buttons"></script>-->
<!--<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-599ed322f4cc9c4b"></script> -->

</html>    