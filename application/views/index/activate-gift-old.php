<!DOCTYPE html>
<html>
<head>
	<!-- Favicon -->
	<!--link rel="shortcut icon" href="<?php echo base_url();?>assets/images/logo/xc-favicon.png"/-->

	<!-- Title -->
	<title>Activate Gift</title>

	<!-- CSS -->
	<link rel="stylesheet" href="<?php echo base_url();?>asset/gift/css/marketplace.css">
	<link rel="stylesheet" href="<?php echo base_url();?>asset/gift/css/bootstrap.css">
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700" rel="stylesheet">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

	<!-- JS -->
	<script src="<?php echo base_url();?>asset/gift/js/jquery.js"></script>
    <script src="<?php echo base_url();?>asset/gift/js/popper.js"></script>
    <script src="<?php echo base_url();?>asset/gift/js/bootstrap.js"></script>
	<script src="<?php echo base_url();?>asset/gift/js/select2.min.js"></script>

	<style type="text/css">
		body{margin:0; background:#7965E9;}
		#term_cond[type="checkbox"]:checked + label:before {
		    border-right: 2px solid #fff;
		    border-bottom: 2px solid #fff;
		    width: 8px;
		    height: 11px;
		    z-index: 1;
		    top: 0;
		    left: 0;
		}
		#term_cond[type="checkbox"] + label:after {
		    border: 1px solid #50E3C2;
		    background: #50E3C2;
		    width: 15px;
		    height: 15px;
		    transform: scale(1);
		}
		.right-list{
			margin-left: -5%;
		}
	</style>
</head>
<body>

	<div class="container-fluid">

		<div class="first-label">
			<h3 style="font-family: 'Roboto', sans-serif; color: #fff; text-align :center;">Welcome to Xebra. Here's our gift to you<h3/><!-- Here is your gift for being a part of Xebra business cohorts -->
		</div>

		<div class="img-div">
			<img height="220" width="800" src="<?php echo base_url();?>asset/images/gift.png">
		</div>

		<div class="middle-div">
			<h3 style="font-family: 'Roboto', sans-serif; color: #fff; text-align: center;">Get complete access to Free Forever Walk plan of Xebra. Now boost your revenues & profits<h3/><!-- You will get complete access to Booster pack of Xebra Business Intelligence Application for 3 months -->
			<hr class="one-hr">
		</div>

		<div class="row">
			<div class="col-sm-12">
				<h3 style="font-size:25px; font-family: 'Roboto', sans-serif; color: #fff; text-align:center;">What does the Walk Plan include?</h3>
			</div>
		</div>
		<div class="row list-buzz">
			<div class="col-sm-2"></div>
			<div class="col-sm-4 mar-top">
				<ul class="feature_list">
					<li>Customised invoices & one click online receipts</li>
					<li>Expense recording & payment</li>
					<li>Employee Master & Payroll</li>
					<li>Asset Tracker & Depreciation</li>
				</ul>
			</div>
			<div class="col-sm-4 mar-top">
				<ul class="feature_list right-list">
					<li>Give out portal access to your clients and vendors</li>
					<li>One-click GST & TDS excel ready</li>
					<li>Auto record your Cash & Bank entries</li>
					<li>Grow your business on Marketplace</li>
				</ul>
			</div>
			<div class="col-sm-2"></div>
			<hr class="one-hr">
		</div>

		<div class="row">
			<div class="col-sm-12 mar-top">
				<h3 style="font-size: 16px; font-family: 'Roboto', sans-serif; color: #fff; text-align: center; margin-top: 20px; margin-bottom: 20px;">Follow this simple 3 steps process <h3/>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-3"></div>
			<div class="col-sm-2">
				<p style="float:left;" class="numberCircle">1</p><div class="step-one">Fill Personal Profile </div>
			</div>
			<div class="col-sm-2">
				<p style="float:left;" class="numberCircle">2</p><div class="step-one">Fill Company Profile </div>
			</div>
			<div class="col-sm-2">
				<p style="float:left;" class="numberCircle">3</p><div class="step-three">Start preparing Invoices </div>
			</div>
			<div class="col-sm-3"></div>
		</div>

		<footer class="gift-terms">
			<form method="post" action="marketplace/validate-activate-gift">
				<?php $csrf = array(
					'name' => $this->security->get_csrf_token_name(),
					'hash' => $this->security->get_csrf_hash()
				);
				?>
				<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
				<input type="checkbox" id="term_cond" class="term_cond_chkbox" value="1" name="term_cond">
		    <label class="checkbox" for="term_cond"><span class="check-boxs">I accept the <a>terms and conditions</a> and <a>privacy policy</a></span></label>
				<a style="color:#fff" class="btn btn-theme btn-large right accept-btn disabled" href="<?php echo base_url()?>signup">Accept</a>
				<!--button type="button" class="accept-btn btn btn-info" disabled>Accept</button-->
			</form>
		</footer>

	</div>

</body>
<script type="text/javascript">
      $(document).ready(function() {
				$("#term_cond").change(function(){
					if ($(this).prop('checked')==true){
							$('.accept-btn').removeClass('disabled');
    			}else if ($(this).prop('checked')==false){
							$('.accept-btn').addClass('disabled');
    			}
				});
			});
</script>
</html>
