    <div id="reset_password_model" class="modal fade" role="dialog" style="margin-top:30px; margin-left:150px;">
        <div class="modal-dialog modal-lg reset_pass_form">
            <!-- Modal content-->
            <div class="container">
                <div class="row conf_pass">
                <div class="col-md-offset-1 col-md-5 forgot_pass">
                    <div class="modal-content">
                        <!--<div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">×</button>
                            <h4 class="modal-title"><img src="<?php echo base_url(); ?>public/images/header_1.png" alt="Logo" class="logo_style_3_popup"/></h4>
                            <label class="logo_line">Most Social Invoicing & Business Intelligence Platform</label>
                        </div>-->
						<div class="col-lg-12" style="text-align:center; margin-top:15px; font-family: Verdana,sans-serif !important; color:black;">
							<p style="font-size:18px;"><b>Reset your password</b></p>
							<p style="font-size:14px; font-weight:500 !important;">Your password should be difficult for others to guess</p>
							<p style="font-size:13px;"> We recommend that you use a combination of upper case and lower case letter as well as numbers</p>
						</div> 	
                        <div class="modal-body ">
                            <div class="modal-loader" style="display: none;"></div>
                            <form  id="reset_password_frm" name="reset_password_frm" class="" method="post" accept-charset="utf-8" novalidate="true">
								<?php $csrf = array(
					'name' => $this->security->get_csrf_token_name(),
					'hash' => $this->security->get_csrf_hash()
				);
				?>
				<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
                            <div class="row">
                           		 
                                <!--<div class="col-lg-12">
                                	<div class="input-group">
                                    	<label>Enter New Password</label>
                                    </div>
                                </div>-->
                                <div class="col-lg-12 login_form_textbox">
                                    <div class="input-group forgot_email" style="border:1px solid #ababab; border-radius:4px;">
                                      <span class="input-group-addon" style="padding: 6px 13px !important; border:none !important; border-right:1px solid #ababab !important;"><i class="fa fa-lock fa-lg"></i></span>
                                      <input style="font-family: Verdana,sans-serif; height:34px; border:none !important; font-size:13px; width:75%;" class="form-control password_strength show_password" name="new_password" id="new_password" type="password" placeholder="NEW PASSWORD" onblur="alphanumeric2(document.reset_password_frm.new_password)">
                                      <input type="hidden" name="email" value="<?php echo $email_ei; ?>">
                                      <input type="hidden" name="token" value="<?php echo $token_ei; ?>">
                                    </div>
                                    <!--<div class="req_star_textbx"><font style="color:red">*</font></div>-->
									<div class="hide-show">
										<span style="position:absolute; left:320px; top:8px; font-family: Verdana,sans-serif;">Show</span>
									</div>
                                </div>
								
								<!--<div class="col-lg-12">
                                 <span id="pwdMeter" class="neutral"></span>
								</div>-->
                                
                                <!--<div class="col-lg-12 login_form_textbox">
                                  <div class="col-lg-6 login_rem_2">
                                        <input type="checkbox" class="form_checkbox show_pass_check" id="show_paas"> 
                                        <div class="login_rem">Show password</div>
                                    </div>
                                  </div>-->
                                  
                                 <label for="invalid_password" class="error" id="invalid_password" ></label>
									<span  id="invalid_password_alpha_2" class="error pass_2" style="display:none;">Please Enter Alphanumeric Values and Special Characters for Password</span>
                                
                                <div class="col-lg-12 login_form_textbox">
                                    <div class="input-group forgot_email">
                                      <span class="input-group-addon"><i class="fa fa-lock fa-lg"></i></span>
                                      <input style="height:34px; border:1px solid #ababab; font-size:13px; font-family: Verdana,sans-serif;" class="form-control" name="confirm_password" id="confirm_password" type="password" placeholder="CONFIRM NEW PASSWORD" required>
                                    </div>
                                    <!--<div class="req_star_textbx"><font style="color:red">*</font></div>-->
                                </div>
                                <div class="col-lg-12 error_cls">
                                    	<div id="error_popup" name="error_popup">
                            			</div>
                                    </div>
                                <div class="col-lg-12">
                                <div class="col-lg-6">
                                    <div class="form-group login_btn_1" style="margin-left:-15px !important;">
                                        <input style="background-color:#8533ff !important; font-family: Verdana,sans-serif; font-size:14px !important; width:150px; padding-left:10px !important;" type="submit" class="btn login_form_btn" value="RESET PASSWORD" name="submit" id="submit">
                                       
                                    </div>
                                 </div>
                                 <div class="col-lg-6">
                                    <div class="form-group login_btn_1" style="margin-top:7px; margin-left:-25px;">
                                        <b>or</b>  <a onclick="open_login()" style="font-size:15px; color:#8533ff; font-family: Verdana,sans-serif;"><b>Cancel</b></a>
										<!--<input type="button"  class="btn login_form_btn" value="BACK" onclick="open_login()">-->
                                    </div>
                                 </div>   
                                </div>
                               <hr>  
                             </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
	</div>

<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>-->
<script src="<?php echo base_url(); ?>public/js/jquery.min.js"></script>
<script src="http://code.jquery.com/jquery-1.10.2.js"></script>
  <script type="text/javascript">
    
  $(document).ready(function(){

    $("#reset_password_model").submit(function(e){

    e.preventDefault();

     }).validate({

         rules: {

          new_password: {

            required:true,

            minlength:8,

          },

          confirm_password: {

            required:true,

            minlength:8,

            equalTo: "#new_password"

          },

         },

          messages: {

          new_password: {

            required:"Password is required.",

            minlength:"Password must contained at least 8 characters",

          },

          confirm_password: {

            required:"Confirm Password is required.",

            minlength:"Password must contained at least 8 characters",

            equalTo:"Password And Confirm Password Should Match.",

          },

          },


        
        submitHandler: function(form) {
          $(".modal-loader").fadeIn("slow");
          var frm=$(form).serialize();

          $.ajax({

              url:base_url+"index/forgot_password",

                        type: "post",

                        data: {'csrf_test_name':csrf_hash,"frm":frm,},

            dataType: 'json',

                        success: function(res) {

              //alert(res);
              $(".modal-loader").fadeOut("slow");
                            if(res == true)

              { 
                    $('#forgotpassword_modal').modal('hide');
                    $('#forgotpassword_modal #get_email').val('');
                    //$("#reset_password_model").modal('show');
                    $('#forget_pass_success').modal('show');
              }

              else

              {

                $('#forgotpassword_modal #get_email').val('');

                $("#forgot_password_frm #error_popup").html("<div class='error_login'><span>Error:</span>This email ID is not registered with us</div>");

              }

            }

          });

      }
    });
    });
      
  </script>