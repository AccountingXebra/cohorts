

    <!-- //////////////////////////////////////////////////////////////////////////// -->

    <!-- START MAIN -->


    <div id="main">

      <!-- START WRAPPER -->

      <div class="wrapper">

       

       	 <!-- START LEFT SIDEBAR NAV-->

        <!-- END LEFT SIDEBAR NAV-->

        <!-- //////////////////////////////////////////////////////////////////////////// -->

        <!-- START CONTENT -->

        <section id="content">

          <div class="container">

            <div class="page-content">

              <div class="row">
                
                <div class="modal-dialog modal-lg signup_form">
            <!-- Modal content-->
            <div class="container">
                <div class="row signup_form">
                <div class="signup_frm">
                    <div class="modal-content">
                        <div class="modal-header">

                            <h4 class="modal-title"><img src="<?php echo base_url(); ?>home_assets/images/header_1.png" alt="eazyinvoice-logo" class="logo_style_3_popup"/></h4>
                            <label class="logo_line_2">Most Social Invoicing & Business Intelligence Platform</label>
                        </div>
                        <div class="modal-body">
                            <div>
                            <label class="logo_line_2">Please register to make invoicing incredibly easy!</label>
                                
                            </div>
                            <form id="google_login_form" name="google_login_form" class="" method="post" accept-charset="utf-8" novalidate="true" action="<?php echo base_url();?>Index/insert_google_data">
                            <div class="row">
								<?php $csrf = array(
					'name' => $this->security->get_csrf_token_name(),
					'hash' => $this->security->get_csrf_hash()
				);
				?>
				<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
                           		 <div class="col-lg-12 error_cls">
                                    	<div id="error_popup" name="error_popup">
                            			</div>
                                    </div>
                                <?php $user_info = $this->session->userdata('user_info');?>		
                                <div class="col-lg-12 login_form_textbox">
                                    <div class="input-group">
                                      <span class="input-group-addon"><i class="fa fa-at fa-lg"></i></span>
                                      <input class="form-control" name="email" id="email" type="text" placeholder="example@companyname.com" value="<?php echo $user_info['user_profile']['email']; ?>" readonly="readonly">
                                    </div>
                                    <div class="req_star_textbx"><font style="color:red">*</font></div>
                                </div>
                                <div class="col-lg-12 login_form_textbox">
                                   <div class="input-group">
                                          <span class="input-group-addon"><i class="fa fa-lock fa-2x"></i></span>
                                          <input class="form-control password_strength show_password" name="password_2" id="password_2" type="password" placeholder="Password(AlphaNumeric - min. 8 characters)" onblur="alphanumeric(document.signup_frm.password_2)">
                                    </div>
                                    <div class="req_star_textbx"><font style="color:red">*</font></div>
                                </div>
								<div class="col-lg-12">
                                 <span id="pwdMeter" class="neutral"></span>
								</div>
									<span  id="invalid_password_alpha"  class="error pass_2" style="display:none;">Please Enter Alphanumeric Values and Special Character for Password</span>
                               <div class="col-lg-12 login_form_textbox">
                                  <div class="col-lg-6 login_rem_2">
                                        <input type="checkbox" class="form_checkbox show_pass_check" id="show_paas"> 
                                        <div class="login_rem">Show password</div>
                                    </div>
                                  </div>
                                  
                                <div class="col-lg-12 login_form_textbox">
                                   <div class="input-group">
                                          <span class="input-group-addon"><i class="fa fa-refresh fa-lg"></i></span>
                                          <input class="form-control" name="confirm_password" id="confirm_password" type="password" placeholder="Confirm Password">
                                    </div>
                                    <div class="req_star_textbx"><font style="color:red">*</font></div>
                                </div>
                                <div class="col-lg-12 login_form_textbox">
                                   <div class="input-group">
                                          <span class="input-group-addon"><i class="fa fa-phone fa-lg"></i></span>
                                          <input class="form-control" name="contact" id="contact" type="text" placeholder="Mobile Number">
                                    </div>
                                    <div class="req_star_textbx"><font style="color:red">*</font></div>
                                </div>

                                
                                    <div class="col-lg-12 login_rem_2 login_form_textbox">
                                        <input type="checkbox" class="form_checkbox" name="subscribe" id="subscribe" checked="checked" value="1"/>
                                        <div class="signup_newsletter">Subscribe to the newsletter</div>
                                    </div>
                                   
                                    <div class="col-lg-12 login_form_textbox">
                                    	<div>By Signing Up, You are agreed to Xebra's <div class="terms_condition"><a href="<?php echo base_url();?>Home1/terms_and_conditions">Terms and Conditions</a></div> and <div class="terms_condition"><a href="<?php echo base_url();?>Home1/privacy_policy">Privacy Policy.</a></div>
                                        </div>
                                    </div> 
                                 
                                <div class="col-lg-12">
                                    <div class="form-group login_btn_1">
                                        <input type="submit"  class="btn login_form_btn" value="SUBMIT" name="signup_submit" id="signup_submit">
                                    </div>
                                </div>
                                
                                    <div class="col-lg-12">
                                        <div class="login_signup_link">Already Registered ?<a href="<?php echo base_url();?>/Home"> Log In</a></div>
                                    </div>
                             </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>


              </div>

            </div>

          </div>        	 

        </section>

        <!-- END CONTENT -->

        </div>

        

        <!-- END WRAPPER -->

      </div>

      <!-- END MAIN -->

<script type="text/javascript">

$(document).ready(function(){
$("#google_login_form").validate({
				 rules: {
					email: {
						required:true,
						email:true,
						/*remote: {
							
							url:  base_url+'Home1/check_email_exist_in_tbl', 
							type: "post",
							data: { 'email':email,
							  email: function() {
								return $( "#email" ).val();
							  }
							}
							   
						},*/
					},
					password_2: {
						required:true,
						minlength:8,
						//regex:"/^(?=.*[a-z])[A-Za-z0-9\d=!\-@._*]+$/",
						
					},
					confirm_password: {
						required:true,
						minlength:8,
						equalTo: '#password_2',
					},
					contact: {
						required:true,
						number:true,
						maxlength:10,
						minlength:10,
					},
				 },
				  messages: {
					email: {
						required:"An Email id is required.",
						email:"Please Enter valid Email ID"
						
					},
					password_2: {
						required:"Password is required.",
						minlength:"Password must contain at least 8 characters.",
					},
					confirm_password: {
						required:"Confirm Password is required.",
						minlength:"Password must contain at least 8 characters.",
						equalTo:"Password Does Not Matched..!!",
					},
					contact: {
						required:"Contact Number is required.(10-digit e.g - 1234567890)",
						number: "Please Enter Valid Contact Number.(10-digit e.g-1234567890)",
						maxlength:"Please Enter Valid Contact Number.(10-digit e.g-1234567890)",
						minlength:"Please Enter Valid Contact Number.(10-digit e.g-1234567890)",
					},
				  },
				  submitHandler: function(form) {
						var frm_signup=$(form).serialize();
								 //$(form).ajaxSubmit();
								$.ajax({
								url:base_url+'Home/insert_google_data',
								type:"POST",
								data:{'csrf_test_name':csrf_hash,"frm_signup":frm_signup,},
								dataType: 'json',
								success:function(res){
										//alert(res);
										if(res == true)
										{
												$("#signup_frm #invalid_password_alpha").css('display','none');
												 $("#signup_frm #invalid_password_alpha").find("input[name=password_2], textarea").val("");
												  $("#signup_frm").find("input[type=text], textarea").val("");
												$('#signup_modal').modal('hide');
												$('#successful_signup_model').modal('show');
										}
										else if(res == '0')
										{
											$("#signup_modal #error_popup").html("<div class='error_login'>This email ID is already registered with us !!</div>");
										}
										else if(res == false)
										{
											$("#signup_modal #error_popup").html("<div class='error_login'><span>Error:</span>Email Not Send ! Please try again!</div>");
										}
										
									},
			
							});
							
						}
				  
			});
});
</script>