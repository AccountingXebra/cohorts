<div class="ubea-section header_padding" id="ubea-faq" data-section="faq" >
			<div class="ubea-container">
				<div class="row">
					<div class="col-md-8 col-md-offset-2 text-center ubea-heading">
						<h2>Frequently Asked Questions</h2>
						<p>Finance, Credit Period Tracking, Tax Related</p>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">

						<div class="ubea-accordion">
							<div class="ubea-accordion-heading">
								<div class="icon"><i class="icon-cross"></i></div>
								<h3>What are the currency options available?</h3>
							</div>
							<div class="ubea-accordion-content">
								<div class="inner">
									<p>There are five options. Indian Rupees, Pound, US Dollar, Euro and Yen.</p>
								</div>
							</div>
						</div>
						<div class="ubea-accordion">
							<div class="ubea-accordion-heading">
								<div class="icon"><i class="icon-cross"></i></div>
								<h3>Is there a limit to the credit days I set for payments?</h3>
							</div>
							<div class="ubea-accordion-content">
								<div class="inner">
									<p>No. You can choose the number of credit days based on your payment preferences.</p>
								</div>
							</div>
						</div>
						<div class="ubea-accordion">
							<div class="ubea-accordion-heading">
								<div class="icon"><i class="icon-cross"></i></div>
								<h3>How do I convert estimates into invoices?</h3>
							</div>
							<div class="ubea-accordion-content">
								<div class="inner">
									<ul>
										<li>Go to ‘View Invoice’. </li>
										<li>You can select a previous estimate and resend it as an Invoice by editing it.</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="ubea-accordion">
							<div class="ubea-accordion-heading">
								<div class="icon"><i class="icon-cross"></i></div>
								<h3>How do I set the GST limit for the service I provide?</h3>
							</div>
							<div class="ubea-accordion-content">
								<div class="inner">
									<ul>
										<li>Go to ‘Company Setting’.</li>
										<li>Edit the value in the ‘GST’ textbox.</li>
									</ul>
								</div>
							</div>
						</div>
						
					</div>
					<div class="col-md-6">
						<div class="ubea-accordion">
							<div class="ubea-accordion-heading">
								<div class="icon"><i class="icon-cross"></i></div>
								<h3>How do I set my custom agency commission value?</h3>
							</div>
							<div class="ubea-accordion-content">
								<div class="inner">
									<ul>
										<li>Go to the ‘Create Invoice’ section. </li>
										<li>On filling out the details of the invoice you can set the ‘Agency Commission’ value in percentile.</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="ubea-accordion">
							<div class="ubea-accordion-heading">
								<div class="icon"><i class="icon-cross"></i></div>
								<h3>How do I also create estimates and proformas invoices?</h3>
							</div>
							<div class="ubea-accordion-content">
								<div class="inner">
									<ul>
										<li>Go to ‘Create Invoice’. </li>
										<li>Under ‘Select Type of Bill’ you can choose your preference.</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="ubea-accordion">
							<div class="ubea-accordion-heading">
								<div class="icon"><i class="icon-cross"></i></div>
								<h3>Is the software GST complaint for India?</h3>
							</div>
							<div class="ubea-accordion-content">
								<div class="inner">
									<p>Yes.</p>
								</div>
							</div>
						</div>
						<div class="ubea-accordion">
							<div class="ubea-accordion-heading">
								<div class="icon"><i class="icon-cross"></i></div>
								<h3>How do I change the payment status of invoices?</h3>
							</div>
							<div class="ubea-accordion-content">
								<div class="inner">
									<ul>
										<li>Go to ‘View Invoice’.</li>
										<li>Click ‘Pay/Paid’ to change the status.</li>
									</ul>
								</div>
							</div>
						</div>

					</div>
				</div>
				
				<div class="row">
					<div class="col-md-8 col-md-offset-2 text-center ubea-heading">
						<p>Financial Analysis, Trends</p>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">

						<div class="ubea-accordion">
							<div class="ubea-accordion-heading">
								<div class="icon"><i class="icon-cross"></i></div>
								<h3>Where can I find a summary of my payments on a monthly basis?</h3>
							</div>
							<div class="ubea-accordion-content">
								<div class="inner">
									<p>You can find this in the dashboard.</p>
								</div>
							</div>
						</div>
						<div class="ubea-accordion">
							<div class="ubea-accordion-heading">
								<div class="icon"><i class="icon-cross"></i></div>
								<h3>How do I estimate my commission?</h3>
							</div>
							<div class="ubea-accordion-content">
								<div class="inner">
									<p>You will find a summary of your commissions in the dashboard.</p>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="ubea-accordion">
							<div class="ubea-accordion-heading">
								<div class="icon"><i class="icon-cross"></i></div>
								<h3>Where can I view the breakup of my revenue streams?</h3>
							</div>
							<div class="ubea-accordion-content">
								<div class="inner">
									<p>You can find this in the dashboard. </p>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-md-8 col-md-offset-2 text-center ubea-heading">
						<p>Subscription, Trial Period</p>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">

						<div class="ubea-accordion">
							<div class="ubea-accordion-heading">
								<div class="icon"><i class="icon-cross"></i></div>
								<h3>What are the subscription options and timelines for agencies?</h3>
							</div>
							<div class="ubea-accordion-content">
								<div class="inner">
									<p>TBD</p>
								</div>
							</div>
						</div>
						<div class="ubea-accordion">
							<div class="ubea-accordion-heading">
								<div class="icon"><i class="icon-cross"></i></div>
								<h3>How do I auto-renew my subscription to Xebra?</h3>
							</div>
							<div class="ubea-accordion-content">
								<div class="inner">
									<p>Your annual package will automatically renew after 12 months of your subscription period.</p>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="ubea-accordion">
							<div class="ubea-accordion-heading">
								<div class="icon"><i class="icon-cross"></i></div>
								<h3>What are the payment options to subscribe?</h3>
							</div>
							<div class="ubea-accordion-content">
								<div class="inner">
									<ul>
										<li>Netbanking</li>
										<li>Debit Card</li>
										<li>Credit Card </li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-md-8 col-md-offset-2 text-center ubea-heading">
						<p>Features, Invoicing</p>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="ubea-accordion">
							<div class="ubea-accordion-heading">
								<div class="icon"><i class="icon-cross"></i></div>
								<h3>How many client contacts can I add?</h3>
							</div>
							<div class="ubea-accordion-content">
								<div class="inner">
									<p>You can add up to 2 client contacts.</p>
								</div>
							</div>
						</div>
						<div class="ubea-accordion">
							<div class="ubea-accordion-heading">
								<div class="icon"><i class="icon-cross"></i></div>
								<h3>How do I edit pre-entered client information if I need to update the same?</h3>
							</div>
							<div class="ubea-accordion-content">
								<div class="inner">
									<ul>
										<li>Go to ‘View Client’</li>
										<li>Click the edit button besides the name of the client whose information you would like to edit</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="ubea-accordion">
							<div class="ubea-accordion-heading">
								<div class="icon"><i class="icon-cross"></i></div>
								<h3>How do I email previous invoices?</h3>
							</div>
							<div class="ubea-accordion-content">
								<div class="inner">
									<ul>
										<li>Go to ‘View Invoice’</li>
										<li>Click the email button besides the invoice you would like to email</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="ubea-accordion">
							<div class="ubea-accordion-heading">
								<div class="icon"><i class="icon-cross"></i></div>
								<h3>How do I add my digital signature?</h3>
							</div>
							<div class="ubea-accordion-content">
								<div class="inner">
									<p>It’s simple. </p>
									<ul>
										<li>Go to ‘Company Setting’</li>
										<li>Add your signature in the size and format that is specified</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="ubea-accordion">
							<div class="ubea-accordion-heading">
								<div class="icon"><i class="icon-cross"></i></div>
								<h3>How can I add a P.O. number?</h3>
							</div>
							<div class="ubea-accordion-content">
								<div class="inner">
									<ul>
										<li>Visit ‘Create Invoice’</li>
										<li>Add the P.O. number if any</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="ubea-accordion">
							<div class="ubea-accordion-heading">
								<div class="icon"><i class="icon-cross"></i></div>
								<h3>How do I replicate and send invoices based on previous ones?</h3>
							</div>
							<div class="ubea-accordion-content">
								<div class="inner">
									<ul>
										<li>Visit ‘Create Invoice’</li>
										<li>Select ‘Create recurring invoice’</li>
										<li>Select the invoice that you would like to replicate</li>
									</ul>
								</div>
							</div>
						</div>
						
					</div>
					<div class="col-md-6">
						<div class="ubea-accordion">
							<div class="ubea-accordion-heading">
								<div class="icon"><i class="icon-cross"></i></div>
								<h3>How can I customise the invoice with my company logo and instructions?</h3>
							</div>
							<div class="ubea-accordion-content">
								<div class="inner">
									<ul>
										<li>Go to ‘Company Setting’</li>
										<li>Fill in your accurate details</li>
										<li>Add your logo within the dimensions of 228 x 107 pixels</li>
										<li>Customize the header and footer of your </li>
									</ul>
								</div>
							</div>
						</div>
						<div class="ubea-accordion">
							<div class="ubea-accordion-heading">
								<div class="icon"><i class="icon-cross"></i></div>
								<h3>Where do I download previous invoices?</h3>
							</div>
							<div class="ubea-accordion-content">
								<div class="inner">
									<ul>
										<li>Go to ‘View Invoice’</li>
										<li>Click the download button besides the invoice you would like to download</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="ubea-accordion">
							<div class="ubea-accordion-heading">
								<div class="icon"><i class="icon-cross"></i></div>
								<h3>Where can I find the archives of previous invoices?</h3>
							</div>
							<div class="ubea-accordion-content">
								<div class="inner">
									<p>Go to ‘View Invoice’</p>
								</div>
							</div>
						</div>
						<div class="ubea-accordion">
							<div class="ubea-accordion-heading">
								<div class="icon"><i class="icon-cross"></i></div>
								<h3>How many invoice design formats do you offer?</h3>
							</div>
							<div class="ubea-accordion-content">
								<div class="inner">
									<p>Presently there are three design formats you can choose from. In time, we shall increase this.</p>
								</div>
							</div>
						</div>
						<div class="ubea-accordion">
							<div class="ubea-accordion-heading">
								<div class="icon"><i class="icon-cross"></i></div>
								<h3>How do I reissue a backdated invoice?</h3>
							</div>
							<div class="ubea-accordion-content">
								<div class="inner">
									<ul>
										<li>Go to ‘View Invoice’</li>
										<li>Select any backdated invoice and resend it by clicking the envelop icon</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="ubea-accordion">
							<div class="ubea-accordion-heading">
								<div class="icon"><i class="icon-cross"></i></div>
								<h3>How can my clients view the invoices I have raised?</h3>
							</div>
							<div class="ubea-accordion-content">
								<div class="inner">
									<p>TBD</p>
								</div>
							</div>
						</div>

					</div>
				</div>
				
				<div class="row">
					<div class="col-md-8 col-md-offset-2 text-center ubea-heading">
						<p>Reviews, Reports</p>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">

						<div class="ubea-accordion">
							<div class="ubea-accordion-heading">
								<div class="icon"><i class="icon-cross"></i></div>
								<h3>Will my name be visible when I leave behind a testimonial?</h3>
							</div>
							<div class="ubea-accordion-content">
								<div class="inner">
									<p>You can choose to have your name visible or invisible to clients.</p>
								</div>
							</div>
						</div>
						<div class="ubea-accordion">
							<div class="ubea-accordion-heading">
								<div class="icon"><i class="icon-cross"></i></div>
								<h3>What kind of reports can I generate?</h3>
							</div>
							<div class="ubea-accordion-content">
								<div class="inner">
									<ul>
										<li>Taxation summary</li>
										<li>Overdue clients</li>
										<li>Service-wise revenue</li>
										<li>Client-wise revenue contribution</li>
										<li>Month and inter-month revenue report</li>
										<li>Budgeted v/s achieved graph</li>
										<li>Average days of payment</li>
										<li>Average media commission</li>
										<li>Client rating report</li>
										<li>Credit history report</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="ubea-accordion">
							<div class="ubea-accordion-heading">
								<div class="icon"><i class="icon-cross"></i></div>
								<h3>How do I rate my clients?</h3>
							</div>
							<div class="ubea-accordion-content">
								<div class="inner">
									<ul>
										<li>Visit ‘Rate Client’</li>
										<li>Add an appropriate rating on a scale of 1-5</li>
										<li>This rating will not be visible to your client</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
				
			</div>
		</div>
    	</div>