<div id="ubea-contact" data-section="contact" class="ubea-cover ubea-cover-xs" >
	<div class="overlay"></div>
    <div class="ubea-container">
        <div class="container">
            <div class="row">
                <div class="col-md-12" id="contact_sec">
                    <div class="col-md-3">
                        <div class="text_2 text_3">
                            <div class="contact_montserrat_bold">Contact</div>&nbsp;<div class="contact_montserrat_bold_blue">Us</div>
                        </div>
                        <div class="contact_montserrat_light">We would love to hear from you</div>
                    </div>
                    <div class="col-md-9">
                        <div class="contact_form">Write to Us</div>
                        <form id="contact_us_form" name="contact_us_form" method="post">
							<?php $csrf = array(
					'name' => $this->security->get_csrf_token_name(),
					'hash' => $this->security->get_csrf_hash()
				);
				?>
				<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
                            <div class="contact_form_1">
                                <input type="text" id="name" name="name" class="input_text" placeholder="Name">
                            </div>
                            <div class="contact_form_1">
                                <input type="text" id="email" name="email" class="input_text" placeholder="Your Email">
                            </div>
                            <div class="contact_form_1">
                                <input type="text" id="subject" name="subject" class="input_text" placeholder="Subject">
                            </div>
                            <div class="contact_form_1">
                                <textarea class="input_text" placeholder="Your Message"></textarea>
                            </div>
                            <div class="contact_form_1">
                                <button type="button" class="contact_btn">SEND YOUR MESSAGE</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>