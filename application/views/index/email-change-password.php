
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Reset Password - Xebra</title>
	<script src="https://www.google.com/recaptcha/api.js"></script>
	<?php include('header.php'); ?>
    <style type="text/css">
        .forgot_email label.error{
          margin-top: 32px !important;
        }
        .modal-loader {
          position: fixed;
          left: 0px;
          top: 0px;
          width: 100%;
          height: 100%;
          z-index: 9999;
          background: url(./public/images/loader.gif) center no-repeat;
      }
       .dataTables_length {
  
		margin-left: 500px;
		   }
		.eazy{
			font-weight:900%;
		}
		
		/*.dot {
		  height: 100px;
		  width: 100px;
		  background-color: #FF7D9A;
		  border-radius: 50%;
		  display: inline-block;
		}*/
	
		#login_submit:hover{
			background: #8533ff !important;
			color:white !important;
		}
		
		body{
			font-family: "Roboto",sans-serif !important;
		}
	
      </style>
   
            <!-- Modal content-->
			<span class="dot"></span>
            <div class="container " style="margin: 50px 0; width: 100%;">
              <div class="col-md-12">
              	<div class="col-md-4">
              	</div>
                  <div class="row">
                  <div class="col-md-7 login_page" style="margin-left: 0; padding-left: 35px; padding-right: 35px; width:35% !important;">
                      <div class="modal-content" style="box-shadow: none;">
                          <div style="text-align:center; margin:10px 23px 0px 22px;">
							<p style="font-size:18px; font-family: Verdana,sans-serif;"><b>Reset your password</b></p>
							<p style="font-size:13px; font-family: Verdana,sans-serif; font-weight:500 !important;">Your password should be difficult for others to guess</p>
							<p style="font-family: Verdana,sans-serif; font-size:13px;"> We recommend that you use a combination of upper case and lower case letter as well as numbers</p>
                          </div>
						
							<div class="modal-body" style="margin:-10px 10px 0px 18px !important;">
								<!--<form action="" id="login_frm" name="login_frm" class="" method="post" accept-charset="utf-8" novalidate="true">-->
								<form action="" id="reset_password_frm" name="reset_password_frm" class="" method="post" accept-charset="utf-8" novalidate="true">
									<?php $csrf = array(
					'name' => $this->security->get_csrf_token_name(),
					'hash' => $this->security->get_csrf_hash()
				);
				?>
				<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
									<div class="row">
										<div class="col-lg-12 login_form_textbox" style="margin-bottom:12px;">
											<div class="input-group forgot_email" style="border:1px solid #ababab; border-radius:4px;">
											  <span class="input-group-addon" style="padding: 6px 13px !important; border:none !important; border-right:1px solid #ababab !important;"><i class="fa fa-lock fa-lg"></i></span>
											 
											  <input style="font-family: Verdana,sans-serif; height:34px; border:none !important; font-size:12px; width:75%;" class="form-control password_strength show_password" name="new_password" id="new_password" type="password" placeholder="NEW PASSWORD" onblur="alphanumeric2(document.reset_password_frm.new_password)">
											</div>
											<!--<div class="req_star_textbx"><font style="color:red">*</font></div>-->
											<div class="hide-show">
												<span style="position:absolute; left:83%; top:4px; font-family: 'Roboto', sans-serif !important;"><img width="29" height="28" src="<?php echo base_url(); ?>public/images/no-monkey.png" alt="small-eazy"></img></span>
											</div>
										</div>
										<div class="col-lg-12 login_form_textbox" style="margin-top:-20px; margin-bottom:-12px !important;">
											<label style="margin:-17% 0 0 14px !important;" for="password_2" class="error pass-error" style="display: none;">Password must contain at least 8 characters.</label>
											<label style="font-family: Verdana,sans-serif; font-size:10.2px !important; text-align:left;">Min 8 Characters, 1 Uppercase, 1 Number & 1 Special Character</label>
										</div>
										<div class="col-lg-12 login_form_textbox" >
											<div class="input-group forgot_email" style="border:1px solid #ababab; border-radius:4px;">
												<span style="padding: 6px 13px !important; border:none !important; border-right:1px solid #ababab !important;" class="input-group-addon"><i class="fa fa-lock fa-lg"></i></span>
												<input style="border:none !important; height:34px; border:1px solid #ababab; font-size:12px; width:75%; font-family: Verdana, sans-serif !important;" class="form-control" name="confirm_password" id="confirm_password" type="password" placeholder="CONFIRM NEW PASSWORD" required>
											</div>
											<div class="hide-show-confirm">
												<span style="position:absolute; left:83%; top:4px; font-family: 'Roboto', sans-serif !important;"><img width="29" height="28" src="<?php echo base_url(); ?>public/images/no-monkey.png" alt="small-eazy"></img></span>
											</div>
											<!--<div class="req_star_textbx"><font style="color:red">*</font></div>-->
										</div>
										<input type="hidden" name="email" value="<?php echo $email_ei; ?>">
										<input type="hidden" name="token" value="<?php echo $token_ei; ?>">
										<!-- Recaptcha Here -->
										<!--<div class="g-recaptcha" data-sitekey="6LezsAMfAAAAAMgw6faSmKmz_zCQaABku2Mkn6Pb" data-callback="onSubmit" data-size="invisible"></div>-->
										
										<div class="g-recaptcha" data-sitekey="<?= recaptcha_invisible ?>" data-callback="onSubmit" data-size="invisible"></div>
										
										<div class="col-lg-12 error_cls">
											<div id="error_popup" name="error_popup"></div>
										</div>
										<div class="col-lg-12" style="margin-top:5px;">
											<div class="col-lg-6">
												<div class="form-group login_btn_1" style="margin-left:-15px !important;">
													<input onMouseOver="this.style.color='#FFFFFF'" style="background-color:#8533ff !important; font-family: Verdana,sans-serif; font-size:13px !important; width:150px; padding-left:14px !important;" type="submit" class="btn login_form_btn" value="RESET PASSWORD" name="submit" id="submit">
												</div>
											</div>
											<div class="col-lg-6">
												<div class="form-group login_btn_1" style="margin-top:5px; margin-left:0px; font-size:16px;">
													<b>or &nbsp;</b>  <a href="<?php echo base_url();?>" style="font-size:15px; color:#8533ff; font-family: Verdana,sans-serif;"><b>Cancel</b></a>
													<!--<input type="button"  class="btn login_form_btn" value="BACK" onclick="open_login()">-->
												</div>
											</div>   
										</div>
									</div>
									
								</form>
							</div>
					  
					  </div>
					  
                  </div>
              </div>
          </div>
        </div>	
		<div class="footer" style="margin-top:75px; border-top:1px solid #C0C0C0;">
			<div class="row" style="text-align:center; padding-top:10px;">
				<div style="margin-right:-15px; font-size:13px;">
					<p style="color:white;"><a style="color:white;" href="https://www.xebra.in/terms-conditions/" target="_blank"> Terms & Conditions </a> | <a style="color:white;" href="https://www.xebra.in/privacy-policy/" target="_blank"> Privacy Policy </a> | <a style="color:white;" href="https://www.xebra.in/support-faq/" target="_blank"> FAQs </a></p>
				</div>
				<div style="margin-right:-10px; margin-top:5px;">
					<label style="font-size:13px; color:#B0B7CA;">Copyright &copy; <?php echo date('Y');?> Xebra. All Rights Reserved</label>
				</div>
			</div>
		</div>
		
		<div id="change-pwd-success" class="modal success modal-set" style="width:470px; margin-left:34%; margin-top:100px;">
		   <!--<img class="geen" src="<?php echo base_url();?>asset/images/green.png">-->
		   <div class="modal-content success-msg">
			  <div style="text-align:center; padding-right:10px; padding-top:10px;">
			  <!--<a href="" class="modal-close close-pop pass-suc"><img src="<?php echo base_url();?>asset/images/popupdelete.png"></a></div>-->	
			  <div style="margin-left:10px;"><p><b>Your new password has been set. Please Log In</b></p></div>
		   </div>
		</div>
		</div>
		<div id="something-wrong" class="modal modal-set" style="width:470px; margin-left:446px; margin-top:100px;">
		   <!--<img class="geen" src="<?php echo base_url();?>asset/images/green.png">-->
		   <div class="modal-content warning-msg">
			  <div style="text-align:right; padding-right:10px; padding-top:12px;"><a href="" class="modal-close close-pop pass-suc"><img src="<?php echo base_url();?>asset/images/popupdelete.png" alt="delete"></a></div>	
			  <div style="margin-left:10px; font-size:16px; margin-top:-24px !important;"><p style="text-align:center; color:red;">Something has gone wrong. Please try again</p></div>
		   </div>
		</div>
		
		<script type="text/javascript">
			$(document).ready(function() {
			$(".pass-error").hide();
			$("#new_password").blur(function(){
				var txt = $(this).val();
				var letters = /^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%*#?&])[A-Za-z\d$@$!%*#?&]{8,}$/; 
				if(txt.match(letters)){ 
					$(".pass-error").hide();
					return true; 
				}else { 
					$(".pass-error").show();
					return false; 
				} 
			}); 
				
				//To show modal
				/*$('#submit').click(function(){
					//$("#change-pwd-success").modal('show');
					$("#something-wrong").modal('show');
				});*/
				
				//To close modal 	
				$('.pass-suc').click(function(){
					$("#change-pwd-success").modal('close');
					$("#something-wrong").modal('close');
				});				
				
				$('.hide-show-confirm').show();
				$('.hide-show-confirm span').addClass('show')
				  
				$('.hide-show-confirm span').click(function(){
				if( $(this).hasClass('show') ) {
					//$(this).text('Hide');
					$(this).html('<img width="29" height="28" src="<?php echo base_url(); ?>public/images/off-monkey.png" alt="small-eazy"></img>');
					$('#confirm_password').attr('type','text');
					$(this).removeClass('show');
				} else {
					//$(this).text('Show');
					$(this).html('<img width="29" height="28" src="<?php echo base_url(); ?>public/images/no-monkey.png" alt="small-eazy"></img>');
					$('#confirm_password').attr('type','password');
					$(this).addClass('show');
				}
				});
				
				/*$("#reset_password_frm").validate({
					rules:{
						new_password: {
							required:true,
							checklowercase: true,
							checkuppercase: true,
							checknumber: true,
							checkspecialchar: true,
							minlength:8,
						},
						confirm_password: {
							required:true,
							minlength:8,
							equalTo: '#new_password',
						},
					},
				  messages:{
					new_password: {

						required:"Password is required",
						checklowercase:"At least 1 Lower case",
						checkuppercase:"At least 1 Upper case",
						checknumber:"At least 1 Number",
						checkspecialchar:"At least 1 Special Character",
						minlength:"Password must contain at least 8 characters.",

					},  
					confirm_password: {

						required:"Confirm Password is required.",

						minlength:"Password must contain at least 8 characters.",

						equalTo:"Your password doesn't match",

					},
				  },
				  success: function(e) {
                       
				  },
				  submitHandler: function(form) {
					form.submit();
				  },
				});*/
			});
			
		</script>
		<?php include('footer.php'); ?>

