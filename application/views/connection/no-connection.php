<?php $this->load->view('template/header'); ?>
<!-- END HEADER -->
<!-- //////////////////////////////////////////////////////////////////////////// -->
<style type="text/css">
	#breadcrumbs-wrapper{
		padding-bottom:10px !important;
	}
	
	#search_comp[type=text] {
		width: 90% !important;
		box-sizing: border-box !important;
		border: 1px solid #ccc !important;
		border-radius:4px !important;
		font-size: 16px !important;
		background-color: white !important;
		background-image: url('<?php echo base_url(); ?>asset/css/img/icons/search.png') !important;
		background-position: 260px 10px !important; 
		background-repeat: no-repeat !important;
		padding: 12px 20px 12px 14px !important;
		outline: none !important;
	}
	
	#find_contact{
		height:45px;
		line-height:45px;
	}
	
	#view_sent_request{
		color:#50E2C2 !important;
		font-size:15px;
	}
	
	.request{
		padding-top:12px !important;
		/*border-left:1px solid #bec4d4;*/
		margin-left:-15px !important;
	}
	
	.top-label{
		font-size:13px;
		margin-top:30px;
	}
	
	.content-div{
		margin-top:20px;
		margin-left:20px;
	}
	
	.company-div{
		height: 250px;
		width: 24% !important;
		margin: 5px;
		padding: 10px;
		border-radius:5px;
	}
	
	.company-div:hover{
		box-shadow: 0 1px 10px #7864e94d;
	}
	
	.white-box{
		background-color:#fff !important;
	}
	
	.div_close{
		margin-right:8px; 
		margin-top:15px;
	}
	
	.text-center{
		text-align:center;
	}
	
	.org-name{
		margin:8px 0;
		font-size:15px !important;
	}
	
	.connect{
		background-color:#fff !important;
		border:1px solid #7864e9 !important;
		color: #7864e9 !important;
		height:35px !important;
		line-height:35px !important;
		margin:15px 0;
	}
	
	.com-loc{
		font-size:20px !important;
	}
	
	.org-tagline{
		font-size:13px !important;
	}
	
	.org-loc{
		font-size:12px !important;
	}
		
	.org_details{
		background-color:#fff !important;
		text-align:center;
	}
		
	#new-connection{
		overflow-x:hidden !important;
		top:25% !important;
	}
	
	#new-connection .modalheader{
		padding:22px 30px !important;
	}
	
	#new-connection .conn-org{
		text-align:center;
		font-size:14px;
	}
	
	#new-connection .conn-org-tagline{
		font-size:12px;
	}
	
	.radio-list{
		padding:30px 0 !important;
	}
	
	 #con-client[type="radio"] + label:after {
		 margin:1px !important;
	 }
	 
	 #con-vendor[type="radio"] + label:after {
		 margin:1px !important;
	 }
	
	 #con-both[type="radio"] + label:after {
		 margin:1px !important;
	 }
	 
	 .suceess-msg{
		 font-size:14px;
	 }
	 
	.top-activity{
		text-align:right;
		font-size:12px;
		margin-top:30px;
		margin-right:35px;
	}
	
	.close-pop{
		cursor:pointer !important;
	}
</style>
<!-- START MAIN -->
<div id="main" height="80%">
  <!-- START WRAPPER -->
  <div class="wrapper">
    <!-- START LEFT SIDEBAR NAV-->
    <?php $this->load->view('template/sidebar'); ?>
    <!-- END LEFT SIDEBAR NAV-->
    <!-- START CONTENT -->
    <section id="content" class="bg-cp sales_invoices-search">
      <div id="breadcrumbs-wrapper">
        <div class="container">
          <div class="row">
            <div class="col s10 m6 l4">
              <h5 class="breadcrumbs-title my-ex">My Ecosystem</h5>
              <ol class="breadcrumbs">
                <li><a href="">MY COMMUNITY / MY ECOSYSTEM</a>
              </ol>
            </div>
            <div class="col s2 m6 l4">
                <input type="text" id="search_comp" name="search" value="<?php if(@$post_data['search']){echo $post_data['search']; } ?>" placeholder="Search by company name">
            </div>
			<div class="col s2 m6 l2 request">
				<a href="<?php base_url(); ?>sent_receive" id="view_sent_request" class="">View sent requests</a>
			</div>
			<div class="col s2 m6 l2">
				<a href="<?php base_url(); ?>with_connection" id="find_contact" class="btn btn-theme btn-large modal-trigger">FIND CONTACTS</a>
			</div>
         </div>
       </div>
     </div>
	
	 <?php $this->load->view('connection/new-conn-request'); ?>

     <div class="container custom content-div">
		<div class="row">
			<div class="col l12 s12 m12">
				<div class="col l6 s12 m12">
					<p class="top-label">COMPANIES YOU MAY KNOW</p>
				</div>
				<div class="col l6 s12 m12 right">
					<p class="top-activity"><a href="<?php base_url(); ?>sent_receive" id="view_sent_request" class="">View Activity History</a></p>
				</div>
            </div>
       </div>
	   <div class="row">
			<div class="col l12 s12 m12">
				<div class="col l12 s12 m12">
					
						<!-- Connect Sent Successfully -->
					<div class="col l3 s12 m12 white-box company-div" id="ok" hidden>
						<div class="row">
							<div class="col s12 m12 l12">
								<a id="div_close" class="div_close modal-close close-pop"><img width="18" height="18" src="https://localhost/xebra/asset/images/popupdelete1.png" alt="delete"></a>
							</div>
						</div>
						<div class="row">
							<div class="col s12 m12 l12 text-center">	
							  <img width="150" height="100" src="https://localhost/xebra/public/images/success.png" alt="Logo" class="">
							</div>
						</div>
						<div class="row">
							<div class="col s12 m12 l12 text-center">	
								<label class="suceess-msg"><b>Connection request sent</b></label><br>
								<label class="suceess-msg"><b>successfully</b></label>
							</div>
						</div>
					</div>
					<!----------------      END             --------------------->
					<?php foreach($conn as $key => $value){?>
						<div class="col l3 s12 m12 white-box company-div" id="conn">
						<div class="row">
							<div class="col s12 m12 l12">
								<a id="div_close" class="div_close modal-close close-pop"><img width="18" height="18" src="https://localhost/xebra/asset/images/popupdelete1.png" alt="delete"></a>
							</div>
						</div>
						
						<div class="row">
							<div class="col s12 m12 l12 text-center">	
								<img width="150" height="50" src="<?php echo base_url(); ?>public/images/<?= $conn[$key]->bus_company_logo; ?>" alt="Logo" class="">

							</div>
						</div>
						<div class="row">
							<div class="col s12 m12 l12 text-center">	
								<p class="org-name"><b><?php echo $conn[$key]->bus_company_name; ?></b></p>
							</div>
						</div>
						<div class="row">
							<div class="col s12 m12 l12 text-center">	
								
							</div>
						</div>
						
						<div class="row">
							<div class="col s12 m12 l12 text-center">	
								<i class="material-icons view-icons com-loc">location_on</i><label class="org-loc"><b><?php echo $conn[$key]->name; ?></b></label>
							</div>
						</div>
						
						<div class="row">
							<div class="col s12 m12 l12 text-center">	
								<a id="connect" type="button" class="connect btn btn-theme btn-large modal-trigger" href="#new-connection">CONNECT</a>
							</div>
						</div>
						
					</div>
					<?php }?>
					<!----------------      END             --------------------->
					
					<!----------------      END             --------------------->
					
				</div>
            </div>
       </div>
	   <div class="row">
			<div class="col l12 s12 m12"> <p> &nbsp; &nbsp; </p> </div>
	   </div>
    </div>
   </section>
   <!-- END CONTENT -->
   </div>
   <!-- END WRAPPER -->
</div>
<!-- END MAIN -->

	<!-- New Connection Modal -->
	<div id="new-connection" class="modal modal-md">
		<div class="modalheader">
			<h4>Connection Box</h4>
			<a class="modal-close close-pop"><img src="<?php echo base_url();?>asset/images/popupdelete1.png"></a>
		</div>

		<div class="modalbody">
			<form class="addconn" id="add_connection_info" name="add_connection_info">
			  <?php $csrf = array(
					'name' => $this->security->get_csrf_token_name(),
					'hash' => $this->security->get_csrf_hash()
				);
				?>
				<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
			  <div class="row" style="margin-bottom:0px !important;">
				<div class="col l12 s10 m10 fieldset">
				  <div class="row">
					<div class="col l12 s12 m12 fieldset org_details">
						<p class="conn-org"><b>"Windchimes Communication"</b></p>
						<label class="conn-org-tagline">This Company is connected with me as</label>
					</div>
					<div class="col l12 s12 m12 fieldset org_details radio-list">
						<div class="col l3 s12 m12 fieldset"></div>
						<div class="col l2 s12 m12 fieldset">
							<input type="radio" class="form-check-input" id="con-client" name="connect" value="" checked>
							<label class="form-check-label" for="con-client">Client</label>
						</div>
						<div class="col l2 s12 m12 fieldset">
							<input type="radio" class="form-check-input" id="con-vendor" name="connect" value="">
							<label class="form-check-label" for="con-vendor">Vendor</label>
						</div>
						<div class="col l2 s12 m12 fieldset">
							<input type="radio" class="form-check-input" id="con-both" name="connect" value="">
							<label class="form-check-label" for="con-both">Both</label>
						</div>
						<div class="col l3 s12 m12 fieldset"></div>
					</div>
				</div>
			  </div>

			  <div class="row">
				<div class="col l6 m6 s12 fieldset">
				</div>
				<div class="col l6 m6 s12 fieldset buttonset">
				  <div class="right">
					<button type="submit"id="submit"class="connecting modal-close btn-flat theme-primary-btn theme-btn theme-btn-large right">CONNECT</button>
					<button type="button" class="modal-close btn-flat theme-flat-btn theme-btn theme-btn-large modal-close">CANCEL</button>
				  </div>
				</div>
			  </div>
			</form>
		</div>
	</div>

<script type="text/javascript">
	$(document).ready(function() {
		$('.div_close').on('click', function(){
			console.log('OK');
			$(this).closest(".company-div").remove();
		});
		$('.connecting').on('click', function(){
			console.log('OK');
			$("#conn").hide();
			console.log('BYE');
			$("#ok").show();
			var bus_id=$(this).val();
 		if(bus_id!=''){
 			srec_counter=0;
 			console.log('yeah');
	 		$.ajax({
					type: "POST",
					data: {'csrf_test_name':csrf_hash,'bus_id':bus_id},
					url: base_url+'connection/sent_receive',
					
	                success:function(result)
					{
						var data=JSON.parse(result);

                          $("#employee_name").val(data.name);
                           $("#employee_designation").val(data.designation);
                           $("#reporting_manager_name").val(data.reporting_to);
                           $("#reporting_managers_designation").val(data.manager_designation);

                        

						$('select').material_select();	
					}
	 		});
	 		}	
		});
		
	}); 
</script>

<?php $this->load->view('template/footer'); ?>