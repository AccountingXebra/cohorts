	  <style>
		.total-req{
			font-size:13px;
		}
		
		.request-div:hover{
			box-shadow: 0 1px 10px #7864e94d;
		}
	
		.request-div{
			height: 180px;
			width: 32% !important;
			margin: 10px;
			padding: 20px 10px !important;
			border-radius:5px;
		}
		
		.req_accept{
			background-color:#fff !important;
			border:1px solid #7864e9 !important;
			color: #7864e9 !important;
			height:35px !important;
			line-height:35px !important;
			margin:15px -5px;
		}
		
		.req_decline{
			background-color:#fff !important;
			border:1px solid #ccc !important;
			color: #ccc !important;
			height:35px !important;
			line-height:35px !important;
			margin:15px -5px;
		}
		
		.org-logo-req{
			margin-top:20px;
		}
		
		.top-all{
			text-align:right;
			font-size:12px;
			margin-top:30px;
			margin-right:35px;
		}
	  </style>	
	  <div class="container custom content-div">
		<div class="row">
			<div class="col l12 s12 m12">
				<div class="col l6 s12 m12">
					<p class="top-label">NEW CONNECTION REQUESTS  <label class="total-req">(5)</label></p>
				</div>
				<div class="col l6 s12 m12 right">
					<p class="top-all"><a href="<?php base_url(); ?>sent_receive" id="view_sent_request" class="">View All</a></p>
				</div>
            </div>
       </div>
	   <div class="row">
			<div class="col l12 s12 m12">
				<div class="col l12 s12 m12">
					<div class="col l4 s12 m12 white-box request-div">
						<div class="row">
							<div class="col s12 m12 l4 org-logo-req">	
								<img width="105" height="50" src="https://localhost/xebra/public/images/xebra-logo.png" alt="Logo" class="">
							</div>
							<div class="col s12 m12 l8">	
								<p class="org-name"><b>Windchimes Communication</b></p>
								<label class="org-tagline"><b>Digital Experiential Agency</b></label></br>
								<i class="material-icons view-icons com-loc">location_on</i><label class="org-loc"><b>Mumbai</b></label></br>
								<a style="width:40%; padding:0px !important; margin-right:5px;" href="#" id="req_accept" class="req_accept btn btn-theme btn-large">ACCEPT</a>
								<a style="width:40%; padding:0px !important;" href="#" id="req_decline" class="req_decline btn btn-theme btn-large">DECLINE</a>
							</div>
						</div>
					</div>
					
					<!----------------      END             --------------------->
					<div class="col l4 s12 m12 white-box request-div">
						<div class="row">
							<div class="col s12 m12 l4 org-logo-req">	
								<img width="105" height="50" src="https://localhost/xebra/public/images/sos.png" alt="Logo" class="">
							</div>
							<div class="col s12 m12 l8">	
								<p class="org-name"><b>Secure Assured Services</b></p>
								<label class="org-tagline"><b>Digital Experiential Agency</b></label></br>
								<i class="material-icons view-icons com-loc">location_on</i><label class="org-loc"><b>Mumbai</b></label></br>
								<a style="width:40%; padding:0px !important; margin-right:5px;" href="#" id="req_accept" class="req_accept btn btn-theme btn-large">ACCEPT</a>
								<a style="width:40%; padding:0px !important;" href="#" id="req_decline" class="req_decline btn btn-theme btn-large">DECLINE</a>
							</div>
						</div>
					</div>
					<!----------------      END             --------------------->
					<div class="col l4 s12 m12 white-box request-div">
						<div class="row">
							<div class="col s12 m12 l4 org-logo-req">	
								<img width="105" height="50" src="https://localhost/xebra/public/images/suh.png" alt="Logo" class="">
							</div>
							<div class="col s12 m12 l8">	
								<p class="org-name"><b>Shufl Pvt. Ltd.</b></p>
								<label class="org-tagline"><b>Digital Experiential Agency</b></label></br>
								<i class="material-icons view-icons com-loc">location_on</i><label class="org-loc"><b>Mumbai</b></label></br>
								<a style="width:40%; padding:0px !important; margin-right:5px;" href="#" id="req_accept" class="req_accept btn btn-theme btn-large">ACCEPT</a>
								<a style="width:40%; padding:0px !important;" href="#" id="req_decline" class="req_decline btn btn-theme btn-large">DECLINE</a>
							</div>
						</div>
					</div>
					<!----------------      END             --------------------->
				</div>
            </div>
       </div>
    </div>