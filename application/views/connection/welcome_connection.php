	<?php $this->load->view('template/header'); ?>
    <style>
		.page-content{
			margin-top:50px !important;
		}
		
		.welcom-container{
			margin-top:7% !important;
		}
		
		.breadcrumbs{
			margin-top:10px;
		}
	</style>
	<!-- END HEADER --> 
    <!-- START MAIN -->
    <div id="main">
      <!-- START WRAPPER -->
      <div class="wrapper">
        <!-- START LEFT SIDEBAR NAV-->
        <?php $this->load->view('template/sidebar'); ?>
        <!-- END LEFT SIDEBAR NAV-->
        <!-- START CONTENT -->
        <section id="content">
          <div class="page-header">
            <div class="container">
              <h2 class="page-title">My Ecosystem</h2>
			  <ol class="breadcrumbs">
				<li><a href="">MY COMMUNITY / MY ECOSYSTEM</a>
              </ol>
              </div>
            </div>
          </div>

          <div class="page-content">
            <div class="container">
              <div class="welcom-container">
                <div class="welcom-content">
                  <p class="welcom-title">Grow Your Business</p>
                  <p class="welcom-text">Find companies in various segment of industries, connect <br> with them and explore synergies</p>
                  <p class="welcome-button"><!-- btn-welcome cri-btn -->
					<a href="<?php base_url(); ?>no_connection" id="get_connect" class="btn btn-theme btn-large modal-trigger">LET'S ADD YOUR FIRST CONNECTION</a>
				  </p>
                </div>
              </div>
            </div>
          </div>
        </section>
        <!-- END CONTENT -->
        </div>
        <!-- END WRAPPER -->
      </div>
      <!-- END MAIN -->
    <?php $this->load->view('template/footer'); ?>