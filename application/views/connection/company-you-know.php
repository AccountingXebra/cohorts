     <style>
		.company-div:hover{
			box-shadow: 0 1px 10px #7864e94d;
		}
		
		.close-pop{
			cursor:pointer !important;
		}
	 </style>
	 <div class="container custom content-div">
		<div class="row">
			<div class="col l12 s12 m12">
				<div class="col l6 s12 m12">
					<p class="top-label">COMPANIES YOU MAY KNOW</p>
				</div>
				<div class="col l6 s12 m12 right">
					<p class="top-activity"><a href="<?php base_url(); ?>sent_receive" id="view_sent_request" class="">View Activity History</a></p>
				</div>
            </div>
       </div>
	   <div class="row">
			<div class="col l12 s12 m12">
				<div class="col l12 s12 m12">
					<div class="col l3 s12 m12 white-box company-div">
						<div class="row">
							<div class="col s12 m12 l12">
								<a id="div_close" class="div_close modal-close close-pop"><img width="18" height="18" src="https://localhost/xebra/asset/images/popupdelete1.png" alt="delete"></a>
							</div>
						</div>
						<div class="row">
							<div class="col s12 m12 l12 text-center">	
								<img width="150" height="50" src="https://localhost/xebra/public/images/Eazy-Invoice-Logo.png" alt="Logo" class="">
							</div>
						</div>
						<div class="row">
							<div class="col s12 m12 l12 text-center">	
								<p class="org-name"><b>Windchimes Communication</b></p>
							</div>
						</div>
						<div class="row">
							<div class="col s12 m12 l12 text-center">	
								<label class="org-tagline"><b>Digital Experiential Agency</b></label>
							</div>
						</div>
						<div class="row">
							<div class="col s12 m12 l12 text-center">	
								<i class="material-icons view-icons com-loc">location_on</i><label class="org-loc"><b>Mumbai</b></label>
							</div>
						</div>
						<div class="row">
							<div class="col s12 m12 l12 text-center">	
								<a id="connect" type="button" class="connect btn btn-theme btn-large modal-trigger" href="#new-connection">CONNECT</a>
							</div>
						</div>
						
					</div>
					
					<!----------------      END             --------------------->
					<div class="col l3 s12 m12 white-box company-div">
						<div class="row">
							<div class="col s12 m12 l12">
								<a id="div_close" class="div_close modal-close close-pop"><img width="18" height="18" src="https://localhost/xebra/asset/images/popupdelete1.png" alt="delete"></a>
							</div>
						</div>
						<div class="row">
							<div class="col s12 m12 l12 text-center">	
								<img width="150" height="50" src="https://localhost/xebra/public/images/sos.png" alt="Logo" class="">
							</div>
						</div>
						<div class="row">
							<div class="col s12 m12 l12 text-center">	
								<p class="org-name"><b>Security Assured Services</b></p>
							</div>
						</div>
						<div class="row">
							<div class="col s12 m12 l12 text-center">	
								<label class="org-tagline"><b>Digital Experiential Agency</b></label>
							</div>
						</div>
						<div class="row">
							<div class="col s12 m12 l12 text-center">	
								<i class="material-icons view-icons com-loc">location_on</i><label class="org-loc"><b>Pune</b></label>
							</div>
						</div>
						<div class="row">
							<div class="col s12 m12 l12 text-center">	
								<a href="#" id="connect" class="connect btn btn-theme btn-large">CONNECT</a>
							</div>
						</div>
						
					</div>
					<!----------------      END             --------------------->
					<div class="col l3 s12 m12 white-box company-div">
						<div class="row">
							<div class="col s12 m12 l12">
								<a id="div_close" class="div_close modal-close close-pop"><img width="18" height="18" src="https://localhost/xebra/asset/images/popupdelete1.png" alt="delete"></a>
							</div>
						</div>
						<div class="row">
							<div class="col s12 m12 l12 text-center">	
								<img width="150" height="50" src="https://localhost/xebra/public/images/suh.png" alt="Logo" class="">
							</div>
						</div>
						<div class="row">
							<div class="col s12 m12 l12 text-center">	
								<p class="org-name"><b>Shufl Pvt. Ltd.</b></p>
							</div>
						</div>
						<div class="row">
							<div class="col s12 m12 l12 text-center">	
								<label class="org-tagline"><b>Digital Experiential Agency</b></label>
							</div>
						</div>
						<div class="row">
							<div class="col s12 m12 l12 text-center">	
								<i class="material-icons view-icons com-loc">location_on</i><label class="org-loc"><b>Delhi</b></label>
							</div>
						</div>
						<div class="row">
							<div class="col s12 m12 l12 text-center">	
								<a href="#" id="connect" class="connect btn btn-theme btn-large">CONNECT</a>
							</div>
						</div>
						
					</div>
					<!----------------      END             --------------------->
					<div class="col l3 s12 m12 white-box company-div">
						<div class="row">
							<div class="col s12 m12 l12">
								<a id="div_close" class="div_close modal-close close-pop"><img width="18" height="18" src="https://localhost/xebra/asset/images/popupdelete1.png" alt="delete"></a>
							</div>
						</div>
						<div class="row">
							<div class="col s12 m12 l12 text-center">	
								<img width="150" height="50" src="https://localhost/xebra/public/images/execu.png" alt="Logo" class="">
							</div>
						</div>
						<div class="row">
							<div class="col s12 m12 l12 text-center">	
								<p class="org-name"><b>Executive Security</b></p>
							</div>
						</div>
						<div class="row">
							<div class="col s12 m12 l12 text-center">	
								<label class="org-tagline"><b>Digital Experiential Agency</b></label>
							</div>
						</div>
						<div class="row">
							<div class="col s12 m12 l12 text-center">	
								<i class="material-icons view-icons com-loc">location_on</i><label class="org-loc"><b>Ratnagiri</b></label>
							</div>
						</div>
						<div class="row">
							<div class="col s12 m12 l12 text-center">	
								<a href="#" id="connect" class="connect btn btn-theme btn-large">CONNECT</a>
							</div>
						</div>
						
					</div>
					<!----------------      END             --------------------->
				</div>
            </div>
       </div>
	 </div>