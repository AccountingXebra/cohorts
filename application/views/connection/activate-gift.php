<!DOCTYPE">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Activate Gift</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700" rel="stylesheet">
    <style type="text/css">
        .first-label{
			margin-top:2%;
		}
		
		.middle-div{
			margin-top:1%;
		}
		
		.img-div{
			text-align:center;
		}
		
		.one-hr{
			width:60%;
		}
		
		.list-buzz{
			width: 100%;
			margin-right: 10px;
			text-align:center;
		}
		
		.feature_list li{
			color:#fff;
			font-size:14px;
			text-align:left;
			margin-left:50px;
		}
		
		.mar-top{
			margin-top:-20px;
		}
		
		.numberCircle {
		  border-radius: 50%;
		  behavior: url(PIE.htc);
		  /* remove if you don't care about IE8 */
		  width: 50px;
		  height: 50px;
		  padding: 13px;
		  background: #50e3c2;
		  border: 2px solid #fff;
		  color: #fff;
		  text-align: center;
		  font: 20px Arial, sans-serif;
		}
		
		.step-one{
			border: 2px solid #fff;
			border-radius:5px;
			padding: 14px 0px 12px 52px !important;
			margin-top:7px;
			margin-left:10px;
			line-height:7px;
			color:#fff;
			font-size:13px;
		}
		
		.step-three{
			border: 2px solid #fff;
			border-radius:5px;
			padding: 14px 0px 12px 16px !important;
			margin-top:7px;
			margin-left:35px;
			line-height:7px;
			color:#fff;
			font-size:13px;
		}
		
		.gift-terms{
			background-color:#fff;
			height:9%;
			text-align:center;
			padding-top:12px;
			font-family: 'Roboto', sans-serif;
		}
		
		.accept-btn{
			background-color:#7965E9 !important;
			border:1px solid #7965E9 !important;
			color:#fff;
			margin-left:10px;
		}
		
		.term_cond{
			margin-top:5px;
		}
    </style>
</head>

<body style="margin:0; background:#7965E9;">
	<div class="first-label">
		<h3 style="font-family: 'Roboto', sans-serif; color:fff; text-align:center;">Here is your gift for being a part of Xebra business eco-system<h3/>
	</div>
	<div class="img-div">
		<img height="200" width="800" class="columnImage" src="<?php echo base_url(); ?>public/images/gift.png" border="0"/>
	</div>
	<div class="middle-div">
		<h3 style="font-family: 'Roboto', sans-serif; color:fff; text-align:center;">You will get complete access to Xebra's Invoicing module and Community module<h3/>
		<hr class="one-hr"></hr>
	</div>
	<div class="row list-buzz">
		<div class="col-sm-2"></div>
		<div class="col-sm-4 mar-top">
			<h3 style="font-size:15px; font-family: 'Roboto', sans-serif; color:fff; text-align:center;">What does the invoicing module include?<h3/>
			<ul class="feature_list">
				<li>Prepare your client roster</li>
				<li>Record all your Items</li>
				<li>Create Estimat, Proforma Invoice or Invoice</li>
				<li>Prepare Credit Notes</li>
			</ul>
		</div>
		<div class="col-sm-4 mar-top">
			<h3 style="font-size:15px; font-family: 'Roboto', sans-serif; color:fff; text-align:center;">What does the invoicing module include?<h3/>
			<ul class="feature_list">
				<li>Build </li>
				<li>Record all your Items</li>
				<li>Create Estimat, Proforma Invoice or Invoice</li>
				<li>Prepare Credit Notes</li>
			</ul>
		</div>
		<div class="col-sm-2"></div>
		<hr class="one-hr"></hr>
	</div>
	<div class="row">
		<div class="col-sm-12 mar-top">
			<h3 style="font-size:16px; font-family: 'Roboto', sans-serif; color:fff; text-align:center;">Follow this simple 3 steps process <h3/>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-3 "></div>
		<div class="col-sm-2">
			<p style="float:left;" class="numberCircle">1</p><div class="step-one">Fill Personal Profile </div>
		</div>
		<div class="col-sm-2 ">
			<p style="float:left;" class="numberCircle">2</p><div class="step-one">Fill Company Profile </div>
		</div>
		<div class="col-sm-2 ">
			<p style="float:left;" class="numberCircle">3</p><div class="step-three">Start preparing Invoices </div>
		</div>
		<div class="col-sm-3 "></div>
	</div>
	<footer class="gift-terms">
		<input type="checkbox" id="term_cond" class="term_cond_chkbox" value="1" name="term_cond">
        <label class="checkboxx2 checkboxx-in" for="term_cond"><span class="check-boxs">I accept the <a>terms and conditions</a> and <a>privacy policy</a></span></label>
		<button type="button" class="accept-btn btn btn-info">Accept</button>
	</footer>
</body>
</html>
