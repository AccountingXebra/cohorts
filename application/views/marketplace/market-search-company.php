
	<?php $this->load->view('my-community/Cohorts-header');?>
	<style>
		
		.select-wrapper label.error:not(.active) {
			margin: -30px 0 0 -11px;
		}
		
		/*----------START SEARCH DROPDOWN CSS--------*/
		.select2-container {
		  width: 100% !important;
		}
		
		.select2-container--default .select2-selection--multiple {
			border:1px solid #afb8c9 !important;
			border-radius:5px !important;
		}
		
		input[type="search"]:not(.browser-default) {
		  height: 30px;
		  font-size: 14px;
		  margin: 0;
		  margin-top:8px !important;
		  border-radius: 5px;
		}
		
		.select2-container--default .select2-selection--multiple .select2-selection__rendered {
		  font-size: 13px;
		  line-height: 35px;
		  color: #666;
		  font-weight: 400;
		}
		
		.select2-container--default .select2-selection--multiple .select2-selection__arrow {
		  height: 32px;
		}
		
		.select2-search--dropdown {
		  padding: 0;
		  margin-top:-10px !important;
		}
		
		input[type="search"]:not(.browser-default):focus:not([readonly]) {
		  border-bottom: 1px solid #bbb;
		  box-shadow: none;
		}
		
		.select2-container--default .select2-selection--multiple:focus {
			outline: none;
		}
		
		.select2-container--default .select2-results__option--highlighted[aria-selected] {
			background: #fffaef;
			color: #666;
		}
		
		.select2-container--default .select2-results > .select2-results__options {
		  font-size: 14px;
		  border-radius: 5px;
		  box-shadow: 0px 2px 6px #B0B7CA;
		}
		
		.select2-dropdown {
		  border: none;
		  border-radius: 5px;
		}
		
		.select2-container .select2-selection--multiple {
		  height: 48px;
		}
		
		.select2-container .select2-selection--single {
		  height: 48px;
		}
		
		.select2-results__option[aria-selected] {
		  border-bottom: 1px solid #f2f7f9;
		  padding: 14px 16px;
		  border-top-left-radius: 0;
		}
		
		.select2-container--default .select2-search--dropdown .select2-search__field {
		   /* border: none;*/
			border: 1px solid #d0d0d0;
			padding: 0 0 0 15px !important;
			width: 93.5%;
			max-width: 100%;
			background: #fff;
			border-radius: 4px;
			border-top-left-radius: 4px;
			border-top-right-radius: 4px;
		}
		
		.select2-container--open .select2-dropdown--below {
		  margin-top: 0px;
		/*border-top: 1px solid #aaa;
		  border-radius: 5px !important;*/
		}
		
		ul#select2-currency-results {
		  width: 97%;
		}
		
		/*---Dropdown error message format---*/
		.select-wrapper + label.error{
		 margin: 18px 0 0 -10px;
		} 
		
		select.error + label.error:not(.active){
		 margin: -20px 0 0 -10px; 
		}
		
		select + label.error.active{
		  margin-left: -10px;
		}
	
		body {
			/*overflow-y:hidden;*/
		}
		
		#search_comp[type=text] {
			width: 55% !important;
			box-sizing: border-box !important;
			border: 1px solid #ccc !important;
			border-radius:5px !important;
			font-size: 16px !important;
			background-color: none !important;
			background-image: url('<?php echo base_url(); ?>asset/css/img/icons/search.png') !important;
			background-position: 10px 10px !important; 
			background-repeat: no-repeat !important;
			padding: 12px 20px 12px 40px !important;
			outline: none !important;
		}
		
		#select2-market_country-container{
			margin-top:10px !important;
		}
		
		.select2-container--default .select2-selection--single .select2-selection__arrow {
			top:10px !important;
		}
		
		/* Select Box css */
		
		.multi-loc input[type=text]:not(.browser-default) {
			font-size: 13px !important;
			padding-left:10px !important;
			color: #9e9e9e !important;
		}
		
		.multi-loc .dropdown-content.select-dropdown {
			margin-top:47spx!important;
			overflow-y: scroll !important;
		}
		
		.loc_select .select2{
			width:200px !important;
		}
		
		
		#nature input[type=text]:not(.browser-default) {
			font-size: 13px !important;
			padding-left:10px !important;
			color: #9e9e9e !important;
		}
		
		#nature .dropdown-content.select-dropdown {
			margin-top:47spx!important;
			overflow-y: scroll !important;
		}
		
		.loc_select{
			width:240px !important;
			border:1px solid #e8ebf4;
			height:50px;
			border-radius:5px;
			padding-left:15px;
			margin-bottom:20px;
		}
		
		.nat_select{
			width:240px !important;
			margin-left:23px;
			border:1px solid #e8ebf4;
			height:50px;
			border-radius:5px;
			padding-left:15px;
			margin-bottom:20px;
		}
		/* Select Box css end */
		.page-link{
		   font-size:13px;
		}
		
		.rate-star{
			padding:10px 0  10px 10px !important;
			margin-top:-5px;
		}
		
		#five_star + label {
			padding:0 0 0 25px !important;
		}
		
		#four_star + label {
			padding:0 0 0 25px !important;
		}
		
		#three_star + label {
			padding:0 0 0 25px !important;
		}
		
		#two_star + label {
			padding:0 0 0 25px !important;
		}
		
		#one_star + label {
			padding:0 0 0 25px !important;
		}
		
		[type="checkbox"] + label:before, [type="checkbox"]:not(.filled-in) + label:after {
			margin: 4px 0 0 0 !important;		
		}
	</style>
    <!-- START MAIN -->   
    <div id="main">
		<!-- START WRAPPER -->
		<div class="wrapper">
			<!-- START LEFT SIDEBAR NAV-->
			<?php //$this->load->view('template/sidebar.php');?>
			<!-- END LEFT SIDEBAR NAV-->

			<!-- START CONTENT -->
			<section id="content"> <!---->
			  <form action="<?php echo base_url(); ?>market_place/search_company_result" method="GET" id="search-comp-form">
				<?php $csrf = array(
					'name' => $this->security->get_csrf_token_name(),
					'hash' => $this->security->get_csrf_hash()
				);
				?>
				<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
			  <div class="row">
				<div class="col s12 m12 l3" style="margin-right:-10px !important;">
					<div class="col l12 m12 s12 white-box" style="margin-left:-10px !important; padding-bottom:450px !important;">
						<div class="row border-bottom" style="margin-left:-12px !important;">
							<h6 class="white-box-title" style="padding: 11px 34px !important">Filter by</h6>
						</div>

						<div class="row form-group border-bottom">
							<div class="col s12 m12 l12">
								<div class="col s12 m12 l12">
									<label>NATURE OF BUSINESS</label>
								</div>								
								
								<div class="col s12 m12 l12" style="margin-top:20px !important;">
									<select id="nature_of_business" name="nature_of_business" class="loc_select js-example-basic-multiple form-control"  data-live-search="true">
									
										 
                                  	<option value="">BUSINESS CATEGORY</option>
                                  
                                  <option value="Accounting & Taxation">Accounting & Taxation</option>
                                  <option value="Advertising">Advertising</option>
                                  <option value="Animation Studio">Animation Studio</option>
                                  <option value="Architecture">Architecture</option>
                                  <option value="Arts & Crafts">Arts & Crafts</option>
                                  <option value="Audit & Tax">Audit & Tax</option>
                                  <option value="Brand Consulting">Brand Consulting</option>
                                  <option value="Celebrity Management">Celebrity Management</option>
                                  <option value="Consultant">Consultant</option>
                                  <option value="Content Studio">Content Studio</option>
                                  <option value="Cyber Security">Cyber Security</option>
                                  <option value="Data Analytics">Data Analytics</option>
                                  <option value="Digital Influencer">Digital Influencer</option>
                                  <option value="Digital & Social Media">Digital & Social Media</option>
                                  <option value="Direct Marketing">Direct Marketing </option>
                                  <option value="Entertainment">Entertainment</option>
                                  <option value="Event Planning">Event Planning</option>
                                  <option value="Florist">Florist</option>
                                  <option value="Foreign Exchange">Foreign Exchange</option>
                                  <option value="Financial and Banking">Financial and Banking</option>
                                  <option value="Gaming Studio">Gaming Studio</option>
                                  <option value="DESIGN & UI/UX">DESIGN & UI/UX</option>
                                  <option value="Hardware Servicing">Hardware Servicing</option>
                                  <option value="Industry Bodies">Industry Bodies</option>
                                  <option value="Insurance">Insurance</option>
                                  <option value="Interior Designing ">Interior Designing </option>
                                  <option value="Legal Firm">Legal Firm</option>
                                  <option value="Media Planning & Buying">Media Planning & Buying</option>
                                  <option value="Mobile Services">Mobile Services</option>
                                  <option value="Music">Music</option>
                                  <option value="Non-Profit">Non-Profit</option>
                                  <option value="Outdoor / Hoarding">Outdoor / Hoarding</option>
                                  <option value="Photography">Photography</option>
                                  <option value="Printing">Printing</option>
                                  <option value="Production Studio">Production Studio</option>
                                  <option value="PR / Image Management">PR / Image Management</option>
                                  <option value="Publishing">Publishing</option>
                                  <option value="Real Estate">Real Estate</option>
                                  <option value="Recording Studio">Recording Studio</option>
                                  <option value="Research">Research</option>
                                  <option value="Sales Promotion">Sales Promotion</option>
                                  <option value="Staffing & Recruitment">Staffing & Recruitment</option>
                                  <option value="Stock & Shares">Stock & Shares</option>
                                  <option value="Technology (AI, AR, VR)">Technology (AI, AR, VR)</option>
                                  <option value="Tours & Travel">Tours & Travel</option>
                                  <option value="Training & Coaching">Training & Coaching</option>
                                  <option value="Translation & Voice Over">Translation & Voice Over</option>
                                  <option value="Therapists">Therapists</option>
                                  <option value="Visual Effects / VFX">Visual Effects / VFX</option>
                                  <option value="Web Development">Web Development</option>
                                </select>
										
								</div>
							</div>
						</div>

						
						<div class="row border-bottom" style="margin-left:10px;">
							<div class="col s12 m12 l12">
								<label>RATING</label>
							</div>						
							<div class="col s12 m12 l12 rate-star">
								<div style="float:left; width:30%;">
									<input type="checkbox" id="five_star"  name="five_star"/>
									<label class="label-star" for="five_star">5 Star</label>
								</div>	
								<div style="float:left; width:70%; text-align:right; margin-top:-5px;">
									<span style="font-size:150%; color:#50e3c2;">&starf;</span><span style="font-size:150%; color:#50e3c2;">&starf;</span><span style="font-size:150%;color:#50e3c2;">&starf;</span><span style="font-size:150%;color:#50e3c2;">&starf;</span><span style="font-size:150%;color:#50e3c2;">&starf;</span>
								</div>	
								<div style="float:left; width:30%;">
									<input type="checkbox" id="four_star"  name="four_star"/><label class="label-star" for="four_star">4 Star</label>
								</div>
								<div style="float:left; width:70%; text-align:right;margin-top:-5px;">
									<span style="font-size:150%; color:#50e3c2;">&starf;</span><span style="font-size:150%; color:#50e3c2;">&starf;</span><span style="font-size:150%;color:#50e3c2;">&starf;</span><span style="font-size:150%;color:#50e3c2;">&starf;</span>
								</div>
								<div style="float:left; width:30%;">
									<input type="checkbox" id="three_star"  name="three_star"/><label class="label-star" for="three_star">3 Star</label>
								</div>
								<div style="float:left; width:70%; text-align:right; margin-top:-5px;">
									<span style="font-size:150%; color:#50e3c2;">&starf;</span><span style="font-size:150%; color:#50e3c2;">&starf;</span><span style="font-size:150%;color:#50e3c2;">&starf;</span>
								</div>
								<div style="float:left; width:30%;">
									<input type="checkbox" id="two_star"  name="two_star"/><label class="label-star" for="two_star">2 Star</label>
								</div>
								<div style="float:left; width:70%; text-align:right; margin-top:-5px;">
									<span style="font-size:150%; color:#50e3c2;">&starf;</span><span style="font-size:150%; color:#50e3c2;">&starf;</span>
								</div>
								<div style="float:left; width:30%;">
									<input type="checkbox" id="one_star"  name="one_star"/><label class="label-star" for="one_star">1 Star</label>
								</div>
								<div style="float:left; width:70%; text-align:right; margin-top:-5px;">
									<span style="font-size:150%; color:#50e3c2;">&starf;</span>
								</div>
							</div>
						</div>
						
						<div class="row form-group">
							<div class="col s12 m12 l12">
								<div class="col s12 m12 l12">
									<label>LOCATION</label>
								</div>								
								<!-- <div class="col s12 m12 l12" style="margin-top:20px !important;">
									<select class="mark-country js-example-basic-single" name="market_country" id="market_country">
										<option value="">COUNTRY</option>
										<?php 
										//for($i=0;$i<count($countries);$i++)
										{
											?> <option value="<?php // echo $countries[$i]->country_id; ?>"><?php //echo $countries[$i]->country_name; ?></option> <?php
										}
										?>
										
                                  </select>
								</div> -->
								
								<div class="col s12 m12 l12" style="margin-top:20px !important;">
									<select id="multi_loc" name="multi_loc[]" class="loc_select js-example-basic-multiple form-control"  multiple="multiple" data-live-search="true">
										<option value="">SELECT CITY</option>
										<?php 
										for($i=0;$i<count($cities);$i++)
										{
											?> <option value="<?php echo $cities[$i]['city_id']; ?>"><?php echo $cities[$i]['name']; ?></option> <?php
										}
										?>
									</select>	
								</div>
							</div>
						</div>
						
					</div>			
				</div>
				<div class="col s12 m12 l9" style="margin-left:-22px !important;">
					<div class="page-header">
						<div class="container">
							<h2 class="page-title">My Marketplace</h2>
							<ol class="breadcrumbs" style="margin-top:10px !important;">
								<li><a class="page-link" href="<?= base_url(); ?>market_place/search_company">MY Marketplace</a></li>
							</ol>
						</div>
					</div>
					<div class="page-content">
						<div class="container" style="margin-top:70px !important;">
							<div class="col s12 m12 l12 welcom-container">
								<h5><b>Find Companies</b></h5>
							</div>
							<div class="col s12 m12 l12" style="margin-bottom:15px !important;">
								<p style="text-align:center;">
									Find companies in different industrial segments to connect with <br>
									and grow your business synergies
								</p>
							</div>
							<div class="col s12 m12 l12">
								<div class="col s12 m12 l12" style="text-align:center;">
									<input type="text" id="search_comp" name="search" placeholder="Search by name or nature of business" required>
								</div>
								<div class="col s12 m12 l12" style="margin:-18px 0px 0px 192px !important;">
									<label>E.g. Google or Digital Media</label>
								</div>
								<div class="col s12 m12 l12" style="text-align:center; margin-top:20px !important;">
									<!-- <a href="<?php //echo base_url(); ?>market_place/search_company_result" style="margin-top:0px !important;" class="btn btn-welcome" href="#">Search</a> -->
									<input type="submit" style="margin-top:0px !important;" id="submit" class="btn btn-welcome" value="Search">
								</div>
							</div>
						</div>
					</div>
				</div>
			  </div>
			  </form>
			</section>
			<!-- END CONTENT -->
		</div>
		<!-- END WRAPPER -->
	</div>
	<!-- END MAIN -->
	
	<script type="text/javascript">
		$(document).ready(function(){
			if ($('.menu-icon').hasClass('open')) {
				$('.menu-icon').removeClass('open');

				$('.menu-icon').addClass('close');

				$('#left-sidebar-nav').removeClass('nav-lock');

				$('.header-search-wrapper').removeClass('sideNav-lock');

				$('#header').addClass('header-collapsed');
				
				$('#logo_img').show();
				$('#eazy_logo').hide();
				
				$('#left-sidebar-nav').toggleClass('nav-expanded nav-collapsed');

				$('#main').toggleClass('main-full');
			}

			    $("#market_country").on("change",function(){
   
			    var country_id = $(this).val();
			   if(country_id!=''){
			    
				    // $.ajax({
				    //   url:base_url+'profile/get_states',
				    //   type:"POST",
				    //   data:{'country_id':country_id},
				    //   success:function(res){
				    //     //$(".state_label").hasClass('select-dropdown').find('ul').html(res);
				    //     $("#state1").html(res);
				    //     $("#state1").parents('.input-field').addClass('label-active');
				    //     $('#state1').material_select();
				    //        //  $('#statesec').addClass('bodrpinkright');
				    //   },
				    // }); 
			   		
			   		$.ajax({
				      url:base_url+'market_place/get_cities',
				      type:"POST",
				      data:{'csrf_test_name':csrf_hash,'country_id':country_id},
				      success:function(res){
				      	//alert(res);
				        //$(".state_label").hasClass('select-dropdown').find('ul').html(res);
				        $("#multi_loc").html(res);
				        $("#multi_loc").parents('.input-field').addClass('label-active');
				        $('#multi_loc').material_select();
				           //  $('#statesec').addClass('bodrpinkright');
				      },
				    }); 

			   
			        setTimeout(function(){ 
			          makeredcolor("select2-state-container");     
			        }, 400);
			   
			     }else{
			      // Materialize.toast('Please select the country', 2000,'red rounded');
			     }
			        
			   });

		});
	</script>
	
	<script type="text/javascript">
		//$('.js-example-basic-multiple').select2();
		$('.js-example-basic-single').select2();
		$('#multi_loc').select2({
			placeholder: 'SELECT LOCATION',
			allowClear: true,
			columns: 4,
			search: true,
			selectAll: true
		});
		
	</script>
	
	<!--<script>
		$(document).ready(function() {
            $('#submit').click(function(){
				var multiple_loc = $("#multi_loc").val();  
				var search_query = $("#search_comp").val();
				var country = $("#market_country").val();
				alert(country);
                $.ajax({
                type:"GET",
                url : base_url+'market_place/search_company_result',
                data:{ 
					"multipleloc":multiple_loc,
					"searchquery":search_query,
					"country": country,
				} , 
                success : function(data){
					alert(data);	
                },
				});
            });
        });
	</script>-->
	<?php $this->load->view('template/footer.php');?>	