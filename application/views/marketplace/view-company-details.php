<?php $this->load->view('my-community/Cohorts-header');?>
<style type="text/css">
  .green-bg-right:before {
      top: -39px !important;
      width: 270px;
   }
   .green-bg-right:after { 
      width: 270px;
   }
   
   .act_details{
	   font-size:11px !important;
	   color:white !important;
   }
   
   .act_data{
	   font-size:13px !important;
	   color:white !important;
   }
   
   #my_activity th{
	   color:#808080 !important;
	   font-size:13px;
   }
   
   #my_activity td{
	   font-size:12px;
	   padding: 15px 12px !important;
   }
   
   #my_activity thead{
	   border-bottom:none !important;
   }
   
   #my_activity{
	    border-collapse:separate;
		border-spacing:0px 5px;
   }
   
   .company_name{
	   font-size:15px !important;
	   font-weight:500 !important;
	   /*color:#000000 !important;*/
   }
   
   .logo_co{
	   margin-bottom:-10px !important;
   }
   
   .nature_buzz{
	   font-size:13px !important;
   }
   
   #send-mail-comp{
	   background: white !important;
	   color:#000000;
	   width:150px !important;
	   border:1px solid #7864e9 !important;
	   font-size:14px !important;
   }
   
   #connect-to-comp{
	   width:150px !important;
	   font-size:14px !important;
   }
   
   #connect-to-comp:active {
		background: #7864e9 !important;
   }
   
   .cmp_info{
	   font-size:13px !important;
   }
   
   .cmp_details{
	   font-size:13px !important;
	   color:#000000 !important;
   }
   
	.loc-add{
		margin-left:16px !important;
		width:40px !important;
	}
	
	.view-label{
		font-size:13px !important;
	}
	
	.view-content{
		font-size:13px !important;
		color:#000000 !important;
	}
	
	.select_loc{
		padding-left:10px; 
		padding-right:10px;
		padding-top:12px;
		padding-bottom:12px;		   
		text-align:center; 
		width:15px !important; 
		height:15px !important;
		color:#1a1a1a !important; 
		background-color:#f0f5f5 !important; 
		border-radius:18px; 
		font-size:15px;
	}
	
	#cke_1_contents{
		height:120px !important;
	}
	
	label.fill_details{
		font-size:0.875rem !importantl
	}
	
	.filed_name{
		font-size:14px !important;
		font-weight:500;
	}
</style>
    <!-- START MAIN -->
    <div id="main">
      <!-- START WRAPPER -->
      <div class="wrapper">
        <!-- START LEFT SIDEBAR NAV-->
        <?php //$this->load->view('template/sidebar.php');?>
        <!-- END LEFT SIDEBAR NAV-->
        <!-- //////////////////////////////////////////////////////////////////////////// -->
        <!-- START CONTENT -->
          <section id="content" class="bg-theme-gray">
            <div class="container">
              <div class="plain-page-header">
                <div class="row">
                  <div class="col l6 s12 m6">
                    <a class="go-back underline" href="<?php echo base_url();?>market_place/search_company_result">Back to My Marketplace</a>
                  </div>
                  <div class="col l6 s12 m6">
                    <div class="right-2">
					
                    </div>
                    <div class="right-1">
                      <a href=""><i class="fa fa-print" style="font-size:48px;color:black"></i></a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <?php //echo '<pre>'; print_r($result); echo '</pre>'; ?>
            <div class="container">
				<div class="row">
					<div class="col s12 m12 l12"><!--before-red-bg red-bg-right-->
						<div class="col l12 m12 s12 view-box white-box">
							<div class="row">
								<div class="col l12 m12 s12" style="text-align:right; padding-top:20px !important; padding-right:40px !important;">
									<a href="<?php echo $result[0]['bus_facebook']; ?>" target="_blank"><img width="20" height="20" src="<?php echo base_url(); ?>asset/css/img/icons/facebook.png" alt="Logo"/></a>
									<a href="<?php echo $result[0]['bus_twitter']; ?>" target="_blank"><img width="20" height="20" src="<?php echo base_url(); ?>asset/css/img/icons/twitter.png" alt="Logo"/></a>
									<a href="<?php echo $result[0]['bus_linkedin']; ?>" target="_blank"><img width="20" height="20" src="<?php echo base_url(); ?>asset/css/img/icons/linkedin.png" alt="Logo"/></a>
									<a href="<?php echo $result[0]['bus_googleplus']; ?>" target="_blank"><img width="20" height="20" src="<?php echo base_url(); ?>asset/css/img/icons/google+.png" alt="Logo"/></a>
								</div>
							</div>
							
							<div class="row">
								<div class="col l12 m12 s12" style="text-align:center;">
									<img width="100" height="100" src="<?php echo base_url(); ?>public/upload/company_logos/<?php echo $result[0]['bus_id'].'/'.$result[0]['bus_company_logo']; ?>" alt="Logo" class="logo_co"></img><br><br>
									<label class="company_name"><b><?php echo $result[0]['bus_company_name']; ?></b></label>
								</div>
								<div class="col l12 m12 s12" style="text-align:center;">
									<label class="nature_buzz"><?php echo $result[0]['nature_of_bus']; ?></label>
								</div>
								<div class="col l12 m12 s12" style="text-align:center; margin-bottom:20px !important;">
									&nbsp&nbsp&nbsp
								</div>
								<div class="col l12 m12 s12" style="text-align:center;">
									<a style="margin-top:0px !important;" id="send-mail-comp" class="btn btn-welcome modal-trigger modal-close" href="#send_mailto_comp_modal">SEND MAIL</a>
									<!-- <a style="margin-top:0px !important;" id="connect-to-comp" class="btn btn-welcome modal-trigger modal-close" href="#">CONNECT</a> -->
								</div>
								<!--Bottom Border -->
								<div class="col l11 m12 s12 border-bottom" style="margin-left:11px !important; width:98% !important; margin-top:30px !important;"></div>
							</div>

							<?php

								if($result[0]['bus_incorporation_date'])
								{
									$timestamp = strtotime($result[0]['bus_incorporation_date']);
									$inc_date =  date('nS F, Y', $timestamp); 
									
								}else{
									$inc_date = '-';
								}

								if($result[0]['bus_company_size'])
								{
									$no_of_employee = $result[0]['bus_company_size'];

								}else{

									$no_of_employee = '-';
								}


								if($result[0]['bus_company_revenue'])
								{
									if($result[0]['bus_company_revenue'] ==  5 )
									{
										$revenue = '> '.$result[0]['bus_company_revenue'].' CRORES';	
									}
									if($result[0]['bus_company_revenue'] ==  100)
									{
										$revenue = $result[0]['bus_company_revenue'].' CRORES +';	

									}else{

										$revenue = $result[0]['bus_company_revenue'].' CRORES';
									}
									

								}else{

									$revenue = '-';
								}
								
								 
                                                           

								if($result[0]['rate'])
								{
									$rating = $result[0]['rate'];

								}else{

									$rating = '<span style="font-size:150%; color:#ccc;">&starf;</span><span style="font-size:150%; color:#ccc;">&starf;</span><span style="font-size:150%;color:#ccc;">&starf;</span><span style="font-size:150%;color:#ccc;">&starf;</span><span style="font-size:150%;color:#ccc;">&starf;</span>';
								}


							?>		

							<div class="row">
								<div class="col l12 m12 s12" style="margin-top:20px ;">
									<div class="col l1 m1 s1"></div>
									<div class="col l3 m3 s3">
										<label class="cmp_info">DATE OF INCORPORATION</label><br>
										<label class="cmp_details"><b><?php echo $inc_date; ?></b></label>
									</div>
									<div class="col l3 m3 s3">
										<label class="cmp_info">NO OF EMPLOYEES</label><br>
										<label class="cmp_details"><b><?php echo $no_of_employee; ?> Employees</b></label>
									</div>
									<div class="col l3 m3 s3">
										<label class="cmp_info">REVENUE</label><br>
										<label class="cmp_details"><b> <?php echo $revenue; ?></b></label>
									</div>
									<div class="col l2 m2 s2">
										<label class="cmp_info">RATING</label><br>
										<label class="cmp_details"><b> <?php echo $rating; ?></b></label>
										
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<!-- Address Details Start --->
				<div class="row">
					<div class="col s12 m12 l12">
						<div class="col l12 m12 s12 view-box white-box before-red-bg red-bg-right">
							<div class="row">
								<div class="col l12 m12 s12">
									<h6 class="white-box-title" style="padding: 11px 34px !important">Address Details</h6>
								</div>
							</div>
							<?php 

							if($result[0]['bus_billing_address'])
							{
								$address = $result[0]['bus_billing_address'].', '.$result[0]['name'].', '.$result[0]['state_name'].', '.$result[0]['country_name'].' '.$result[0]['bus_billing_zipcode'];
							}

							?>
							<div class="row">
								<div class="col l6 m6 s6">
									<div class="col l12 s12 m12">
										<div class="col l1 s1 m1 loc-add"><i class="material-icons view-icons">location_on</i></div>
										<div class="col l10 s10 m10">
											<span class="view-label">BILLING ADDRESS</span>
											<p class="view-content"><?php echo $address; ?> </p>
										</div>
									</div>
									<div class="col l12 s12 m12" style="margin:20px 0px 0px 55px !important; ">
										<div class="col l4 s4 m4">
											<label class="cmp_info">STATE</label><br>
											<label class="cmp_details"><b> <?php echo $result[0]['state_name']; ?> </b></label>
										</div>
										<div class="col l4 s4 m4">
											<label class="cmp_info">COUNTRY</label><br>
											<label class="cmp_details"><b> <?php echo $result[0]['state_name']; ?> </b></label>
										</div>
									</div>
								</div>
								<?php //echo '<pre>'; print_r($branch); echo '</pre>'; ?>
								<?php

								for($i=0;$i<count($branch);$i++)
								{
									?>
									<div class="col l6 m6 s6">
										<div class="col l12 s12 m12">
											<div class="col l1 s1 m1 loc-add"><i class="material-icons view-icons">location_on</i></div>
											<div class="col l10 s10 m10">
												<span class="view-label">BRANCH ADDRESS</span>
												<p class="view-content"> <?php echo $branch[$i]['address'].', '. $branch[$i]['name'].', '. $branch[$i]['state_name'].', '. $branch[$i]['country_name'] ?> </p>
											</div>
										</div>
										<div class="col l12 s12 m12" style="margin:20px 0px 0px 55px !important; ">
											<div class="col l4 s4 m4">
												<label class="cmp_info">STATE</label><br>
												<label class="cmp_details"><b> <?php echo $branch[$i]['state_name']; ?> </b></label>
											</div>
											<div class="col l4 s4 m4">
												<label class="cmp_info">COUNTRY</label><br>
												<label class="cmp_details"><b> <?php echo $branch[$i]['country_name']; ?> </b></label>
											</div>
										</div>
									</div>
									<?php
								}

								?>
								
							</div>
						</div>
					</div>
				</div>
				<!-- Address Details End --->	
				
				<div class="row">
					<div class="col s12 m12 l12">
						<!-- Service Offer Start --->	<!-- before-red-bg red-bg-right -->
						<div class="col l6 m6 s6 view-box white-box" style="width:40% !important; margin-right:20px !important;">
							<div class="row">
								<div class="col l12 m12 s12">
									<h6 class="white-box-title" style="padding: 11px 34px !important"> Services Offered</h6>
								</div>
							</div>

							<div class="row" style="width:94% !important;">
								<div class="col l12 s12 m12" style="margin-left:32px !important; margin-bottom:20px !important;">
									<?php
									$services = explode("|@|",$result['0']['bus_services_keywords']);
									for($i=0;$i<count($services);$i++)
									{
										if($services[$i])
										{
											?>
											<div class="col l6 s6 m6" style="height:40px !important;">
												<label class="select_loc"> <?php echo $services[$i]; ?> </label>
											</div>
											<?php
										}
									}
									?>
									
								</div>
							</div>
						</div>
						<!-- Service Offer End --->	
						
						<!-- Web Presence Start --->	
						<div class="col l3 m3 s3 view-box white-box" style="width:30% !important; margin-right:20px !important;">
							<div class="row">
								<div class="col l12 m12 s12">
									<h6 class="white-box-title" style="padding: 11px 34px !important">Web Presence</h6>
								</div>
							</div>
							<div class="row">
								<div class="col l12 s12 m12" style="margin-left:32px !important; margin-bottom:20px !important;">
									<label class="cmp_info">WEBSITE URL</label><br>
									<label class="cmp_details"><b> <?php echo $result[0]['bus_website_url']; ?> </b></label>
								</div>
							</div>
							<div class="row">
								<div class="col l12 s12 m12" style="margin-left:32px !important; margin-bottom:20px !important;">
									<label class="cmp_info"> CLIENTS </label><br>
									<label class="cmp_details"><b> <?php  ?> </b></label>
								</div>
							</div>
							<div class="row">
								<div class="col l12 s12 m12" style="margin-left:32px !important;">
									<label class="cmp_info">CASE STUDIES</label><br>
									<label class="cmp_details"><b> <?php echo $result[0]['bus_case_study']; ?></b></label>
								</div>
							</div>
						</div>
						<!-- Web Presence End --->	
						
						<!-- Contact Details Start --->	
						<div class="col l3 m3 s3 view-box white-box" style="width:25% !important;">
							<div class="row">
								<div class="col l12 m12 s12">
									<h6 class="white-box-title" style="padding: 11px 34px !important">Contact Details</h6>
								</div>
							</div>

							<div class="row">
								<div class="col l12 s12 m12" style="margin-left:32px !important; margin-bottom:20px !important;">
									<label class="cmp_info">CONTACT PERSON</label><br>
									<label class="cmp_details"><b> <?php if($contact_person){ echo $contact_person[0]->reg_username; } ?>  </b></label>
								</div>
							</div>
							<div class="row">
								<div class="col l12 s12 m12" style="margin-left:32px !important; margin-bottom:20px !important;">
									<label class="cmp_info"> CONTACT NUMBER </label><br>
									<label class="cmp_details"><b>  <?php if($contact_person){ echo $contact_person[0]->reg_mobile; } ?> </b></label>
								</div>
							</div>
							<div class="row">
								<div class="col l12 s12 m12" style="margin-left:32px !important;">
									<label class="cmp_info">EMAIL ID</label><br>
									<label class="cmp_details"><b>  <?php if($contact_person){ echo $contact_person[0]->reg_email; } ?> </b></label>
								</div>
							</div>
						</div>
						<!-- Contact Details End --->	
					</div>
				</div>
				
				</div>
        </section>
    </div>
    </div>
	
	
	<div id="send_mailto_comp_modal" class="modal modal-md modal-display" style="max-width:600px !important;">
		<div class="modalheader border-bottom" style="padding-bottom:15px; padding-top:25px !important; text-align:left;">
			<h4 style="padding-left:0px; font-size:20px !important;	">Send Email</h4>
			<a class="modal-close close-pop" style="margin-right:-15px !important;"><img src="<?php echo base_url();?>asset/images/popupdelete1.png" alt="delete"></a>
		</div>
		<div class="modalbody">
			<div class="choose_subscription" style="margin-top:15px;">
				<form class="" id="send_emailto_form_form" name="emailto_form" method="post">
					<?php $csrf = array(
					'name' => $this->security->get_csrf_token_name(),
					'hash' => $this->security->get_csrf_hash()
				);
				?>
				<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
					<div class="row border-bottom">
						<div class="col s12 m12 l12 login_form_textbox">
							<div class="col s12 m12 l12" style="margin-top:5px !important; margin-bottom:5px !important;">
								<span class="filed_name"> Send a message to Google, Inc </span>
							</div>
							<div class="col s12 m12 l12 input-group" style="margin-top:5px;">
								<textarea style="height:75px !important;" id="message" name="message" class="form-control" rows="7" id="message" placeholder="ENTER MESSAGE" style="font-size:13px;"required></textarea>
							</div>
							<div class="col s12 m12 l12" style="margin-top:5px !important; margin-bottom:20px !important;">
								<label class="fill_details"> Please enter your name, email id, phone number, so that service provider can reply you </label>
							</div>
						</div>
					</div>
					<input type="hidden" id='bus_id' name="bus_id" value="<?php echo $result[0]['bus_id']; ?>">
					<div class="row" style="margin-bottom:20px !important; margin-top:0px !important;">
						<div class="col l12 s12 m6"></div>
						<div class="col l12 s12 m6 fieldset" style='text-align:right; margin-left:-20px;'>
							<input id="tocomp_email_close" type="button" class="btn btn-default modal-close" data-dismiss="modal" style="font-size:12px; color:black;" value="CANCEL"></input>
							<input id="tocomp_email_proceed" type="submit" class="btn btn-default" data-dismiss="modal" style="font-size:12px; background-color:#7864e9; color:white;" value="SEND"></input>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	
	<script src="<?= base_url(); ?>asset/ckeditor/ckeditor.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {


		$("#send_emailto_form_form").submit(function(e){
				e.preventDefault();
				for ( instance in CKEDITOR.instances ) {
			        CKEDITOR.instances[instance].updateElement();
			    }
			}).validate({

				rules:{
					
					message:{
						required:true,
					},
					
				},

				messages:{
					
					message:{
						required:"Message is required",
					},

				},
				submitHandler:function(form){

						var bus_id = $('#bus_id').val();
						var email = $('#email').val();
						var message = $('#message').val();

						//$('#tocomp_email_proceed').prop('disabled', true);

								$.ajax({
								url:base_url+'market_place/send_email',
								type:"POST",
								data:{
										'csrf_test_name':csrf_hash,
										"bus_id":bus_id,
										"email":email,
										"message":message,
									 },
								success:function(res){

										if(res == 1)
										{
											$("#message").val('');
											$('#send_mailto_comp_modal').modal('close');
											$('#tocomp_email_proceed').prop('disabled', false);
											Materialize.toast('Email has been sent.', 2000,'green rounded');
										}
										else
										{
											$("#message").val('');
											$('#send_mailto_comp_modal').modal('close');
											$('#tocomp_email_proceed').prop('checked', false);
											Materialize.toast('Error. Email was not sent.', 2000,'red rounded');											
										}
									},
					});
				},
			});

			
		CKEDITOR.replace( 'message', { toolbar : 'Basic' });

		var ce_type=$('#ce_type').val();
		$('#Subject').val('');
		CKupdate();
		$.ajax({
				url:base_url+'customise_emails/get_customise_email',
				type:"POST",
				data:{'csrf_test_name':csrf_hash,"ce_type":ce_type},
				success:function(res){
					var data = JSON.parse(res);
					//alert(data);
					//alert(ce_type);
					if(data != false){
						$('#subject').val(data[0].ce_subject);
						//$('#ce_id').val(data[0].ce_id);
						CKEDITOR.instances['message'].setData(data[0].ce_message);
					}
				},
			}); 
		});

		function CKupdate(){
			for ( instance in CKEDITOR.instances ){
				CKEDITOR.instances[instance].updateElement();
				CKEDITOR.instances[instance].setData('');
		   }
		}
	</script>
<?php $this->load->view('template/footer.php');?>