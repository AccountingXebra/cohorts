
<?php $this->load->view('my-community/Cohorts-header');?>
	<style>
		.select-wrapper label.error:not(.active) {
			margin: -30px 0 0 -11px;
		}
		
		/*----------START SEARCH DROPDOWN CSS--------*/
		.select2-container {
		  width: 100% !important;
		}
		
		.select2-container--default .select2-selection--multiple {
			border:1px solid #afb8c9 !important;
			border-radius:5px !important;
		}
		
		input[type="search"]:not(.browser-default) {
		  height: 30px;
		  font-size: 14px;
		  margin: 0;
		  margin-top:8px !important;
		  border-radius: 5px;
		}
		
		.select2-container--default .select2-selection--multiple .select2-selection__rendered {
		  font-size: 13px;
		  line-height: 35px;
		  color: #666;
		  font-weight: 400;
		}
		
		.select2-container--default .select2-selection--multiple .select2-selection__arrow {
		  height: 32px;
		}
		
		.select2-search--dropdown {
		  padding: 0;
		  margin-top:-10px !important;
		}
		
		input[type="search"]:not(.browser-default):focus:not([readonly]) {
		  border-bottom: 1px solid #bbb;
		  box-shadow: none;
		}
		
		.select2-container--default .select2-selection--multiple:focus {
			outline: none;
		}
		
		.select2-container--default .select2-results__option--highlighted[aria-selected] {
			background: #fffaef;
			color: #666;
		}
		
		.select2-container--default .select2-results > .select2-results__options {
		  font-size: 14px;
		  border-radius: 5px;
		  box-shadow: 0px 2px 6px #B0B7CA;
		}
		
		.select2-dropdown {
		  border: none;
		  border-radius: 5px;
		}
		
		.select2-container .select2-selection--multiple {
		  height: 48px;
		}
		
		.select2-container .select2-selection--single {
		  height: 48px;
		}
		
		.select2-results__option[aria-selected] {
		  border-bottom: 1px solid #f2f7f9;
		  padding: 14px 16px;
		  border-top-left-radius: 0;
		}
		
		.select2-container--default .select2-search--dropdown .select2-search__field {
		   /* border: none;*/
			border: 1px solid #d0d0d0;
			padding: 0 0 0 15px !important;
			width: 93.5%;
			max-width: 100%;
			background: #fff;
			border-radius: 4px;
			border-top-left-radius: 4px;
			border-top-right-radius: 4px;
		}
		
		.select2-container--open .select2-dropdown--below {
		  margin-top: 0px;
		/*border-top: 1px solid #aaa;
		  border-radius: 5px !important;*/
		}
		
		ul#select2-currency-results {
		  width: 97%;
		}
		
		/*---Dropdown error message format---*/
		.select-wrapper + label.error{
		 margin: 18px 0 0 -10px;
		} 
		
		select.error + label.error:not(.active){
		 margin: -20px 0 0 -10px; 
		}
		
		select + label.error.active{
		  margin-left: -10px;
		}
		
		#select2-market-country-container{
			margin-top:10px !important;
		}
		
		.select2-container--default .select2-selection--single .select2-selection__arrow {
			top:10px !important;
		}
		
	
		/*body {
			overflow-y:hidden;
		}*/
		
		#search_comp[type=text] {
			width: 70% !important;
			box-sizing: border-box !important;
			border: 1px solid #ccc !important;
			border-radius:4px !important;
			font-size: 16px !important;
			background-color: white !important;
			background-image: url('<?php echo base_url(); ?>asset/css/img/icons/search.png') !important;
			background-position: 260px 10px !important; 
			background-repeat: no-repeat !important;
			padding: 12px 20px 12px 14px !important;
			outline: none !important;
		}
		
		.multi-loc .select-dropdown {
			border:1px solid #afb8c9 !important;
			border-radius:5px !important;
		}
		
		.multi-loc input[type=text]:not(.browser-default) {
			font-size: 13px !important;
			padding-left:10px !important;
			color: #9e9e9e !important;
		}
		
		.multi-loc .dropdown-content.select-dropdown {
			margin-top:47spx!important;
			overflow-y: scroll !important;
		}
		
		.loc_title{
			font-size:14px !important;
		}
		
		.select_loc{
		   padding-left:10px; 
		   padding-right:10px;
		   padding-top:3px;
		   padding-bottom:3px;		   
		   text-align:center; 
		   width:15px !important; 
		   height:15px !important;
		   color:#ffff; 
		   background-color:#B0B7CA !important; 
		   border-radius:15px; 
		   font-size:14px;
	   }
	   
	   .remove_loc{
		   padding-left:5px !important;
		   color: white !important;
	   }
	   
	   .comp_name{
		   font-size:14px;
		   color:#000000;
	   }
	   
	   .page-link{
		   font-size:13px;
	   }
	   
	   .loc_select{
			width:240px !important;
			border:1px solid #e8ebf4;
			height:50px;
			border-radius:5px;
			padding-left:15px;
			margin-bottom:20px;
		}
		
		.nat_select{
			width:240px !important;
			margin-left:23px;
			border:1px solid #e8ebf4;
			height:50px;
			border-radius:5px;
			padding-left:15px;
			margin-bottom:20px;
		}
		
		.rate-star{
			padding:10px 0  10px 10px !important;
			margin-top:-5px;
		}
		
		#five_star + label {
			padding:0 0 0 25px !important;
		}
		
		#four_star + label {
			padding:0 0 0 25px !important;
		}
		
		#three_star + label {
			padding:0 0 0 25px !important;
		}
		
		#two_star + label {
			padding:0 0 0 25px !important;
		}
		
		#one_star + label {
			padding:0 0 0 25px !important;
		}
		
		[type="checkbox"] + label:before, [type="checkbox"]:not(.filled-in) + label:after {
			margin: 4px 0 0 0 !important;		
		}
		
		.rate-btn{
			background-color:#fff !important;
			color:#7965E9 !important;
			border:1px solid #7965E9;
			border-radius:5px;
		}
	</style>
    <!-- START MAIN -->   
    <div id="main">
		<!-- START WRAPPER -->
		<div class="wrapper">
			<?php
			// echo '<pre>';
			// print_r($result);
			// echo '</pre>';
			?>
			<!-- START LEFT SIDEBAR NAV-->
			<?php //$this->load->view('template/sidebar.php');?>
			<!-- END LEFT SIDEBAR NAV-->

			<!-- START CONTENT -->
			<section id="content">
			  <form action="" method="post" id="search-rslt-form">
				<?php $csrf = array(
					'name' => $this->security->get_csrf_token_name(),
					'hash' => $this->security->get_csrf_hash()
				);
				?>
				<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
			  <div class="row">
				<div class="col s12 m12 l3" style="margin-right:-10px !important;">
					<div class="col l12 m12 s12 white-box" style="margin-left:-10px !important; padding-bottom:450px !important;">
						<div class="row border-bottom" style="margin-left:-12px !important;">
							<h6 class="white-box-title" style="padding: 11px 34px !important">Filter by</h6>
						</div>
						<!--div class="row border-bottom">
							<select id="nature" name="nature[]" class="nat_select js-example-basic-multiple form-control" data-live-search="true">
								<option value="">NATURE OF BUSINESS</option>
								<option value="1"> NN </option>
							</select>	
						</div-->
                        <div class="row form-group border-bottom">
							<div class="col s12 m12 l12">
								<div class="col s12 m12 l12">
									<label>NATURE OF BUSINESS</label>
								</div>								
								
								<div class="col s12 m12 l12" style="margin-top:20px !important;">
									<select id="nature_of_business" name="nature_of_business" class="loc_select js-example-basic-multiple form-control"  data-live-search="true">
									
										 
                                  	<option value="">BUSINESS CATEGORY</option>
                                  
                                  <option value="Accounting & Taxation">Accounting & Taxation</option>
                                  <option value="Advertising">Advertising</option>
                                  <option value="Animation Studio">Animation Studio</option>
                                  <option value="Architecture">Architecture</option>
                                  <option value="Arts & Crafts">Arts & Crafts</option>
                                  <option value="Audit & Tax">Audit & Tax</option>
                                  <option value="Brand Consulting">Brand Consulting</option>
                                  <option value="Celebrity Management">Celebrity Management</option>
                                  <option value="Consultant">Consultant</option>
                                  <option value="Content Studio">Content Studio</option>
                                  <option value="Cyber Security">Cyber Security</option>
                                  <option value="Data Analytics">Data Analytics</option>
                                  <option value="Digital Influencer">Digital Influencer</option>
                                  <option value="Digital & Social Media">Digital & Social Media</option>
                                  <option value="Direct Marketing">Direct Marketing </option>
                                  <option value="Entertainment">Entertainment</option>
                                  <option value="Event Planning">Event Planning</option>
                                  <option value="Florist">Florist</option>
                                  <option value="Foreign Exchange">Foreign Exchange</option>
                                  <option value="Financial and Banking">Financial and Banking</option>
                                  <option value="Gaming Studio">Gaming Studio</option>
                                  <option value="DESIGN & UI/UX">DESIGN & UI/UX</option>
                                  <option value="Hardware Servicing">Hardware Servicing</option>
                                  <option value="Industry Bodies">Industry Bodies</option>
                                  <option value="Insurance">Insurance</option>
                                  <option value="Interior Designing ">Interior Designing </option>
                                  <option value="Legal Firm">Legal Firm</option>
                                  <option value="Media Planning & Buying">Media Planning & Buying</option>
                                  <option value="Mobile Services">Mobile Services</option>
                                  <option value="Music">Music</option>
                                  <option value="Non-Profit">Non-Profit</option>
                                  <option value="Outdoor / Hoarding">Outdoor / Hoarding</option>
                                  <option value="Photography">Photography</option>
                                  <option value="Printing">Printing</option>
                                  <option value="Production Studio">Production Studio</option>
                                  <option value="PR / Image Management">PR / Image Management</option>
                                  <option value="Publishing">Publishing</option>
                                  <option value="Real Estate">Real Estate</option>
                                  <option value="Recording Studio">Recording Studio</option>
                                  <option value="Research">Research</option>
                                  <option value="Sales Promotion">Sales Promotion</option>
                                  <option value="Staffing & Recruitment">Staffing & Recruitment</option>
                                  <option value="Stock & Shares">Stock & Shares</option>
                                  <option value="Technology (AI, AR, VR)">Technology (AI, AR, VR)</option>
                                  <option value="Tours & Travel">Tours & Travel</option>
                                  <option value="Training & Coaching">Training & Coaching</option>
                                  <option value="Translation & Voice Over">Translation & Voice Over</option>
                                  <option value="Therapists">Therapists</option>
                                  <option value="Visual Effects / VFX">Visual Effects / VFX</option>
                                  <option value="Web Development">Web Development</option>
                                </select>
										
								</div>
							</div>
						</div>
						
						<div class="row border-bottom" style="margin-left:10px;">
							<div class="col s12 m12 l12">
								<label>RATING</label>
							</div>						
							<div class="col s12 m12 l12 rate-star">
								<div style="float:left; width:30%;">
									<input type="checkbox" id="five_star"  name="five_star"/><label class="label-star" for="five_star">5 Star</label>
								</div>	
								<div style="float:left; width:70%; text-align:right; margin-top:-5px;">
									<span style="font-size:150%; color:#50e3c2;">&starf;</span><span style="font-size:150%; color:#50e3c2;">&starf;</span><span style="font-size:150%;color:#50e3c2;">&starf;</span><span style="font-size:150%;color:#50e3c2;">&starf;</span><span style="font-size:150%;color:#50e3c2;">&starf;</span>
								</div>	
								<div style="float:left; width:30%;">
									<input type="checkbox" id="four_star"  name="four_star"/><label class="label-star" for="four_star">4 Star</label>
								</div>
								<div style="float:left; width:70%; text-align:right;margin-top:-5px;">
									<span style="font-size:150%; color:#50e3c2;">&starf;</span><span style="font-size:150%; color:#50e3c2;">&starf;</span><span style="font-size:150%;color:#50e3c2;">&starf;</span><span style="font-size:150%;color:#50e3c2;">&starf;</span>
								</div>
								<div style="float:left; width:30%;">
									<input type="checkbox" id="three_star"  name="three_star"/><label class="label-star" for="three_star">3 Star</label>
								</div>
								<div style="float:left; width:70%; text-align:right; margin-top:-5px;">
									<span style="font-size:150%; color:#50e3c2;">&starf;</span><span style="font-size:150%; color:#50e3c2;">&starf;</span><span style="font-size:150%;color:#50e3c2;">&starf;</span>
								</div>
								<div style="float:left; width:30%;">
									<input type="checkbox" id="two_star"  name="two_star"/><label class="label-star" for="two_star">2 Star</label>
								</div>
								<div style="float:left; width:70%; text-align:right; margin-top:-5px;">
									<span style="font-size:150%; color:#50e3c2;">&starf;</span><span style="font-size:150%; color:#50e3c2;">&starf;</span>
								</div>
								<div style="float:left; width:30%;">
									<input type="checkbox" id="one_star"  name="one_star"/><label class="label-star" for="one_star">1 Star</label>
								</div>
								<div style="float:left; width:70%; text-align:right; margin-top:-5px;">
									<span style="font-size:150%; color:#50e3c2;">&starf;</span>
								</div>
							</div>
						</div>
						
						<div class="row form-group">
							<div class="col s12 m12 l12">
								<div class="col s12 m12 l12">
									<label>LOCATION</label>
								</div>
								
								<!--<div class="col s12 m12 l12" style="margin-top:20px !important;">
									<select class="mark-country js-example-basic-single" name="market_country" id="market-country">
										<option value="">COUNTRY</option>
										<?php 
										//for($i=0;$i<count($countries);$i++)
										//{
											?> <option value="<?php //echo $countries[$i]->country_id; ?>" <?php //echo ($countries[$i]->country_id == $post_data['market_country'])?'selected':''; ?> ><?php //echo $countries[$i]->country_name; ?></option> <?php
										//}
										?>
                                  </select>
								</div>--> 
								<?php
										//print_r($cities);
										//exit;
										?>
								<div class="col s12 m12 l12" style="margin-top:20px !important;">
									<select id="multi_loc" name="multi_loc" class="loc_select js-example-basic-multiple form-control"  multiple="multiple" data-live-search="true">
										<option value="">SELECT CITY</option>
										<?php 
										for($i=0;$i<count($cities);$i++)
										{
											?> <option value="<?php echo $cities[$i]['city_id']; ?>" 
												<?php echo in_array($cities[$i]['city_id'], $post_data['multi_loc'])?'selected':''; ?>
											 ><?php echo $cities[$i]['name']; ?></option> <?php
										}
										?>
									</select>	
								</div>
							</div>
						</div>
						
					</div>			
				</div>
				<div class="col s12 m12 l9" style="margin-left:-22px !important;">
					<div class="page-header">
						<div class="container">
							<div class="col s6 m6 l6">
							<h2 class="page-title">My Marketplace</h2>
							<ol class="breadcrumbs" style="margin-top:10px !important;">
								<li><a class="page-link" href="<?= base_url(); ?>market_place/search_company">MY Marketplace </a></li>
							</ol>
							</div>
							<div class="col s6 m6 l6" style="text-align:right; margin-left:-12px !important;">
								<input type="text" id="search_comp" name="search" value="<?php if(@$post_data['search']){echo $post_data['search']; } ?>" placeholder="Search by name or nature of business">
							</div>
						</div>
					</div>
					</form>
					<div class="page-content">
						<div class="row">
							<div class="col s12 m12 l12" style="margin-top:20px !important;">	
								<div class="col s12 m12 l12">	
									<div class="col s12 m12 l12">	
										<div class="col s1 m1 l2" style="width:12%;">
											<label class="loc_title">LOCATION: </label>
										</div>
										<div class="col s1 m1 l5 selected-city" >
											<?php
											for($i=0;$i<count($selected_cities);$i++)
											{
												?>
												<label class="select_loc"><?php echo $selected_cities[$i]['name']; ?><!--<a href="#" class="remove_loc">X</a>--></label>
												<?php
											}
											?>
										
										</div>
										<div class="col s1 m1 l5" style="text-align:right;">
											<label>Total Result Found: <span id='count_update'><?php echo count($result); ?></span> </label>
										</div>
									</div>



									<div class="col s12 m12 l12" style="margin-top:20px !important;">	
										<div class="col l12 m12 s12">

											<div id="search_update">	

											<?php for($i=0;$i<count($result);$i++){ ?>

													<div class="col l6 m6 s6 white-box" style="height:180px !important; width:48% !important; margin-right:5px; padding:10px !important; margin-top:10px !important;">
													<div class="row" style="height:55px; margin-bottom:5px !important;">
														<div class="col s6 m6 l8" style="margin-top:5px !important;">
															<label class="comp_name"><b><?php echo $result[$i]['bus_company_name']; ?></b></label><br>
															<label class="nature_buzz"><?php echo $result[$i]['nature_of_bus']; ?></label>
															
														</div>
														<?php

															if($result[$i]['bus_company_logo'] != '')
															{
																$company_logo = '<img src="'.base_url().'public/upload/company_logos/'.$result[$i]['bus_id'].'/'.$result[$i]['bus_company_logo'].'" height="50px" class="logo_style_2" width="50px">';
															}
															else
															{
																$company_logo = '';	
															}

														?>
														<div class="col s6 m6 l4" style="text-align:right">
															<!-- <img width="100" height="50" src="<?php echo base_url(); ?>public/images/Eazy-Invoice-Logo.png" alt="Logo" class="logo_style_2"/> -->
															<?php echo $company_logo; ?>
														</div>	
													</div>
													<div class="row" style="height:25px; margin-bottom:5px !important;">
														<div class="col s6 m6 l8">
															<div class="col l2 s1 m1" style="margin-left:-15px !important;"><i class="material-icons view-icons">location_on</i></div>
															<div class="col l2 s1 m1"><label> <?php echo $result[$i]['name']; ?> </label></div>
														</div>

														<div class="col s6 m6 l4">

														</div>
													</div>
													
													<div class="row" style="margin-bottom:5px !important; margin-top:10%;">
														<div class="col s6 m6 l6" style="font-size:14px;"> 	
															<a href="<?php echo base_url(); ?>market_place/view_company_details?token=<?php echo $result[$i]['bus_id']; ?>" style="color:#7965E9;" onclick=""><b>VIEW PROFILE</b></a>
														</div>
														<div class="col s6 m6 l6" style="text-align:right;">
														<button href="#rating_modal<?=$result[$i]['bus_id']?>" class="modal-trigger rate-btn">Rate me</button>

															  <!-- rating model -->

   <div id="rating_modal<?=$result[$i]['bus_id']?>" class="modal modal-rating" style="max-height:100%;height:75%; width:56%;">
   <img class="geen" src="<?php echo base_url();?>asset/images/green.png" alt="green">
   <form name="rating_frm" id="rating_frm" method="post">
	<?php $csrf = array(
					'name' => $this->security->get_csrf_token_name(),
					'hash' => $this->security->get_csrf_hash()
				);
				?>
				<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
      <div class="modal-content">
         <div class="modal-header">
            <a class="modal-close close-pop"><img src="<?php echo base_url();?>asset/images/popupdelete.png" alt="delete"></a>
         </div>
      </div>
      <div class="modal-content" style="padding: 24px 0 0 0;">
         <div class="row">
            <h4 style="width: unset;margin: 0 0 0 0px;font-size: 20px;font-weight: 600;text-align: center;">How likely are you to recommend Xebra to a friend or colleague?</h4>
         </div>
         <div class="row">
            <div class="col l10 s12 m12 offset-l1">
               <!-- offset-s6 -->
               <div class="rating-rating">
                  <label class="radio-lable">
                     <input name="comp_id" class="rate" type="hidden" value="<?=$result[$i]['bus_id']?>"/>
                  <input name="rate" class="rate" type="radio" value="1"/>
                  <span>1</span>
                  </label>
                  <label class="radio-lable">
                  <input name="rate" class="rate" type="radio" value="2"/>
                  <span>2</span>
                  </label>
                  <label class="radio-lable">
                  <input name="rate" class="rate" type="radio"  value="3"/>
                  <span>3</span>
                  </label>
                  <label class="radio-lable">
                  <input name="rate" class="rate" type="radio"value="4" />
                  <span>4</span>
                  </label>
                  <label class="radio-lable">
                  <input name="rate" class="rate" type="radio"   value="5"/>
                  <span>5</span>
                  </label>
                  <div class="">
                     <i class="Medium material-icons" style="    margin-top: -8px;">thumb_down</i>
                     <i class="Medium material-icons" style=" float:right;margin-top: -8px;">thumb_up</i>
                  </div>
               </div>
               <span id="rating_error"></span>
            </div>
         </div>

          <div class="row">
            <div class="col l11 s12 m12" style="margin-left: 3.333333%;">
               <p class="title-note" style="margin-bottom: 15px;">Please tell us why you rated Xebra <span id="rate_ratio"></span> out of 5?</p>
               <div class="row notess note-mor">
                  <div class="s12 m12 l12">
                     <div class="note">
                        <textarea id="bill1" placeholder="GIVE YOUR FEEDBACK HERE" type="text" name="feedback" class="bill-box ex-noss materialize-textarea ratingtextbox" style="height: 54px;min-height: 6rem;margin-bottom:0;"></textarea>
                        <label for="bill1"></label>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="modal-footer">
         <div class="row" >
            <div class="col l11 s12 m12 cancel-deactiv pull-right" style="margin-left: 35px;">
               <a class=" modal-close btn-flat theme-flat-btn theme-btn theme-btn-large model-cancel " type="button">CANCEL</a>
               <button class="btn-flat theme-primary-btn theme-btn theme-btn-large  modal-trigger dea rating_comp">SUBMIT</button>
            </div>
         </div>
      </div>
   </form>
</div>

    <!--  rating model  --><label> <?php 
                                                               $rating= $this->Market_place_model->selectData('rating', '*', array('bus_id'=> $result[$i]['bus_id']));
                                                               // print_r($rating);
                                                              
                                                               $count=0;
                                                               foreach($rating as $valueRate){
                                                                 $count+=$valueRate->rate;
                                                               }
															   
                                                               if(sizeof($rating)>0){
																	$xyz = round($count/sizeof($rating));
																	if($xyz==5){
																		echo '<span style="font-size:150%; color:#50e3c2;">&starf;</span><span style="font-size:150%; color:#50e3c2;">&starf;</span><span style="font-size:150%;color:#50e3c2;">&starf;</span><span style="font-size:150%;color:#50e3c2;">&starf;</span><span style="font-size:150%;color:#50e3c2;">&starf;</span>';
																	}
																	if($xyz==4){
																		echo '<span style="font-size:150%; color:#50e3c2;">&starf;</span><span style="font-size:150%; color:#50e3c2;">&starf;</span><span style="font-size:150%;color:#50e3c2;">&starf;</span><span style="font-size:150%;color:#50e3c2;">&starf;</span>';
																	}
																	if($xyz==3){
																		echo '<span style="font-size:150%; color:#50e3c2;">&starf;</span><span style="font-size:150%; color:#50e3c2;">&starf;</span><span style="font-size:150%;color:#50e3c2;">&starf;</span>';
																	}
																	if($xyz==2){
																		echo '<span style="font-size:150%; color:#50e3c2;">&starf;</span><span style="font-size:150%; color:#50e3c2;">&starf;</span>';
																	}
																	if($xyz==1){
																		echo '<span style="font-size:150%; color:#50e3c2;">&starf;</span>';
																	}
																	//print $count/sizeof($rating);
															   }else{
																	echo '<span style="font-size:150%; color:#ccc;">&starf;</span><span style="font-size:150%; color:#ccc;">&starf;</span><span style="font-size:150%;color:#ccc;">&starf;</span><span style="font-size:150%;color:#ccc;">&starf;</span><span style="font-size:150%;color:#ccc;">&starf;</span>';
                                                               }
                                                              
															 ?> </label>
										</div>
													</div>
													
												</div>

											
<?php  }?>


										</div>
											



																					
										</div>
									</div>



								</div>
							</div>
						</div>
					</div>
				</div>
			  
			</section>
			<!-- END CONTENT -->
		</div>
		<!-- END WRAPPER -->
	</div>
	<!-- END MAIN -->
	
	<script type="text/javascript">
		$(document).ready(function(){
			if ($('.menu-icon').hasClass('open')) {
				$('.menu-icon').removeClass('open');

				$('.menu-icon').addClass('close');

				$('#left-sidebar-nav').removeClass('nav-lock');

				$('.header-search-wrapper').removeClass('sideNav-lock');

				$('#header').addClass('header-collapsed');
				
				$('#logo_img').show();
				$('#eazy_logo').hide();
				
				$('#left-sidebar-nav').toggleClass('nav-expanded nav-collapsed');

				$('#main').toggleClass('main-full');
			}
		});
	</script>
	
	<script type="text/javascript">
		$('.js-example-basic-multiple').select2();
		$('.js-example-basic-single').select2();
		
		$('#multi_loc').select2({
			placeholder: 'SELECT LOCATION',
			allowClear: true,
			columns: 4,
			search: true,
			selectAll: true
		});
	</script>
	
	<script type="text/javascript">
		$(document).ready(function() {
			$("#multi_loc, #search_comp").change(function(){
				
				var options = $('#multi_loc option:selected');
				
				var selected_text = $.map(options ,function(option) {
					return option.text;
				});
				//$.each(selected_val, function( key, value ) {
				
				$(".selected-city").empty();
				$("#multi_loc :selected").map(function(i, el) {
					var value = $(el).val();
					var text= $(el).text();
					
					$(".selected-city").append("<label class='"+value+" select_loc'>"+ text +"</label>");
				});
				
				var change_loc = $("#multi_loc").text();
				
				var search_again = $("#search_comp").val();
				var country_again = $("#market-country").val();
				
				$.ajax({
                type:"POST",
                url : base_url+'market_place/AjaxDataRefresh',
                data:{ 
					'csrf_test_name':csrf_hash,
					"multi_loc":change_loc,
					"search":search_again,
					//"countryagain": country_again,
				} , 
                success : function(data){
					//alert(data);	
					$("#search_update").html(data);
                },
				});


				$.ajax({
                type:"POST",
                url : base_url+'market_place/AjaxDataRefresh_count',
                data:{ 
					'csrf_test_name':csrf_hash,
					"multi_loc":change_loc,
					"search":search_again,
					//"countryagain": country_again,
				} , 
                success : function(data){
					//alert(data);	
					$("#count_update").html(data);
                },
				});

			});
			
			
		});
		
		function removeCity(cityid){
			alert(cityid);
			$("#multi_loc option[value='"+cityid+"']").prop("selected", false);//deselect but not removed
			//$('#multi_loc').multiSelect('refresh');
			$('.'+ cityid +'').remove();
		}
	</script>

	<?php $this->load->view('template/footer.php');?>	