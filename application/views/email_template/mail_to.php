	<style>
	.modal-header {
		padding: 30px;	
	}
	
	#mail_sub{
		background-color: #66a3ff;
	}
	
	input[type=text]:not(.browser-default){
		height:1rem;
		width:50%;
	}
	</style>
	<div class="modal-header">
		<h4>Mail To</h4>
		<a class="modal-close close-pop"><img src="<?php echo base_url(); ?>asset/images/popupdelete1.png"></a>
	</div>
	<div class="modal-body">
        <form action="" id="mail_frm" name="mail_to" class="" method="post" accept-charset="utf-8" novalidate="true">
            <?php $csrf = array(
					'name' => $this->security->get_csrf_token_name(),
					'hash' => $this->security->get_csrf_hash()
				);
				?>
				<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
			<div class="row" style="margin-top:20px;">
				<div class="col s12 m12 l12">
                    <div class="col s12 m12 l12 input-group border-bottom">
                        <span>Mail To : </span>
						<input class="form-control" name="name" id="to_id" type="text" placeholder="mail to">
                    </div>
                </div>
                <div class="col s12 m12 l12 login_form_textbox">
					<div class="col s12 m12 l12 input-group border-bottom" style="margin-top:10px;">
						<span> CC :</span>
						<input class="form-control" name="cc" id="cc" type="text" placeholder="cc">
					</div>
                </div>
                <div class="col s12 m12 l12 login_form_textbox">
					<div class="col s12 m12 l12 input-group border-bottom" style="margin-top:10px;">
						<span> Subject :</span>
						<input class="form-control" name="subject" id="subject" type="text" placeholder="subject">
					</div>
                </div>
                <div class="col s12 m12 l12 login_form_textbox">
					<div class="col s12 m12 l12 input-group border-bottom" style="margin-top:10px;">
                        <span> Message :</span>
                        <!--<input class="form-control" name="msg" id="msg" type="text" placeholder="message">-->
						<textarea class="form-control" name="message" rows="8" id="comment"></textarea>
					</div>
                </div>
				<div class="col s12 m12 l12">
					<div class="form-group login_btn_1" style="text-align:right; margin-top:15px;">
						<input type="submit" id="mail_sub" class="btn" value="SUBMIT" name="mail_submit" id="mail_to">
					</div>
                </div>
            </div>
        </form>
    </div>