<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>For Subscribe</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700" rel="stylesheet">
    <style type="text/css">
        @media only screen and (max-width: 600px) {
            .templateColumns {
                width: 100% !important;
                background: #f1eff0;
                border: 1px solid #FFF;
            }

            .columnImage {
                height: auto !important;
                max-width: 600px !important;
                width: 100% !important;
            }
        }

        @media only screen and (max-width: 360px) {
            .columnImage {
                height: auto !important;
                max-width: 360px !important;
                width: 100% !important;
            }

            .footer {
                background: #fff !important;
            }

            .footer-left {
                padding: 0px 35px;
            }

            .footer4 {
                padding-right: 20px;
            }
        }
    </style>
</head>

<body style="margin:0;">
<table align="center" border="0" cellpadding="0" cellspacing="0" width="700" class="templateColumns"
       style="border-collapse: collapse;border: 1px solid #fff;background:#fff;">
    <tr>
        <td align="center">
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="700" class="templateColumns">
                <!--<tr>
                    <td><a href="#" target="_blank"><img width="200" height="50" class="columnImage" src="<?php echo base_url(); ?>public/images/Eazy-Invoice-Logo.png" border="0"/></a></td>
                    <td><img class="columnImage" src="<?php echo base_url(); ?>asset/images/email/topslice.jpg" border="0"/></td>
                </tr>-->
				<tr style="background-color:#e6e6ff;">
					<td><a href="#" target="_blank"><img style="margin:10px !important;" width="205" height="55" class="columnImage" src="<?php echo base_url(); ?>public/images/xebra-logo.png" border="0"/></a></td>  
				</tr>
            </table>
        </td>
    </tr>
    <tr>
        <td align="center" style="font-family: 'Roboto', sans-serif;color: #503cb5;"><h3
                style="margin: 40px 0px 20px 0;font-size: 20px;">…And we’re great together again!</h3></td>
    </tr>
    <tr>
        <td align="center" style="font-family: 'Roboto', sans-serif;color: #503cb5;"><img class="columnImage"src="<?php echo base_url(); ?>asset/images/email/12-Paying-for-Trial-Pack.gif" border="0"/></td>
    </tr>
    <tr>
        <td align="center" style="font-family: 'Roboto', sans-serif;color: #000; font-weight: 500">
            <p style="font-size: 15px">Hey <span style="color: #503cb7"> <?php //echo $payment_details['buyer_name']; ?></span>,</p>
            <p style="font-size: 15px">Thank you for choosing Xebra.</p>
            <p style="font-size: 15px">Below are the details of the plan you have selected:</p>
            <p style="font-size: 15px; padding-bottom: 50px;"><?php //echo $plan_details['plan']; ?> Package</p>
        </td>
    </tr>
    <tr align="center">
        <td align="center">
        <table width="500" align="center">
            <tr>
                <td align="center"><p style="font-size:16px; margin-left:120px;"> <b>PLAN SUMMARY</b> </p></td>
            </tr>
            <tr>
                <td><p style="font-size:14px; margin-left:70px;">Payment ID :</p></td>
                <td><p><?php echo $payment_details['payments'][0]['payment_id']; ?> </p></td>
            </tr>
            <tr>
                <td><p style="font-size:14px; margin-left:70px;">Subscription Plan :</p></td>
                <td><p> <?php echo $plan_details['plan']; ?> </p></td>
            </tr>
            <tr>
                <td><p style="font-size:14px; margin-left:70px;">Space :</p></td>
                <td><p> <?php echo @$plan_details['storage']; ?> </p></td>
            </tr>
            <tr>
                <td><p style="font-size:14px; margin-left:70px;">SMS :</p></td>
                <td><p> <?php echo @$plan_details['sms_count']; ?> </p></td>
            </tr>
            <tr>
                <td><p style="font-size:14px; margin-left:70px;">AMOUNT :</p></td>
                <td><p> <?php echo $plan_details['plan_amt']; ?> </p></td>
            </tr>
            <?php if(@$plan_details['cgst']){ ?>
            <tr>
                <td><p style="font-size:14px; margin-left:70px;">CGST :</p></td>
                <td><p> <?php echo @$plan_details['cgst']; ?> </p></td>
            </tr>
            <?php } ?>
            <?php if(@$plan_details['sgst']){ ?>
            <tr>
                <td><p style="font-size:14px; margin-left:70px;">SGST :</p></td>
                <td><p> <?php echo @$plan_details['sgst']; ?> </p></td>
            </tr>
            <?php } ?>
            <?php if(@$plan_details['igst']){ ?>
            <tr>
                <td><p style="font-size:14px; margin-left:70px;">IGST :</p></td>
                <td><p> <?php echo @$plan_details['igst']; ?> </p></td>
            </tr>
            <?php } ?>
            <tr>
                <td><p style="font-size:14px; margin-left:70px;">TOTAL AMOUNT (Including all tax):</p></td>
                <td><p> <?php echo $plan_details['total']; ?> </p></td>
            </tr>
        </table>
        </td>
    </tr>
	<tr><td><img class="columnImage" src="<?php echo base_url(); ?>/asset/images/off.png" border="0"/></td></td>
    </tr>
    <tr>
        <td>
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="700" class="templateColumns"style="border-collapse: collapse;border: 1px solid #413a68;background:#413a68;">
                <tr>
                    <td align="center">
                        <p style="margin: 10px 22px; border-bottom: 1.5px solid #ffffff;"><a href="https://twitter.com/xebradotin" target="_blank"><img style="padding: 0px 0px 10px 0;" class="" src="<?php echo base_url(); ?>/asset/images/tweet.png" border="0"/></a>&nbsp;&nbsp;<a href="https://www.facebook.com/Xebra.in/" target="_blank"><img style="padding: 0px 0px 10px 0;" class="" src="<?php echo base_url(); ?>/asset/images/face.png" border="0"/></a>&nbsp;&nbsp;<a href="https://www.linkedin.com/company/xebrabiztech/" target="_blank"><img style="padding: 0px 0px 10px 0;" class="" src="<?php echo base_url(); ?>/asset/images/linkedin.png" border="0"/></a>&nbsp;&nbsp;<a href="https://www.instagram.com/xebradotin/" target="_blank"><img style="padding: 0px 0px 10px 0;" class="" src="<?php echo base_url(); ?>/asset/images/instagram.png" border="0"/></a>&nbsp;<a href="https://www.youtube.com/channel/UCddSZ6gFMbpIdXeSeucNfTQ?view_as=subscriber" target="_blank"><img style="padding: 0px 0px 10px 0;" class="" src="<?php echo base_url(); ?>/asset/images/youtube.png" width="40" height="30" border="0"/></a></p>
                    </td>
                </tr>
                <!--tr>
                    <td align="center" style="color: #ffffff;font-family: 'Roboto', sans-serif;">
                        <p style="font-size: 12px;">Copyright &copy; Xebra. All Rights Reserved.</p>
                        <p style="font-size: 12px;">Want to change how you receive these emails?<br>
                        You can <a href="#" style="color: #ffffff">update your preferences</a> or <a href="#" style="color: #ffffff">unsubscribe from this list</a></p>
                    </td>
                </tr-->
            </table>
        </td>
    </tr>
	<tr>
        <td align="center" style="font-family: 'Roboto', sans-serif;color: #000; font-weight: 500">
			<p>..............................................................................................</p>
			<p style="font-size:13px !important;">THIS IS AN AUTO-GENERATED EMAIL. DO NOT REPLY ON THIS.</p>
		</td>
	</tr>
</table>
</body>
</html>
