<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Welcome to Xebra Cohorts</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700" rel="stylesheet">
    <style type="text/css">
		@media only screen and (max-width: 600px) {
            .templateColumns {
                width: 100% !important;
                background: #f1eff0;
                border: 1px solid #FFF;
            }

            .columnImage {
                height: auto !important;
                max-width: 600px !important;
                width: 100% !important;
            }
        }

        @media only screen and (max-width: 360px) {
            .columnImage {
                height: auto !important;
                max-width: 360px !important;
                width: 100% !important;
            }

            .footer {
                background: #fff !important;
            }

            .footer-left {
                padding: 0px 35px;
            }

            .footer4 {
                padding-right: 20px;
            }
        }
    </style>
</head>

<body style="margin:0;">
	<table align="center" border="0" cellpadding="0" cellspacing="0" width="700" class="templateColumns"style="border-collapse: collapse;border: 1px solid #fff;background:#fff;">
    <tr>
        <td align="center">
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="700" class="templateColumns">
                <!--<tr>
                    <td><a href="#" target="_blank"><img width="200" height="50" class="columnImage" src="<?php echo base_url(); ?>public/images/Eazy-Invoice-Logo.png" border="0"/></a></td>
                    <td><img class="columnImage" src="<?php echo base_url(); ?>asset/images/email/topslice.jpg" border="0"/></td>
                </tr>-->
				<tr style="background-color:#e6e6ff;">
					<td><a href="#" target="_blank"><img style="margin:10px !important;" width="185" height="75" class="columnImage" src="<?php echo base_url(); ?>public/images/xebracohort-mail.png" border="0"/></a></td>  
				</tr>
            </table>
        </td>
    </tr>
    <tr>
        <td align="center" style="font-family: 'Roboto', sans-serif;color: #503cb5;"><h3 style="margin: 40px 0px 20px 0;font-size: 20px;">Thank you for signing up</h3></td>
    </tr>
    <tr>
        <td align="center" style="font-family: 'Roboto', sans-serif;color: #503cb5;"><img class="columnImage"src="<?php echo base_url(); ?>asset/images/email/help-hand.gif" border="0"/></td>
    </tr>
    <tr><?php ?>
        <td align="center" style="font-family: 'Roboto', sans-serif;color: #000; font-weight: 500">
            <p style="font-size: 15px">Hi <span style="color: #503cb7"> <?php echo $user_name;?></span>,</p>
            <p style="font-size: 20px">Welcome to Xebra!</p>
            <p style="font-size: 15px">As fellow entrepreneurs, we both realise that knowing how our business is faring is half the battle won. However, it can be a frustrating experience collecting numbers from multiple applications and excel sheets, let alone making sense out of them.</p>
			<p style="font-size: 15px">Xebra creates easy to understand graphs and tables that will give you a correct picture of your business, helping you in making critical business decisions.</p>
			<p style="font-size: 15px">And along with that, we have also thrown in modules that will take care of invoicing. expense tracking, asset recording, tax calculating and carrying out your entire accounting! Phew!!</p>
			<p style="font-size: 15px">One request, before I leave. I would like to know more about you and your business so hit reply and let me know.</p>
			<!--<p style="font-size: 15px">We would love to hear about your experiences with Xebra Cohorts and how we can improvise it. Do send in your ideas and suggestions to <a style="color:#7864e9;" href="mailto:cohorts@xebra.in">cohorts(at)xebra.in</p>-->
        </td>
    </tr>
    <tr>
        <td align="center" style="font-family: 'Roboto', sans-serif;color: #503cb5;">
			<img style="width:75px; border-radius:60%;" class="" src="<?php echo base_url(); ?>/asset/images/nimesh9.png" border="0" alt="ceo"/>
			<p style="float:right; margin:2.5% 30% 0 -23.5%; color:#000; font-size:15px;"><label><b><a class="direct" style="color: #000; margin: 0 0 0 -96%;" href="https://www.linkedin.com/in/nimesh9" target="_blank" rel="noopener noreferrer"><strong>Nimesh Shah</strong></a></b></label></p>
			<p style="float:right; margin: 5.5% 17% 0 -40%; color:#000; font-size:15px;"><label style="float:right; color:#000;">CEO of Xebra</label></p>
		</td>
    </tr>
    <tr><td><img class="columnImage" src="<?php echo base_url(); ?>/asset/images/off.png" border="0"/></td></td>
    </tr>
    <tr>
        <td>
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="700" class="templateColumns"style="border-collapse: collapse;border: 1px solid #413a68;background:#413a68;">
                <tr>
                    <td align="center">
                        <p style="margin: 10px 22px; border-bottom: 1.5px solid #ffffff;"><a href="https://twitter.com/xebradotin" target="_blank"><img style="padding: 0px 0px 10px 0;" class="" src="<?php echo base_url(); ?>/asset/images/tweet.png" border="0"/></a>&nbsp;&nbsp;<a href="https://www.facebook.com/Xebra.in/" target="_blank"><img style="padding: 0px 0px 10px 0;" class="" src="<?php echo base_url(); ?>/asset/images/face.png" border="0"/></a>&nbsp;&nbsp;<a href="https://www.linkedin.com/company/xebrabiztech/" target="_blank"><img style="padding: 0px 0px 10px 0;" class="" src="<?php echo base_url(); ?>/asset/images/linkedin.png" border="0"/></a>&nbsp;&nbsp;<a href="https://www.instagram.com/xebradotin/" target="_blank"><img style="padding: 0px 0px 10px 0;" class="" src="<?php echo base_url(); ?>/asset/images/instagram.png" border="0"/></a>&nbsp;<a href="https://www.youtube.com/channel/UCddSZ6gFMbpIdXeSeucNfTQ?view_as=subscriber" target="_blank"><img style="padding: 0px 0px 10px 0;" class="" src="<?php echo base_url(); ?>/asset/images/youtube.png" width="40" height="30" border="0"/></a></p>
                    </td>
                </tr>
                <!--tr>
                    <td align="center" style="color: #ffffff;font-family: 'Roboto', sans-serif;">
                        <p style="font-size: 12px;">Copyright &copy; Xebra. All Rights Reserved.</p>
                        <p style="font-size: 12px;">Want to change how you receive these emails?<br>
                        You can <a href="#" style="color: #ffffff">update your preferences</a> or <a href="#" style="color: #ffffff">unsubscribe from this list</a></p>
                    </td>
                </tr-->
            </table>
        </td>
    </tr>
	<tr>
        <td align="center" style="font-family: 'Roboto', sans-serif;color: #000; font-weight: 500">
			<p>..............................................................................................</p>
			<p style="font-size:13px !important;">THIS IS AN AUTO-GENERATED EMAIL. DO NOT REPLY ON THIS.</p>
		</td>
	</tr>
</table>
</body>
</html>
