<!DOCTYPE>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>OTP</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700" rel="stylesheet">
    <style type="text/css">
        @media only screen and (max-width: 600px) {
            .templateColumns {
                width: 100% !important;
                background: #f1eff0;
                border: 1px solid #FFF;
            }

            .columnImage {
                height: auto !important;
                max-width: 600px !important;
                width: 100% !important;
            }
        }

        @media only screen and (max-width: 360px) {
            .columnImage {
                height: auto !important;
                max-width: 360px !important;
                width: 100% !important;
            }

            .footer {
                background: #fff !important;
            }

            .footer-left {
                padding: 0px 35px;
            }

            .footer4 {
                padding-right: 20px;
            }
        }
		}

		#credential {
			border:1px solid black;
			/*border-collapse: collapse;*/
			width: 40%;
		}
    </style>
</head>

<body style="margin:0;">
<table align="center" border="0" cellpadding="0" cellspacing="0" width="700" class="templateColumns"
       style="border-collapse: collapse;border: 1px solid #fff;background:#fff;">
    <tr>
        <td align="center">
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="700" class="templateColumns">
                <!--<tr>
                    <td><a href="#" target="_blank"><img width="200" height="50" class="columnImage" src="<?php echo base_url(); ?>public/images/Eazy-Invoice-Logo.png" border="0"/></a></td>
                    <td><img class="columnImage" src="<?php echo base_url(); ?>asset/images/email/topslice.jpg" border="0"/></td>
                </tr>-->
				<tr style="background-color:#e6e6ff;">
					<td><a href="#" target="_blank"><img style="margin:10px !important;" width="205" height="55" class="columnImage" src="<?php echo base_url(); ?>public/images/xebra-logo.png" border="0"/></a></td>  
				</tr>
            </table>
        </td>
    </tr>
    <tr>
        <td align="center" style="font-family: 'Roboto', sans-serif;color: #503cb5;"><h3
                style="margin: 40px 0px 20px 0;font-size: 20px;">Your employer has sent you the access key</h3></td>
    </tr>
    <tr>
        <td align="center" style="font-family: 'Roboto', sans-serif;color: #503cb5;"><img class="columnImage" src="<?php echo base_url(); ?>asset/images/email/29-Change-in-Access-Permision.gif" border="0"/></td>
    </tr>
    <tr>
        <td align="center" style="padding-top:25px; font-family: 'Roboto', sans-serif;color: #000; font-weight: 500">
            <p style="font-size: 15px">Employee Portal URL</p>
        </td>
    </tr>
	<tr>
		<td align="center" style="padding-top:15px; font-family: 'Roboto', sans-serif;color: #000; font-weight: 500">
            <p style="font-size: 20px"><a style="text-decoration: none;" href="https://eazyinvoice.com/client">https://eazyinvoice.com/client</a></p>
        </td>
	</tr>
	<tr>
		<td align="center" style="padding-top:15px;"><!--border:1px solid #94b8b8;-->
		<table id="credential" style="border:1px solid #94b8b8; border-radius:10px; width:40%; padding:8px;" class="table">
			<tr>
				<td style="border-bottom:1px solid #94b8b8 !important; border-right:1px solid #94b8b8 !important; line-height:20px; width:90px; padding-bottom:5px;">Username</td>
				<td style="border-bottom:1px solid #94b8b8 !important; text-align:center;"></td>
			</tr>
			<tr>
				<td style="padding-top:5px; border-right:1px solid #94b8b8 !important; line-height:20px;">Password</td>
				<td style="text-align:center;"></td>
			</tr>
		</table>
		</td>
	</tr>
    <tr>
        <td align="center" style="font-family: 'Roboto', sans-serif;color: #000; font-weight: 500">
			<p style="font-size: 15px">&nbsp </p>
            <p style="font-size: 15px">Hey <span style="color: #503cb7">	&lt;Employee Name&gt;</span>,</p>
            <p style="font-size: 15px">Your access permission has been successfully updated on EasyInvoices. </p>
            <p style="font-size: 15px">Here’s to some happy invoicing!</p>

            
        </td>
    </tr>
    <tr>
        <td align="center" style="font-family: 'Roboto', sans-serif;color: #000; font-weight: 500">
            <br>
			<a href="" style="color:#503cb5;">You can make your expense vouchers from here</a>
        </td>
    </tr>
	<tr><td><img class="columnImage" src="<?php echo base_url(); ?>/asset/images/off.png" border="0"/></td></td>
    </tr>
    <tr>
        <td>
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="700" class="templateColumns"style="border-collapse: collapse;border: 1px solid #413a68;background:#413a68;">
                <tr>
                    <td align="center">
                        <p style="margin: 10px 22px; border-bottom: 1.5px solid #ffffff;"><a href="https://twitter.com/xebradotin" target="_blank"><img style="padding: 0px 0px 10px 0;" class="" src="<?php echo base_url(); ?>/asset/images/tweet.png" border="0"/></a>&nbsp;&nbsp;<a href="https://www.facebook.com/Xebra.in/" target="_blank"><img style="padding: 0px 0px 10px 0;" class="" src="<?php echo base_url(); ?>/asset/images/face.png" border="0"/></a>&nbsp;&nbsp;<a href="https://www.linkedin.com/company/xebrabiztech/" target="_blank"><img style="padding: 0px 0px 10px 0;" class="" src="<?php echo base_url(); ?>/asset/images/linkedin.png" border="0"/></a>&nbsp;&nbsp;<a href="https://www.instagram.com/xebradotin/" target="_blank"><img style="padding: 0px 0px 10px 0;" class="" src="<?php echo base_url(); ?>/asset/images/instagram.png" border="0"/></a>&nbsp;<a href="https://www.youtube.com/channel/UCddSZ6gFMbpIdXeSeucNfTQ?view_as=subscriber" target="_blank"><img style="padding: 0px 0px 10px 0;" class="" src="<?php echo base_url(); ?>/asset/images/youtube.png" width="40" height="30" border="0"/></a></p>
                    </td>
                </tr>
                <!--tr>
                    <td align="center" style="color: #ffffff;font-family: 'Roboto', sans-serif;">
                        <p style="font-size: 12px;">Copyright &copy; Xebra. All Rights Reserved.</p>
                        <p style="font-size: 12px;">Want to change how you receive these emails?<br>
                        You can <a href="#" style="color: #ffffff">update your preferences</a> or <a href="#" style="color: #ffffff">unsubscribe from this list</a></p>
                    </td>
                </tr-->
            </table>
        </td>
    </tr>
	<tr>
        <td align="center" style="font-family: 'Roboto', sans-serif;color: #000; font-weight: 500">
			<p>..............................................................................................</p>
			<p style="font-size:13px !important;">THIS IS AN AUTO-GENERATED EMAIL. DO NOT REPLY ON THIS.</p>
		</td>
	</tr>
</table>
</body>
</html>
