<?php $this->load->view('my-community/Cohorts-header');?>
    <style>
		.sticky {
			position: fixed;
			top: 70px;
			min-width: 93%;
			max-width: 100%;
			z-index:999;
			background: white;
			color: black;
		}

		.sticky + .scrollbody {
			padding-top: 102px;
		}
		/*....................*/
			@media all and (-ms-high-contrast: none), (-ms-high-contrast: active) { 
		a{
			color:#000;
		}
		.btn-date-bill {
			line-height: 34px !important;
		}
		.select2-container{
			margin-top:-4px;
		}
			}	
		.select-country, .city-select, .type_sub, .type_pack{
			margin-right:10px !important;
		}

		.sub_bulk{
			min-width:140px !important;
			top:203px !important;
		}

		div#breadcrumbs-wrapper {
			margin-top: 0px !important;
			padding: 15px 0 !important;
		}

		.btn-dropdown-action{
			min-width:163px !important;
		}
		
		.btn-stated {
			max-width:76px;
			background-position: 94px center !important;
		}

		/*....................*/

		.add-new {
			margin: 0px 0 0 0 !important;
		}

		.btn-date{
			max-width:80px;
			font-size:12px !important;
		    margin: 0 0 0 6px !important;
		}
		
		@-moz-document url-prefix() {
			
			.btn-date{
				max-width:80px !important;
				height:33px !important;
				padding-top:6px !important;
				margin-left:0px !important;
			}
		}

		.date-cng[type=text]:not(.browser-default) {
			font-size: 12px !important;
		}

		.days_since_reg{
			width:200px !important;
		}

		.btn-dropdown-select > input.select-dropdown {
			max-width:165px !important;
			font-size:13px !important;
		}

		.action-btn-wapper span.caret {
			margin: 15px 8px 0 0;
		}
		.client_name{
			max-width:155px !important;
		}

		.select-emp{
			max-width:156px !important;
			margin-right:5px !important;
			font-size:13px;
		}

		.btn-search{
			//margin: 0px !important;
			margin-left:38px;
		}

		a.filter-search.btn-search.btn.active {
			margin-right: -40px !important;
		}

		.dataTables_length {
			margin-left: 500px;
		}

		#referal_code_table_length .dropdown-content {
			min-width: 95px;
            top:400px !important;
		}

		#referal_code_table_length{
			border:1px solid #B0B7CA !important;
			height:38px;
			border-radius:4px;
			width:96px;
			margin-top:5px;
			margin-left:50%;
		}

		#referal_code_table_length .select-wrapper input.select-dropdown {
			margin-top:-3px !important;
			margin-left:10px !important;
		}

		.referal_bulk_action:not(:checked) + label:after {
			top:5px !important;
			left:0px !important;
		}

		#referal_table_length .select-wrapper span.caret {
			margin: 17px 7px 0 0;
		}

		.referal_code_action:not(:checked) + label:after {
			top:5px !important;
			left:6px !important;
		}

		.referal_bulk_action label{
			margin-left:0px !important;
		}

		.referal_bulk_action.filled-in:checked + label:after {
			top:5px !important;
		}

		.referal_bulk_action.filled-in:checked + label:before {
			top:5px !important;
		}

		::placeholder{
			color:#000000 !important;
			font-size:12px !important;
		}

		a.addmorelink {
			margin-top:-25px !important;
		}
		
		.dataTables_scrollBody{
			height:auto !important;
		}
		
		.viewes{
			color:#50E3C2 !important;
			font-size:14px !important;
		}
		
		table.dataTable.display tbody tr td:nth-child(2){
			text-align:center !important;
		}
		
		table.dataTable.display tbody tr td:nth-child(3){
			text-align:center !important;
		}
		
		table.dataTable.display tbody tr td:nth-child(4){
			text-align:center !important;
		}
		table.dataTable.display tbody tr td:nth-child(5){
			text-align:center !important;
		}
		table.dataTable.display tbody tr td:nth-child(6){
			text-align:center !important;
		}
		table.dataTable.display tbody tr td:nth-child(7){
			text-align:center !important;
		}
		table.dataTable.display tbody tr td:nth-child(8){
			text-align:center !important;
		}
		@media only screen and (max-width: 600px) {
			table.dataTable.display.table-type1 tbody tr{ float:left; width:180%; }
			table.dataTable.display tbody tr td{ float:left; }
			
			table.dataTable.display.table-type1 thead tr{ width:180%; }
			table.responsive-table thead{ float:none; width:200%; overflow-y:scroll;}
			table.dataTable.display.table-type1 thead th{ float:left; }
		}	
	</style>
	<!-- END HEADER -->

    <!-- START MAIN -->
    <div id="main" style="padding:10px 0 0 0px !important;">
      <!-- START WRAPPER -->
      <div class="wrapper">
        <!-- START CONTENT -->

		<!-- START LEFT SIDEBAR NAV-->

         <?php //$this->load->view('template/sidebar.php');?>

        <!-- END LEFT SIDEBAR NAV-->

        <!-- //////////////////////////////////////////////////////////////////////////// -->

        <!-- START CONTENT -->


        <section id="content" class="bg-cp referal-search" >
			<div id="breadcrumbs-wrapper">
				<div class="container">
					<div class="row">
						<div class="col s12 m12 l6">
							<h5 class="breadcrumbs-title my-ex">My Referrals Program<small class="grey-text">(<span id="rev_count"> <?= $count ?> </span> Total)</small></h5>
							<ol class="breadcrumbs">
								<li><a href="">MY ACCOUNT / MY REFERRAL PROGRAM</a>
							</ol>
						</div>
						<div class="col s12 m12 l6" style="text-align:right;">
							<a class="btn btn-theme btn-large right" href="<?php echo base_url(); ?>refer_earn/edit_your_referal_code">EDIT YOUR REFERRAL DETAILS</a>
						</div>
					</div>
				</div>
			</div>
		<!-- <form id="referal_list_form" name="referal_list_form" method="post" action="" > -->
		<div class="row ref-bot  ml-0" style="padding-left:51px">
              <div class="col l12 s12 m12">
				<div class="col s12 m12 l4  refrance" style="margin-left:-10px !important; padding:2% 0 0% 0;">
				  <p style="margin-top:11px; font-size:20px !important;">Referral Code: <?= $refer_code[0]->refer_code ?></p></label>
				</div>
				<div class="col s12 m12 l4 refrance" style="margin-left:-0px !important; ">
                    <div class="col l12 s12 m12" style="margin:10px 0 0 -8px;">
						<a href="#view_email_copy" class="viewes modal-trigger" title="View Email">View Email</a>
					</div>
					<input type="hidden" id="referal_code" value="<?= $refer_code[0]->refer_id ?>">
				  <div class="col s12 m12 l2  refrance1" style="margin-left:-0px !important; padding: 0 1px 0 0;width: 71.333%;">
				  <input style="border-radius: 5px !important; height: 36px; line-height: 36px;" placeholder="INVITE BY EMAIL" id="email" name="email" type="text" class="bill-box bill1" value="<?php if(isset($_GET['email'])){echo $_GET['email'];}?>">
				  </div>
				  <div class="col s12 m12 l2  refrance2" style="margin-left:-0px !important;margin-left: -0px !important;width: 10%;margin-right: -3px; padding:0;">
				  <button style="min-width:88px; border-radius:5px !important; margin-top: -1px; margin-right: 1px; height: 42px; line-height:42px; border-radius: 0" id="invite_email" class="modal-close btn-flat theme-primary-btn theme-btn theme-btn-large">INVITE</button>
				  </div>
				</div>
				<div class="col s12 m12 l4  refrance" style="margin-left:-0px !important;">
					<div class="col l12 s12 m12" style="margin:10px 0 0 -8px;">
						<a href="#view_sms_copy" class="viewes modal-trigger" title="View Message">View Message</a>
					</div>			
				  <div class="col s12 m12 l2  refrance" style="margin-left:-0px !important; padding: 0 1px 0 0;width: 71.333%;">
				  <input style="border-radius: 5px !important; height: 36px; line-height: 36px;" placeholder="INVITE BY SMS" id="phone_no" name="phone_no" type="text" class="bill-box bill1 numeric_number">
				  </div>
				  <div class="col s12 m12 l2  refrance" style="margin-left:-0px !important;margin-left: -0px !important;width: 10%;margin-right: -3px; padding:0;">
				  <button style="min-width:88px; border-radius:5px !important; margin-top: -1px; margin-right: 1px; height: 42px; line-height:42px; border-radius: 0" id="invite_phone_no" type="submit" class="modal-close btn-flat theme-primary-btn theme-btn theme-btn-large">INVITE</button>
				  </div>
				</div>

			</div>


			</div>
		<!-- </form> -->

          <div id="bulk-action-wrapper">
            <div class="container">
			  <div class="row">
                <div class="col l12 s12 m12">
					<a href="javascript:void(0);" class="addmorelink right" onclick="reset_referalfilter();" title="Reset all">Reset</a>
				</div>
			  </div>
              <div class="row">
                <div class="col l5 s12 m12">
				<div class="col l6 s12 m12">
                    <a class='dropdown-button border-radius-6 btn-dropdown-action btn-bulk btn-default uppercase no-background' href='#' data-activates='dropdown_bulk0t4'>Bulk Actions <i class="arrow-icon"></i></a>
                    <ul id='dropdown_bulk0t4' class='dropdown-content'>
                        <li><a id="download_multiple_refer_list" data-multi_srec="0"><i class="dropdwon-icon icon download"></i>DOWNLOAD</a></li>
                    </ul>
				</div>
				<div class="col l6 s12 m12 searchbtn">
					<a class="filter-search btn-search btn" style="margin-left:-35%;">
						<input type="text" name="search_referal" id="search_referal" class="search-hide-show" style="display:none" />
						<i class="material-icons ser search-btn-field-show">search</i>
					</a>
				</div>

                </div>
				<div class="col l7 s12 m12">
				<div class="action-btn-wapper right">
				   <!--select class='ml-3px border-radius-6 border-split-form btn-dropdown-select select-like-dropdown by-statys days_since_reg' id="search_by_naturec" name="search_by_naturec">
                    <option value="">NATURE OF CODE</option>
                    <option value="">ALL</option>
                    <option value="1">Coupon Code</option>
                    <option value="2">Referal Code</option>
				   </select-->

				   <input type="text" placeholder="START DATE" class="btn-date icon-calendar-green rangedatepicker_list date-cng btn-stated out-line" id="referal_start_date" name="referal_start_date" readonly="readonly">
				   <input type="text" placeholder="END DATE" class="btn-date icon-calendar-red rangedatepicker_list date-cng btn-stated out-line" id="referal_end_date" name="referal_end_date" readonly="readonly">
                </div>
				<div class="row" style="margin-top:50px !important;">
					<div class="col s6 m6 l6"></div>
					<div class="col s6 m6 l6" style="text-align:right;">

					</div>
				</div>
				</div>
               </div>
              </div>
            </div>
          </div>

          <div class="container">
            <div class="row">
              <div class="col l12 s12 m12">
                  <table id="referal_code_table" class="responsive-table display table-type1" cellspacing="0">
                    <thead id="fixedHeader">
                      <tr>
                        <th style="width:10px;">
							<input type="checkbox" class="purple filled-in" name="referal_bulk" id="referal_bulk"/>
							<label style="margin-top:7px;" for="referal_bulk"></label>
						</th>
                        <th style="text-align:center; width: 150px; padding-top:0px !important;">INVITE SENT DATE</th>
                        <th style="text-align:center; width: 150px">PERSON NAME & </br>EMAIL ID / CELL NO</th>
						<th style="text-align:center; width: 150px">SIGNED UP DATE</th>
						<th style="text-align:center; width: 150px">SUBSCRIPTION STATUS</th>
                        <th style="text-align:center; width: 150px">TYPE OF SUBSCRIPTION</th>
						<th style="text-align:center; width: 150px">REFERRAL FEE</br>INR</th>
                        <th style="text-align:center; width: 150px">PAYMENT STATUS</th>
                      </tr>
                    </thead>
                    <tbody class="scrollbody">

                    </tbody>
                  </table>
              </div>

            </div>
          </div>
        </section>
        <!-- END CONTENT -->
        </div>
        <!-- END WRAPPER -->
      </div>

    <div id="send_email_coupon_modal" class="modal modal-md ps-active-y" style="margin-top:-45px !important; max-width:510px !important;">
	</div>
	
	<div id="view_email_copy" class="modal modal-md ps-active-y" style="margin-top:-2%; max-width:515px;">
		<div class="modalheader" >
			<h4>Invite Email Copy</h4>
			<a class="modal-close close-pop"><img src="<?php echo base_url(); ?>asset/images/popupdelete1.png" alt="delete"></a>
		</div>

		<div class="modalbody">
			<form class="viewemailcopy" id="viewemailcopy" name="viewemailcopy">
				<?php $csrf = array(
		'name' => $this->security->get_csrf_token_name(),
		'hash' => $this->security->get_csrf_hash()
	);
	?>
	<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
				<div class="row">
				<div class="col l12 s12 m12">
				<table align="center" border="0" cellpadding="0" cellspacing="0" width="700" class="templateColumns" style="border-collapse: collapse;border: 1px solid #fff;background:#fff;">
				<tr>
					<td align="center" style="font-family: 'Roboto', sans-serif;color: #000; font-weight: 500">
						<p style="font-size: 15px; padding:0 5%;">Hey<span style="color: #503cb7"></span>,</p>
						<p style="font-size: 15px; padding:0 5%;">I am currently using Xebra and find this application</br> particularly useful in getting a deeper and better understanding</br> of my business financials.</p>
						<p style="font-size: 15px; padding:0 5%;">Also, they have merged invoicing, expense monitoring, asset</br> tracking, payroll, banking, tax calculation and accounting into</br> one making it seamless data flow</p>
						<p style="font-size: 15px; padding:0 5%;">
						You can begin by subscribing their <a style="color:#7864e9;" href="https://www.xebra.in/signup">Walk plan</a> which is a </br>free-forever plan. </br>
						<a target="_blank" style="color:#7864e9;" href="https://www.xebra.in/pricing">https://www.xebra.in/pricing</a> 
						<!--Alternatively use my code <span style="color:#7864e9;">'<?php //print $refer_code[0]->refer_code;?>'</span> and get</br>Rs. 5000 on their <a style="color:#7864e9;" href="https://www.xebra.in/xebra/signup">Gallop plan</a> which has many more features-->
						</p>
						<!--You can begin by subscribing their <a href="https://www.xebra.in/xebra/?signup=1">Walk plan</a> which is a free-forever plan or their <a href="https://www.xebra.in/xebra/?signup=1">Canter plan</a> which has many more features</p-->
						<!--p style="font-size: 15px; padding:0 5%;">timely business decisions. It combines invoicing, expense,</p>
						<p style="font-size: 15px; padding:0 5%;">asset tracking, payroll & automated accounting.</p>
						<p style="font-size: 15px; padding:0 5%;">You can <span style="color: #503cb7"><a href="https://www.xebra.in/xebra/?signup=1">Subscribe to Full Throttle Pack</a></span></p>
						<p style="font-size: 15px; padding:0 5%;">with this code '<?php //print $refer_code[0]->refer_code;?>' and avail a discount of Rs. 5,000.</p-->
					</td>
				</tr>
				</table>
				</div> 
				</div>
			</form>
		</div>
		<div class="modal-content">
			<div class="modal-footer">
				<div class="row">
				<div class="col l4 s12 m12"></div>
				<div class="col l12 s12 m12 cancel-deactiv text-center">
					<a style="font-size:15px; color:#fff; background-color:#7864e9;" class=" modal-close btn-flat theme-flat-btn theme-btn theme-btn-large model-cancel " type="button">OK</a>
				</div>
				</div>
			</div>
		</div>
	</div>	
	
	<div id="view_sms_copy" class="modal modal-md ps-active-y">
		<div class="modalheader">
			<h4>Invite SMS Copy</h4>
			<a class="modal-close close-pop"><img src="<?php echo base_url(); ?>asset/images/popupdelete1.png" alt="delete"></a>
		</div>

		<div class="modalbody">
			<form class="viewemailcopy" id="viewemailcopy" name="viewemailcopy">
				<?php $csrf = array(
		'name' => $this->security->get_csrf_token_name(),
		'hash' => $this->security->get_csrf_hash()
	);
	?>
	<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
				<div class="row">
				<div class="col l12 s12 m12">
				<table align="center" border="0" cellpadding="0" cellspacing="0" width="700" class="templateColumns" style="border-collapse: collapse;border: 1px solid #fff;background:#fff;">
				<tr>
					<td align="center" style="font-family: 'Roboto', sans-serif;color: #000; font-weight: 500">
						<p style="font-size: 15px; padding:0 5%;">
						Hi, do try out Xebra as you can then carry out Invoicing,</br> Expenses & Asset tracking, Payroll, Tax Calculation, Banking</br> & Accounting - all in one.</br>
						<a target="_blank" style="color:#7864e9;" href="https://www.xebra.in/pricing">https://www.xebra.in/pricing</a> 
						<!--Hi,
						I am using Xebra app that merges invoicing, expenses, </br>payroll, banking, tax calculation & accounting. </br>Use code <span style="color:#7864e9;">'<?php //print $refer_code[0]->refer_code;?>'</span> to get Rs 5000 off on Gallop plan </br>-->
						</p>
					</td>
					<!-- Hey, I recently read about the Xebra application that helps you </br> understand your business growth and profitability better.</br>It shows you how your company is faring so you can take </br>timely business decisions. It combines invoicing, expense,</br>asset tracking, payroll & automated accounting.</br> You can Subscribe to the Full Throttle pack </br>with this code '<?php //print $refer_code[0]->refer_code;?>' and avail a discount of Rs. 5,000.</br> -->
				</tr>
				</table>
				</div> 
				</div>
			</form>
		</div>
		<div class="modal-content">
			<div class="modal-footer">
				<div class="row">
				<div class="col l4 s12 m12"></div>
				<div class="col l12 s12 m12 cancel-deactiv text-center">
					<a style="font-size:15px; color:#fff; background-color:#7864e9;" class=" modal-close btn-flat theme-flat-btn theme-btn theme-btn-large model-cancel " type="button">OK</a>
				</div>
				</div>
			</div>
		</div>
	</div>	

      <!-- END MAIN -->

      <?php $this->load->view('template/footer'); ?>
