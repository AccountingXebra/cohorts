<?php $this->load->view('my-community/Cohorts-header');?>
    <!-- START MAIN -->
<style type="text/css">

	.disabledbutton {
		pointer-events: none;
		opacity: 0.4;
	}

	.border-split-form .select-wrapper {
		padding: 7px 0 2px 0 !important;
	}

	input.full-bg:not(.first)  {
		border-top: 0 !important;
	}

	/*----------START SEARCH DROPDOWN CSS--------*/
	.select2-container--default .select2-selection--single {
	  border:none;
	}
	input[type="search"]:not(.browser-default) {
	  height: 30px;
	  font-size: 14px;
	  margin: 0;
	  border-radius: 5px;

	}
	.select2-container--default .select2-selection--single .select2-selection__rendered {
	  font-size: 12px;
	  line-height: 35px;
	  color: #666;
	  font-weight: 400;
	}
	.select2-container--default .select2-selection--single .select2-selection__arrow {
	  height: 32px;
	  right: 14px;
	}
	.select2-search--dropdown {
	  padding: 0;
	}
	input[type="search"]:not(.browser-default):focus:not([readonly]) {
	  border-bottom: 1px solid #bbb;
	  box-shadow: none;
	}
	.select2-container--default .select2-selection--single:focus {
		outline: none;
	}
	.select2-container--default .select2-results__option--highlighted[aria-selected] {
		background: #fffaef;
	  color: #666;
	}
	.select2-container--default .select2-results > .select2-results__options {
	  font-size: 14px;
	  border-radius: 5px;
	  box-shadow: 0px 2px 6px #B0B7CA;
	  width: 98%;
	}
	.select2-dropdown {
	  border: none;
	  border-radius: 5px;
	}
	.select2-container .select2-selection--single {
	  height: 53px;
	}
	.select2-results__option[aria-selected] {
	  border-bottom: 1px solid #f2f7f9;
	  padding: 14px 16px;
	}
	.select2-container--default .select2-search--dropdown .select2-search__field {
		border: 1px solid #d0d0d0;
		padding: 0 0 0 15px !important;
		width: 95%;
		max-width: 100%;
		background: #fff;
	}
	.select2-container--open .select2-dropdown--below {
	  margin-top: -15px;
	}
	/*----------END SEARCH DROPDOWN CSS--------*/
	span.hoveraction {
	  top: 10px !important;
	}
	a.sac.info-ref.tooltipped.info-tooltipped{
		 position: absolute;
	}
	/*---Dropdown error message format---*/
	.select-wrapper + label.error{
	 margin: 18px 0 0 -10px;

	}
	select.error + label.error:not(.active){

	 margin: -15px 0 0 -10px;
	}
	select + label.error.active{
	  margin-left: -10px;
	}
	/*---End dropdown error message format---*/
	i.cicon{
	  position: relative;
	  margin: -55px 24px 0 0;
	  float: right;
	}
	.clold{
		  margin: 15px 0 15px 10px !important;
	}
	.uploaddoc {
		margin: 13px 0 27px 20px !important;
		color:#666 !important;
	}

	p.gstin {
		color: #666;
	}
	.info-ref{
		  margin: 0 4px -4px 0;
	}
	.input-field.padd-n label {
		left: 0px;
	}
	.border-split-form .select-wrapper .caret{
		  color: #666;
	}
	.sup-name {
	   width: 227px !important;
	}
	.footer_btn{
		height:35px !important;
		line-height: 35px;
	}

	.nature_of_code{
		border-right: 1px solid #EEF2FE !important;
	}

	.select-wrapper + label.error{
		margin: 18px 0 0 -10px;
	}

	select.error + label.error:not(.active){
		margin: -15px 0 0 -10px;
	}

	select + label.error.active{
		margin-left: -10px;
	}
	.badge-cover{
		margin: -6px 0 0 15px !important;
	}

</style>

  <div id="main" style="padding:10px 0 0 0px !important;">
      <!-- START WRAPPER -->
      <div class="wrapper">
        <!-- START CONTENT -->
<!-- START LEFT SIDEBAR NAV-->

         <?php //$this->load->view('template/sidebar.php');?>

        <!-- END LEFT SIDEBAR NAV-->

        <!-- //////////////////////////////////////////////////////////////////////////// -->

        <!-- START CONTENT -->

  <form class="create-company-form border-split-form" id="create_referal_code" method="post" action="<?php echo base_url(); ?>refer_earn/edit_your_referal_code">

  <section id="content" class="bg-theme-gray documents-search">
	<?php $csrf = array(
		'name' => $this->security->get_csrf_token_name(),
		'hash' => $this->security->get_csrf_hash()
	);
	?>
	<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
    <div class="container">
      <div class="plain-page-header">
        <div class="row">
          <div class="col l6 s12 m6">
            <a class="go-back bg-l underline" href="<?php echo base_url(); ?>refer_earn">Back to Referrals</a>
          </div>
          <div class="col l6 s12 m6">
          </div>
        </div>
         <div class="page-content">

            <div class="row" style="margin:43px;">
              <div class="col s12 m12 l3"></div>
                <div class="col s12 m12 l6">

                    <div class="row">
                      <div class="box-wrapper bg-white shadow border-radius-6">
                        <div class="box-header">
                          <h3 class="box-title">Edit Referral Details</h3>
                        </div>
                        <div class="box-body">
							<div class="row ">
								<div class="col s12 m12 l12">
									<div class="input-field col s12 m12 l6 padd-n" style="border-right:1px solid #eef2fe;">
										<label for="referal_code" class="full-bg-label">REFERRAL CODE</label>
										<input id="referal_code" name="referal_code" class="readonly-bg-grey full-bg adjust-width border-right" type="text"  value="<?= $refer_data[0]->refer_code ?>" readonly="readonly">
									</div>
                                <div class="input-field col s12 m12 l6 padd-n">
                                  <label for="contact_name" class="full-bg-label">CONTACT NAME<span class="required_field"> *</span></label>
                                 <input id="contact_name" name="contact_name" class="full-bg adjust-width first border-right" type="text"  value="<?= $personal_profile[0]->reg_username ?>">
                                </div>
                              </div>
                        </div>


                          <div class="row">
                            <div class="col s12 m12 l12">
                              <div class="input-field col s12 m12 l6 padd-n" style="border-right:1px solid #eef2fe;">
                                  <label for="billing_address" class="full-bg-label">BILLING ADDRESS</label>
                                 <input id="billing_address" name="billing_address" class="full-bg adjust-width " type="text"  value="<?= $refer_data[0]->refer_billing_address ?>">
                              </div>
                              <div class="input-field col s12 m12 l6 padd-n" >
                                <label for="email_id"class="full-bg-label">EMAIL ID<span class="required_field"> *</span></label>
                                <input id="email_id" name="email_id"class="full-bg adjust-width" type="email" value="<?= $refer_data[0]->refer_email ?>">
                              </div>
							  <div class="input-field col s12 m12 l6 padd-n" style="border-top:1px solid #EEF2FE !important; border-right:1px solid #eef2fe;">
                                <label for="cell_no"class="full-bg-label">CELL NO.<span class="required_field"> *</span></label>
                                <input id="cell_no" name="cell_no"class="full-bg adjust-width" type="text" value="<?= $refer_data[0]->refer_cell_no ?>">
                              </div>
							  <div class="input-field col s12 m12 l6 padd-n" style="border-top:1px solid #EEF2FE !important;">
                                <label for="pan_no"class="full-bg-label">PAN NO.<span class="required_field"> *</span></label>
                                <input id="pan_no" name="pan_no" class="full-bg adjust-width" type="text" value="<?= $refer_data[0]->refer_pan_no ?>">
                              </div>
							  <div class="input-field col s12 m12 l12 padd-n" style="border-top:1px solid #EEF2FE !important;">
                                <label for="bank_account_details"class="full-bg-label">BANK ACCOUNT DETAILS <small class="badge-cover "><?php if($refer_data[0]->ac_number!="" ){echo "1";}else{echo "0";}?></small></label>
                                <!--input id="bank_account_details" name="bank_account_details"class="full-bg adjust-width" type="text" value=""-->
								<a href="#editbranch" class="right modal-trigger" style="margin:10px 0;">
                                   <img src="<?php echo base_url(); ?>asset/css/img/icons/pluse.png" class="" alt="plus">
                                </a>
                              </div>
				  <div  class="input-field" id="edit_company_bank_list">
                    <?php //if($bank_details != '') {
                        //$j = 1;
                         //foreach($bank_details as $bank) { ?>
                            <div class="<?php //if($j>1){ echo "bankmore"; } ?> popup-append-box-cover">
                              <div class="gst-boxs add-gst ">
                                <!--div class="col l1 s1 m1 sno">
                                  <div class="row">
                                    <?php //echo $j; ?>
                                  </div>
                                </div-->
                                <div class="col l12 s1 m1">
                                  <div class="col l5 s5 m5 gstnum  <?php //if($bank->status=='Inactive'){?> deactive_record <?php //} ?>" style="border-left: none;">
                                    <span class="gstlabel"> BANK ACCOUNT NUMBER</span><p><span id="bank_acc_number"><?php echo $refer_data[0]->ac_number;?></span></p>
                                  </div>
                                  <div class="col l6 s6 m6 gstplace  <?php //if($bank->status=='Inactive'){?> deactive_record <?php //} ?>">
                                    <span class="gstlabel">BANK NAME</span><p><span id="edit_bank_name"><?php echo $refer_data[0]->bank_name; ?></span></p>
                                  </div>
                                  <div class="col l1 s1 m1 gstaction" style="padding-top: 4.2%;">
                                   <!-- <a class="edited_bank_removed modal-trigger  <?php //if($bank->status=='Inactive'){?> deactive_record <?php //} ?>" data-cbank_id="<?php //echo $bank->cbank_id; ?>" data-bus_id="<?php //echo $row->bus_id; ?>" data-target="removed_existing_bank">
                                      <img src="<?php echo base_url(); ?>asset/images/delete.png" alt="delete" class="delete-icon">
                                    </a>-->
                                    <a href="#editbranch" class="right modal-trigger" data-acc_no="<?php //echo $bank->cbank_account_no; ?>" data-cbank_name="<?php //echo $bank->cbank_name; ?>" data-cbank_branch_name="<?php //echo $bank->cbank_branch_name; ?>" data-cbank_account_type="<?php //echo $bank->cbank_account_type; ?>" data-cbank_ifsc_code="<?php //echo $bank->cbank_ifsc_code; ?>"
                                    data-cbank_swift_code="<?php //echo $bank->cbank_swift_code; ?>" data-cbank_swift_code="<?php //echo $bank->cbank_swift_code; ?>" data-cbank_currency_code="<?php //echo $bank->cbank_currency_code; ?>" data-cbank_currency="<?php //echo $bank->currency; ?>" data-cbank_currencycode="<?php //echo $bank->currencycode; ?>" data-cbank_id="<?php //echo $bank->cbank_id; ?>" data-country_id="<?php //echo $bank->cbank_country_code; ?>" data-country_name="<?php //echo $bank->country_name; ?>" data-bus_id="<?php //echo $row->bus_id; ?>" data-opening_balance="<?php //echo $bank->opening_bank_balance; ?>" data-opening_balance_date="<?php //echo $bank->opening_balance_date; ?>"><i class="material-icons" style="color: #000;">mode_edit</i></a>
                                  </div>
                                </div>
                              </div>
                            </div>
                           <?php //$j++; } } ?>
                            </div>
							<div class="row">
                              <div class="col l12 s12 m12 text-center">
                                <a onclick="showMoredata('bankmore','showbankboxs')" id="showbankboxs" class="cursor"><i class="material-icons">keyboard_arrow_down</i></a>

                                <!-- <a class="showmore-row cursor"><i data-id="company_bank_list" class="material-icons">keyboard_arrow_down</i></a> -->
                              </div>
                            </div>
                            </div>
                          </div>
						</div>
                      </div>
                    </div>
					<div class="row">
                      <div class="col s12 m12 l12 mt-2">
                        <div class="form-botom-divider"></div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col s12 m12 l6 right">
						<button class="btn-flat theme-primary-btn theme-btn footer_btn right">Save</button>
						<button class="btn-flat theme-flat-btn theme-btn footer_btn right mr-5" type="button" onclick="location.href = '<?php echo base_url();?>refer_earn';" style="margin-right:10px !important;">Cancel</button>
                      </div>
                    </div>
                </div>
            </div>
		</div>
	</div>
	</div>
    </div>
	
    </section>
    <!--div class="col s12 m12 l12">
		<div class="footer-pay footer-btns last-sec" style="position:fixed;">
            <div class="form-botom-divider" style="margin: 0 0 10px 0 !important;"></div>
            <div class="row" style="margin-bottom:7px !important;">
              <div class="text-center">
                <!--div class="col s12 m12 l5"></div>
                <div class="col s12 m12 l2">
                    <div class="text-center">
						<a class="view"><i class="material-icons know-more-form">keyboard_arrow_up</i></a>
                    </div>
                </div-->
                <!--div class="col s12 m12 l7">
                  <button class="btn-flat theme-primary-btn theme-btn footer_btn right">Save</button>
                  <button class="btn-flat theme-flat-btn theme-btn footer_btn right mr-5" type="button" onclick="location.href = '<?php //echo base_url();?>refer_earn';" style="margin-right:10px !important;">Cancel</button>
                </div>
              </div>
            </div>
        </div>
    </div-->
    </form>
    <!-- END CONTENT -->
    </div>
	<!-- END WRAPPER -->
    </div>
<div id="editbranch" class="modal modal-md" style="margin-top:-3% !important;">
   <style>
	.branch-add .dropdown-content{
		height:40vh !important;
		overflow-y: scroll !important;
	}
	
	.add-branch-curr .dropdown-content{
		height:30vh !important;
		overflow-y: scroll !important;
	}
   </style>
   <div class="modalheader">
      <h4>Edit Bank Details</h4>
      <a class="modal-close close-pop"><img src="<?php echo base_url();?>asset/images/popupdelete1.png" alt="delete"></a>
   </div>
   <div class="modalbody">
      <form class="addgstin" id="bank_details" name="bank_details" method="post">
		<?php $csrf = array(
		'name' => $this->security->get_csrf_token_name(),
		'hash' => $this->security->get_csrf_hash()
	);
	?>
	<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
         <div class="row gstrow">
            <div class="col l12 s12 m12 fieldset">
               <div class="col l6 s12 m6 fieldset">
                  <div class="input-field">
                     <label for="ac_number" class="full-bg-label">Enter Bank Account Number <span class="required_field">*</span></label>
                     <input name="ac_number" id="ac_number" class="full-bg adjust-width gstin numeric_number" type="text" value="<?= $refer_data[0]->ac_number ?>">
                  </div>
               </div>
               <div class="col l6 s12 m6 fieldset">
                  <div class="input-field">
                     <label class="full-bg-label" for="bank_name">Enter Bank Name <span class="required_field">*</span></label>
                     <input id="bank_name" name="bank_name" class="full-bg adjust-width" type="text"  value="<?= $refer_data[0]->bank_name ?>">
                  </div>
               </div>
            </div>
         </div>
         <div class="row gstrow">
            <div class="col l12 s12 m12 fieldset">
				<div class="col l6 s12 m6 input-set border-bottom">
					<div class="input-field label-active" style="margin-left:5px;">
						<label for="acc_type" class="full-bg-label select-label">Select Account Type</label>
						<select id="acc_type" class="country-dropdown check-label" name="acc_type">
							
							<option value="<?= $refer_data[0]->acc_type ?>" selected><?= $refer_data[0]->acc_type ?></option>
							<option value="Current">Current</option>
							<option value="Savings">Savings</option>
							<option value="Loan">Loan</option>
							<option value="Cash Credit">Cash Credit</option>
						</select>
					</div>
				</div>
				<div class="col l6 s12 m6 fieldset">
				   <div class="input-field">
					  <label for="bank_branch_name" class="full-bg-label">Enter Branch Name</label>
					  <input id="bank_branch_name" name="bank_branch_name" class="full-bg adjust-width border-top-none " type="text" value="<?=$refer_data[0]->bank_branch_name ?>">
				   </div>
				</div>
			</div>
         </div>
         <div class="row gstrow">
            <div class="col l12 s12 m12 fieldset">
			   <div class="col l6 s12 m6 input-set border-bottom">
               <div class="input-field" >
                   <label for="bank_country" class="full-bg-label select-label">COUNTRY</label>
                   <select class="country-dropdown branch-add check-label" name="bus_bank_country" id="bank_country">
                      <option value=""></option>
                     <?php foreach ($countries as $country) { ?>
                         <option value="<?php echo $country->country_id; ?>"<?php if($country->country_id==$refer_data[0]->bus_bank_country){echo "selected";}?>><?php echo $country->country_name; ?></option>
                      <?php } ?>
                   </select>
               </div>
			</div>
            <div class="col l6 s12 m6 fieldset">
               <div class="input-field">
                 <label class="full-bg-label" for="ifsc_code">Enter IFSC Code</label>
                 <input id="ifsc_code" name="ifsc_code" class="full-bg adjust-width" type="text" value="<?= $refer_data[0]->ifsc_code ?>">
               </div>
            </div>
			   </div>
         </div>
         <div class="row gstrow">
            <div class="col l12 s12 m12 fieldset">
				<div class="col l6 s12 m6 input-set border-bottom">
				   <div class="input-field" >
				   
					   <label for="bank_country" class="full-bg-label select-label">Currency</label>
					   <select class="country-dropdown add-branch-curr check-label" name="bank_cur_check" id="bank_cur_check">
						 <option value=""></option>
                     <?php 
                        
                     foreach ($currency as $cur) { ?>
                         <option value="<?php echo $cur->currency_id; ?>"<?php if($cur->currency_id==$refer_data[0]->bank_cur_check){echo "selected";}?>><?php echo $cur->currency." (".$cur->currencycode.")"; ?></option>
                      <?php } ?>
					   </select>
				   </div>
				</div>
            <div class="col l6 s12 m6 fieldset">
               <div class="input-field">
                 <label class="full-bg-label" for="swift_code">Enter Swift Code</label> <input id="swift_code" name="swift_code" class="full-bg adjust-width" type="text" value="<?= $refer_data[0]->swift_code ?>">
               </div>
            </div>
			</div>
		 </div>
		

         <div class="row gstrow">
            <div class="col l12 s12 m12 fieldset">

            </div>
         </div>
         <div class="row">
            <div class="col l6 m6 s12 fieldset">
            </div>
            <div class="col l6 m6 s12 fieldset buttonset">
               <div class="right">
                  <button type="submit" id="referal_bank_edit" class="btn-flat theme-primary-btn theme-btn theme-btn-large right">Save</button>
                  <button type="button" class="modal-close btn-flat theme-flat-btn theme-btn theme-btn-large">CANCEL</button>
               </div>
            </div>
         </div>
      </form>
   </div>
</div>


<!--div id="review" class="modal fade">
    <div class="row" style="margin-top:30px;">
      <div class="col s12 m12 l12">

      <div class="col s12 m12 l10" style="margin-left:43px;">
          <label style="font-size:18px; color:black; text-align:center;"><b>Your total invoiced amount doesn't tally with </b></label><br>
          <label style="font-size:18px; color:black; margin-left:48px;"><b>the total receipt amount entered</b></label>
      </div>

      </div>
    </div>
    <div class="row">
      <div class="col s12 m12 l12">
        <div class="right">
            <button type="submit" class="modal-close btn-flat theme-primary-btn theme-btn theme-btn-large">REVIEW</button>
            <button type="button" id="SubForm"  class="btn-flat theme-flat-btn theme-btn theme-btn-large">PROCEED</button>
        </div>
      </div>
</div>
</div-->

	<script type="text/javascript">
      $(document).ready(function() {
        $('.js-example-basic-single').select2();
        $('.select2-selection__rendered').each(function () {
           $(this).html($(this).html().replace(/(\*)/g, '<span style="color: red;">$1</span>'));
        });

           $('#referal_bank_edit').on('click',function(){

         

           	var ac_number= $('#ac_number').val();
           	 	var bank_name= $('#bank_name').val();
           	 	 	var acc_type= $('#acc_type').val();
           	 	 	 	var bank_branch_name= $('#bank_branch_name').val();
           	 	 	 	 	var bank_country= $('#bank_country').val();
           	 	 	 	 	 	var ifsc_code= $('#ifsc_code').val();
           	 	 	 	 	 	 	var bank_cur_check= $('#bank_cur_check').val();
           	 	 	 	 	 	 	 	var swift_code= $('#swift_code').val();


           	 	 	 	 	 	 	 	$('#bank_acc_number').html(ac_number);

           	 	 	 	 	 	 	 	$('#edit_bank_name').html(bank_name);


           });

	  });
	</script>
      <!-- jQuery Library -->
  <?php $this->load->view('template/footer.php');?>
      <!-- END MAIN -->
