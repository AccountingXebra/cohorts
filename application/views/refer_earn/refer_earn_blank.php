 <!-- //////////////////////////////////////////////////////////////////////////// -->
	<?php $this->load->view('my-community/Cohorts-header');?>
    <!-- START MAIN -->
   
    <div id="main">

      <!-- START WRAPPER -->

      <div class="wrapper">

        <!-- START LEFT SIDEBAR NAV-->

         <?php //$this->load->view('template/sidebar.php');?>

        <!-- END LEFT SIDEBAR NAV-->

        <!-- //////////////////////////////////////////////////////////////////////////// -->

        <!-- START CONTENT -->
        <section id="content">
          <div class="page-header">
            <div class="container">
              <h2 class="page-title">Refer & Earn</h2>
            </div>
          </div>

          <div class="page-content">
            <div class="container">
              <div class="welcom-container">
                <div class="Welcome-image">
                  <img src="<?php echo base_url();?>asset/css/img/icons/blank-stage/ei-blank.gif" class="gifblank" alt="com-blank">
                </div>
                <div class="welcom-content">
                  <p class="welcom-title" style="font-size: 22px; font-weight: 500; margin: 0px 0 0 0;">Hey <?php echo ucfirst($this->session->userdata['user_session']['ei_username']); ?>!</p>
                  <p class="welcom-text">Welcome aboard! How about having all your referals here :)<br/>Let’s get started by adding one below.</p>
                  <p class="welcome-button">
                    <a class="btn btn-welcome" href="<?php echo base_url();?>refer_earn/create">ADD NEW REFERAL</a>
                  </p>
                </div>
              </div>
            </div>
          </div>
        </section>
        <!-- END CONTENT -->

        </div>

        <!-- END WRAPPER -->

      </div>

      <!-- END MAIN -->
<script type="text/javascript">
 // var bus_type= "<?php // echo $select_service; ?>";

</script>
	 <?php $this->load->view('template/footer.php');?>
 