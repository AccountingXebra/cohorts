
ALTER TABLE `refer_earn` ADD `ac_number` VARCHAR(256) NOT NULL AFTER `refer_pan_no`;
ALTER TABLE `refer_earn` ADD `bank_name` VARCHAR(256) NOT NULL AFTER `ac_number`;
ALTER TABLE `refer_earn` ADD `acc_type` VARCHAR(256) NOT NULL AFTER `bank_name`;
ALTER TABLE `refer_earn` ADD `bank_branch_name` VARCHAR(256) NOT NULL AFTER `acc_type`;
ALTER TABLE `refer_earn` ADD `bus_bank_country` INT(20) NOT NULL AFTER `bank_branch_name`;
ALTER TABLE `refer_earn` ADD `ifsc_code` VARCHAR(256) NOT NULL AFTER `bus_bank_country`;
ALTER TABLE `refer_earn` ADD `bank_cur_check` INT(20) NOT NULL AFTER `ifsc_code`;
ALTER TABLE `refer_earn` ADD `swift_code` VARCHAR(256) NOT NULL AFTER `bank_cur_check`;
ALTER TABLE `refer_earn`
  DROP `refer_bank_no`;
  
<!----------------- 03-10-2019 ----------------------->  
ALTER TABLE `asset_type_list` ADD `asset_type` VARCHAR(256) NOT NULL AFTER `atl_id`;
UPDATE `asset_type_list` SET `asset_type` = 'tangible', `asset_type_name` = 'Computers, Tablets & Phones' WHERE `asset_type_list`.`atl_id` = 1;
UPDATE `asset_type_list` SET `asset_type` = 'intangible', `asset_type_name` = 'Software' WHERE `asset_type_list`.`atl_id` = 2;
UPDATE `asset_type_list` SET `asset_type` = 'tangible', `asset_type_name` = 'Furniture & Fixtures' WHERE `asset_type_list`.`atl_id` = 3;
UPDATE `asset_type_list` SET `asset_type` = 'tangible' WHERE `asset_type_list`.`atl_id` = 4;
UPDATE `asset_type_list` SET `asset_type` = 'tangible' WHERE `asset_type_list`.`atl_id` = 5;
UPDATE `asset_type_list` SET `asset_type` = 'tangible', `asset_type_name` = 'Office Equipments' WHERE `asset_type_list`.`atl_id` = 6;
UPDATE `asset_type_list` SET `asset_type` = 'tangible' WHERE `asset_type_list`.`atl_id` = 7;
UPDATE `asset_type_list` SET `asset_type` = 'tangible', `asset_type_name` = 'Inventory' WHERE `asset_type_list`.`atl_id` = 8;
INSERT INTO `asset_type_list` (`atl_id`, `asset_type`, `asset_type_name`, `sac_no`, `notes`) VALUES ('9', 'intangible', 'Goodwill', '', '');
INSERT INTO `asset_type_list` (`atl_id`, `asset_type`, `asset_type_name`, `sac_no`, `notes`) VALUES ('10', 'intangible', 'Patents', '', '');
INSERT INTO `asset_type_list` (`atl_id`, `asset_type`, `asset_type_name`, `sac_no`, `notes`) VALUES ('11', 'intangible', 'Brand Name', '', '');
INSERT INTO `asset_type_list` (`atl_id`, `asset_type`, `asset_type_name`, `sac_no`, `notes`) VALUES ('12', 'intangible', 'Trademarks & Copyrights', '', '');
INSERT INTO `asset_type_list` (`atl_id`, `asset_type`, `asset_type_name`, `sac_no`, `notes`) VALUES ('13', 'intangible', 'Financial Securities', '', '');
INSERT INTO `asset_type_list` (`atl_id`, `asset_type`, `asset_type_name`, `sac_no`, `notes`) VALUES ('14', 'intangible', 'Permits', '', '');

<!-- 16-10-2019 -->
ALTER TABLE `employee_master` ADD `email_of_confirmation` VARCHAR(256) NOT NULL AFTER `confirmation_reminder`;
ALTER TABLE `employee_master` ADD `email_of_appraisal` VARCHAR(256) NOT NULL AFTER `approval_reminder`;
