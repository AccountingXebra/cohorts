

ALTER TABLE `sales_invoices` ADD `inv_export_currency` INT(11) NOT NULL AFTER `inv_place_of_supply`;
ALTER TABLE `sales_invoices` ADD `inv_dollar_number` INT(11) NOT NULL AFTER `inv_export_currency`;
ALTER TABLE `sales_invoices` ADD `inv_inr_value` INT(11) NOT NULL AFTER `inv_dollar_number`;

UPDATE `countries` SET `sortname` = 'USA' WHERE `countries`.`country_id` = 231;

UPDATE `countries` SET `country_name` = 'United States Of America' WHERE `countries`.`country_id` = 231;












/* Avinash Subscription Table */ 


SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `invoice`
--

-- --------------------------------------------------------

--
-- Table structure for table `subscription`
--

CREATE TABLE `subscription` (
  `subscription_id` int(11) NOT NULL,
  `reg_id` int(11) NOT NULL,
  `bus_id` int(11) NOT NULL,
  `invoice_no` varchar(100) NOT NULL,
  `name` varchar(256) DEFAULT NULL,
  `email` varchar(256) DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `smscount` int(11) NOT NULL,
  `storage` varchar(256) NOT NULL,
  `validity` int(11) NOT NULL,
  `payment_mode` varchar(100) NOT NULL,
  `payment_status` varchar(100) NOT NULL,
  `payment_id` varchar(256) NOT NULL,
  `createdat` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedat` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subscription`
--

INSERT INTO `subscription` (`subscription_id`, `reg_id`, `bus_id`, `invoice_no`, `name`, `email`, `amount`, `smscount`, `storage`, `validity`, `payment_mode`, `payment_status`, `payment_id`, `createdat`, `updatedat`) VALUES
(1, 72, 143, 'INV001', 'Avinash chavan', 'avinash@windchimes.co.in', 1000, 500, '1', 365, 'CARD', 'Completed', 'MOJO8c18505A79025753', '2018-12-17 18:30:00', '2018-12-21 13:32:56'),
(2, 72, 144, 'INV002', 'Avinash chavan', 'avinash@windchimes.co.in', 1000, 500, '1', 365, 'CARD', 'Completed', 'MOJO8c18505A79025762', '2018-12-25 18:30:00', '2018-12-26 13:32:56');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `subscription`
--
ALTER TABLE `subscription`
  ADD PRIMARY KEY (`subscription_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `subscription`
--
ALTER TABLE `subscription`
  MODIFY `subscription_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

/* Subscription table End */




/* My Alert Start */

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";



CREATE TABLE `my_alerts` (
  `id` int(11) NOT NULL,
  `bus_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `alert_name` varchar(250) DEFAULT NULL,
  `alert_on` varchar(100) DEFAULT NULL,
  `alert_condition` varchar(200) DEFAULT NULL,
  `alert_number` varchar(100) NOT NULL,
  `alert_date` date NOT NULL,
  `alert_target` varchar(100) DEFAULT NULL,
  `alert_interval` varchar(200) DEFAULT NULL,
  `alert_reminder` date NOT NULL DEFAULT '0000-00-00',
  `alert_notification` int(11) NOT NULL,
  `alert_mobile` varchar(250) DEFAULT NULL,
  `alert_email` varchar(250) DEFAULT NULL,
  `alert_msg` int(11) NOT NULL,
  `alert_mail` int(11) NOT NULL,
  `alert_type` varchar(50) NOT NULL,
  `alert_stakeholder` varchar(1000) NOT NULL,
  `alert_occasion` varchar(1000) NOT NULL,
  `status` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  `createdat` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedat` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
ALTER TABLE `my_alerts`
  ADD PRIMARY KEY (`id`);


--
ALTER TABLE `my_alerts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=87;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;


/* My Alert End */










