

ALTER TABLE `sales_invoices` ADD `inv_export_currency` INT(11) NOT NULL AFTER `inv_place_of_supply`;
ALTER TABLE `sales_invoices` ADD `inv_dollar_number` INT(11) NOT NULL AFTER `inv_export_currency`;
ALTER TABLE `sales_invoices` ADD `inv_inr_value` INT(11) NOT NULL AFTER `inv_dollar_number`;

UPDATE `countries` SET `sortname` = 'USA' WHERE `countries`.`country_id` = 231;

UPDATE `countries` SET `country_name` = 'United States Of America' WHERE `countries`.`country_id` = 231;

INSERT INTO `access_permissions` (`id`, `access`, `created_date`, `status`) VALUES (NULL, 'HR', '2018-12-27', '0'), (NULL, 'Employee', '2018-12-27', '0');





Table structure for table `customise_invoices`
--

CREATE TABLE `customise_invoices` (
  `cust_inv_id` int(11) NOT NULL,
  `inv_id` int(11) NOT NULL,
  `bus_id` int(11) NOT NULL,
  `reg_id` int(11) NOT NULL,
  `cust_id` int(11) NOT NULL,
  `inv_invoice_no` varchar(256) NOT NULL,
  `paper_size` varchar(256) NOT NULL,
  `inv_orientation` varchar(256) NOT NULL,
  `inv_margins` varchar(256) NOT NULL,
  `tmp_back_color` varchar(100) NOT NULL,
  `inv_layout` varchar(100) NOT NULL,
  `set_for_all_tmp` int(6) NOT NULL,
  `hdr_logo` int(5) NOT NULL,
  `font_color` varchar(100) NOT NULL,
  `font_size` varchar(100) NOT NULL,
  `shipping_address` int(5) NOT NULL,
  `late_fees` int(5) NOT NULL,
  `bank_details` int(5) NOT NULL,
  `pan_no` int(5) NOT NULL,
  `eq_levy` int(5) NOT NULL,
  `payment_btn` int(5) NOT NULL,
  `cin_no` int(5) NOT NULL,
  `purchase_order_date` int(5) NOT NULL,
  `purchase_order` int(5) NOT NULL,
  `terms_condition` int(5) NOT NULL,
  `inv_signature` int(5) NOT NULL,
  `signature_logo` varchar(256) NOT NULL,
  `client_name` varchar(256) NOT NULL,
  `client_designation` varchar(256) NOT NULL,
  `set_for_all_cont` int(5) NOT NULL,
  `eoe_ftr` int(5) NOT NULL,
  `ftr_font_color` varchar(256) NOT NULL,
  `ftr_font_size` varchar(256) NOT NULL,
  `ftr_back_color` varchar(256) NOT NULL,
  `company_address` int(5) NOT NULL,
  `company_email` int(5) NOT NULL,
  `company_contact` int(5) NOT NULL,
  `company_website` int(5) NOT NULL,
  `fb_link` int(5) NOT NULL,
  `twitter_link` int(5) NOT NULL,
  `liked_link` int(5) NOT NULL,
  `set_for_all_ftr` mediumint(5) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `customise_invoices`
--
ALTER TABLE `customise_invoices`
  ADD PRIMARY KEY (`cust_inv_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `customise_invoices`
--
ALTER TABLE `customise_invoices`
  MODIFY `cust_inv_id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;





ALTER TABLE `customise_invoices` CHANGE `liked_link` `linked_link` INT(5) NOT NULL;

ALTER TABLE `customise_invoices` ADD `gst` INT(5) NOT NULL AFTER `eq_levy`;

ALTER TABLE `sales_credit_debit` ADD `cdn_export_currency` INT(11) NOT NULL AFTER `cd_place`

ALTER TABLE `sales_credit_debit` ADD `cdn_dollar_number` INT(11) NOT NULL AFTER `cdn_export_currency`


ALTER TABLE `sales_credit_debit` ADD `cdn_inr_value` INT(11) NOT NULL AFTER `cdn_dollar_number`

ALTER TABLE `sales_credit_debit` ADD `cdn_export_type` INT(11) NOT NULL AFTER `cdn_export_currency`;

ALTER TABLE `sales_invoices` ADD `inv_export_type` INT(11) NOT NULL AFTER `inv_recurring_frequency`;
ALTER TABLE `sales_credit_debit` ADD `cd_terms` TEXT NOT NULL AFTER `cd_notes`;
///////////////
20-1-2019
////////
ALTER TABLE `registration` ADD `reg_mob_otp` INT(11) NOT NULL AFTER `reg_otp`;


ALTER TABLE `sales_invoices` ADD `inv_other_total` DECIMAL(10,2) NOT NULL AFTER `inv_cess_total`;

ALTER TABLE `sales_invoices_list` ADD `invl_other` INT(11) NOT NULL AFTER `invl_cess_amt`, ADD `invl_other_amt` DECIMAL(10,2) NOT NULL AFTER `invl_other`;
ALTER TABLE `sales_estimate_list` ADD `estl_other` INT(11) NOT NULL AFTER `estl_cess_amt`, ADD `estl_other_amt` DECIMAL(10,2) NOT NULL AFTER `estl_other`;

ALTER TABLE `sales_proforma_invoices_list` ADD `prol_other` INT(11) NOT NULL AFTER `prol_cess_amt`, ADD `prol_other_amt` DECIMAL(10,2) NOT NULL AFTER `prol_other`
ALTER TABLE `sales_credit_debit` ADD `cd_other_total` DECIMAL(10,2) NOT NULL AFTER `cd_cess_total`;
ALTER TABLE `sales_credit_debit_list` ADD `cdl_other` INT(11) NOT NULL AFTER `cdl_cess_amt`, ADD `cdl_other_amt` DECIMAL(10,2) NOT NULL AFTER `cdl_other`

////////////////////////////////////25-1-2019/////////////


CREATE TABLE `activity_history` (
  `activity_id` int(11) NOT NULL,
  `login_id` int(11) NOT NULL,
  `bus_id` int(11) NOT NULL,
  `cust_id` int(11) NOT NULL,
  `activity` int(11) NOT NULL,
  `createdat` int(11) NOT NULL,
  `updatedat` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activity_history`
--
ALTER TABLE `activity_history`
  ADD PRIMARY KEY (`activity_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activity_history`
--
ALTER TABLE `activity_history`
  MODIFY `activity_id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `activity_history` CHANGE `cust_id` `task_id` INT(11) NOT NULL;
ALTER TABLE `activity_history` CHANGE `createdat` `createdat` DATETIME NOT NULL;
ALTER TABLE `activity_history` CHANGE `updatedat` `updatedat` DATETIME NOT NULL;




/////////////////31-1-2019//////////////////////
CREATE TABLE `eazyinvoice`.`eq_levy_taxes` ( `eq_levy_id` INT(11) NOT NULL AUTO_INCREMENT , `sac_no` VARCHAR(256) NOT NULL , `gst` INT(11) NOT NULL , `cess` INT(11) NOT NULL , `other` INT(11) NOT NULL , `eq_levy_per` INT(11) NOT NULL , `eq_levy_amt` DECIMAL(10,2) NOT NULL , `createdat` DATETIME NOT NULL , `updatedat` DATETIME NOT NULL , PRIMARY KEY (`eq_levy_id`)) ENGINE = InnoDB;

ALTER TABLE `eq_levy_taxes` ADD `set_default` INT(11) NOT NULL AFTER `eq_levy_amt`;

ALTER TABLE `sales_invoices` ADD `inv_terms` TEXT NOT NULL AFTER `inv_notes`;
ALTER TABLE `sales_credit_debit` ADD `due_date` DATE NOT NULL AFTER `cd_notes`;



////////////////////6-2-2019///////////////

ALTER TABLE `eq_levy_taxes` ADD `service_type` INT(11) NOT NULL AFTER `set_default`;

//////////////////////////////////////////////////////////////////////////////////////
ALTER TABLE `other_document` ADD `other_reminder` VARCHAR(56) NOT NULL AFTER `other_type`, ADD `other_start_date` DATE NOT NULL AFTER `other_reminder`, ADD `other_end_date` DATE NOT NULL AFTER `other_start_date`;
ALTER TABLE `sales_invoices` ADD `set_default_terms` INT(11) NOT NULL AFTER `inv_terms`;



//////////////////////18-2-2019//////////////////////

ALTER TABLE `gst_number` ADD `state_code` INT(11) NOT NULL AFTER `place`, ADD `address` TEXT NOT NULL AFTER `state_code`, ADD `city` VARCHAR(256) NOT NULL AFTER `address`, ADD `zipcode` VARCHAR(256) NOT NULL AFTER `city`, ADD `email` VARCHAR(256) NOT NULL AFTER `zipcode`, ADD `contact` VARCHAR(256) NOT NULL AFTER `email`;

////////////////////26-02-2019///////////////////
ALTER TABLE `sales_receipts` ADD `sr_dollar_number` INT(11) NOT NULL AFTER `srec_forex_loss`, ADD `sr_inr_value` INT(11) NOT NULL AFTER `sr_dollar_number`;
ALTER TABLE `sales_receipts_list` ADD `srecl_conv_amt` DECIMAL(10,2) NOT NULL AFTER `srecl_inv_amt`;

ALTER TABLE `sales_expense_voucher` ADD `emp_code` VARCHAR(256) NOT NULL AFTER `cust_id`, ADD `emp_name` VARCHAR(256) NOT NULL AFTER `emp_code`, ADD `department` VARCHAR(256) NOT NULL AFTER `emp_name`, ADD `approver_name` VARCHAR(256) NOT NULL AFTER `department`;
ALTER TABLE `sales_expense_voucher` ADD `exp_voi_date` DATE NOT NULL AFTER `ev_exp_vchr_no`;
ALTER TABLE `sales_expense_voucher` ADD `total_expense` DECIMAL(10,2) NOT NULL AFTER `exp_voi_date`, ADD `total_markup` DECIMAL(10,2) NOT NULL AFTER `total_expense`, ADD `total_amount` DECIMAL(10,2) NOT NULL AFTER `total_markup`;
/////////////////27-02-2019////////////
ALTER TABLE `company_bank` CHANGE `cbank_ifsc_code` `cbank_ifsc_code` VARCHAR(256) NULL DEFAULT NULL;
ALTER TABLE `company_bank` CHANGE `cbank_swift_code` `cbank_swift_code` VARCHAR(256) NULL DEFAULT NULL;
ALTER TABLE `gst_number` ADD `country` INT(11) NOT NULL AFTER `place`;
ALTER TABLE `gst_number` CHANGE `city` `city` INT(11) NOT NULL;

/////////////////////////5-3-2019///////////

ALTER TABLE `sales_expense_voucher` ADD `used_status` INT(11) NOT NULL AFTER `status`;

/////////////////////26-3-2019//////////////////

ALTER TABLE `sales_customers` ADD `cust_last_revenue` DECIMAL(10,2) NOT NULL AFTER `cust_revenue_target`;

//////////////////////1-4-2019/////////////////
ALTER TABLE `sales_invoices` CHANGE `inv_inr_value` `inv_inr_value` DECIMAL(10,2) NOT NULL;
ALTER TABLE `sales_receipts` CHANGE `sr_inr_value` `sr_inr_value` DECIMAL(10,2) NOT NULL;
ALTER TABLE `sales_credit_debit` CHANGE `cdn_inr_value` `cdn_inr_value` DECIMAL(10,2) NOT NULL;

///////////////////11-4-2019/////////////////////////////////
ALTER TABLE `srf_document` ADD `bus_id` INT(11) NOT NULL AFTER `srf_id`, ADD `cust_id` INT(11) NOT NULL AFTER `bus_id`;
ALTER TABLE `srf_document` ADD `srf_reminder` VARCHAR(256) NOT NULL AFTER `srf_type`, ADD `srf_start_date` DATE NOT NULL AFTER `srf_reminder`, ADD `srf_end_date` DATE NOT NULL AFTER `srf_start_date`;
///////////////////////////////////////////////////////////

//////////////////////15-4-2019///////////////////////////////////////
ALTER TABLE `registration` ADD `is_login` INT(11) NOT NULL DEFAULT '0' AFTER `status`;
ALTER TABLE `customise_invoices` ADD `set_invoice_no` INT(11) NOT NULL AFTER `inv_invoice_no`, ADD `inv_no_pre` VARCHAR(256) NOT NULL AFTER `set_invoice_no`;
ALTER TABLE `sales_invoices` ADD `inv_invoice_no_view` VARCHAR(256) NOT NULL AFTER `inv_invoice_no`;

/////////////////////////////////////////////////////////////////////////////////

CREATE TABLE `equilization_levy_payment` (
  `payment_id` int(11) NOT NULL,
  `payment_date` date NOT NULL,
  `payment_for` int(11) NOT NULL,
  `equil_head` varchar(256) NOT NULL,
  `equil_amount` decimal(10,2) NOT NULL,
  `pamt_date` date NOT NULL,
  `bank_id` int(11) NOT NULL,
  `payment_mode` varchar(256) NOT NULL,
  `payment_status` varchar(256) NOT NULL,
  `paymt_amt` decimal(10,2) NOT NULL,
  `intrest_charges` decimal(10,2) NOT NULL,
  `penalty_charges` decimal(10,2) NOT NULL,
  `other_charges` decimal(10,2) NOT NULL,
  `note` text NOT NULL,
  `createdat` datetime NOT NULL,
  `updatedat` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `equilization_levy_payment`
--
ALTER TABLE `equilization_levy_payment`
  ADD PRIMARY KEY (`payment_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `equilization_levy_payment`
--
ALTER TABLE `equilization_levy_payment`
  MODIFY `payment_id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `coupon_promo` ADD `code_date` DATE NOT NULL AFTER `promo_code`, ADD `nature_of_code` VARCHAR(256) NOT NULL AFTER `code_date`, ADD `referrel` VARCHAR(256) NOT NULL AFTER `nature_of_code`, ADD `description` VARCHAR(256) NOT NULL AFTER `referrel`, ADD `target_group` VARCHAR(256) NOT NULL AFTER `description`, ADD `discount` INT(11) NOT NULL AFTER `target_group`;
ALTER TABLE `coupon_promo` CHANGE `promo_code` `coupon_code` VARCHAR(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;

ALTER TABLE `registration` CHANGE `reg_istrial` `reg_istrial` INT(11) NOT NULL COMMENT ' 0-subscriber, 1-in trial';
/////////////////////////////////////////////////////////////////////////////////



CREATE TABLE `equilization_levy_payment_list` (
  `pamtl_id` int(11) NOT NULL,
  `payment_id` int(11) NOT NULL,
  `payment_date` date NOT NULL,
  `payment_for` int(11) NOT NULL,
  `equil_head` varchar(256) NOT NULL,
  `equil_amount` decimal(10,2) NOT NULL,
  `challan_no` varchar(256) NOT NULL,
  `challan_date` date NOT NULL,
  `challan_pic` varchar(256) NOT NULL,
  `createdat` datetime NOT NULL,
  `updatedat` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `equilization_levy_payment_list`
--
ALTER TABLE `equilization_levy_payment_list`
  ADD PRIMARY KEY (`pamtl_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `equilization_levy_payment_list`
--
ALTER TABLE `equilization_levy_payment_list`
  MODIFY `pamtl_id` int(11) NOT NULL AUTO_INCREMENT;

  ALTER TABLE `equilization_levy_payment` ADD `bus_id` INT(11) NOT NULL AFTER `payment_id`, ADD `gst_id` INT(11) NOT NULL AFTER `bus_id`;
  ALTER TABLE `equilization_levy_payment` ADD `equi_payment_code` VARCHAR(256) NOT NULL AFTER `gst_id`, ADD `inv_id` INT(11) NOT NULL AFTER `equi_payment_code`, ADD `inv_date` DATE NOT NULL AFTER `inv_id`, ADD `exp_id` INT(11) NOT NULL AFTER `inv_date`, ADD `exp_date` DATE NOT NULL AFTER `exp_id`;
  ALTER TABLE `equilization_levy_payment_list` ADD `inv_id` INT(11) NOT NULL AFTER `payment_for`, ADD `inv_date` DATE NOT NULL AFTER `inv_id`, ADD `exp_id` INT(11) NOT NULL AFTER `inv_date`, ADD `exp_date` DATE NOT NULL AFTER `exp_id`;

  ///////////////////////////24-4-2019///////////////////
  

CREATE TABLE `sms_tracker` (
  `activity_id` int(11) NOT NULL,
  `login_id` int(11) NOT NULL,
  `reg_id` int(11) NOT NULL,
  `bus_id` int(11) NOT NULL,
  `module_name` varchar(256) NOT NULL,
  `action_taken` varchar(256) NOT NULL,
  `reference` varchar(512) NOT NULL,
  `createdat` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedat` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `sms_tracker`
--
ALTER TABLE `sms_tracker`
  ADD PRIMARY KEY (`activity_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `sms_tracker`
--
ALTER TABLE `sms_tracker`
  MODIFY `activity_id` int(11) NOT NULL AUTO_INCREMENT;


  //////////////////////////////////////////////////
  ALTER TABLE `my_alerts` ADD `parent_name` VARCHAR(256) NOT NULL AFTER `parent_id`;
///////////////////////////////////////////////////////////////////////


  
CREATE TABLE `tbl_comment` ( `comment_id` int(11) NOT NULL, `parent_comment_id` int(11) DEFAULT NULL, `comment` text NOT NULL, `comment_sender_name` varchar(40) NOT NULL, `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP, `graph` varchar(256), `tagging` int(11) ) ENGINE=InnoDB DEFAULT CHARSET=utf8

-- -- Dumping data for table `tbl_comment` -- 
INSERT INTO `tbl_comment` (`comment_id`, `parent_comment_id`, `comment`, `comment_sender_name`, `date`) VALUES (1, 0, ' asd', 'asd', '2017-12-14 07:54:57'), (2, 0, ' sss', 'Vincy', '2017-12-14 07:56:01'), (3, 2, ' ccc', 'xc', '2017-12-14 07:56:12'), (4, 0, 'sdf ', 'sdf', '2017-12-14 07:58:29'), (5, 0, ' ', 'as', '2017-12-14 08:02:32'), (6, 0, ' sdfsdf', 'dsfdsf', '2017-12-14 08:42:44'), (7, 0, ' ssss', 'ss', '2017-12-14 08:42:55'), (8, 3, 'New Comment ', 'Vincy', '2017-12-14 12:33:03'), (9, 2, 'jj ', 'gh', '2017-12-14 12:39:21'), (10, 2, 'jj ', 'ghasdsd', '2017-12-14 12:39:35'), (11, 0, ' asdasd', 'asdasd', '2017-12-14 12:40:01'), (12, 1, ' asdasd', 'aasd', '2017-12-14 12:40:10'), (13, 1, 'sss ', 'sss', '2017-12-14 12:40:38'), (14, 0, ' asdasd', 'asdasd', '2017-12-14 12:40:55'), (15, 1, 'vvv', 'vvv', '2017-12-14 12:41:14'), (16, 0, ' sss', 'sss', '2017-12-14 12:51:17')

 
-- -- Indexes for dumped tables -- -- -- Indexes for table `tbl_comment` --
 ALTER TABLE `tbl_comment` ADD PRIMARY KEY (`comment_id`)

-- -- AUTO_INCREMENT for dumped tables -- -- -- AUTO_INCREMENT for table `tbl_comment` -- ALTER TABLE `tbl_comment` MODIFY `comment_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27

RENAME TABLE `eazyinvoice`.`tbl_comment` TO `eazyinvoice`.`tbl_ba_comment`;

ALTER TABLE `tbl_ba_comment` ADD `bus_id` INT(11) NOT NULL AFTER `comment_id`, ADD `reg_id` INT(11) NOT NULL AFTER `bus_id`, ADD `gst_id` INT(11) NOT NULL AFTER `reg_id`;


/////////////////////////////////////////////
ALTER TABLE `feedback` ADD `createdat` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `feedback`, ADD `updatedat` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `createdat`;
ALTER TABLE `sales_expense_voucher` ADD `vchr_dollar_number` INT(11) NOT NULL AFTER `approver_name`, ADD `vchr_inr_value` DECIMAL(10,2) NOT NULL AFTER `vchr_dollar_number`;
ALTER TABLE `sales_expense_voucher_list` ADD `evl_cgst` INT(11) NOT NULL AFTER `evl_markup_amt`, ADD `evl_cgst_amt` DECIMAL(10,2) NOT NULL AFTER `evl_cgst`, ADD `evl_igst` INT(11) NOT NULL AFTER `evl_cgst_amt`, ADD `evl_igst_amt` DECIMAL(10,2) NOT NULL AFTER `evl_igst`, ADD `evl_sgst` INT(11) NOT NULL AFTER `evl_igst_amt`, ADD `evl_sgst_amt` DECIMAL(10,2) NOT NULL AFTER `evl_sgst`, ADD `evl_other` INT(11) NOT NULL AFTER `evl_sgst_amt`, ADD `evl_other_amt` DECIMAL(10,2) NOT NULL AFTER `evl_other`;

CREATE TABLE `assets` (
  `asset_id` int(11) NOT NULL,
  `bus_id` int(11) NOT NULL,
  `reg_id` int(11) NOT NULL,
  `gst_id` int(11) NOT NULL,
  `asset_code` varchar(256) NOT NULL,
  `asset_name` varchar(256) NOT NULL,
  `asset_date` date NOT NULL,
  `asset_description` varchar(256) NOT NULL,
  `nature_asset` varchar(256) NOT NULL,
  `asset_type` varchar(256) NOT NULL,
  `sac_no` varchar(256) NOT NULL,
  `asset_gst` varchar(256) NOT NULL,
  `asset_cess_tax` varchar(256) NOT NULL,
  `vendor_name` varchar(256) NOT NULL,
  `vendor_bill_add` varchar(256) NOT NULL,
  `vendor_country` int(11) NOT NULL,
  `vendor_state` int(11) NOT NULL,
  `vendor_city` int(11) NOT NULL,
  `vendor_pin` varchar(25) NOT NULL,
  `contact_person` varchar(256) NOT NULL,
  `mobile_no` varchar(25) NOT NULL,
  `contact_number` varchar(25) NOT NULL,
  `email_id` varchar(256) NOT NULL,
  `vendor_currency` int(11) NOT NULL,
  `vendor_gst_no` varchar(256) NOT NULL,
  `payment_advice` varchar(256) NOT NULL,
  `status` varchar(11) NOT NULL DEFAULT 'Active',
  `createdat` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedat` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `assets`
--

INSERT INTO `assets` (`asset_id`, `bus_id`, `reg_id`, `gst_id`, `asset_code`, `asset_name`, `asset_date`, `asset_description`, `nature_asset`, `asset_type`, `sac_no`, `asset_gst`, `asset_cess_tax`, `vendor_name`, `vendor_bill_add`, `vendor_country`, `vendor_state`, `vendor_city`, `vendor_pin`, `contact_person`, `mobile_no`, `contact_number`, `email_id`, `vendor_currency`, `vendor_gst_no`, `payment_advice`, `status`, `createdat`, `updatedat`) VALUES
(2, 152, 73, 92, 'AREG1', 'Laptop', '0000-00-00', 'xyz', 'tangible', '', '12', '12', '12', '12', 'mumbaii', 101, 22, 2707, '345678', 'sdcd', '1234567890', '2345678', 'man@ha,sd', 0, '3242423', '', 'Active', '2019-06-24 17:14:21', '2019-06-24 17:14:21');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `assets`
--
ALTER TABLE `assets`
  ADD PRIMARY KEY (`asset_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `assets`
--
ALTER TABLE `assets`
  MODIFY `asset_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;


  ///////////////////////////Asset Tracker//////////////////////////////////////////////////////
  

CREATE TABLE `assets_tracker` (
  `asset_track_id` int(11) NOT NULL,
  `bus_id` int(11) NOT NULL,
  `reg_id` int(11) NOT NULL,
  `gst_id` int(11) NOT NULL,
  `asset_code` varchar(256) NOT NULL,
  `asset_name` varchar(256) NOT NULL,
  `warranty_expiry` date NOT NULL,
  `asset_description` varchar(256) NOT NULL,
  `serial_no` varchar(256) NOT NULL,
  `set_email_remainder` varchar(256) NOT NULL,
  `status_asset_details` varchar(256) NOT NULL,
  `employee_code` varchar(256) NOT NULL,
  `employee_name` varchar(256) NOT NULL,
  `location` varchar(256) NOT NULL,
  `amc_provider_name` varchar(256) NOT NULL,
  `contact_person` int(11) NOT NULL,
  `mobile_number` varchar(25) NOT NULL,
  `contact_number` varchar(25) NOT NULL,
  `email_id` varchar(256) NOT NULL,
  `type_of_service` varchar(256) NOT NULL,
  `frequency_of_service` int(11) NOT NULL,
  `set_remainder` varchar(256) NOT NULL,
  `amc_doc` varchar(256) NOT NULL,
  `status` varchar(11) NOT NULL DEFAULT 'Active',
  `createdat` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedat` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `assets_tracker`
--

INSERT INTO `assets_tracker` (`asset_track_id`, `bus_id`, `reg_id`, `gst_id`, `asset_code`, `asset_name`, `warranty_expiry`, `asset_description`, `serial_no`, `set_email_remainder`, `status_asset_details`, `employee_code`, `employee_name`, `location`, `amc_provider_name`, `contact_person`, `mobile_number`, `contact_number`, `email_id`, `type_of_service`, `frequency_of_service`, `set_remainder`, `amc_doc`, `status`, `createdat`, `updatedat`) VALUES
(1, 152, 73, 92, '2', 'Laptop', '0000-00-00', 'xyz', '343434', 'manoj.bhoir9@gmail.com', 'In Use', '1', 'Mr manoj bhoir', 'Maharashtra', 'rtte', 0, '588345593', '2345678', 'jhsadha@sgdh.cn', 'sahd', 2, '19/06/2019', '', 'Active', '2019-06-25 18:50:12', '2019-06-25 18:50:12');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `assets_tracker`
--
ALTER TABLE `assets_tracker`
  ADD PRIMARY KEY (`asset_track_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `assets_tracker`
--
ALTER TABLE `assets_tracker`
  MODIFY `asset_track_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;



ALTER TABLE `tds_payment` ADD `gst_id` INT(11) NOT NULL AFTER `bus_id`;


ALTER TABLE `assets_tracker` CHANGE `contact_person` `contact_person` VARCHAR(256) NOT NULL;

ALTER TABLE `registration` ADD `auth_token` VARCHAR(256) NULL AFTER `forget_pwd_token`;

ALTER TABLE `expense_debit_note_list` CHANGE `service_id` `exp_id` INT(11) NOT NULL;
ALTER TABLE `expense_purchase_order_list` CHANGE `service_id` `exp_id` INT(11) NOT NULL;

CREATE TABLE `petty_cash_list` ( `p_id` int(11) NOT NULL, `reg_id` int(11) NOT NULL, `bus_id` int(11) NOT NULL, `gst_id` int(11) NOT NULL, `petty_cash_date` date NOT NULL, `exp_name` varchar(11) NOT NULL, `particulars` int(20) NOT NULL, `opening_balance` int(11) NOT NULL, `debit_amount` int(11) NOT NULL, `credit_amount` int(11) NOT NULL, `closing_balance` int(11) NOT NULL, `percentage` varchar(11) NOT NULL, `petty_cash_code` int(11) NOT NULL, `petty_cash_notes` int(20) NOT NULL, `status` varchar(11) NOT NULL, `actions` varchar(11) NOT NULL, `created_at` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `updated_at` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ) ENGINE=InnoDB DEFAULT CHARSET=latin1
ALTER TABLE `petty_cash_list` ADD PRIMARY KEY (`p_id`);
ALTER TABLE `petty_cash_list` MODIFY `p_id` int(11) NOT NULL AUTO_INCREMENT;


INSERT INTO `asset_type_list` (`atl_id`, `asset_type_name`, `sac_no`, `notes`) VALUES (NULL, 'Computer', '', ''), (NULL, 'Computer Software', '', ''), (NULL, 'Furniture & Fixture', '', ''), (NULL, 'Plant & Machinery', '', ''), (NULL, 'Land ', '', '');
INSERT INTO `asset_type_list` (`atl_id`, `asset_type_name`, `sac_no`, `notes`) VALUES (NULL, 'Office Equipment', '', ''), (NULL, 'Vehicle', '', '');
INSERT INTO `asset_type_list` (`atl_id`, `asset_type_name`, `sac_no`, `notes`) VALUES (NULL, 'Building', '', '');

ALTER TABLE `asset_purchase` CHANGE `asset_name` `asset_name` INT(11) NOT NULL;
ALTER TABLE `asset_purchase` ADD `apur_cess_total` DECIMAL(10,2) NOT NULL AFTER `updatedat`, ADD `apur_other_total` DECIMAL(10,2) NOT NULL AFTER `apur_cess_total`;
ALTER TABLE `asset_purchase_list` ADD `cess_amount` DECIMAL(10,2) NOT NULL AFTER `igst_amount`, ADD `other_amount` DECIMAL(10,2) NOT NULL AFTER `cess_amount`;
ALTER TABLE `asset_purchase_list` ADD `cgst_per` INT(11) NOT NULL AFTER `amount`, ADD `sgst_per` INT(11) NOT NULL AFTER `cgst_per`, ADD `igst_per` INT(11) NOT NULL AFTER `sgst_per`, ADD `other_per` INT(11) NOT NULL AFTER `igst_per`;
ALTER TABLE `asset_purchase_list` ADD `createdat` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `other_per`, ADD `updatedat` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `createdat`;
ALTER TABLE `company_expense` CHANGE `ce_date` `ce_date` DATE NOT NULL;
ALTER TABLE `company_expense` CHANGE `ce_refbilldate` `ce_refbilldate` DATE NOT NULL;
ALTER TABLE `company_expense` CHANGE `ce_refpodate` `ce_refpodate` DATE NOT NULL;
ALTER TABLE `company_expense` CHANGE `ce_recurdate` `ce_recurdate` DATE NOT NULL;
ALTER TABLE `company_expense` CHANGE `ce_recurfreq` `ce_recurfreq` VARCHAR(256) NOT NULL;

ALTER TABLE `ledger` ADD `taxable_amt` DECIMAL(10,2) NOT NULL AFTER `cust_name`;

ALTER TABLE `businesslist` ADD `opening_bank_balance` DECIMAL(10,2) NOT NULL AFTER `cbranch_id`, ADD `opening_cash_balance` DECIMAL(10,2) NOT NULL AFTER `opening_bank_balance`;
ALTER TABLE `assets` CHANGE `asset_type` `asset_type` INT(11) NOT NULL;
ALTER TABLE `asset_purchase` ADD `apur_discount_total` DECIMAL(10,2) NOT NULL AFTER `apur_taxable_amt`;
ALTER TABLE `asset_purchase_list` ADD `cess_per` INT(11) NOT NULL AFTER `other_per`;
ALTER TABLE `asset_purchase` CHANGE `vendor_id` `vendor_name` VARCHAR(256) NOT NULL;



CREATE TABLE `assets_other_taxes` (
  `stax_id` int(11) NOT NULL,
  `asset_id` int(11) NOT NULL,
  `stax_name` varchar(100) NOT NULL,
  `stax_percentage` decimal(10,2) NOT NULL,
  `status` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  `createdat` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedat` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `assets_other_taxes`
--
ALTER TABLE `assets_other_taxes`
  ADD PRIMARY KEY (`stax_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `assets_other_taxes`
--
ALTER TABLE `assets_other_taxes`
  MODIFY `stax_id` int(11) NOT NULL AUTO_INCREMENT;

  /////////////////////////////////////////////////////////

  ALTER TABLE `customise_invoices` CHANGE `inv_layout` `inv_layout` INT(11) NOT NULL;

  ALTER TABLE `company_expense` ADD `exp_status` VARCHAR(256) NOT NULL AFTER `ce_notes`;

/////////////////////////////////////////////////////////////////////

CREATE TABLE `rating` (
  `review_id` int(11) NOT NULL,
  `reg_id` int(11) NOT NULL,
  `bus_id` int(11) NOT NULL,
  `rate` int(11) NOT NULL,
  `feedback` text DEFAULT NULL,
  `act_taken` varchar(256) NOT NULL,
  `by_whom` int(11) NOT NULL,
  `act_date` date NOT NULL,
  `createdat` datetime NOT NULL DEFAULT current_timestamp(),
  `updatedat` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


--
-- Indexes for dumped tables
--

--
-- Indexes for table `rating`
--
ALTER TABLE `rating`
  ADD PRIMARY KEY (`review_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `rating`
--
ALTER TABLE `rating`
  MODIFY `review_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

  ALTER TABLE `account_jv` ADD `common_id` INT(11) NOT NULL AFTER `resource_id`, ADD `ledger_name` VARCHAR(256) NOT NULL AFTER `common_id`;
  ALTER TABLE `balance_sheet` ADD `common_id` INT(11) NOT NULL AFTER `ledger_type`, ADD `ledger_name` VARCHAR(256) NOT NULL AFTER `common_id`;
  ALTER TABLE `profit_and_loss` ADD `common_id` INT(11) NOT NULL AFTER `ledger_type`, ADD `ledger_name` VARCHAR(256) NOT NULL AFTER `common_id`;
  ALTER TABLE `ledger` ADD `common_id` INT(11) NOT NULL AFTER `ledger_type`;

  ALTER TABLE `company_expense` CHANGE `ce_eqpercent` `ce_eqpercent` DECIMAL(3,2) NOT NULL, CHANGE `ce_eq_taxableamt` `ce_eq_taxableamt` DECIMAL(10,2) NOT NULL, CHANGE `ce_discount_total` `ce_discount_total` DECIMAL(10,2) NOT NULL, CHANGE `ce_taxable_total` `ce_taxable_total` DECIMAL(10,2) NOT NULL, CHANGE `ce_cgst_total` `ce_cgst_total` DECIMAL(10,2) NOT NULL, CHANGE `ce_sgst_total` `ce_sgst_total` DECIMAL(10,2) NOT NULL, CHANGE `ce_cess_total` `ce_cess_total` DECIMAL(10,2) NOT NULL, CHANGE `ce_other_total` `ce_other_total` DECIMAL(10,2) NOT NULL, CHANGE `ce_grandtotal` `ce_grandtotal` DECIMAL(10,2) NOT NULL, CHANGE `ce_paid` `ce_paid` DECIMAL(10,2) NOT NULL, CHANGE `ce_pending_amt` `ce_pending_amt` DECIMAL(10,2) NOT NULL;

  ALTER TABLE `expense_vendors` ADD `submerchant_id` VARCHAR(256) NOT NULL AFTER `vendor_currency`;
  ALTER TABLE `employee_master` ADD `submerchant_id` VARCHAR(256) NOT NULL AFTER `currency`;

  ALTER TABLE `profit_and_loss` CHANGE `ledger_type` `ledger_type` VARCHAR(256) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;
ALTER TABLE `ledger` CHANGE `ledger_name` `ledger_name` VARCHAR(256) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;
ALTER TABLE `ledger` CHANGE `ledger_type` `ledger_type` VARCHAR(256) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;
ALTER TABLE `account_jv` CHANGE `date` `date` DATETIME NOT NULL;
ALTER TABLE `balance_sheet` CHANGE `ledger_type` `ledger_type` VARCHAR(256) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;

  ALTER TABLE `expense_vendors` ADD `bank_acc_no` VARCHAR(256) NOT NULL AFTER `submerchant_id`, ADD `bank_name` VARCHAR(256) NOT NULL AFTER `bank_acc_no`, ADD `bank_ifsc_code` VARCHAR(256) NOT NULL AFTER `bank_name`, ADD `branch_name` VARCHAR(256) NOT NULL AFTER `bank_ifsc_code`;

  ALTER TABLE `petty_cash_list` ADD `select_type` VARCHAR(256) NOT NULL AFTER `petty_cash_date`;


  ALTER TABLE `asset_payments` ADD `asset_purchase_no` VARCHAR(256) NOT NULL AFTER `apay_date`, ADD `apay_apur_id` VARCHAR(256) NOT NULL AFTER `asset_purchase_no`, ADD `apay_apur_type` VARCHAR(256) NOT NULL AFTER `apay_apur_id`, ADD `apay_apur_nature` VARCHAR(256) NOT NULL AFTER `apay_apur_type`, ADD `apay_apur_amt` DECIMAL(10,2) NOT NULL AFTER `apay_apur_nature`, ADD `apay_apur_date` DATE NOT NULL AFTER `apay_apur_amt`;
  ALTER TABLE `asset_payments` CHANGE `asset_purchase_no` `asset_purchase_no` INT(11) NOT NULL;
  ALTER TABLE `asset_payments` CHANGE `apay_apur_date` `asset_date` DATE NOT NULL;
  ALTER TABLE `asset_payments` ADD `apay_apur_name` VARCHAR(256) NOT NULL AFTER `apay_apur_id`;

  ALTER TABLE `prof_tax` CHANGE `pt_id` `pt_id` INT(11) NOT NULL AUTO_INCREMENT, CHANGE `createdat` `createdat` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, CHANGE `updatedat` `updatedat` TIMESTAMP on update CURRENT_TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP, CHANGE `pt_pay_no` `pt_pay_no` VARCHAR(256) NOT NULL, CHANGE `type_pt` `type_pt` INT(11) NOT NULL, CHANGE `loc` `loc` VARCHAR(256) NOT NULL, CHANGE `prox_tax_yr` `prox_tax_yr` VARCHAR(256) NOT NULL, CHANGE `pt_pay_receiptchdate` `pt_pay_receiptchdate` DATE NOT NULL, CHANGE `pay_date` `pay_date` DATE NOT NULL, CHANGE `pay_status1` `pay_status1` VARCHAR(256) NOT NULL, CHANGE `pay_mode` `pay_mode` VARCHAR(256) NOT NULL, CHANGE `pay_bank_id` `pay_bank_id` INT(11) NOT NULL, CHANGE `challanno` `challanno` VARCHAR(256) NOT NULL, CHANGE `tds_pay_receiptchdate` `tds_pay_receiptchdate` DATE NOT NULL;

  ALTER TABLE `salary_expense` CHANGE `Status` `sstatus` VARCHAR(25) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;

  ALTER TABLE `salary_expense` ADD `status` VARCHAR(256) NOT NULL DEFAULT 'Active' AFTER `sstatus`;
  ALTER TABLE `asset_sale` ADD `asset_sale_code` VARCHAR(256) NOT NULL AFTER `asal_id`;


  CREATE TABLE `asset_sale_list` (
  `asl_id` int(11) NOT NULL,
  `asal_id` int(11) NOT NULL,
  `bus_id` int(11) NOT NULL,
  `gst_id` int(11) NOT NULL,
  `particulars` varchar(200) NOT NULL,
  `hsn` varchar(100) NOT NULL,
  `quantity` int(11) NOT NULL,
  `rate` decimal(10,0) NOT NULL,
  `discount` decimal(10,0) NOT NULL,
  `taxable_amt` decimal(10,0) NOT NULL,
  `cgst_amt` decimal(10,0) NOT NULL,
  `sgst_amt` decimal(10,0) NOT NULL,
  `igst_amount` decimal(10,2) NOT NULL,
  `cess_amount` decimal(10,2) NOT NULL,
  `other_amount` decimal(10,2) NOT NULL,
  `amount` decimal(10,0) NOT NULL,
  `cgst_per` int(11) NOT NULL,
  `sgst_per` int(11) NOT NULL,
  `igst_per` int(11) NOT NULL,
  `other_per` int(11) NOT NULL,
  `cess_per` int(11) NOT NULL,
  `createdat` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedat` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `asset_sale_list`
--
ALTER TABLE `asset_sale_list`
  ADD PRIMARY KEY (`asl_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `asset_sale_list`
--
ALTER TABLE `asset_sale_list`
  MODIFY `asl_id` int(11) NOT NULL AUTO_INCREMENT;


  ALTER TABLE `asset_sale` ADD `sale_date` DATE NOT NULL AFTER `asal_date`;

  ALTER TABLE `asset_sale` ADD `asal_cess_total` DECIMAL(10,2) NOT NULL AFTER `updatedat`, ADD `asal_other_total` DECIMAL(10,2) NOT NULL AFTER `asal_cess_total`;
  ALTER TABLE `asset_sale` ADD `asal_discount_total` DECIMAL(10,2) NOT NULL AFTER `asal_other_total`;



ALTER TABLE `asset_sale` ADD `asset_type_id` INT(11) NOT NULL AFTER `asset_sale_code`, ADD `asset_nature` VARCHAR(256) NOT NULL AFTER `asset_type_id`;

ALTER TABLE `asset_sale` ADD `asset_name` INT(11) NOT NULL AFTER `asset_sale_code`;

ALTER TABLE `asset_sale` CHANGE `asset_type_id` `asset_type_id` VARCHAR(256) NOT NULL;
ALTER TABLE `asset_sale` CHANGE `asset_type_id` `asset_type_id` INT(11) NOT NULL;
ALTER TABLE `asset_sale` CHANGE `asset_apur_id` `asset_apur_id` VARCHAR(256) NULL DEFAULT NULL;
ALTER TABLE `asset_sale` ADD `asal_igst_total` DECIMAL(10,2) NOT NULL AFTER `asal_sgst_total`;
ALTER TABLE `asset_sales_receipt` CHANGE `asrec_date` `asrec_date` DATE NOT NULL;
ALTER TABLE `asset_sales_receipt` CHANGE `asrec_receipt_date` `asrec_receipt_date` DATE NOT NULL;

ALTER TABLE `asset_sales_receipt` ADD `asal_no` INT(11) NOT NULL AFTER `asrec_code`, ADD `asset_name` VARCHAR(256) NOT NULL AFTER `asal_no`, ADD `sale_date` DATE NOT NULL AFTER `asset_name`, ADD `purchase_name` VARCHAR(256) NOT NULL AFTER `sale_date`, ADD `sales_amount` DECIMAL(10,2) NOT NULL AFTER `purchase_name`, ADD `sales_amount_inr` DECIMAL(10,2) NOT NULL AFTER `sales_amount`;
ALTER TABLE `asset_sales_receipt` ADD `asset_no` INT(11) NOT NULL AFTER `asal_no`;

ALTER TABLE `businesslist` ADD `opening_balance_date` DATE NOT NULL AFTER `opening_cash_balance`, ADD `cash_balance_date` DATE NOT NULL AFTER `opening_balance_date`

CREATE TABLE `sales_expense_voucher` (
  `ev_id` int(11) NOT NULL,
  `reg_id` int(11) NOT NULL,
  `bus_id` int(11) NOT NULL,
  `cust_id` int(11) NOT NULL,
  `emp_code` varchar(256) NOT NULL,
  `emp_name` varchar(256) NOT NULL,
  `department` varchar(256) NOT NULL,
  `emp_designation` varchar(256) NOT NULL,
  `approver_name` varchar(256) NOT NULL,
  `vchr_dollar_number` int(11) NOT NULL,
  `vchr_inr_value` decimal(10,2) NOT NULL,
  `ev_exp_vchr_no` varchar(200) NOT NULL,
  `exp_voi_date` date NOT NULL,
  `total_expense` decimal(10,2) NOT NULL,
  `total_markup` decimal(10,2) NOT NULL,
  `total_amount` decimal(10,2) NOT NULL,
  `ev_note` text NOT NULL,
  `status` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  `used_status` int(11) NOT NULL,
  `createdat` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedat` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `sales_expense_voucher`
  ADD PRIMARY KEY (`ev_id`),
  ADD KEY `cust_id` (`cust_id`),
  ADD KEY `reg_id` (`reg_id`),
  ADD KEY `bus_id` (`bus_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `sales_expense_voucher`
--
ALTER TABLE `sales_expense_voucher`
  MODIFY `ev_id` int(11) NOT NULL AUTO_INCREMENT


ALTER TABLE `expense_payment_list` ADD `epayl_exp_name` VARCHAR(256) NOT NULL AFTER `epayl_exp_no`;

ALTER TABLE `expense_payment_list` ADD `epayl_exp_cat` VARCHAR(256) NOT NULL AFTER `epayl_exp_name`;

ALTER TABLE `employee_master` ADD `swift_code` VARCHAR(256) NOT NULL AFTER `bank_ifsc_code`;

ALTER TABLE `employee_master` ADD `emergency_contact_relationship` VARCHAR(256) NOT NULL AFTER `emergency_contact_number`;
ALTER TABLE `expense_vendors` ADD `nature_of_bus` VARCHAR(256) NOT NULL AFTER `vendor_due_date`;
ALTER TABLE `sales_customers` ADD `nature_of_bus` VARCHAR(256) NOT NULL AFTER `cust_due_date`;

ALTER TABLE `assets_tracker` CHANGE `contact_person` `contact_person` VARCHAR(256) NOT NULL;

ALTER TABLE `company_bank` ADD `opening_bank_balance` DECIMAL(10,2) NOT NULL AFTER `cbank_country_code`, ADD `opening_balance_date` DATE NOT NULL AFTER `opening_bank_balance`

ALTER TABLE `my_alerts` ADD `subparent_id` INT(11) NOT NULL AFTER `parent_name`;
ALTER TABLE `employee_master` ADD `portal_access` INT(11) NOT NULL DEFAULT '0' AFTER `pincode_local`;
ALTER TABLE `subscription` ADD `status` VARCHAR(11) NOT NULL AFTER `billing_state`;

ALTER TABLE `tds_payment` ADD `tds_from_date` DATE NOT NULL AFTER `type_tds`, ADD `tds_to_date` DATE NOT NULL AFTER `tds_from_date`;

INSERT INTO `currency` (`currency_id`, `currency`, `currencycode`, `symbol`) VALUES (NULL, 'Dirham', 'AED', 'د.إ');
ALTER TABLE `assets_tracker` ADD `email_remainder_amc` VARCHAR(256) NOT NULL AFTER `set_remainder`;
INSERT INTO `access_permissions` (`id`, `access`, `created_date`, `status`) VALUES (NULL, 'Employee', '', '');


ALTER TABLE `coupon_promo` ADD `pack_select` VARCHAR(256) NOT NULL AFTER `target_group`;


ALTER TABLE `expense_debit_note` ADD `equ_levy_debit` INT(11) NOT NULL AFTER `debit_grant_total`;


ALTER TABLE `businesslist` ADD `opening_petty_balance` DECIMAL(10) NOT NULL AFTER `cash_balance_date`;
ALTER TABLE `businesslist` ADD `petty_balance_date` DATE NOT NULL AFTER `opening_petty_balance`;

ALTER TABLE `asset_payments` ADD `srec_notes` TEXT NOT NULL AFTER `status`;

ALTER TABLE `assets` ADD `notes` TEXT NOT NULL AFTER `status`;



ALTER TABLE `sales_expense_voucher` ADD `set_default` VARCHAR(11) NOT NULL AFTER `vchr_dollar_number`;

ALTER TABLE `expense_debit_note` ADD `sign_show` VARCHAR(11) NOT NULL AFTER `status`;

ALTER TABLE `employee_master` ADD `bank_currency` INT(11) NOT NULL AFTER `country`;

ALTER TABLE `salary_expense` CHANGE `salary_exp_date` `salary_exp_date` DATE NOT NULL;

ALTER TABLE `tbl_ba_comment` ADD `priority` VARCHAR(11) NOT NULL AFTER `tagging`;


CREATE TABLE `voucher_comments` (
  `comment_id` int(11) NOT NULL,
  `voucher_id` int(11) NOT NULL,
  `bus_id` int(11) NOT NULL,
  `reg_id` int(11) NOT NULL,
  `gst_id` int(11) NOT NULL,
  `parent_comment_id` int(11) DEFAULT NULL,
  `comment` text NOT NULL,
  `comment_sender_name` varchar(40) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `graph` varchar(256) DEFAULT NULL,
  `tagging` text,
  `priority` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `voucher_comments`


ALTER TABLE `registration` ADD `emp_id` INT(11) NOT NULL AFTER `is_login`;
ALTER TABLE `sales_expense_voucher` ADD `approve_status` INT(11) NOT NULL DEFAULT '1' AFTER `status`;
ALTER TABLE `company_expense` ADD `em_tdshead` VARCHAR(256) NOT NULL AFTER `ce_tdshead`;

ALTER TABLE `tbl_ba_comment` CHANGE `tagging` `tagging` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL;


ALTER TABLE `sales_credit_debit` ADD `sign_show` VARCHAR(11) NOT NULL AFTER `status`
ALTER TABLE `sales_credit_debit` ADD  `equ_levy_credit` INT(11) NOT NULL AFTER `cd_grant_total`;

ALTER TABLE `sales_invoices` ADD `inv_attachment_id` TEXT NOT NULL AFTER `inv_attachment`;

ALTER TABLE `sales_credit_debit` ADD `exp_vou_id` INT(11) NOT NULL AFTER `equ_levy_credit`;

//.............................................//
ALTER TABLE `employee_master` CHANGE `date_of_exit` `date_of_exit` DATE NOT NULL;

ALTER TABLE `employee_master` ADD `bank_country` INT(11) NOT NULL AFTER `bank_name`;



ALTER TABLE `salary_expense` ADD `recurring_month_add` VARCHAR(11) NOT NULL AFTER `month_days`, ADD `recurring_month_ded` VARCHAR(11) NOT NULL AFTER `recurring_month_add`;

ALTER TABLE `tds_employee` ADD `tds_month` VARCHAR(50) NOT NULL AFTER `type_of_tds_emp`;


ALTER TABLE `employee_master` CHANGE `date_of_approval` `date_of_approval` DATE NOT NULL;

/////////////09-01-2002///////////////////////
ALTER TABLE `tbl_ba_comment` ADD `comment_for` VARCHAR(256) NOT NULL AFTER `comment_sender_name`;

ALTER TABLE `employee_payment` ADD `advnc_taken` DECIMAL(10,2) NOT NULL AFTER `payment_forex_gain`, ADD `emp_epf` DECIMAL(10,2) NOT NULL AFTER `advnc_taken`, ADD `gratutity_ex` DECIMAL(10,2) NOT NULL AFTER `emp_epf`, ADD `emp_sup_ann` DECIMAL(10,2) NOT NULL AFTER `gratutity_ex`, ADD `loan_paid` DECIMAL(10,2) NOT NULL AFTER `emp_sup_ann`;

ALTER TABLE `employee_payment_list` ADD `advnc_taken` DECIMAL(10,2) NOT NULL AFTER `exp_conv_amt`;

ALTER TABLE `tds_employee` ADD `employee_id` INT(11) NOT NULL AFTER `tdsemp_id`;
ALTER TABLE `tds_employee` CHANGE `employee_id` `employee_id` VARCHAR(256) NOT NULL;
ALTER TABLE `assets_tracker` ADD `set_reminder_date` DATE NOT NULL AFTER `email_remainder_amc`;

ALTER TABLE `sales_expense_voucher` ADD `approverId` TEXT NOT NULL AFTER `approve_status`;
ALTER TABLE `sales_expense_voucher` CHANGE `approve_status` `approve_status` TEXT NOT NULL;

///////////////////////14////////////////////
ALTER TABLE `assets_tracker` ADD `set_reminder_date` DATE NOT NULL AFTER `frequency_of_service`;
ALTER TABLE `asset_purchase` ADD `apur_export_type` INT(11) NOT NULL AFTER `apur_supply_place`, ADD `apur_export_currency` INT(11) NOT NULL AFTER `apur_export_type`;
ALTER TABLE `asset_purchase` ADD `apur_dollar_number` INT(11) NOT NULL AFTER `apur_export_currency`, ADD `apur_inr_value` DECIMAL(10,2) NOT NULL AFTER `apur_dollar_number`;
ALTER TABLE `asset_sale` ADD `asale_export_type` INT(11) NOT NULL AFTER `asal_supply_place`, ADD `asale_export_currency` INT(11) NOT NULL AFTER `asale_export_type`
ALTER TABLE `asset_sale` ADD `asale_dollar_number` INT(11) NOT NULL AFTER `asale_export_currency`, ADD `asale_inr_value` DECIMAL(10,2) NOT NULL AFTER `asale_dollar_number`;
ALTER TABLE `asset_sale` ADD `pl_amt` DECIMAL(10,2) NOT NULL AFTER `asal_profit_loss`;

ALTER TABLE `asset_sale` ADD `asal_country` INT(11) NOT NULL AFTER `asal_supply_place`;


CREATE TABLE `cash_statement` (
  `cash_id` int(11) NOT NULL,
  `cash_code` varchar(256) NOT NULL,
  `cash_type` varchar(256) NOT NULL,
  `bank_id` int(11) NOT NULL,
  `cash_amount` decimal(10,2) NOT NULL,
  `cash_note` text NOT NULL,
  `cash_date` date NOT NULL,
  `createdat` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedat` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cash_statement`
--
ALTER TABLE `cash_statement`
  ADD PRIMARY KEY (`cash_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cash_statement`
--
ALTER TABLE `cash_statement`
  MODIFY `cash_id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
ALTER TABLE `cash_statement` ADD `bus_id` INT(11) NOT NULL AFTER `cash_code`, ADD `gst_id` INT(11) NOT NULL AFTER `bus_id`, ADD `reg_id` INT(11) NOT NULL AFTER `gst_id`;
ALTER TABLE `cash_statement` ADD `status` VARCHAR(256) NOT NULL AFTER `cash_date`;
ALTER TABLE `petty_cash_list` CHANGE `petty_cash_notes` `petty_cash_notes` VARCHAR(256) NOT NULL;
ALTER TABLE `petty_cash_list` CHANGE `particulars` `particulars` VARCHAR(256) NOT NULL;

ALTER TABLE `sales_expense_voucher_list` CHANGE `evl_exp_type_id` `evl_exp_type` VARCHAR(256) NOT NULL

///////////////////22///////////////////////
ALTER TABLE `employee_payment` ADD `esic_amt` DECIMAL(10,2) NOT NULL AFTER `emp_epf`;

ALTER TABLE `card_statement` CHANGE `state_period` `state_period` VARCHAR(256) NOT NULL;

ALTER TABLE `asset_payments` ADD `apay_conv_amt` DECIMAL(10,2) NOT NULL AFTER `apay_apur_amt`;
ALTER TABLE `asset_payments` CHANGE `vendor_id` `vendor_id` VARCHAR(256) NOT NULL;

ALTER TABLE `asset_purchase` CHANGE `apur_dollar_number` `apur_dollar_number` INT(11) NOT NULL DEFAULT '1';
ALTER TABLE `asset_purchase` CHANGE `apur_inr_value` `apur_inr_value` DECIMAL(10,2) NOT NULL DEFAULT '1';
ALTER TABLE `asset_payments` CHANGE `ap_dollar_number` `ap_dollar_number` INT(11) NOT NULL DEFAULT '1';
ALTER TABLE `asset_payments` CHANGE `ap_inr_value` `ap_inr_value` DECIMAL(10,2) NOT NULL DEFAULT '1';
UPDATE `asset_purchase` SET apur_inr_value=1;
UPDATE `asset_payments` SET ap_inr_value=1;

UPDATE `asset_payments` SET ap_dollar_number=1;

ALTER TABLE `asset_sale` CHANGE `asale_dollar_number` `asale_dollar_number` INT(11) NOT NULL DEFAULT '1';
ALTER TABLE `asset_sale` CHANGE `asale_inr_value` `asale_inr_value` DECIMAL(10,2) NOT NULL DEFAULT '1';
UPDATE `asset_sale` SET asale_dollar_number=1
UPDATE `asset_sale` SET asale_inr_value=1
ALTER TABLE `asset_sales_receipt` ADD `asale_dollar_no` INT(11) NOT NULL DEFAULT '1' AFTER `sales_amount_inr`, ADD `asale_inr_value` DECIMAL(10,2) NOT NULL DEFAULT '1' AFTER `asale_dollar_no`;
ALTER TABLE `asset_sales_receipt` CHANGE `asale_dollar_no` `asale_dollar_number` INT(11) NOT NULL DEFAULT '1';

CREATE TABLE `asset_bank` (
  `cbank_id` int(11) NOT NULL,
  `bus_id` int(11) NOT NULL,
  `asset_id` int(11) NOT NULL,
  `cbank_name` varchar(250) NOT NULL,
  `cbank_account_no` varchar(100) NOT NULL,
  `cbank_ifsc_code` varchar(256) DEFAULT NULL,
  `cbank_swift_code` varchar(256) DEFAULT NULL,
  `cbank_currency_code` int(11) DEFAULT NULL,
  `cbank_account_type` varchar(50) NOT NULL,
  `cbank_branch_name` varchar(250) NOT NULL,
  `cbank_country_code` int(11) DEFAULT NULL,
  `opening_bank_balance` decimal(10,2) NOT NULL,
  `opening_balance_date` date NOT NULL,
  `status` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  `createdat` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedat` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `asset_bank`
--



--
-- Indexes for dumped tables
--

--
-- Indexes for table `asset_bank`
--
ALTER TABLE `asset_bank`
  ADD PRIMARY KEY (`cbank_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `asset_bank`
--
ALTER TABLE `asset_bank`
  MODIFY `cbank_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;





  CREATE TABLE `vendor_bank` (
  `cbank_id` int(11) NOT NULL,
  `bus_id` int(11) NOT NULL,
  `vendor_id` int(11) NOT NULL,
  `cbank_name` varchar(250) NOT NULL,
  `cbank_account_no` varchar(100) NOT NULL,
  `cbank_ifsc_code` varchar(256) DEFAULT NULL,
  `cbank_swift_code` varchar(256) DEFAULT NULL,
  `cbank_currency_code` int(11) DEFAULT NULL,
  `cbank_account_type` varchar(50) NOT NULL,
  `cbank_branch_name` varchar(250) NOT NULL,
  `cbank_country_code` int(11) DEFAULT NULL,
  `opening_bank_balance` decimal(10,2) NOT NULL,
  `opening_balance_date` date NOT NULL,
  `status` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  `createdat` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedat` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `vendor_bank`
--


--
-- Indexes for dumped tables
--

--
-- Indexes for table `vendor_bank`
--
ALTER TABLE `vendor_bank`
  ADD PRIMARY KEY (`cbank_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `vendor_bank`
--
ALTER TABLE `vendor_bank`
  MODIFY `cbank_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;


  ALTER TABLE `prof_tax` ADD `empr_share_amt` DECIMAL(10,2) NOT NULL AFTER `deposit`, ADD `empr_share_amt_inr` DECIMAL(10,2) NOT NULL AFTER `empr_share_amt`, ADD `emp_name_id` VARCHAR(256) NOT NULL AFTER `empr_share_amt_inr`, ADD `exp_vch_no` VARCHAR(256) NOT NULL AFTER `emp_name_id`, ADD `exp_vch_amt` DECIMAL(10,2) NOT NULL AFTER `exp_vch_no`, ADD `exp_vch_amt_inr` DECIMAL(10,2) NOT NULL AFTER `exp_vch_amt`, ADD `pay_empr_share_amt` DECIMAL(10,2) NOT NULL AFTER `exp_vch_amt_inr`, ADD `admin_chrg` DECIMAL(10,2) NOT NULL AFTER `pay_empr_share_amt`, ADD `insp_chrg` DECIMAL(10,2) NOT NULL AFTER `admin_chrg`, ADD `exp_amt` DECIMAL(10,2) NOT NULL AFTER `insp_chrg`;

  ALTER TABLE `prof_tax` ADD `forex_gain` DECIMAL(10,2) NOT NULL AFTER `adv_pay`, ADD `forex_loss` DECIMAL(10,2) NOT NULL AFTER `forex_gain`;
  ALTER TABLE `prof_tax` ADD `pay_empr_share_amt_inr` DECIMAL NOT NULL AFTER `pay_empr_share_amt`;
  ALTER TABLE `prof_tax` CHANGE `empr_share_amt_inr` `empl_share_amt` DECIMAL(10,2) NOT NULL;
  ALTER TABLE `prof_tax` CHANGE `pay_empr_share_amt_inr` `pay_empl_share_amt` DECIMAL(10,0) NOT NULL;

  ALTER TABLE `sales_expense_voucher` ADD `ev_status` VARCHAR(11) NOT NULL AFTER `used_status`;
  ALTER TABLE `sales_expense_voucher` CHANGE `ev_status` `ev_status` VARCHAR(11) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'Unpaid';

  ALTER TABLE `prof_tax` CHANGE `forex_gain` `pr_forex_gain` DECIMAL(10,2) NOT NULL, CHANGE `forex_loss` `pr_forex_loss` DECIMAL(10,2) NOT NULL;
  ALTER TABLE `prof_tax` ADD `pr_dollar_number` INT(11) NOT NULL DEFAULT '1' AFTER `pr_forex_loss`, ADD `pr_inr_value` DECIMAL(10,2) NOT NULL DEFAULT '1' AFTER `pr_dollar_number`;
  ALTER TABLE `sales_expense_voucher` ADD `pay_status` VARCHAR(11) NOT NULL AFTER `ev_status`;
  ALTER TABLE `sales_expense_voucher` CHANGE `pay_status` `pay_status` VARCHAR(11) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'Unpaid';
  ALTER TABLE `sales_invoices` CHANGE `inv_is_rev_charge` `inv_rev_charge` INT(11) NOT NULL;

  ALTER TABLE `my_alerts` ADD `notes` TEXT NOT NULL AFTER `occasion_date`;

//////////////////////////Accounting/////////////////////////////////////////
  ALTER TABLE `balance_sheet` ADD `esic_pay` DECIMAL(10,2) NOT NULL AFTER `pt_pay`, ADD `loan_pay` DECIMAL(10,2) NOT NULL AFTER `esic_pay`;
  ALTER TABLE `account_jv` ADD `esic_credit` DECIMAL(10,2) NOT NULL AFTER `epf_debit`, ADD `esic_debit` DECIMAL(10,2) NOT NULL AFTER `esic_credit`, ADD `loan_credit` DECIMAL(10,2) NOT NULL AFTER `esic_debit`, ADD `loan_debit` DECIMAL(10,2) NOT NULL AFTER `loan_credit`;
  ALTER TABLE `ledger` ADD `esic_credit` DECIMAL(10,2) NOT NULL AFTER `pt_credit`, ADD `esic_debit` DECIMAL(10,2) NOT NULL AFTER `esic_credit`, ADD `loan_credit` DECIMAL(10,2) NOT NULL AFTER `esic_debit`, ADD `loan_debit` DECIMAL(10,2) NOT NULL AFTER `loan_credit`;
  ALTER TABLE `account_jv` ADD `medical_allowance` DECIMAL(10,2) NOT NULL AFTER `hra_debit`, ADD `education_allowance` DECIMAL(10,2) NOT NULL AFTER `medical_allowance`, ADD `conveyance_credit` DECIMAL(10,2) NOT NULL AFTER `education_allowance`, ADD `special_allowance_credit` DECIMAL(10,2) NOT NULL AFTER `conveyance_credit`, ADD `entertainment_allowence_credit` DECIMAL(10,2) NOT NULL AFTER `special_allowance_credit`, ADD `advance_credit` DECIMAL(10,2) NOT NULL AFTER `entertainment_allowence_credit`, ADD `joining_bonus_credit` DECIMAL(10,2) NOT NULL AFTER `advance_credit`, ADD `incentive_credit` DECIMAL(10,2) NOT NULL AFTER `joining_bonus_credit`, ADD `employee_bonus_credit` DECIMAL(10,2) NOT NULL AFTER `incentive_credit`, ADD `employer_contri_credit` DECIMAL(10,2) NOT NULL AFTER `employee_bonus_credit`;

  ALTER TABLE `prof_tax` ADD `pay_empr_amt` DECIMAL(10,2) NOT NULL AFTER `pay_empl_share_amt`, ADD `pay_emp_amt` DECIMAL(10,2) NOT NULL AFTER `pay_empr_amt`;

  ALTER TABLE `ledger` ADD `epf_debit` DECIMAL(10,2) NOT NULL AFTER `epf_credit`;
  ALTER TABLE `ledger` ADD `bank_id` INT(11) NOT NULL AFTER `bank_debit`;
  ALTER TABLE `ledger` ADD `bank_id_debit` INT(11) NOT NULL AFTER `bank_id`;
  ALTER TABLE `profit_and_loss` ADD `exp_name` VARCHAR(256) NOT NULL AFTER `ledger_name`;
  ALTER TABLE `profit_and_loss` ADD `bad_debts` DECIMAL(10,2) NOT NULL AFTER `other_loss`;
  ALTER TABLE `offers` CHANGE `offer_number` `offer_number` VARCHAR(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL;