-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 20, 2019 at 08:53 AM
-- Server version: 5.7.14
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `eazyinvoice`
--

-- --------------------------------------------------------

--
-- Table structure for table `account`
--

CREATE TABLE `account` (
  `acc_id` int(11) NOT NULL,
  `acc_no` varchar(50) NOT NULL,
  `date` varchar(10) NOT NULL,
  `reg_id` int(11) NOT NULL,
  `bus_id` int(11) NOT NULL,
  `gst_id` int(11) NOT NULL,
  `acc_name` varchar(250) NOT NULL,
  `acc_type` varchar(500) NOT NULL,
  `acc_cat` varchar(20) NOT NULL,
  `notes` varchar(500) NOT NULL,
  `status` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  `createdat` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedat` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `account`
--

INSERT INTO `account` (`acc_id`, `acc_no`, `date`, `reg_id`, `bus_id`, `gst_id`, `acc_name`, `acc_type`, `acc_cat`, `notes`, `status`, `createdat`, `updatedat`) VALUES
(2, 'ACC 000001', '12/06/2019', 73, 152, 93, 'dcxvc', 'Cash Equivalents', 'B', 'xcv', 'Active', '2019-06-12 12:56:20', '2019-06-12 12:56:20'),
(5, 'ACC 000001', '19/06/2019', 73, 152, 92, 'ABC ', 'Accounts receivable/Sundry debtors', 'B', 'hey', 'Active', '2019-06-18 20:32:47', '2019-06-18 20:32:47'),
(6, 'ACC 000002', '19/06/2019', 73, 152, 92, 'XYZ', 'Cash Equivalents', 'B', 'hey again', 'Active', '2019-06-18 20:33:04', '2019-06-18 20:33:04');

-- --------------------------------------------------------

--
-- Table structure for table `account_jv`
--

CREATE TABLE `account_jv` (
  `jvacc_id` int(11) NOT NULL,
  `jv_no` varchar(50) NOT NULL,
  `date` varchar(10) NOT NULL,
  `reg_id` int(11) NOT NULL,
  `bus_id` int(11) NOT NULL,
  `gst_id` int(11) NOT NULL,
  `dacc_name` varchar(250) NOT NULL,
  `debit` varchar(50) NOT NULL,
  `cacc_name` varchar(50) NOT NULL,
  `credit` varchar(20) NOT NULL,
  `narration` varchar(500) NOT NULL,
  `status` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  `createdat` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedat` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `account_jv`
--

INSERT INTO `account_jv` (`jvacc_id`, `jv_no`, `date`, `reg_id`, `bus_id`, `gst_id`, `dacc_name`, `debit`, `cacc_name`, `credit`, `narration`, `status`, `createdat`, `updatedat`) VALUES
(3, 'JV 000003', '11/06/2019', 73, 152, 93, 'vxccv', '1500', 'vxc', '1000', 'xcvxc new', 'Active', '2019-06-11 05:03:06', '2019-06-12 12:47:34'),
(4, 'JV 000001', '19/06/2019', 73, 152, 92, 'ABC ', '2000', 'XYZ', '5000', 'vaahhh', 'Active', '2019-06-18 20:33:33', '2019-06-18 20:33:33');

-- --------------------------------------------------------

--
-- Table structure for table `appraisal`
--

CREATE TABLE `appraisal` (
  `apr_id` int(11) NOT NULL,
  `employee_id` varchar(256) NOT NULL,
  `date_of_appraisal` varchar(10) NOT NULL,
  `reg_id` int(11) NOT NULL,
  `bus_id` int(11) NOT NULL,
  `gst_id` int(11) NOT NULL,
  `employee_name` varchar(250) NOT NULL,
  `employee_designation` varchar(100) NOT NULL,
  `revised_salary_applicable_from` varchar(10) NOT NULL,
  `comments` varchar(500) NOT NULL,
  `training_area` varchar(150) NOT NULL,
  `reporting_manager_name` varchar(40) NOT NULL,
  `reporting_managers_designation` varchar(40) NOT NULL,
  `current_annual_ctc` varchar(50) NOT NULL,
  `revised_annual_ctc` varchar(40) NOT NULL,
  `status` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  `createdat` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedat` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `appraisal`
--

INSERT INTO `appraisal` (`apr_id`, `employee_id`, `date_of_appraisal`, `reg_id`, `bus_id`, `gst_id`, `employee_name`, `employee_designation`, `revised_salary_applicable_from`, `comments`, `training_area`, `reporting_manager_name`, `reporting_managers_designation`, `current_annual_ctc`, `revised_annual_ctc`, `status`, `createdat`, `updatedat`) VALUES
(12, '2', '20/06/2019', 73, 152, 92, 'Mr Vishvam Vyas', 'jhbjh', '13/06/2019', 'sdbhxzc', 'vxcvxc', 'ABCB', 'CEO', '1000', '1500', 'Active', '2019-06-20 08:50:51', '2019-06-20 08:50:51');

-- --------------------------------------------------------

--
-- Table structure for table `appraisal_list_add`
--

CREATE TABLE `appraisal_list_add` (
  `apla_id` int(11) NOT NULL,
  `apr_id` int(11) NOT NULL,
  `particulars_add` varchar(50) NOT NULL,
  `amount_add` varchar(50) NOT NULL,
  `recurring_month_add` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `appraisal_list_add`
--

INSERT INTO `appraisal_list_add` (`apla_id`, `apr_id`, `particulars_add`, `amount_add`, `recurring_month_add`) VALUES
(11, 12, 'zx', '500', 'Yes');

-- --------------------------------------------------------

--
-- Table structure for table `appraisal_list_ded`
--

CREATE TABLE `appraisal_list_ded` (
  `apld_id` int(11) NOT NULL,
  `apr_id` int(11) NOT NULL,
  `particulars_ded` varchar(50) NOT NULL,
  `amount_ded` varchar(50) NOT NULL,
  `recurring_month_ded` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `appraisal_list_ded`
--

INSERT INTO `appraisal_list_ded` (`apld_id`, `apr_id`, `particulars_ded`, `amount_ded`, `recurring_month_ded`) VALUES
(12, 12, 'vxxcv', '1000', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `company_expense`
--

CREATE TABLE `company_expense` (
  `exp_id` int(11) NOT NULL,
  `bus_id` int(11) NOT NULL,
  `reg_id` int(11) NOT NULL,
  `gst_id` int(11) NOT NULL,
  `ce_number` varchar(100) DEFAULT NULL,
  `ce_date` varchar(10) NOT NULL,
  `ce_name` varchar(20) NOT NULL,
  `ce_category` varchar(20) NOT NULL,
  `ce_refbillno` varchar(10) NOT NULL,
  `ce_refbilldate` varchar(10) NOT NULL,
  `ce_refpono` varchar(15) NOT NULL,
  `ce_refpodate` varchar(10) NOT NULL,
  `ce_nature` varchar(10) NOT NULL,
  `ce_recurdate` varchar(10) NOT NULL,
  `ce_no_of_recur` int(3) NOT NULL,
  `ce_recurfreq` int(3) NOT NULL,
  `ce_vendorname` varchar(20) NOT NULL,
  `ce_gstin` varchar(20) NOT NULL,
  `ce_supplyplace` varchar(30) NOT NULL,
  `supply_curr` varchar(20) NOT NULL,
  `appl_tds` int(2) NOT NULL,
  `ce_tdstype` varchar(35) NOT NULL,
  `ce_tdshead` varchar(5) NOT NULL,
  `ce_tdspercent` decimal(3,2) NOT NULL,
  `ce_tdsamt` decimal(15,2) DEFAULT NULL,
  `ce_status` varchar(100) NOT NULL,
  `ce_mode` varchar(100) DEFAULT NULL,
  `alert_jvou` int(2) NOT NULL,
  `ce_alertdate` varchar(10) NOT NULL,
  `equa_levy` int(2) NOT NULL,
  `ce_eqhead` varchar(10) NOT NULL,
  `ce_eqpercent` decimal(3,0) NOT NULL,
  `ce_eq_taxableamt` decimal(15,0) NOT NULL,
  `ce_discount_total` decimal(15,2) DEFAULT NULL,
  `ce_taxable_total` decimal(15,2) DEFAULT NULL,
  `ce_cgst_total` decimal(15,2) DEFAULT NULL,
  `ce_sgst_total` decimal(15,2) DEFAULT NULL,
  `ce_igst_total` decimal(15,2) NOT NULL,
  `ce_cess_total` decimal(15,2) DEFAULT NULL,
  `ce_other_total` decimal(15,2) DEFAULT NULL,
  `ce_grandtotal` decimal(15,2) DEFAULT NULL,
  `ce_dollar_number` int(11) DEFAULT NULL,
  `ce_inr_value` decimal(15,2) DEFAULT NULL,
  `ce_paid` decimal(15,2) NOT NULL,
  `ce_pending_amt` decimal(15,2) NOT NULL,
  `ce_upload` varchar(250) DEFAULT NULL,
  `ce_notes` text NOT NULL,
  `status` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  `createdat` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedat` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `company_expense`
--

INSERT INTO `company_expense` (`exp_id`, `bus_id`, `reg_id`, `gst_id`, `ce_number`, `ce_date`, `ce_name`, `ce_category`, `ce_refbillno`, `ce_refbilldate`, `ce_refpono`, `ce_refpodate`, `ce_nature`, `ce_recurdate`, `ce_no_of_recur`, `ce_recurfreq`, `ce_vendorname`, `ce_gstin`, `ce_supplyplace`, `supply_curr`, `appl_tds`, `ce_tdstype`, `ce_tdshead`, `ce_tdspercent`, `ce_tdsamt`, `ce_status`, `ce_mode`, `alert_jvou`, `ce_alertdate`, `equa_levy`, `ce_eqhead`, `ce_eqpercent`, `ce_eq_taxableamt`, `ce_discount_total`, `ce_taxable_total`, `ce_cgst_total`, `ce_sgst_total`, `ce_igst_total`, `ce_cess_total`, `ce_other_total`, `ce_grandtotal`, `ce_dollar_number`, `ce_inr_value`, `ce_paid`, `ce_pending_amt`, `ce_upload`, `ce_notes`, `status`, `createdat`, `updatedat`) VALUES
(65, 152, 73, 93, 'CEXP000005', '06/06/2019', '68', '5', '', '', '', '', '', '', 0, 0, '12', '', '', '', 1, '1', '2', '3.00', '100.00', '', NULL, 0, '', 0, '', '0', '0', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 0, '0.00', '0.00', '0.00', NULL, '', 'Active', '2019-06-06 11:02:05', '2019-06-15 17:31:59'),
(66, 152, 73, 93, 'CEXP000006', '06/06/2019', '68', '5', '', '', '', '', '', '', 0, 0, '12', '', '', '', 1, '1', '2', '3.00', '200.00', '', NULL, 0, '', 0, '', '0', '0', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 0, '0.00', '0.00', '0.00', NULL, '', 'Active', '2019-06-06 11:04:38', '2019-06-15 17:32:04'),
(67, 152, 73, 93, 'CEXP000007', '12/06/2019', '67', '1', 'asd', '02/06/2019', '1452', '04/06/2019', 'recurring', '17/06/2019', 3, 0, '12', '1', '27', '1', 1, '1', '2', '3.00', '300.00', '', NULL, 0, '', 0, '', '0', '0', '20.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 0, '0.00', '0.00', '0.00', NULL, 'hello', 'Active', '2019-06-12 04:56:19', '2019-06-15 17:32:52'),
(70, 152, 73, 92, 'CEXP000007', '13/06/2019', '69', '6', '', '', '', '', '', '', 0, 0, '12', '', '', '', 1, '(0020) Company Deductees', '1', '2.00', '100.00', 'Paid', NULL, 0, '', 0, '', '0', '0', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 0, '0.00', '0.00', '0.00', NULL, '', 'Active', '2019-06-13 04:38:41', '2019-06-17 12:28:22'),
(71, 152, 73, 92, 'CEXP000008', '13/06/2019', '69', '6', '', '', '', '', '', '', 0, 0, '12', '97', '', '', 1, '(0021) Non-Company Deductees', '1', '2.00', '400.00', '', NULL, 0, '', 1, '', '0', '0', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 0, '0.00', '0.00', '0.00', NULL, 'hey', 'Active', '2019-06-13 13:38:45', '2019-06-17 12:28:49'),
(72, 152, 73, 92, 'CEXP000009', '14/06/2019', '69', '6', 'ads', '', '', '', 'onetime', '', 0, 0, '12', '97', '', '', 1, '(0021) Non-Company Deductees', '1', '2.00', '500.00', '', NULL, 0, '', 1, '', '0', '0', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 0, '0.00', '0.00', '0.00', NULL, 'heyy', 'Active', '2019-06-14 06:13:39', '2019-06-19 07:43:39'),
(73, 152, 73, 92, 'CEXP000010', '17/06/2019', '69', '6', '', '', '', '', '', '', 0, 0, '12', '', '', '', 1, '(0020) Company Deductees', '1', '2.00', '600.00', '', NULL, 0, '', 0, '', '0', '0', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 0, '0.00', '0.00', '0.00', NULL, '', 'Active', '2019-06-17 07:14:53', '2019-06-19 07:43:44');

-- --------------------------------------------------------

--
-- Table structure for table `company_expense_list`
--

CREATE TABLE `company_expense_list` (
  `cel_id` int(11) NOT NULL,
  `exp_id` int(11) NOT NULL,
  `ce_particulars` varchar(250) NOT NULL,
  `ce_hsn` varchar(200) NOT NULL,
  `ce_qty` decimal(15,2) DEFAULT NULL,
  `ce_rate` decimal(15,2) DEFAULT NULL,
  `ce_discount` decimal(15,2) DEFAULT NULL,
  `ce_taxable_amt` decimal(15,2) DEFAULT NULL,
  `ce_cgst` decimal(15,2) DEFAULT NULL,
  `ce_cgst_amt` decimal(15,2) DEFAULT NULL,
  `ce_sgst` decimal(15,2) DEFAULT NULL,
  `ce_sgst_amt` decimal(15,2) DEFAULT NULL,
  `ce_igst_amt` decimal(15,2) NOT NULL,
  `ce_igst` decimal(15,2) NOT NULL,
  `ce_cess_amt` decimal(15,2) DEFAULT NULL,
  `ce_cess` decimal(15,2) DEFAULT NULL,
  `ce_other_amt` decimal(15,2) DEFAULT NULL,
  `ce_other` decimal(15,2) DEFAULT NULL,
  `ce_total_amt` decimal(15,2) DEFAULT NULL,
  `status` enum('Active','Inactive') NOT NULL,
  `createdat` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedat` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `company_expense_list`
--

INSERT INTO `company_expense_list` (`cel_id`, `exp_id`, `ce_particulars`, `ce_hsn`, `ce_qty`, `ce_rate`, `ce_discount`, `ce_taxable_amt`, `ce_cgst`, `ce_cgst_amt`, `ce_sgst`, `ce_sgst_amt`, `ce_igst_amt`, `ce_igst`, `ce_cess_amt`, `ce_cess`, `ce_other_amt`, `ce_other`, `ce_total_amt`, `status`, `createdat`, `updatedat`) VALUES
(1, 0, 'sd', '123654', '0.00', '0.00', '0.00', '0.00', '6.00', '0.00', '6.00', '0.00', '0.00', '0.00', '0.00', '12.00', '0.00', '0.00', '0.00', 'Active', '2019-06-06 11:02:05', '2019-06-06 11:02:05'),
(2, 66, 'zfd', '123654', '0.00', '0.00', '0.00', '0.00', '6.00', '0.00', '6.00', '0.00', '0.00', '0.00', '0.00', '12.00', '0.00', '0.00', '0.00', 'Active', '2019-06-06 11:04:38', '2019-06-06 11:04:38'),
(3, 66, 'dsfsd', '', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'Active', '2019-06-06 11:04:38', '2019-06-06 11:04:38'),
(4, 67, 'vxcv', '1452', '2.00', '20.00', '10.00', '0.00', '3.00', '0.00', '3.00', '0.00', '0.00', '0.00', '0.00', '2.00', '0.00', '0.00', '0.00', 'Active', '2019-06-12 04:56:19', '2019-06-12 04:56:19'),
(5, 67, 'xvcx', '', '2.00', '20.00', '10.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'Active', '2019-06-12 04:56:19', '2019-06-12 04:56:19'),
(6, 68, 'xcv', '', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'Active', '2019-06-12 11:40:16', '2019-06-12 11:40:16'),
(7, 69, 'xc', '1452', '0.00', '0.00', '0.00', '0.00', '6.00', '0.00', '6.00', '0.00', '0.00', '0.00', '0.00', '13.00', '0.00', '0.00', '0.00', 'Active', '2019-06-12 11:42:20', '2019-06-12 11:42:20'),
(8, 70, 'bn', '1452', '0.00', '0.00', '0.00', '0.00', '6.00', '0.00', '6.00', '0.00', '0.00', '0.00', '0.00', '13.00', '0.00', '0.00', '0.00', 'Active', '2019-06-13 04:38:42', '2019-06-13 04:38:42'),
(9, 71, 'asc', '1452', '0.00', '0.00', '0.00', '0.00', '6.00', '12.50', '6.00', '12.00', '1.00', '0.75', '0.00', '13.00', '0.00', '0.00', '0.00', 'Active', '2019-06-13 13:38:45', '2019-06-18 18:59:24'),
(10, 71, 'Equalisation Levy', '1452', '0.00', '0.00', '0.00', '9.00', '6.00', '1.00', '6.00', '1.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'Active', '2019-06-13 13:38:45', '2019-06-13 13:38:45'),
(11, 72, 'zxc', '1452', '0.00', '0.00', '0.00', '0.00', '6.00', '0.00', '6.00', '0.00', '0.00', '0.00', '0.00', '13.00', '0.00', '0.00', '0.00', 'Active', '2019-06-14 06:13:40', '2019-06-14 06:13:40'),
(12, 72, 'Equalisation Levy', '1452', '0.00', '0.00', '0.00', '9.00', '6.00', '1.00', '6.00', '1.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'Active', '2019-06-14 06:13:40', '2019-06-14 06:13:40'),
(13, 73, 'cvx', '145', '0.00', '0.00', '0.00', '0.00', '9.00', '0.00', '9.00', '0.00', '0.00', '0.00', '0.00', '3.00', '0.00', '0.00', '0.00', 'Active', '2019-06-17 07:14:53', '2019-06-17 07:14:53'),
(14, 74, 'dggf', '1452', '0.00', '0.00', '0.00', '0.00', '6.00', '0.00', '6.00', '0.00', '0.00', '0.00', '0.00', '13.00', '0.00', '0.00', '0.00', 'Active', '2019-06-18 16:04:46', '2019-06-18 16:04:46'),
(19, 79, 'xc', '1452', '0.00', '0.00', '0.00', '0.00', '6.00', '0.00', '6.00', '0.00', '0.00', '0.00', '0.00', '13.00', '0.00', '0.00', '0.00', 'Active', '2019-06-18 17:13:33', '2019-06-18 17:13:33'),
(20, 80, 'dfsfsd', '14253', '100.00', '10.00', '100.00', '900.00', '3.00', '0.00', '3.00', '0.00', '0.00', '0.00', '54.00', '6.00', '0.00', '0.00', '0.00', 'Active', '2019-06-20 04:27:40', '2019-06-20 04:27:40'),
(21, 80, 'Equalisation Levy', '14253', '0.00', '0.00', '0.00', '9.00', '3.00', '0.27', '3.00', '0.27', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 'Active', '2019-06-20 04:27:40', '2019-06-20 04:27:40');

-- --------------------------------------------------------

--
-- Table structure for table `employee_master`
--

CREATE TABLE `employee_master` (
  `emp_id` int(11) NOT NULL,
  `reg_id` int(11) NOT NULL,
  `bus_id` int(11) NOT NULL,
  `status` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  `createdat` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedat` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `title` varchar(25) NOT NULL,
  `first_name` varchar(25) NOT NULL,
  `last_name` varchar(25) NOT NULL,
  `permanent_address` varchar(25) NOT NULL,
  `city` varchar(25) NOT NULL,
  `pincode` varchar(25) NOT NULL,
  `local_address` varchar(25) NOT NULL,
  `birthday` varchar(25) NOT NULL,
  `birthday_reminder` varchar(25) NOT NULL,
  `anniversary` varchar(25) NOT NULL,
  `anniversary_reminder` varchar(25) NOT NULL,
  `employee_id` varchar(25) NOT NULL,
  `employee_email_id` varchar(25) NOT NULL,
  `employee_mobile_no` varchar(25) NOT NULL,
  `blood_group` varchar(25) NOT NULL,
  `allergies` varchar(25) NOT NULL,
  `medical_concern` varchar(25) NOT NULL,
  `emergency_contact_name` varchar(25) NOT NULL,
  `emergency_contact_number` varchar(25) NOT NULL,
  `designation` varchar(25) NOT NULL,
  `department` varchar(25) NOT NULL,
  `location` varchar(25) NOT NULL,
  `reporting_to` varchar(25) NOT NULL,
  `date_of_joining` varchar(25) NOT NULL,
  `date_of_confirmation` varchar(25) NOT NULL,
  `confirmation_reminder` varchar(25) NOT NULL,
  `date_of_approval` varchar(25) NOT NULL,
  `approval_reminder` varchar(25) NOT NULL,
  `pan` varchar(25) NOT NULL,
  `pf_no` varchar(25) NOT NULL,
  `bank_acc_no` varchar(25) NOT NULL,
  `bank_name` varchar(25) NOT NULL,
  `bank_ifsc_code` varchar(25) NOT NULL,
  `branch_name` varchar(25) NOT NULL,
  `state` varchar(25) NOT NULL,
  `country` varchar(25) NOT NULL,
  `date_of_exit` varchar(25) NOT NULL,
  `emp_profile_image` varchar(250) NOT NULL,
  `gst_id` int(11) NOT NULL,
  `currency` varchar(25) NOT NULL,
  `country_local` varchar(25) NOT NULL,
  `state_local` varchar(25) NOT NULL,
  `city_local` varchar(25) NOT NULL,
  `pincode_local` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `employee_master`
--

INSERT INTO `employee_master` (`emp_id`, `reg_id`, `bus_id`, `status`, `createdat`, `updatedat`, `title`, `first_name`, `last_name`, `permanent_address`, `city`, `pincode`, `local_address`, `birthday`, `birthday_reminder`, `anniversary`, `anniversary_reminder`, `employee_id`, `employee_email_id`, `employee_mobile_no`, `blood_group`, `allergies`, `medical_concern`, `emergency_contact_name`, `emergency_contact_number`, `designation`, `department`, `location`, `reporting_to`, `date_of_joining`, `date_of_confirmation`, `confirmation_reminder`, `date_of_approval`, `approval_reminder`, `pan`, `pf_no`, `bank_acc_no`, `bank_name`, `bank_ifsc_code`, `branch_name`, `state`, `country`, `date_of_exit`, `emp_profile_image`, `gst_id`, `currency`, `country_local`, `state_local`, `city_local`, `pincode_local`) VALUES
(1, 73, 152, 'Active', '2019-06-17 02:06:09', '2019-06-18 15:35:23', 'Mr', 'manoj', '', 'MD/s-72,Sector-G', '5909', '226012', 'MD/s-72,Sector-G', '', '', '', '', '16BCE0955', 'vishvam.vyas2016@vitstude', 'w', 'AB+', 'none', 'none', 'ubuub', '9532998084', 'Student', 'web', 'Maharashtra', 'aaiu', '18/06/2019', '19/06/2019', 'One Week Before', '19/06/2019', 'On That Day', 'w', '1516516', 'chgtcty', 'tydytd', 'TYDYT', 'YEYT', '42', '1', '18/06/2019', '4W858xv1.jpg', 92, '', '1', '42', '5909', '226012'),
(2, 73, 152, 'Active', '2019-06-18 01:10:09', '2019-06-18 12:42:10', 'Mr', 'Vishvam', 'Vyas', 'MD/s-72,Sector-G', '5912', '226012', 'MD/s-72,Sector-G', '', '', '', '', '16BCE0955', 'vishvam.vyas2016@vitstude', 'w', 'AB+', 'bhjh', 'bhbhhbhjbh', 'hbbjhb', 'hhjb', 'jhbjh', 'hjbjhbh', 'Maharashtra', 'aaiu', '24/06/2019', '25/06/2019', 'One Week Before', '20/06/2019', 'One Week Before', 'ca', 'wef ', 'hjbhjBHjhbbhjhbHB', 'HBJBHJBHhbbh', 'hbjbhj', 'hbjbhj', '42', '1', '04/06/2019', '', 92, '1', '1', '42', '5912', '226012'),
(5, 73, 152, 'Active', '2019-06-18 13:04:39', '2019-06-18 13:31:10', 'Mr', 'Vishvam', 'Vyas', 'MD/s-72,Sector-G', '5912', '226012', 'MD/s-72,Sector-G', '', '', '', '', 'g', 'vishvam.vyas2016@vitstude', '8765387241', 'AB-', 'none', 'bhbhhbhjbh', 'Yashesh Vyas', '9532998084', 'Student', 'ker', 'Maharashtra', 'aaiu', '12/06/2019', '13/06/2019', 'One Week Before', '12/06/2019', 'On That Day', 'hjbjhbv', 'bjhbh', 'jbjhb', 'jhbjhb', 'jhb', 'jhb', '42', '1', '04/06/2019', '7d96db35f23f02a77e9b6cb5dff629f8.jpg', 92, 'Afghanis ?', '1', '42', '5912', '226012');

-- --------------------------------------------------------

--
-- Table structure for table `expense_list`
--

CREATE TABLE `expense_list` (
  `exp_id` int(11) NOT NULL,
  `exp_code` varchar(256) NOT NULL,
  `exp_date` varchar(10) NOT NULL,
  `reg_id` int(11) NOT NULL,
  `bus_id` int(11) NOT NULL,
  `gst_id` int(11) NOT NULL,
  `exp_name` varchar(250) NOT NULL,
  `exp_category` int(11) NOT NULL,
  `hsn_no` varchar(100) NOT NULL,
  `em_gst` varchar(3) NOT NULL,
  `em_cess` varchar(3) NOT NULL,
  `em_tdstype` varchar(15) NOT NULL,
  `em_tdshead` varchar(15) NOT NULL,
  `em_tdspercent` varchar(3) NOT NULL,
  `notes` text NOT NULL,
  `status` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  `createdat` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedat` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `expense_list`
--

INSERT INTO `expense_list` (`exp_id`, `exp_code`, `exp_date`, `reg_id`, `bus_id`, `gst_id`, `exp_name`, `exp_category`, `hsn_no`, `em_gst`, `em_cess`, `em_tdstype`, `em_tdshead`, `em_tdspercent`, `notes`, `status`, `createdat`, `updatedat`) VALUES
(14, '', '', 2, 66, 0, 'aniket', 1, 'hsn7897895', '', '', '', '', '', 'done', 'Active', '2018-07-11 07:42:19', '2018-07-11 07:42:19'),
(15, '', '', 2, 66, 0, 'aniket', 1, 'hsn7897895', '', '', '', '', '', 'done', 'Active', '2018-07-11 08:12:24', '2018-07-11 08:12:24'),
(16, '', '', 2, 66, 0, 'aniket', 1, 'hsn7897895', '', '', '', '', '', 'done', 'Active', '2018-07-11 08:12:49', '2018-07-11 08:12:49'),
(17, '', '', 2, 66, 0, 'expense testing', 3, '0001', '', '', '', '', '', 'testestestst', 'Active', '2018-07-11 08:16:46', '2018-07-11 08:16:46'),
(18, '', '', 2, 66, 0, 'testing0000000000000001', 2, '003', '', '', '', '', '', 'testttttt', 'Active', '2018-07-11 08:20:09', '2018-07-11 08:28:52'),
(19, '', '', 2, 66, 0, 'testing00000000000022', 2, '7894156', '', '', '', '', '', 'dsadsadsadddddddddddddddddddddddd', 'Active', '2018-07-11 10:11:18', '2018-07-11 10:11:18'),
(24, 'EXP000024', '29/05/2019', 73, 152, 0, 'xv', 4, 'cxv', '12', '12', '2', '1', '12', 'no no', 'Active', '2019-05-24 08:23:48', '2019-05-29 11:02:09'),
(41, 'EXP000025', '28/05/2019', 73, 152, 0, 'ada', 0, '1', '1', '2', '2', '1', '1', 'sc', 'Active', '2019-05-28 07:46:07', '2019-05-28 07:46:07'),
(48, 'EXP000026', '29/05/2019', 73, 152, 0, 'abc', 5, '123', '12', '10', '2', '2', '8', 'hello', 'Active', '2019-05-29 13:35:51', '2019-05-29 13:35:51'),
(49, 'EXP000027', '30/05/2019', 73, 152, 0, 'xvc', 6, '123654', '18', '12', 'vx', '12', '2', 'xvc', 'Active', '2019-05-30 05:10:53', '2019-05-30 05:10:53'),
(50, 'EXP000028', '03/06/2019', 73, 152, 0, 'xvz', 3, '123654', '12', '12', '1', '2', '12', 'hello heya', 'Active', '2019-05-31 06:01:00', '2019-06-03 12:35:10'),
(54, 'EXP000029', '03/06/2019', 73, 152, 0, 'sdf', 1, 'sdf', '12', '1', 'zxc', '2', '2', 'xvx', 'Active', '2019-06-03 11:23:11', '2019-06-03 11:23:11'),
(67, 'EXP000030', '03/06/2019', 73, 152, 0, 'Latest', 1, '1452', '5', '2', '1', '2', '3', 'Last check', 'Active', '2019-06-03 12:37:12', '2019-06-03 12:37:41'),
(68, 'EXP000031', '06/06/2019', 73, 152, 0, 'last', 5, '123654', '12', '12', '1', '2', '3', 'hello', 'Active', '2019-06-06 10:15:31', '2019-06-17 10:57:14'),
(69, 'EXP000032', '12/06/2019', 73, 152, 92, 'abcd', 6, '1452', '12', '13', '12', '1', '2', 'hey', 'Active', '2019-06-12 05:06:34', '2019-06-16 12:04:15'),
(72, 'EXP000001', '12/06/2019', 73, 152, 93, 'zx', 0, '', '', '', '', '', '', '', 'Active', '2019-06-12 10:51:52', '2019-06-12 10:51:52'),
(73, 'EXP000033', '14/06/2019', 73, 152, 92, 'xvc', 0, '14', '6', '6', '', '1', '2', 'zc', 'Active', '2019-06-14 11:35:00', '2019-06-18 11:14:45'),
(74, 'EXP000034', '16/06/2019', 73, 152, 92, 'new expense', 8, '145', '18', '3', '', '1', '3', 'hey chnge', 'Active', '2019-06-16 12:21:47', '2019-06-18 11:14:49'),
(76, 'EXP000035', '18/06/2019', 73, 152, 92, 'Office Rent', 3, '14253', '6', '6', '', '1', '3', 'fddf', 'Active', '2019-06-18 16:01:38', '2019-06-18 16:01:38');

-- --------------------------------------------------------

--
-- Table structure for table `expense_list_tax`
--

CREATE TABLE `expense_list_tax` (
  `exp_id` varchar(10) NOT NULL,
  `stax_name` varchar(15) DEFAULT NULL,
  `stax_percentage` varchar(3) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `expense_list_tax`
--

INSERT INTO `expense_list_tax` (`exp_id`, `stax_name`, `stax_percentage`) VALUES
('20', 'cst', '2'),
('', '', ''),
('EXP000001', '1', '1'),
('33', 'xvx', '12'),
('EXP000025', 'c', '11'),
('35', 'cx', 'cx'),
('37', 'xz', '12'),
('41', NULL, NULL),
('42', 'as', '12'),
('43', '12', '12'),
('44', 'cvx12', '12'),
('45', '12', '12'),
('46', '12', '12'),
('47', '12', '12'),
('64', 'zv', '12'),
('64', 'zvas', '11'),
('65', 'aa', '1'),
('65', 'bb', '2'),
('66', 'zx', '12'),
('67', 'ab', '2'),
('67', 'abcd', '4'),
('68', 'asd', '12'),
('68', 'asd', '12'),
('69', 'adc', '12'),
('73', 'xc', '12'),
('74', 'vat', '3'),
('74', 'sas', '4'),
('74', 'serv', '3');

-- --------------------------------------------------------

--
-- Table structure for table `tds`
--

CREATE TABLE `tds` (
  `tds_id` int(11) NOT NULL,
  `tds_head` varchar(500) NOT NULL,
  `tds_section` varchar(5) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tds`
--

INSERT INTO `tds` (`tds_id`, `tds_head`, `tds_section`) VALUES
(1, '193 - Interest on Securities', '193'),
(2, '194 - Dividend', '194'),
(3, '195 - Other sums payable to a non-resident', '195'),
(4, '4BA - Certain income from units of a business trust', '196'),
(5, '4BB - Winning from Horse race', '4BB'),
(6, '4DA - Payment in respect of life insurance policy', '4DA'),
(7, '4EE - Payments in respect of Deposits under National Saving Schemes', '4EE'),
(8, '4LA - Payment of Compensation on Acquisition of Certain Immovable property', '4LA'),
(9, '4LB - Income by way of interest from infrastructure debt fund payable to a non-resident', '4LB'),
(10, '4LC - Income by way of interest from specified Company payable to a non-resident', '4LC'),
(11, '4LD - Interest on Rupee denominated bond of Company or Government Securities', '4LD'),
(12, '6CA - Alcoholic liquor for human consumption', '6CA'),
(13, '6CB - Timber obtained under forest lease', '6CB'),
(14, '6CC - Timber obtained other than forest lease', '6CC'),
(15, '6CD - Any other forest produce not being timber or tendu leaves', '6CD'),
(16, '6CE - Scrap', '6CE'),
(17, '6CF - Parking Lot', '6CF'),
(18, '6CG - Toll Plaza', '6CG'),
(19, '6CH - Mining and Quarrying', '6CH'),
(20, '6CI - Tendu Leaves', '6CI'),
(21, '6CJ - Minerals', '6CJ'),
(22, '6CK - Bullion and Jewellery', '6CK'),
(23, '92A - Payment to Govt. Employees other than Union Govt. employees', '92A'),
(24, '92B - Payment of Employees other than Govt. Employees', '92B'),
(25, '94A - Interest other than Interest on Securities', '94A'),
(26, '94B - Winning from lotteries and crossword puzzles', '94B'),
(27, '94C - Payment of contractors and sub-contractors', '94C'),
(28, '94D - Insurance commission', '94D'),
(29, '94E - Payments to non-resident Sportsmen/Sport Associations', '94E'),
(30, '94F - Payments on account of Re-purchase of Units by Mutual Funds of UTI', '94F'),
(31, '94G - Commission,prize etc. on sale of Lottery tickets', '94G'),
(32, '94H - Commission or Brokerage', '94H'),
(33, '94I - Rent', '94I'),
(34, '94J - Fees for Professional or Technical Services', '94J'),
(35, '94K - Income Payable to a resident assessee in respect of units of a specified Mutual Fund or of the Units of the UTI', '94K'),
(36, '96A - Income in respect of Units of non-residents', '96A'),
(37, '96B - Payments in respect of Units to an Offshore Fund', '96B'),
(38, '96C - Income from foreign currency Bonds or Shares of Indian Company payable to a non-resident', '96C'),
(39, '96D - Income of Foreign Institutional investors from securities', '96D'),
(40, '2AA - Payment of accumulated balance due to an employee', '2AA'),
(41, 'LBB - Income in respect of units of investment fund', 'LBB'),
(42, '6CL - TCS on sale of Motor vehicle', '6CL'),
(43, '6CM - TCS on sale in cash of any goods (other than bullion/jewellery)', '6CM'),
(44, '6CN - TCS on providing of any services (other than Ch-XVII-B)', '6CN'),
(45, 'LBC - Income in respect of investment in securitization trust', 'LBC'),
(46, '4IC - Payment under specified agreement', '4IC');

-- --------------------------------------------------------

--
-- Table structure for table `tds_payment`
--

CREATE TABLE `tds_payment` (
  `tdspay_id` int(11) NOT NULL,
  `tds_pay_no` varchar(256) NOT NULL,
  `tds_pay_date` varchar(10) NOT NULL,
  `reg_id` int(11) NOT NULL,
  `bus_id` int(11) NOT NULL,
  `gst_id` int(11) NOT NULL,
  `tds_pay_type` varchar(35) NOT NULL,
  `tds_pay_head` varchar(250) NOT NULL,
  `tds_from_date` varchar(15) NOT NULL,
  `tds_to_date` varchar(15) NOT NULL,
  `tds_pay_receiptno` varchar(30) NOT NULL,
  `tds_pay_receiptchdate` varchar(10) NOT NULL,
  `bsr_code_no` varchar(30) NOT NULL,
  `challan_pic` varchar(250) DEFAULT NULL,
  `pay_date` varchar(10) NOT NULL,
  `pay_status` varchar(20) NOT NULL,
  `pay_mode` varchar(15) NOT NULL,
  `pay_bank_id` varchar(30) NOT NULL,
  `pay_amount` varchar(30) NOT NULL,
  `int_charge` varchar(30) NOT NULL,
  `pen_charge` varchar(30) NOT NULL,
  `pay_other` varchar(30) NOT NULL,
  `notes` text NOT NULL,
  `status` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  `createdat` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedat` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tds_payment`
--

INSERT INTO `tds_payment` (`tdspay_id`, `tds_pay_no`, `tds_pay_date`, `reg_id`, `bus_id`, `gst_id`, `tds_pay_type`, `tds_pay_head`, `tds_from_date`, `tds_to_date`, `tds_pay_receiptno`, `tds_pay_receiptchdate`, `bsr_code_no`, `challan_pic`, `pay_date`, `pay_status`, `pay_mode`, `pay_bank_id`, `pay_amount`, `int_charge`, `pen_charge`, `pay_other`, `notes`, `status`, `createdat`, `updatedat`) VALUES
(2, 'TDSP000001', '17/06/2019', 73, 152, 92, '(0020) Company Deductees', '1', '11/06/2019', '18/06/2019', '14523', '17/06/2019', '125654', NULL, '17/06/2019', 'Paid', 'cash', '', '1000', '100', '20', '10', 'tds', 'Active', '2019-06-17 11:14:49', '2019-06-19 13:50:15'),
(4, 'TDSP000002', '19/06/2019', 73, 152, 92, '(0021) Non-Company Deductees', '1', '09/06/2019', '18/06/2019', '12546', '19/06/2019', '125632', NULL, '19/06/2019', 'Paid', 'cash', '', '1000', '0', '100', '0', '', 'Active', '2019-06-19 12:18:41', '2019-06-19 13:50:21');

-- --------------------------------------------------------

--
-- Table structure for table `tds_payment_list`
--

CREATE TABLE `tds_payment_list` (
  `tdslist_id` int(11) NOT NULL,
  `tdspay_id` int(11) NOT NULL,
  `tds_pay_receiptno` varchar(30) NOT NULL,
  `tds_pay_receiptchdate` varchar(10) NOT NULL,
  `bsr_code_no` varchar(30) NOT NULL,
  `challan_pic` varchar(250) DEFAULT NULL,
  `status` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  `createdat` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedat` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tds_payment_list`
--

INSERT INTO `tds_payment_list` (`tdslist_id`, `tdspay_id`, `tds_pay_receiptno`, `tds_pay_receiptchdate`, `bsr_code_no`, `challan_pic`, `status`, `createdat`, `updatedat`) VALUES
(1, 62, '1452', '12/06/2019', '1236', NULL, 'Active', '2019-06-12 05:03:22', '2019-06-12 05:03:22'),
(2, 62, '1452', '10/06/2019', '1452', NULL, 'Active', '2019-06-12 05:03:22', '2019-06-12 05:03:22'),
(3, 63, 'cvb', '12/06/2019', '', NULL, 'Active', '2019-06-12 12:04:34', '2019-06-12 12:04:34'),
(4, 64, '', '12/06/2019', '', NULL, 'Active', '2019-06-12 12:06:24', '2019-06-12 12:06:24'),
(5, 65, 'cvb', '15/06/2019', 'cv', NULL, 'Active', '2019-06-15 07:07:18', '2019-06-15 07:07:18');

-- --------------------------------------------------------

--
-- Table structure for table `tds_payment_list_details`
--

CREATE TABLE `tds_payment_list_details` (
  `exp_id` int(11) NOT NULL,
  `tdspay_id` int(11) NOT NULL,
  `com_exp_no` varchar(12) NOT NULL,
  `tds_pay_amt` varchar(20) NOT NULL,
  `tds_per` varchar(5) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tds_payment_list_details`
--

INSERT INTO `tds_payment_list_details` (`exp_id`, `tdspay_id`, `com_exp_no`, `tds_pay_amt`, `tds_per`) VALUES
(70, 2, 'CEXP000007', '100', '2.00'),
(73, 2, 'CEXP000010', '600', '2.00'),
(72, 4, 'CEXP000009', '500.00', '2.00'),
(71, 4, 'CEXP000008', '400.00', '2.00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `account`
--
ALTER TABLE `account`
  ADD PRIMARY KEY (`acc_id`);

--
-- Indexes for table `account_jv`
--
ALTER TABLE `account_jv`
  ADD PRIMARY KEY (`jvacc_id`);

--
-- Indexes for table `appraisal`
--
ALTER TABLE `appraisal`
  ADD PRIMARY KEY (`apr_id`);

--
-- Indexes for table `appraisal_list_add`
--
ALTER TABLE `appraisal_list_add`
  ADD PRIMARY KEY (`apla_id`);

--
-- Indexes for table `appraisal_list_ded`
--
ALTER TABLE `appraisal_list_ded`
  ADD PRIMARY KEY (`apld_id`);

--
-- Indexes for table `company_expense`
--
ALTER TABLE `company_expense`
  ADD PRIMARY KEY (`exp_id`);

--
-- Indexes for table `company_expense_list`
--
ALTER TABLE `company_expense_list`
  ADD PRIMARY KEY (`cel_id`);

--
-- Indexes for table `employee_master`
--
ALTER TABLE `employee_master`
  ADD PRIMARY KEY (`emp_id`);

--
-- Indexes for table `expense_list`
--
ALTER TABLE `expense_list`
  ADD PRIMARY KEY (`exp_id`);

--
-- Indexes for table `tds`
--
ALTER TABLE `tds`
  ADD PRIMARY KEY (`tds_id`);

--
-- Indexes for table `tds_payment`
--
ALTER TABLE `tds_payment`
  ADD PRIMARY KEY (`tdspay_id`);

--
-- Indexes for table `tds_payment_list`
--
ALTER TABLE `tds_payment_list`
  ADD PRIMARY KEY (`tdslist_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `account`
--
ALTER TABLE `account`
  MODIFY `acc_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `account_jv`
--
ALTER TABLE `account_jv`
  MODIFY `jvacc_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `appraisal`
--
ALTER TABLE `appraisal`
  MODIFY `apr_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `appraisal_list_add`
--
ALTER TABLE `appraisal_list_add`
  MODIFY `apla_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `appraisal_list_ded`
--
ALTER TABLE `appraisal_list_ded`
  MODIFY `apld_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `company_expense`
--
ALTER TABLE `company_expense`
  MODIFY `exp_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=81;
--
-- AUTO_INCREMENT for table `company_expense_list`
--
ALTER TABLE `company_expense_list`
  MODIFY `cel_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `employee_master`
--
ALTER TABLE `employee_master`
  MODIFY `emp_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `expense_list`
--
ALTER TABLE `expense_list`
  MODIFY `exp_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=77;
--
-- AUTO_INCREMENT for table `tds`
--
ALTER TABLE `tds`
  MODIFY `tds_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;
--
-- AUTO_INCREMENT for table `tds_payment`
--
ALTER TABLE `tds_payment`
  MODIFY `tdspay_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tds_payment_list`
--
ALTER TABLE `tds_payment_list`
  MODIFY `tdslist_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
