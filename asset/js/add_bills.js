$(document).ready(function(){
	$(".edit_bs_billing_add").on("click",function(){
		if($('#customer_name').val() == ''){
			Materialize.toast('Please Select Customer First!!', 2000,'red rounded');
		} else {
				$('#bs_edit_billing_address').modal('open');
				$.ajax({
					url:base_url+'My_invoices/get_all_countries',
					type:"POST",
					data:{'csrf_test_name':csrf_hash,},
					success:function(res){
						$("#bs_edit_billing_address #new_bs_country").html(res);
						$("#bs_edit_billing_address #new_bs_country").parents('.input-field').addClass('label-active');
						$('#bs_edit_billing_address #new_bs_country').material_select();
						},
					});
					$("#new_bs_country").on("change",function(){
						var country_id = $(this).val();
						$.ajax({
							url:base_url+'Company_profile/get_states',
							type:"POST",
							data:{'csrf_test_name':csrf_hash,'country_id':country_id},
							success:function(res){
								$("#new_bs_state").html(res);
								$("#new_bs_state").parents('.input-field').addClass('label-active');
								$('#new_bs_state').material_select();
							},
						});
					});
					$("#new_bs_state").on("change",function(){
						var state_id = $(this).val();
						$.ajax({
							url:base_url+'Company_profile/get_cities',
							type:"POST",
							data:{'csrf_test_name':csrf_hash,'state_id':state_id},
							success:function(res){
							 $("#new_bs_city").html(res);
							 $("#new_bs_city").parents('.input-field').addClass('label-active');
							 $('#new_bs_city').material_select();
							},
						});
					});
			}
		});
		
		$(".edit_bs_shipping_add").on("click",function(){
			if($('#bs_customer_id').val() == ''){
				Materialize.toast('Please Select Customer First!!', 2000,'red rounded');
			} else {
				$('#bs_edit_shipping_address').modal('open');
				$.ajax({
					url:base_url+'My_invoices/get_all_countries',
					type:"POST",
					data:{'csrf_test_name':csrf_hash,},
					success:function(res){
						$("#bs_edit_shipping_address #new_bs_shipping_country").html(res);
						$("#bs_edit_shipping_address #new_bs_shipping_country").parents('.input-field').addClass('label-active');
						$('#bs_edit_shipping_address #new_bs_shipping_country').material_select();
						},
					});
					$("#new_bs_shipping_country").on("change",function(){
						var country_id = $(this).val();
						$.ajax({
							url:base_url+'Company_profile/get_states',
							type:"POST",
							data:{'csrf_test_name':csrf_hash,'country_id':country_id},
							success:function(res){
								$("#new_bs_shipping_state").html(res);
								$("#new_bs_shipping_state").parents('.input-field').addClass('label-active');
								$('#new_bs_shipping_state').material_select();
							},
						});
					});
					$("#new_bs_shipping_state").on("change",function(){
						var state_id = $(this).val();
						$.ajax({
							url:base_url+'Company_profile/get_cities',
							type:"POST",
							data:{'csrf_test_name':csrf_hash,'state_id':state_id},
							success:function(res){
							 $("#new_bs_shipping_city").html(res);
							 $("#new_bs_shipping_city").parents('.input-field').addClass('label-active');
							 $('#new_bs_shipping_city').material_select();
							},
						});
					});
				}
			});
	$("#edit_bs_billing_address_form").submit(function(e){
				e.preventDefault();
			}).validate({
				rules:{
					new_bs_billing_address:{
						required:true
					},
				},
				messages:{
					new_bs_billing_address:{
						required:"Billing Address is required",
					},
				},
				submitHandler:function(form){
						var frm=$(form).serialize();
								$.ajax({
								url:base_url+'My_bills/update_billing_address',
								type:"POST",
								data:{'csrf_test_name':csrf_hash,"frm":frm,'same-as':$("#same-as-bill-supply").val(),},
								success:function(res){
									$('.billing_address span.bill_add').html(res);
									$('#bs_edit_billing_address').modal('close');
									Materialize.toast('New billing address set', 2000,'green rounded');
							},						
					});
				},
			});
			
			$("#edit_bs_shipping_address_form").submit(function(e){
				e.preventDefault();
			}).validate({
				rules:{
					new_bs_shipping_address:{
						required:true
					},
				},
				messages:{
					new_dc_shipping_address:{
						required:"Shipping Address is required",
					},
				},
				submitHandler:function(form){
						var frm=$(form).serialize();
								$.ajax({
								url:base_url+'My_bills/update_shipping_address',
								type:"POST",
								data:{'csrf_test_name':csrf_hash,"frm":frm,},
								success:function(res){
									$('.shipping_address span.ship_add').html(res);
									$('#bs_edit_shipping_address').modal('close');
									Materialize.toast('New Shipping Address have been set!!', 2000,'green rounded');
							},						
					});
				},
			});
	$('.submit_type').on('click' , function ()  {
		var submit_type = $(this).data('submit_type');	
		$('#btn_submit_type').val(submit_type);
		return true;
	});
	$("#create_bills").validate();
	$(window).load(function() {
		var company_profile = $('#header_company_profiles').val();
		var company_gst = $('#branch_gst_list').val();
		$('#company_profile').val(company_profile);
		$('#company_gst').val(company_gst);
    });
	$('#customer_name').change(function(){
	var customer_id=$(this).val();
	var company_id=$("#header_company_profiles").val();
	$.ajax({
				url:base_url+'My_bills/get_customer_gst_info',
				type:"POST",
				data:{'csrf_test_name':csrf_hash,"customer_id":customer_id,},
				success:function(res){
					 var res = $.parseJSON(res);
					$("#customer_gstin_bs").html(res[0]);
					$('#customer_gstin_bs').material_select();
					$("#customer_place").html(res[1]);
					$('#customer_place').material_select();
					$("#po_number").html(res[2]);
					$('#po_number').material_select();
					$("#invoice_no").html(res['invoice_no']);
					$('#invoice_no').material_select();
					$('.billing_address span.bill_add').html(res[4]);
					if($("#same-as-bill-supply").is(':checked')){
						$('.shipping_address span.ship_add').html(res[4]);
					}
					else{
						$('.shipping_address span.ship_add').html(res[5]);
					}
				},
				complete:function(res){
							$.ajax({
								url:base_url+'My_bills/get_gst_value',
								type:"POST",
								data:{'csrf_test_name':csrf_hash,},
								success:function(res){
									var data = JSON.parse(res);
									$("#tot_gst").val(data.gst);
									var tot_gst = data.gst;
										$.ajax({
											url:base_url+'My_bills/fetch_gst',
											type:"POST",
											data:{'csrf_test_name':csrf_hash,"customer_id":customer_id,"company_id":company_id,},
											success:function(res1){
												res1=$.parseJSON(res1);
												var half_gst = parseFloat($("#tot_gst").val())/2;
												if($("#document_type").val()==5){
													$("#igst").val(parseFloat($('#tot_gst').val()));
													$("#sgst").val("0.00");
													$("#cgst").val("0.00");
													$(".branch_igst").val(parseFloat($('#tot_gst').val()));
													$(".branch_sgst").val("0.00");
													$(".branch_cgst").val("0.00");
												}
												else if(res1==true && $("#document_type").val()!=5){
													$("#igst").val("0.00");
													$("#sgst").val(half_gst);
													$("#cgst").val(half_gst);
													$(".branch_igst").val("0.00");
													$(".branch_sgst").val(half_gst);
													$(".branch_cgst").val(half_gst);
												}
												else
												{
													$("#igst").val(parseFloat($('#tot_gst').val()));
													$("#sgst").val("0.00");
													$("#cgst").val("0.00");
													$(".branch_igst").val(parseFloat($('#tot_gst').val()));
													$(".branch_sgst").val("0.00");
													$(".branch_cgst").val("0.00");
												}
												
											},
											complete:function(res1){
												$('input[name="bs_array[]"]').each(function(){
													calculate_total($(this).val());
												});
											},
										});
								},
							});
						},
			});
	  });
	$('#customer_gstin_bs').on('change' ,function(){
		var customer_id = $(this).val();
		$('#customer_place option[value="'+customer_id+'"]').attr('selected','selected');
		$('#customer_place').material_select();
	});
	$('#po_number').on('change' ,function(){
		var customer_id = $(this).val();
		$.ajax({
			url:base_url+'My_bills/get_po_date',
			type:"POST",
			data:{'csrf_test_name':csrf_hash,"customer_id":customer_id,},
			success:function(res){
				$("#po_date").val(res);
			},
		});
	});	
	$('#invoice_no').on('change' ,function(){
		var inv_id = $(this).val();
		$.ajax({
			url:base_url+'My_bills/get_invoice_date',
			type:"POST",
			data:{'csrf_test_name':csrf_hash,"inv_id":inv_id,},
			success:function(res){
				$("#invoice_date").val(res);
			},
		});
	});	
	
	$('#same-as-bill-supply').click(function(){
		//$(document).on("click","#same_bill",function(){
			//$("#same_bill").on("click",function(){
				if($(this).is(':checked')){
					$(".edit_shipping_add").hide();
					$.ajax({
								url:base_url+'My_bills/bill_ship_same',
								type:"POST",
								data:{'csrf_test_name':csrf_hash,},
								success:function(res){
									$('.shipping_address span.ship_add').html(res);
								},						
						});
					
				}
				else{
					$(".edit_shipping_add").show();
				}
	});
 	var count = 1;
	$(".add_new_row").click(function(e){
		e.preventDefault();
		var businesstype = $("#business_type").val();
		if($('#customer_name').val() == '')
		{
			Materialize.toast('Please select client', 2000,'red rounded');
		}
		else
		{
			if(businesstype != '1')
		{
			var newTr = $('<tr id="row_' + count + '"></tr>');
			$.ajax({
				url:base_url+'My_bills/get_services',
				type:"POST",
				data:{'csrf_test_name':csrf_hash,},
				success:function(res){
					var result_opt=$.parseJSON(res);
					var elemt = newTr.html('<input type="hidden" name="bs_array[]" value="' + count + '"><td class="meal over"><div class="scroll-hover-cover"><span class="hoveraction"><a href="#" class="delete_row" id="row_'+count+'" name="row_'+count+'"><img src="'+base_url+'asset/css/img/icons/delete.png"></a></span><select class="select-wrapper tabl-select border-radius-6 btn-dropdown-select select-like-dropdown create-bil-suyp sales-in-drop service" id="service_'+count+'" name="service_'+count+'"><option value="">Select Service</option></select></td><td class="ali-lef"><input type="text"  required="required" autofocus="autofocus" class="form-control" id="particular_' + count + '" value="" name="particular_' + count + '"></td><td><input type="text"  required="required" autofocus="autofocus" class="form-control" id="hsn_sac_' + count + '" value="" name="hsn_sac_' + count + '"></td><td><input type="text"  required="required" autofocus="autofocus" class="form-control qty" id="qty_' + count + '" name="qty_' + count + '" value="1"></td><td><select class="select-wrapper tabl-select border-radius-6 btn-dropdown-select select-like-dropdown create-bil-suyp sales-in-drop units" id="unit_'+count+'" name="unit_'+count+'"><option value="">Select Unit</option></select></td><td><input type="text"  required="required" autofocus="autofocus" class="form-control rate" id="rate_' + count + '" name="rate_' + count + '" value="0"></td><td class="meal"><input type="text" required="required" autofocus="autofocus" class="form-control discount" id="discount_' + count + '" value="0.00" name="discount_' + count + '"><input type="hidden" required="required" autofocus="autofocus" class="form-control disc_amt" id="discount_amt_' + count + '" value="0.00" name="discount_amt_' + count + '"></td><td class="meal"><input type="text" autofocus="autofocus" class="igst_amt form-control" id="igst_amt_' + count + '" name="igst_amt_' + count + '" value="0.00" readonly ></td><td class="meal"><input type="text" autofocus="autofocus" class="branch_igst form-control" id="igst_' + count + '" name="igst_' + count + '" value="'+ $("#igst").val() +'" readonly></td><td class="meal"><input type="text" autofocus="autofocus" class="cgst_amt form-control" id="cgst_amt_' + count + '" value="0.00" name="cgst_amt_' + count + '" readonly ></td><td class="meal"><input type="text" autofocus="autofocus" class="branch_cgst form-control" id="cgst_' + count + '" name="cgst_' + count + '"  value="'+ $("#cgst").val() +'" readonly></td><td class="meal"><input type="text" autofocus="autofocus" class="sgst_amt form-control" id="sgst_amt_' + count + '" value="0.00" name="sgst_amt_' + count + '" readonly ></td><td class="meal"><input type="text" autofocus="autofocus" class="branch_sgst form-control" id="sgst_' + count + '" name="sgst_' + count + '" value="'+ $("#sgst").val() +'" readonly></td><td><input type="text" autofocus="autofocus" class="form-control cess" id="cess_' + count + '" value="0" name="cess_' + count + '"><input type="hidden" autofocus="autofocus" class="cess_amt form-control" id="cess_amt_' + count + '" value="" name="cess_amt_' + count + '" readonly ></td><td class="last-btr"></td><td class="fix num-s meal credit-ntns"><input type="text" autofocus="autofocus" class="form-control amnt" id="amount_' + count + '" value="0" name="amount_' + count + '" readonly ><input type="hidden" class="sub_amt_class" id="sub_tot_amount_' + count + '" value="0" name="sub_tot_amount_' + count + '" readonly ></td>');
						
					$('#scrol-id tr.totalamount').before(elemt);
														
					  //$(".service").html(result_opt[0]);
					  // $(".units").html(result_opt[1]);
					  $("#service_"+count).html(result_opt[0]);
					 // $("#service_"+count).material_select();
					  
					  $("#unit_"+count).html(result_opt[1]);
					  $("#unit_"+count).material_select();
					  
					  $('select').not('.disabled').material_select();
						
					$(document).on('change', '.qty,.rate,.cess,.discount', function () {
						var row_id=$(this).attr('id').split("_");
						calculate_total(row_id[1]);
					});
						
						$("#counter").val(count);
						 count++;
					},
			});	
		} else {
			
			var newTr = $('<tr id="row_' + count + '"></tr>');
			$.ajax({
				url:base_url+'My_bills/get_services',
				type:"POST",
				data:{'csrf_test_name':csrf_hash,},
				success:function(res){
					var result_opt=$.parseJSON(res);
					var elemt = newTr.html('<input type="hidden" name="bs_array[]" value="' + count + '"><td class="meal over"><div class="scroll-hover-cover"><span class="hoveraction"><a href="#" class="delete_row" id="row_'+count+'" name="row_'+count+'"><img src="'+base_url+'asset/css/img/icons/delete.png"></a></span><select class="select-wrapper tabl-select border-radius-6 btn-dropdown-select select-like-dropdown create-bil-suyp sales-in-drop service" id="service_'+count+'" name="service_'+count+'"><option value="">Select Service</option></select></td><td class="ali-lef"><input type="text"  required="required" autofocus="autofocus" class="form-control" id="particular_' + count + '" value="" name="particular_' + count + '"></td><td><input type="text"  required="required" autofocus="autofocus" class="form-control" id="hsn_sac_' + count + '" value="" name="hsn_sac_' + count + '"></td><td><input type="text"  required="required" autofocus="autofocus" class="form-control qty" id="qty_' + count + '" name="qty_' + count + '" value="1"></td><td><input type="text"  required="required" autofocus="autofocus" class="form-control rate" id="rate_' + count + '" name="rate_' + count + '" value="0"></td><td class="meal"><input type="text" required="required" autofocus="autofocus" class="form-control discount" id="discount_' + count + '" value="0.00" name="discount_' + count + '"><input type="hidden" required="required" autofocus="autofocus" class="form-control disc_amt" id="discount_amt_' + count + '" value="0.00" name="discount_amt_' + count + '"></td><td class="meal"><input type="text" autofocus="autofocus" class="igst_amt form-control" id="igst_amt_' + count + '" name="igst_amt_' + count + '" value="0.00" readonly ></td><td class="meal"><input type="text" autofocus="autofocus" class="branch_igst form-control" id="igst_' + count + '" name="igst_' + count + '" value="'+ $("#igst").val() +'" readonly></td><td class="meal"><input type="text" autofocus="autofocus" class="cgst_amt form-control" id="cgst_amt_' + count + '" value="0.00" name="cgst_amt_' + count + '" readonly ></td><td class="meal"><input type="text" autofocus="autofocus" class="branch_cgst form-control" id="cgst_' + count + '" name="cgst_' + count + '"  value="'+ $("#cgst").val() +'" readonly></td><td class="meal"><input type="text" autofocus="autofocus" class="sgst_amt form-control" id="sgst_amt_' + count + '" value="0.00" name="sgst_amt_' + count + '" readonly ></td><td class="meal"><input type="text" autofocus="autofocus" class="branch_sgst form-control" id="sgst_' + count + '" name="sgst_' + count + '" value="'+ $("#sgst").val() +'" readonly></td><td><input type="text" autofocus="autofocus" class="form-control cess" id="cess_' + count + '" value="0" name="cess_' + count + '"><input type="hidden" autofocus="autofocus" class="cess_amt form-control" id="cess_amt_' + count + '" value="" name="cess_amt_' + count + '" readonly ></td><td class="last-btr"></td><td class="fix num-s meal credit-ntns"><input type="text" autofocus="autofocus" class="form-control amnt" id="amount_' + count + '" value="0" name="amount_' + count + '" readonly ><input type="hidden" class="sub_amt_class" id="sub_tot_amount_' + count + '" value="0" name="sub_tot_amount_' + count + '" readonly ></td>');
						
					 // $('#scrol-id tbody').append(elemt);
					
					 $('#scrol-id tr.totalamount').before(elemt);
						
					  $("#service_"+count).html(result_opt[0]);
					  $("#service_"+count).material_select();

					  $('select').not('.disabled').material_select();
						$(document).on('change', '.qty,.rate,.cess,.discount', function () {
							var row_id=$(this).attr('id').split("_");
							calculate_total(row_id[1]);
						});
					  $("#counter").val(count);
						 count++;
					},
			});		
		}
		}
	});
 
	$('.qty,.rate,.discount').change(function () {
		var row_id=$(this).attr('id').split("_");
		calculate_total(row_id[1]);
	});
	
	$(document).off().on('change', '.service',  function () {
		
		if($(this).val() == 'add_new_service') {
			$.ajax({
				url:base_url+'My_bills/get_units',
				type:"POST",
				data:{'csrf_test_name':csrf_hash,},
				success:function(res){
						$('#invoice_add_new_service').modal('open');
						$("#invoice_unit").html(res);
						$("#invoice_unit").parents('.input-field').addClass('label-active');
						$('#invoice_unit').material_select();
					},
			});	
		} 
		else 
		{
			var row_id=$(this).attr('id').split("_");
			var service_id = $(this).val();
			calculate_total(row_id[1]);
		}
	});

	
	
var tot = 0;
function calculate_total(id)
{
	
	var row = $('#row_'+id);
	var quantity = row.find('#qty_'+id).val();
	var rate = row.find('#rate_'+id).val();
	var service = row.find('#service_'+id).val();
	var discount = row.find('#discount_'+id).val();
	var cess = row.find('#cess_'+id).val();
	var igst = row.find('#igst_'+id).val();
	var cgst = row.find('#cgst_'+id).val();
	var sgst = row.find('#sgst_'+id).val();
	var customer = $("#customer_id").val();

	
	$.ajax({
			url:base_url+'My_bills/get_service_info',
			type:"POST",
			data:{'csrf_test_name':csrf_hash,"service_id":service},
			success:function(res){
				var data=JSON.parse(res);
				
					$("#hsn_sac_"+id).val(data[0].hsn_sac_no);
					$("#unit_"+id).val(data[0].unit);
					var tot=parseFloat(quantity) * parseFloat(rate);
					$("#sub_tot_amount_"+id).val(tot);
					var tot_gst=parseFloat(igst)+parseFloat(sgst)+parseFloat(cgst);
					var applicable_cess=(parseFloat(tot)*parseFloat(cess))/100;
					$("#cess_amt_"+id).val(applicable_cess);
					var applicable_disc=(parseFloat(tot)*parseFloat(discount))/100;
					$("#discount_amt_"+id).val(applicable_disc);
					
				
				$('#igst_amt_'+id).val((parseFloat(tot)*parseFloat(igst))/100);
				$('#cgst_amt_'+id).val((parseFloat(tot)*parseFloat(cgst))/100);
					$('#sgst_amt_'+id).val((parseFloat(tot)*parseFloat(sgst))/100);
					var applicable_gst=(parseFloat(tot) * parseFloat(tot_gst))/100;
					//tot=(tot+applicable_gst+applicable_cess)-applicable_disc;
					if ($('#reversecharge').length>0){
						tot=(tot)-applicable_disc;
					}
					else{
						tot=(tot+applicable_gst+applicable_cess)-applicable_disc;
					}
					$("#amount_"+id).val(parseFloat(tot));
					total=0;
					$('.sub_amt_class').each(function(){
						if($(this).val()==''){
							$(this).val(0);
						}
						total=parseFloat(total)+parseFloat($(this).val());
					});
					$("#total").val(parseFloat(total));
					cess_amt=0;
					$('.cess_amt').each(function(){
						if($(this).val()==''){
							$(this).val(0);
						}
						cess_amt=parseFloat(cess_amt)+parseFloat($(this).val());
					});
					$(".cess_tot_amt").val(parseFloat(cess_amt));
					discount_amt=0;
						$('.disc_amt').each(function(){
							if($(this).val()==''){
								$(this).val(0);
							}
							discount_amt=parseFloat(discount_amt)+parseFloat($(this).val());
						});
						$(".disc_tot_amt").val(parseFloat(discount_amt));
						igst_tot_amt=0;
						$('.igst_amt').each(function(){
							if($(this).val()==''){
								$(this).val(0);
							}
							igst_tot_amt=parseFloat(igst_tot_amt)+parseFloat($(this).val());
						});
						$(".igst_tot_amt").val(parseFloat(igst_tot_amt));
						cgst_tot_amt=0;
						$('.cgst_amt').each(function(){
							if($(this).val()==''){
								$(this).val(0);
							}
							cgst_tot_amt=parseFloat(cgst_tot_amt)+parseFloat($(this).val());
						});
						$(".cgst_tot_amt").val(parseFloat(cgst_tot_amt));
						sgst_tot_amt=0;
						$('.sgst_amt').each(function(){
							if($(this).val()==''){
								$(this).val(0);
							}
							sgst_tot_amt=parseFloat(sgst_tot_amt)+parseFloat($(this).val());
						});
						$(".sgst_tot_amt").val(parseFloat(sgst_tot_amt));
						final_amount=0;
						$('.amnt').each(function(){
							if($(this).val()==''){
								$(this).val(0);
							}
							final_amount=parseFloat(final_amount)+parseFloat($(this).val());
						});
						$("#totalamount").val(parseFloat(final_amount));
						$(".grandtotalamount").html(parseFloat(final_amount));
						
				},
		});
		
}

	$(document).on('click', '.delete_row',  function() {
 	var id=$(this).attr('id');
		//calculate_total(id);
		$(this).closest("tr").remove();
	 
	});
});