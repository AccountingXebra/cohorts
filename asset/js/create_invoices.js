
 var count = 1;
$(".addnewrow").click(function(e){
	e.preventDefault();
	var businesstype = $(this).data('businesstype');
	var document_type=$("#document_type").val();
	if($('#customer_id').val() == '')
	{
		Materialize.toast('Please select client', 2000,'red rounded');
	}
	else if(document_type==''){
		Materialize.toast('Please select Invoice type', 2000,'red rounded');
	}
	else {
		
		if(businesstype != '1')
		{
			var newTr = $('<tr id="row_' + count + '"></tr>');
			$.ajax({
				url:base_url+'My_invoices/get_services',
				type:"POST",
				data:{'csrf_test_name':csrf_hash,},
				success:function(res){
					var result_opt=$.parseJSON(res);
					var elemt = newTr.html('<input type="hidden" name="invoice_array[]" value="' + count + '"><td class="meal over"><div class="scroll-hover-cover"><span class="hoveraction"><a href="#" class="delete_row" id="row_'+count+'" name="row_'+count+'"><img src="'+base_url+'asset/css/img/icons/delete.png"></a></span><select class="select-wrapper tabl-select border-radius-6 btn-dropdown-select select-like-dropdown create-bil-suyp sales-in-drop service" id="service_'+count+'" name="service_'+count+'"><option value="">Select Service</option></select></td><td class="ali-lef"><input type="text"  required="required" autofocus="autofocus" class="form-control" id="particular_' + count + '" value="" name="particular_' + count + '"></td><td><input type="text"  required="required" autofocus="autofocus" class="form-control" id="hsn_sac_' + count + '" value="" name="hsn_sac_' + count + '"></td><td><input type="text"  required="required" autofocus="autofocus" class="form-control qty" id="qty_' + count + '" name="qty_' + count + '" value="1"></td><td><select class="select-wrapper tabl-select border-radius-6 btn-dropdown-select select-like-dropdown create-bil-suyp sales-in-drop units" id="unit_'+count+'" name="unit_'+count+'"><option value="">Select Unit</option></select></td><td><input type="text"  required="required" autofocus="autofocus" class="form-control rate" id="rate_' + count + '" name="rate_' + count + '" value="0"></td><td class="meal"><input type="text" required="required" autofocus="autofocus" class="form-control discount" id="discount_' + count + '" value="0.00" name="discount_' + count + '"><input type="hidden" required="required" autofocus="autofocus" class="form-control disc_amt" id="discount_amt_' + count + '" value="0.00" name="discount_amt_' + count + '"></td><td class="meal"><input type="text" autofocus="autofocus" class="igst_amt form-control" id="igst_amt_' + count + '" name="igst_amt_' + count + '" value="0.00" readonly ></td><td class="meal"><input type="text" autofocus="autofocus" class="branch_igst form-control" id="igst_' + count + '" name="igst_' + count + '" value="'+ $("#igst").val() +'" readonly></td><td class="meal"><input type="text" autofocus="autofocus" class="cgst_amt form-control" id="cgst_amt_' + count + '" value="0.00" name="cgst_amt_' + count + '" readonly ></td><td class="meal"><input type="text" autofocus="autofocus" class="branch_cgst form-control" id="cgst_' + count + '" name="cgst_' + count + '"  value="'+ $("#cgst").val() +'" readonly></td><td class="meal"><input type="text" autofocus="autofocus" class="sgst_amt form-control" id="sgst_amt_' + count + '" value="0.00" name="sgst_amt_' + count + '" readonly ></td><td class="meal"><input type="text" autofocus="autofocus" class="branch_sgst form-control" id="sgst_' + count + '" name="sgst_' + count + '" value="'+ $("#sgst").val() +'" readonly></td><td><input type="text" autofocus="autofocus" class="form-control cess" id="cess_' + count + '" value="0" name="cess_' + count + '"><input type="hidden" autofocus="autofocus" class="cess_amt form-control" id="cess_amt_' + count + '" value="" name="cess_amt_' + count + '" readonly ></td><td class="last-btr"></td><td class="fix num-s meal credit-ntns"><input type="text" autofocus="autofocus" class="form-control amnt" id="amount_' + count + '" value="0" name="amount_' + count + '" readonly ><input type="hidden" class="sub_amt_class" id="sub_tot_amount_' + count + '" value="0" name="sub_tot_amount_' + count + '" readonly ></td>');
						
					
						if($("#scrol-id tr.media_comm_row").length>0 && $("#scrol-id tr.late_fee_row").length>0 && $("#scrol-id tr.equ_leavy_row").length>0){
						$("tr.media_comm_row").attr("id",'row_'+(count+1));
						$("tr.media_comm_row .particulars").attr('id','particular_'+(count+1));
						$("tr.media_comm_row .particulars").attr('name','particular_'+(count+1));
						$("tr.media_comm_row .qty").attr('id','qty_'+(count+1));
						$("tr.media_comm_row .qty").attr('name','qty_'+(count+1));
						$("tr.media_comm_row .units").attr('id','unit_'+(count+1));
						$("tr.media_comm_row .units").attr('name','unit_'+(count+1));
						$("tr.media_comm_row .branch_igst").attr('id','igst_'+(count+1));
						$("tr.media_comm_row .branch_igst").attr('name','igst_'+(count+1));
						$("tr.media_comm_row .igst_amt").attr('id','igst_amt_'+(count+1));
						$("tr.media_comm_row .igst_amt").attr('name','igst_amt_'+(count+1));
						$("tr.media_comm_row .branch_cgst").attr('id','cgst_'+(count+1));
						$("tr.media_comm_row .branch_cgst").attr('name','cgst_'+(count+1));
						$("tr.media_comm_row .cgst_amt").attr('id','cgst_amt_'+(count+1));
						$("tr.media_comm_row .cgst_amt").attr('name','cgst_amt_'+(count+1));
						$("tr.media_comm_row .branch_sgst").attr('id','sgst_'+(count+1));
						$("tr.media_comm_row .branch_sgst").attr('name','sgst_'+(count+1));
						$("tr.media_comm_row .sgst_amt").attr('id','sgst_amt_'+(count+1));
						$("tr.media_comm_row .sgst_amt").attr('name','sgst_amt_'+(count+1));
						$("tr.media_comm_row .cess").attr('id','cess_'+(count+1));
						$("tr.media_comm_row .cess").attr('name','cess_'+(count+1));
						$("tr.media_comm_row .cess_amt").attr('id','cess_amt_'+(count+1));
						$("tr.media_comm_row .cess_amt").attr('name','cess_amt_'+(count+1));

						$("tr.media_comm_row .other").attr('id','other_'+(count+1));
						$("tr.media_comm_row .other").attr('name','other_'+(count+1));
						$("tr.media_comm_row .other_amt").attr('id','other_amt_'+(count+1));
						$("tr.media_comm_row .other_amt").attr('name','other_amt_'+(count+1));
						$("tr.media_comm_row .discount").attr('id','discount_'+(count+1));
						$("tr.media_comm_row .discount").attr('name','discount_'+(count+1));
						$("tr.media_comm_row .disc_amt").attr('id','discount_amt_'+(count+1));
						$("tr.media_comm_row .disc_amt").attr('name','discount_amt_'+(count+1));
						$("tr.media_comm_row .amnt").attr('id','amount_'+(count+1));
						$("tr.media_comm_row .amnt").attr('name','amount_'+(count+1));
						$("tr.media_comm_row .sub_amt_class").attr('id','sub_tot_amount_'+(count+1));
						$("tr.media_comm_row .sub_amt_class").attr('name','sub_tot_amount_'+(count+1));
						$("tr.media_comm_row .hsn_sac_num").attr('id','hsn_sac_'+(count+1));
						$("tr.media_comm_row .hsn_sac_num").attr('name','hsn_sac_'+(count+1));
						$("tr.media_comm_row  .service").attr('id','service_'+(count+1));
						$("tr.media_comm_row  .service").attr('name','service_'+(count+1));
						$("tr.media_comm_row .rate").attr('id','rate_'+(count+1));
						$("tr.media_comm_row .rate").attr('name','rate_'+(count+1));
						$("tr.media_comm_row .row_num").val((count+1));
							
						$("tr.late_fee_row").attr("id",'row_'+(count+2));
						$("tr.late_fee_row .particulars").attr('id','particular_'+(count+2));
						$("tr.late_fee_row .particulars").attr('name','particular_'+(count+2));
						$("tr.late_fee_row .qty").attr('id','qty_'+(count+2));
						$("tr.late_fee_row .qty").attr('name','qty_'+(count+2));
						$("tr.media_comm_row .units").attr('id','unit_'+(count+2));
						$("tr.media_comm_row .units").attr('name','unit_'+(count+2));
						$("tr.late_fee_row .branch_igst").attr('id','igst_'+(count+2));
						$("tr.late_fee_row .branch_igst").attr('name','igst_'+(count+2));
						$("tr.late_fee_row .igst_amt").attr('id','igst_amt_'+(count+2));
						$("tr.late_fee_row .igst_amt").attr('name','igst_amt_'+(count+2));
						$("tr.late_fee_row .branch_cgst").attr('id','cgst_'+(count+2));
						$("tr.late_fee_row .branch_cgst").attr('name','cgst_'+(count+2));
						$("tr.late_fee_row .cgst_amt").attr('id','cgst_amt_'+(count+2));
						$("tr.late_fee_row .cgst_amt").attr('name','cgst_amt_'+(count+2));
						$("tr.late_fee_row .branch_sgst").attr('id','sgst_'+(count+2));
						$("tr.late_fee_row .branch_sgst").attr('name','sgst_'+(count+2));
						$("tr.late_fee_row .sgst_amt").attr('id','sgst_amt_'+(count+2));
						$("tr.late_fee_row .sgst_amt").attr('name','sgst_amt_'+(count+2));
						$("tr.late_fee_row .cess").attr('id','cess_'+(count+2));
						$("tr.late_fee_row .cess").attr('name','cess_'+(count+2));
						$("tr.late_fee_row .cess_amt").attr('id','cess_amt_'+(count+2));
						$("tr.late_fee_row .cess_amt").attr('name','cess_amt_'+(count+2));

                        $("tr.late_fee_row .other").attr('id','other_'+(count+2));
						$("tr.late_fee_row .other").attr('name','other_'+(count+2));
						$("tr.late_fee_row .other_amt").attr('id','other_amt_'+(count+2));
						$("tr.late_fee_row .other_amt").attr('name','other_amt_'+(count+2));

						$("tr.late_fee_row .discount").attr('id','discount_'+(count+2));
						$("tr.late_fee_row .discount").attr('name','discount_'+(count+2));
						$("tr.late_fee_row .disc_amt").attr('id','discount_amt_'+(count+2));
						$("tr.late_fee_row .disc_amt").attr('name','discount_amt_'+(count+2));
						$("tr.late_fee_row .amnt").attr('id','amount_'+(count+2));
						$("tr.late_fee_row .amnt").attr('name','amount_'+(count+2));
						$("tr.late_fee_row .sub_amt_class").attr('id','sub_tot_amount_'+(count+2));
						$("tr.late_fee_row .sub_amt_class").attr('name','sub_tot_amount_'+(count+2));
						$("tr.late_fee_row .hsn_sac_num").attr('id','hsn_sac_'+(count+2));
						$("tr.late_fee_row .hsn_sac_num").attr('name','hsn_sac_'+(count+2));
						$("tr.late_fee_row  .service").attr('id','service_'+(count+2));
						$("tr.late_fee_row  .service").attr('name','service_'+(count+2));
						$("tr.late_fee_row .rate").attr('id','rate_'+(count+2));
						$("tr.late_fee_row .rate").attr('name','rate_'+(count+2));
						$("tr.late_fee_row .row_num").val((count+2));	
						
						$("tr.equ_leavy_row").attr("id",'row_'+(count+3));
						$("tr.equ_leavy_row .particulars").attr('id','particular_'+(count+3));
						$("tr.equ_leavy_row .particulars").attr('name','particular_'+(count+3));
						$("tr.equ_leavy_row .qty").attr('id','qty_'+(count+3));
						$("tr.equ_leavy_row .qty").attr('name','qty_'+(count+3));
						$("tr.equ_leavy_row .units").attr('id','unit_'+(count+3));
						$("tr.equ_leavy_row .units").attr('name','unit_'+(count+3));
						$("tr.equ_leavy_row .branch_igst").attr('id','igst_'+(count+3));
						$("tr.equ_leavy_row .branch_igst").attr('name','igst_'+(count+3));
						$("tr.equ_leavy_row .igst_amt").attr('id','igst_amt_'+(count+3));
						$("tr.equ_leavy_row .igst_amt").attr('name','igst_amt_'+(count+3));
						$("tr.equ_leavy_row .branch_cgst").attr('id','cgst_'+(count+3));
						$("tr.equ_leavy_row .branch_cgst").attr('name','cgst_'+(count+3));
						$("tr.equ_leavy_row .cgst_amt").attr('id','cgst_amt_'+(count+3));
						$("tr.equ_leavy_row .cgst_amt").attr('name','cgst_amt_'+(count+3));
						$("tr.equ_leavy_row .branch_sgst").attr('id','sgst_'+(count+3));
						$("tr.equ_leavy_row .branch_sgst").attr('name','sgst_'+(count+3));
						$("tr.equ_leavy_row .sgst_amt").attr('id','sgst_amt_'+(count+3));
						$("tr.equ_leavy_row .sgst_amt").attr('name','sgst_amt_'+(count+3));
						$("tr.equ_leavy_row .cess").attr('id','cess_'+(count+3));
						$("tr.equ_leavy_row .cess").attr('name','cess_'+(count+3));
						$("tr.equ_leavy_row .cess_amt").attr('id','cess_amt_'+(count+3));
						$("tr.equ_leavy_row .cess_amt").attr('name','cess_amt_'+(count+3));
                        $("tr.equ_leavy_row .other").attr('id','other_'+(count+3));
						$("tr.equ_leavy_row .other").attr('name','other_'+(count+3));
						$("tr.equ_leavy_row .other_amt").attr('id','other_amt_'+(count+3));
						$("tr.equ_leavy_row .other_amt").attr('name','other_amt_'+(count+3));

						$("tr.equ_leavy_row .discount").attr('id','discount_'+(count+3));
						$("tr.equ_leavy_row .discount").attr('name','discount_'+(count+3));
						$("tr.equ_leavy_row .disc_amt").attr('id','discount_amt_'+(count+3));
						$("tr.equ_leavy_row .disc_amt").attr('name','discount_amt_'+(count+3));
						$("tr.equ_leavy_row .amnt").attr('id','amount_'+(count+3));
						$("tr.equ_leavy_row .amnt").attr('name','amount_'+(count+3));
						$("tr.equ_leavy_row .sub_amt_class").attr('id','sub_tot_amount_'+(count+3));
						$("tr.equ_leavy_row .sub_amt_class").attr('name','sub_tot_amount_'+(count+3));
						$("tr.equ_leavy_row .hsn_sac_num").attr('id','hsn_sac_'+(count+3));
						$("tr.equ_leavy_row .hsn_sac_num").attr('name','hsn_sac_'+(count+3));
						$("tr.equ_leavy_row  .service").attr('id','service_'+(count+3));
						$("tr.equ_leavy_row  .service").attr('name','service_'+(count+3));
						$("tr.equ_leavy_row .rate").attr('id','rate_'+(count+3));
						$("tr.equ_leavy_row .rate").attr('name','rate_'+(count+3));
						$("tr.equ_leavy_row .row_num").val((count+3));
							
							$("#scrol-id tbody tr:nth-last-child(3)").before(elemt);
						}
						else if(($("#scrol-id tr.media_comm_row").length>0 && $("#scrol-id tr.late_fee_row").length>0) || ($("#scrol-id tr.media_comm_row").length>0 &&  $("#scrol-id tr.equ_leavy_row").length>0) || ($("#scrol-id tr.late_fee_row").length>0 && $("#scrol-id tr.equ_leavy_row").length>0)){
							if($("#scrol-id tr.media_comm_row").length>0){
								$("tr.media_comm_row").attr("id",'row_'+(count+1));
								$("tr.media_comm_row .particulars").attr('id','particular_'+(count+1));
								$("tr.media_comm_row .particulars").attr('name','particular_'+(count+1));
								$("tr.media_comm_row .qty").attr('id','qty_'+(count+1));
								$("tr.media_comm_row .qty").attr('name','qty_'+(count+1));
								$("tr.media_comm_row .units").attr('id','unit_'+(count+1));
								$("tr.media_comm_row .units").attr('name','unit_'+(count+1));
								$("tr.media_comm_row .branch_igst").attr('id','igst_'+(count+1));
								$("tr.media_comm_row .branch_igst").attr('name','igst_'+(count+1));
								$("tr.media_comm_row .igst_amt").attr('id','igst_amt_'+(count+1));
								$("tr.media_comm_row .igst_amt").attr('name','igst_amt_'+(count+1));
								$("tr.media_comm_row .branch_cgst").attr('id','cgst_'+(count+1));
								$("tr.media_comm_row .branch_cgst").attr('name','cgst_'+(count+1));
								$("tr.media_comm_row .cgst_amt").attr('id','cgst_amt_'+(count+1));
								$("tr.media_comm_row .cgst_amt").attr('name','cgst_amt_'+(count+1));
								$("tr.media_comm_row .branch_sgst").attr('id','sgst_'+(count+1));
								$("tr.media_comm_row .branch_sgst").attr('name','sgst_'+(count+1));
								$("tr.media_comm_row .sgst_amt").attr('id','sgst_amt_'+(count+1));
								$("tr.media_comm_row .sgst_amt").attr('name','sgst_amt_'+(count+1));
								$("tr.media_comm_row .cess").attr('id','cess_'+(count+1));
								$("tr.media_comm_row .cess").attr('name','cess_'+(count+1));
								$("tr.media_comm_row .cess_amt").attr('id','cess_amt_'+(count+1));
								$("tr.media_comm_row .cess_amt").attr('name','cess_amt_'+(count+1));

									$("tr.media_comm_row .other").attr('id','other_'+(count+1));
								$("tr.media_comm_row .other").attr('name','other_'+(count+1));
								$("tr.media_comm_row .other_amt").attr('id','other_amt_'+(count+1));
								$("tr.media_comm_row .other_amt").attr('name','other_amt_'+(count+1));
								$("tr.media_comm_row .discount").attr('id','discount_'+(count+1));
								$("tr.media_comm_row .discount").attr('name','discount_'+(count+1));
								$("tr.media_comm_row .disc_amt").attr('id','discount_amt_'+(count+1));
								$("tr.media_comm_row .disc_amt").attr('name','discount_amt_'+(count+1));
								$("tr.media_comm_row .amnt").attr('id','amount_'+(count+1));
								$("tr.media_comm_row .amnt").attr('name','amount_'+(count+1));
								$("tr.media_comm_row .sub_amt_class").attr('id','sub_tot_amount_'+(count+1));
								$("tr.media_comm_row .sub_amt_class").attr('name','sub_tot_amount_'+(count+1));
								$("tr.media_comm_row .hsn_sac_num").attr('id','hsn_sac_'+(count+1));
								$("tr.media_comm_row .hsn_sac_num").attr('name','hsn_sac_'+(count+1));
								$("tr.media_comm_row  .service").attr('id','service_'+(count+1));
								$("tr.media_comm_row  .service").attr('name','service_'+(count+1));
								$("tr.media_comm_row .rate").attr('id','rate_'+(count+1));
								$("tr.media_comm_row .rate").attr('name','rate_'+(count+1));
								$("tr.media_comm_row .row_num").val((count+1));
							}
							if($("#scrol-id tr.late_fee_row").length>0){
								$("tr.late_fee_row").attr("id",'row_'+(count+2));
								$("tr.late_fee_row .particulars").attr('id','particular_'+(count+2));
								$("tr.late_fee_row .particulars").attr('name','particular_'+(count+2));
								$("tr.late_fee_row .qty").attr('id','qty_'+(count+2));
								$("tr.late_fee_row .qty").attr('name','qty_'+(count+2));
								$("tr.late_fee_row .units").attr('id','unit_'+(count+2));
								$("tr.late_fee_row .units").attr('name','unit_'+(count+2));
								$("tr.late_fee_row .branch_igst").attr('id','igst_'+(count+2));
								$("tr.late_fee_row .branch_igst").attr('name','igst_'+(count+2));
								$("tr.late_fee_row .igst_amt").attr('id','igst_amt_'+(count+2));
								$("tr.late_fee_row .igst_amt").attr('name','igst_amt_'+(count+2));
								$("tr.late_fee_row .branch_cgst").attr('id','cgst_'+(count+2));
								$("tr.late_fee_row .branch_cgst").attr('name','cgst_'+(count+2));
								$("tr.late_fee_row .cgst_amt").attr('id','cgst_amt_'+(count+2));
								$("tr.late_fee_row .cgst_amt").attr('name','cgst_amt_'+(count+2));
								$("tr.late_fee_row .branch_sgst").attr('id','sgst_'+(count+2));
								$("tr.late_fee_row .branch_sgst").attr('name','sgst_'+(count+2));
								$("tr.late_fee_row .sgst_amt").attr('id','sgst_amt_'+(count+2));
								$("tr.late_fee_row .sgst_amt").attr('name','sgst_amt_'+(count+2));
								$("tr.late_fee_row .cess").attr('id','cess_'+(count+2));
								$("tr.late_fee_row .cess").attr('name','cess_'+(count+2));
								$("tr.late_fee_row .cess_amt").attr('id','cess_amt_'+(count+2));
								$("tr.late_fee_row .cess_amt").attr('name','cess_amt_'+(count+2));
                                 $("tr.late_fee_row .other").attr('id','other_'+(count+2));
								$("tr.late_fee_row .other").attr('name','other_'+(count+2));
								$("tr.late_fee_row .other_amt").attr('id','other_amt_'+(count+2));
								$("tr.late_fee_row .other_amt").attr('name','other_amt_'+(count+2));

								$("tr.late_fee_row .discount").attr('id','discount_'+(count+2));
								$("tr.late_fee_row .discount").attr('name','discount_'+(count+2));
								$("tr.late_fee_row .disc_amt").attr('id','discount_amt_'+(count+2));
								$("tr.late_fee_row .disc_amt").attr('name','discount_amt_'+(count+2));
								$("tr.late_fee_row .amnt").attr('id','amount_'+(count+2));
								$("tr.late_fee_row .amnt").attr('name','amount_'+(count+2));
								$("tr.late_fee_row .sub_amt_class").attr('id','sub_tot_amount_'+(count+2));
								$("tr.late_fee_row .sub_amt_class").attr('name','sub_tot_amount_'+(count+2));
								$("tr.late_fee_row .hsn_sac_num").attr('id','hsn_sac_'+(count+2));
								$("tr.late_fee_row .hsn_sac_num").attr('name','hsn_sac_'+(count+2));
								$("tr.late_fee_row  .service").attr('id','service_'+(count+2));
								$("tr.late_fee_row  .service").attr('name','service_'+(count+2));
								$("tr.late_fee_row .rate").attr('id','rate_'+(count+2));
								$("tr.late_fee_row .rate").attr('name','rate_'+(count+2));
								$("tr.late_fee_row .row_num").val((count+2));	
							}
							if($("#scrol-id tr.equ_leavy_row").length>0){
								$("tr.equ_leavy_row").attr("id",'row_'+(count+3));
								$("tr.equ_leavy_row .particulars").attr('id','particular_'+(count+3));
								$("tr.equ_leavy_row .particulars").attr('name','particular_'+(count+3));
								$("tr.equ_leavy_row .qty").attr('id','qty_'+(count+3));
								$("tr.equ_leavy_row .qty").attr('name','qty_'+(count+3));
								$("tr.equ_leavy_row .units").attr('id','unit_'+(count+3));
								$("tr.equ_leavy_row .units").attr('name','unit_'+(count+3));
								$("tr.equ_leavy_row .branch_igst").attr('id','igst_'+(count+3));
								$("tr.equ_leavy_row .branch_igst").attr('name','igst_'+(count+3));
								$("tr.equ_leavy_row .igst_amt").attr('id','igst_amt_'+(count+3));
								$("tr.equ_leavy_row .igst_amt").attr('name','igst_amt_'+(count+3));
								$("tr.equ_leavy_row .branch_cgst").attr('id','cgst_'+(count+3));
								$("tr.equ_leavy_row .branch_cgst").attr('name','cgst_'+(count+3));
								$("tr.equ_leavy_row .cgst_amt").attr('id','cgst_amt_'+(count+3));
								$("tr.equ_leavy_row .cgst_amt").attr('name','cgst_amt_'+(count+3));
								$("tr.equ_leavy_row .branch_sgst").attr('id','sgst_'+(count+3));
								$("tr.equ_leavy_row .branch_sgst").attr('name','sgst_'+(count+3));
								$("tr.equ_leavy_row .sgst_amt").attr('id','sgst_amt_'+(count+3));
								$("tr.equ_leavy_row .sgst_amt").attr('name','sgst_amt_'+(count+3));
								$("tr.equ_leavy_row .cess").attr('id','cess_'+(count+3));
								$("tr.equ_leavy_row .cess").attr('name','cess_'+(count+3));
								$("tr.equ_leavy_row .cess_amt").attr('id','cess_amt_'+(count+3));
								$("tr.equ_leavy_row .cess_amt").attr('name','cess_amt_'+(count+3));

								$("tr.equ_leavy_row .other").attr('id','other_'+(count+3));
								$("tr.equ_leavy_row .other").attr('name','other_'+(count+3));
								$("tr.equ_leavy_row .other_amt").attr('id','other_amt_'+(count+3));
								$("tr.equ_leavy_row .other_amt").attr('name','other_amt_'+(count+3));
								$("tr.equ_leavy_row .discount").attr('id','discount_'+(count+3));
								$("tr.equ_leavy_row .discount").attr('name','discount_'+(count+3));
								$("tr.equ_leavy_row .disc_amt").attr('id','discount_amt_'+(count+3));
								$("tr.equ_leavy_row .disc_amt").attr('name','discount_amt_'+(count+3));
								$("tr.equ_leavy_row .amnt").attr('id','amount_'+(count+3));
								$("tr.equ_leavy_row .amnt").attr('name','amount_'+(count+3));
								$("tr.equ_leavy_row .sub_amt_class").attr('id','sub_tot_amount_'+(count+3));
								$("tr.equ_leavy_row .sub_amt_class").attr('name','sub_tot_amount_'+(count+3));
								$("tr.equ_leavy_row .hsn_sac_num").attr('id','hsn_sac_'+(count+3));
								$("tr.equ_leavy_row .hsn_sac_num").attr('name','hsn_sac_'+(count+3));
								$("tr.equ_leavy_row  .service").attr('id','service_'+(count+3));
								$("tr.equ_leavy_row  .service").attr('name','service_'+(count+3));
								$("tr.equ_leavy_row .rate").attr('id','rate_'+(count+3));
								$("tr.equ_leavy_row .rate").attr('name','rate_'+(count+3));
								$("tr.equ_leavy_row .row_num").val((count+3));
							}
							$("#scrol-id tbody tr:nth-last-child(2)").before(elemt);
						}
						else if($("#scrol-id tr.media_comm_row").length>0 || $("#scrol-id tr.late_fee_row").length>0
 || $("#scrol-id tr.equ_leavy_row").length>0
)
						{
							if($("#scrol-id tr.media_comm_row").length>0){
								$("tr.media_comm_row").attr("id",'row_'+(count+1));
								$("tr.media_comm_row .particulars").attr('id','particular_'+(count+1));
								$("tr.media_comm_row .particulars").attr('name','particular_'+(count+1));
								$("tr.media_comm_row .qty").attr('id','qty_'+(count+1));
								$("tr.media_comm_row .qty").attr('name','qty_'+(count+1));
								$("tr.media_comm_row .units").attr('id','unit_'+(count+1));
								$("tr.media_comm_row .units").attr('name','unit_'+(count+1));
								$("tr.media_comm_row .branch_igst").attr('id','igst_'+(count+1));
								$("tr.media_comm_row .branch_igst").attr('name','igst_'+(count+1));
								$("tr.media_comm_row .igst_amt").attr('id','igst_amt_'+(count+1));
								$("tr.media_comm_row .igst_amt").attr('name','igst_amt_'+(count+1));
								$("tr.media_comm_row .branch_cgst").attr('id','cgst_'+(count+1));
								$("tr.media_comm_row .branch_cgst").attr('name','cgst_'+(count+1));
								$("tr.media_comm_row .cgst_amt").attr('id','cgst_amt_'+(count+1));
								$("tr.media_comm_row .cgst_amt").attr('name','cgst_amt_'+(count+1));
								$("tr.media_comm_row .branch_sgst").attr('id','sgst_'+(count+1));
								$("tr.media_comm_row .branch_sgst").attr('name','sgst_'+(count+1));
								$("tr.media_comm_row .sgst_amt").attr('id','sgst_amt_'+(count+1));
								$("tr.media_comm_row .sgst_amt").attr('name','sgst_amt_'+(count+1));
								$("tr.media_comm_row .cess").attr('id','cess_'+(count+1));
								$("tr.media_comm_row .cess").attr('name','cess_'+(count+1));
								$("tr.media_comm_row .cess_amt").attr('id','cess_amt_'+(count+1));
								$("tr.media_comm_row .cess_amt").attr('name','cess_amt_'+(count+1));
                                
                                $("tr.media_comm_row .other").attr('id','other_'+(count+1));
								$("tr.media_comm_row .other").attr('name','other_'+(count+1));
								$("tr.media_comm_row .other_amt").attr('id','other_amt_'+(count+1));
								$("tr.media_comm_row .other_amt").attr('name','other_amt_'+(count+1));

								$("tr.media_comm_row .discount").attr('id','discount_'+(count+1));
								$("tr.media_comm_row .discount").attr('name','discount_'+(count+1));
								$("tr.media_comm_row .disc_amt").attr('id','discount_amt_'+(count+1));
								$("tr.media_comm_row .disc_amt").attr('name','discount_amt_'+(count+1));
								$("tr.media_comm_row .amnt").attr('id','amount_'+(count+1));
								$("tr.media_comm_row .amnt").attr('name','amount_'+(count+1));
								$("tr.media_comm_row .sub_amt_class").attr('id','sub_tot_amount_'+(count+1));
								$("tr.media_comm_row .sub_amt_class").attr('name','sub_tot_amount_'+(count+1));
								$("tr.media_comm_row .hsn_sac_num").attr('id','hsn_sac_'+(count+1));
								$("tr.media_comm_row .hsn_sac_num").attr('name','hsn_sac_'+(count+1));
								$("tr.media_comm_row  .service").attr('id','service_'+(count+1));
								$("tr.media_comm_row  .service").attr('name','service_'+(count+1));
								$("tr.media_comm_row .rate").attr('id','rate_'+(count+1));
								$("tr.media_comm_row .rate").attr('name','rate_'+(count+1));
								$("tr.media_comm_row .row_num").val((count+1));
							}
							if($("#scrol-id tr.late_fee_row").length>0){
								$("tr.late_fee_row").attr("id",'row_'+(count+2));
								$("tr.late_fee_row .particulars").attr('id','particular_'+(count+2));
								$("tr.late_fee_row .particulars").attr('name','particular_'+(count+2));
								$("tr.late_fee_row .qty").attr('id','qty_'+(count+2));
								$("tr.late_fee_row .qty").attr('name','qty_'+(count+2));
								$("tr.late_fee_row .units").attr('id','unit_'+(count+2));
								$("tr.late_fee_row .units").attr('name','unit_'+(count+2));
								$("tr.late_fee_row .branch_igst").attr('id','igst_'+(count+2));
								$("tr.late_fee_row .branch_igst").attr('name','igst_'+(count+2));
								$("tr.late_fee_row .igst_amt").attr('id','igst_amt_'+(count+2));
								$("tr.late_fee_row .igst_amt").attr('name','igst_amt_'+(count+2));
								$("tr.late_fee_row .branch_cgst").attr('id','cgst_'+(count+2));
								$("tr.late_fee_row .branch_cgst").attr('name','cgst_'+(count+2));
								$("tr.late_fee_row .cgst_amt").attr('id','cgst_amt_'+(count+2));
								$("tr.late_fee_row .cgst_amt").attr('name','cgst_amt_'+(count+2));
								$("tr.late_fee_row .branch_sgst").attr('id','sgst_'+(count+2));
								$("tr.late_fee_row .branch_sgst").attr('name','sgst_'+(count+2));
								$("tr.late_fee_row .sgst_amt").attr('id','sgst_amt_'+(count+2));
								$("tr.late_fee_row .sgst_amt").attr('name','sgst_amt_'+(count+2));
								$("tr.late_fee_row .cess").attr('id','cess_'+(count+2));
								$("tr.late_fee_row .cess").attr('name','cess_'+(count+2));
								$("tr.late_fee_row .cess_amt").attr('id','cess_amt_'+(count+2));
								$("tr.late_fee_row .cess_amt").attr('name','cess_amt_'+(count+2));

								$("tr.late_fee_row .other").attr('id','other_'+(count+2));
								$("tr.late_fee_row .other").attr('name','other_'+(count+2));
								$("tr.late_fee_row .other_amt").attr('id','other_amt_'+(count+2));
								$("tr.late_fee_row .other_amt").attr('name','other_amt_'+(count+2));
								$("tr.late_fee_row .discount").attr('id','discount_'+(count+2));
								$("tr.late_fee_row .discount").attr('name','discount_'+(count+2));
								$("tr.late_fee_row .disc_amt").attr('id','discount_amt_'+(count+2));
								$("tr.late_fee_row .disc_amt").attr('name','discount_amt_'+(count+2));
								$("tr.late_fee_row .amnt").attr('id','amount_'+(count+2));
								$("tr.late_fee_row .amnt").attr('name','amount_'+(count+2));
								$("tr.late_fee_row .sub_amt_class").attr('id','sub_tot_amount_'+(count+2));
								$("tr.late_fee_row .sub_amt_class").attr('name','sub_tot_amount_'+(count+2));
								$("tr.late_fee_row .hsn_sac_num").attr('id','hsn_sac_'+(count+2));
								$("tr.late_fee_row .hsn_sac_num").attr('name','hsn_sac_'+(count+2));
								$("tr.late_fee_row  .service").attr('id','service_'+(count+2));
								$("tr.late_fee_row  .service").attr('name','service_'+(count+2));
								$("tr.late_fee_row .rate").attr('id','rate_'+(count+2));
								$("tr.late_fee_row .rate").attr('name','rate_'+(count+2));
								$("tr.late_fee_row .row_num").val((count+2));	
							}
							if($("#scrol-id tr.equ_leavy_row").length>0){
								$("tr.equ_leavy_row").attr("id",'row_'+(count+3));
								$("tr.equ_leavy_row .particulars").attr('id','particular_'+(count+3));
								$("tr.equ_leavy_row .particulars").attr('name','particular_'+(count+3));
								$("tr.equ_leavy_row .qty").attr('id','qty_'+(count+3));
								$("tr.equ_leavy_row .qty").attr('name','qty_'+(count+3));
								$("tr.equ_leavy_row .units").attr('id','unit_'+(count+3));
								$("tr.equ_leavy_row .units").attr('name','unit_'+(count+3));
								$("tr.equ_leavy_row .branch_igst").attr('id','igst_'+(count+3));
								$("tr.equ_leavy_row .branch_igst").attr('name','igst_'+(count+3));
								$("tr.equ_leavy_row .igst_amt").attr('id','igst_amt_'+(count+3));
								$("tr.equ_leavy_row .igst_amt").attr('name','igst_amt_'+(count+3));
								$("tr.equ_leavy_row .branch_cgst").attr('id','cgst_'+(count+3));
								$("tr.equ_leavy_row .branch_cgst").attr('name','cgst_'+(count+3));
								$("tr.equ_leavy_row .cgst_amt").attr('id','cgst_amt_'+(count+3));
								$("tr.equ_leavy_row .cgst_amt").attr('name','cgst_amt_'+(count+3));
								$("tr.equ_leavy_row .branch_sgst").attr('id','sgst_'+(count+3));
								$("tr.equ_leavy_row .branch_sgst").attr('name','sgst_'+(count+3));
								$("tr.equ_leavy_row .sgst_amt").attr('id','sgst_amt_'+(count+3));
								$("tr.equ_leavy_row .sgst_amt").attr('name','sgst_amt_'+(count+3));
								$("tr.equ_leavy_row .cess").attr('id','cess_'+(count+3));
								$("tr.equ_leavy_row .cess").attr('name','cess_'+(count+3));
								$("tr.equ_leavy_row .cess_amt").attr('id','cess_amt_'+(count+3));
								$("tr.equ_leavy_row .cess_amt").attr('name','cess_amt_'+(count+3));
								$("tr.equ_leavy_row .other").attr('id','other_'+(count+3));
								$("tr.equ_leavy_row .other").attr('name','other_'+(count+3));
								$("tr.equ_leavy_row .other_amt").attr('id','other_amt_'+(count+3));
								$("tr.equ_leavy_row .other_amt").attr('name','other_amt_'+(count+3));
								$("tr.equ_leavy_row .discount").attr('id','discount_'+(count+3));
								$("tr.equ_leavy_row .discount").attr('name','discount_'+(count+3));
								$("tr.equ_leavy_row .disc_amt").attr('id','discount_amt_'+(count+3));
								$("tr.equ_leavy_row .disc_amt").attr('name','discount_amt_'+(count+3));
								$("tr.equ_leavy_row .amnt").attr('id','amount_'+(count+3));
								$("tr.equ_leavy_row .amnt").attr('name','amount_'+(count+3));
								$("tr.equ_leavy_row .sub_amt_class").attr('id','sub_tot_amount_'+(count+3));
								$("tr.equ_leavy_row .sub_amt_class").attr('name','sub_tot_amount_'+(count+3));
								$("tr.equ_leavy_row .hsn_sac_num").attr('id','hsn_sac_'+(count+3));
								$("tr.equ_leavy_row .hsn_sac_num").attr('name','hsn_sac_'+(count+3));
								$("tr.equ_leavy_row  .service").attr('id','service_'+(count+3));
								$("tr.equ_leavy_row  .service").attr('name','service_'+(count+3));
								$("tr.equ_leavy_row .rate").attr('id','rate_'+(count+3));
								$("tr.equ_leavy_row .rate").attr('name','rate_'+(count+3));
								$("tr.equ_leavy_row .row_num").val((count+3));
							}	
							$("#scrol-id tbody tr:nth-last-child(1)").before(elemt);
						}
						else{
					 		 $('#scrol-id tbody').before(elemt);
						}
														
					  //$(".service").html(result_opt[0]);
					  // $(".units").html(result_opt[1]);
					  $("#service_"+count).html(result_opt[0]);
					 // $("#service_"+count).material_select();
					  
					  $("#unit_"+count).html(result_opt[1]);
					  $("#unit_"+count).material_select();
					  
					  $('select').not('.disabled').material_select();
						
					$(document).on('change', '.qty,.rate,.cess,.discount', function () {
						var row_id=$(this).attr('id').split("_");
						calculate_total(row_id[1]);
					});
						
						$("#counter").val(count);
						 count++;
					},
			});	
		} else {
			
			var newTr = $('<tr id="row_' + count + '"></tr>');
			$.ajax({
				url:base_url+'My_invoices/get_services',
				type:"POST",
				data:{'csrf_test_name':csrf_hash,},
				success:function(res){
					var result_opt=$.parseJSON(res);
					var elemt = newTr.html('<input type="hidden" name="invoice_array[]" value="' + count + '"><td class="meal over"><div class="scroll-hover-cover"><span class="hoveraction"><a href="#" class="delete_row" id="row_'+count+'" name="row_'+count+'"><img src="'+base_url+'asset/css/img/icons/delete.png"></a></span><select class="select-wrapper tabl-select border-radius-6 btn-dropdown-select select-like-dropdown create-bil-suyp sales-in-drop service" id="service_'+count+'" name="service_'+count+'"><option value="">Select Service</option></select></td><td class="ali-lef"><input type="text"  required="required" autofocus="autofocus" class="form-control" id="particular_' + count + '" value="" name="particular_' + count + '"></td><td><input type="text"  required="required" autofocus="autofocus" class="form-control" id="hsn_sac_' + count + '" value="" name="hsn_sac_' + count + '"></td><td><input type="text"  required="required" autofocus="autofocus" class="form-control qty" id="qty_' + count + '" name="qty_' + count + '" value="1"></td><td><input type="text"  required="required" autofocus="autofocus" class="form-control rate" id="rate_' + count + '" name="rate_' + count + '" value="0"></td><td class="meal"><input type="text" required="required" autofocus="autofocus" class="form-control discount" id="discount_' + count + '" value="0.00" name="discount_' + count + '"><input type="hidden" required="required" autofocus="autofocus" class="form-control disc_amt" id="discount_amt_' + count + '" value="0.00" name="discount_amt_' + count + '"></td><td class="meal"><input type="text" autofocus="autofocus" class="igst_amt form-control" id="igst_amt_' + count + '" name="igst_amt_' + count + '" value="0.00" readonly ></td><td class="meal"><input type="text" autofocus="autofocus" class="branch_igst form-control" id="igst_' + count + '" name="igst_' + count + '" value="'+ $("#igst").val() +'" readonly></td><td class="meal"><input type="text" autofocus="autofocus" class="cgst_amt form-control" id="cgst_amt_' + count + '" value="0.00" name="cgst_amt_' + count + '" readonly ></td><td class="meal"><input type="text" autofocus="autofocus" class="branch_cgst form-control" id="cgst_' + count + '" name="cgst_' + count + '"  value="'+ $("#cgst").val() +'" readonly></td><td class="meal"><input type="text" autofocus="autofocus" class="sgst_amt form-control" id="sgst_amt_' + count + '" value="0.00" name="sgst_amt_' + count + '" readonly ></td><td class="meal"><input type="text" autofocus="autofocus" class="branch_sgst form-control" id="sgst_' + count + '" name="sgst_' + count + '" value="'+ $("#sgst").val() +'" readonly></td><td><input type="text" autofocus="autofocus" class="form-control cess" id="cess_' + count + '" value="0" name="cess_' + count + '"><input type="hidden" autofocus="autofocus" class="cess_amt form-control" id="cess_amt_' + count + '" value="" name="cess_amt_' + count + '" readonly ></td><td class="last-btr"></td><td class="fix num-s meal credit-ntns"><input type="text" autofocus="autofocus" class="form-control amnt" id="amount_' + count + '" value="0" name="amount_' + count + '" readonly ><input type="hidden" class="sub_amt_class" id="sub_tot_amount_' + count + '" value="0" name="sub_tot_amount_' + count + '" readonly ></td>');
						
					 // $('#scrol-id tbody').append(elemt);
					if($("#scrol-id tr.media_comm_row").length>0 && $("#scrol-id tr.late_fee_row").length>0 && $("#scrol-id tr.equ_leavy_row").length>0){
						$("tr.media_comm_row").attr("id",'row_'+(count+1));
						$("tr.media_comm_row .particulars").attr('id','particular_'+(count+1));
						$("tr.media_comm_row .particulars").attr('name','particular_'+(count+1));
						$("tr.media_comm_row .qty").attr('id','qty_'+(count+1));
						$("tr.media_comm_row .qty").attr('name','qty_'+(count+1));
						$("tr.media_comm_row .branch_igst").attr('id','igst_'+(count+1));
						$("tr.media_comm_row .branch_igst").attr('name','igst_'+(count+1));
						$("tr.media_comm_row .igst_amt").attr('id','igst_amt_'+(count+1));
						$("tr.media_comm_row .igst_amt").attr('name','igst_amt_'+(count+1));
						$("tr.media_comm_row .branch_cgst").attr('id','cgst_'+(count+1));
						$("tr.media_comm_row .branch_cgst").attr('name','cgst_'+(count+1));
						$("tr.media_comm_row .cgst_amt").attr('id','cgst_amt_'+(count+1));
						$("tr.media_comm_row .cgst_amt").attr('name','cgst_amt_'+(count+1));
						$("tr.media_comm_row .branch_sgst").attr('id','sgst_'+(count+1));
						$("tr.media_comm_row .branch_sgst").attr('name','sgst_'+(count+1));
						$("tr.media_comm_row .sgst_amt").attr('id','sgst_amt_'+(count+1));
						$("tr.media_comm_row .sgst_amt").attr('name','sgst_amt_'+(count+1));
						$("tr.media_comm_row .cess").attr('id','cess_'+(count+1));
						$("tr.media_comm_row .cess").attr('name','cess_'+(count+1));
						$("tr.media_comm_row .cess_amt").attr('id','cess_amt_'+(count+1));
						$("tr.media_comm_row .cess_amt").attr('name','cess_amt_'+(count+1));

                        $("tr.media_comm_row .other").attr('id','other_'+(count+1));
						$("tr.media_comm_row .other").attr('name','other_'+(count+1));
						$("tr.media_comm_row .other_amt").attr('id','other_amt_'+(count+1));
						$("tr.media_comm_row .other_amt").attr('name','other_amt_'+(count+1));

						$("tr.media_comm_row .discount").attr('id','discount_'+(count+1));
						$("tr.media_comm_row .discount").attr('name','discount_'+(count+1));
						$("tr.media_comm_row .disc_amt").attr('id','discount_amt_'+(count+1));
						$("tr.media_comm_row .disc_amt").attr('name','discount_amt_'+(count+1));
						$("tr.media_comm_row .amnt").attr('id','amount_'+(count+1));
						$("tr.media_comm_row .amnt").attr('name','amount_'+(count+1));
						$("tr.media_comm_row .sub_amt_class").attr('id','sub_tot_amount_'+(count+1));
						$("tr.media_comm_row .sub_amt_class").attr('name','sub_tot_amount_'+(count+1));
						$("tr.media_comm_row .hsn_sac_num").attr('id','hsn_sac_'+(count+1));
						$("tr.media_comm_row .hsn_sac_num").attr('name','hsn_sac_'+(count+1));
						$("tr.media_comm_row  .service").attr('id','service_'+(count+1));
						$("tr.media_comm_row  .service").attr('name','service_'+(count+1));
						$("tr.media_comm_row .rate").attr('id','rate_'+(count+1));
						$("tr.media_comm_row .rate").attr('name','rate_'+(count+1));
						$("tr.media_comm_row .row_num").val((count+1));
							
						$("tr.late_fee_row").attr("id",'row_'+(count+2));
						$("tr.late_fee_row .particulars").attr('id','particular_'+(count+2));
						$("tr.late_fee_row .particulars").attr('name','particular_'+(count+2));
						$("tr.late_fee_row .qty").attr('id','qty_'+(count+2));
						$("tr.late_fee_row .qty").attr('name','qty_'+(count+2));
						$("tr.late_fee_row .branch_igst").attr('id','igst_'+(count+2));
						$("tr.late_fee_row .branch_igst").attr('name','igst_'+(count+2));
						$("tr.late_fee_row .igst_amt").attr('id','igst_amt_'+(count+2));
						$("tr.late_fee_row .igst_amt").attr('name','igst_amt_'+(count+2));
						$("tr.late_fee_row .branch_cgst").attr('id','cgst_'+(count+2));
						$("tr.late_fee_row .branch_cgst").attr('name','cgst_'+(count+2));
						$("tr.late_fee_row .cgst_amt").attr('id','cgst_amt_'+(count+2));
						$("tr.late_fee_row .cgst_amt").attr('name','cgst_amt_'+(count+2));
						$("tr.late_fee_row .branch_sgst").attr('id','sgst_'+(count+2));
						$("tr.late_fee_row .branch_sgst").attr('name','sgst_'+(count+2));
						$("tr.late_fee_row .sgst_amt").attr('id','sgst_amt_'+(count+2));
						$("tr.late_fee_row .sgst_amt").attr('name','sgst_amt_'+(count+2));
						$("tr.late_fee_row .cess").attr('id','cess_'+(count+2));
						$("tr.late_fee_row .cess").attr('name','cess_'+(count+2));
						$("tr.late_fee_row .cess_amt").attr('id','cess_amt_'+(count+2));
						$("tr.late_fee_row .cess_amt").attr('name','cess_amt_'+(count+2));
                        $("tr.late_fee_row .other").attr('id','other_'+(count+2));
						$("tr.late_fee_row .other").attr('name','other_'+(count+2));
						$("tr.late_fee_row .other_amt").attr('id','other_amt_'+(count+2));
						$("tr.late_fee_row .other_amt").attr('name','other_amt_'+(count+2));

						$("tr.late_fee_row .discount").attr('id','discount_'+(count+2));
						$("tr.late_fee_row .discount").attr('name','discount_'+(count+2));
						$("tr.late_fee_row .disc_amt").attr('id','discount_amt_'+(count+2));
						$("tr.late_fee_row .disc_amt").attr('name','discount_amt_'+(count+2));
						$("tr.late_fee_row .amnt").attr('id','amount_'+(count+2));
						$("tr.late_fee_row .amnt").attr('name','amount_'+(count+2));
						$("tr.late_fee_row .sub_amt_class").attr('id','sub_tot_amount_'+(count+2));
						$("tr.late_fee_row .sub_amt_class").attr('name','sub_tot_amount_'+(count+2));
						$("tr.late_fee_row .hsn_sac_num").attr('id','hsn_sac_'+(count+2));
						$("tr.late_fee_row .hsn_sac_num").attr('name','hsn_sac_'+(count+2));
						$("tr.late_fee_row  .service").attr('id','service_'+(count+2));
						$("tr.late_fee_row  .service").attr('name','service_'+(count+2));
						$("tr.late_fee_row .rate").attr('id','rate_'+(count+2));
						$("tr.late_fee_row .rate").attr('name','rate_'+(count+2));
						$("tr.late_fee_row .row_num").val((count+2));	
						
						$("tr.equ_leavy_row").attr("id",'row_'+(count+3));
						$("tr.equ_leavy_row .particulars").attr('id','particular_'+(count+3));
						$("tr.equ_leavy_row .particulars").attr('name','particular_'+(count+3));
						$("tr.equ_leavy_row .qty").attr('id','qty_'+(count+3));
						$("tr.equ_leavy_row .qty").attr('name','qty_'+(count+3));
						$("tr.equ_leavy_row .branch_igst").attr('id','igst_'+(count+3));
						$("tr.equ_leavy_row .branch_igst").attr('name','igst_'+(count+3));
						$("tr.equ_leavy_row .igst_amt").attr('id','igst_amt_'+(count+3));
						$("tr.equ_leavy_row .igst_amt").attr('name','igst_amt_'+(count+3));
						$("tr.equ_leavy_row .branch_cgst").attr('id','cgst_'+(count+3));
						$("tr.equ_leavy_row .branch_cgst").attr('name','cgst_'+(count+3));
						$("tr.equ_leavy_row .cgst_amt").attr('id','cgst_amt_'+(count+3));
						$("tr.equ_leavy_row .cgst_amt").attr('name','cgst_amt_'+(count+3));
						$("tr.equ_leavy_row .branch_sgst").attr('id','sgst_'+(count+3));
						$("tr.equ_leavy_row .branch_sgst").attr('name','sgst_'+(count+3));
						$("tr.equ_leavy_row .sgst_amt").attr('id','sgst_amt_'+(count+3));
						$("tr.equ_leavy_row .sgst_amt").attr('name','sgst_amt_'+(count+3));
						$("tr.equ_leavy_row .cess").attr('id','cess_'+(count+3));
						$("tr.equ_leavy_row .cess").attr('name','cess_'+(count+3));
						$("tr.equ_leavy_row .cess_amt").attr('id','cess_amt_'+(count+3));
						$("tr.equ_leavy_row .cess_amt").attr('name','cess_amt_'+(count+3));
                        $("tr.equ_leavy_row .other").attr('id','other_'+(count+3));
						$("tr.equ_leavy_row .other").attr('name','other_'+(count+3));
						$("tr.equ_leavy_row .other_amt").attr('id','other_amt_'+(count+3));
						$("tr.equ_leavy_row .other_amt").attr('name','other_amt_'+(count+3));

						$("tr.equ_leavy_row .discount").attr('id','discount_'+(count+3));
						$("tr.equ_leavy_row .discount").attr('name','discount_'+(count+3));
						$("tr.equ_leavy_row .disc_amt").attr('id','discount_amt_'+(count+3));
						$("tr.equ_leavy_row .disc_amt").attr('name','discount_amt_'+(count+3));
						$("tr.equ_leavy_row .amnt").attr('id','amount_'+(count+3));
						$("tr.equ_leavy_row .amnt").attr('name','amount_'+(count+3));
						$("tr.equ_leavy_row .sub_amt_class").attr('id','sub_tot_amount_'+(count+3));
						$("tr.equ_leavy_row .sub_amt_class").attr('name','sub_tot_amount_'+(count+3));
						$("tr.equ_leavy_row .hsn_sac_num").attr('id','hsn_sac_'+(count+3));
						$("tr.equ_leavy_row .hsn_sac_num").attr('name','hsn_sac_'+(count+3));
						$("tr.equ_leavy_row  .service").attr('id','service_'+(count+3));
						$("tr.equ_leavy_row  .service").attr('name','service_'+(count+3));
						$("tr.equ_leavy_row .rate").attr('id','rate_'+(count+3));
						$("tr.equ_leavy_row .rate").attr('name','rate_'+(count+3));
						$("tr.equ_leavy_row .row_num").val((count+3));
							
							$("#scrol-id tbody tr:nth-last-child(3)").before(elemt);
						}
						else if(($("#scrol-id tr.media_comm_row").length>0 && $("#scrol-id tr.late_fee_row").length>0) || ($("#scrol-id tr.media_comm_row").length>0 &&  $("#scrol-id tr.equ_leavy_row").length>0) || ($("#scrol-id tr.late_fee_row").length>0 && $("#scrol-id tr.equ_leavy_row").length>0)){
							if($("#scrol-id tr.media_comm_row").length>0){
								$("tr.media_comm_row").attr("id",'row_'+(count+1));
								$("tr.media_comm_row .particulars").attr('id','particular_'+(count+1));
								$("tr.media_comm_row .particulars").attr('name','particular_'+(count+1));
								$("tr.media_comm_row .qty").attr('id','qty_'+(count+1));
								$("tr.media_comm_row .qty").attr('name','qty_'+(count+1));
								$("tr.media_comm_row .branch_igst").attr('id','igst_'+(count+1));
								$("tr.media_comm_row .branch_igst").attr('name','igst_'+(count+1));
								$("tr.media_comm_row .igst_amt").attr('id','igst_amt_'+(count+1));
								$("tr.media_comm_row .igst_amt").attr('name','igst_amt_'+(count+1));
								$("tr.media_comm_row .branch_cgst").attr('id','cgst_'+(count+1));
								$("tr.media_comm_row .branch_cgst").attr('name','cgst_'+(count+1));
								$("tr.media_comm_row .cgst_amt").attr('id','cgst_amt_'+(count+1));
								$("tr.media_comm_row .cgst_amt").attr('name','cgst_amt_'+(count+1));
								$("tr.media_comm_row .branch_sgst").attr('id','sgst_'+(count+1));
								$("tr.media_comm_row .branch_sgst").attr('name','sgst_'+(count+1));
								$("tr.media_comm_row .sgst_amt").attr('id','sgst_amt_'+(count+1));
								$("tr.media_comm_row .sgst_amt").attr('name','sgst_amt_'+(count+1));
								$("tr.media_comm_row .cess").attr('id','cess_'+(count+1));
								$("tr.media_comm_row .cess").attr('name','cess_'+(count+1));
								$("tr.media_comm_row .cess_amt").attr('id','cess_amt_'+(count+1));
								$("tr.media_comm_row .cess_amt").attr('name','cess_amt_'+(count+1));
                             
                                $("tr.media_comm_row .other").attr('id','other_'+(count+1));
								$("tr.media_comm_row .other").attr('name','other_'+(count+1));
								$("tr.media_comm_row .other_amt").attr('id','other_amt_'+(count+1));
								$("tr.media_comm_row .other_amt").attr('name','other_amt_'+(count+1));
								$("tr.media_comm_row .discount").attr('id','discount_'+(count+1));
								$("tr.media_comm_row .discount").attr('name','discount_'+(count+1));
								$("tr.media_comm_row .disc_amt").attr('id','discount_amt_'+(count+1));
								$("tr.media_comm_row .disc_amt").attr('name','discount_amt_'+(count+1));
								$("tr.media_comm_row .amnt").attr('id','amount_'+(count+1));
								$("tr.media_comm_row .amnt").attr('name','amount_'+(count+1));
								$("tr.media_comm_row .sub_amt_class").attr('id','sub_tot_amount_'+(count+1));
								$("tr.media_comm_row .sub_amt_class").attr('name','sub_tot_amount_'+(count+1));
								$("tr.media_comm_row .hsn_sac_num").attr('id','hsn_sac_'+(count+1));
								$("tr.media_comm_row .hsn_sac_num").attr('name','hsn_sac_'+(count+1));
								$("tr.media_comm_row .service").attr('id','service_'+(count+1));
								$("tr.media_comm_row .service").attr('name','service_'+(count+1));
								$("tr.media_comm_row .rate").attr('id','rate_'+(count+1));
								$("tr.media_comm_row .rate").attr('name','rate_'+(count+1));
								$("tr.media_comm_row .row_num").val((count+1));
							}
							if($("#scrol-id tr.late_fee_row").length>0){
								$("tr.late_fee_row").attr("id",'row_'+(count+2));
								$("tr.late_fee_row .particulars").attr('id','particular_'+(count+2));
								$("tr.late_fee_row .particulars").attr('name','particular_'+(count+2));
								$("tr.late_fee_row .qty").attr('id','qty_'+(count+2));
								$("tr.late_fee_row .qty").attr('name','qty_'+(count+2));
								$("tr.late_fee_row .branch_igst").attr('id','igst_'+(count+2));
								$("tr.late_fee_row .branch_igst").attr('name','igst_'+(count+2));
								$("tr.late_fee_row .igst_amt").attr('id','igst_amt_'+(count+2));
								$("tr.late_fee_row .igst_amt").attr('name','igst_amt_'+(count+2));
								$("tr.late_fee_row .branch_cgst").attr('id','cgst_'+(count+2));
								$("tr.late_fee_row .branch_cgst").attr('name','cgst_'+(count+2));
								$("tr.late_fee_row .cgst_amt").attr('id','cgst_amt_'+(count+2));
								$("tr.late_fee_row .cgst_amt").attr('name','cgst_amt_'+(count+2));
								$("tr.late_fee_row .branch_sgst").attr('id','sgst_'+(count+2));
								$("tr.late_fee_row .branch_sgst").attr('name','sgst_'+(count+2));
								$("tr.late_fee_row .sgst_amt").attr('id','sgst_amt_'+(count+2));
								$("tr.late_fee_row .sgst_amt").attr('name','sgst_amt_'+(count+2));
								$("tr.late_fee_row .cess").attr('id','cess_'+(count+2));
								$("tr.late_fee_row .cess").attr('name','cess_'+(count+2));
								$("tr.late_fee_row .cess_amt").attr('id','cess_amt_'+(count+2));
								$("tr.late_fee_row .cess_amt").attr('name','cess_amt_'+(count+2));
								$("tr.late_fee_row .other").attr('id','other_'+(count+2));
								$("tr.late_fee_row .other").attr('name','other_'+(count+2));
								$("tr.late_fee_row .other_amt").attr('id','other_amt_'+(count+2));
								$("tr.late_fee_row .other_amt").attr('name','other_amt_'+(count+2));
								$("tr.late_fee_row .discount").attr('id','discount_'+(count+2));
								$("tr.late_fee_row .discount").attr('name','discount_'+(count+2));
								$("tr.late_fee_row .disc_amt").attr('id','discount_amt_'+(count+2));
								$("tr.late_fee_row .disc_amt").attr('name','discount_amt_'+(count+2));
								$("tr.late_fee_row .amnt").attr('id','amount_'+(count+2));
								$("tr.late_fee_row .amnt").attr('name','amount_'+(count+2));
								$("tr.late_fee_row .sub_amt_class").attr('id','sub_tot_amount_'+(count+2));
								$("tr.late_fee_row .sub_amt_class").attr('name','sub_tot_amount_'+(count+2));
								$("tr.late_fee_row .hsn_sac_num").attr('id','hsn_sac_'+(count+2));
								$("tr.late_fee_row .hsn_sac_num").attr('name','hsn_sac_'+(count+2));
								$("tr.late_fee_row  .service").attr('id','service_'+(count+2));
								$("tr.late_fee_row  .service").attr('name','service_'+(count+2));
								$("tr.late_fee_row .rate").attr('id','rate_'+(count+2));
								$("tr.late_fee_row .rate").attr('name','rate_'+(count+2));
								$("tr.late_fee_row .row_num").val((count+2));	
							}
							if($("#scrol-id tr.equ_leavy_row").length>0){
								$("tr.equ_leavy_row").attr("id",'row_'+(count+3));
								$("tr.equ_leavy_row .particulars").attr('id','particular_'+(count+3));
								$("tr.equ_leavy_row .particulars").attr('name','particular_'+(count+3));
								$("tr.equ_leavy_row .qty").attr('id','qty_'+(count+3));
								$("tr.equ_leavy_row .qty").attr('name','qty_'+(count+3));
								$("tr.equ_leavy_row .branch_igst").attr('id','igst_'+(count+3));
								$("tr.equ_leavy_row .branch_igst").attr('name','igst_'+(count+3));
								$("tr.equ_leavy_row .igst_amt").attr('id','igst_amt_'+(count+3));
								$("tr.equ_leavy_row .igst_amt").attr('name','igst_amt_'+(count+3));
								$("tr.equ_leavy_row .branch_cgst").attr('id','cgst_'+(count+3));
								$("tr.equ_leavy_row .branch_cgst").attr('name','cgst_'+(count+3));
								$("tr.equ_leavy_row .cgst_amt").attr('id','cgst_amt_'+(count+3));
								$("tr.equ_leavy_row .cgst_amt").attr('name','cgst_amt_'+(count+3));
								$("tr.equ_leavy_row .branch_sgst").attr('id','sgst_'+(count+3));
								$("tr.equ_leavy_row .branch_sgst").attr('name','sgst_'+(count+3));
								$("tr.equ_leavy_row .sgst_amt").attr('id','sgst_amt_'+(count+3));
								$("tr.equ_leavy_row .sgst_amt").attr('name','sgst_amt_'+(count+3));
								$("tr.equ_leavy_row .cess").attr('id','cess_'+(count+3));
								$("tr.equ_leavy_row .cess").attr('name','cess_'+(count+3));
								$("tr.equ_leavy_row .cess_amt").attr('id','cess_amt_'+(count+3));
								$("tr.equ_leavy_row .cess_amt").attr('name','cess_amt_'+(count+3));
                                $("tr.equ_leavy_row .other").attr('id','other_'+(count+3));
								$("tr.equ_leavy_row .other").attr('name','other_'+(count+3));
								$("tr.equ_leavy_row .other_amt").attr('id','other_amt_'+(count+3));
								$("tr.equ_leavy_row .other_amt").attr('name','other_amt_'+(count+3));

								$("tr.equ_leavy_row .discount").attr('id','discount_'+(count+3));
								$("tr.equ_leavy_row .discount").attr('name','discount_'+(count+3));
								$("tr.equ_leavy_row .disc_amt").attr('id','discount_amt_'+(count+3));
								$("tr.equ_leavy_row .disc_amt").attr('name','discount_amt_'+(count+3));
								$("tr.equ_leavy_row .amnt").attr('id','amount_'+(count+3));
								$("tr.equ_leavy_row .amnt").attr('name','amount_'+(count+3));
								$("tr.equ_leavy_row .sub_amt_class").attr('id','sub_tot_amount_'+(count+3));
								$("tr.equ_leavy_row .sub_amt_class").attr('name','sub_tot_amount_'+(count+3));
								$("tr.equ_leavy_row .hsn_sac_num").attr('id','hsn_sac_'+(count+3));
								$("tr.equ_leavy_row .hsn_sac_num").attr('name','hsn_sac_'+(count+3));
								$("tr.equ_leavy_row  .service").attr('id','service_'+(count+3));
								$("tr.equ_leavy_row  .service").attr('name','service_'+(count+3));
								$("tr.equ_leavy_row .rate").attr('id','rate_'+(count+3));
								$("tr.equ_leavy_row .rate").attr('name','rate_'+(count+3));
								$("tr.equ_leavy_row .row_num").val((count+3));
							}
							$("#scrol-id tbody tr:nth-last-child(2)").before(elemt);
						}
						else if($("#scrol-id tr.media_comm_row").length>0 || $("#scrol-id tr.late_fee_row").length>0
 || $("#scrol-id tr.equ_leavy_row").length>0
)
						{
							if($("#scrol-id tr.media_comm_row").length>0){
								$("tr.media_comm_row").attr("id",'row_'+(count+1));
								$("tr.media_comm_row .particulars").attr('id','particular_'+(count+1));
								$("tr.media_comm_row .particulars").attr('name','particular_'+(count+1));
								$("tr.media_comm_row .qty").attr('id','qty_'+(count+1));
								$("tr.media_comm_row .qty").attr('name','qty_'+(count+1));
								$("tr.media_comm_row .branch_igst").attr('id','igst_'+(count+1));
								$("tr.media_comm_row .branch_igst").attr('name','igst_'+(count+1));
								$("tr.media_comm_row .igst_amt").attr('id','igst_amt_'+(count+1));
								$("tr.media_comm_row .igst_amt").attr('name','igst_amt_'+(count+1));
								$("tr.media_comm_row .branch_cgst").attr('id','cgst_'+(count+1));
								$("tr.media_comm_row .branch_cgst").attr('name','cgst_'+(count+1));
								$("tr.media_comm_row .cgst_amt").attr('id','cgst_amt_'+(count+1));
								$("tr.media_comm_row .cgst_amt").attr('name','cgst_amt_'+(count+1));
								$("tr.media_comm_row .branch_sgst").attr('id','sgst_'+(count+1));
								$("tr.media_comm_row .branch_sgst").attr('name','sgst_'+(count+1));
								$("tr.media_comm_row .sgst_amt").attr('id','sgst_amt_'+(count+1));
								$("tr.media_comm_row .sgst_amt").attr('name','sgst_amt_'+(count+1));
								$("tr.media_comm_row .cess").attr('id','cess_'+(count+1));
								$("tr.media_comm_row .cess").attr('name','cess_'+(count+1));
								$("tr.media_comm_row .cess_amt").attr('id','cess_amt_'+(count+1));
								$("tr.media_comm_row .cess_amt").attr('name','cess_amt_'+(count+1));
                                $("tr.media_comm_row .other").attr('id','other_'+(count+1));
								$("tr.media_comm_row .other").attr('name','other_'+(count+1));
								$("tr.media_comm_row .other_amt").attr('id','other_amt_'+(count+1));
								$("tr.media_comm_row .other_amt").attr('name','other_amt_'+(count+1));

								$("tr.media_comm_row .discount").attr('id','discount_'+(count+1));
								$("tr.media_comm_row .discount").attr('name','discount_'+(count+1));
								$("tr.media_comm_row .disc_amt").attr('id','discount_amt_'+(count+1));
								$("tr.media_comm_row .disc_amt").attr('name','discount_amt_'+(count+1));
								$("tr.media_comm_row .amnt").attr('id','amount_'+(count+1));
								$("tr.media_comm_row .amnt").attr('name','amount_'+(count+1));
								$("tr.media_comm_row .sub_amt_class").attr('id','sub_tot_amount_'+(count+1));
								$("tr.media_comm_row .sub_amt_class").attr('name','sub_tot_amount_'+(count+1));
								$("tr.media_comm_row .hsn_sac_num").attr('id','hsn_sac_'+(count+1));
								$("tr.media_comm_row .hsn_sac_num").attr('name','hsn_sac_'+(count+1));
								$("tr.media_comm_row  .service").attr('id','service_'+(count+1));
								$("tr.media_comm_row  .service").attr('name','service_'+(count+1));
								$("tr.media_comm_row .rate").attr('id','rate_'+(count+1));
								$("tr.media_comm_row .rate").attr('name','rate_'+(count+1));
								$("tr.media_comm_row .row_num").val((count+1));
							}
							if($("#scrol-id tr.late_fee_row").length>0){
								$("tr.late_fee_row").attr("id",'row_'+(count+2));
								$("tr.late_fee_row .particulars").attr('id','particular_'+(count+2));
								$("tr.late_fee_row .particulars").attr('name','particular_'+(count+2));
								$("tr.late_fee_row .qty").attr('id','qty_'+(count+2));
								$("tr.late_fee_row .qty").attr('name','qty_'+(count+2));
								$("tr.late_fee_row .branch_igst").attr('id','igst_'+(count+2));
								$("tr.late_fee_row .branch_igst").attr('name','igst_'+(count+2));
								$("tr.late_fee_row .igst_amt").attr('id','igst_amt_'+(count+2));
								$("tr.late_fee_row .igst_amt").attr('name','igst_amt_'+(count+2));
								$("tr.late_fee_row .branch_cgst").attr('id','cgst_'+(count+2));
								$("tr.late_fee_row .branch_cgst").attr('name','cgst_'+(count+2));
								$("tr.late_fee_row .cgst_amt").attr('id','cgst_amt_'+(count+2));
								$("tr.late_fee_row .cgst_amt").attr('name','cgst_amt_'+(count+2));
								$("tr.late_fee_row .branch_sgst").attr('id','sgst_'+(count+2));
								$("tr.late_fee_row .branch_sgst").attr('name','sgst_'+(count+2));
								$("tr.late_fee_row .sgst_amt").attr('id','sgst_amt_'+(count+2));
								$("tr.late_fee_row .sgst_amt").attr('name','sgst_amt_'+(count+2));
								$("tr.late_fee_row .cess").attr('id','cess_'+(count+2));
								$("tr.late_fee_row .cess").attr('name','cess_'+(count+2));
								$("tr.late_fee_row .cess_amt").attr('id','cess_amt_'+(count+2));
								$("tr.late_fee_row .cess_amt").attr('name','cess_amt_'+(count+2));
                                 $("tr.late_fee_row .other").attr('id','other_'+(count+2));
								$("tr.late_fee_row .other").attr('name','other_'+(count+2));
								$("tr.late_fee_row .other_amt").attr('id','other_amt_'+(count+2));
								$("tr.late_fee_row .other_amt").attr('name','other_amt_'+(count+2));

								$("tr.late_fee_row .discount").attr('id','discount_'+(count+2));
								$("tr.late_fee_row .discount").attr('name','discount_'+(count+2));
								$("tr.late_fee_row .disc_amt").attr('id','discount_amt_'+(count+2));
								$("tr.late_fee_row .disc_amt").attr('name','discount_amt_'+(count+2));
								$("tr.late_fee_row .amnt").attr('id','amount_'+(count+2));
								$("tr.late_fee_row .amnt").attr('name','amount_'+(count+2));
								$("tr.late_fee_row .sub_amt_class").attr('id','sub_tot_amount_'+(count+2));
								$("tr.late_fee_row .sub_amt_class").attr('name','sub_tot_amount_'+(count+2));
								$("tr.late_fee_row .hsn_sac_num").attr('id','hsn_sac_'+(count+2));
								$("tr.late_fee_row .hsn_sac_num").attr('name','hsn_sac_'+(count+2));
								$("tr.late_fee_row  .service").attr('id','service_'+(count+2));
								$("tr.late_fee_row  .service").attr('name','service_'+(count+2));
								$("tr.late_fee_row .rate").attr('id','rate_'+(count+2));
								$("tr.late_fee_row .rate").attr('name','rate_'+(count+2));
								$("tr.late_fee_row .row_num").val((count+2));	
							}
							if($("#scrol-id tr.equ_leavy_row").length>0){
								$("tr.equ_leavy_row").attr("id",'row_'+(count+3));
								$("tr.equ_leavy_row .particulars").attr('id','particular_'+(count+3));
								$("tr.equ_leavy_row .particulars").attr('name','particular_'+(count+3));
								$("tr.equ_leavy_row .qty").attr('id','qty_'+(count+3));
								$("tr.equ_leavy_row .qty").attr('name','qty_'+(count+3));
								$("tr.equ_leavy_row .branch_igst").attr('id','igst_'+(count+3));
								$("tr.equ_leavy_row .branch_igst").attr('name','igst_'+(count+3));
								$("tr.equ_leavy_row .igst_amt").attr('id','igst_amt_'+(count+3));
								$("tr.equ_leavy_row .igst_amt").attr('name','igst_amt_'+(count+3));
								$("tr.equ_leavy_row .branch_cgst").attr('id','cgst_'+(count+3));
								$("tr.equ_leavy_row .branch_cgst").attr('name','cgst_'+(count+3));
								$("tr.equ_leavy_row .cgst_amt").attr('id','cgst_amt_'+(count+3));
								$("tr.equ_leavy_row .cgst_amt").attr('name','cgst_amt_'+(count+3));
								$("tr.equ_leavy_row .branch_sgst").attr('id','sgst_'+(count+3));
								$("tr.equ_leavy_row .branch_sgst").attr('name','sgst_'+(count+3));
								$("tr.equ_leavy_row .sgst_amt").attr('id','sgst_amt_'+(count+3));
								$("tr.equ_leavy_row .sgst_amt").attr('name','sgst_amt_'+(count+3));
								$("tr.equ_leavy_row .cess").attr('id','cess_'+(count+3));
								$("tr.equ_leavy_row .cess").attr('name','cess_'+(count+3));
								$("tr.equ_leavy_row .cess_amt").attr('id','cess_amt_'+(count+3));
								$("tr.equ_leavy_row .cess_amt").attr('name','cess_amt_'+(count+3));
                                 $("tr.equ_leavy_row .other").attr('id','other_'+(count+3));
								$("tr.equ_leavy_row .other").attr('name','other_'+(count+3));
								$("tr.equ_leavy_row .other_amt").attr('id','other_amt_'+(count+3));
								$("tr.equ_leavy_row .other_amt").attr('name','other_amt_'+(count+3));

								$("tr.equ_leavy_row .discount").attr('id','discount_'+(count+3));
								$("tr.equ_leavy_row .discount").attr('name','discount_'+(count+3));
								$("tr.equ_leavy_row .disc_amt").attr('id','discount_amt_'+(count+3));
								$("tr.equ_leavy_row .disc_amt").attr('name','discount_amt_'+(count+3));
								$("tr.equ_leavy_row .amnt").attr('id','amount_'+(count+3));
								$("tr.equ_leavy_row .amnt").attr('name','amount_'+(count+3));
								$("tr.equ_leavy_row .sub_amt_class").attr('id','sub_tot_amount_'+(count+3));
								$("tr.equ_leavy_row .sub_amt_class").attr('name','sub_tot_amount_'+(count+3));
								$("tr.equ_leavy_row .hsn_sac_num").attr('id','hsn_sac_'+(count+3));
								$("tr.equ_leavy_row .hsn_sac_num").attr('name','hsn_sac_'+(count+3));
								$("tr.equ_leavy_row  .service").attr('id','service_'+(count+3));
								$("tr.equ_leavy_row  .service").attr('name','service_'+(count+3));
								$("tr.equ_leavy_row .rate").attr('id','rate_'+(count+3));
								$("tr.equ_leavy_row .rate").attr('name','rate_'+(count+3));
								$("tr.equ_leavy_row .row_num").val((count+3));
							}	
							$("#scrol-id tbody tr:nth-last-child(1)").before(elemt);
						}
						else{
					 		 $('#scrol-id tbody').before(elemt);
						};
						
					  $("#service_"+count).html(result_opt[0]);
					  $("#service_"+count).material_select();

					  $('select').not('.disabled').material_select();
						$(document).on('change', '.qty,.rate,.cess,.discount', function () {
							var row_id=$(this).attr('id').split("_");
							calculate_total(row_id[1]);
						});
					  $("#counter").val(count);
						 count++;
					},
			});		
		}
}
});
$(document).ready( function() {	
	
	
	$('#customer_id').on('change' ,function(){
				var customer_id = $(this).val();
				var company_id=$("#header_company_profiles").val();
				if(customer_id == 'add_new_customer'){
					$('#add_customer_modal').modal('open');
					$.ajax({
					url:base_url+'My_invoices/get_all_countries',
					type:"POST",
					data:{'csrf_test_name':csrf_hash,},
					success:function(res){
						$("#add_customer_modal #new_cust_country").html(res);
						$("#add_customer_modal #new_cust_country").parents('.input-field').addClass('label-active');
						$('#add_customer_modal #new_cust_country').material_select();
						$("#add_customer_modal #new_cust_ship_country").html(res);
						$("#add_customer_modal #new_cust_ship_country").parents('.input-field').addClass('label-active');
						
						$('#add_customer_modal #new_cust_ship_country').material_select();
						},
					});
					
					$("#new_cust_country").on("change",function(){
						var country_id = $(this).val();
						$.ajax({
							url:base_url+'Company_profile/get_states',
							type:"POST",
							data:{'csrf_test_name':csrf_hash,'country_id':country_id},
							success:function(res){
								$("#new_cust_state").html(res);
								$("#new_cust_state").parents('.input-field').addClass('label-active');
								$('#new_cust_state').material_select();
							},
						});
					});
					
					$("#new_cust_ship_country").on("change",function(){
						var country_id = $(this).val();
						$.ajax({
							url:base_url+'Company_profile/get_states',
							type:"POST",
							data:{'csrf_test_name':csrf_hash,'country_id':country_id},
							success:function(res){
								$("#new_cust_ship_state").html(res);
								$("#new_cust_ship_state").parents('.input-field').addClass('label-active');
								$('#new_cust_ship_state').material_select();
							},
						});
					});
					
					$("#new_cust_state").on("change",function(){
						var state_id = $(this).val();
						$.ajax({
							url:base_url+'Company_profile/get_cities',
							type:"POST",
							data:{'csrf_test_name':csrf_hash,'state_id':state_id},
							success:function(res){
							 $("#new_cust_city").html(res);
							 $("#new_cust_city").parents('.input-field').addClass('label-active');
							 $('#new_cust_city').material_select();
							},
						});
					});
					
					$("#new_cust_ship_state").on("change",function(){
						var state_id = $(this).val();
						$.ajax({
							url:base_url+'Company_profile/get_cities',
							type:"POST",
							data:{'csrf_test_name':csrf_hash,'state_id':state_id},
							success:function(res){
							 $("#new_cust_ship_city").html(res);
							 $("#new_cust_ship_city").parents('.input-field').addClass('label-active');
							 $('#new_cust_ship_city').material_select();
							},
						});
					});
					
					$("#add_new_customer_form").submit(function(e){
						e.preventDefault();
					}).validate({
						rules:{
							new_cust_company_name:{
								required:true,
							},
							new_cust_pan_no:{
								required:true,
								pannoregex:true,
							},
						},
						messages:{
							new_cust_company_name:{
								required:"Company Name is required",
							},
							new_cust_pan_no:{
								required:"Pan Number is required",
								pannoregex:"Enter PAN no in a valid format",
							},
						},
						submitHandler:function(form){
								var frm=$(form).serialize();
										$.ajax({
										url:base_url+'My_invoices/add_new_customer',
										type:"POST",
										data:{'csrf_test_name':csrf_hash,"frm":frm,},
										success:function(res){
											console.log(res);
												if(res != false)
												{
													$("#add_new_customer_form").find("input[type=text],textarea,option").val("");
													$('#add_customer_modal').modal('close');
													$("#customer_id").html(res);
													$("#customer_id").parents('.input-field').addClass('label-active');
													$('#customer_id').material_select();
													Materialize.toast('Client details updated successfully', 2000,'green rounded');
												}										
											},
							});
						},
					});
					
				} else {
					$.ajax({
						url:base_url+'My_invoices/get_customer_gst_info',
						type:"POST",
						data:{'csrf_test_name':csrf_hash,"customer_id":customer_id,},
						success:function(res){
							 var res = $.parseJSON(res);
							$("#customer_gstin").html(res[0]);
							$('#customer_gstin').material_select();
							$("#customer_place").html(res[1]);
							$('#customer_place').material_select();
							$('.billing_address span.bill_add').html(res[2]);
							if($("#same-as-bill-invoice").is(':checked')){
								console.log('same')
								$('.shipping_address span.ship_add').html(res[2]);
							}
							else{
								console.log('not same')
								$('.shipping_address span.ship_add').html(res[3]);
							}
							$('#po_num').html(res[4]);
							$('#po_num').material_select();
							//$('.shipping_address span').html(res[3]);
							//$('.shipping_address').closest('span').html(res[3]);
							//$('.chkbox').parent('.ship_add').html(res[3]);
						},
						complete:function(res){
							$.ajax({
								url:base_url+'My_invoices/get_gst_value',
								type:"POST",
								data:{'csrf_test_name':csrf_hash,},
								success:function(res){
									var data = JSON.parse(res);
									$("#tot_gst").val(data.gst);
									var tot_gst = data.gst;
										$.ajax({
											url:base_url+'My_invoices/fetch_gst',
											type:"POST",
											data:{'csrf_test_name':csrf_hash,"customer_id":customer_id,"company_id":company_id,},
											success:function(res1){
												res1=$.parseJSON(res1);
												var half_gst = parseFloat($("#tot_gst").val())/2;
												if($("#document_type").val()==5){
													$("#igst").val(parseFloat($('#tot_gst').val()));
													$("#sgst").val("0.00");
													$("#cgst").val("0.00");
													$(".branch_igst").val(parseFloat($('#tot_gst').val()));
													$(".branch_sgst").val("0.00");
													$(".branch_cgst").val("0.00");
												}
												else if(res1==true && $("#document_type").val()!=5){
													$("#igst").val("0.00");
													$("#sgst").val(half_gst);
													$("#cgst").val(half_gst);
													$(".branch_igst").val("0.00");
													$(".branch_sgst").val(half_gst);
													$(".branch_cgst").val(half_gst);
												}
												else
												{
													$("#igst").val(parseFloat($('#tot_gst').val()));
													$("#sgst").val("0.00");
													$("#cgst").val("0.00");
													$(".branch_igst").val(parseFloat($('#tot_gst').val()));
													$(".branch_sgst").val("0.00");
													$(".branch_cgst").val("0.00");
												}
												
											},
											complete:function(res1){
												$('input[name="invoice_array[]"]').each(function(){
													calculate_total($(this).val());
												});
											},
										});
								},
							});
						},
					});
				}
			});	
	
	$("#media_com_amount").on('change',function(){
		var val=(parseFloat($(this).val())*parseFloat($("#media_comm_per").val()))/100;
		$("#scrol-id tr.media_comm_row .rate").val(val);
		$('input[name="invoice_array[]"]').each(function(){
			calculate_total($(this).val());
		});
	});
	$("#media_comm_per").on('change',function(){
		var val=(parseFloat($(this).val())*parseFloat($("#media_com_amount").val()))/100;
		$("#scrol-id tr.media_comm_row .rate").val(val);
		$('input[name="invoice_array[]"]').each(function(){
			calculate_total($(this).val());
		});
	});
	$("#media_com_amount").on('blur',function(){
		var val=(parseFloat($(this).val())*parseFloat($("#media_comm_per").val()))/100;
		$("#scrol-id tr.media_comm_row .rate").val(val);

	});

	$("#media_com_amount").on('focus',function(){
		var val=(parseFloat($(this).val())*parseFloat($("#media_comm_per").val()))/100;
		$("#scrol-id tr.media_comm_row .rate").val(val);
	});
	
	
	$("#late_fee_amt").on('change',function(){
		var val=(parseFloat($(this).val())*parseFloat($("#late_fee_per").val()))/100;
		$("#scrol-id tr.late_fee_row .rate").val(val);
		$('input[name="invoice_array[]"]').each(function(){
			calculate_total($(this).val());
		});
	});
	$("#late_fee_per").on('change',function(){
		var val=(parseFloat($(this).val())*parseFloat($("#late_fee_amt").val()))/100;
		$("#scrol-id tr.late_fee_row .rate").val(val);
		$('input[name="invoice_array[]"]').each(function(){
			calculate_total($(this).val());
		});
	});
	$("#late_fee_amt").on('blur',function(){
		var val=(parseFloat($(this).val())*parseFloat($("#late_fee_per").val()))/100;
		$("#scrol-id tr.late_fee_row .rate").val(val);

	});

	$("#late_fee_amt").on('focus',"#late_fee_amt",function(){
		var val=(parseFloat($(this).val())*parseFloat($("#late_fee_per").val()))/100;
		$("#scrol-id tr.late_fee_row .rate").val(val);
	});
	
	$("#equ_leavy_amount").on('change' ,function(){
		var amt=(parseFloat($(this).val())*parseFloat($("#equ_leavy_per").val()))/100;
		$("#scrol-id tr.equ_leavy_row .rate").val(amt);
		$('input[name="invoice_array[]"]').each(function(){
			calculate_total($(this).val());
		});
	});
	$("#equ_leavy_per").on('change',function(){
		var amt=(parseFloat($(this).val())*parseFloat($("#equ_leavy_amount").val()))/100;
		$("#scrol-id tr.equ_leavy_row .rate").val(amt);
		$('input[name="invoice_array[]"]').each(function(){
			calculate_total($(this).val());
		});
	});
	$("#equ_leavy_amount").on('blur',function(){
		var amt=(parseFloat($(this).val())*parseFloat($("#equ_leavy_per").val()))/100;
		$("#scrol-id tr.equ_leavy_row .rate").val(amt);
	});
	$("#equ_leavy_amount").on('focus',function(){
		var amt=(parseFloat($(this).val())*parseFloat($("#equ_leavy_per").val()))/100;
		$("#scrol-id tr.equ_leavy_row .rate").val(amt);
	});
	
	$(document).on('change', '.qty,.rate,.cess,.discount', function () {
		var row_id=$(this).attr('id').split("_");
		calculate_total(row_id[1]);
	});
	
	$("#equ-leavy").on("change",function(){
		var count2=parseInt($("#counter").val())+3;
		var businesstype = $("#business_type").val();
		if($(this).is(':checked')){
				$(".equ_leavy_details").css('display','block');
			if(businesstype!=1){
				var equ_leavy=$('<tr id="row_'+count2+'" class="equ_leavy_row"><input name="invoice_array[]" value="'+count2+'" type="hidden" class="row_num" ><td class="meal over">Equilization Leavy<input class="service" name="service_'+count2+'" id="service_'+count2+'" value="e" type="hidden"></td><td class="ali-lef"><input required="required" autofocus="autofocus" class="form-control particulars" id="particular_'+count2+'" value="" name="particular_'+count2+'" type="text"></td><td><input autofocus="autofocus" class="form-control hsn_sac_num" id="hsn_sac_'+count2+'" value="" name="hsn_sac_'+count2+'" type="hidden"></td><td><input required="required" autofocus="autofocus" class="form-control qty" id="qty_'+count2+'" name="qty_'+count2+'" value="1" type="text"></td><td></td><td><input required="required" autofocus="autofocus" class="form-control rate" id="rate_'+count2+'" name="rate_'+count2+'" value="0" type="text" readonly></td><td class="meal"><input required="required" autofocus="autofocus" class="form-control discount" id="discount_'+count2+'" value="0.00" name="discount_'+count2+'" type="hidden"><input required="required" autofocus="autofocus" class="form-control disc_amt" id="discount_amt_'+count2+'" value="0.00" name="discount_amt_'+count2+'" type="hidden"></td><td class="meal"><input autofocus="autofocus" class="igst_amt form-control" id="igst_amt_'+count2+'" name="igst_amt_'+count2+'" value="0.00" readonly="" type="text"></td><td class="meal"><input autofocus="autofocus" class="branch_igst form-control" id="igst_'+count2+'" name="igst_'+count2+'" value="'+ $("#igst").val()+'" readonly="" type="text"></td><td class="meal"><input autofocus="autofocus" class="cgst_amt form-control" id="cgst_amt_'+count2+'" value="0.00" name="cgst_amt_'+count2+'" readonly="" type="text"></td><td class="meal"><input autofocus="autofocus" class="branch_cgst form-control" id="cgst_'+count2+'" name="cgst_'+count2+'" value="'+ $("#cgst").val()+'" readonly="" type="text"></td><td class="meal"><input autofocus="autofocus" class="sgst_amt form-control" id="sgst_amt_'+count2+'" value="0.00" name="sgst_amt_'+count2+'" readonly="" type="text"></td><td class="meal"><input autofocus="autofocus" class="branch_sgst form-control" id="sgst_'+count2+'" name="sgst_'+count2+'" value="'+ $("#sgst").val()+'" readonly="" type="text"></td><td><input autofocus="autofocus" class="form-control cess" id="cess_'+count2+'" value="0" name="cess_'+count2+'" type="text"><input autofocus="autofocus" class="cess_amt form-control" id="cess_amt_'+count2+'" value="0" name="cess_amt_'+count2+'" readonly="" type="hidden"></td><td class="last-btr"></td><td class="fix num-s meal credit-ntns"><input autofocus="autofocus" class="form-control amnt" id="amount_'+count2+'" value="0" name="amount_'+count2+'" readonly="" type="text"><input class="sub_amt_class" id="sub_tot_amount_'+count2+'" value="0" name="sub_tot_amount_'+count2+'" readonly="" type="hidden"></td></tr>');
					
		}else{
			var equ_leavy=$('<tr id="row_'+count2+'" class="equ_leavy_row"><input type="hidden" name="invoice_array[]" value="'+count2+'" class="row_num"><td class="meal over">Equilization Leavy<input class="service" name="service_'+count2+'" id="service_'+count2+'" value="e" type="hidden"><div class="scroll-hover-cover"><span class="hoveraction"></span></div></td><td class="ali-lef"><input type="text" required="required" autofocus="autofocus" class="form-control particulars" id="particular_'+count2+'" value="" name="particular_'+count2+'" aria-invalid="false"></td><td><input type="hidden" autofocus="autofocus" class="form-control hsn_sac_num" id="hsn_sac_'+count2+'" value="" name="hsn_sac_'+count2+'" aria-invalid="false"></td><td><input type="text" required="required" autofocus="autofocus" class="form-control qty" id="qty_'+count2+'" name="qty_'+count2+'" value="1" aria-invalid="false"></td><td><input type="text" required="required" autofocus="autofocus" class="form-control rate" id="rate_'+count2+'" name="rate_'+count2+'" value="0" aria-invalid="false" readonly></td><td class="meal"><input type="hidden" required="required" autofocus="autofocus" class="form-control discount" id="discount_'+count2+'" value="0.00" name="discount_'+count2+'" aria-invalid="false"><input type="hidden" required="required" autofocus="autofocus" class="form-control disc_amt" id="discount_amt_'+count2+'" value="0" name="discount_amt_'+count2+'"></td><td class="meal"><input type="text" autofocus="autofocus" class="igst_amt form-control" id="igst_amt_'+count2+'" name="igst_amt_'+count2+'" value="0.00" readonly=""></td><td class="meal"><input type="text" autofocus="autofocus" class="branch_igst form-control" id="igst_'+count2+'" name="igst_'+count2+'" value="'+ $("#igst").val()+'" readonly=""></td><td class="meal"><input type="text" autofocus="autofocus" class="cgst_amt form-control" id="cgst_amt_'+count2+'" value="0.00" name="cgst_amt_'+count2+'" readonly=""></td><td class="meal"><input type="text" autofocus="autofocus" class="branch_cgst form-control" id="cgst_'+count2+'" name="cgst_'+count2+'" value="'+ $("#cgst").val()+'" readonly=""></td><td class="meal"><input type="text" autofocus="autofocus" class="sgst_amt form-control" id="sgst_amt_'+count2+'" value="0.00" name="sgst_amt_'+count2+'" readonly=""></td><td class="meal"><input type="text" autofocus="autofocus" class="branch_sgst form-control" id="sgst_'+count2+'" name="sgst_'+count2+'" value="'+ $("#sgst").val()+'" readonly=""></td><td><input type="text" autofocus="autofocus" class="form-control cess" id="cess_'+count2+'" value="0" name="cess_'+count2+'"><input type="hidden" autofocus="autofocus" class="cess_amt form-control" id="cess_amt_'+count2+'" value="0" name="cess_amt_'+count2+'" readonly=""></td><td class="last-btr"></td><td class="fix num-s meal credit-ntns"><input type="text" autofocus="autofocus" class="form-control amnt" id="amount_'+count2+'" value="0" name="amount_'+count2+'" readonly="" aria-invalid="false"><input type="hidden" class="sub_amt_class" id="sub_tot_amount_'+count2+'" value="0" name="sub_tot_amount_'+count2+'" readonly=""></td></tr>');
		}
			equ_leavy.appendTo("#scrol-id tbody:last");
			
		}else{
				$(".equ_leavy_details").css('display','none');
				$("#scrol-id tbody .equ_leavy_row").remove();
		}
		$('input[name="invoice_array[]"]').each(function(){
			calculate_total($(this).val());
		});
	});
	$("#late-fees").on("change",function(){
		var count1=parseInt($("#counter").val())+2;
		var businesstype = $("#business_type").val();
		if($(this).is(':checked')){
				$(".late_fee_details").css('display','block');
			if(businesstype!=1){
				var late_fees=$('<tr id="row_'+count1+'" class="late_fee_row"><input name="invoice_array[]" value="'+count1+'" type="hidden" class="row_num"><td class="meal over">Late Fees<input class="service" name="service_'+count1+'" id="service_'+count1+'" value="l" type="hidden"></td><td class="ali-lef"><input required="required" autofocus="autofocus" class="form-control particulars" id="particular_'+count1+'" value="" name="particular_'+count1+'" type="text"></td><td><input autofocus="autofocus" class="form-control hsn_sac_num" id="hsn_sac_'+count1+'" value="" name="hsn_sac_'+count1+'" type="hidden"></td><td><input required="required" autofocus="autofocus" class="form-control qty" id="qty_'+count1+'" name="qty_'+count1+'" value="1" type="text"></td><td></td><td><input required="required" autofocus="autofocus" class="form-control rate" id="rate_'+count1+'" name="rate_'+count1+'" value="0" type="text" readonly></td><td class="meal"><input required="required" autofocus="autofocus" class="form-control discount" id="discount_'+count1+'" value="0.00" name="discount_'+count1+'" type="hidden"><input required="required" autofocus="autofocus" class="form-control disc_amt" id="discount_amt_'+count1+'" value="0.00" name="discount_amt_'+count1+'" type="hidden"></td><td class="meal"><input autofocus="autofocus" class="igst_amt form-control" id="igst_amt_'+count1+'" name="igst_amt_'+count1+'" value="0.00" readonly="" type="text"></td><td class="meal"><input autofocus="autofocus" class="branch_igst form-control" id="igst_'+count1+'" name="igst_'+count1+'" value="'+ $("#igst").val()+'" readonly="" type="text"></td><td class="meal"><input autofocus="autofocus" class="cgst_amt form-control" id="cgst_amt_'+count1+'" value="0.00" name="cgst_amt_'+count1+'" readonly="" type="text"></td><td class="meal"><input autofocus="autofocus" class="branch_cgst form-control" id="cgst_'+count1+'" name="cgst_'+count1+'" value="'+ $("#cgst").val()+'" readonly="" type="text"></td><td class="meal"><input autofocus="autofocus" class="sgst_amt form-control" id="sgst_amt_'+count1+'" value="0.00" name="sgst_amt_'+count1+'" readonly="" type="text"></td><td class="meal"><input autofocus="autofocus" class="branch_sgst form-control" id="sgst_'+count1+'" name="sgst_'+count1+'" value="'+ $("#sgst").val()+'" readonly="" type="text"></td><td><input autofocus="autofocus" class="form-control cess" id="cess_'+count1+'" value="0" name="cess_'+count1+'" type="text"><input autofocus="autofocus" class="cess_amt form-control" id="cess_amt_'+count1+'" value="0" name="cess_amt_'+count1+'" readonly="" type="hidden"></td><td class="last-btr"></td><td class="fix num-s meal credit-ntns"><input autofocus="autofocus" class="form-control amnt" id="amount_'+count1+'" value="0" name="amount_'+count1+'" readonly="" type="text"><input class="sub_amt_class" id="sub_tot_amount_'+count1+'" value="0" name="sub_tot_amount_'+count1+'" readonly="" type="hidden"></td></tr>');
					
		}else{
			var late_fees=$('<tr id="row_'+count1+'" class="late_fee_row"><input type="hidden" name="invoice_array[]" value="'+count1+'" class="row_num"><td class="meal over">Late Fees<input class="service" name="service_'+count1+'" id="service_'+count1+'" value="l" type="hidden"><div class="scroll-hover-cover"><span class="hoveraction"></span></div></td><td class="ali-lef"><input type="text" required="required" autofocus="autofocus" class="form-control particulars" id="particular_'+count1+'" value="" name="particular_'+count1+'" aria-invalid="false"></td><td><input type="hidden"  autofocus="autofocus" class="form-control hsn_sac_num" id="hsn_sac_'+count1+'" value="" name="hsn_sac_'+count1+'" aria-invalid="false"></td><td><input type="text" required="required" autofocus="autofocus" class="form-control qty" id="qty_'+count1+'" name="qty_'+count1+'" value="1" aria-invalid="false"></td><td><input type="text" required="required" autofocus="autofocus" class="form-control rate" id="rate_'+count1+'" name="rate_'+count1+'" value="0" aria-invalid="false" readonly></td><td class="meal"><input type="hidden" required="required" autofocus="autofocus" class="form-control discount" id="discount_'+count1+'" value="0.00" name="discount_'+count1+'" aria-invalid="false"><input type="hidden" required="required" autofocus="autofocus" class="form-control disc_amt" id="discount_amt_'+count1+'" value="0" name="discount_amt_'+count1+'"></td><td class="meal"><input type="text" autofocus="autofocus" class="igst_amt form-control" id="igst_amt_'+count1+'" name="igst_amt_'+count1+'" value="0.00" readonly=""></td><td class="meal"><input type="text" autofocus="autofocus" class="branch_igst form-control" id="igst_'+count1+'" name="igst_'+count1+'" value="'+ $("#igst").val()+'" readonly=""></td><td class="meal"><input type="text" autofocus="autofocus" class="cgst_amt form-control" id="cgst_amt_'+count1+'" value="0.00" name="cgst_amt_'+count1+'" readonly=""></td><td class="meal"><input type="text" autofocus="autofocus" class="branch_cgst form-control" id="cgst_'+count1+'" name="cgst_'+count1+'" value="'+ $("#cgst").val()+'" readonly=""></td><td class="meal"><input type="text" autofocus="autofocus" class="sgst_amt form-control" id="sgst_amt_'+count1+'" value="0.00" name="sgst_amt_'+count1+'" readonly=""></td><td class="meal"><input type="text" autofocus="autofocus" class="branch_sgst form-control" id="sgst_'+count1+'" name="sgst_'+count1+'" value="'+ $("#sgst").val()+'" readonly=""></td><td><input type="text" autofocus="autofocus" class="form-control cess" id="cess_'+count1+'" value="0" name="cess_'+count1+'"><input type="hidden" autofocus="autofocus" class="cess_amt form-control" id="cess_amt_'+count1+'" value="0" name="cess_amt_'+count1+'" readonly=""></td><td class="last-btr"></td><td class="fix num-s meal credit-ntns"><input type="text" autofocus="autofocus" class="form-control amnt" id="amount_'+count1+'" value="0" name="amount_'+count1+'" readonly="" aria-invalid="false"><input type="hidden" class="sub_amt_class" id="sub_tot_amount_'+count1+'" value="0" name="sub_tot_amount_'+count1+'" readonly=""></td></tr>');
		}
			late_fees.appendTo("#scrol-id tbody:last");
		}
		else{
				$(".late_fee_details").css('display','none');
				$("#scrol-id tbody .late_fee_row").remove();
		}
		$('input[name="invoice_array[]"]').each(function(){
				calculate_total($(this).val());
		});
	});
	$("#media-comm").on("change",function(){
		var count0=parseInt($("#counter").val())+1;
		var businesstype = $("#business_type").val();
		if($(this).is(':checked')){
			$(".media_comm_details").css('display','block');
			if(businesstype!=1){
				
				var media_comm=$('<tr id="row_'+count0+'" class="media_comm_row"><input name="invoice_array[]" value="'+count0+'" type="hidden" class="row_num"><td class="meal over">Media Commission<input class="service" name="service_'+count0+'" id="service_'+count0+'" value="m" type="hidden"></td><td class="ali-lef"><input required="required" autofocus="autofocus" class="form-control particulars" id="particular_'+count0+'" value="" name="particular_'+count0+'" type="text"></td><td><input  autofocus="autofocus" class="form-control hsn_sac_num" id="hsn_sac_'+count0+'" value="" name="hsn_sac_'+count0+'" type="hidden"></td><td><input required="required" autofocus="autofocus" class="form-control qty" id="qty_'+count0+'" name="qty_'+count0+'" value="1" type="text"></td><td></td><td><input required="required" autofocus="autofocus" class="form-control rate" id="rate_'+count0+'" name="rate_'+count0+'" value="0" type="text" readonly></td><td class="meal"><input required="required" autofocus="autofocus" class="form-control discount" id="discount_'+count0+'" value="0.00" name="discount_'+count0+'" type="hidden"><input required="required" autofocus="autofocus" class="form-control disc_amt" id="discount_amt_'+count0+'" value="0.00" name="discount_amt_'+count0+'" type="hidden"></td><td class="meal"><input autofocus="autofocus" class="igst_amt form-control" id="igst_amt_'+count0+'" name="igst_amt_'+count0+'" value="0.00" readonly="" type="text"></td><td class="meal"><input autofocus="autofocus" class="branch_igst form-control" id="igst_'+count0+'" name="igst_'+count0+'" value="'+ $("#igst").val()+'" readonly="" type="text"></td><td class="meal"><input autofocus="autofocus" class="cgst_amt form-control" id="cgst_amt_'+count0+'" value="0.00" name="cgst_amt_'+count0+'" readonly="" type="text"></td><td class="meal"><input autofocus="autofocus" class="branch_cgst form-control" id="cgst_'+count0+'" name="cgst_'+count0+'" value="'+ $("#cgst").val()+'" readonly="" type="text"></td><td class="meal"><input autofocus="autofocus" class="sgst_amt form-control" id="sgst_amt_'+count0+'" value="0.00" name="sgst_amt_'+count0+'" readonly="" type="text"></td><td class="meal"><input autofocus="autofocus" class="branch_sgst form-control" id="sgst_'+count0+'" name="sgst_'+count0+'" value="'+ $("#sgst").val()+'" readonly="" type="text"></td><td><input autofocus="autofocus" class="form-control cess" id="cess_'+count0+'" value="0" name="cess_'+count0+'" type="text"><input autofocus="autofocus" class="cess_amt form-control" id="cess_amt_'+count0+'" value="0" name="cess_amt_'+count0+'" readonly="" type="hidden"></td><td class="last-btr"></td><td class="fix num-s meal credit-ntns"><input autofocus="autofocus" class="form-control amnt" id="amount_'+count0+'" value="0" name="amount_'+count0+'" readonly="" type="text"><input class="sub_amt_class" id="sub_tot_amount_'+count0+'" value="0" name="sub_tot_amount_'+count0+'" readonly="" type="hidden"></td></tr>');
					
		}else{
			var media_comm=$('<tr id="row_'+count0+'" class="media_comm_row"><input type="hidden" name="invoice_array[]" value="'+count0+'" class="row_num"><td class="meal over">Media Commission<input class="service" name="service_'+count0+'" id="service_'+count0+'" value="m" type="hidden"><div class="scroll-hover-cover"><span class="hoveraction"></span></div></td><td class="ali-lef"><input type="text" required="required" autofocus="autofocus" class="form-control particulars" id="particular_'+count0+'" value="" name="particular_'+count0+'" aria-invalid="false"></td><td><input type="hidden"  autofocus="autofocus" class="form-control hsn_sac_num" id="hsn_sac_'+count0+'" value="" name="hsn_sac_'+count0+'" aria-invalid="false"></td><td><input type="text" required="required" autofocus="autofocus" class="form-control qty" id="qty_'+count0+'" name="qty_'+count0+'" value="1" aria-invalid="false"></td><td><input type="text" required="required" autofocus="autofocus" class="form-control rate" id="rate_'+count0+'" name="rate_'+count0+'" value="0" aria-invalid="false" readonly></td><td class="meal"><input type="hidden" required="required" autofocus="autofocus" class="form-control discount" id="discount_'+count0+'" value="0.00" name="discount_'+count0+'" aria-invalid="false"><input type="hidden" required="required" autofocus="autofocus" class="form-control disc_amt" id="discount_amt_'+count0+'" value="0" name="discount_amt_'+count0+'"></td><td class="meal"><input type="text" autofocus="autofocus" class="igst_amt form-control" id="igst_amt_'+count0+'" name="igst_amt_'+count0+'" value="0.00" readonly=""></td><td class="meal"><input type="text" autofocus="autofocus" class="branch_igst form-control" id="igst_'+count0+'" name="igst_'+count0+'" value="'+ $("#igst").val()+'" readonly=""></td><td class="meal"><input type="text" autofocus="autofocus" class="cgst_amt form-control" id="cgst_amt_'+count0+'" value="0.00" name="cgst_amt_'+count0+'" readonly=""></td><td class="meal"><input type="text" autofocus="autofocus" class="branch_cgst form-control" id="cgst_'+count0+'" name="cgst_'+count0+'" value="'+ $("#cgst").val()+'" readonly=""></td><td class="meal"><input type="text" autofocus="autofocus" class="sgst_amt form-control" id="sgst_amt_'+count0+'" value="0.00" name="sgst_amt_'+count0+'" readonly=""></td><td class="meal"><input type="text" autofocus="autofocus" class="branch_sgst form-control" id="sgst_'+count0+'" name="sgst_'+count0+'" value="'+ $("#sgst").val()+'" readonly=""></td><td><input type="text" autofocus="autofocus" class="form-control cess" id="cess_'+count0+'" value="0" name="cess_'+count0+'"><input type="hidden" autofocus="autofocus" class="cess_amt form-control" id="cess_amt_'+count0+'" value="0" name="cess_amt_'+count0+'" readonly=""></td><td class="last-btr"></td><td class="fix num-s meal credit-ntns"><input type="text" autofocus="autofocus" class="form-control amnt" id="amount_'+count0+'" value="0" name="amount_'+count0+'" readonly="" aria-invalid="false"><input type="hidden" class="sub_amt_class" id="sub_tot_amount_'+count0+'" value="0" name="sub_tot_amount_'+count0+'" readonly=""></td></tr>');
		}
			media_comm.appendTo("#scrol-id tbody:last");
			
			$(".media_rate").on('change',function(){
				$("#media_com_amount").val($(this).val());
			});
			
			/*$('input[name="invoice_array[]"]').each(function(){
				calculate_total($(this).val());
			});*/
		}
		else{
				$(".media_comm_details").css('display','none');
				$("#scrol-id tbody .media_comm_row").remove();
		}
		$('input[name="invoice_array[]"]').each(function(){
			calculate_total($(this).val());
		});
		
	});
	
	$( '.reverse_charge_chkbox').on("change",function() {
		var businesstype = $(this).data('businesstype');
		if($(this).is(':checked')){
			if(businesstype!=1){
				var row='<tr id="reversecharge">'+
                          '<th colspan="6" class="total-amt"><p>TAX UNDER REVERSE CHARGE</p></th>'+
                          '<th class="yello-clr bg-non"></th>'+
                          '<th class="meal yello-clr"><input type="text" name="rev_igst_tot_amt" class="readonly_field igst_tot_amt" readonly="" value="0"></th>'+
                          '<th class="yello-clr bg-non"></th>'+
                          '<th class="meal yello-clr"><input type="text" name="rev_cgst_tot_amt" class="readonly_field cgst_tot_amt" readonly="" value="0"></th>'+
                          '<th class="yello-clr bg-non"></th>'+
                          '<th class="meal yello-clr"><input type="text" class="readonly_field sgst_tot_amt" name="rev_sgst_tot_amt" readonly="" value="0"></th>'+
                          '<th class="yello-clr bg-non"></th>'+
                          '<th class="meal yello-clr"><input type="text" class="readonly_field cess_tot_amt" name="rev_cess_tot_amt" readonly="" value="0"></th>'+
                          '<th class="meal yello-clr"></th>'+
                          '<th class="last-btr"></th>'+
                          '<th class="fix empty-v meal emty yello-clr number"><p><input type="text" name="reverse_charge_amt" id="reverse_charge_amt" class="rev_charge_amt" value="0"></p></th>'+
                        '</tr>';
			}
			else{
			var row='<tr id="reversecharge">'+
                              '<th colspan="5" class="total-amt"><p>TAX UNDER REVERSE CHARGE</p></th>'+
                              '<th class="yello-clr bg-non"></th>'+
                              '<th class="meal yello-clr"><input type="text" name="rev_igst_tot_amt" class="readonly_field igst_tot_amt" readonly="" value="0"></th>'+
                              '<th class="yello-clr bg-non"></th>'+
                              '<th class="meal yello-clr"><input type="text" name="rev_cgst_tot_amt" class="readonly_field cgst_tot_amt" readonly="" value="0"></th>'+
                              '<th class="yello-clr bg-non"></th>'+
                              '<th class="meal yello-clr"><input type="text" class="readonly_field sgst_tot_amt" name="rev_sgst_tot_amt" readonly="" value="0"></th>'+
                              '<th class="yello-clr bg-non"></th>'+
                              '<th class="meal yello-clr"><input type="text" class="readonly_field cess_tot_amt" name="rev_cess_tot_amt" readonly="" value="0"></th>'+
                              '<th class="meal yello-clr"></th>'+
                              '<th class="last-btr"></th>'+
                              '<th class="fix empty-v meal emty yello-clr number"><p><input type="text" name="reverse_charge_amt" id="reverse_charge_amt" class="rev_charge_amt" value="0"></p></th>'+
                       '</tr>';
			}
			$('#scrol-id tr.totalamount').after(row);
			$('input[name="invoice_array[]"]').each(function(){
				calculate_total($(this).val());
			});
			
		} else {
			$('#reversecharge').remove();
			$('input[name="invoice_array[]"]').each(function(){
				calculate_total($(this).val());
			});
		}
	});
	$(document).off().on('change', 'select.service',  function () {
	if($(this).val() == 'add_new_service') {
		$.ajax({
			url:base_url+'My_invoices/get_units',
			type:"POST",
			data:{'csrf_test_name':csrf_hash,},
			success:function(res){
					$('#invoice_add_new_service').modal('open');
					$("#invoice_unit").html(res);
					$("#invoice_unit").parents('.input-field').addClass('label-active');
					$('#invoice_unit').material_select();
				},
		});	
	} else {
		var row_id=$(this).attr('id').split("_");
		var service_id = $(this).val();
		calculate_total(row_id[1]);
	}
});



	$(".edit_billing_add").on("click",function(){
				$('#edit_billing_address_modal').modal('open');
					$.ajax({
						url:base_url+'My_invoices/check_billing_inv_session',
						type:"POST",
						data:{'csrf_test_name':csrf_hash,},
						success:function(res){
								var result=$.parseJSON(res);
								
								if(result!=false){
									
									$.ajax({
										url:base_url+'My_invoices/get_all_countries',
										type:"POST",
										data:{'csrf_test_name':csrf_hash,},
										success:function(res){
											$("#edit_billing_address_modal #new_inv_country").html(res);
											$("#edit_billing_address_modal #new_inv_country").parents('.input-field').addClass('label-active');
											$('#edit_billing_address_modal #new_inv_country option[value="'+result.country+'"]').attr('selected','selected');
											$('#edit_billing_address_modal #new_inv_country').material_select();
											
											},
										});
										if(result.country!=''){
											$.ajax({
												url:base_url+'Company_profile/get_states',
												type:"POST",
												data:{'csrf_test_name':csrf_hash,'country_id':result.country},
												success:function(res){
													$("#new_inv_state").html(res);
													$("#new_inv_state").parents('.input-field').addClass('label-active');
													$('#edit_billing_address_modal #new_inv_state option[value="'+result.state+'"]').attr('selected','selected');
													$('#new_inv_state').material_select();
												},
											});
										}
										else{
											$("#new_inv_country").on("change",function(){
											var country_id = $(this).val();
												$.ajax({
													url:base_url+'Company_profile/get_states',
													type:"POST",
													data:{'csrf_test_name':csrf_hash,'country_id':country_id},
													success:function(res){
														$("#new_inv_state").html(res);
														$("#new_inv_state").parents('.input-field').addClass('label-active');
														$('#new_inv_state').material_select();
													},
												});
											});
										}
									if(result.state!=''){
										$.ajax({
											url:base_url+'Company_profile/get_cities',
											type:"POST",
											data:{'csrf_test_name':csrf_hash,'state_id':result.state},
											success:function(res){
											 $("#new_inv_city").html(res);
											 $("#new_inv_city").parents('.input-field').addClass('label-active');
											$('#edit_billing_address_modal #new_inv_city option[value="'+result.city+'"]').attr('selected','selected');
											 $('#new_inv_city').material_select();
											},
										});
									}
									else{
										$("#new_inv_state").on("change",function(){
											var state_id = $(this).val();
											$.ajax({
												url:base_url+'Company_profile/get_cities',
												type:"POST",
												data:{'csrf_test_name':csrf_hash,'state_id':state_id},
												success:function(res){
												 $("#new_inv_city").html(res);
												 $("#new_inv_city").parents('.input-field').addClass('label-active');
												 $('#new_inv_city').material_select();
												},
											});
										});
									}
								}
								else{
									$.ajax({
										url:base_url+'My_invoices/get_all_countries',
										type:"POST",
										data:{'csrf_test_name':csrf_hash,},
										success:function(res){
											$("#edit_billing_address_modal #new_inv_country").html(res);
											$("#edit_billing_address_modal #new_inv_country").parents('.input-field').addClass('label-active');
											$('#edit_billing_address_modal #new_inv_country').material_select();
											},
										});
										$("#new_inv_country").on("change",function(){
											var country_id = $(this).val();
											$.ajax({
												url:base_url+'Company_profile/get_states',
												type:"POST",
												data:{'csrf_test_name':csrf_hash,'country_id':country_id},
												success:function(res){
													$("#new_inv_state").html(res);
													$("#new_inv_state").parents('.input-field').addClass('label-active');
													$('#new_inv_state').material_select();
												},
											});
										});
										$("#new_inv_state").on("change",function(){
											var state_id = $(this).val();
											$.ajax({
												url:base_url+'Company_profile/get_cities',
												type:"POST",
												data:{'csrf_test_name':csrf_hash,'state_id':state_id},
												success:function(res){
												 $("#new_inv_city").html(res);
												 $("#new_inv_city").parents('.input-field').addClass('label-active');
												 $('#new_inv_city').material_select();
												},
											});
										});
								}
							},
					});
			});
			$(".edit_shipping_add").on("click",function(){
				$('#edit_shipping_address_modal').modal('open');
					$.ajax({
						url:base_url+'My_invoices/check_shipping_inv_session',
						type:"POST",
						data:{'csrf_test_name':csrf_hash,},
						success:function(res){
							var result=$.parseJSON(res);

							if(result!=false){
								$.ajax({
										url:base_url+'My_invoices/get_all_countries',
										type:"POST",
										data:{'csrf_test_name':csrf_hash,},
										success:function(res){
											$("#edit_shipping_address_modal #new_inv_ship_country").html(res);
											$("#edit_shipping_address_modal #new_inv_ship_country").parents('.input-field').addClass('label-active');
											$('#edit_shipping_address_modal #new_inv_ship_country option[value="'+result.country+'"]').attr('selected','selected');
											$('#edit_shipping_address_modal #new_inv_ship_country').material_select();
											
											},
										});
										if(result.country!=''){
											$.ajax({
												url:base_url+'Company_profile/get_states',
												type:"POST",
												data:{'csrf_test_name':csrf_hash,'country_id':result.country},
												success:function(res){
													$("#new_inv_ship_state").html(res);
													$("#new_inv_ship_state").parents('.input-field').addClass('label-active');
													$('#edit_shipping_address_modal #new_inv_ship_state option[value="'+result.state+'"]').attr('selected','selected');
													$('#new_inv_ship_state').material_select();
												},
											});
										}
										else{
											$("#new_inv_ship_country").on("change",function(){
											var country_id = $(this).val();
												$.ajax({
													url:base_url+'Company_profile/get_states',
													type:"POST",
													data:{'csrf_test_name':csrf_hash,'country_id':country_id},
													success:function(res){
														$("#new_inv_ship_state").html(res);
														$("#new_inv_ship_state").parents('.input-field').addClass('label-active');
														$('#new_inv_ship_state').material_select();
													},
												});
											});
										}
									if(result.state!=''){
										$.ajax({
											url:base_url+'Company_profile/get_cities',
											type:"POST",
											data:{'csrf_test_name':csrf_hash,'state_id':result.state},
											success:function(res){
											 $("#new_inv_ship_city").html(res);
											 $("#new_inv_ship_city").parents('.input-field').addClass('label-active');
											$('#edit_shipping_address_modal #new_inv_ship_city option[value="'+result.city+'"]').attr('selected','selected');
											 $('#new_inv_ship_city').material_select();
											},
										});
									}
									else{
										$("#new_inv_ship_state").on("change",function(){
											var state_id = $(this).val();
											$.ajax({
												url:base_url+'Company_profile/get_cities',
												type:"POST",
												data:{'csrf_test_name':csrf_hash,'state_id':state_id},
												success:function(res){
												 $("#new_inv_ship_city").html(res);
												 $("#new_inv_ship_city").parents('.input-field').addClass('label-active');
												 $('#new_inv_ship_city').material_select();
												},
											});
										});
									}
							}
							else
							{
								$.ajax({
								url:base_url+'My_invoices/get_all_countries',
								type:"POST",
								data:{'csrf_test_name':csrf_hash,},
								success:function(res){
									$("#edit_shipping_address_modal #new_inv_ship_country").html(res);
									$("#edit_shipping_address_modal #new_inv_ship_country").parents('.input-field').addClass('label-active');
									$('#edit_shipping_address_modal #new_inv_ship_country').material_select();
									},
								});
								$("#new_inv_ship_country").on("change",function(){
									var country_id = $(this).val();
									$.ajax({
										url:base_url+'Company_profile/get_states',
										type:"POST",
										data:{'csrf_test_name':csrf_hash,'country_id':country_id},
										success:function(res){
											$("#new_inv_ship_state").html(res);
											$("#new_inv_ship_state").parents('.input-field').addClass('label-active');
											$('#new_inv_ship_state').material_select();
										},
									});
								});
								$("#new_inv_ship_state").on("change",function(){
									var state_id = $(this).val();
									$.ajax({
										url:base_url+'Company_profile/get_cities',
										type:"POST",
										data:{'csrf_test_name':csrf_hash,'state_id':state_id},
										success:function(res){
										 $("#new_inv_ship_city").html(res);
										 $("#new_inv_ship_city").parents('.input-field').addClass('label-active');
										 $('#new_inv_ship_city').material_select();
										},
									});
								});
							}
						},
					});
				
				
			});
});


/*$(document).off().on('change', 'select.service',  function () {
	if($(this).val() == 'add_new_service') {
		$.ajax({
			url:base_url+'My_invoices/get_units',
			type:"POST",
			data:{},
			success:function(res){
					$('#invoice_add_new_service').modal('open');
					$("#invoice_unit").html(res);
					$("#invoice_unit").parents('.input-field').addClass('label-active');
					$('#invoice_unit').material_select();
				},
		});	
	} else {
		var row_id=$(this).attr('id').split("_");
		var service_id = $(this).val();
		calculate_total(row_id[1]);
	}
});*/

var tot = 0;
function calculate_total(id)
{
	var row = $('#row_'+id);
	var quantity = row.find('#qty_'+id).val();
	var rate = row.find('#rate_'+id).val();
	var service = row.find('#service_'+id).val();
	var discount = row.find('#discount_'+id).val();
	var cess = row.find('#cess_'+id).val();
	var other = row.find('#other_'+id).val();
	var igst = row.find('#igst_'+id).val();
	var cgst = row.find('#cgst_'+id).val();
	var sgst = row.find('#sgst_'+id).val();
	var customer = $("#customer_id").val();
	if(service=='l' || service=='m' || service=='e' || service=='-1'){
		$("#hsn_sac_"+id).val('');	
					$("#unit_"+id).val('');
					var tot=parseFloat(quantity) * parseFloat(rate);
					$("#sub_tot_amount_"+id).val(tot);
					var tot_gst=parseFloat(igst)+parseFloat(sgst)+parseFloat(cgst);
					var applicable_cess=(parseFloat(tot)*parseFloat(cess))/100;
					$("#cess_amt_"+id).val(applicable_cess);
					var applicable_other=(parseFloat(tot)*parseFloat(other))/100;
					$("#other_amt_"+id).val(applicable_other);
					var applicable_disc=(parseFloat(tot)*parseFloat(discount))/100;
					$("#discount_amt_"+id).val(applicable_disc);
					$('#igst_amt_'+id).val((parseFloat(tot)*parseFloat(igst))/100);
					$('#cgst_amt_'+id).val((parseFloat(tot)*parseFloat(cgst))/100);
					$('#sgst_amt_'+id).val((parseFloat(tot)*parseFloat(sgst))/100);
					var applicable_gst=(parseFloat(tot) * parseFloat(tot_gst))/100;
					//tot=(tot+applicable_gst+applicable_cess)-applicable_disc;
					if ($('#reversecharge').length>0){
						tot=(tot)-applicable_disc;
					}
					else{
						tot=(tot+applicable_gst+applicable_cess+applicable_other)-applicable_disc;
					}
					$("#amount_"+id).val(parseFloat(tot));
					total=0;
					$('.sub_amt_class').each(function(){
						if($(this).val()==''){
							$(this).val(0);
						}
						total=parseFloat(total)+parseFloat($(this).val());
					});
					$("#total").val(parseFloat(total));
					cess_amt=0;
					$('.cess_amt').each(function(){
						if($(this).val()==''){
							$(this).val(0);
						}
						cess_amt=parseFloat(cess_amt)+parseFloat($(this).val());
					});
					$(".cess_tot_amt").val(parseFloat(cess_amt));
                    
                    $('.other_amt').each(function(){
						if($(this).val()==''){
							$(this).val(0);
						}
						other_amt=parseFloat(other_amt)+parseFloat($(this).val());
					});
					$(".other_tot_amt").val(parseFloat(other_amt));
					discount_amt=0;
						$('.disc_amt').each(function(){
							if($(this).val()==''){
								$(this).val(0);
							}
							discount_amt=parseFloat(discount_amt)+parseFloat($(this).val());
						});
						$(".disc_tot_amt").val(parseFloat(discount_amt));
						igst_tot_amt=0;
						$('.igst_amt').each(function(){
							if($(this).val()==''){
								$(this).val(0);
							}
							igst_tot_amt=parseFloat(igst_tot_amt)+parseFloat($(this).val());
						});
						$(".igst_tot_amt").val(parseFloat(igst_tot_amt));
						cgst_tot_amt=0;
						$('.cgst_amt').each(function(){
							if($(this).val()==''){
								$(this).val(0);
							}
							cgst_tot_amt=parseFloat(cgst_tot_amt)+parseFloat($(this).val());
						});
						$(".cgst_tot_amt").val(parseFloat(cgst_tot_amt));
						sgst_tot_amt=0;
						$('.sgst_amt').each(function(){
							if($(this).val()==''){
								$(this).val(0);
							}
							sgst_tot_amt=parseFloat(sgst_tot_amt)+parseFloat($(this).val());
						});
						$(".sgst_tot_amt").val(parseFloat(sgst_tot_amt));
						final_amount=0;
						$('.amnt').each(function(){
							if($(this).val()==''){
								$(this).val(0);
							}
							final_amount=parseFloat(final_amount)+parseFloat($(this).val());
						});
						$("#totalamount").val(parseFloat(final_amount));
						$(".grandtotalamount").html(parseFloat(final_amount));
				
						if ($('#reversecharge').length>0){	reverse_charge=parseFloat($("input[name='rev_igst_tot_amt']").val())+parseFloat($("input[name='rev_cgst_tot_amt']").val())+parseFloat($("input[name='rev_sgst_tot_amt']").val())+parseFloat($("input[name='rev_cess_tot_amt']").val());

								$(".rev_charge_amt").val(reverse_charge);

							}
	}
	else{
			$.ajax({
			url:base_url+'My_invoices/get_service_info',
			type:"POST",
			data:{'csrf_test_name':csrf_hash,"service_id":service},
			success:function(res){
				var data=JSON.parse(res);
					$("#hsn_sac_"+id).val(data[0].hsn_sac_no);	
					$("#unit_"+id).val(data[0].unit);
					var tot=parseFloat(quantity) * parseFloat(rate);
					$("#sub_tot_amount_"+id).val(tot);
					var tot_gst=parseFloat(igst)+parseFloat(sgst)+parseFloat(cgst);
					var applicable_cess=(parseFloat(tot)*parseFloat(cess))/100;
					$("#cess_amt_"+id).val(applicable_cess);
					var applicable_other=(parseFloat(tot)*parseFloat(other))/100;
					$("#other_amt_"+id).val(applicable_other);
					var applicable_disc=(parseFloat(tot)*parseFloat(discount))/100;
					$("#discount_amt_"+id).val(applicable_disc);
					$('#igst_amt_'+id).val((parseFloat(tot)*parseFloat(igst))/100);
					$('#cgst_amt_'+id).val((parseFloat(tot)*parseFloat(cgst))/100);
					$('#sgst_amt_'+id).val((parseFloat(tot)*parseFloat(sgst))/100);
					var applicable_gst=(parseFloat(tot) * parseFloat(tot_gst))/100;
					//tot=(tot+applicable_gst+applicable_cess)-applicable_disc;
					if ($('#reversecharge').length>0){
						tot=(tot)-applicable_disc;
					}
					else{
						tot=(tot+applicable_gst+applicable_cess+applicable_other)-applicable_disc;
					}
					$("#amount_"+id).val(parseFloat(tot));
					total=0;
					$('.sub_amt_class').each(function(){
						if($(this).val()==''){
							$(this).val(0);
						}
						total=parseFloat(total)+parseFloat($(this).val());
					});
					$("#total").val(parseFloat(total));
					cess_amt=0;
					$('.cess_amt').each(function(){
						if($(this).val()==''){
							$(this).val(0);
						}
						cess_amt=parseFloat(cess_amt)+parseFloat($(this).val());
					});
					$(".cess_tot_amt").val(parseFloat(cess_amt));

					other_amt=0;
					$('.other_amt').each(function(){
						if($(this).val()==''){
							$(this).val(0);
						}
						other_amt=parseFloat(other_amt)+parseFloat($(this).val());
					});
					$(".other_tot_amt").val(parseFloat(other_amt));
					discount_amt=0;
						$('.disc_amt').each(function(){
							if($(this).val()==''){
								$(this).val(0);
							}
							discount_amt=parseFloat(discount_amt)+parseFloat($(this).val());
						});
						$(".disc_tot_amt").val(parseFloat(discount_amt));
						igst_tot_amt=0;
						$('.igst_amt').each(function(){
							if($(this).val()==''){
								$(this).val(0);
							}
							igst_tot_amt=parseFloat(igst_tot_amt)+parseFloat($(this).val());
						});
						$(".igst_tot_amt").val(parseFloat(igst_tot_amt));
						cgst_tot_amt=0;
						$('.cgst_amt').each(function(){
							if($(this).val()==''){
								$(this).val(0);
							}
							cgst_tot_amt=parseFloat(cgst_tot_amt)+parseFloat($(this).val());
						});
						$(".cgst_tot_amt").val(parseFloat(cgst_tot_amt));
						sgst_tot_amt=0;
						$('.sgst_amt').each(function(){
							if($(this).val()==''){
								$(this).val(0);
							}
							sgst_tot_amt=parseFloat(sgst_tot_amt)+parseFloat($(this).val());
						});
						$(".sgst_tot_amt").val(parseFloat(sgst_tot_amt));
						final_amount=0;
						$('.amnt').each(function(){
							if($(this).val()==''){
								$(this).val(0);
							}
							final_amount=parseFloat(final_amount)+parseFloat($(this).val());
						});
						$("#totalamount").val(parseFloat(final_amount));
						$(".grandtotalamount").html(parseFloat(final_amount));
				
						if ($('#reversecharge').length>0){	reverse_charge=parseFloat($("input[name='rev_igst_tot_amt']").val())+parseFloat($("input[name='rev_cgst_tot_amt']").val())+parseFloat($("input[name='rev_sgst_tot_amt']").val())+parseFloat($("input[name='rev_cess_tot_amt']").val());

								$(".rev_charge_amt").val(reverse_charge);

							}
				},
		});
	}

		
}