/*
 * DataTables - Tables
 */


$(function() {
	
  $('#data-table-simple').DataTable();

/*  $('#table1').dataTable( {
    "aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0 , 2 , 3, 4, 5, 6, 7] }],
    "order": [],
    "bPaginate": false,
    "bLengthChange": false,
    "bFilter": true,
    "bInfo": false,
    "searching":false
  });*/

  $('#past-invoice').dataTable( {
    "aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0 , 2 , 3, 4, 6, 7, 8] }],
    "order": [],
    "bPaginate": false,
    "bLengthChange": false,
    "bFilter": true,
    "bInfo": false,
    "searching":false
  });

$('#my-stack-service').dataTable( {
    "aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0 ,2 ,3 ,4 , 5] }],
    "order": [],
    "bPaginate": false,
    "bLengthChange": false,
    "bFilter": true,
    "bInfo": false,
    "searching":false
  });
  
  $('#table2').dataTable( {
    "aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0 , 1 , 3 , 4 , 6] }],
    "order": [],
    "bPaginate": false,
    "bLengthChange": false,
    "bFilter": true,
    "bInfo": false,
    "searching":false
  });
 
  var table_purchase_return = "";
  	$(document).ready(function(){
		getPurchaseReturns();
		$('#search_by_supplier').change( function() { getPurchaseReturns() } );
		$('#search').on('keyup', function() { getPurchaseReturns() } );
	});

function getPurchaseReturns()
	{
		if(table_purchase_return != "")
		{ 
			table_purchase_return.destroy();
			$.fn.DataTable.ext.errMode = 'none';
			table_purchase_return.draw();
		}
 table_purchase_return =  $('#my-purchase-return').DataTable( {
		 "bServerSide": true,
		"bProcessing": true,
		"bDeferRender": true,
		"aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0 , 2 , 3, 4, 5, 6, 7] }],
		 "aoColumns": [	
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			//{ "bSortable": false },
			//{ "bSortable": false },
			//{ "bSortable": false },
			{ "bSortable": false },
			{"bVisible": false},
			//{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			//{"bVisible": false},
			//{ "bSortable": false },
			
			//{ "bSortable": false },
			//"bSortable": false,
		],
		"fnServerParams": function ( aoData ) {
			aoData.push( { "name": "search_by_supplier", "value": $("#search_by_supplier").val() } );
			aoData.push( { "name": "search", "value": $("#search").val() } );
		},
		"order": [],
		"bPaginate": true,
		"bLengthChange": false,
		"bFilter": true,
		"bInfo": false,
		"searching":false,
		"iDisplayLength": 10,
		"aLengthMenu": [10, 20, 40],
		"aaSorting": [[ 1, "desc" ]],
		"sAjaxSource": base_url+"My_purchase_return/get_pr_list",
		"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
			if($(aData[9]+' div').hasClass('deactive_record')){
				$('td', nRow).parent('tr').addClass('deactivate-row');
			}
		
			$('td:first-child', nRow).addClass('bulk');
			$('td:nth-child(2)', nRow).addClass('profil-img');
			$('td:last-child', nRow).addClass('action-tab');
		 },
		 "fnDrawCallback": function () {
			  $('.dropdown-button, .translation-button, .dropdown-menu').dropdown({
							  inDuration: 300,
							  outDuration: 225,
							  constrainWidth: false,
							  hover: true,
							  gutter: 0,
							  belowOrigin: true,
							  alignment: 'left',
							  stopPropagation: false
							});
			
			$('.deactive_pr').off().on('click', function(){
				var dc_id = $(this).data('cn_id');
				$('#deactivate_purchase_note').modal('open');
				$('#deactivate_purchase_note #deactivate_pr_id').val(dc_id);
			});
			
			$('.remove_pr').off().on('click', function(){

				var dc_id = $('#deactivate_pr_id').val();
			
				$.ajax({

					url:base_url+'My_purchase_return/deactive_purchase_return',

					type:"POST",

					data:{'csrf_test_name':csrf_hash,"dc_id":dc_id,},

					success:function(res){
							if(res == true)
							{
								window.location.href=base_url+'My_purchase_return/manage_purchase_return';
								Materialize.toast('Purchase Return has been successfully deactivated', 2000,'green rounded');
							}
							else
							{
								window.location.href=base_url+'My_purchase_return/manage_purchase_return';
								Materialize.toast('Error while processing!', 2000,'red rounded');
							}
						},
					});										
			});
			
			$('.activate_pr').off().on('click',function(){
				var note_id = $(this).data('cn_id');;
				 $('#activate_purchase_return').modal('open');
				 $('#activate_purchase_return #activate_pr_id').val(note_id);
			});
			
			$('.active_pr').off().on('click', function(){
				var dc_id = $('#activate_pr_id').val();
				$.ajax({
					url:base_url+'My_purchase_return/activate_purchase_return',
					type:"POST",
					data:{'csrf_test_name':csrf_hash,"dc_id":dc_id,},
					success:function(res){
							if(res == true)
							{
								window.location.href=base_url+'My_purchase_return/manage_purchase_return';
								Materialize.toast('Purchase Return has been successfully activated', 3000,'green rounded');
							}
							else
							{
								window.location.href=base_url+'My_purchase_return/manage_purchase_return';
								Materialize.toast('Error while processing!', 2000,'red rounded');
							}
						},

					});										
			});
				var purchase_return = [];
				var pr_values = "";
				$(".pr_bulk_action").on('click', function(e) {
					if($(this).is(':checked')){
						var total_checked = $(".pr_bulk_action:checked").length;
						  if (jQuery.inArray(purchase_return, $(this).val())== '-1') {
							purchase_return.push($(this).val());
						  } 
					}
					else
					{
						var remove_id = $(this).val();	
						purchase_return = jQuery.grep(purchase_return, function(value) {
						  return value != remove_id;
						});
					}
						pr_values = purchase_return.join(",");
						$('#deactive_multiple_pr').attr('data-multi_pr',pr_values);
					});
						
		 },
	  }); 
	}	
	
 var table_purchase_credit_notes = "";
  	$(document).ready(function(){
		getPurchaseNotes();
		$('#search_by_supplier').change( function() { getPurchaseNotes() } );
		$('#search_by_type').change( function() { getPurchaseNotes() } );
		$('#search').on('keyup', function() { getPurchaseNotes() } );
	});
function getPurchaseNotes()
	{
		if(table_purchase_credit_notes != "")
		{ 
			table_purchase_credit_notes.destroy();
			$.fn.DataTable.ext.errMode = 'none';
			table_purchase_credit_notes.draw();
		}
 table_purchase_credit_notes =  $('#my_purchase_credit_debit_notes').DataTable( {
		 "bServerSide": true,
		"bProcessing": true,
		"bDeferRender": true,
		"aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0 , 2 , 3, 4, 5, 6, 7] }],
		 "aoColumns": [	
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			//{ "bSortable": false },
			//{ "bSortable": false },
			//{ "bSortable": false },
			{ "bSortable": false },
			{"bVisible": false},
			//{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			//{"bVisible": false},
			//{ "bSortable": false },
			
			//{ "bSortable": false },
			//"bSortable": false,
		],
		"fnServerParams": function ( aoData ) {
			aoData.push( { "name": "search_by_supplier", "value": $("#search_by_supplier").val() } );
			aoData.push( { "name": "search_by_type", "value": $("#search_by_type").val() } );
			aoData.push( { "name": "search", "value": $("#search").val() } );
		},
		"order": [],
		"bPaginate": true,
		"bLengthChange": false,
		"bFilter": true,
		"bInfo": false,
		"searching":false,
		"iDisplayLength": 10,
		"aLengthMenu": [10, 20, 40],
		"aaSorting": [[ 1, "desc" ]],
		"sAjaxSource": base_url+"My_purchase_credit_debit_notes/get_notes_list",
		"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
			if($(aData[9]+' div').hasClass('deactive_record')){
				$('td', nRow).parent('tr').addClass('deactivate-row');
			}
		
			$('td:first-child', nRow).addClass('bulk');
			$('td:nth-child(2)', nRow).addClass('profil-img');
			$('td:last-child', nRow).addClass('action-tab');
		 },
		 "fnDrawCallback": function () {
			  $('.dropdown-button, .translation-button, .dropdown-menu').dropdown({
							  inDuration: 300,
							  outDuration: 225,
							  constrainWidth: false,
							  hover: true,
							  gutter: 0,
							  belowOrigin: true,
							  alignment: 'left',
							  stopPropagation: false
							});
			
			$('.deactive_pur_cn').off().on('click', function(){
				var dc_id = $(this).data('cn_id');
				$('#deactivate_purchase_note').modal('open');
				$('#deactivate_purchase_note #deactivate_cn_id').val(dc_id);
			});
			
			$('.remove_note').off().on('click', function(){

				var dc_id = $('#deactivate_cn_id').val();
			
				$.ajax({

					url:base_url+'My_purchase_credit_debit_notes/deactive_note',

					type:"POST",

					data:{'csrf_test_name':csrf_hash,"dc_id":dc_id,},

					success:function(res){
							if(res == true)
							{
								window.location.href=base_url+'My_purchase_credit_debit_notes/manage_credit_debit_notes';
								Materialize.toast('Credit note deactivated successfully', 2000,'green rounded');
							}
							else
							{
								window.location.href=base_url+'My_purchase_credit_debit_notes/manage_credit_debit_notes';
								Materialize.toast('Error while processing!', 2000,'red rounded');
							}
						},
					});										
			});
			
			$('.activate_pr_cn').off().on('click',function(){
				var note_id = $(this).data('cn_id');;
				 $('#activate_purchase_note').modal('open');
				 $('#activate_purchase_note #activate_cn_id').val(note_id);
			});
			
			$('.active_note').off().on('click', function(){
				var dc_id = $('#activate_cn_id').val();
				$.ajax({
					url:base_url+'My_purchase_credit_debit_notes/activate_note',
					type:"POST",
					data:{'csrf_test_name':csrf_hash,"dc_id":dc_id,},
					success:function(res){
							if(res == true)
							{
								window.location.href=base_url+'My_purchase_credit_debit_notes/manage_credit_debit_notes';
								Materialize.toast('Credit / Debit Note has been successfully activated', 3000,'green rounded');
							}
							else
							{
								window.location.href=base_url+'My_purchase_credit_debit_notes/manage_credit_debit_notes';
								Materialize.toast('Error while processing!', 2000,'red rounded');
							}
						},

					});										
			});
				var purchase_notes = [];
				var pur_notes_values = "";
				$(".purc_cn_bulk_action").on('click', function(e) {
					if($(this).is(':checked')){
						var total_checked = $(".purc_cn_bulk_action:checked").length;
						  if (jQuery.inArray(purchase_notes, $(this).val())== '-1') {
							purchase_notes.push($(this).val());
						  } 
					}
					else
					{
						var remove_id = $(this).val();	
						purchase_notes = jQuery.grep(purchase_notes, function(value) {
						  return value != remove_id;
						});
					}
						pur_notes_values = purchase_notes.join(",");
						$('#deactive_multiple_purchase_notes').attr('data-multi_pr_note',pur_notes_values);
					});
						
		 },
	  }); 
	}	
	
 var table_credit_notes = "";
  	$(document).ready(function(){
		getNotes();
		$('#search_by_customer').change( function() { getNotes() } );
		$('#search_by_type').change( function() { getNotes() } );
		$('#search').on('keyup', function() { getNotes() } );
	});

function getNotes()
	{
		if(table_credit_notes != "")
		{ 
			table_credit_notes.destroy();
			$.fn.DataTable.ext.errMode = 'none';
			table_credit_notes.draw();
		}
 table_credit_notes =  $('#credi-notes').DataTable( {
		 "bServerSide": true,
		"bProcessing": true,
		"bDeferRender": true,
		"aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0 , 2 , 3, 4, 5, 6, 7] }],
		 "aoColumns": [	
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			//{ "bSortable": false },
			//{ "bSortable": false },
			//{ "bSortable": false },
			{ "bSortable": false },
			{"bVisible": false},
			//{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			//{"bVisible": false},
			//{ "bSortable": false },
			
			//{ "bSortable": false },
			//"bSortable": false,
		],
		"fnServerParams": function ( aoData ) {
			aoData.push( { "name": "search_by_customer", "value": $("#search_by_customer").val() } );
			aoData.push( { "name": "search_by_type", "value": $("#search_by_type").val() } );
			aoData.push( { "name": "search", "value": $("#search").val() } );
		},
		"order": [],
		"bPaginate": true,
		"bLengthChange": false,
		"bFilter": true,
		"bInfo": false,
		"searching":false,
		"iDisplayLength": 10,
		"aLengthMenu": [10, 20, 40],
		"aaSorting": [[ 1, "desc" ]],
		"sAjaxSource": base_url+"My_credit_debit_notes/get_list",
		"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
			if($(aData[9]+' div').hasClass('deactive_record')){
				$('td', nRow).parent('tr').addClass('deactivate-row');
			}
		
			$('td:first-child', nRow).addClass('bulk');
			$('td:nth-child(2)', nRow).addClass('profil-img');
			$('td:last-child', nRow).addClass('action-tab');
		 },
		 "fnDrawCallback": function () {
			  $('.dropdown-button, .translation-button, .dropdown-menu').dropdown({
							  inDuration: 300,
							  outDuration: 225,
							  constrainWidth: false,
							  hover: true,
							  gutter: 0,
							  belowOrigin: true,
							  alignment: 'left',
							  stopPropagation: false
							});
			
			$('.deactive_cn').off().on('click', function(){
				var dc_id = $(this).data('cn_id');
				$('#deactivate_note').modal('open');
				$('#deactivate_note #deactivate_note_id').val(dc_id);
			});
			
			$('.remove_cn').off().on('click', function(){

				var dc_id = $('#deactivate_note_id').val();
			
				$.ajax({

					url:base_url+'My_credit_debit_notes/deactive_note',

					type:"POST",

					data:{'csrf_test_name':csrf_hash,"dc_id":dc_id,},

					success:function(res){
							if(res == true)
							{
								window.location.href=base_url+'My_credit_debit_notes/manage_credit_debit_notes';
								Materialize.toast('Credit note deactivated successfully', 2000,'green rounded');
							}
							else
							{
								window.location.href=base_url+'My_credit_debit_notes/manage_credit_debit_notes';
								Materialize.toast('Error while processing!', 2000,'red rounded');
							}
						},
					});										
			});
			
			$('.activate_cn').off().on('click',function(){
				var rc_id = $(this).data('cn_id');;
				 $('#activate_note').modal('open');
				 $('#activate_note #activate_note_id').val(rc_id);
			});
			
			$('.active_cn').off().on('click', function(){
				var dc_id = $('#activate_note_id').val();
				$.ajax({
					url:base_url+'My_credit_debit_notes/activate_note',
					type:"POST",
					data:{'csrf_test_name':csrf_hash,"dc_id":dc_id,},
					success:function(res){
							if(res == true)
							{
								window.location.href=base_url+'My_credit_debit_notes/manage_credit_debit_notes';
								Materialize.toast('Credit / Debit Note has been successfully activated', 2000,'green rounded');
							}
							else
							{
								window.location.href=base_url+'My_credit_debit_notes/manage_credit_debit_notes';
								Materialize.toast('Error while processing!', 2000,'red rounded');
							}
						},

					});										
			});
				var notes = [];
				var notes_values = "";
				$(".cn_bulk_action").on('click', function(e) {
					if($(this).is(':checked')){
						var total_checked = $(".cn_bulk_action:checked").length;
						  if (jQuery.inArray(notes, $(this).val())== '-1') {
							notes.push($(this).val());
						  } 
					}
					else
					{
						var remove_id = $(this).val();	
						notes = jQuery.grep(notes, function(value) {
						  return value != remove_id;
						});
					}
						notes_values = notes.join(",");
						$('#deactive_multiple_notes').attr('data-multi_note',notes_values);
					});
						
		 },
	  }); 
	}
	
	 var table_adv_payments = "";
  	$(document).ready(function(){

		getAdvPayments();
		$('#search_by_supplier').change( function() { getAdvPayments() } );
		$('#search').on('keyup', function() { getAdvPayments() } );
	});
	
	function getAdvPayments()
	{
		if(table_adv_payments != "")
		{ 
			table_adv_payments.destroy();
			$.fn.DataTable.ext.errMode = 'none';
			table_adv_payments.draw();
		}
 table_adv_payments =  $('#my-add-paymnt').DataTable( {
		 "bServerSide": true,
		"bProcessing": true,
		"bDeferRender": true,
		"aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0 , 2 , 3, 4, 5, 6, 7] }],
		 "aoColumns": [	
			{ "bSortable": false },
			{ "bSortable": true },
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
		//	{"bVisible": false},
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			
			//{"bVisible": false},
			//{ "bSortable": false },
			
			//{ "bSortable": false },
			//"bSortable": false,
		],
		"fnServerParams": function ( aoData ) {
			aoData.push( { "name": "search_by_supplier", "value": $("#search_by_supplier").val() } );
			aoData.push( { "name": "search", "value": $("#search").val() } );
		},
		"order": [],
		"bPaginate": true,
		"bLengthChange": false,
		"bFilter": true,
		"bInfo": false,
		"searching":false,
		"iDisplayLength": 10,
		"aLengthMenu": [10, 20, 40],
		"aaSorting": [[ 1, "desc" ]],
		"sAjaxSource": base_url+"My_purchase_advance_payments/get_payments_list",
		"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
	
			if($(aData[11]+' div').hasClass('deactive_record')){
				$('td', nRow).parent('tr').addClass('deactivate-row');
			}
		
			$('td:first-child', nRow).addClass('bulk');
			$('td:nth-child(2)', nRow).addClass('profil-img');
			$('td:last-child', nRow).addClass('action-tab');
		 },
		 "fnDrawCallback": function () {
			  $('.dropdown-button, .translation-button, .dropdown-menu').dropdown({
							  inDuration: 300,
							  outDuration: 225,
							  constrainWidth: false,
							  hover: true,
							  gutter: 0,
							  belowOrigin: true,
							  alignment: 'left',
							  stopPropagation: false
							});
			
			$('.deactive_ap').off().on('click', function(){
				var ap_id = $(this).data('ap_id');
				$('#deactivate_ap').modal('open');
				$('#deactivate_ap #deactivate_ap_id').val(ap_id);
			});
			
			$('.remove_ap').off().on('click', function(){

				var dc_id = $('#deactivate_ap_id').val();
			
				$.ajax({

					url:base_url+'My_purchase_advance_payments/deactive_advance_payment',

					type:"POST",

					data:{'csrf_test_name':csrf_hash,"dc_id":dc_id,},

					success:function(res){
							if(res == true)
							{
								window.location.href=base_url+'My_purchase_advance_payments/manage_advance_payments';
								Materialize.toast('Advance Payment has been successfully deactivated', 3000,'green rounded');
							}
							else
							{
								window.location.href=base_url+'My_purchase_advance_payments/manage_advance_payments';
								Materialize.toast('Error while processing!', 2000,'red rounded');
							}
						},

					});										
			});
			
			$('.activate_ap').off().on('click',function(){
				var ap_id = $(this).data('ap_id');;
				 $('#activate_adv_payment').modal('open');
				 $('#activate_adv_payment #activate_ap_id').val(ap_id);
			});
			
			$('.active_ap').off().on('click', function(){
				var dc_id = $('#activate_ap_id').val();
				$.ajax({
					url:base_url+'My_purchase_advance_payments/activate_advance_payment',
					type:"POST",
					data:{'csrf_test_name':csrf_hash,"dc_id":dc_id,},
					success:function(res){
							if(res == true)
							{
								window.location.href=base_url+'My_purchase_advance_payments/manage_advance_payments';
								Materialize.toast('Advance Payment has been successfully activated', 3000,'green rounded');
							}
							else
							{
								window.location.href=base_url+'My_purchase_advance_payments/manage_advance_payments';
								Materialize.toast('Error while processing!', 2000,'red rounded');
							}
						},

					});										
			});
				var adv_payment = [];
				var payments_values = "";
				$(".ap_bulk_action").on('click', function(e) {
					if($(this).is(':checked')){
						var total_checked = $(".ap_bulk_action:checked").length;
						  if (jQuery.inArray(adv_payment, $(this).val())== '-1') {
							adv_payment.push($(this).val());
						  } 
					}
					else
					{
						var remove_id = $(this).val();	
						adv_payment = jQuery.grep(adv_payment, function(value) {
						  return value != remove_id;
						});
					}
						payments_values = adv_payment.join(",");
						$('#deactive_multiple_ap').attr('data-multi_ap',payments_values);
					});
						
		 },
	  }); 
	}
	
  var table_receipts = "";
  	$(document).ready(function(){

		getReceipts();
		$('#search_by_customer').change( function() { getReceipts() } );
		$('#search').on('keyup', function() { getReceipts() } );
	});
	
	function getReceipts()
	{
		if(table_receipts != "")
		{ 
			table_receipts.destroy();
			$.fn.DataTable.ext.errMode = 'none';
			table_receipts.draw();
		}
 table_receipts =  $('#adv_receipts').DataTable( {
		 "bServerSide": true,
		"bProcessing": true,
		"bDeferRender": true,
		"aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0 , 2 , 3, 4, 5, 6, 7] }],
		 "aoColumns": [	
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
		//	{"bVisible": false},
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			
			//{"bVisible": false},
			//{ "bSortable": false },
			
			//{ "bSortable": false },
			//"bSortable": false,
		],
		"fnServerParams": function ( aoData ) {
			aoData.push( { "name": "search_by_customer", "value": $("#search_by_customer").val() } );
			aoData.push( { "name": "search", "value": $("#search").val() } );
		},
		"order": [],
		"bPaginate": true,
		"bLengthChange": false,
		"bFilter": true,
		"bInfo": false,
		"searching":false,
		"iDisplayLength": 10,
		"aLengthMenu": [10, 20, 40],
		"aaSorting": [[ 1, "desc" ]],
		"sAjaxSource": base_url+"My_receipts/get_receipts_list",
		"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
	
			if($(aData[11]+' div').hasClass('deactive_record')){
				$('td', nRow).parent('tr').addClass('deactivate-row');
			}
		
			$('td:first-child', nRow).addClass('bulk');
			$('td:nth-child(2)', nRow).addClass('profil-img');
			$('td:last-child', nRow).addClass('action-tab');
		 },
		 "fnDrawCallback": function () {
			  $('.dropdown-button, .translation-button, .dropdown-menu').dropdown({
							  inDuration: 300,
							  outDuration: 225,
							  constrainWidth: false,
							  hover: true,
							  gutter: 0,
							  belowOrigin: true,
							  alignment: 'left',
							  stopPropagation: false
							});
			
			$('.deactive_rc').off().on('click', function(){
				var dc_id = $(this).data('dc_id');
				$('#deactivate_rc').modal('open');
				$('#deactivate_rc #deactivate_rc_id').val(dc_id);
			});
			
			$('.remove_rc').off().on('click', function(){

				var dc_id = $('#deactivate_rc_id').val();
			
				$.ajax({

					url:base_url+'My_receipts/deactive_receipt',

					type:"POST",

					data:{'csrf_test_name':csrf_hash,"dc_id":dc_id,},

					success:function(res){
							if(res == true)
							{
								window.location.href=base_url+'My_receipts/manage_receipts';
								Materialize.toast('Advance Receipt has been successfully deactivated', 3000,'green rounded');
							}
							else
							{
								window.location.href=base_url+'My_receipts/manage_receipts';
								Materialize.toast('Error while processing!', 2000,'red rounded');
							}
						},

					});										
			});
			
			$('.activate_rc').off().on('click',function(){
				var rc_id = $(this).data('rc_id');;
				 $('#activate_receipt').modal('open');
				 $('#activate_receipt #activate_rc_id').val(rc_id);
			});
			
			$('.active_rc').off().on('click', function(){
				var dc_id = $('#activate_rc_id').val();
				$.ajax({
					url:base_url+'My_receipts/activate_receipt',
					type:"POST",
					data:{'csrf_test_name':csrf_hash,"dc_id":dc_id,},
					success:function(res){
							if(res == true)
							{
								window.location.href=base_url+'My_receipts/manage_receipts';
								Materialize.toast('Advance Receipt has been successfully activated', 2000,'green rounded');
							}
							else
							{
								window.location.href=base_url+'My_receipts/manage_receipts';
								Materialize.toast('Error while processing!', 2000,'red rounded');
							}
						},

					});										
			});
				var receipts = [];
				var receipts_values = "";
				$(".dc_bulk_action").on('click', function(e) {
					if($(this).is(':checked')){
						var total_checked = $(".dc_bulk_action:checked").length;
						  if (jQuery.inArray(receipts, $(this).val())== '-1') {
							receipts.push($(this).val());
						  } 
					}
					else
					{
						var remove_id = $(this).val();	
						receipts = jQuery.grep(receipts, function(value) {
						  return value != remove_id;
						});
					}
						receipts_values = receipts.join(",");
						$('#deactive_multiple_rc').attr('data-multi_rc',receipts_values);
					});
						
		 },
	  }); 
	}
	
   /* var table_delivery = "";
  	$(document).ready(function(){
		getDeliveryChallans();
		$('#search_by_customer').change( function() { getDeliveryChallans() } );
		$('#search').on('keyup', function() { getDeliveryChallans() } );
	}); 
	*/
	function getDeliveryChallans()
	{
		if(table_delivery != "")
		{ 
			table_delivery.destroy();
			$.fn.DataTable.ext.errMode = 'none';
			table_delivery.draw();
		}
 table_delivery =  $('#delivery_challan_tbl').DataTable( {
		 "bServerSide": true,
		"bProcessing": true,
		"bDeferRender": true,
		"aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0 , 2 , 3, 4, 5, 6, 7] }],
		 "aoColumns": [	
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			{"bVisible": false},
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			
			//{"bVisible": false},
			//{ "bSortable": false },
			
			//{ "bSortable": false },
			//"bSortable": false,
		],
		"fnServerParams": function ( aoData ) {
			aoData.push( { "name": "search_by_customer", "value": $("#search_by_customer").val() } );
			aoData.push( { "name": "search", "value": $("#search").val() } );
		},
		"order": [],
		"bPaginate": true,
		"bLengthChange": false,
		"bFilter": true,
		"bInfo": false,
		"searching":false,
		"iDisplayLength": 10,
		"aLengthMenu": [10, 20, 40],
		"aaSorting": [[ 1, "desc" ]],
		"sAjaxSource": base_url+"My_delivery_challan/get_delivery_challan_list",
		"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
	
			if($(aData[12]+' div').hasClass('deactive_record')){
				$('td', nRow).parent('tr').addClass('deactivate-row');
			}
		
			$('td:first-child', nRow).addClass('bulk');
			$('td:nth-child(2)', nRow).addClass('profil-img');
			$('td:last-child', nRow).addClass('action-tab');
		 },
		 "fnDrawCallback": function () {
			  $('.dropdown-button, .translation-button, .dropdown-menu').dropdown({
							  inDuration: 300,
							  outDuration: 225,
							  constrainWidth: false,
							  hover: true,
							  gutter: 0,
							  belowOrigin: true,
							  alignment: 'left',
							  stopPropagation: false
							});
			
			$('.deactive_dc').off().on('click', function(){
				var dc_id = $(this).data('dc_id');
				console.log(dc_id);
				$('#deactivate_dc').modal('open');
				$('#deactivate_dc #deactivate_dc_id').val(dc_id);
			});
			
			$('.remove_dc').off().on('click', function(){

				var dc_id = $('#deactivate_dc_id').val();
			
				$.ajax({

					url:base_url+'My_delivery_challan/deactive_delivery_challan',

					type:"POST",

					data:{'csrf_test_name':csrf_hash,"dc_id":dc_id,},

					success:function(res){
							if(res == true)
							{
								window.location.href=base_url+'My_delivery_challan/manage_delivery_challan';
								Materialize.toast('Delivery Challan has been successfully deactivated', 2000,'green rounded');
							}
							else
							{
								window.location.href=base_url+'My_delivery_challan/manage_delivery_challan';
								Materialize.toast('Error while processing!', 2000,'red rounded');
							}
						},

					});										
			});
			
			$('.activate_dc').off().on('click',function(){
				var dc_id = $(this).data('dc_id');;
				 $('#activate_delivery_challan').modal('open');
				 $('#activate_delivery_challan #activate_dc_id').val(dc_id);
			});
			
			$('.active_dc').off().on('click', function(){
				var dc_id = $('#activate_dc_id').val();
				$.ajax({
					url:base_url+'My_delivery_challan/activate_delivery_challan',
					type:"POST",
					data:{'csrf_test_name':csrf_hash,"dc_id":dc_id,},
					success:function(res){
							if(res == true)
							{
								window.location.href=base_url+'My_delivery_challan/manage_delivery_challan';
								Materialize.toast('Delivery Challan has been successfully activated', 2000,'green rounded');
							}
							else
							{
								window.location.href=base_url+'My_delivery_challan/manage_delivery_challan';
								Materialize.toast('Error while processing!', 2000,'red rounded');
							}
						},

					});										
			});
					var challans = [];
					var challans_values = "";
					$(".dc_bulk_action").on('click', function(e) {
						if($(this).is(':checked')){
							var total_checked = $(".dc_bulk_action:checked").length;
							  if (jQuery.inArray(challans, $(this).val())== '-1') {
								challans.push($(this).val());
							  } 
						}
						else
						{
							var remove_id = $(this).val();	
							challans = jQuery.grep(challans, function(value) {
							  return value != remove_id;
							});
						}
							challans_values = challans.join(",");
							$('#deactive_multiple_dc').attr('data-multi_dc',challans_values);
						});
						
		 },
	  }); 
	}
	
	 var table_bill_supply = "";
  	$(document).ready(function(){
		getBillSupplies();
		$('#search_by_customer').change( function() { getBillSupplies() } );
		$('#search').on('keyup', function() { getBillSupplies() } );
	}); 
	
	function getBillSupplies()
	{
		if(table_bill_supply != "")
		{ 
			table_bill_supply.destroy();
			$.fn.DataTable.ext.errMode = 'none';
			table_bill_supply.draw();
		}
 table_bill_supply =  $('#bill_supply_tbl').DataTable( {
		 "bServerSide": true,
		"bProcessing": true,
		"bDeferRender": true,
		"aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0 , 2 , 3, 4, 5, 6, 7] }],
		 "aoColumns": [	
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
		],
		"fnServerParams": function ( aoData ) {
			aoData.push( { "name": "search_by_customer", "value": $("#search_by_customer").val() } );
			aoData.push( { "name": "search", "value": $("#search").val() } );
		},
		"order": [],
		"bPaginate": true,
		"bLengthChange": false,
		"bFilter": true,
		"bInfo": false,
		"searching":false,
		"iDisplayLength": 10,
		"aLengthMenu": [10, 20, 40],
		"aaSorting": [[ 1, "desc" ]],
		"sAjaxSource": base_url+"My_bills/get_bill_supply_list",
		"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
	
			if($(aData[7]+' div').hasClass('deactive_record')){
				$('td', nRow).parent('tr').addClass('deactivate-row');
			}
		
			$('td:first-child', nRow).addClass('bulk');
			$('td:nth-child(2)', nRow).addClass('profil-img');
			$('td:last-child', nRow).addClass('action-tab');
		 },
		 "fnDrawCallback": function () {
			  $('.dropdown-button, .translation-button, .dropdown-menu').dropdown({
							  inDuration: 300,
							  outDuration: 225,
							  constrainWidth: false,
							  hover: true,
							  gutter: 0,
							  belowOrigin: true,
							  alignment: 'left',
							  stopPropagation: false
							});
			
			$('.deactive_bs').off().on('click', function(){
				var bs_id = $(this).data('bs_id');
				$('#deactivate_bs').modal('open');
				$('#deactivate_bs #deactivate_bs_id').val(bs_id);
			});
			
			$('.remove_bs').off().on('click', function(){

				var bs_id = $('#deactivate_bs_id').val();
			
				$.ajax({

					url:base_url+'My_bills/deactive_bill_supply',

					type:"POST",

					data:{'csrf_test_name':csrf_hash,"bs_id":bs_id,},

					success:function(res){
							if(res == true)
							{
								window.location.href=base_url+'My_bills/manage_bill_of_supply';
								Materialize.toast('Bill Of Supply has been successfully deactivated', 2000,'green rounded');
							}
							else
							{
								window.location.href=base_url+'My_bills/manage_bill_of_supply';
								Materialize.toast('Error while processing!', 2000,'red rounded');
							}
						},

					});										
			});
			
			$('.activate_bs').off().on('click',function(){
				var bs_id = $(this).data('bs_id');;
				 $('#activate_bill_supply').modal('open');
				 $('#activate_bill_supply #activate_bs_id').val(bs_id);
			});
			
			$('.active_bs').off().on('click', function(){
				var bs_id = $('#activate_bs_id').val();
				$.ajax({
					url:base_url+'My_bills/activate_bill_supply',
					type:"POST",
					data:{'csrf_test_name':csrf_hash,"bs_id":bs_id,},
					success:function(res){
							if(res == true)
							{
								window.location.href=base_url+'My_bills/manage_bill_of_supply';
								Materialize.toast('Bill Of Supply has been successfully activated', 2000,'green rounded');
							}
							else
							{
								window.location.href=base_url+'My_bills/manage_bill_of_supply';
								Materialize.toast('Error while processing!', 2000,'red rounded');
							}
						},

					});		
				
			});
			 
			 $(".bs_send_email").on('click',  function(e) {
				var bs_id = $(this).data('bs_id');
				$('#send_email_to_cust').modal('open');
				$('#send_email_to_cust #send_mail_bs_id').val(bs_id);
			});

			$('.send_mail_bs').off().on('click',function(){
				var bs_id = $('#send_mail_bs_id').val();
				$.ajax({
					url:base_url+'My_bills/send_email_to_customer',
					type:"POST",
					data:{'csrf_test_name':csrf_hash,"bs_id":bs_id},
					success:function(res){
							if(res == true)
							{
								Materialize.toast('Customer Details sent successfully!', 2000,'green rounded');
							}
							else
							{
								Materialize.toast('Error while processing!', 2000,'red rounded');
							}
						},
					});										
			});
					
						
		 },
	  }); 
	}
	 
	
	
   var table_activity = "";
  	$(document).ready(function(){
		getActivities();
		$('#search_by_user').change( function() { getActivities() } );
		$('#search_by_company').change( function() { getActivities() } );
		$('#search_start_date').change( function() { getActivities() } );
		$('#search_end_date').change( function() { getActivities() } );
		$('#search').on('keyup', function() { getActivities() } );
	}); 
	
	function getActivities()
	{
		if(table_activity != "")
		{ 
			table_activity.destroy();
			$.fn.DataTable.ext.errMode = 'none';
			table_activity.draw();
		}
 table_activity =  $('#activity').DataTable( {
	 "bServerSide": true,
	"bProcessing": true,
	"bDeferRender": true,
	"aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0 , 2 , 3, 4, 5, 6, 7] }],
	 "aoColumns": [	
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{"bVisible": false},
		//{ "bSortable": false },
		
		{ "bSortable": false },
		//"bSortable": false,
	],
	"fnServerParams": function ( aoData ) {
		aoData.push( { "name": "search_by_user", "value": $("#search_by_user").val() } );
		aoData.push( { "name": "search_by_company", "value": $("#search_by_company").val() } );
		aoData.push( { "name": "search_start_date", "value": $("#search_start_date").val() } );
		aoData.push( { "name": "search_end_date", "value": $("#search_end_date").val() } );
		aoData.push( { "name": "search", "value": $("#search").val() } );
	},
    "order": [],
    "bPaginate": true,
    "bLengthChange": false,
    "bFilter": true,
    "bInfo": false,
    "searching":false,
	"iDisplayLength": 10,
	"aLengthMenu": [10, 20, 40],
	"aaSorting": [[ 1, "desc" ]],
	"sAjaxSource": base_url+"My_account/get_activities",
	"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {

		if(aData[7] == '1'){
			$('td', nRow).parent('tr').addClass('deactivate-row');	
		}
		$('td:first-child', nRow).addClass('bulk');
		$('td:nth-child(2)', nRow).addClass('profil-img');
		$('td:last-child', nRow).addClass('action-tab');
     },
	 "fnDrawCallback": function () {
		  $('.dropdown-button, .translation-button, .dropdown-menu').dropdown({
						  inDuration: 300,
						  outDuration: 225,
						  constrainWidth: false,
						  hover: true,
						  gutter: 0,
						  belowOrigin: true,
						  alignment: 'left',
						  stopPropagation: false
						});
		
				var activities = [];
				var activities_values = "";
				$(".activities_bulk_action").on('click', function(e) {
					if($(this).is(':checked')){
						var total_checked = $(".activities_bulk_action:checked").length;
						  if (jQuery.inArray(activities, $(this).val())== '-1') {
							activities.push($(this).val());
						  } 
					}
					else
					{
						var remove_id = $(this).val();	
						activities = jQuery.grep(activities, function(value) {
						  return value != remove_id;
						});
					}
						activities_values = activities.join(",");
						$('#deactive_multiple_activities').attr('data-multi_activity',activities_values);
					});			
	 },
  }); 
	}
  
  var table_company = "";
  	$(document).ready(function(){
		//var oTable='';
		//getCompanies();
		/* $('#state').change( function() { get_table() } );
		$('#country').change( function() { get_table() } );
		$('#search').on('keyup', function() { get_table() } ); */
	});
 /* function get_table(){
			$('#company-profile').destroy();
			$.fn.DataTable.ext.errMode = 'none';
			$('#company-profile').draw();
 } */
 function getCompanies()
	{
		if(table_company != "")
		{ 
			table_company.destroy();
			$.fn.DataTable.ext.errMode = 'none';
			table_company.draw();
		}
 table_company =  $('#company-profile1').DataTable( {
	 "bServerSide": true,
	"bProcessing": true,
	"bDeferRender": true,
    "aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0 , 1 , 3 , 4 , 6, 7, 8] }],
	 "aoColumns": [	
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": true },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": true },
		{ "bSortable": false },
		{"bVisible": false},
		{ "bSortable": false },
		
		//{ "bSortable": false },
		//"bSortable": false,
	],
	"fnServerParams": function ( aoData ) {
	aoData.push( { "name": "country", "value": $("#country").val() } );
	aoData.push( { "name": "state", "value": $("#state").val() } );
	aoData.push( { "name": "search", "value": $("#search").val() } );
	},
    "order": [],
    "bPaginate": true,
    "bLengthChange": false,
    "bFilter": true,
    "bInfo": false,
    "searching":false,
	"iDisplayLength": 10,
	"aLengthMenu": [10, 20, 40],
	"aaSorting": [[ 1, "desc" ]],
	"sAjaxSource": base_url+"profile/get_company_profiles",
	"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
		//if($(aData[7]+' div.status-action').hasClass('deactivate'))
		/*if($(aData[8]).find('li.deactivation').length > 0){
		}
		else {
			$('td', nRow).addClass('change_backgrnd');
		}*/
		/*if(aData[6] == 'Admin'){
			//table.dataTable.display.table-type1 tbody tr
			$('td', nRow).addClass('change_backgrnd');
			//$('td', nRow).parent('tr').css('background','lavender');
		}*/
		if($(aData[8]+' div').hasClass('deactive_record')){
			$('td', nRow).parent('tr').addClass('deactivate-row');
		}

		$('td:first-child', nRow).addClass('bulk');
		$('td:nth-child(2)', nRow).addClass('profil-img');
		$('td:last-child', nRow).addClass('action-tab');
     },
	 "fnDrawCallback": function () {
		  $('.dropdown-button, .translation-button, .dropdown-menu').dropdown({
						  inDuration: 300,
						  outDuration: 225,
						  constrainWidth: false,
						  hover: true,
						  gutter: 0,
						  belowOrigin: true,
						  alignment: 'left',
						  stopPropagation: false
						});
									
			$('.deactive_company_profile').off().on('click', function(){

				var profile = $(this).data('profileid');

				 $('#deactive_company_modal').modal('open');

				 $('#deactive_company_modal #company_profile').val(profile);

			});
			
			$('.deactive_company_status').off().on('click', function(){

				var profile_id = $('#company_profile').val();
			
				$.ajax({

					url:base_url+'Company_profile/deactive_company',

					type:"POST",

					data:{'csrf_test_name':csrf_hash,"profile_id":profile_id,},

					success:function(res){
							if(res == true)
							{
								window.location.href=base_url+'Company_profile/manage_company_profile';
								Materialize.toast('Company profile deactivated successfully', 2000,'green rounded');
							}
							else
							{
								window.location.href=base_url+'Company_profile/manage_company_profile';
								Materialize.toast('Error while processing!', 2000,'red rounded');
							}
						},

					});										
			});
			
			$('.active_company_profile').off().on('click',function(){

				var profile = $(this).data('profileid');

				 $('#active_company_profile').modal('open');

				 $('#active_company_profile #activate_cmp_profile_id').val(profile);

			});
			
			$('.active_cmp_profile').off().on('click',function(){

				var profile_id = $('#activate_cmp_profile_id').val();

				$.ajax({

					url:base_url+'Company_profile/activate_company',

					type:"POST",

					data:{'csrf_test_name':csrf_hash,"profile_id":profile_id,},

					success:function(res){
							if(res == true)
							{
								window.location.href=base_url+'Company_profile/manage_company_profile';
								Materialize.toast('Company Profile has been successfully activated', 2000,'green rounded');
							}
							else
							{
								window.location.href=base_url+'Company_profile/manage_company_profile';
								Materialize.toast('Error while processing!', 2000,'red rounded');
							}

						},

					});										
			});
			
			var cmp_profiles = [];
				var cmp_profiles_values = "";
				$(".company_profile_bulk_action").on('click', function(e) {
					if($(this).is(':checked')){
						var total_checked = $(".company_profile_bulk_action:checked").length;
						  if (jQuery.inArray(cmp_profiles, $(this).val())== '-1') {
							cmp_profiles.push($(this).val());
						  } 
					}
					else
					{
						var remove_id = $(this).val();	
						cmp_profiles = jQuery.grep(cmp_profiles, function(value) {
						  return value != remove_id;
						});
					}
						cmp_profiles_values = cmp_profiles.join(",");
						$('#deactive_multiple_cmp_profiles').attr('data-multi_profiles',cmp_profiles_values);
					});
			
	 },
  }); 
	}
	
var table_customers = "";
	
	$(document).ready(function(){
		
	//	getCustomers();
  		//$('#state').change( function() { getCustomers() } );
		//$('#country').change( function() { getCustomers() } );
		//$('#search').on('keyup', function() { getCustomers() } );
	});
	
	function getCustomers111() {
		
		if(table_customers != "")
		{ 	table_customers.destroy();
			$.fn.DataTable.ext.errMode = 'none';
			table_customers.draw();
		}
		
	table_customers = $('#my-customers').DataTable( {
			"bServerSide": true,
			"bProcessing": true,
			"bDeferRender": true,
			"aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0 , 1 , 3 , 4 , 6] }],
			 "aoColumns": [	
				{ "bSortable": false },
				{ "bSortable": true },
				{ "bSortable": false },
				{ "bSortable": false },
				{ "bSortable": true },
				{ "bSortable": false },
				{ "bSortable": false },
				//{"bVisible": false},
				{"bVisible": false},
				{ "bSortable": false },
			],
			"fnServerParams": function ( aoData ) {
				aoData.push( { "name": "country", "value": $("#country").val() } );
				aoData.push( { "name": "state", "value": $("#state").val() } );
				aoData.push( { "name": "search", "value": $("#search").val() } );
			},
			"order": [],
			"bPaginate": true,
			"bLengthChange": false,
			"bFilter": true,
			"bInfo": false,
			"searching":false,
			"iDisplayLength": 10,
			"aLengthMenu": [10, 20, 40],
			"aaSorting": [[ 1, "desc" ]],
			"sAjaxSource": base_url+"My_customers/get_customer_profiles",
			"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
				/*if($(aData[8]).find('li.deactivation').length > 0){
				}
				else {
					$('td', nRow).addClass('change_backgrnd');
				}*/
				/*if(aData[6] == 'Admin'){
					//table.dataTable.display.table-type1 tbody tr
					$('td', nRow).addClass('change_backgrnd');
					//$('td', nRow).parent('tr').css('background','lavender');
				}*/
				if($(aData[8]+' div').hasClass('deactive_record')){
					$('td', nRow).parent('tr').addClass('deactivate-row');
				}
		
				$('td:first-child', nRow).addClass('bulk');
				$('td:last-child', nRow).addClass('action-tab');
			 },
			 "fnDrawCallback": function () {
				  $('.dropdown-button, .translation-button, .dropdown-menu').dropdown({
								  inDuration: 300,
								  outDuration: 225,
								  constrainWidth: false,
								  hover: true,
								  gutter: 0,
								  belowOrigin: true,
								  alignment: 'left',
								  stopPropagation: false
								});
								
				$('.deactive_customer').on('click',function(){
		
						var customer_id = $(this).data('customer_id');
		
						 $('#deactive_customer_model').modal('open');
		
						 $('#deactive_customer_model #deactive_customer_id').val(customer_id);
		
					});
		
					$('.deactive_customer_profile').off().on('click',function(){
		
						var customer_id = $('#deactive_customer_id').val();
		
						$.ajax({
		
							url:base_url+'My_customers/deactive_customer',
		
							type:"POST",
		
							data:{'csrf_test_name':csrf_hash,"customer_id":customer_id,},
		
							success:function(res){
									if(res == true)
									{
										window.location.href=base_url+'My_customers/list_customers';
										Materialize.toast('Customer has been successfully deactivated', 2000,'green rounded');
									}
									else
									{
										window.location.href=base_url+'My_customers/list_customers';
										Materialize.toast('Error while processing!', 2000,'red rounded');
									}
								},
							});										
					});
					
					$('.activate_customer').off().on('click',function(){
		
						var customer_id = $(this).data('customer_id');
		
						 $('#active_customer_model').modal('open');
		
						 $('#active_customer_model #activate_customer_id').val(customer_id);
		
					});
					
					$('.activate_customer_profile').off().on('click',function(){
		
						var activate_customer_id = $('#activate_customer_id').val();
		
						$.ajax({
		
							url:base_url+'My_customers/active_customer',
		
							type:"POST",
		
							data:{'csrf_test_name':csrf_hash,"activate_customer_id":activate_customer_id,},
		
							success:function(res){
									if(res == true)
									{
										window.location.href=base_url+'My_customers/list_customers';
										Materialize.toast('Client activated successfully', 2000,'green rounded');
									}
									else
									{
										window.location.href=base_url+'My_customers/list_customers';
										Materialize.toast('Error while processing!', 2000,'red rounded');
									}
		
								},
		
							});										
					});
					
					$('.send_invitation').on('click', function(){
						var customer_id = $(this).data('cust_id');
						
						$('#invite_customer_modal').modal('open');
						$('#invite_customer_modal #customer_id').val(customer_id);
					});	
					
					$('.invite_customer').off().on('click',function(){
						var customer_id = $('#customer_id').val();
						$.ajax({
							url:base_url+'My_customers/invite_customers',
							type:"POST",
							data:{'csrf_test_name':csrf_hash,"customer_id":customer_id,},
							success:function(res){
									if(res == true)
									{
										Materialize.toast('Your customer has been invited to experience the convenience of My Easy Invoices', 5000,'green rounded');
										location.reload();
									}
									else
									{
										Materialize.toast('Error while processing!', 2000,'red rounded');
										location.reload();
									}
								},
							});										
					});	
					$(document).off().on('click', '.send_mail' ,function(){
						var customer_id = $(this).data('customer_id');
						var customer_email = $(this).data('cust_email');
						$('#email_customer_modal').modal('open');
						$('#email_customer_modal #email_cust_id').val(customer_id);
						$('#email_customer_modal #email_cust_email').val(customer_email);
					});
					
					$('.send_mail_customer').off().on('click',function(){
						var customer_id = $('#email_cust_id').val();
						var customer_email = $('#email_cust_email').val();
						$.ajax({
							url:base_url+'My_customers/send_email_to_customer',
							type:"POST",
							data:{'csrf_test_name':csrf_hash,"customer_id":customer_id,"customer_email":customer_email,},
							success:function(res){
									if(res == true)
									{
										Materialize.toast('Customer Details sent successfully!', 2000,'green rounded');
									}
									else
									{
										Materialize.toast('Error while processing!', 2000,'red rounded');
									}
								},
							});										
					});	
					
					
				var customer_profiles = [];
				var customer_profiles_values = "";

				$(".customer_profile_bulk_action").off().on('click', function() {
					if($(this).is(':checked')){
						  if (jQuery.inArray(customer_profiles, $(this).val())== '-1') {
							customer_profiles.push($(this).val());
						  } 
					}
					else
					{
						var remove_id = $(this).val();	
						customer_profiles = jQuery.grep(customer_profiles, function(value) {
						  return value != remove_id;
						});
					}
						customer_profiles_values = customer_profiles.join(",");
						$('#deactive_multiple_customer_profiles').attr('data-multi_profiles',customer_profiles_values);
					});
					
			$(document).on('click', '.view_accessport' ,function(){
				if($(this).prop('checked')==true){
					var customer_id = $(this).val();
					$('#view_add_modal-popup').modal('open');
						$('.portal_access_yes').on('click',function(){
						$.ajax({
							url:base_url+'My_customers/edit_portal_access',
							type:"POST",
							data:{'csrf_test_name':csrf_hash,"customer_id":customer_id,"change_status":1},
							success:function(res){
									location.reload();
								},
							});
						});		
				} else {
					var customer_id = $(this).val();
					$('#view_modal-popup').modal('open');
					$('.portal_access_yes').on('click',function(){
						$.ajax({
							url:base_url+'My_customers/edit_portal_access',
							type:"POST",
							data:{'csrf_test_name':csrf_hash,"customer_id":customer_id,"change_status":0},
							success:function(res){
									location.reload();
								},
							});
						});	
				}
			});	
			 },
		  }); 
	} 
  
  
 /*var table_personal_profiles = "";
	
	$(document).ready(function(){	
	
  		$('#state').change( function() { getProfiles() } );
		$('#country').change( function() { getProfiles() } );
		$('#search').on('keyup', function() { getProfiles() } );
	});
	*/
	/*function getProfiles12() {
		
		if(table_personal_profiles != "")
		{ 	table_personal_profiles.destroy();
			$.fn.DataTable.ext.errMode = 'none';
			table_personal_profiles.draw();
		}
		
	table_personal_profiles =  $('#personal-profile').DataTable( {
			"bServerSide": true,
			"bProcessing": true,
			"bDeferRender": true,
			"aoColumnDefs": [{ "bSortable": false, "aTargets": [0,1,3,4,5,7] }],
			 "aoColumns": [	
				{ "bSortable": false },
				{ "bSortable": false },
				{ "bSortable": true },
				{ "bSortable": true },
				{ "bSortable": false },
				{ "bSortable": true },
				//{ "bSortable": true },
				//{ "bSortable": false },
				{"bVisible": false},
				{ "bSortable": false },
				//"bSortable": false,
			],
			"fnServerParams": function ( aoData ) {
				aoData.push( { "name": "country", "value": $("#country").val() } );
				aoData.push( { "name": "state", "value": $("#state").val() } );
				aoData.push( { "name": "search", "value": $("#search").val() } );
			},
			"order": [],
			"bPaginate": true,
			"bLengthChange": false,
			"bFilter": true,
			"bInfo": false,
			"searching":false,
			"iDisplayLength": 10,
			"aLengthMenu": [10, 20, 40],
			"aaSorting": [[ 1, "desc" ]],
			"sAjaxSource": base_url+"Personal_profile/get_profiles",
			"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {

				if($(aData[7]+' div').hasClass('deactive_record')){
					$('td', nRow).parent('tr').addClass('deactivate-row');
				}
		
				$('td:first-child', nRow).addClass('bulk');
				$('td:nth-child(2)', nRow).addClass('profil-img');
				$('td:last-child', nRow).addClass('action-tab');
			 },
			 "fnDrawCallback": function () {
				 $('.dropdown-button, .translation-button, .dropdown-menu').dropdown({
								  inDuration: 300,
								  outDuration: 225,
								  constrainWidth: false,
								  hover: true,
								  gutter: 0,
								  belowOrigin: true,
								  alignment: 'left',
								  stopPropagation: false
								});
								
					$('.deactive_profile').on('click',function(){
		
						var profile = $(this).data('profileid');
		
						 $('#deactive_profile').modal('open');
		
						 $('#deactive_profile #profile_id').val(profile);
		
					});
		
					$('.deactive_user').on('click',function(){
		
						var profile_id = $('#profile_id').val();
		
						$.ajax({
		
							url:base_url+'Personal_profile/deactive_profile/' + profile_id,
		
							type:"POST",
		
							data:{"profile_id":profile_id,},
		
							success:function(res){
									if(res == true)
									{
										window.location.href=base_url+'Personal_profile/manage_profile';
										Materialize.toast('Profile has been successfully deactivated', 2000,'green rounded');
									}
									else
									{
										window.location.href=base_url+'Personal_profile/manage_profile';
										Materialize.toast('Error while processing. Please try again', 2000,'red rounded');
									}
								},
		
							});										
		
					});
					
					$('.active_profile').on('click',function(){
		
						var profile = $(this).data('profileid');
		
						 $('#active_profile').modal('open');
		
						 $('#active_profile #activate_profile_id').val(profile);
		
					});
		
					$('.active_user').on('click',function(){
		
						var profile_id = $('#activate_profile_id').val();
		
						$.ajax({
		
							url:base_url+'Personal_profile/activate_profile/' + profile_id,
		
							type:"POST",
		
							data:{"profile_id":profile_id,},
		
							success:function(res){
									if(res == true)
									{
										window.location.href=base_url+'Personal_profile/manage_profile';
										Materialize.toast('Profile has been successfully activated', 2000,'green rounded');
									}
									else
									{
										window.location.href=base_url+'Personal_profile/manage_profile';
										Materialize.toast('Error while processing. Please try again', 2000,'red rounded');
									}
		
								},
		
							});										
					});
					
				var profiles = [];				
				var profiles_values = "";
				$(".personal_profile_bulk_action").on('click', function(e) {
					if($(this).is(':checked')){
						var total_checked = $(".personal_profile_bulk_action:checked").length;
						  if (jQuery.inArray(profiles, $(this).val())== '-1') {
							profiles.push($(this).val());
						  } 
					}
					else
					{
						var remove_id = $(this).val();	
						profiles = jQuery.grep(profiles, function(value) {
						  return value != remove_id;
						});
					}
						profiles_values = profiles.join(",");
						$('#deactive_multiple_profiles').attr('data-multi_profiles',profiles_values);
					});
								
			$('.print_profiles').on('click' ,function(){
				var profile = $(this).data('profileid');
				var url = '&flag=' + 1;
				window.location.href = base_url+'Personal_profile/view_profile/' + profile + '?' + url;
			});	
			
			 },
		  }); 
	}*/
 
  var table_invoices = "";
  	$(document).ready(function(){
		getInvoices();
		$('#select_by_customer').change( function() { getInvoices() } );
		$('#start_date').change( function() { getInvoices() } );
		$('#end_date').change( function() { getInvoices() } );
		$('#search_by_status').change( function() { getInvoices() } );
		$('#search').on('keyup', function() { getInvoices() } );
	});
 
 function getInvoices()
	{
		if(table_invoices != "")
		{ 
			table_invoices.destroy();
			$.fn.DataTable.ext.errMode = 'none';
			table_invoices.draw();
		}
 table_invoices =  $('#my-invoices').DataTable( {
	 "bServerSide": true,
	"bProcessing": true,
	"bDeferRender": true,
    "aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0 , 1 , 3 , 4 , 6, 7] }],
	 "aoColumns": [	
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{"bVisible": false},
	//	{"bVisible": false},
		{ "bSortable": false },
		
	],
	"fnServerParams": function ( aoData ) {
	aoData.push( { "name": "customer", "value": $("#select_by_customer").val() } );
	aoData.push( { "name": "start_date", "value": $("#start_date").val() } );
	aoData.push( { "name": "end_date", "value": $("#end_date").val() } );
	aoData.push( { "name": "status", "value": $("#search_by_status").val() } );
	aoData.push( { "name": "search", "value": $("#search").val() } );
	},
    "order": [],
    "bPaginate": true,
    "bLengthChange": false,
    "bFilter": true,
    "bInfo": false,
    "searching":false,
	"iDisplayLength": 10,
	"aLengthMenu": [10, 20, 40],
	"aaSorting": [[ 1, "desc" ]],
	"sAjaxSource": base_url+"My_invoices/get_invoices",
	"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {

		if($(aData[8]+' div').hasClass('deactive_record')){
					console.log(aData[7]);
			$('td', nRow).parent('tr').addClass('deactivate-row');
		}

		$('td:first-child', nRow).addClass('bulk');
		$('td:nth-child(2)', nRow).addClass('profil-img');
		$('td:last-child', nRow).addClass('action-tab');
     },
	 "fnDrawCallback": function () {
		  $('.dropdown-button, .translation-button, .dropdown-menu').dropdown({
						  inDuration: 300,
						  outDuration: 225,
						  constrainWidth: false,
						  hover: true,
						  gutter: 0,
						  belowOrigin: true,
						  alignment: 'left',
						  stopPropagation: false
						});
									
			$('.delete_invoice').off().on('click', function(){

				
				var invoice_id = $(this).data('invid');
				 $('#delete_invoice').modal('open');

				 $('#delete_invoice #remove_invoice_id').val(invoice_id);

			});
		 	$('.delete_this_invoice').off().on('click', function(){

				var invoice_id = $('#remove_invoice_id').val();
			
				$.ajax({

					url:base_url+'My_invoices/delete_invoice',

					type:"POST",

					data:{'csrf_test_name':csrf_hash,"invoice_id":invoice_id,},

					success:function(res){
							if(res == true)
							{
								window.location.href=base_url+'My_invoices/manage_invoices';
								Materialize.toast('Sales invoice deleted successfully', 2000,'green rounded');
							}
							else
							{
								window.location.href=base_url+'My_invoices/manage_invoices';
								Materialize.toast('Error while processing!', 2000,'red rounded');
							}
						},

					});										
			});
			$('.deactive_invoice').off().on('click', function(){
					var invoice_id = $(this).data('invoice_id');	

				 $('#deactivate_invoice').modal('open');

				 $('#deactivate_invoice #remove_invoice_id').val(invoice_id);
				});
			$('.deactive_this_invoice').off().on('click', function(){
				var invoice_id = $('#deactivate_invoice #remove_invoice_id').val();
				$.ajax({

					url:base_url+'My_invoices/deactivate_invoice',

					type:"POST",

					data:{'csrf_test_name':csrf_hash,"invoice_id":invoice_id,},

					success:function(res){
							if(res == true)
							{
								window.location.href=base_url+'My_invoices/manage_invoices';
								Materialize.toast('Invoice has been successfully deactivated', 2000,'green rounded');
							}
							else
							{
								window.location.href=base_url+'My_invoices/manage_invoices';
								Materialize.toast('Error while processing!', 2000,'red rounded');
							}
						},

					});										
			});
			
			$('.active_invoice').off().on('click',function(){

				var invoice_id = $(this).data('invoice_id');

				 $('#activate_invoice').modal('open');

				 $('#activate_invoice #active_invoice_id').val(invoice_id);

			});
			
			$('.activate_this_invoice').off().on('click',function(){

				var invoice_id = $('#active_invoice_id').val();

				$.ajax({

					url:base_url+'My_invoices/activate_invoice',

					type:"POST",

					data:{'csrf_test_name':csrf_hash,"invoice_id":invoice_id,},

					success:function(res){
							if(res == true)
							{
								window.location.href=base_url+'My_invoices/manage_invoices';
								Materialize.toast('Invoice has been successfully activated', 2000,'green rounded');
							}
							else
							{
								window.location.href=base_url+'My_invoices/manage_invoices';
								Materialize.toast('Error while processing!', 2000,'red rounded');
							}

						},

					});										
			});
			
			var invoices = [];
				var invoices_values = "";
				$(".invoice_bulk_action").on('click', function(e) {
					if($(this).is(':checked')){
						var total_checked = $(".invoice_bulk_action:checked").length;
						  if (jQuery.inArray(invoices, $(this).val())== '-1') {
							invoices.push($(this).val());
						  } 
					}
					else
					{
						var remove_id = $(this).val();	
						invoices = jQuery.grep(invoices, function(value) {
						  return value != remove_id;
						});
					}
						invoices_values = invoices.join(",");
						$('#deactive_multiple_invoices').attr('data-multi_invoices',invoices_values);
					});
					
					$(document).off().on('click', '.email_invoice' ,function(){
						var customer_id = $(this).data('cust_id');
						var invoice_id = $(this).data('invoice_id');
						$('#email_invoice_modal').modal('open');
						$('#email_invoice_modal #customer_id').val(customer_id);
						$('#email_invoice_modal #invoice_id').val(invoice_id);
					});
					$('.mail_invoice').off().on('click',function(){
								var customer_id = $('#customer_id').val();
								var invoice_id = $('#invoice_id').val();
								$.ajax({
									url:base_url+'My_invoices/send_mail_to_customers',
									type:"POST",
									data:{'csrf_test_name':csrf_hash,"customer_id":customer_id,"invoice_id":invoice_id,},
									success:function(res){
											if(res == true)
											{
												Materialize.toast('Invoice Details sent successfully!', 2000,'green rounded');
											}
											else
											{
												Materialize.toast('Error while processing!', 2000,'red rounded');
											}
										},
									});										
							});	
	 },
  }); 
	}
  var table_purchase_invoices = "";
  	$(document).ready(function(){
		getPurchaseInvoices();
		$('#supplier_name').change( function() { getPurchaseInvoices() } );
		$('#purchase_start_date').change( function() { getPurchaseInvoices() } );
		$('#purchase_end_date').change( function() { getPurchaseInvoices() } );
		$('#search_by_pay_status').change( function() { getPurchaseInvoices() } );
		$('#search_purchase_inv').on('keyup', function() { getPurchaseInvoices() } );
	});
 
 function getPurchaseInvoices()
	{
		if(table_purchase_invoices != "")
		{ 
			table_purchase_invoices.destroy();
			$.fn.DataTable.ext.errMode = 'none';
			table_purchase_invoices.draw();
		}
 table_purchase_invoices =  $('#my-purchase-invoices').DataTable( {
	 "bServerSide": true,
	"bProcessing": true,
	"bDeferRender": true,
    "aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0 , 1 , 3 , 4 , 6, 7] }],
	 "aoColumns": [	
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{"bVisible": false},
	//	{"bVisible": false},
		{ "bSortable": false },
		
	],
	"fnServerParams": function ( aoData ) {
	aoData.push( { "name": "supplier_name", "value": $("#supplier_name").val() } );
	aoData.push( { "name": "start_date", "value": $("#purchase_start_date").val() } );
	aoData.push( { "name": "end_date", "value": $("#purchase_end_date").val() } );
	aoData.push( { "name": "status", "value": $("#search_by_pay_status").val() } );
	aoData.push( { "name": "search", "value": $("#search_purchase_inv").val() } );
	},
    "order": [],
    "bPaginate": true,
    "bLengthChange": false,
    "bFilter": true,
    "bInfo": false,
    "searching":false,
	"iDisplayLength": 10,
	"aLengthMenu": [10, 20, 40],
	"aaSorting": [[ 1, "desc" ]],
	"sAjaxSource": base_url+"my_purchase_invoices/get_purchase_invoices",
	"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {

		if($(aData[8]+' div').hasClass('deactive_record')){
					console.log(aData[7]);
			$('td', nRow).parent('tr').addClass('deactivate-row');
		}

		$('td:first-child', nRow).addClass('bulk');
		$('td:nth-child(2)', nRow).addClass('profil-img');
		$('td:last-child', nRow).addClass('action-tab');
     },
	 "fnDrawCallback": function () {
		  $('.dropdown-button, .translation-button, .dropdown-menu').dropdown({
						  inDuration: 300,
						  outDuration: 225,
						  constrainWidth: false,
						  hover: true,
						  gutter: 0,
						  belowOrigin: true,
						  alignment: 'left',
						  stopPropagation: false
						});
									
			/*$('.delete_invoice').off().on('click', function(){

				
				var invoice_id = $(this).data('invid');
				 $('#delete_invoice').modal('open');

				 $('#delete_invoice #remove_invoice_id').val(invoice_id);

			});
		 	$('.delete_this_invoice').off().on('click', function(){

				var invoice_id = $('#remove_invoice_id').val();
			
				$.ajax({

					url:base_url+'my_purchase_invoices/delete_invoice',

					type:"POST",

					data:{"invoice_id":invoice_id,},

					success:function(res){
							if(res == true)
							{
								window.location.href=base_url+'my_purchase_invoices/manage_invoices';
								Materialize.toast('Sales invoice deleted successfully', 2000,'green rounded');
							}
							else
							{
								window.location.href=base_url+'my_purchase_invoices/manage_invoices';
								Materialize.toast('Error while processing!', 2000,'red rounded');
							}
						},

					});										
			});*/
			$('.deactive_purchase_invoice').off().on('click', function(){
					var invoice_id = $(this).data('invoice_id');	

				 $('#deactivate_invoice').modal('open');

				 $('#deactivate_invoice #remove_invoice_id').val(invoice_id);
				});
			$('.deactive_this_purchase_invoice').off().on('click', function(){

				var invoice_id = $('#remove_invoice_id').val();
			
				$.ajax({

					url:base_url+'my_purchase_invoices/deactivate_invoice',

					type:"POST",

					data:{'csrf_test_name':csrf_hash,"invoice_id":invoice_id,},

					success:function(res){
							if(res == true)
							{
								window.location.href=base_url+'my_purchase_invoices/manage_invoices';
								Materialize.toast('Invoice has been successfully deactivated', 2000,'green rounded');
							}
							else
							{
								window.location.href=base_url+'my_purchase_invoices/manage_invoices';
								Materialize.toast('Error while processing!', 2000,'red rounded');
							}
						},

					});										
			});
			
			$('.active_purchase_invoice').off().on('click',function(){

				var invoice_id = $(this).data('invoice_id');

				 $('#activate_invoice').modal('open');

				 $('#activate_invoice #active_invoice_id').val(invoice_id);

			});
			
			$('.activate_this_purchase_invoice').off().on('click',function(){

				var invoice_id = $('#active_invoice_id').val();

				$.ajax({

					url:base_url+'my_purchase_invoices/activate_invoice',

					type:"POST",

					data:{'csrf_test_name':csrf_hash,"invoice_id":invoice_id,},

					success:function(res){
							if(res == true)
							{
								window.location.href=base_url+'my_purchase_invoices/manage_invoices';
								Materialize.toast('Invoice has been successfully activated', 2000,'green rounded');
							}
							else
							{
								window.location.href=base_url+'my_purchase_invoices/manage_invoices';
								Materialize.toast('Error while processing!', 2000,'red rounded');
							}

						},

					});										
			});
			
			var invoices = [];
				var invoices_values = "";
				$(".invoice_bulk_action").on('click', function(e) {
					if($(this).is(':checked')){
						var total_checked = $(".invoice_bulk_action:checked").length;
						  if (jQuery.inArray(invoices, $(this).val())== '-1') {
							invoices.push($(this).val());
						  } 
					}
					else
					{
						var remove_id = $(this).val();	
						invoices = jQuery.grep(invoices, function(value) {
						  return value != remove_id;
						});
					}
						invoices_values = invoices.join(",");
						$('#deactive_multiple_invoices').attr('data-multi_invoices',invoices_values);
					});
					
					$(document).off().on('click', '.email_purchase_invoice' ,function(){
						var customer_id = $(this).data('cust_id');
						var invoice_id = $(this).data('invoice_id');
						$('#email_purchase_invoice_modal').modal('open');
						$('#email_purchase_invoice_modal #supplier_id').val(customer_id);
						$('#email_purchase_invoice_modal #invoice_id').val(invoice_id);
					});
					$('.mail_purchase_invoice').off().on('click',function(){
								var supplier_id = $('#supplier_id').val();
								var invoice_id = $('#invoice_id').val();
								$.ajax({
									url:base_url+'my_purchase_invoices/send_mail_to_customers',
									type:"POST",
									data:{'csrf_test_name':csrf_hash,"supplier_id":supplier_id,"invoice_id":invoice_id,},
									success:function(res){
											if(res == true)
											{
												Materialize.toast('Invoice Details sent successfully!', 2000,'green rounded');
											}
											else
											{
												Materialize.toast('Error while processing!', 2000,'red rounded');
											}
										},
									});										
							});	
	 },
  }); 
	}
 var table_manuf_services = "";
  	$(document).ready(function(){
		//getServices_2();
		//$('#search').on('keyup', function() { getServices_2() } );
	});

 function getServices_2()
	{
		if(table_manuf_services != "")
		{ 
			table_manuf_services.destroy();
			$.fn.DataTable.ext.errMode = 'none';
			table_manuf_services.draw();
		}
		
 table_manuf_services =  $('#category_table_manufacture').DataTable( {
		
		 "bServerSide": true,
	"bProcessing": true,
	"bDeferRender": true,
    "aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0 , 1 , 3 , 4 , 5, 6] }],
	 "aoColumns": [	
			{ "bSortable": false },
				{ "bSortable": true },
				{ "bSortable": false },
				{ "bSortable": false },
				{ "bSortable": false },
				{ "bSortable": false },
				{ "bSortable": false },
				{ "bSortable": false },
				{ "bSortable": false },
				null,null,
				{ "bSortable": false },
				{"bVisible": false},
				{ "bSortable": false },
	],
	"fnServerParams": function ( aoData ) {
			aoData.push( { "name": "search", "value": $("#search").val() } );
	},
	"order": [],
    "bPaginate": true,
    "bLengthChange": false,
    "bFilter": true,
    "bInfo": false,
    "searching":false,
	"iDisplayLength": 10,
	"aLengthMenu": [10, 20, 40],
	"aaSorting": [[ 1, "desc" ]],
	"sAjaxSource": base_url+"My_services/get_services_manufacturing",
	"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
		if($(aData[13]+' div').hasClass('deactive_record')){
			$('td', nRow).parent('tr').addClass('deactivate-row');
		}

		$('td:first-child', nRow).addClass('bulk');
		$('td:nth-child(2)', nRow).addClass('profil-img');
		$('td:last-child', nRow).addClass('action-tab');
     },
			"fnDrawCallback": function () {
				//actDltLink();
				  $('.dropdown-button, .translation-button, .dropdown-menu').dropdown({
						  inDuration: 300,
						  outDuration: 225,
						  constrainWidth: false,
						  hover: true,
						  gutter: 0,
						  belowOrigin: true,
						  alignment: 'left',
						  stopPropagation: false
						});
						
				$('.deactive_service').off().on('click', function(){
	
					var service_id = $(this).data('service_id');
	
					 $('#remove_service').modal('open');
	
					 $('#remove_service #remove_service_id').val(service_id);
	
				});
				$('.deactive_this_service').on('click', function(){

				var service_id = $('#remove_service_id').val();
			
				$.ajax({

					url:base_url+'My_services/deactive_service',

					type:"POST",

					data:{'csrf_test_name':csrf_hash,"service_id":service_id,},

					success:function(res){
							if(res == true)
							{
								window.location.href=base_url+'My_services/manage_services_for_manufacturing';
								Materialize.toast('Service has been deactivated successfully', 2000,'green rounded');
							}
							else
							{
								window.location.href=base_url+'My_services/manage_services_for_manufacturing';
								Materialize.toast('Error while processing!', 2000,'red rounded');
							}
						},

					});										
			});
			$('.active_service').off().on('click',function(){

				var service_id = $(this).data('service_id');

				 $('#active_service_modal').modal('open');

				 $('#active_service_modal #active_service_id').val(service_id);

			});
			
			$('.activate_this_service').off().on('click',function(){

				var service_id = $('#active_service_id').val();

				$.ajax({

					url:base_url+'My_services/activate_service',

					type:"POST",

					data:{'csrf_test_name':csrf_hash,"service_id":service_id,},

					success:function(res){
							if(res == true)
							{
								window.location.href=base_url+'My_services/manage_services_for_manufacturing';
								Materialize.toast('Service has been activated successfully', 2000,'green rounded');
							}
							else
							{
								window.location.href=base_url+'My_services/manage_services_for_manufacturing';
								Materialize.toast('Error while processing!', 2000,'red rounded');
							}

						},

					});										
			});
			var services = [];
				var services_values = "";
				$(".services_bulk_action").on("click", function() {
					if($(this).is(':checked')){
						var total_checked = $(".services_bulk_action:checked").length;
						  if (jQuery.inArray(services, $(this).val())== '-1') {
							services.push($(this).val());
						  } 
					}
					else
					{
						var remove_id = $(this).val();	
						services = jQuery.grep(services, function(value) {
						  return value != remove_id;
						});
					}
						services_values = services.join(",");
						$('#deactive_multiple_services').attr('data-multi_profiles',services_values);
					});
			}
    });
	}

/* var table_sales = "";
  	$(document).ready(function(){
		getSalesReturns();
		$('#search').on('keyup', function() { getSalesReturns() } );
		$('#search_by_customers').on('change', function() { getSalesReturns() } );
	});*/

 function getSalesReturns1()
	{
		if(table_sales != "")
		{ 
			table_sales.destroy();
			$.fn.DataTable.ext.errMode = 'none';
			table_sales.draw();
		}
		
 table_sales =  $('#my-sales-return1').DataTable( {
	 "bServerSide": true,
	"bProcessing": true,
	"bDeferRender": true,
    "aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0 , 1 , 3 , 4 , 5, 6, 7, 8] }],
	 "aoColumns": [	
		{ "bSortable": false },
		{ "bSortable": true },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		//{"bVisible": false},
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
	],
	"fnServerParams": function ( aoData ) {
			aoData.push( { "name": "search", "value": $("#search").val() } );
			aoData.push( { "name": "search_by_customers", "value": $("#search_by_customers").val() } );
	},
    "order": [],
    "bPaginate": true,
    "bLengthChange": false,
    "bFilter": true,
    "bInfo": false,
    "searching":false,
	"iDisplayLength": 10,
	"aLengthMenu": [10, 20, 40],
	"aaSorting": [[ 1, "desc" ]],
	"sAjaxSource": base_url+"My_sales_return/get_sales_returns",
	"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
		
		if($(aData[8]+' div').hasClass('deactive_record')){
			$('td', nRow).parent('tr').addClass('deactivate-row');
		}

		$('td:first-child', nRow).addClass('bulk');
		$('td:nth-child(2)', nRow).addClass('profil-img');
		$('td:last-child', nRow).addClass('action-tab');
     },
	 "fnDrawCallback": function () {
		  $('.dropdown-button, .translation-button, .dropdown-menu').dropdown({
						  inDuration: 300,
						  outDuration: 225,
						  constrainWidth: false,
						  hover: true,
						  gutter: 0,
						  belowOrigin: true,
						  alignment: 'left',
						  stopPropagation: false
						});
									
			$('.sales_delete').off().on('click', function(){
				var sales_id = $(this).data('id');
				 $('#delete_sales').modal('open');
				 $('#delete_sales #remove_sales_return').val(sales_id);
			});
			
			$('.deactivate_sr').off().on('click',function(){
				var sales_id = $('#remove_sales_return').val();
				$.ajax({
					url:base_url+'My_sales_return/deactive_sales',
					type:"POST",
					data:{'csrf_test_name':csrf_hash,"sales_id":sales_id,},
					success:function(res){
							if(res == true)
							{
								window.location.href=base_url+'My_sales_return/all';
								Materialize.toast('Sales Return has been removed successfully', 2000,'green rounded');
							}
							else
							{
								window.location.href=base_url+'My_sales_return/all';
								Materialize.toast('Error while processing!', 2000,'red rounded');
							}
						},
					});										
			});
			
			$('.activate_sr').off().on('click',function(){
				var sales_id = $(this).data('id');
				 $('#actiavte_sales').modal('open');
				 $('#actiavte_sales #activate_sales_return').val(sales_id);
			});
			
			$('.active_sr').off().on('click',function(){
				var sales_id = $('#activate_sales_return').val();
				$.ajax({
					url:base_url+'My_sales_return/activate_sales',
					type:"POST",
					data:{'csrf_test_name':csrf_hash,"sales_id":sales_id,},
					success:function(res){
							if(res == true)
							{
								window.location.href=base_url+'My_sales_return/all';
								Materialize.toast('Sales Return has been activated successfully', 2000,'green rounded');
							}
							else
							{
								window.location.href=base_url+'My_sales_return/all';
								Materialize.toast('Error while processing!', 2000,'red rounded');
							}
						},
					});										
			});
			
				var sales = [];
				var sales_values = "";
			$(".sales_bulk_action").off().on('click', function() {
					if($(this).is(':checked')){
						  if (jQuery.inArray(sales, $(this).val())== '-1') {
							sales.push($(this).val());
						  } 
					}
					else
					{
						var remove_id = $(this).val();	
						sales = jQuery.grep(sales, function(value) {
						  return value != remove_id;
						});
					}
						sales_values = sales.join(",");
						$('#deactive_multiple_sales').attr('data-multi_sales',sales_values);
					});
						
																
				/*var services = [];
				var services_values = "";
				$(".services_bulk_action").on("click", function() {
					if($(this).is(':checked')){
						var total_checked = $(".services_bulk_action:checked").length;
						  if (jQuery.inArray(services, $(this).val())== '-1') {
							services.push($(this).val());
						  } 
					}
					else
					{
						var remove_id = $(this).val();	
						services = jQuery.grep(services, function(value) {
						  return value != remove_id;
						});
					}
						services_values = services.join(",");
						$('#deactive_multiple_services').attr('data-multi_profiles',services_values);
					});*/
			
	 },
  }); 
	}
	
	
			
  var table_services = "";
  	$(document).ready(function(){
		//getServices();
		$('#search').on('keyup', function() { getServices() } );
	});
 
 function getServices()
	{
		if(table_services != "")
		{ 
			table_services.destroy();
			$.fn.DataTable.ext.errMode = 'none';
			table_services.draw();
		}
		
 table_services =  $('#services').DataTable( {
	 "bServerSide": true,
	"bProcessing": true,
	"bDeferRender": true,
    "aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0 , 1 , 3 , 4 , 5, 6] }],
	 "aoColumns": [	
		{ "bSortable": false },
		{ "bSortable": true },
		{ "bSortable": false },
		{ "bSortable": false },
		{ "bSortable": false },
		{"bVisible": false},
		{ "bSortable": false },
	],
	"fnServerParams": function ( aoData ) {
			aoData.push( { "name": "search", "value": $("#search").val() } );
	},
    "order": [],
    "bPaginate": true,
    "bLengthChange": false,
    "bFilter": true,
    "bInfo": false,
    "searching":false,
	"iDisplayLength": 10,
	"aLengthMenu": [10, 20, 40],
	"aaSorting": [[ 1, "desc" ]],
	"sAjaxSource": base_url+"My_services/get_services",
	"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
		if($(aData[6]+' div').hasClass('deactive_record')){
			$('td', nRow).parent('tr').addClass('deactivate-row');
		}

		$('td:first-child', nRow).addClass('bulk');
		$('td:nth-child(2)', nRow).addClass('profil-img');
		$('td:last-child', nRow).addClass('action-tab');
     },
	 "fnDrawCallback": function () {
		  $('.dropdown-button, .translation-button, .dropdown-menu').dropdown({
						  inDuration: 300,
						  outDuration: 225,
						  constrainWidth: false,
						  hover: true,
						  gutter: 0,
						  belowOrigin: true,
						  alignment: 'left',
						  stopPropagation: false
						});
									
			$('.deactive_service').off().on('click', function(){

				var service_id = $(this).data('service_id');

				 $('#remove_service').modal('open');

				 $('#remove_service #remove_service_id').val(service_id);

			});
			
			$('.deactive_this_service').on('click', function(){

				var service_id = $('#remove_service_id').val();
			
				$.ajax({

					url:base_url+'My_services/deactive_service',

					type:"POST",

					data:{'csrf_test_name':csrf_hash,"service_id":service_id,},

					success:function(res){
							if(res == true)
							{
								window.location.href=base_url+'My_services/manage_services';
								Materialize.toast('Service has been deactivated successfully', 2000,'green rounded');
							}
							else
							{
								window.location.href=base_url+'My_services/manage_services';
								Materialize.toast('Error while processing!', 2000,'red rounded');
							}
						},

					});										
			});
			
			$('.active_service').off().on('click',function(){

				var service_id = $(this).data('service_id');

				 $('#active_service_modal').modal('open');

				 $('#active_service_modal #active_service_id').val(service_id);

			});
			
			$('.activate_this_service').off().on('click',function(){

				var service_id = $('#active_service_id').val();

				$.ajax({

					url:base_url+'My_services/activate_service',

					type:"POST",

					data:{'csrf_test_name':csrf_hash,"service_id":service_id,},

					success:function(res){
							if(res == true)
							{
								window.location.href=base_url+'My_services/manage_services';
								Materialize.toast('Service has been activated successfully', 2000,'green rounded');
							}
							else
							{
								window.location.href=base_url+'My_services/manage_services';
								Materialize.toast('Error while processing!', 2000,'red rounded');
							}

						},

					});										
			});
														
				var services = [];
				var services_values = "";
				$(".services_bulk_action").on("click", function() {
					if($(this).is(':checked')){
						var total_checked = $(".services_bulk_action:checked").length;
						  if (jQuery.inArray(services, $(this).val())== '-1') {
							services.push($(this).val());
						  } 
					}
					else
					{
						var remove_id = $(this).val();	
						services = jQuery.grep(services, function(value) {
						  return value != remove_id;
						});
					}
						services_values = services.join(",");
						$('#deactive_multiple_services').attr('data-multi_profiles',services_values);
					});
			
	 },
  }); 
	}
		
	
  var table = $('#data-table-row-grouping').DataTable({
    "columnDefs": [{
      "visible": false,
      "targets": 2
    }],
    "order": [
      [2, 'asc']
    ],
    "displayLength": 25,
    "drawCallback": function(settings) {
      var api = this.api();
      var rows = api.rows({
        page: 'current'
      }).nodes();
      var last = null;

      api.column(2, {
        page: 'current'
      }).data().each(function(group, i) {
        if (last !== group) {
          $(rows).eq(i).before(
            '<tr class="group"><td colspan="5">' + group + '</td></tr>'
          );

          last = group;
        }
      });
    }
  });

  // Order by the grouping
  $('#data-table-row-grouping tbody').on('click', 'tr.group', function() {
    var currentOrder = table.order()[0];
    if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
      table.order([2, 'desc']).draw();
    }
    else {
      table.order([2, 'asc']).draw();
    }
  });


});

// Datatable click on select issue fix
$(window).on('load', function() {
  $(".dropdown-content.select-dropdown li").on("click", function() {
    var that = this;
    setTimeout(function() {
      if ($(that).parent().parent().find('.select-dropdown').hasClass('active')) {
        // $(that).parent().removeClass('active');
        $(that).parent().parent().find('.select-dropdown').removeClass('active');
        $(that).parent().hide();
      }
    }, 100);
  });
});



/* 14-May-2018 shubham.js Start */
 var table_expense_voucher = "";
  	$(document).ready(function(){
		get_expense_vouchers();
		$('#search_by_customer').change( function() { get_expense_vouchers() } );
		$('#search').on('keyup', function() { get_expense_vouchers() } );
	}); 
	
	function get_expense_vouchers()
	{
		if(table_expense_voucher != "")
		{ 
			table_expense_voucher.destroy();
			$.fn.DataTable.ext.errMode = 'none';
			table_expense_voucher.draw();
		}
    table_expense_voucher =  $('#expense_voucher').DataTable( {
        "bServerSide": true,
        "bProcessing": true,
        "bDeferRender": true,
        "aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0 , 2 , 3, 4, 5, 6, 7] }],
         "aoColumns": [	
          { "bSortable": false },
          { "bSortable": false },
          { "bSortable": false },
          { "bSortable": false },
          { "bSortable": false },
        ],
        "fnServerParams": function ( aoData ) {
          aoData.push( { "name": "search_by_customer", "value": $("#search_by_customer").val() } );
          aoData.push( { "name": "search", "value": $("#search").val() } );
        },
        "order": [],
        "bPaginate": true,
        "bLengthChange": false,
        "bFilter": true,
        "bInfo": false,
        "searching":false,
        "iDisplayLength": 10,
        "aLengthMenu": [10, 20, 40],
        "aaSorting": [[ 1, "desc" ]],
        "sAjaxSource": base_url+"My_vouchers/get_expense_vouchers",
        "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {

          if($(aData[7]+' div').hasClass('deactive_record')){
            $('td', nRow).parent('tr').addClass('deactivate-row');
          }

          $('td:first-child', nRow).addClass('bulk');
          $('td:nth-child(2)', nRow).addClass('profil-img');
          $('td:last-child', nRow).addClass('action-tab');
         },
         "fnDrawCallback": function () {
            $('.dropdown-button, .translation-button, .dropdown-menu').dropdown({
                    inDuration: 300,
                    outDuration: 225,
                    constrainWidth: false,
                    hover: true,
                    gutter: 0,
                    belowOrigin: true,
                    alignment: 'left',
                    stopPropagation: false
                  });

          $('.deactive_voucher').off().on('click', function(){
            var bs_id = $(this).data('bs_id');
            $('#deactive_voucher').modal('open');
            $('#deactive_voucher_id').val(bs_id);
            
          });

          $('.remove_vc').off().on('click', function(){

            var bs_id = $('#deactive_voucher_id').val();

            $.ajax({

              url:base_url+'My_vouchers/deactive_expense_voucher',

              type:"POST",

              data:{'csrf_test_name':csrf_hash,"vc_id":bs_id,},

              success:function(res){
                  if(res == true)
                  {
                    window.location.href=base_url+'My_vouchers/view_voucher_list';
                    Materialize.toast('Expense Voucher deleted successfully', 2000,'green rounded');
                  }
                  else
                  {
                    window.location.href=base_url+'My_vouchers/view_voucher_list';
                    Materialize.toast('Error while processing!', 2000,'red rounded');
                  }
                },

              });										
          });

          $('.activate_vc').off().on('click',function(){
            var bs_id = $(this).data('bs_id');;
             $('#activate_voucher').modal('open');
             $('#activate_vc_id').val(bs_id);
          });

          $('.active_vc').off().on('click', function(){
            var vc_id = $('#activate_vc_id').val();
            $.ajax({
              url:base_url+'My_vouchers/active_expense_voucher',
              type:"POST",
              data:{'csrf_test_name':csrf_hash,"vc_id":vc_id,},
              success:function(res){
                  if(res == true)
                  {
                    window.location.href=base_url+'My_vouchers/view_voucher_list';
                    Materialize.toast('Expense Voucher deleted successfully', 2000,'green rounded');
                  }
                  else
                  {
                    window.location.href=base_url+'My_vouchers/view_voucher_list';
                    Materialize.toast('Error while processing!', 2000,'red rounded');
                  }
                },

              });		

          });

           $(".bs_send_email").on('click',  function(e) {
            var bs_id = $(this).data('bs_id');
            $('#send_email_to_cust').modal('open');
            $('#send_email_to_cust #send_mail_bs_id').val(bs_id);
          });

          $('.send_mail_bs').off().on('click',function(){
            var bs_id = $('#send_mail_bs_id').val();
            $.ajax({
              url:base_url+'My_bills/send_email_to_customer',
              type:"POST",
              data:{'csrf_test_name':csrf_hash,"bs_id":bs_id},
              success:function(res){
                  if(res == true)
                  {
                    Materialize.toast('Customer Details sent successfully!', 2000,'green rounded');
                  }
                  else
                  {
                    Materialize.toast('Error while processing!', 2000,'red rounded');
                  }
                },
              });										
          });


         },
        }); 
	}
/* 14-May-2018 shubham.js End */


/* 19-May-2018 shubham.js Start */
    var table_purchase_bill_supply = "";
  	$(document).ready(function(){
		getPurchase_BillSupplies();
		$('#search_by_customer').change( function() { getPurchase_BillSupplies() } );
		$('#search').on('keyup', function() { getPurchase_BillSupplies() } );
	}); 
	
	function getPurchase_BillSupplies()
	{
		if(table_purchase_bill_supply != "")
		{ 
			table_purchase_bill_supply.destroy();
			$.fn.DataTable.ext.errMode = 'none';
			table_purchase_bill_supply.draw();
		}
  
    table_purchase_bill_supply =  $('#purchase_bill_supply').DataTable( {
    "bServerSide": true,
		"bProcessing": true,
		"bDeferRender": true,
		"aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0 , 2 , 3, 4, 5, 6, 7] }],
		 "aoColumns": [	
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
		],
		"fnServerParams": function ( aoData ) {
			aoData.push( { "name": "search_by_customer", "value": $("#search_by_customer").val() } );
			aoData.push( { "name": "search", "value": $("#search").val() } );
		},
		"order": [],
		"bPaginate": true,
		"bLengthChange": false,
		"bFilter": true,
		"bInfo": false,
		"searching":false,
		"iDisplayLength": 10,
		"aLengthMenu": [10, 20, 40],
		"aaSorting": [[ 1, "desc" ]],
		"sAjaxSource": base_url+"My_purchase_bill_supply/get_bill_supply_list",
		"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
	
			if($(aData[7]+' div').hasClass('deactive_record')){
				$('td', nRow).parent('tr').addClass('deactivate-row');
			}
		
			$('td:first-child', nRow).addClass('bulk');
			$('td:nth-child(2)', nRow).addClass('profil-img');
			$('td:last-child', nRow).addClass('action-tab');
		 },
		 "fnDrawCallback": function () {
			  $('.dropdown-button, .translation-button, .dropdown-menu').dropdown({
							  inDuration: 300,
							  outDuration: 225,
							  constrainWidth: false,
							  hover: true,
							  gutter: 0,
							  belowOrigin: true,
							  alignment: 'left',
							  stopPropagation: false
							});
			
			$('.deactive_bs').off().on('click', function(){
				var bs_id = $(this).data('bs_id');
      	$('#deactivate_bs').modal('open');
				$('#deactivate_bs #deactivate_bs_id').val(bs_id);
			});
			
			$('.remove_bs').off().on('click', function(){

				var bs_id = $('#deactivate_bs_id').val();
			
      	$.ajax({

					url:base_url+'My_purchase_bill_supply/deactive_bill_supply',

					type:"POST",

					data:{'csrf_test_name':csrf_hash,"bs_id":bs_id,},

					success:function(res){
							if(res == true)
							{
								window.location.href=base_url+'My_purchase_bill_supply/view_bill_supply_list';
								Materialize.toast('Bill Of Supply has been successfully deactivated', 2000,'green rounded');
							}
							else
							{
								window.location.href=base_url+'My_purchase_bill_supply/view_bill_supply_list';
								Materialize.toast('Error while processing!', 2000,'red rounded');
							}
						},

					});										
			});
			
			$('.activate_bs').off().on('click',function(){
          var bs_id = $(this).data('bs_id');
    		 $('#activate_bill_supply').modal('open');
				 $('#activate_bill_supply #activate_bs_id').val(bs_id);
			});
			
			$('.active_bs').off().on('click', function(){
				var bs_id = $('#activate_bs_id').val();
				$.ajax({
					url:base_url+'My_purchase_bill_supply/activate_bill_supply',
					type:"POST",
					data:{'csrf_test_name':csrf_hash,"bs_id":bs_id,},
					success:function(res){
							if(res == true)
							{
								window.location.href=base_url+'My_purchase_bill_supply/view_bill_supply_list';
								Materialize.toast('Bill Of Supply has been successfully activated', 2000,'green rounded');
							}
							else
							{
								window.location.href=base_url+'My_purchase_bill_supply/view_bill_supply_list';
								Materialize.toast('Error while processing!', 2000,'red rounded');
							}
						},

					});		
				
			});
			 
			 $(".bs_send_email").on('click',  function(e) {
				var bs_id = $(this).data('bs_id');
				$('#send_email_to_cust').modal('open');
				$('#send_email_to_cust #send_mail_bs_id').val(bs_id);
			});

			$('.send_mail_bs').off().on('click',function(){
				var bs_id = $('#send_mail_bs_id').val();
				$.ajax({
					url:base_url+'My_purchase_bill_supply/send_email_to_customer',
					type:"POST",
					data:{'csrf_test_name':csrf_hash,"bs_id":bs_id},
					success:function(res){
							if(res == true)
							{
								Materialize.toast('Customer Details sent successfully!', 2000,'green rounded');
							}
							else
							{
								Materialize.toast('Error while processing!', 2000,'red rounded');
							}
						},
					});										
			});
					
						
		 },
	  }); 
	}
	
/* 19-May-2018 shubham.js End */

/* 21-May-2018 shubham.js Start */
    var table_purchase_order = "";
  	$(document).ready(function(){
		getPurchase_orders();
		$('#search_by_customer').change( function() { getPurchase_orders() } );
		$('#search').on('keyup', function() { getPurchase_orders() } );
	}); 
	
	function getPurchase_orders()
	{
		if(table_purchase_order != "")
		{ 
			table_purchase_order.destroy();
			$.fn.DataTable.ext.errMode = 'none';
			table_purchase_order.draw();
		}
  
    table_purchase_order =  $('#purchase_orders').DataTable( {
    "bServerSide": true,
		"bProcessing": true,
		"bDeferRender": true,
		"aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0 , 2 , 3, 4, 5, 6, 7] }],
		 "aoColumns": [	
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
			{ "bSortable": false },
		],
		"fnServerParams": function ( aoData ) {
			aoData.push( { "name": "search_by_customer", "value": $("#search_by_customer").val() } );
			aoData.push( { "name": "search", "value": $("#search").val() } );
		},
		"order": [],
		"bPaginate": true,
		"bLengthChange": false,
		"bFilter": true,
		"bInfo": false,
		"searching":false,
		"iDisplayLength": 10,
		"aLengthMenu": [10, 20, 40],
		"aaSorting": [[ 1, "desc" ]],
		"sAjaxSource": base_url+"My_purchase_orders/get_purchase_order_list",
		"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
	
			if($(aData[7]+' div').hasClass('deactive_record')){
				$('td', nRow).parent('tr').addClass('deactivate-row');
			}
		
			$('td:first-child', nRow).addClass('bulk');
			$('td:nth-child(2)', nRow).addClass('profil-img');
			$('td:last-child', nRow).addClass('action-tab');
		 },
		 "fnDrawCallback": function () {
			  $('.dropdown-button, .translation-button, .dropdown-menu').dropdown({
							  inDuration: 300,
							  outDuration: 225,
							  constrainWidth: false,
							  hover: true,
							  gutter: 0,
							  belowOrigin: true,
							  alignment: 'left',
							  stopPropagation: false
							});
			
			$('.deactive_bs').off().on('click', function(){
				var bs_id = $(this).data('bs_id');
      	$('#deactivate_bs').modal('open');
				$('#deactivate_bs #deactivate_bs_id').val(bs_id);
			});
			
			$('.remove_bs').off().on('click', function(){

				var bs_id = $('#deactivate_bs_id').val();
			
      	$.ajax({

					url:base_url+'My_purchase_orders/deactive_bill_supply',

					type:"POST",

					data:{'csrf_test_name':csrf_hash,"bs_id":bs_id,},

					success:function(res){
							if(res == true)
							{
								window.location.href=base_url+'My_purchase_orders/view_purchase_orders';
								Materialize.toast('Purchase Order has been successfully deactivated', 2000,'green rounded');
							}
							else
							{
								window.location.href=base_url+'My_purchase_orders/view_purchase_orders';
								Materialize.toast('Error while processing!', 2000,'red rounded');
							}
						},

					});										
			});
			
			$('.activate_bs').off().on('click',function(){
          var bs_id = $(this).data('bs_id');
    		 $('#activate_bill_supply').modal('open');
				 $('#activate_bill_supply #activate_bs_id').val(bs_id);
			});
			
			$('.active_bs').off().on('click', function(){
				var bs_id = $('#activate_bs_id').val();
				$.ajax({
					url:base_url+'My_purchase_orders/activate_bill_supply',
					type:"POST",
					data:{'csrf_test_name':csrf_hash,"bs_id":bs_id,},
					success:function(res){
							if(res == true)
							{
								window.location.href=base_url+'My_purchase_orders/view_purchase_orders';
								Materialize.toast('Purchase Order has been successfully activated', 2000,'green rounded');
							}
							else
							{
								window.location.href=base_url+'My_purchase_orders/view_purchase_orders';
								Materialize.toast('Error while processing!', 2000,'red rounded');
							}
						},

					});		
				
			});
			 
			 $(".bs_send_email").on('click',  function(e) {
				var bs_id = $(this).data('bs_id');
				$('#send_email_to_cust').modal('open');
				$('#send_email_to_cust #send_mail_bs_id').val(bs_id);
			});

			$('.send_mail_bs').off().on('click',function(){
				var bs_id = $('#send_mail_bs_id').val();
				$.ajax({
					url:base_url+'My_purchase_orders/send_email_to_customer',
					type:"POST",
					data:{'csrf_test_name':csrf_hash,"bs_id":bs_id},
					success:function(res){
							if(res == true)
							{
								Materialize.toast('Purchase Order Details sent successfully!', 2000,'green rounded');
							}
							else
							{
								Materialize.toast('Error while processing!', 2000,'red rounded');
							}
						},
					});										
			});
					
						
		 },
	  }); 
	}
	
/* 21-May-2018 shubham.js End */


/* 23-May-2018 shubham.js start */
var table_expense_types_table = "";
  	$(document).ready(function(){
		get_expense_types();
		$('#search_by_customer').change( function() { get_expense_types() } );
		$('#search').on('keyup', function() { get_expense_types() } );
	}); 
	
	function get_expense_types()
	{
		if(table_expense_types_table != "")
		{ 
			table_expense_types_table.destroy();
			$.fn.DataTable.ext.errMode = 'none';
			table_expense_types_table.draw();
		}
    table_expense_types_table =  $('#expense_types_list').DataTable( {
        "bServerSide": true,
        "bProcessing": true,
        "bDeferRender": true,
        "aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0 , 2 , 3, 4] }],
         "aoColumns": [	
          { "bSortable": false },
          { "bSortable": false },
          { "bSortable": false },
          { "bSortable": false },
          { "bSortable": false }, 
        ],
        "fnServerParams": function ( aoData ) {
           aoData.push( { "name": "search", "value": $("#search").val() } );
        },
       "order": [],
        "bPaginate": true,
        "bLengthChange": false,
        "bFilter": true,
        "bInfo": false,
        "searching":false,
        "iDisplayLength": 10,
        "aLengthMenu": [10, 20, 40],
        "aaSorting": [[ 1, "desc" ]],
        "sAjaxSource": base_url+"My_expense_types/get_expense_types_list",
        "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {

          if($(aData[4]+' div').hasClass('deactive_record')){
            $('td', nRow).parent('tr').addClass('deactivate-row');
          }

          $('td:first-child', nRow).addClass('bulk');
          $('td:nth-child(2)', nRow).addClass('profil-img');
          $('td:last-child', nRow).addClass('action-tab');
         },
         "fnDrawCallback": function () {
          
         $('.dropdown-button, .translation-button, .dropdown-menu').dropdown({
                    inDuration: 300,
                    outDuration: 225,
                    constrainWidth: false,
                    hover: true,
                    gutter: 0,
                    belowOrigin: true,
                    alignment: 'left',
                    stopPropagation: false
                  });

          $('.deactive_expense_type').off().on('click', function(){
            var exp_id = $(this).data('expense_type_id');
            $('#deactivate_exp_type').modal('open');
            $('#deactivate_exp_type_id').val(exp_id);
            
          });

          $('.remove_exp_type').off().on('click', function(){

            var exp_id = $('#deactivate_exp_type_id').val();

            $.ajax({

              url:base_url+'My_expense_types/deactive_expense_type',

              type:"POST",

              data:{'csrf_test_name':csrf_hash,"exp_id":exp_id,},

              success:function(res){
                  if(res == true)
                  {
                    window.location.href=base_url+'My_expense_types/view_expense_types';
                    Materialize.toast('Expense Type has been successfully deactivated', 2000,'green rounded');
                  }
                  else
                  {
                    window.location.href=base_url+'My_expense_types/view_expense_types';
                    Materialize.toast('Error while processing!', 2000,'red rounded');
                  }
                },

              });										
          });

          $('.active_expense_type').off().on('click',function(){
            var exp_id = $(this).data('expense_type_id');;
             $('#active_expense_type').modal('open');
             $('#active_expense_type_id').val(exp_id);
          });

          $('.activate_exp_type_btn').off().on('click', function(){
            var exp_id = $('#active_expense_type_id').val();
            $.ajax({
              url:base_url+'My_expense_types/active_expense_type',
              type:"POST",
              data:{'csrf_test_name':csrf_hash,"exp_id":exp_id,},
              success:function(res){
                  if(res == true)
                  {
                    window.location.href=base_url+'My_expense_types/view_expense_types';
                    Materialize.toast('Expense Type has been successfully Activated', 2000,'green rounded');
                  }
                  else
                  {
                    window.location.href=base_url+'My_expense_types/view_expense_types';
                    Materialize.toast('Error while processing!', 2000,'red rounded');
                  }
                },

              });		

          });
         },
        }); 
	}


/* 23-May-2018 shubham.js End */



var table_suppliers = "";
	
	$(document).ready(function(){
		
		getSuppliers();
  		$('#state').change( function() { getSuppliers() } );
		$('#country').change( function() { getSuppliers() } );
		$('#search').on('keyup', function() { getSuppliers() } );
	});
	
	function getSuppliers() {
		
		if(table_suppliers != "")
		{ 	table_suppliers.destroy();
			$.fn.DataTable.ext.errMode = 'none';
			table_suppliers.draw();
		}
		
	table_suppliers = $('#my-supplier').DataTable( {
			"bServerSide": true,
			"bProcessing": true,
			"bDeferRender": true,
			"aoColumnDefs": [{ "bSortable": false, "aTargets": [ 0 , 1 , 3 , 4 , 6] }],
			 "aoColumns": [	
				{ "bSortable": false },
				{ "bSortable": true },
				{ "bSortable": true },
				{ "bSortable": false },
				{ "bSortable": false },
				{ "bSortable": false },
				//{ "bSortable": false },
				{"bVisible": false},
				//{"bVisible": false},
				{ "bSortable": false },
			],
			"fnServerParams": function ( aoData ) {
				aoData.push( { "name": "country", "value": $("#country").val() } );
				aoData.push( { "name": "state", "value": $("#state").val() } );
				aoData.push( { "name": "search", "value": $("#search").val() } );
			},
			"order": [],
			"bPaginate": true,
			"bLengthChange": false,
			"bFilter": true,
			"bInfo": false,
			"searching":false,
			"iDisplayLength": 10,
			"aLengthMenu": [10, 20, 40],
			"aaSorting": [[ 1, "desc" ]],
			"sAjaxSource": base_url+"My_suppliers/get_supplier_profiles",
			"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
				if($(aData[7]+' div').hasClass('deactive_record')){
					$('td', nRow).parent('tr').addClass('deactivate-row');
				}
		
				$('td:first-child', nRow).addClass('bulk');
				$('td:nth-child(2)', nRow).addClass('profil-img');
				//$('td:last-child', nRow).addClass('action-tab');
			 },
			 "fnDrawCallback": function () {
				  $('.dropdown-button, .translation-button, .dropdown-menu').dropdown({
								  inDuration: 300,
								  outDuration: 225,
								  constrainWidth: false,
								  hover: true,
								  gutter: 0,
								  belowOrigin: true,
								  alignment: 'left',
								  stopPropagation: false
								});
								
				$('.deactive_supplier').on('click',function(){
						var supplier_id = $(this).data('supplier_id');
						 $('#deactivate_supp_modal').modal('open');
						 $('#deactivate_supp_modal #deactivate_supplier_id').val(supplier_id);
					});
		
					$('.deactivate_supplier').off().on('click',function(){
						var supplier_id = $('#deactivate_supplier_id').val();
						$.ajax({
							url:base_url+'My_suppliers/deactive_supplier',
							type:"POST",
							data:{'csrf_test_name':csrf_hash,"supplier_id":supplier_id,},
							success:function(res){
									if(res == true)
									{
										window.location.href=base_url+'My_suppliers/manage_suppliers';
										Materialize.toast('Customer has been successfully deactivated', 2000,'green rounded');
									}
									else
									{
										window.location.href=base_url+'My_suppliers/manage_suppliers';
										Materialize.toast('Error while processing!', 2000,'red rounded');
									}
								},
							});										
					});
					
					$('.activate_supplier').off().on('click',function(){
						var supplier_id = $(this).data('supplier_id');
						 $('#activate_supp_modal').modal('open');
						 $('#activate_supp_modal #activate_supplier_id').val(supplier_id);
					});
					
					$('.active_supplier').off().on('click',function(){
						var activate_supplier_id = $('#activate_supplier_id').val();
						$.ajax({
							url:base_url+'My_suppliers/active_supplier_profile',
							type:"POST",
							data:{'csrf_test_name':csrf_hash,"activate_supplier_id":activate_supplier_id,},
							success:function(res){
									if(res == true)
									{
										window.location.href=base_url+'My_suppliers/manage_suppliers';
										Materialize.toast('Supplier has been successfully activated', 2000,'green rounded');
									}
									else
									{
										window.location.href=base_url+'My_suppliers/manage_suppliers';
										Materialize.toast('Error while processing!', 2000,'red rounded');
									}
								},
							});										
					});
					
					$('.send_invitation').on('click', function(){
						var supplier_id = $(this).data('supplier_id');
						
						$('#invite_supplier_modal').modal('open');
						$('#invite_supplier_modal #invite_supplier_id').val(supplier_id);
					});	
					
					$('.invite_supplier').off().on('click',function(){
						var supplier_id = $('#invite_supplier_id').val();
						$.ajax({
							url:base_url+'My_suppliers/invite_supplier',
							type:"POST",
							data:{'csrf_test_name':csrf_hash,"supplier_id":supplier_id,},
							success:function(res){
									if(res == true)
									{
										Materialize.toast('Your customer has been invited to experience the convenience of Xebra', 5000,'green rounded');
									}
									else
									{
										Materialize.toast('Error while processing!', 2000,'red rounded');
									}
									location.reload();
								},
							});										
					});
						
					$(document).off().on('click', '.send_mail_to_supp' ,function(){
						var supplier_id = $(this).data('supplier_id');
						$('#email_supplier_modal').modal('open');
						$('#email_supplier_modal #email_supplier_id').val(supplier_id);
					});
					
					$('.send_mail_supplier').off().on('click',function(){
						var supplier_id = $('#email_supplier_id').val();
						$.ajax({
							url:base_url+'My_suppliers/send_email_to_supplier',
							type:"POST",
							data:{'csrf_test_name':csrf_hash,"supplier_id":supplier_id,},
							success:function(res){
									if(res == true)
									{
										Materialize.toast('Supplier Details sent successfully!', 2000,'green rounded');
									}
									else
									{
										Materialize.toast('Error while processing!', 2000,'red rounded');
									}
								},
							});										
					});			
					
				var supplier_profiles = [];
				var supplier_profiles_values = "";

				$(".supplier_profile_bulk_action").off().on('click', function() {
					if($(this).is(':checked')){
						  if (jQuery.inArray(supplier_profiles, $(this).val())== '-1') {
							supplier_profiles.push($(this).val());
						  } 
					}
					else
					{
						var remove_id = $(this).val();	
						supplier_profiles = jQuery.grep(supplier_profiles, function(value) {
						  return value != remove_id;
						});
					}
						supplier_profiles_values = supplier_profiles.join(",");
						$('#deactive_multiple_suppliers_profiles').attr('data-multi_profiles',supplier_profiles_values);
					});
			 },
		  }); 
	} 
  