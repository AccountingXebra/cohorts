//^[0-9]{2}[A-Z]{5}[0-9]{4}[A-Z]{1}[1-9A-Z]{1}Z[0-9A-Z]{1}$
jQuery.validator.addMethod("gstreg", function(postal, element) {
    return this.optional(element) ||
    postal.match(/^([0][1-9]|[1-2][0-9]|[3][0-7])([a-zA-Z]{5}[0-9]{4}[a-zA-Z]{1}[1-9a-zA-Z]{1}[zZ]{1}[0-9a-zA-Z]{1})+$/);
}, "Enter GSTIN in a valid format");
jQuery.validator.addMethod("mobileno", function(mobileno, element) {
    return this.optional(element) ||
    mobileno.match(/^[6-9]\d{9}$/);
}, "Please enter a valid Mobile Number.");
 jQuery.validator.addMethod("pincode", function(postal, element) {
    return this.optional(element) ||
    postal.match(/^[1-9][0-9]{5}$/);
}, "Please enter valid Pin Code.");
 jQuery.validator.addMethod("ifscno", function(postal, element) {
    return this.optional(element) ||
    postal.match(/^[A-Za-z]{4}\d{7}$/);
}, "Please enter valid IFSC Code.");


			$("#otp_check").validate({

				rules:{

					otp:{

						required:true,

					},
				},

				messages:{

					otp:{

						required:"OTP is required",

					},
				},

			});

			$('.deactive_profile_2').on('click',function(){

				var profile = $(this).data('profileid');

				 $('#deactive_profile_2').modal('open');

				 $('#deactive_profile_2 #personal_profile_id_2').val(profile);

			});

			$('.deactive_user_2').on('click',function(){

				var profile_id = $('#personal_profile_id_2').val();
				$.ajax({

					url:base_url+'Personal_profile/deactive_profile_2',

					type:"POST",

					data:{'csrf_test_name':csrf_hash,"profile_id":profile_id,},

					success:function(res){
							//	console.log(res);
							window.location.href=base_url+'Personal_profile/manage_profile';

						},

					});
			});

			$('.active_profile_2').on('click',function(){

				var profile = $(this).data('profileid');

				 $('#active_profile_2').modal('open');



				 $('#active_profile_2 #activate_personal_profile_id_2').val(profile);

			});

			$('.active_user_2').on('click',function(){

				var profile_id = $('#activate_personal_profile_id_2').val();
				$.ajax({

					url:base_url+'Personal_profile/activate_profile_2',

					type:"POST",

					data:{'csrf_test_name':csrf_hash,"profile_id":profile_id,},

					success:function(res){

							window.location.href=base_url+'Personal_profile/manage_profile';

						},

					});

			});
			/*----Start Change Password ----*/

			$("#change_password_frm").submit(function(e){
				e.preventDefault();
			}).validate({

				rules:{

					old_password:{
						required:true,
						minlength:8,
					},

					new_password:{
						required:true,
						checklowercase: true,
						checkuppercase: true,
						checknumber: true,
						checkspecialchar: true,
						minlength:8,
					},
					confirm_password:{
						required:true,
						equalTo: '#new_password',
					},
				},

				messages:{

					old_password:{
						required:"Old Password is required",
						minlength:"Please Enter Minimum 8 characters long password",
					},

					new_password:{
						required:"New Password is required",
						checklowercase:"At list 1 Lower case",
						checkuppercase:"At list 1 Upper case",
						checknumber:"At list 1 Numberr",
						checkspecialchar:"At list 1 Special Character",
						minlength:"Please Enter Minimum 8 characters long password",
					},

					confirm_password:{
						required:"Confirm your new password",
						equalTo:"Your password doesn't match",
					},
				},
				submitHandler:function(form){
						var frm=$(form).serialize();
						if(csrf_hash===""){
							csrf_hash=csrf_hash;
						}
								 //$(form).ajaxSubmit();
								$.ajax({
								url:base_url+'index/change_password',
								type:"POST",
								data:{'csrf_test_name':csrf_hash,"frm":frm,},
								success:function(res){
									//console.log(res);
										str1=res.split('/');
									if(str1[1]){
										csrf_hash=str1[1];
									}	
										if(str1[0] == 'TRUE')
										{
											 $('#change_pass_success').modal('open');

											 $('#password-modal').modal('close');
											 // OTP Validation before Password change
											 // $('#password-otp-modal').modal('open');
											 // $('#password-otp-modal #mobile_no_pwd').prop("checked",false);
											 // $('#password-otp-modal #email_address_pwd').prop("checked",false);
											/* $('#pass-succ').modal({
												dismissible: false, // Modal can be dismissed by clicking outside of the modal
											  });

											 $('#pass-succ').modal('open');*/
											 window.setTimeout(function(){

											        // Move to a new location or you can do something else
											        window.location.href = base_url+'logout';

											    }, 5000);
										}
										else
										{
											$('#change_pass_error').modal('open');
										}
										$("#change_password_frm #old_password").val('');
										$("#change_password_frm #new_password").val('');
										$("#change_password_frm #confirm_password").val('');
									},
					});
				},

			});


			$.validator.addMethod("checklowercase",
			function(value, element) {
			    return /^(?=(.*[a-z]){1,}).{1,}$/.test(value);
			});
			$.validator.addMethod("checkuppercase",
			function(value, element) {
			    return /^(?=.*?[A-Z]).{1,}$/.test(value);
			});
			$.validator.addMethod("checknumber",
			function(value, element) {
			    return /^(?=(.*[\d]){1,}).{1,}$/.test(value);
			});
			$.validator.addMethod("checkspecialchar",
			function(value, element) {
			    return /^(?=(.*[\W]){1,})(?!.*\s).{1,}$/.test(value);
			});

			$('.sent_changepwd_otp').on('click',function(){
				var mobile_no=0;
				var email_add=0;
				var otp_sentense="An OTP has successfully sent to registered Email Address, Enter it below to Change Password";
				if($('#mobile_no_pwd').prop("checked") == false && $('#email_address_pwd').prop("checked") == false){
					Materialize.toast('Please select at least one!!', 2000,'red rounded');

				}else{
				  if($('#mobile_no_pwd').is(":checked")){
					mobile_no=1;
					otp_sentense="An OTP has successfully sent to registered Mobile Number, Enter it below to Change Password";
				  }
				   if($('#email_address_pwd').is(":checked")){
					email_add=1;
					otp_sentense="An OTP has successfully sent to registered Email Address, Enter it below to Change Password";
				  }
				  if($('#mobile_no_pwd').prop("checked") == true && $('#email_address_pwd').prop("checked") == true){
				  	otp_sentense="An OTP has successfully sent to registered Mobile Number and Email Address, Enter it below to Change Password";
				  }

				 $.ajax({
						type:"POST",
						url: base_url + 'index/sent_change_pwdotp',
						data:{'csrf_test_name':csrf_hash,'mobile_no':mobile_no,'email_add':email_add},
						success: function (res) {
							var data = JSON.parse(res);
							if (data == true) {
								$('#password-otp-modal').modal('close');
								 $('#password_enter_otp_modal').modal('open');

								$('#password_enter_otp_modal').modal('open');

								$('#password_enter_otp_modal #otp_pwd_sentense').text(otp_sentense);

							}
						}

					});
				}

			});
				$("#pwd_otp_mail").submit(function(e){
					e.preventDefault();
						}).validate({
							rules:{

								'acc_pwdotp':{
									required:true,
									number:true,
									minlength:6,
									maxlength:6,
								},

							},
							messages:{

								'acc_pwdotp':{
									required:"OTP is required",
									minlength:"Please Enter 6 digit OTP code",
									maxlength:"Please Enter only 6 digit OTP code",

								},

							},
							submitHandler:function(form){

									var frm=$(form).serialize();
											$.ajax({
											url:base_url+'index/check_otp_for_password',
											type:"POST",
											data:{'csrf_test_name':csrf_hash,"frm":frm,},
											success:function(res){
												//console.log(res);
												var data = JSON.parse(res);

												if(data!= false)
												{
													$('#change-password-notification').modal('open');
													//setTimeout(function(){ location.reload(); }, 3000);
													//Materialize.toast('Tax Information have been set', 2000,'green rounded');
												}
												else
												{
													//Materialize.toast('Error while processing. Please try again', 2000,'red rounded');
													$('#otp_error').modal('open');
												}
												$("#password_enter_otp_modal").find("input[type=text]").val("");
												$("#password_enter_otp_modal").modal('close');


										},

								});
							},
						});

			/*----End Change Password ----*/
			/*$("#otp_password_frm").submit(function(e){
				e.preventDefault();
			}).validate({

				rules:{

					password_otp:{
						required:true,
						minlength:6,
						maxlength:6,
					},
				},

				messages:{

					password_otp:{
						required:"OTP is required",
						minlength:"Please Enter 6 digit OTP code",
						maxlength:"Please Enter only 6 digit OTP code",
					},
				},
				submitHandler:function(form){
						var frm=$(form).serialize();
								 //$(form).ajaxSubmit();
								$.ajax({
								url:base_url+'Dashboard/check_otp_for_password',
								type:"POST",
								data:{"frm":frm,},
								success:function(res){

										if(res == true)
										{
											location.reload();
										}
										else
										{
											$("#otp_password_frm #password_otp").val('');
											$('#otp_error').modal('open');

										}
									},
					});
				},
			});
*/
			$('.show_password_chkbox').on('change',function(){
				$('.show_password').attr('type','Password');
				var isChecked = $(this).prop('checked');
			  	if (isChecked) {
					$('.show_password').attr('type','text');
			  	} else {
					$('.show_password').attr('type','Password');
			  	}
			});
			$('.show_password_chkbox_2').on('change',function(){
				$('.show_password_2').attr('type','Password');
				var isChecked = $(this).prop('checked');
			  	if (isChecked) {
					$('.show_password_2').attr('type','text');
			  	} else {
					$('.show_password_2').attr('type','Password');
			  	}
			});


			$('.add_gst').on('click',function(){
				$('#add_gst_frm').modal('open');
			});

			$("#add_gst_from").submit(function(e){
				$(this).find(".statecode_gst,.js-typeahead,.billing_add").attr('required',true);
				$(this).find(".statecode_gst").attr('gstreg',true);
				//$('#add_gst_from input').not('[type="submit"],[type="button"],[type="hidden"]').attr('required','required');
				e.preventDefault();
			}).validate({

				rules:{
					/*'gstin[]':{
						required:true,
						gstregex:true,
					},
					'location[]':{
						required:true,
					},*/
				},

				messages:{

					/*'gstin[]':{
						required:"GST Number is required",
						gstregex:"GSTInNo must contain only letters and numbers.",
					},
					'location[]':{
						required:"Place of supply is required",
					},*/
				},
				submitHandler:function(form){
						var frm=$(form).serialize();
								 //$(form).ajaxSubmit();
								$.ajax({
								url:base_url+'profile/add_gstin_info',
								type:"POST",
								data:{'csrf_test_name':csrf_hash,"frm":frm,},
								success:function(res){
									if(res!= false)
									{
										var data=JSON.parse(res);
										//console.log(data);
										var html_1 ='';
										var final_html ='';
										if(data != '')
										{
											for(var i=0;i<data.length;i++){
												if(data[i].status=="Inactive"){
													   var deac="deactive_record";
												   }else{
													    var deac="";
												   }
												   console.log(deac);
											if(i == 0)
											   {
												html_1='<div class="gst-boxs add-gst '+deac+'"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">GST NO.</span><p>'+data[i].gst_no+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">PLACE OF SUPPLY</span><p>'+data[i].place+'</p></div><div class="col l1 s1 m1 gstaction"><a class="remove_cmp_gst_model" data-location="'+data[i].place+'" data-gst_no="'+data[i].gst_no+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a></div></div></div>';
											   }
											   else
											   {
												 html_1='<div class="gstmore"><div class="gst-boxs add-gst '+deac+'"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">GST NO.</span><p>'+data[i].gst_no+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">PLACE OF SUPPLY</span><p>'+data[i].place+'</p></div><div class="col l1 s1 m1 gstaction"><a class="remove_cmp_gst_model" data-location="'+data[i].place+'" data-gst_no="'+data[i].gst_no+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a></div></div></div></div>';

											   }
												 final_html = final_html + html_1;
												 $("#company_gst_array_list").html(final_html);
												 $(".tot_gst").html(data.length);
												 $('#tot_gst_nu').val(data.length);
											}
											Materialize.toast('GSTIN Details have been set.', 2000,'green rounded');
										}
										else
										{
											Materialize.toast('GSTIN Details have been set.', 2000,'green rounded');
											//html_1='<div class="gst-boxs add-gst">Add GSTIN Data</div>';
											//$("#company_gst_array_list").html(html_1);
										}
									}
									else
									{
										Materialize.toast('Error while processing. Please try again', 2000,'red rounded');
									}
									$("#add_gst_from").find("input[type=text], textarea,select").val("");
									$("#add_cmp_gst_frm").modal('close');

								$(document).on('click','.remove_cmp_gst_model',function(){

									var gst_no = $(this).data('gst_no');
									var location = $(this).data('location');

									$('#remove_company_gst_data').modal('open');
									$('#remove_company_gst_data #gst_no').val(gst_no);
									$('#remove_company_gst_data #location').val(location);

								});

									$('.remove_company_gst_data').off().on('click',function(){

										var gst_no = $('#gst_no').val();
										var location = $('#location').val();

										$.ajax({

											url:base_url+'profile/remove_company_gst_info',

											type:"post",

											data:{'csrf_test_name':csrf_hash,"gst_no":gst_no,"location":location},

											success:function(res){
													if(res!= false)
													{

														var data=JSON.parse(res);

														var html_1 ='';
														var final_html ='';
														if(data != '')
														{


															for(var i=0;i<data.length;i++){

															if(i == 0)
															   {
																html_1='<div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
															   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">GST NO.</span><p>'+data[i].gst_no+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">PLACE OF SUPPLY</span><p>'+data[i].place+'</p></div><div class="col l1 s1 m1 gstaction"><a class="remove_cmp_gst_model" data-location="'+data[i].place+'" data-gst_no="'+data[i].gst_no+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a></div></div></div>';
															   }
															   else
															   {
																 html_1='<div class="gstmore"><div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
															   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">GST NO.</span><p>'+data[i].gst_no+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">PLACE OF SUPPLY</span><p>'+data[i].place+'</p></div><div class="col l1 s1 m1 gstaction"><a class="remove_cmp_gst_model" data-location="'+data[i].place+'" data-gst_no="'+data[i].gst_no+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a></div></div></div></div>';

															   }

																 final_html = final_html + html_1;
																 $("#company_gst_array_list").html(final_html);
																 $(".tot_gst").html(data.length);
															}
															Materialize.toast('GSTIN Details deactivated.', 2000,'green rounded');
														}
														else
														{
															Materialize.toast('GSTIN Details deactivated.', 2000,'green rounded');
															//html_1='<div class="gst-boxs add-gst">Add GSTIN Data</div>';
															//$("#company_gst_array_list").html(html_1);
															$(".tot_gst").html(0);
														}
													}
													else
													{
														//html_1='<div class="gst-boxs add-gst">Add GSTIN Data</div>';
														//$("#company_gst_array_list").html(html_1);
														$(".tot_gst").html(0);
													}
												},

											});
									});

									},

					});
				},

			});


			$('.show_social').hide();
			$('.edit_social_media').on('click',function(){
				$('.show_social').toggle();
			});


	$(document).ready(function () {
			$(document).on('click','.edit_gst_info',function(){

				var id = $(this).data('id');;
				var companyid = $(this).data('companyid');
				var gst_no = $(this).data('gst_no');
				var place = $(this).data('gst_place');

				var address = $(this).data('address');
				var country = $(this).data('country');
				var state_code = $(this).data('state_code');
				var city = $(this).data('city');
				var zipcode = $(this).data('zipcode');
				var email = $(this).data('email');
				var contact = $(this).data('contact');
				var status = $(this).data('status');

				$('#edit_company_gstmodal').modal('open');
				$('#edit_company_gstmodal #gst_no_edit,#edit_company_gstmodal #gst_edit_location #billing_address2 #country2 #state2 #city2').parents('.input-field').children().addClass('active');
				$('#edit_company_gstmodal #cmp_gst_id').val(id);
				$('#edit_company_gstmodal #cmp_id_edit_gst').val(companyid);
				$('#edit_company_gstmodal #gst_no_edit').val(gst_no);
				$('#edit_company_gstmodal #gst_edit_location').val(place);

				$('#edit_company_gstmodal #email2').val(email);
				$('#edit_company_gstmodal #contact2').val(contact);
				$('#edit_company_gstmodal #billing_address2').val(address);
				//$('#edit_company_gstmodal #country2').val(country);
				//$('#country2 option[value="'+country+'"]').attr('selected',true).change();
				//$('#country2').material_select();
				//$('#edit_company_gstmodal #state2').val(state);
				//$('#edit_gstmodal #state2').val(state);
				//$('#state2 option[value="'+state+'"]').prop('selected',true).change();
				//$('#state2').material_select();
				//$('#edit_company_gstmodal #city2').val(city);
				//$('#edit_gstmodal #city2').val(city);
				//$('#city2 option[value="'+city+'"]').attr('selected',true).change();
				//$('#city2').material_select();
				$('#edit_company_gstmodal #pincode2').val(zipcode);
				$('#edit_company_gstmodal #status2').val(status);
				$('#status option[value="'+status+'"]').attr('selected',true);
				$('#status').material_select();

                setTimeout(function(){ 
           
  
 
          $("#edit_company_gstmodal #country2").val(country).prop("selected", "selected").change();
          $('#edit_company_gstmodal #country2').material_select();
        setTimeout(function(){ 
          $("#edit_company_gstmodal #state2").val(state_code).prop("selected", "selected").change();
          $('#edit_company_gstmodal #state2').material_select();
        setTimeout(function(){ 
          $('#edit_company_gstmodal #city2 option[value='+city+']').attr("selected", 'selected').change();
          //("select#city_local").val(city).prop("selected", "selected").change();
          $('#edit_company_gstmodal #city2').material_select();


        }, 200); 
        }, 150);
        }, 100);




			});

			$("#edit_gst_from").submit(function(e){
					e.preventDefault();
			}).validate({

				rules:{
					gst_no_edit:{
						required:true,
						gstreg:true,
					},
					gst_edit_location:{
						required:true,
					},
				},
				messages:{
					gst_no_edit:{
						required:"GST Number is required",

					},
					gst_edit_location:{
						required:"Place of supply is required",
					},
				},
				submitHandler:function(form){
						var frm=$(form).serialize();
								$.ajax({
								url:base_url+'profile/edit_gstin_info',
								type:"POST",
								data:{'csrf_test_name':csrf_hash,"frm":frm,},
								success:function(res){
										if(res!= false)
										{
											var data=JSON.parse(res);
											var html_1 ='';
											var final_html ='';
											if(data != '')
											{
												for(var i=0;i<data.length;i++){
																if(data[i].status=="Inactive"){
													   var deac="deactive_record";
												   }else{
													    var deac="";
												   }
												if(i == 0)
												   {
													html_1='<div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
												   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum '+deac+'"><span class="gstlabel">GST NO.</span><p>'+data[i].gst_no+'</p></div><div class="col l6 s6 m6 gstplace '+deac+'"><span class="gstlabel">PLACE OF SUPPLY</span><p>'+data[i].place+'</p></div><div class="col l1 s1 m1 gstaction"><a class="edit_remove_cmpgst '+deac+'" data-id="'+data[i].gst_id+'"  data-companyid="'+data[i].bus_id+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a><a class="edit_gst_info" data-id="'+data[i].gst_id+'"  data-companyid="'+data[i].bus_id+'" data-gst_no="'+data[i].gst_no+'" data-gst_place="'+data[i].place+'" data-address="'+data[i].address+'" data-country="'+data[i].country+'" data-state_code="'+data[i].state_code+'" data-city="'+data[i].city+'" data-zipcode="'+data[i].zipcode+'" data-email="'+data[i].email+'" data-contact="'+data[i].contact+'" data-status="'+data[i].status+'"><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div>';
												   }
												   else
												   {
													 html_1='<div class="gstmore active"><div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
												   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum '+deac+'"><span class="gstlabel">GST NO.</span><p>'+data[i].gst_no+'</p></div><div class="col l6 s6 m6 gstplace '+deac+'"><span class="gstlabel">PLACE OF SUPPLY</span><p>'+data[i].place+'</p></div><div class="col l1 s1 m1 gstaction"><a class="edit_remove_cmpgst '+deac+'" data-id="'+data[i].gst_id+'" data-companyid="'+data[i].bus_id+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a><a class="edit_gst_info" data-id="'+data[i].gst_id+'"  data-companyid="'+data[i].bus_id+'" data-gst_no="'+data[i].gst_no+'" data-gst_place="'+data[i].place+'" data-address="'+data[i].address+'" data-country="'+data[i].country+'" data-state_code="'+data[i].state_code+'" data-city="'+data[i].city+'" data-zipcode="'+data[i].zipcode+'" data-email="'+data[i].email+'" data-contact="'+data[i].contact+'" data-status="'+data[i].status+'"><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div></div>';

												   }
													 final_html = final_html + html_1;
													 $("#company_gst_array_list_edit").html(final_html);
													 $(".tot_gst").html(data.length);
													 $('#showgstboxs i').html('keyboard_arrow_up');
												}
												Materialize.toast('GSTIN Details has been updated.', 2000,'green rounded');
											}
											else
											{
												Materialize.toast('GSTIN Details has been updated.', 2000,'green rounded');
												//html_1='<div class="gst-boxs add-gst">Add GSTIN Data</div>';
												//$("#company_gst_array_list_edit").html(html_1);
											}
										}
										else
										{
											Materialize.toast('Error while processing. Please try again', 2000,'red rounded');
										}
										$("#edit_gst_from").find("input[type=text], textarea").val("");
										$("#edit_company_gstmodal").modal('close');
									},
					});
				},
				});
			});

	$(document).ready( function() {
			$('.add_gst_2').on('click',function(){
				$('#add_gst_frm_2').modal('open');
				var companyid = $(this).data('company_id');

				$('#add_gst_frm_2 #company_id_gst_2').val(companyid);

			});
			$("#add_gst_from_2").submit(function(e){
				$(this).find(".statecode_gst,.js-typeahead").attr('required',true);
				$(this).find(".statecode_gst").attr('gstreg',true);
				e.preventDefault();
			}).validate({
				rules:{
				/*	'gstin[]':{
						required:true,
						gstregex:true,
					},
					'location[]':{
						required:true,
					},*/
				},
				messages:{

					/*'gstin[]':{
						required:"GST Number is required",
						gstregex:"GSTInNo must contain only letters and numbers.",
					},
					'location[]':{
						required:"Place of supply is required",
					},*/
				},
				submitHandler:function(form){
						var frm=$(form).serialize();
								$.ajax({
								url:base_url+'profile/add_another_gstin_info',
								type:"post",
								data:{'csrf_test_name':csrf_hash,"frm":frm,},
								success:function(res){
										if(res!= false)
										{
											var data=JSON.parse(res);
											var html_1 ='';
											var final_html ='';
											if(data != '')
											{
												for(var i=0;i<data.length;i++){
													if(data[i].status=="Inactive"){
													   var deac="deactive_record";
												   }else{
													    var deac="";
												   }
												if(i == 0)
												   {
													html_1='<div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
												   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum '+deac+'"><span class="gstlabel">GST NO.</span><p>'+data[i].gst_no+'</p></div><div class="col l6 s6 m6 gstplace '+deac+'"><span class="gstlabel">PLACE OF SUPPLY</span><p>'+data[i].place+'</p></div><div class="col l1 s1 m1 gstaction"><a class="edit_remove_cmpgst '+deac+'" data-id="'+data[i].gst_id+'"  data-companyid="'+data[i].bus_id+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a><a class="edit_gst_info" data-id="'+data[i].gst_id+'"  data-companyid="'+data[i].bus_id+'" data-gst_no="'+data[i].gst_no+'" data-gst_place="'+data[i].place+'" data-address="'+data[i].address+'" data-country="'+data[i].country+'" data-state_code="'+data[i].state_code+'" data-city="'+data[i].city+'" data-zipcode="'+data[i].zipcode+'" data-email="'+data[i].email+'" data-contact="'+data[i].contact+'" data-status="'+data[i].status+'"><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div>';
												   }
												   else
												   {
													 html_1='<div class="gstmore"><div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
												   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum '+deac+'"><span class="gstlabel">GST NO.</span><p>'+data[i].gst_no+'</p></div><div class="col l6 s6 m6 gstplace '+deac+'"><span class="gstlabel">PLACE OF SUPPLY</span><p>'+data[i].place+'</p></div><div class="col l1 s1 m1 gstaction"><a class="edit_remove_cmpgst '+deac+'" data-id="'+data[i].gst_id+'"  data-companyid="'+data[i].bus_id+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a><a class="edit_gst_info" data-id="'+data[i].gst_id+'"  data-companyid="'+data[i].bus_id+'" data-gst_no="'+data[i].gst_no+'" data-gst_place="'+data[i].place+'" data-address="'+data[i].address+'" data-country="'+data[i].country+'" data-state_code="'+data[i].state_code+'" data-city="'+data[i].city+'" data-zipcode="'+data[i].zipcode+'" data-email="'+data[i].email+'" data-contact="'+data[i].contact+'" data-status="'+data[i].status+'"><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div></div>';

												   }
													 final_html = final_html + html_1;
													 $("#company_gst_array_list_edit").html(final_html);
													 $(".tot_gst").html(data.length);
												}
												Materialize.toast('GSTIN Details have been set.', 2000,'green rounded');
											}
											else
											{
												Materialize.toast('GSTIN Details have been set.', 2000,'green rounded');
												//html_1='<div class="gst-boxs add-gst">Add GSTIN Data</div>';
												//$("#company_gst_array_list_edit").html(html_1);
											}
										}
										else
										{
											Materialize.toast('Error while processing. Please try again', 2000,'red rounded');
										}
										$("#add_gst_from_2").find("input[type=text], textarea").val("");
										$("#add_gst_frm_2").modal('close');
							},
					});
				},
			});
		});
			//My_Customers


			$("#add_customer_gstin").submit(function(e){
				$(this).find(".statecode_gst,.js-typeahead,.billing_add").attr('required',true);
				$(this).find(".statecode_gst").attr('gstreg',true);

				e.preventDefault();
			}).validate({
				rules:{

					/*'gstin[]':{
						required:true,
						gstreg:true,
						gstreg:true,
					},
					'location[]':{
						required:true,
					},*/
				},
				messages:{

					gstin:{
						required:"GST Number is required",

					},
					location:{
						required:"Place of supply is required",
					},

					billing_address:{
						required:"Billing address is required",
					},
				},
				submitHandler:function(form){
						var frm=$(form).serialize();
								$.ajax({
								url:base_url+'sales/add_gst_info',
								type:"POST",
								data:{'csrf_test_name':csrf_hash,"frm":frm},
								success:function(res){
									if(res!= false)
									{
										var data=JSON.parse(res);
										var html_1 ='';
										var final_html ='';
										if(data != '')
										{
											for(var i=0;i<data.length;i++){
												if(data[i].status=="Inactive"){
													   var deac="deactive_record";
												   }else{
													    var deac="";
												   }
												   console.log(deac);
											if(i == 0)
											   {
												html_1='<div class="gst-boxs add-gst '+deac+'"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">GST NO.</span><p>'+data[i].gst_no+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">PLACE OF SUPPLY</span><p>'+data[i].place+'</p></div><div class="col l1 s1 m1 gstaction"><a href="javascript:void(0);" class="remove_gst_model" data-location="'+data[i].place+'" data-gst_no="'+data[i].gst_no+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a></div></div></div>';
											   }
											   else
											   {
												 html_1='<div class="gstmore"><div class="gst-boxs add-gst '+deac+'"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">GST NO.</span><p>'+data[i].gst_no+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">PLACE OF SUPPLY</span><p>'+data[i].place+'</p></div><div class="col l1 s1 m1 gstaction"><a href="javascript:void(0);" class="remove_gst_model" data-location="'+data[i].place+'" data-gst_no="'+data[i].gst_no+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a></div></div></div></div>';

											   }
												 final_html = final_html + html_1;
												 $("#gst_array_list").html(final_html);
												 $(".tot_gst").html(data.length);
												 $("#tot_gst_nu").html(data.length);
												 $('#showgstboxs i').html('keyboard_arrow_down');
											}
											Materialize.toast('GSTIN Details have been set!', 2000,'green rounded');
										}
										else
										{
											Materialize.toast('GSTIN Details has been added.', 2000,'green rounded');
											//html_1='<div class="gst-boxs add-gst">Add GSTIN Data</div>';
											//$("#gst_array_list").html(html_1);
										}
									}
									else
									{
										Materialize.toast('Error while processing. Please try again', 2000,'red rounded');
									}
									$("#add_customer_gstin").find("input[type=text], textarea,select").val("");
									$("#gstmodal").modal('close');


							$(document).ready( function() {
								$(document).on('click','.remove_gst_model',function(){

									var gst_no = $(this).data('gst_no');
									var location = $(this).data('location');

									$('#remove_gst_data').modal('open');
									$('#remove_gst_data #gst_no').val(gst_no);
									$('#remove_gst_data #location').val(location);

								});

									$('.remove_gst_data').off().on('click',function(){

										var gst_no = $('#gst_no').val();
										var location = $('#location').val();

										$.ajax({

											url:base_url+'sales/remove_gst_info',

											type:"post",

											data:{'csrf_test_name':csrf_hash,"gst_no":gst_no,"location":location},

											success:function(res){
													if(res!= false)
													{
														var data=JSON.parse(res);
														var html_1 ='';
														var final_html ='';

														if(data != '')
														{
															for(var i=0;i<data.length;i++){
																if(data[i].status=="Inactive"){
													   var deac="deactive_record";
												   }else{
													    var deac="";
												   }
												   console.log(deac);
															if(i == 0)
															   {
																html_1='<div class="gst-boxs add-gst '+deac+'"><div class="col l1 s1 m1 sno"><div class="row">'+
															   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">GST NO.</span><p>'+data[i].gst_no+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">PLACE OF SUPPLY</span><p>'+data[i].place+'</p></div><div class="col l1 s1 m1 gstaction"><a href="javascript:void(0);" class="remove_gst_model" data-location="'+data[i].place+'" data-gst_no="'+data[i].gst_no+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a></div></div></div>';
															   }
															   else
															   {
																 html_1='<div class="gstmore"><div class="gst-boxs add-gst '+deac+'"><div class="col l1 s1 m1 sno"><div class="row">'+
															   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">GST NO.</span><p>'+data[i].gst_no+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">PLACE OF SUPPLY</span><p>'+data[i].place+'</p></div><div class="col l1 s1 m1 gstaction"><a href="javascript:void(0);" class="remove_gst_model" data-location="'+data[i].place+'" data-gst_no="'+data[i].gst_no+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a></div></div></div></div>';

															   }
																 final_html = final_html + html_1;
																 $("#gst_array_list").html(final_html);
																 $(".tot_gst").html(data.length);
																 $('#showgstboxs i').html('keyboard_arrow_down');
															}
															Materialize.toast('GSTIN Details deactivated.', 2000,'green rounded');
														}
														else
														{
															Materialize.toast('GSTIN Details deactivated', 2000,'green rounded');
															//html_1='<div class="gst-boxs add-gst">Add GSTIN Data</div>';
															//$("#gst_array_list").html(html_1);
															$(".tot_gst").html(0);
														}
													}
													else
														{
															Materialize.toast('GSTIN Details deactivated', 2000,'green rounded');
															//html_1='<div class="gst-boxs add-gst">Add GSTIN Data</div>';
															//$("#gst_array_list").html(html_1);
															$(".tot_gst").html(0);
														}
												},

											});
									});
									});
							},

					});
				},
			});

			$("#add_contactperson").submit(function(e){
				e.preventDefault();
			}).validate({
				rules:{

					name:{
						required:true,
					},
					mobile:{
						//mobileno:true,
						digits:true,
						required:true,
					},
					email:{
						required:true,
						email:true
					},

					// password:{

					// 	required:true,
					// 	checklowercase: true,
					// 	checkuppercase: true,
					// 	checknumber: true,
					// 	checkspecialchar: true,
					// 	minlength:8,
					// 	maxlength:12
					// },
					// cpassword:{
					// 	equalTo:'#password',
					// 	minlength:8,
					// 	maxlength:12
					// },
				},
				messages:{

					name:{
						required:"Name is required",
					},
					email:{
						required:"Email ID is required",
						email:'Enter valid Email ID',
					},
					mobile:{
						required:'Mobile Number is required',
					},
					// password:{
					// 	required:"Password is required",
					// 	checklowercase:"At list 1 Lower case",
					// 	checkuppercase:"At list 1 Upper case",
					// 	checknumber:"At list 1 Numberr",
					// 	checkspecialchar:"At list 1 Special Character",
					// 	minlength:"Password must contain at least 8 characters",
					// 	maxlength:"Maximum 12 character long password allowed",
					// },
					// cpassword:{
					// 	equalTo:"Password doesn't match",
					// },
				},
				submitHandler:function(form){
						var frm=$(form).serialize();
								$.ajax({
								url:base_url+'sales/add_contact_person_info',
								type:"POST",
								data:{'csrf_test_name':csrf_hash,"frm":frm,},
								success:function(res){
									if(res!= false)
									{
										var data=JSON.parse(res);
										var html_person ='';
										var final_html_1 ='';
										if(data != '')
										{
											for(var i=0;i<data.length;i++){
											var primary_user="";
											var tooltip="";
											var tooltip_cls='';
											if(data[i].cp_is_admin=="1"){
												primary_user="checked ";
												tooltip='data-position="left" data-tooltip="Primary User"';
												tooltip_cls='tooltipped';
											}
												if(i == 0)
												{
												html_person='<div class="row"><div class="contact-person-box"><div class="col l1 s1 m1"><input type="radio" id="contact-person"  data-email="'+data[i].cp_email+'" class="radio_primary" name="default" '+primary_user+'><label class="fill-green-radio '+tooltip_cls+'" for="contact-person" '+tooltip+'></label></div><div class="col l10 s10 m10 content-area"><div class="col l3 s3 m3"><div class="gradient-wrapper"><div class="maincircle">100%</div></div></div><div class="col l9 s9 m9"><p class="name"><strong>'+data[i].cp_name+'</strong></p><p class="email">'+data[i].cp_email+'</p></div></div><div class="col l1 s1 m1"><a href="javascript:void(0);" class="remove_contact_person" data-name="'+data[i].cp_name+'" data-email="'+data[i].cp_email+'" data-cust_code="'+data[i].cp_cust_code+'"><i class="material-icons">close</i></a></div></div></div>';

											   }
											   else if(i == 1)
											   {
												  html_person='<div class="row"><div class="contact-person-box"><div class="col l1 s1 m1"><input type="radio" id="contact-person'+(i+1)+'" data-email="'+data[i].cp_email+'" class="radio_primary" name="default" '+primary_user+'><label class="fill-green-radio '+tooltip_cls+'" for="contact-person'+(i+1)+'"  '+tooltip+'></label></div><div class="col l10 s10 m10 content-area"><div class="col l3 s3 m3"><div class="gradient-wrapper"><div class="maincircle">100%</div></div></div><div class="col l9 s9 m9"><p class="name"><strong>'+data[i].cp_name+'</strong></p><p class="email">'+data[i].cp_email+'</p></div></div><div class="col l1 s1 m1"><a href="javascript:void(0);" class="remove_contact_person" data-name="'+data[i].cp_name+'" data-email="'+data[i].cp_email+'" data-cust_code="'+data[i].cp_cust_code+'"><i class="material-icons">close</i></a></div></div></div>';

											   }
											   else
											   {
												  html_person='<div class="contactmore"><div class="row"><div class="contact-person-box"><div class="col l1 s1 m1"><input type="radio" class="radio_primary" id="contact-person'+(i+1)+'" data-email="'+data[i].cp_email+'" name="default" '+primary_user+'><label class="fill-green-radio '+tooltip_cls+'" for="contact-person'+(i+1)+'" '+tooltip+'></label></div><div class="col l10 s10 m10 content-area"><div class="col l3 s3 m3"><div class="gradient-wrapper"><div class="maincircle">100%</div></div></div><div class="col l9 s9 m9"><p class="name"><strong>'+data[i].cp_name+'</strong></p><p class="email">'+data[i].cp_email+'</p></div></div><div class="col l1 s1 m1"><a href="javascript:void(0);" class="remove_contact_person" data-name="'+data[i].cp_name+'" data-email="'+data[i].cp_email+'" data-cust_code="'+data[i].cp_cust_code+'"><i class="material-icons">close</i></a></div></div></div></div>';

											   }
												 final_html_1 = final_html_1 + html_person;
												 $("#person_list").html(final_html_1);
												 $(".tot_person").html(data.length);
												 $('#con_morebtn').show();
												 $('.tooltipped').tooltip();
											}
											Materialize.toast('Contact Details have been set.', 2000,'green rounded');

										}
										else
										{
											html_person='<div class="gst-boxs add-gst">Add Contact Person Data</div>';
											$("#person_list").html(html_person);
											$(".tot_person").html(0);
										}
									}
									else
									{
										Materialize.toast('Error while processing. Please try again', 2000,'red rounded');
									}

									$("#add_contactperson").find("input[type=text],input[type=password], textarea").val("");
									$("#contactmodal").modal('close');


						$(document).ready(function() {

								$(document).off().on('click','.remove_contact_person',function(){
								//$('.remove_contact_person').on('click',function(){

									var name = $(this).data('name');
									var email = $(this).data('email');
									$('#remove_contact_person_model').modal('open');
									$('#remove_contact_person_model #contact_name').val(name);
									$('#remove_contact_person_model #contact_email').val(email);

								});

									$('.remove_person').off().on('click',function(){

										var name = $('#contact_name').val();
										var email = $('#contact_email').val();

										$.ajax({

											url:base_url+'sales/remove_contact_person',

											type:"post",

											data:{'csrf_test_name':csrf_hash,"name":name,"email":email},

											success:function(res){
											if(res!= false)
											{
												var data=JSON.parse(res);
												var html_person ='';
												var final_html_1 ='';
													if(data != '')
													{
														for(var i=0;i<data.length;i++){
														var primary_user="";
														var tooltip="";
														var tooltip_cls='';
														if(data[i].cp_is_admin=="1"){
															primary_user="checked ";
															tooltip='data-position="left" data-tooltip="Primary User"';
															tooltip_cls='tooltipped';
														}
															if(i == 0)
															{
															html_person='<div class="row"><div class="contact-person-box"><div class="col l1 s1 m1"><input type="radio" id="contact-person"  data-email="'+data[i].cp_email+'" class="radio_primary" name="default" '+primary_user+'><label class="fill-green-radio '+tooltip_cls+'" for="contact-person" '+tooltip+'></label></div><div class="col l10 s10 m10 content-area"><div class="col l3 s3 m3"><div class="gradient-wrapper"><div class="maincircle">100%</div></div></div><div class="col l9 s9 m9"><p class="name"><strong>'+data[i].cp_name+'</strong></p><p class="email">'+data[i].cp_email+'</p></div></div><div class="col l1 s1 m1"><a href="javascript:void(0);" class="remove_contact_person" data-name="'+data[i].cp_name+'" data-email="'+data[i].cp_email+'"><i class="material-icons">close</i></a></div></div></div>';

														   }
														   else if(i == 1)
														   {
															  html_person='<div class="row"><div class="contact-person-box"><div class="col l1 s1 m1"><input type="radio" id="contact-person'+(i+1)+'" data-email="'+data[i].cp_email+'" class="radio_primary" name="default" '+primary_user+'><label class="fill-green-radio '+tooltip_cls+'" for="contact-person'+(i+1)+'"  '+tooltip+'></label></div><div class="col l10 s10 m10 content-area"><div class="col l3 s3 m3"><div class="gradient-wrapper"><div class="maincircle">100%</div></div></div><div class="col l9 s9 m9"><p class="name"><strong>'+data[i].cp_name+'</strong></p><p class="email">'+data[i].cp_email+'</p></div></div><div class="col l1 s1 m1"><a href="javascript:void(0);" class="remove_contact_person" data-name="'+data[i].cp_name+'" data-email="'+data[i].cp_email+'"><i class="material-icons">close</i></a></div></div></div>';

														   }
														   else
														   {
															  html_person='<div class="contactmore"><div class="row"><div class="contact-person-box"><div class="col l1 s1 m1"><input type="radio" class="radio_primary" id="contact-person'+(i+1)+'" data-email="'+data[i].cp_email+'" name="default" '+primary_user+'><label class="fill-green-radio '+tooltip_cls+'" for="contact-person'+(i+1)+'" '+tooltip+'></label></div><div class="col l10 s10 m10 content-area"><div class="col l3 s3 m3"><div class="gradient-wrapper"><div class="maincircle">100%</div></div></div><div class="col l9 s9 m9"><p class="name"><strong>'+data[i].cp_name+'</strong></p><p class="email">'+data[i].cp_email+'</p></div></div><div class="col l1 s1 m1"><a href="javascript:void(0);" class="remove_contact_person" data-name="'+data[i].cp_name+'" data-email="'+data[i].cp_email+'"><i class="material-icons">close</i></a></div></div></div></div>';

														   }
															 final_html_1 = final_html_1 + html_person;
															 $("#person_list").html(final_html_1);
															 $(".tot_person").html(data.length);
															 $('#con_morebtn').show();
															 $('.tooltipped').tooltip();
														}
														Materialize.toast('Contact has been removed.', 2000,'green rounded');
													}
													else
													{
														Materialize.toast('Contact has been removed.', 2000,'green rounded');
														html_person='<div class="gst-boxs add-gst">Add Contact Person Data</div>';
														$("#person_list").html(html_person);
														$(".tot_person").html(0);
													}
												}
												else
												{
													Materialize.toast('Error while processing. Please try again', 2000,'red rounded');

												}

											},

										});
									});
								});
							},

					});
				},
			});



			$.validator.addMethod("checklowercase",
			    function(value, element) {
			        return /^(?=(.*[a-z]){1,}).{1,}$/.test(value);
			});
			$.validator.addMethod("checkuppercase",
			    function(value, element) {
			        return /^(?=.*?[A-Z]).{1,}$/.test(value);
			});
			$.validator.addMethod("checknumber",
			    function(value, element) {
			        return /^(?=(.*[\d]){1,}).{1,}$/.test(value);
			});
			$.validator.addMethod("checkspecialchar",
			    function(value, element) {
			        return /^(?=(.*[\W]){1,})(?!.*\s).{1,}$/.test(value);
			});

			$(".cancel_personal_profile").on("click",function(){
				window.location.href=base_url+'Personal_profile/manage_profile';
			});


			$('.add-contact-person').on('click','input[type="radio"][name="default"]',function(){
					 if ($(this).is(':checked')){
					var email = $(this).data('email');

					console.log(email);
					$.ajax({

								url:base_url+'sales/change_primary_user',

								type:"post",

								data:{'csrf_test_name':csrf_hash,"email":email},

								success:function(res){
									console.log(res);
									if(res!= false)
									{
										var data=JSON.parse(res);
										var html_person ='';
										var final_html_1 ='';
										if(data != '')
										{
											for(var i=0;i<data.length;i++){
											var primary_user="";
											var tooltip="";
											var tooltip_cls='';
											if(data[i].cp_is_admin=="1"){
												primary_user="checked ";
												tooltip='data-position="left" data-tooltip="Primary User"';
												tooltip_cls='tooltipped';
											}
												if(i == 0)
												{
												html_person='<div class="row"><div class="contact-person-box"><div class="col l1 s1 m1"><input type="radio" id="contact-person"  data-email="'+data[i].cp_email+'" class="radio_primary" name="default" '+primary_user+'><label class="fill-green-radio '+tooltip_cls+'" for="contact-person" '+tooltip+'></label></div><div class="col l10 s10 m10 content-area"><div class="col l3 s3 m3"><div class="gradient-wrapper"><div class="maincircle">100%</div></div></div><div class="col l9 s9 m9"><p class="name"><strong>'+data[i].cp_name+'</strong></p><p class="email">'+data[i].cp_email+'</p></div></div><div class="col l1 s1 m1"><a href="javascript:void(0);" class="remove_contact_person" data-name="'+data[i].cp_name+'" data-email="'+data[i].cp_email+'"><i class="material-icons">close</i></a></div></div></div>';

											   }
											   else if(i == 1)
											   {
												  html_person='<div class="row"><div class="contact-person-box"><div class="col l1 s1 m1"><input type="radio" id="contact-person'+(i+1)+'" data-email="'+data[i].cp_email+'" class="radio_primary" name="default" '+primary_user+'><label class="fill-green-radio '+tooltip_cls+'" for="contact-person'+(i+1)+'"  '+tooltip+'></label></div><div class="col l10 s10 m10 content-area"><div class="col l3 s3 m3"><div class="gradient-wrapper"><div class="maincircle">100%</div></div></div><div class="col l9 s9 m9"><p class="name"><strong>'+data[i].cp_name+'</strong></p><p class="email">'+data[i].cp_email+'</p></div></div><div class="col l1 s1 m1"><a href="javascript:void(0);" class="remove_contact_person" data-name="'+data[i].cp_name+'" data-email="'+data[i].cp_email+'"><i class="material-icons">close</i></a></div></div></div>';

											   }
											   else
											   {
												  html_person='<div class="contactmore"><div class="row"><div class="contact-person-box"><div class="col l1 s1 m1"><input type="radio" class="radio_primary" id="contact-person'+(i+1)+'" data-email="'+data[i].cp_email+'" name="default" '+primary_user+'><label class="fill-green-radio '+tooltip_cls+'" for="contact-person'+(i+1)+'" '+tooltip+'></label></div><div class="col l10 s10 m10 content-area"><div class="col l3 s3 m3"><div class="gradient-wrapper"><div class="maincircle">100%</div></div></div><div class="col l9 s9 m9"><p class="name"><strong>'+data[i].cp_name+'</strong></p><p class="email">'+data[i].cp_email+'</p></div></div><div class="col l1 s1 m1"><a href="javascript:void(0);" class="remove_contact_person" data-name="'+data[i].cp_name+'" data-email="'+data[i].cp_email+'"><i class="material-icons">close</i></a></div></div></div></div>';

											   }
												 final_html_1 = final_html_1 + html_person;
												 $("#person_list").html(final_html_1);
												 $(".tot_person").html(data.length);
												 $('#con_morebtn').show();
												 $('.tooltipped').tooltip();
											}
											Materialize.toast('Primary contact updated successfully', 2000,'green rounded');

										}
										else
										{
											html_person='<div class="gst-boxs add-gst">Add Contact Person Data</div>';
											$("#person_list").html(html_person);
											$(".tot_person").html(0);
										}
									}
									else
									{
										Materialize.toast('Error while processing. Please try again', 2000,'red rounded');
									}

								},
					});
				}
			});
			$(".add-contact-person-div").on("click",".is_admin",function(){
					var cp_id = $(this).data('cp_id');
					$.ajax({

								url:base_url+'sales/primary_user',

								type:"post",

								data:{'csrf_test_name':csrf_hash,"cp_id":cp_id},

								success:function(res){

									if(res!= false)
									{
										var data=JSON.parse(res);
										var html_person ='';
										var final_html_1 ='';
										if(data != '')
										{
											for(var i=0;i<data.length;i++){

											var primary_user="";
											var tooltip="";
											var tooltip_cls='';
											if(data[i].cp_is_admin=="1"){
												primary_user="checked ";
												tooltip='data-position="left" data-tooltip="Primary User"';
												tooltip_cls='tooltipped';
											}
											if(i == 0)
											{


												html_person='<div class="row"><div class="contact-person-box"><div class="col l1 s1 m1 "><input type="radio" id="contact-person"  class="is_admin" data-cp_id="'+data[i].cp_id+'" name="is_admin" '+primary_user+'><label class="fill-green-radio '+tooltip_cls+'" for="contact-person" '+tooltip+'></label></div><div class="col l10 s10 m10 content-area"><div class="col l3 s3 m3"><div class="gradient-wrapper"><div class="maincircle">100%</div></div></div><div class="col l9 s9 m9"><p class="name"><strong>'+data[i].cp_name+'</strong></p><p class="email">'+data[i].cp_email+'</p></div></div><div class="col l1 s1 m1"><a class="cur remove_contact_person_editpage modal-trigger" data-cust_id="'+data[i].cust_id+'" data-id="'+data[i].cp_id+'"><i class="material-icons" data-target="remove_contact_person_model_editpage">close</i></a><a href="javascript:void(0);" class="edit_contactmodal"  data-id="'+data[i].cp_id+'" data-cust_id="'+data[i].cust_id+'" data-name="'+data[i].cp_name+'" data-mobile="'+data[i].cp_mobile+'" data-email="'+data[i].cp_email+'" data-password="'+data[i].cp_password+'" data-birthday="'+data[i].cp_birth_date+'" data-birthday_remainder="'+data[i].cp_birth_reminder+'" data-anniversary="'+data[i].cp_anniversary_date+'" data-anniversary_reminder="'+data[i].cp_anniversary_reminder+'" data-is_admin="'+data[i].cp_is_admin+'"><i class="material-icons" style="color: #000;margin-top:0px;">mode_edit</i></a></div></div></div>';


											   }
											   else if(i == 1)
											   {
												  html_person='<div class="row"><div class="contact-person-box"><div class="col l1 s1 m1"><input type="radio" id="contact-person'+(i+1)+'"  class="is_admin" data-cp_id="'+data[i].cp_id+'" name="is_admin" '+primary_user+'><label class="fill-green-radio '+tooltip_cls+'" for="contact-person'+(i+1)+'" '+tooltip+'></label></div><div class="col l10 s10 m10 content-area"><div class="col l3 s3 m3"><div class="gradient-wrapper"><div class="maincircle">100%</div></div></div><div class="col l9 s9 m9"><p class="name"><strong>'+data[i].cp_name+'</strong></p><p class="email">'+data[i].cp_email+'</p></div></div><div class="col l1 s1 m1"><a class="cur remove_contact_person_editpage modal-trigger" data-id="'+data[i].cp_id+'" data-cust_id="'+data[i].cust_id+'" data-target="remove_contact_person_model_editpage"><i class="material-icons">close</i></a><a href="javascript:void(0);" class="edit_contactmodal"  data-id="'+data[i].cp_id+'" data-cust_id="'+data[i].cust_id+'" data-name="'+data[i].cp_name+'" data-mobile="'+data[i].cp_mobile+'" data-email="'+data[i].cp_email+'" data-password="'+data[i].cp_password+'" data-birthday="'+data[i].cp_birth_date+'" data-birthday_remainder="'+data[i].cp_birth_reminder+'" data-anniversary="'+data[i].cp_anniversary_date+'" data-anniversary_reminder="'+data[i].cp_anniversary_reminder+'" data-is_admin="'+data[i].cp_is_admin+'"><i class="material-icons" style="color: #000;margin-top:0px;">mode_edit</i></a></div></div></div>';

											   }
											   else
											   {
												  html_person='<div class="contactmore"><div class="row"><div class="contact-person-box"><div class="col l1 s1 m1"><input type="radio"  class="is_admin" id="contact-person'+(i+1)+'" data-cp_id="'+data[i].cp_id+'" name="is_admin" '+primary_user+'><label class="fill-green-radio '+tooltip_cls+'" for="contact-person'+(i+1)+'" '+tooltip+'></label></div><div class="col l10 s10 m10 content-area"><div class="col l3 s3 m3"><div class="gradient-wrapper"><div class="maincircle">100%</div></div></div><div class="col l9 s9 m9"><p class="name"><strong>'+data[i].cp_name+'</strong></p><p class="email">'+data[i].cp_email+'</p></div></div><div class="col l1 s1 m1"><a class="cur remove_contact_person_editpage modal-trigger" data-id="'+data[i].cp_id+'" data-cust_id="'+data[i].cust_id+'" data-target="remove_contact_person_model_editpage"><i class="material-icons">close</i></a><a href="javascript:void(0);" class="edit_contactmodal"  data-id="'+data[i].cp_id+'" data-cust_id="'+data[i].cust_id+'" data-name="'+data[i].cp_name+'" data-mobile="'+data[i].cp_mobile+'" data-email="'+data[i].cp_email+'" data-password="'+data[i].cp_password+'" data-birthday="'+data[i].cp_birth_date+'" data-birthday_remainder="'+data[i].cp_birth_reminder+'" data-anniversary="'+data[i].cp_anniversary_date+'" data-anniversary_reminder="'+data[i].cp_anniversary_reminder+'" data-is_admin="'+data[i].cp_is_admin+'"><i class="material-icons" style="color: #000;margin-top:0px;">mode_edit</i></a></div></div></div></div>';

											   }
												 final_html_1 = final_html_1 + html_person;
												 $(".person_list").html(final_html_1);
												 $(".tot_person").html(data.length);
												 $('.tooltipped').tooltip();
											}
											Materialize.toast('Primary contact updated successfully', 2000,'green rounded');
										}
										else
										{
											html_person='<div class="gst-boxs add-gst">Add Contact Person Data</div>';
											$(".person_list").html(html_person);
											Materialize.toast('Add contact information', 2000,'green rounded');
										}
									}
									else
									{
										Materialize.toast('Error while processing. Please try again', 2000,'red rounded');
									}



								}
					});

			});

$(".add-contact-person-div").on("click",".is_admin_vend",function(){
					var cp_id = $(this).data('cp_id');
					$.ajax({

								url:base_url+'expense/primary_user',

								type:"post",

								data:{'csrf_test_name':csrf_hash,"cp_id":cp_id},

								success:function(res){

									if(res!= false)
									{
										var data=JSON.parse(res);
										var html_person ='';
										var final_html_1 ='';
										if(data != '')
										{
											for(var i=0;i<data.length;i++){

											var primary_user="";
											var tooltip="";
											var tooltip_cls='';
											if(data[i].cp_is_admin=="1"){
												primary_user="checked ";
												tooltip='data-position="left" data-tooltip="Primary User"';
												tooltip_cls='tooltipped';
											}
											if(i == 0)
											{


												html_person='<div class="row"><div class="contact-person-box"><div class="col l1 s1 m1 "><input type="radio" id="contact-person"  class="is_admin" data-cp_id="'+data[i].cp_id+'" name="is_admin" '+primary_user+'><label class="fill-green-radio '+tooltip_cls+'" for="contact-person" '+tooltip+'></label></div><div class="col l10 s10 m10 content-area"><div class="col l3 s3 m3"><div class="gradient-wrapper"><div class="maincircle">100%</div></div></div><div class="col l9 s9 m9"><p class="name"><strong>'+data[i].cp_name+'</strong></p><p class="email">'+data[i].cp_email+'</p></div></div><div class="col l1 s1 m1"><a class="cur remove_contact_person_editpage modal-trigger" data-cust_id="'+data[i].cust_id+'" data-id="'+data[i].cp_id+'"><i class="material-icons" data-target="remove_contact_person_model_editpage">close</i></a><a href="javascript:void(0);" class="edit_contactmodal"  data-id="'+data[i].cp_id+'" data-cust_id="'+data[i].cust_id+'" data-name="'+data[i].cp_name+'" data-mobile="'+data[i].cp_mobile+'" data-email="'+data[i].cp_email+'" data-password="'+data[i].cp_password+'" data-birthday="'+data[i].cp_birth_date+'" data-birthday_remainder="'+data[i].cp_birth_reminder+'" data-anniversary="'+data[i].cp_anniversary_date+'" data-anniversary_reminder="'+data[i].cp_anniversary_reminder+'" data-is_admin="'+data[i].cp_is_admin+'"><i class="material-icons" style="color: #000;margin-top:0px;">mode_edit</i></a></div></div></div>';


											   }
											   else if(i == 1)
											   {
												  html_person='<div class="row"><div class="contact-person-box"><div class="col l1 s1 m1"><input type="radio" id="contact-person'+(i+1)+'"  class="is_admin" data-cp_id="'+data[i].cp_id+'" name="is_admin" '+primary_user+'><label class="fill-green-radio '+tooltip_cls+'" for="contact-person'+(i+1)+'" '+tooltip+'></label></div><div class="col l10 s10 m10 content-area"><div class="col l3 s3 m3"><div class="gradient-wrapper"><div class="maincircle">100%</div></div></div><div class="col l9 s9 m9"><p class="name"><strong>'+data[i].cp_name+'</strong></p><p class="email">'+data[i].cp_email+'</p></div></div><div class="col l1 s1 m1"><a class="cur remove_contact_person_editpage modal-trigger" data-id="'+data[i].cp_id+'" data-cust_id="'+data[i].cust_id+'" data-target="remove_contact_person_model_editpage"><i class="material-icons">close</i></a><a href="javascript:void(0);" class="edit_contactmodal"  data-id="'+data[i].cp_id+'" data-cust_id="'+data[i].cust_id+'" data-name="'+data[i].cp_name+'" data-mobile="'+data[i].cp_mobile+'" data-email="'+data[i].cp_email+'" data-password="'+data[i].cp_password+'" data-birthday="'+data[i].cp_birth_date+'" data-birthday_remainder="'+data[i].cp_birth_reminder+'" data-anniversary="'+data[i].cp_anniversary_date+'" data-anniversary_reminder="'+data[i].cp_anniversary_reminder+'" data-is_admin="'+data[i].cp_is_admin+'"><i class="material-icons" style="color: #000;margin-top:0px;">mode_edit</i></a></div></div></div>';

											   }
											   else
											   {
												  html_person='<div class="contactmore"><div class="row"><div class="contact-person-box"><div class="col l1 s1 m1"><input type="radio"  class="is_admin" id="contact-person'+(i+1)+'" data-cp_id="'+data[i].cp_id+'" name="is_admin" '+primary_user+'><label class="fill-green-radio '+tooltip_cls+'" for="contact-person'+(i+1)+'" '+tooltip+'></label></div><div class="col l10 s10 m10 content-area"><div class="col l3 s3 m3"><div class="gradient-wrapper"><div class="maincircle">100%</div></div></div><div class="col l9 s9 m9"><p class="name"><strong>'+data[i].cp_name+'</strong></p><p class="email">'+data[i].cp_email+'</p></div></div><div class="col l1 s1 m1"><a class="cur remove_contact_person_editpage modal-trigger" data-id="'+data[i].cp_id+'" data-cust_id="'+data[i].cust_id+'" data-target="remove_contact_person_model_editpage"><i class="material-icons">close</i></a><a href="javascript:void(0);" class="edit_contactmodal"  data-id="'+data[i].cp_id+'" data-cust_id="'+data[i].cust_id+'" data-name="'+data[i].cp_name+'" data-mobile="'+data[i].cp_mobile+'" data-email="'+data[i].cp_email+'" data-password="'+data[i].cp_password+'" data-birthday="'+data[i].cp_birth_date+'" data-birthday_remainder="'+data[i].cp_birth_reminder+'" data-anniversary="'+data[i].cp_anniversary_date+'" data-anniversary_reminder="'+data[i].cp_anniversary_reminder+'" data-is_admin="'+data[i].cp_is_admin+'"><i class="material-icons" style="color: #000;margin-top:0px;">mode_edit</i></a></div></div></div></div>';

											   }
												 final_html_1 = final_html_1 + html_person;
												 $(".person_list").html(final_html_1);
												 $(".tot_person").html(data.length);
												 $('.tooltipped').tooltip();
											}
											Materialize.toast('Primary contact updated successfully', 2000,'green rounded');
										}
										else
										{
											html_person='<div class="gst-boxs add-gst">Add Contact Person Data</div>';
											$(".person_list").html(html_person);
											Materialize.toast('Add contact information', 2000,'green rounded');
										}
									}
									else
									{
										Materialize.toast('Error while processing. Please try again', 2000,'red rounded');
									}



								}
					});

			});


		//$('input[type="checkbox"]').click(function(){
		$('body').on("click","#same-bill",function(){
			//$("#same_bill").on("click",function(){

					/*var country = $("#country").val();
					console.log(country);
					var state = $("#state").val();
					var city = $("#city").val();
				if($(this).is(':checked')){
					if($("#billing_address").val()=='' || $("#country").val()==''){
						Materialize.toast('Add Billing Address First!', 2000,'red rounded');
						$(this).prop('checked',false);
					}else{
						console.log(':'+$("#country").val()+':');
					$("#shipping_address").parents('.input-field').children().addClass('active');
					$("#shipping_address").val($("#billing_address").val());
					$("#shipping_pincode").parents('.input-field').children().addClass('active');
					$("#shipping_pincode").val($("#pincode").val());
					//console.log(country);
					$("#shipping_country").parents('.input-field').addClass('label-active');
					$('#shipping_country option[value="'+country+'"]').prop('selected',true).change();
					$('#shipping_country').material_select();
					$.ajax({
						url:base_url+'profile/get_states',
						type:"POST",
						data:{'country_id':$("#country").val()},
						success:function(res){
							$("#shipping_state").html(res);
							$("#shipping_state").parents('.input-field').addClass('label-active');
							$('#shipping_state option[value="'+state+'"]').attr('selected',true).change();
							$('#shipping_state').material_select();
						},
					});
					setTimeout(function(){
					$.ajax({
						url:base_url+'profile/get_cities',
						type:"POST",
						data:{'state_id':$("#state").val()},
						success:function(res){
							$("#shipping_city").html(res);
							$("#shipping_city").parents('.input-field').addClass('label-active');

							$('#shipping_city option[value="'+city+'"]').attr('selected',true).change();
							//$('#shipping_city').material_select();
						},
					});
					}, 800);
				  }
					//console.log($("#shipping_state").val());
				}
				else{
					$("#shipping_pincode").val("");
					$("#shipping_address").val("");
					$("#shipping_country").parents('.input-field').removeClass('label-active');
					$('#shipping_country option[value=""]').attr('selected',true).change();
					$("#shipping_state").parents('.input-field').removeClass('label-active');
					$('#shipping_state option[value=""]').attr('selected',true).change();
					$("#shipping_city").parents('.input-field').removeClass('label-active');
					$('#shipping_city option[value=""]').attr('selected',true).change();
					$('.shipping_values input').val("");
				} */
			});

		$('input[name="same-bill-customer"]').click(function(){
		//$(document).on("click","#same_bill",function(){
			//$("#same_bill").on("click",function(){
					var country = $("#new_cust_country").val();
					var state = $("#new_cust_state").val();
					var city = $("#new_cust_city").val();
				if($(this).is(':checked')){
					$("#new_cust_ship_address").parents('.full-bg-label').addClass('active');
					$("#new_cust_ship_address").val($("#new_cust_billing_address").val());
					$("#new_cust_ship_pincode").parents('.full-bg-label').addClass('label-active');
					$("#new_cust_ship_pincode").val($("#new_cust_pincode").val());
					$("#new_cust_ship_country").parents('.input-field').addClass('label-active');
					$('#new_cust_ship_country option[value="'+country+'"]').attr('selected','selected');
					$('#new_cust_ship_country').material_select();
					$.ajax({
						url:base_url+'profile/get_states',
						type:"POST",
						data:{'csrf_test_name':csrf_hash,'country_id':$("#new_cust_country").val()},
						success:function(res){
							$("#new_cust_ship_state").html(res);
							$("#new_cust_ship_state").parents('.input-field').addClass('label-active');
							$('#new_cust_ship_state option[value="'+state+'"]').attr('selected',true);
							$('#new_cust_ship_state').material_select();
						},
					});
					$.ajax({
						url:base_url+'profile/get_cities',
						type:"POST",
						data:{'csrf_test_name':csrf_hash,'state_id':$("#new_cust_state").val()},
						success:function(res){
							$("#new_cust_ship_city").html(res);
							$("#new_cust_ship_city").parents('.input-field').addClass('label-active');
							$('#new_cust_ship_city option[value="'+city+'"]').attr('selected',true);
							$('#new_cust_ship_city').material_select();
						},
					});
					//console.log($("#shipping_state").val());
				}
				else{
					$("#new_cust_ship_pincode").val("");
					$("#new_cust_ship_address").val("");
					$("#new_cust_ship_country").parents('.input-field').removeClass('label-active');
					$('#new_cust_ship_country option[value="'+country+'"]').attr('selected',false);
					$("#new_cust_ship_state").parents('.input-field').removeClass('label-active');
					$('#new_cust_ship_state option[value="'+state+'"]').attr('selected',false);
					$("#new_cust_ship_city").parents('.input-field').removeClass('label-active');
					$('#new_cust_ship_city option[value="'+city+'"]').attr('selected',false);
					$('.shipping_values input').val("");
				}
			});
		$(document).ready(function () {

			$(document).on('click' , '.edit_cust_gst', function(){
				$('#edit_gstmodal').modal('open');
				var gst_id = $(this).data('gst_id');
				var gst_no = $(this).data('gst_no');
				var location = $(this).data('location');
				var cust_id = $(this).data('cust_id');
				var address = $(this).data('address');
				var country = $(this).data('country');
				var city = $(this).data('city');
				var state = $(this).data('state_code');
				// alert(state);
				// alert(city);
				var zipcode = $(this).data('zipcode');
				$('#edit_gstmodal #gst_no_edit').parents('.input-field').children().addClass('active');
				$('#edit_gstmodal #gst_edit_location').parents('.input-field').children().addClass('active');

				$('#edit_gstmodal #gst_id').val(gst_id);
				$('#edit_gstmodal #cust_id_edit_gst').val(cust_id);
				$('#edit_gstmodal #gst_edit_location').val(location);
				$('#edit_gstmodal #gst_no_edit').val(gst_no);
				$('#edit_gstmodal #edit_billing_address1').val(address);
				$('#edit_gstmodal #pincode2').val(zipcode);
				$('#edit_gstmodal #pincode2').click().focus();
				/*$('#edit_gstmodal #country2').val(country);
				$('select#country2 option[value="'+country+'"]').attr('selected',true).change();
				$('#country2').material_select();
				//await sleep(5000);
				$('#edit_gstmodal #state2').val(state);
				$('select#state2 option[value="'+state+'"]').attr('selected',true).change();
				$('#state2').material_select();
				$('#edit_gstmodal #city2').val(city);
				$('select#city2 option[value="'+city+'"]').attr('selected',true);


				$('#city2').material_select();*/
				 setTimeout(function(){ 
           
  
 
          $("#country2").val(country).prop("selected", "selected").change();
          $('#country2').material_select();
        setTimeout(function(){ 
          $("select#state2").val(state).prop("selected", "selected").change();
          $('#state2').material_select();
        setTimeout(function(){ 
          $('select#city2 option[value='+city+']').attr("selected", 'selected').change();
          //("select#city_local").val(city).prop("selected", "selected").change();
          $('#city2').material_select();


        }, 2000); 
        }, 1500);
        }, 1000);

			});

			$("#edit_customer_gstin").submit(function(e){
				e.preventDefault();
			}).validate({
				rules:{

					'gst_no_edit':{
						required:true,
						gstreg:true,
					},
					'location':{
						required:true,
					},
					'edit_bus_billing_address1':{
						required:true,
					},
					'edit_bus_billing_country1':{
						required:true,
					},
					'edit_bus_billing_state1':{
						required:true,
					},
					'edit_bus_billing_city1':{
						required:true,
					},
					'bus_billing_zipcode1_edit':{
						required:true,
					},
				},
				messages:{

					gst_no_edit:{
						required:"GST Number is required",

					},
					location:{
						required:"Place of supply is required",
					},
					edit_bus_billing_address1:{
						required:"Billing Address is required",
					},
					edit_bus_billing_country1:{
						required:"Country is required",
					},
					edit_bus_billing_state1:{
						required:"State is required",
					},
					edit_bus_billing_city1:{
						required:"City is required",
					},
					bus_billing_zipcode1_edit:{
						required:"Pin Code is required",
					},
				},
				submitHandler:function(form){
						var frm=$(form).serialize();
								$.ajax({
								url:base_url+'sales/edit_gst_info',
								type:"POST",
								data:{'csrf_test_name':csrf_hash,"frm":frm,},
								success:function(res){
									//console.log(res);
									if(res!= false)
									{
										var data=JSON.parse(res);
										var html_1 ='';
										var final_html ='';
										if(data != '')
										{
											for(var i=0;i<data.length;i++){
											if(i == 0)
											   {
												html_1='<div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">GST NO.</span><p>'+data[i].gst_no+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">PLACE OF SUPPLY</span><p>'+data[i].place+'</p></div><div class="col l1 s1 m1 gstaction"><a href="javascript:void(0);" class="edit_remove_gst_data" data-cust_id="'+data[i].bus_id+'" data-gst_id="'+data[i].gst_id+'" data-location="'+data[i].place+'" data-gst_no="'+data[i].gst_no+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a><a href="javascript:void(0);" class="edit_cust_gst" data-cust_id="'+data[i].bus_id+'" data-gst_id="'+data[i].gst_id+'" data-gst_no="'+data[i].gst_no+'" data-location="'+data[i].place+'" data-address="'+data[i].address+'" data-country="'+data[i].country+'" data-state_code="'+data[i].state_code+'" data-city="'+data[i].city+'" data-zipcode="'+data[i].zipcode+'"><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div>';
											   }
											   else
											   {
												 html_1='<div class="gstmore active"><div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">GST NO.</span><p>'+data[i].gst_no+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">PLACE OF SUPPLY</span><p>'+data[i].place+'</p></div><div class="col l1 s1 m1 gstaction"><a href="javascript:void(0);" class="edit_remove_gst_data" data-cust_id="'+data[i].bus_id+'" data-gst_id="'+data[i].gst_id+'" data-location="'+data[i].place+'" data-gst_no="'+data[i].gst_no+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a><a href="javascript:void(0);" class="edit_cust_gst" data-cust_id="'+data[i].bus_id+'" data-gst_id="'+data[i].gst_id+'" data-gst_no="'+data[i].gst_no+'" data-location="'+data[i].place+'" data-address="'+data[i].address+'" data-country="'+data[i].country+'" data-state_code="'+data[i].state_code+'" data-city="'+data[i].city+'" data-zipcode="'+data[i].zipcode+'"><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div></div>';

											   }
												 final_html = final_html + html_1;
												 $("#gst_array_list_edit").html(final_html);
												 $(".tot_gst").html(data.length);
											}
											Materialize.toast('GSTIN Details has been updated!', 2000,'green rounded');
										}
										else
										{
											Materialize.toast('GSTIN Details has been updated!', 2000,'green rounded');
											//html_1='<div class="gst-boxs add-gst">Add GSTIN Data</div>';
											//$("#gst_array_list").html(html_1);
											 Materialize.toast('ADD GSTIN DATA!', 2000,'green rounded');
										}
									}
									else
									{
										Materialize.toast('Error while processing. Please try again', 2000,'red rounded');
									}
									$('#showgstboxs i').html('keyboard_arrow_up');
									$("#edit_customer_gstin").find("input[type=text], textarea").val("");
									$("#edit_gstmodal").modal('close');

							},

					});
				},
			});
		});
			$('.editpage_gstmodal_cls').on('click' ,function(){

				var cust_id = $(this).data('cust_id');

				$('#editpage_gstmodal').modal('open');
				$('#editpage_gstmodal #cust_id').val(cust_id);

			});

			$("#editpage_customer_gstin").submit(function(e){
				e.preventDefault();
			}).validate({
				rules:{

					'gstin1':{
						required:true,
						gstreg:true,
					},
					'location1':{
						required:true,
					},
					'bus_billing_address1':{
						required:true,
					},
					'bus_billing_country1':{
						required:true,
					},
					'bus_billing_state1':{
						required:true,
					},
					'bus_billing_city1':{
						required:true,
					},
					'bus_billing_zipcode1':{
						required:true,
					},
				},
				messages:{

					gstin:{
						required:"GST Number is required",

					},
					location:{
						required:"Place of supply is required",
					},
					bus_billing_address1:{
						required:"Billing Address is required",
					},
					bus_billing_country1:{
						required:"Country is required",
					},
					bus_billing_state1:{
						required:"State is required",
					},
					bus_billing_city1:{
						required:"City is required",
					},
					bus_billing_zipcode1:{
						required:"Pin Code is required",
					},
				},
				submitHandler:function(form){
						var frm=$(form).serialize();
								$.ajax({
								url:base_url+'sales/add_gst_info_2',
								type:"POST",
								data:{'csrf_test_name':csrf_hash,"frm":frm,},
								success:function(res){
									if(res!= false)
									{
										var data=JSON.parse(res);
										var html_1 ='';
										var final_html ='';
										if(data != '')
										{
											for(var i=0;i<data.length;i++){
												if(data[i].status=="Inactive"){
													   var deac="deactive_record";
												   }else{
													    var deac="";
												   }
												   console.log(deac);
											if(i == 0)
											   {
												html_1='<div class="gst-boxs add-gst '+deac+'"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">GST NO.</span><p>'+data[i].gst_no+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">PLACE OF SUPPLY</span><p>'+data[i].place+'</p></div><div class="col l1 s1 m1 gstaction"><a href="javascript:void(0);" class="edit_remove_gst_data" data-cust_id="'+data[i].bus_id+'" data-gst_id="'+data[i].gst_id+'" data-location="'+data[i].place+'" data-gst_no="'+data[i].gst_no+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a><a href="javascript:void(0);" class="edit_cust_gst" data-cust_id="'+data[i].bus_id+'" data-gst_id="'+data[i].gst_id+'" data-gst_no="'+data[i].gst_no+'" data-location="'+data[i].place+'" data-address="'+data[i].address+'" data-country="'+data[i].country+'" data-state_code="'+data[i].state_code+'" data-city="'+data[i].city+'" data-zipcode="'+data[i].zipcode+'"><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div>';
											   }
											   else
											   {
												 html_1='<div class="gstmore active"><div class="gst-boxs add-gst '+deac+'"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">GST NO.</span><p>'+data[i].gst_no+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">PLACE OF SUPPLY</span><p>'+data[i].place+'</p></div><div class="col l1 s1 m1 gstaction"><a href="javascript:void(0);" class="edit_remove_gst_data" data-location="'+data[i].place+'" data-cust_id="'+data[i].bus_id+'" data-gst_id="'+data[i].gst_id+'" data-gst_no="'+data[i].gst_no+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a><a href="javascript:void(0);" class="edit_cust_gst" data-cust_id="'+data[i].bus_id+'" data-gst_id="'+data[i].gst_id+'" data-gst_no="'+data[i].gst_no+'" data-location="'+data[i].place+'" data-address="'+data[i].address+'" data-country="'+data[i].country+'" data-state_code="'+data[i].state_code+'" data-city="'+data[i].city+'" data-zipcode="'+data[i].zipcode+'"><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div></div>';

											   }
												 final_html = final_html + html_1;
												 $("#gst_array_list_edit").html(final_html);
												 $(".tot_gst").html(data.length);
											}
											Materialize.toast('GSTIN Details have been set!', 2000,'green rounded');
										}
										else
										{
											Materialize.toast('GSTIN Details have been set!', 2000,'green rounded');
											//html_1='<div class="gst-boxs add-gst">Add GSTIN Data</div>';
											//$("#gst_array_list_edit").html(html_1);
											$(".tot_gst").html(0);

										}
									}
									else
									{
										Materialize.toast('Error while processing. Please try again', 2000,'red rounded');
									}
									$('#showgstboxs i').html('keyboard_arrow_up');
									$("#editpage_customer_gstin").find("input[type=text], textarea").val("");
									$("#editpage_gstmodal").modal('close');
							},

					});
				},
			});
							$(document).ready( function()  {
								$("#create_invoice #company_name").val($("#header_company_profiles").val());
								$(document).on('click', '.edit_remove_gst_data' ,function(){
									var gst_id = $(this).data('gst_id');
									var cust_id = $(this).data('cust_id');
									var gst_no = $(this).data('gst_no');
									var location = $(this).data('location');

									$('#editpage_remove_gst_data').modal('open');
									$('#editpage_remove_gst_data #remove_gst_no').val(gst_no);
									$('#editpage_remove_gst_data #remove_location').val(location);
									$('#editpage_remove_gst_data #remove_gst_id').val(gst_id);
									$('#editpage_remove_gst_data #remove_cust_id').val(cust_id);

								});

								$('.edit_remove_gst').on('click',function(){

										var gst_id = $('#remove_gst_id').val();
										var cust_id = $('#remove_cust_id').val();

										$.ajax({

											url:base_url+'sales/delete_gst_info',

											type:"post",

											data:{'csrf_test_name':csrf_hash,"gst_id":gst_id,"cust_id":cust_id},

											success:function(res){
													if(res!= false)
													{
														var data=JSON.parse(res);
														var html_1 ='';
														var final_html ='';
														if(data != '')
														{
															for(var i=0;i<data.length;i++){
																if(data[i].status=="Inactive"){
													   var deac="deactive_record";
												   }else{
													    var deac="";
												   }
												   console.log(deac);
															if(i == 0)
															   {
																html_1='<div class="gst-boxs add-gst '+deac+'"><div class="col l1 s1 m1 sno"><div class="row">'+
															   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">GST NO.</span><p>'+data[i].gst_no+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">PLACE OF SUPPLY</span><p>'+data[i].place+'</p></div><div class="col l1 s1 m1 gstaction"><a href="javascript:void(0);" class="edit_remove_gst_data" data-cust_id="'+data[i].bus_id+'" data-gst_id="'+data[i].gst_id+'" data-location="'+data[i].place+'" data-gst_no="'+data[i].gst_no+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a><a href="javascript:void(0);" class="edit_cust_gst" data-cust_id="'+data[i].bus_id+'" data-gst_id="'+data[i].gst_id+'" data-gst_no="'+data[i].gst_no+'" data-location="'+data[i].place+'"><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div>';
															   }
															   else
															   {
																 html_1='<div class="gstmore"><div class="gst-boxs add-gst '+deac+'"><div class="col l1 s1 m1 sno"><div class="row">'+
															   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">GST NO.</span><p>'+data[i].gst_no+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">PLACE OF SUPPLY</span><p>'+data[i].place+'</p></div><div class="col l1 s1 m1 gstaction"><a href="javascript:void(0);" class="edit_remove_gst_data" data-cust_id="'+data[i].bus_id+'" data-gst_id="'+data[i].gst_id+'" data-location="'+data[i].place+'" data-gst_no="'+data[i].gst_no+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a><a href="javascript:void(0);" class="edit_cust_gst" data-cust_id="'+data[i].bus_id+'" data-gst_id="'+data[i].gst_id+'" data-gst_no="'+data[i].gst_no+'" data-location="'+data[i].place+'"><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div></div>';

															   }
																 final_html = final_html + html_1;
																 $("#gst_array_list_edit").html(final_html);
																 $(".tot_gst").html(data.length);
															}
															Materialize.toast('GST Info removed successfully !', 2000,'green rounded');
														}
														else
														{
															Materialize.toast('GST Info removed successfully !', 2000,'green rounded');
															//html_1='<div class="gst-boxs add-gst">Add GSTIN Data</div>';
															//$("#gst_array_list_edit").html(html_1);
															//Materialize.toast('ADD GSTIN DATA!', 2000,'green rounded');
															$(".tot_gst").html(0);
														}
													}
													else
													{
														//html_1='<div class="gst-boxs add-gst">Add GSTIN Data</div>';
														//$("#gst_array_list_edit").html(html_1);
														Materialize.toast('Error while processing!', 2000,'red rounded');
													}
												},

											});
									});
								});

									$('.add-contact-person').on('click','.edit_contactmodal',function() {

									var id = $(this).data('id');
									var name = $(this).data('name');
									var cust_id = $(this).data('cust_id');
									var mobile = $(this).data('mobile');
									var email = $(this).data('email');
									var birthday = $(this).data('birthday');
									var birthday_remainder = $(this).data('birthday_remainder');
									var anniversary = $(this).data('anniversary');
									var anniversary_reminder = $(this).data('anniversary_reminder');
									var is_admin = $(this).data('is_admin');
									var password = $(this).data('password');
									$('#edit_contactperson #edit_name,#edit_contactperson #edit_mobile,#edit_contactperson #edit_email,#edit_birthday,#edit_anniversary').parents('.input-field').children().addClass('active');
									$('#edit_contactperson #edit_primary').prop('checked', false);
									$('#edit_contactmodal').modal('open');
									$('#edit_contactperson #contact_id').val(id);
									$('#edit_contactperson #edit_name').val(name);
									$('#edit_contactperson #edit_mobile').val(mobile);
									$('#edit_contactperson #edit_email').val(email);
									$('#edit_contactperson #edit_birthday').val(birthday);
									//$('#edit_contactperson #edit_birthday_reminder option[value='+birthday_remainder+']').attr('selected','selected');
									$('#edit_contactperson #edit_anniversary').val(anniversary);
									//$('#edit_contactperson #edit_anniversary_reminder option[value='+anniversary_reminder+']').attr('selected','selected');
									if(is_admin == 1){
										$('#edit_contactperson #edit_primary').prop('checked', true);
									}
									$('#edit_contactperson #remove_cust_id').val(is_admin);
									$('#edit_contactperson #edit_cust_id').val(cust_id);
									$('#edit_contactperson #edit_old_password').val(password);

								});


		$("#edit_contactperson").submit(function(e){
				e.preventDefault();
			}).validate({
				rules:{

					edit_name:{
						required:true,
					},

					edit_email:{
						required:true,
						email:true,
					},
					edit_mobile:{
						//mobileno:true,
						digits:true,
						required:true,
					},
					edit_password:{
						minlength:8,
						maxlength:12,
					},
					edit_cpassword:{
						equalTo:'#edit_password',
					},
				},
				messages:{

					edit_name:{
						required:"Name is required",
					},
					edit_mobile:{
						required:'Mobile Number is required',
					},
					edit_email:{
						required:"Email ID is required",
						email:'Enter valid Email ID',
					},
					edit_password:{
						minlength:'Enter minimum 8 characters for password.',
						maxlength:"Maximum 12 character long password allowed.",
					},
				},
				submitHandler:function(form){
						var frm=$(form).serialize();
								$.ajax({
								url:base_url+'sales/edit_contact_person_info',
								type:"POST",
								data:{'csrf_test_name':csrf_hash,"frm":frm,},
								beforeSend: function(){
								 	if($.trim($('#edit_contactperson #edit_password').val())!='' && $('#edit_contactperson #edit_old_password').val()!=$.md5($('#edit_contactperson #edit_password').val())){
										Materialize.toast('Sending Email.......', 2000,'green rounded');
									}
								    },
								success:function(res){
									if(res!= false)
									{
										var data=JSON.parse(res);
										var html_person ='';
										var final_html_1 ='';
										if(data != '')
										{
											for(var i=0;i<data.length;i++){
											var primary_user="";
											var tooltip="";
											var tooltip_cls='';
											if(data[i].cp_is_admin=="1"){
												primary_user="checked ";
												tooltip='data-position="left" data-tooltip="Primary User"';
												tooltip_cls='tooltipped';
											}

											if(i == 0)
											{
												html_person='<div class="row"><div class="contact-person-box"><div class="col l1 s1 m1"><input type="radio" id="contact-person" class="is_admin" data-cp_id="'+data[i].cp_id+'" name="is_admin" '+primary_user+'><label class="fill-green-radio '+tooltip_cls+'" for="contact-person" '+tooltip+'></label></div><div class="col l10 s10 m10 content-area"><div class="col l3 s3 m3"><div class="gradient-wrapper"><div class="maincircle">100%</div></div></div><div class="col l9 s9 m9"><p class="name"><strong>'+data[i].cp_name+'</strong></p><p class="email">'+data[i].cp_email+'</p></div></div><div class="col l1 s1 m1"><a class="cur remove_contact_person_editpage modal-trigger" data-cust_id="'+data[i].cust_id+'" data-id="'+data[i].cp_id+'" data-target="remove_contact_person_model_editpage"><i class="material-icons">close</i></a><a href="javascript:void(0);" class="edit_contactmodal"  data-id="'+data[i].cp_id+'" data-cust_id="'+data[i].cust_id+'" data-name="'+data[i].cp_name+'" data-mobile="'+data[i].cp_mobile+'" data-email="'+data[i].cp_email+'" data-password="'+data[i].cp_password+'" data-birthday="'+data[i].cp_birth_date+'" data-birthday_remainder="'+data[i].cp_birth_reminder+'" data-anniversary="'+data[i].cp_anniversary_date+'" data-anniversary_reminder="'+data[i].cp_anniversary_reminder+'" data-is_admin="'+data[i].cp_is_admin+'"><i class="material-icons" style="color: #000;margin-top:0px;">mode_edit</i></a></div></div></div>';

											   }
											   else if(i == 1)
											   {
												  html_person='<div class="row"><div class="contact-person-box"><div class="col l1 s1 m1"><input type="radio" id="contact-person'+(i+1)+'"  class="is_admin" data-cp_id="'+data[i].cp_id+'" name="is_admin" '+primary_user+'><label class="fill-green-radio '+tooltip_cls+'" for="contact-person'+(i+1)+'" '+tooltip+'></label></div><div class="col l10 s10 m10 content-area"><div class="col l3 s3 m3"><div class="gradient-wrapper"><div class="maincircle">100%</div></div></div><div class="col l9 s9 m9"><p class="name"><strong>'+data[i].cp_name+'</strong></p><p class="email">'+data[i].cp_email+'</p></div></div><div class="col l1 s1 m1"><a class="cur remove_contact_person_editpage modal-trigger" data-id="'+data[i].cp_id+'" data-cust_id="'+data[i].cust_id+'" data-target="remove_contact_person_model_editpage"><i class="material-icons">close</i></a><a href="javascript:void(0);" class="edit_contactmodal"  data-id="'+data[i].cp_id+'" data-cust_id="'+data[i].cust_id+'" data-name="'+data[i].cp_name+'" data-mobile="'+data[i].cp_mobile+'" data-email="'+data[i].cp_email+'" data-password="'+data[i].cp_password+'" data-birthday="'+data[i].cp_birth_date+'" data-birthday_remainder="'+data[i].cp_birth_reminder+'" data-anniversary="'+data[i].cp_anniversary_date+'" data-anniversary_reminder="'+data[i].cp_anniversary_reminder+'" data-is_admin="'+data[i].cp_is_admin+'"><i class="material-icons" style="color: #000;margin-top:0px;">mode_edit</i></a></div></div></div>';

											   }
											   else
											   {
												  html_person='<div class="contactmore"><div class="row"><div class="contact-person-box"><div class="col l1 s1 m1"><input type="radio"  class="is_admin" id="contact-person'+(i+1)+'" data-cp_id="'+data[i].cp_id+'" name="is_admin" '+primary_user+'><label class="fill-green-radio '+tooltip_cls+'" for="contact-person'+(i+1)+'" '+tooltip+'></label></div><div class="col l10 s10 m10 content-area"><div class="col l3 s3 m3"><div class="gradient-wrapper"><div class="maincircle">100%</div></div></div><div class="col l9 s9 m9"><p class="name"><strong>'+data[i].cp_name+'</strong></p><p class="email">'+data[i].cp_email+'</p></div></div><div class="col l1 s1 m1"><a class="cur remove_contact_person_editpage modal-trigger" data-id="'+data[i].cp_id+'" data-cust_id="'+data[i].cust_id+'" data-target="remove_contact_person_model_editpage"><i class="material-icons">close</i></a><a href="javascript:void(0);" class="edit_contactmodal"  data-id="'+data[i].cp_id+'" data-cust_id="'+data[i].cust_id+'" data-name="'+data[i].cp_name+'" data-mobile="'+data[i].cp_mobile+'" data-email="'+data[i].cp_email+'" data-password="'+data[i].cp_password+'" data-birthday="'+data[i].cp_birth_date+'" data-birthday_remainder="'+data[i].cp_birth_reminder+'" data-anniversary="'+data[i].cp_anniversary_date+'" data-anniversary_reminder="'+data[i].cp_anniversary_reminder+'" data-is_admin="'+data[i].cp_is_admin+'"><i class="material-icons" style="color: #000;margin-top:0px;">mode_edit</i></a></div></div></div></div>';

											   }
												 final_html_1 = final_html_1 + html_person;
												 $(".person_list").html(final_html_1);
												 $(".tot_person").html(data.length);
												 $('.tooltipped').tooltip();
											}
											if($.trim($('#edit_contactperson #edit_password').val())!='' && $('#edit_contactperson #edit_old_password').val()!=$.md5($('#edit_contactperson #edit_password').val())){
												//$('.edit_contactmodal').data('password',$.md5($('#edit_contactperson #edit_password').val()));
												Materialize.toast("Your customer's log in and password has been updated", 3000,'green rounded');
											}else{
											Materialize.toast('Contact has been successfully updated!', 2000,'green rounded');
											}
										}
										else
										{
											html_person='<div class="gst-boxs add-gst">Add Contact Person Data</div>';
											$(".person_list").html(html_person);
											Materialize.toast('Add contact information', 2000,'green rounded');
										}
									}
									else
									{
										Materialize.toast('Error while processing. Please try again', 2000,'red rounded');
									}
									$("#edit_contactperson").find("input[type=text],input[type=password], textarea").val("");
									$("#edit_contactmodal").modal('close');

							},

					});
				},
			});
			                    $('.add-contact-person').on('click','.remove_contact_person_editpage',function() {

									var id = $(this).data('id');
									var cust_id = $(this).data('cust_id');

									//$('#remove_contact_person_model_editpage').modal('open');
									$('#remove_contact_person_model_editpage #remove_contact_id').val(id);
									$('#remove_contact_person_model_editpage #remove_contact_cust_id').val(cust_id);

								});

									$('.remove_person_edit').on('click',function(){

										var id = $('#remove_contact_id').val();
										var cust_id = $('#remove_contact_cust_id').val();

										$.ajax({

											url:base_url+'sales/delete_contact_info',

											type:"post",

											data:{'csrf_test_name':csrf_hash,"id":id,"cust_id":cust_id},

											success:function(res){
													if(res!= false)
													{
														var data=JSON.parse(res)
														var html_1 ='';
														var final_html_1 ='';
														if(data != '')
														{
															for(var i=0;i<data.length;i++){
																var primary_user="";
																var tooltip="";
																var tooltip_cls='';
																if(data[i].cp_is_admin=="1"){
																	primary_user="checked ";
																	tooltip='data-position="left" data-tooltip="Primary User"';
																	tooltip_cls='tooltipped';
																}
															if(i == 0)
															{
																html_person='<div class="row"><div class="contact-person-box"><div class="col l1 s1 m1"><input type="radio"  class="is_admin" id="contact-person" data-cp_id="'+data[i].cp_id+'" name="is_admin" '+primary_user+'><label class="fill-green-radio '+tooltip_cls+'" for="contact-person" '+tooltip+'></label></div><div class="col l10 s10 m10 content-area"><div class="col l3 s3 m3"><div class="gradient-wrapper"><div class="maincircle">100%</div></div></div><div class="col l9 s9 m9"><p class="name"><strong>'+data[i].cp_name+'</strong></p><p class="email">'+data[i].cp_email+'</p></div></div><div class="col l1 s1 m1"><a class="cur remove_contact_person_editpage modal-trigger" data-cust_id="'+data[i].cust_id+'" data-id="'+data[i].cp_id+'" data-target="remove_contact_person_model_editpage"><i class="material-icons">close</i></a><a href="javascript:void(0);" class="edit_contactmodal"  data-id="'+data[i].cp_id+'" data-cust_id="'+data[i].cust_id+'" data-name="'+data[i].cp_name+'" data-mobile="'+data[i].cp_mobile+'" data-email="'+data[i].cp_email+'" data-password="'+data[i].cp_password+'" data-birthday="'+data[i].cp_birth_date+'" data-birthday_remainder="'+data[i].cp_birth_reminder+'" data-anniversary="'+data[i].cp_anniversary_date+'" data-anniversary_reminder="'+data[i].cp_anniversary_reminder+'" data-is_admin="'+data[i].cp_is_admin+'"><i class="material-icons" style="color: #000;margin-top:0px;">mode_edit</i></a></div></div></div>';

															   }
															   else if(i == 1)
															   {
																  html_person='<div class="row"><div class="contact-person-box"><div class="col l1 s1 m1"><input type="radio"  class="is_admin" data-cp_id="'+data[i].cp_id+'" id="contact-person'+(i+1)+'" name="is_admin" '+primary_user+'><label class="fill-green-radio '+tooltip_cls+'" for="contact-person'+(i+1)+'" '+tooltip+'></label></div><div class="col l10 s10 m10 content-area"><div class="col l3 s3 m3"><div class="gradient-wrapper"><div class="maincircle">100%</div></div></div><div class="col l9 s9 m9"><p class="name"><strong>'+data[i].cp_name+'</strong></p><p class="email">'+data[i].cp_email+'</p></div></div><div class="col l1 s1 m1"><a class="cur remove_contact_person_editpage modal-trigger" data-cust_id="'+data[i].cust_id+'" data-id="'+data[i].cp_id+'" data-target="remove_contact_person_model_editpage"><i class="material-icons">close</i></a><a href="javascript:void(0);" class="edit_contactmodal"  data-id="'+data[i].cp_id+'" data-cust_id="'+data[i].cust_id+'" data-name="'+data[i].cp_name+'" data-mobile="'+data[i].cp_mobile+'" data-email="'+data[i].cp_email+'" data-password="'+data[i].cp_password+'" data-birthday="'+data[i].cp_birth_date+'" data-bicp_rthday_remainder="'+data[i].cp_birth_reminder+'" data-anniversary="'+data[i].cp_anniversary_date+'" data-anniversary_reminder="'+data[i].cp_anniversary_reminder+'" data-is_admin="'+data[i].cp_is_admin+'"><i class="material-icons" style="color: #000;margin-top:0px;">mode_edit</i></a></div></div></div>';

															   }
															   else
															   {
																  html_person='<div class="contactmore"><div class="row"><div class="contact-person-box"><div class="col l1 s1 m1"><input type="radio"  class="is_admin" data-cp_id="'+data[i].cp_id+'" id="contact-person'+(i+1)+'" name="is_admin" '+primary_user+'><label class="fill-green-radio '+tooltip_cls+'" for="contact-person'+(i+1)+'" '+tooltip+'></label></div><div class="col l10 s10 m10 content-area"><div class="col l3 s3 m3"><div class="gradient-wrapper"><div class="maincircle">100%</div></div></div><div class="col l9 s9 m9"><p class="name"><strong>'+data[i].cp_name+'</strong></p><p class="email">'+data[i].cp_email+'</p></div></div><div class="col l1 s1 m1"><a class="cur remove_contact_person_editpage modal-trigger" data-cust_id="'+data[i].cust_id+'" data-id="'+data[i].cp_id+'" data-target="remove_contact_person_model_editpage"><i class="material-icons">close</i></a><a href="javascript:void(0);" class="edit_contactmodal"  data-id="'+data[i].cp_id+'" data-cust_id="'+data[i].cust_id+'" data-name="'+data[i].cp_name+'" data-mobile="'+data[i].cp_mobile+'" data-email="'+data[i].cp_email+'" data-password="'+data[i].cp_password+'" data-birthday="'+data[i].cp_birth_date+'" data-birthday_remainder="'+data[i].cp_birth_reminder+'" data-anniversary="'+data[i].cp_anniversary_date+'" data-anniversary_reminder="'+data[i].cp_anniversary_reminder+'" data-is_admin="'+data[i].cp_is_admin+'"><i class="material-icons" style="color: #000;margin-top:0px;">mode_edit</i></a></div></div></div></div>';

															   }
																 final_html_1 = final_html_1 + html_person;
																 $(".person_list").html(final_html_1);
																 $(".tot_person").html(data.length);
																 $('.tooltipped').tooltip();
															}
															Materialize.toast('Contact Detail has been successfully removed!', 2000,'green rounded');
														}
														else
														{
															html_person='<div class="gst-boxs add-gst">Add Contact Person Data</div>';
															$(".person_list").html(html_person);
															$(".tot_person").html("0");
														}
													}
													else
													{
														html_person='<div class="gst-boxs add-gst">Add Contact Person Data</div>';
														$(".person_list").html(html_person);
														$(".tot_person").html("0");
														Materialize.toast('Error while processing!', 2000,'red rounded');
													}
												},

											});
									});

			$('.add_contactmodal_for_edit').on('click' ,function(){
				var cust_id = $(this).data('cust_id');

				$('#add_contactmodal_for_edit').modal('open');
				$('#add_contactperson_for_edit #editpage_cust_id').val(cust_id);
			});

			$.validator.addMethod("emailexist",
			    function(value, element) {
			    	code = $('#add_contactperson_for_edit #cust_unique_code').val();
			   //  	$.ajax({
						// url:base_url+'sales/check_contactperson_email_exist',
						// type:"POST",
						// data:{"email":value,"client_code":code},
						// success:function(res){

						// 	},
						// });

			    	var response = $.parseJSON($.ajax({
                                                url: base_url+'sales/check_contactperson_email_exist',
                                                type: 'post',
                                                data : {
												'csrf_test_name':csrf_hash,		
                                                'email' : value,
                                                'client_code': code,
                                                'format' : 'json',
                                                },
                                                dataType: "json", 
                                                async: false
                                                }).responseText);

			    	if(response == 1)
			    	{
			    		return false;
			    	
			    	}else{

			    		return true;	
			    	}
			        
			});

			$("#add_contactperson_for_edit").submit(function(e){
				e.preventDefault();
			}).validate({
				rules:{

					new_contact_name:{
						required:true,
					},

					new_contact_email:{
						required:true,
						email:true,
						emailexist:true,
					},
					new_contact_mobile:{
						//mobileno:true,
						required:true,
						digits:true,
					},
					// new_contact_password:{
					// 	required:true,
					// 	checklowercase: true,
					// 	checkuppercase: true,
					// 	checknumber: true,
					// 	checkspecialchar: true,
					// 	minlength:8,
					// 	maxlength:12
					// },
					// new_contact_cpassword:{
					// 	equalTo:'#new_contact_password',
					// 	minlength:8,
					// 	maxlength:12
					// },
				},
				messages:{

					new_contact_name:{
						required:"Name is required",
					},
					new_contact_email:{
						required:"Email ID is required",
						email:'Enter valid Email ID',
						emailexist:"Email Already exist",
					},
					mobile:{
						//mobileno:'Enter 10 Digit Mobile No.',
						required:"Mobile Number is required",
					},
					// new_contact_password:{
					// 	required:"Password is required",
					// 	checklowercase:"At list 1 Lower case",
					// 	checkuppercase:"At list 1 Upper case",
					// 	checknumber:"At list 1 Numberr",
					// 	checkspecialchar:"At list 1 Special Character",
					// 	minlength:"Password must contain at least 8 characters",
					// 	maxlength:"Maximum 12 character long password allowed",
					// },
					// new_contact_cpassword:{
					// 	equalTo:"Password doesn't Match",
					// },
				},
				submitHandler:function(form){
						var frm=$(form).serialize();
								$.ajax({
								url:base_url+'sales/add_contact_per',
								type:"POST",
								data:{'csrf_test_name':csrf_hash,"frm":frm,},
								success:function(res){
									if(res!= false)
									{
										var data=JSON.parse(res);
										var html_person ='';
										var final_html_1 ='';
										if(data != '')
										{
											for(var i=0;i<data.length;i++){
											var primary_user="";
											if(data[i].cp_is_admin=="1"){
												primary_user="checked";
											}
											if(i == 0)
											{
												html_person='<div class="row"><div class="contact-person-box"><div class="col l1 s1 m1"><input type="radio" id="contact-person"  name="is_admin" class="is_admin" data-cp_id="'+data[i].cp_id+'" '+primary_user+'><label class="fill-green-radio" for="contact-person"></label></div><div class="col l10 s10 m10 content-area"><div class="col l3 s3 m3"><div class="gradient-wrapper"><div class="maincircle">100%</div></div></div><div class="col l9 s9 m9"><p class="name"><strong>'+data[i].cp_name+'</strong></p><p class="email">'+data[i].cp_email+'</p></div></div><div class="col l1 s1 m1"><a class="cur remove_contact_person_editpage modal-trigger" data-cust_id="'+data[i].cust_id+'" data-id="'+data[i].cp_id+'" data-target="remove_contact_person_model_editpage"><i class="material-icons">close</i></a><a href="javascript:void(0);" class="edit_contactmodal"  data-id="'+data[i].cp_id+'" data-cust_id="'+data[i].cust_id+'" data-name="'+data[i].cp_name+'" data-mobile="'+data[i].cp_mobile+'" data-email="'+data[i].cp_email+'" data-password="'+data[i].cp_password+'" data-birthday="'+data[i].cp_birth_date+'" data-birthday_remainder="'+data[i].cp_birth_reminder+'" data-anniversary="'+data[i].cp_anniversary_date+'" data-anniversary_reminder="'+data[i].cp_anniversary_reminder+'" data-is_admin="'+data[i].cp_is_admin+'"><i class="material-icons edit-contact" style="color: #000;margin-top:0px;">mode_edit</i></a></div></div></div>';

											   }
											   else if(i == 1)
											   {
												  html_person='<div class="row"><div class="contact-person-box"><div class="col l1 s1 m1"><input type="radio" id="contact-person"  name="is_admin" class="is_admin" data-cp_id="'+data[i].cp_id+'" '+primary_user+'><label class="fill-green-radio" for="contact-person"></label></div><div class="col l10 s10 m10 content-area"><div class="col l3 s3 m3"><div class="gradient-wrapper"><div class="maincircle">100%</div></div></div><div class="col l9 s9 m9"><p class="name"><strong>'+data[i].cp_name+'</strong></p><p class="email">'+data[i].cp_email+'</p></div></div><div class="col l1 s1 m1"><a class="cur remove_contact_person_editpage modal-trigger" data-cust_id="'+data[i].cust_id+'" data-id="'+data[i].cp_id+'" data-target="remove_contact_person_model_editpage"><i class="material-icons">close</i></a><a href="javascript:void(0);" class="edit_contactmodal"  data-id="'+data[i].cp_id+'" data-cust_id="'+data[i].cust_id+'" data-name="'+data[i].cp_name+'" data-mobile="'+data[i].cp_mobile+'" data-email="'+data[i].cp_email+'" data-password="'+data[i].cp_password+'" data-birthday="'+data[i].cp_birth_date+'" data-birthday_remainder="'+data[i].cp_birth_reminder+'" data-anniversary="'+data[i].cp_anniversary_date+'" data-anniversary_reminder="'+data[i].cp_anniversary_reminder+'" data-is_admin="'+data[i].cp_is_admin+'"><i class="material-icons edit-contact" style="color: #000;margin-top:0px;">mode_edit</i></a></div></div></div>';

											   }
											   else
											   {
												 html_person='<div class="row"><div class="contact-person-box"><div class="col l1 s1 m1"><input type="radio" id="contact-person'+(i+1)+'"  name="is_admin" class="is_admin" data-cp_id="'+data[i].cp_id+'" '+primary_user+'><label class="fill-green-radio" for="contact-person'+(i+1)+'"></label></div><div class="col l10 s10 m10 content-area"><div class="col l3 s3 m3"><div class="gradient-wrapper"><div class="maincircle">100%</div></div></div><div class="col l9 s9 m9"><p class="name"><strong>'+data[i].cp_name+'</strong></p><p class="email">'+data[i].cp_email+'</p></div></div><div class="col l1 s1 m1"><a class="cur remove_contact_person_editpage modal-trigger" data-cust_id="'+data[i].cust_id+'" data-id="'+data[i].cp_id+'" data-target="remove_contact_person_model_editpage"><i class="material-icons">close</i></a><a href="javascript:void(0);" class="edit_contactmodal"  data-id="'+data[i].cp_id+'" data-cust_id="'+data[i].cust_id+'" data-name="'+data[i].cp_name+'" data-mobile="'+data[i].cp_mobile+'" data-email="'+data[i].cp_email+'" data-password="'+data[i].cp_password+'" data-birthday="'+data[i].cp_birth_date+'" data-birthday_remainder="'+data[i].cp_birth_reminder+'" data-anniversary="'+data[i].cp_anniversary_date+'" data-anniversary_reminder="'+data[i].cp_anniversary_reminder+'" data-is_admin="'+data[i].cp_is_admin+'"><i class="material-icons edit-contact" style="color: #000;margin-top:0px;">mode_edit</i></a></div></div></div>';
											   }
												 final_html_1 = final_html_1 + html_person;
												 $(".person_list").html(final_html_1);
												 $(".tot_person").html(data.length);
											}
											Materialize.toast('Contact Details successfully inserted!', 2000,'green rounded');
										}
										else
										{
											html_person='<div class="gst-boxs add-gst">Add Contact Person Data</div>';
											$(".person_list").html(html_person);
										}
									}
									else
									{
										Materialize.toast('Error while processing. Please try again', 2000,'red rounded');
									}
									$("#add_contactperson_for_edit").find("input[type=text],input[type=password], textarea").val("");
									$("#add_contactmodal_for_edit").modal('close');
							},

					});
				},
			});

			$(document).ready( function () {
				$(document).on('click', '.delete_legal_doc',function(){
						var legal_id = $(this).data('legal_id');

						$('#remove_cust_legal_doc').modal('open');
						$('#remove_cust_legal_doc #cust_legal_doc_id').val(legal_id);
					});
					$('.remove_legal_doc').on('click',function(){

					var cust_legal_doc_id = $('#cust_legal_doc_id').val();
					$.ajax({
						url:base_url+'sales/remove_legal_document',
						type:"POST",
						data:{'csrf_test_name':csrf_hash,"cust_legal_doc_id":cust_legal_doc_id,},
						success:function(res){
								 $('#legaldoc'+cust_legal_doc_id).animate({'line-height':0},500).hide(1);
							},
						});
					});


			$(document).on('click', '.delete_po_doc' ,function(){
				var po_id = $(this).data('po_id');

				$('#remove_cust_po_doc').modal('open');
				$('#remove_cust_po_doc #cust_po_doc_id').val(po_id);
			});

			$('.remove_po_doc').on('click',function(){
				var cust_po_doc_id = $('#cust_po_doc_id').val();
				$.ajax({
					url:base_url+'sales/remove_legal_document',
					type:"POST",
					data:{'csrf_test_name':csrf_hash,"cust_legal_doc_id":cust_po_doc_id,},
					success:function(res){
							$('#podoc'+cust_po_doc_id).animate({'line-height':0},1000).hide(1);
						},
					});
			});

			$(document).on('click', '.delete_other_doc' ,function(){
				var other_id = $(this).data('other_id');

				$('#remove_cust_other_doc').modal('open');
				$('#remove_cust_other_doc #cust_other_doc_id').val(other_id);
			});

				$('.remove_other_doc').on('click',function(){

					var cust_other_doc_id = $('#cust_other_doc_id').val();
					$.ajax({
						url:base_url+'sales/remove_other_document',
						type:"POST",
						data:{'csrf_test_name':csrf_hash,"cust_other_doc_id":cust_other_doc_id,},
						success:function(res){
								$('#otherdoc'+cust_other_doc_id).animate({'line-height':0},1000).hide(1);
							},
						});
				});
			});

		$("#bank_details").submit(function(e){
				e.preventDefault();
			}).validate({
				rules:{
					bank_name:{
						required:true,
					},
					ac_number:{
						required:true,
						digits:true,
					},
					acc_type:{
						required:true,
					},
					ifsc_code:{
						required:true,
						ifscno:true,
					},
					bus_bank_country:{
						required:true,
					},
					bank_cur_check:{
						required:true,
					},
					opening_bank_balance:{
						required:true,
					},
					opening_balance_date:{
						required:true,
					},

				},
				messages:{
					bank_name:{
						required:"Bank Name is required",
					},
					ac_number:{
						required:"Bank Account Number is required",
					},
					acc_type:{
						required:"Account type is required",
					},
					bus_bank_country:{
						required:"Country is required",
					},
					ifsc_code:{
						required:"IFSC is required",
						ifscno:"Invalid IFSC Code",
					},
					bank_cur_check:{
						required:"Currency is required",
					},
					opening_bank_balance:{
						required:"Opening Bank Balance is required",
					},
					opening_balance_date:{
						required:"Bank Balance Date is required",
					},
					
				},
				submitHandler:function(form){
								var frm=$(form).serialize();
								 //$(form).ajaxSubmit();
								if(csrf_hash===""){
									csrf_hash=csrf_hash;
								}
								$.ajax({
								url:base_url+'profile/add_bank_info',
								type:"POST",
								data:{'csrf_test_name':csrf_hash,"frm":frm,},
								success:function(res){
									if(res!= false)
									{
										var data=JSON.parse(res);
										if(data['csrf_hash']){
											csrf_hash=data['csrf_hash'];
										}
										//console.log(data.length);
										var html_1 ='';
										var final_html_1 ='';
										if(data != '')
										{

											var i=0;
											for(i=0;i<data.length;i++){
                                               if(data[i].status=="Inactive"){
													   var deac="deactive_record";
												   }else{
													    var deac="";
												   }
											if(i == 0)
											   {
												html_1='<div class="gst-boxs add-gst '+deac+'"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">ENTER BANK ACCOUNT NUMBER</span><p>'+data[i].cbank_account_no+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">BANK NAME</span><p>'+data[i].cbank_name+'</p></div><div class="col l1 s1 m1 gstaction"><a class="remove_bank_detail modal-trigger" data-target="remove_bank_modal" data-acc_no="'+data[i].cbank_account_no+'" data-cbank_name="'+data[i].cbank_name+'"data-cbank_branch_name="'+data[i].cbank_branch_name+'" data-cbank_account_type="'+data[i].cbank_account_type+'" data-cbank_ifsc_code="'+data[i].cbank_ifsc_code+'" ><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a></div></div></div>';
											   }
											   else
											   {

												 html_1='<div class="bankmore"><div class="gst-boxs add-gst '+deac+'"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum "><span class="gstlabel">ENTER BANK ACCOUNT NUMBER</span><p>'+data[i].cbank_account_no+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">BANK NAME</span><p>'+data[i].cbank_name+'</p></div><div class="col l1 s1 m1 gstaction"><a class="remove_bank_detail modal-trigger" data-target="remove_bank_modal" data-acc_no="'+data[i].cbank_account_no+'" data-cbank_name="'+data[i].cbank_name+'"data-cbank_branch_name="'+data[i].cbank_branch_name+'" data-cbank_account_type="'+data[i].cbank_account_type+'" data-cbank_ifsc_code="'+data[i].cbank_ifsc_code+'" ><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a></div></div></div></div>';

											   }

												 final_html_1 = final_html_1 + html_1;
												 $("#company_bank_list").html(final_html_1);
												 $(".tot_bank").html(data.length);
											}
											Materialize.toast('Bank details updated successfully', 2000,'green rounded');;
										}
										else
										{
											Materialize.toast('Bank details updated successfully', 2000,'green rounded');;
											html_1='<div class="gst-boxs add-gst">Add Bank Data</div>';
											$("#company_bank_list").html(html_1);
										}
									}
									else
									{
										Materialize.toast('Error while processing. Please try again', 2000,'red rounded');
									}
									$("#addbranch").find("input[type=text]").val("");
									$("#addbranch").modal('close');
									$("#editbranch").modal('close');

									},

					});
				},
			});



			$("#assets_bank_details").submit(function(e){
				e.preventDefault();
			}).validate({
				rules:{
					bank_name:{
						required:true,
					},
					acc_number:{
						required:true,
						digits:true,
					},
					ifsc_code:{

						ifscno:true,
					},

				},
				messages:{
					bank_name:{
						required:"Bank Name is required",
					},
					acc_number:{
						required:"Bank Account Number is required",
					},
					ifsc_code:{
						ifscno:"Invalid IFSC Code",
					},

				},
				submitHandler:function(form){
						var frm=$(form).serialize();
								 //$(form).ajaxSubmit();
								$.ajax({
								url:base_url+'Assets/add_bank_info',
								type:"POST",
								data:{'csrf_test_name':csrf_hash,"frm":frm,},
								success:function(res){
									if(res!= false)
									{
										var data=JSON.parse(res);
										//console.log(data.length);
										var html_1 ='';
										var final_html_1 ='';
										if(data != '')
										{

											var i=0;
											for(i=0;i<data.length;i++){
                                               if(data[i].status=="Inactive"){
													   var deac="deactive_record";
												   }else{
													    var deac="";
												   }
											if(i == 0)
											   {
												html_1='<div class="gst-boxs add-gst '+deac+'"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum '+deac+'"><span class="gstlabel">ENTER BANK ACCOUNT NUMBER</span><p>'+data[i].cbank_account_no+'</p></div><div class="col l6 s6 m6 gstplace '+deac+'"><span class="gstlabel">BANK NAME</span><p>'+data[i].cbank_name+'</p></div><div class="col l1 s1 m1 gstaction"><a class="remove_bank_detail modal-trigger '+deac+'" data-target="remove_bank_modal" data-acc_no="'+data[i].cbank_account_no+'" data-cbank_name="'+data[i].cbank_name+'"data-cbank_branch_name="'+data[i].cbank_branch_name+'" data-cbank_account_type="'+data[i].cbank_account_type+'" data-cbank_ifsc_code="'+data[i].cbank_ifsc_code+'" ><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a></div></div></div>';
											   }
											   else
											   {

												 html_1='<div class="bankmore"><div class="gst-boxs add-gst '+deac+'"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum '+deac+'"><span class="gstlabel">ENTER BANK ACCOUNT NUMBER</span><p>'+data[i].cbank_account_no+'</p></div><div class="col l6 s6 m6 gstplace '+deac+'"><span class="gstlabel ">BANK NAME</span><p>'+data[i].cbank_name+'</p></div><div class="col l1 s1 m1 gstaction"><a class="remove_bank_detail modal-trigger '+deac+'" data-target="remove_bank_modal" data-acc_no="'+data[i].cbank_account_no+'" data-cbank_name="'+data[i].cbank_name+'"data-cbank_branch_name="'+data[i].cbank_branch_name+'" data-cbank_account_type="'+data[i].cbank_account_type+'" data-cbank_ifsc_code="'+data[i].cbank_ifsc_code+'" ><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a></div></div></div></div>';

											   }

												 final_html_1 = final_html_1 + html_1;
												 $("#asset_bank_list").html(final_html_1);
												 $(".tot_bank").html(data.length);
											}
											Materialize.toast('Bank details updated successfully', 2000,'green rounded');;
										}
										else
										{
											Materialize.toast('Bank details updated successfully', 2000,'green rounded');;
											html_1='<div class="gst-boxs add-gst">Add Bank Data</div>';
											$("#asset_bank_list").html(html_1);
										}
									}
									else
									{
										Materialize.toast('Error while processing. Please try again', 2000,'red rounded');
									}
									$("#addbranch").find("input[type=text]").val("");
									$("#addbranch").modal('close');
									$("#editbranch").modal('close');

									},

					});
				},
			});



			$('#asset_bank_list').off().on('click','.remove_bank_detail',function(){

					$('#remove_bank_modal #remove_ac_number').val($('.remove_bank_detail').data('acc_no'));
					$('#remove_bank_modal #remove_cbank_name').val($('.remove_bank_detail').data('cbank_name'));
					$('#remove_bank_modal #remove_cbank_branch_name').val($('.remove_bank_detail').data('cbank_branch_name'));
					$('#remove_bank_modal #remove_account_type').val($('.remove_bank_detail').data('cbank_account_type'));
					$('#remove_bank_modal #remove_ifsc_code').val($('.remove_bank_detail').data('cbank_ifsc_code'));
			});

				$('.remove_bank_details_asset').off().on('click',function(){
				var ac_no = $('#remove_ac_number').val();
				var cbank = $('#remove_cbank_name').val();
				var branch = $('#remove_cbank_branch_name').val();
				var acc_type = $('#remove_account_type').val();
				var ifsc = $('#remove_ifsc_code').val();

				$.ajax({

					url:base_url+'Assets/remove_bank_details',

					type:"post",

					data:{'csrf_test_name':csrf_hash,"ac_no":ac_no,"cbank":cbank,"branch":branch,"acc_type":acc_type,"ifsc":ifsc},
					success:function(res){
						if(res!= false)
							{
								var data=JSON.parse(res);
								var html_1 ='';
								var final_html_1 ='';
								if(data != '')
								{
									$("#asset_bank_list").html("");
									for(var i=0;i<data.length;i++){
                                                   
												if(data[i].status=="Inactive"){
													   var deac="deactive_record";
												   }else{
													    var deac="";
												   }
												   
										if(i == 0)
											   {
												html_1='<div class="gst-boxs add-gst '+deac+'"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum '+deac+'"><span class="gstlabel">ENTER BANK ACCOUNT NUMBER</span><p>'+data[i].cbank_account_no+'</p></div><div class="col l6 s6 m6 gstplace '+deac+'"><span class="gstlabel">BANK NAME</span><p>'+data[i].cbank_name+'</p></div><div class="col l1 s1 m1 gstaction"><a class="remove_bank_detail modal-trigger '+deac+'" data-target="remove_bank_modal" data-acc_no="'+data[i].cbank_account_no+'" data-cbank_name="'+data[i].cbank_name+'"data-cbank_branch_name="'+data[i].cbank_branch_name+'" data-cbank_account_type="'+data[i].cbank_account_type+'" data-cbank_ifsc_code="'+data[i].cbank_ifsc_code+'" ><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a></div></div></div>';
											   }
											   else
											   {

												 html_1='<div class="bankmore"><div class="gst-boxs add-gst '+deac+'"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum '+deac+'"><span class="gstlabel">ENTER BANK ACCOUNT NUMBER</span><p>'+data[i].cbank_account_no+'</p></div><div class="col l6 s6 m6 gstplace '+deac+'"><span class="gstlabel">BANK NAME</span><p>'+data[i].cbank_name+'</p></div><div class="col l1 s1 m1 gstaction "><a class="remove_bank_detail modal-trigger '+deac+'" data-target="remove_bank_modal" data-acc_no="'+data[i].cbank_account_no+'" data-cbank_name="'+data[i].cbank_name+'"data-cbank_branch_name="'+data[i].cbank_branch_name+'" data-cbank_account_type="'+data[i].cbank_account_type+'" data-cbank_ifsc_code="'+data[i].cbank_ifsc_code+'" ><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a></div></div></div></div>';

											   }
										     final_html_1 = final_html_1 + html_1;
											 $("#asset_bank_list").html(final_html_1);
											 $('#showbankboxs i').html('keyboard_arrow_down');
											 $(".tot_bank").html(data.length);
										}
										Materialize.toast('Bank details deactivated', 2000,'green rounded');
									}
									else
									{
										Materialize.toast('Bank details deactivated', 2000,'green rounded');
										html_1='<div class="gst-boxs add-gst">Add Bank Data</div>';
										$("#asset_bank_list").html(html_1);
										$(".tot_bank").html(0);
									}
								}
								else
								{
									html_1='<div class="gst-boxs add-gst">Add Bank Data</div>';
									$("#asset_bank_list").html(html_1);
									$(".tot_bank").html(0);
								}
						},

					});
				});


				$('.edited_bank_removed_asset').off().on('click',function(){

					$('#removed_existing_bank #remove_cbank_id').val($(this).data('cbank_id'));
					$('#removed_existing_bank #remove_bus_id').val($(this).data('bus_id'));
					$('#removed_existing_bank #remove_asset_id').val($(this).data('asset_id'));

			});
			$(".removed_existing_bank_data_asset").click(function(e){

					var cbank_id=$('#removed_existing_bank #remove_cbank_id').val();
					var bus_id= $('#removed_existing_bank #remove_bus_id').val();
					var asset_id= $('#removed_existing_bank #remove_asset_id').val();
						//console.log(frm);
								$.ajax({
								url:base_url+'Assets/delete_bank_info',
								type:"POST",
								data:{'csrf_test_name':csrf_hash,"bus_id":bus_id,"cbank_id":cbank_id,'asset_id':asset_id},
								success:function(res){
									console.log(res);
										if(res!= false)
										{
										var data=JSON.parse(res);
										//console.log(data.length);
										var html_1 ='';
										var final_html_1 ='';
										if(data != '')
										{

											var i=0;
											for(i=0;i<data.length;i++){
                                             	//alert(data[i].status);
												if(data[i].status=="Inactive"){
													   var deac="deactive_record";
												   }else{
													    var deac="";
												   }
												   console.log(deac);
											if(i == 0)
											   {

												html_1='<div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum '+deac+'"><span class="gstlabel">ENTER BANK ACCOUNT NUMBER </span><p>'+data[i].cbank_account_no+'</p></div><div class="col l6 s6 m6 gstplace '+deac+'"><span class="gstlabel">BANK NAME</span><p>'+data[i].cbank_name+'</p></div><div class="col l1 s1 m1 gstaction"><a class="edited_bank_removed_asset modal-trigger '+deac+'" data-target="removed_existing_bank" data-cbank_id="'+data[i].cbank_id+'" data-asset_id="'+data[i].asset_id+'" data-bus_id="'+data[i].bus_id+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a><a class="edit_bankinfo_asset modal-trigger" data-target="edit_bank_branch_asset" data-acc_no="'+data[i].cbank_account_no+'" data-asset_id="'+data[i].asset_id+'" data-cbank_name="'+data[i].cbank_name+'"data-cbank_branch_name="'+data[i].cbank_branch_name+'" data-cbank_account_type="'+data[i].cbank_account_type+'" data-cbank_ifsc_code="'+data[i].cbank_ifsc_code+'" data-cbank_id="'+data[i].cbank_id+'" data-country_id="'+data[i].country_id+'" data-country_name="'+data[i].country_name+'" data-cbank_currencycode="'+data[i].currency+'" data-cbank_swift_code="'+data[i].cbank_swift_code+'"            data-bus_id="'+data[i].bus_id+'" data-opening_balance="'+data[i].opening_bank_balance+'" data-opening_balance_date="'+data[i].opening_balance_date+'" data-status="'+data[i].status+'" ><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div>';
											   }
											   else
											   {

												 html_1='<div class="bankmore"><div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum '+deac+'"><span class="gstlabel">ENTER BANK ACCOUNT NUMBER </span><p>'+data[i].cbank_account_no+'</p></div><div class="col l6 s6 m6 gstplace '+deac+'"><span class="gstlabel">BANK NAME</span><p>'+data[i].cbank_name+'</p></div><div class="col l1 s1 m1 gstaction"><a class="edited_bank_removed_asset modal-trigger '+deac+'" data-target="removed_existing_bank" data-cbank_id="'+data[i].cbank_id+'" data-asset_id="'+data[i].asset_id+'" data-bus_id="'+data[i].bus_id+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a><a class="edit_bankinfo_asset modal-trigger" data-target="edit_bank_branch_asset" data-acc_no="'+data[i].cbank_account_no+'" data-cbank_name="'+data[i].cbank_name+'"data-cbank_branch_name="'+data[i].cbank_branch_name+'" data-cbank_account_type="'+data[i].cbank_account_type+'" data-cbank_ifsc_code="'+data[i].cbank_ifsc_code+'" data-cbank_id="'+data[i].cbank_id+'" data-asset_id="'+data[i].asset_id+'" data-country_id="'+data[i].country_id+'" data-country_name="'+data[i].country_name+'" data-cbank_currencycode="'+data[i].currency+'" data-cbank_swift_code="'+data[i].cbank_swift_code+'              " data-bus_id="'+data[i].bus_id+'" data-opening_balance="'+data[i].opening_bank_balance+'" data-opening_balance_date="'+data[i].opening_balance_date+'" data-status="'+data[i].status+'"><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div></div>';

											   }

												 final_html_1 = final_html_1 + html_1;
												 $("#edit_asset_bank_list").html(final_html_1);
												 $(".tot_bank").html(data.length);
											}
											Materialize.toast('Bank details deactivated', 2000,'green rounded');
										}
										else
										{
											Materialize.toast('Bank details deactivated', 2000,'green rounded');
											html_1='<div class="gst-boxs add-gst">Add Bank Data</div>';
											$("#edit_asset_bank_list").html(html_1);
										}
									}
									else
									{
										Materialize.toast('Error while processing. Please try again', 2000,'red rounded');
									}
									$("#edit_bank_details_asset").find("input[type=text]").val("");
									$("#edit_bank_branch_asset").modal('close');
									$('#showbankboxs i').html('keyboard_arrow_down');
									},
					});

			});


         $('#edit_asset_bank_list').on('click','.edit_bankinfo_asset',function(){

				$("#edit_bank_branch_asset").find("input[type=text]").val("");
				var bank_name = $(this).data('cbank_name');
				var bank_branch_name = $(this).data('cbank_branch_name');
				var account_type = $(this).data('cbank_account_type');
				var ifsc_code = $(this).data('cbank_ifsc_code');
				var swift_code = $(this).data('cbank_swift_code');
				var bank_account_no = $(this).data('acc_no');
				var cmpid = $(this).data('bus_id');
				var asset_id= $(this).data('asset_id');
				var cbank_id = $(this).data('cbank_id');
				var country_name = $(this).data('country_name');
				var cbank_country_code = $(this).data('country_id');
				var currency_id = $(this).data('cbank_currency_code');
				var currency = $(this).data('cbank_currency');
				var currencycode = $(this).data('cbank_currencycode');
				var cbank_swift_code = $(this).data('cbank_swift_code');
				var opening_bank_balance = $(this).data('opening_balance');
				var opening_balance_date = $(this).data('opening_balance_date');
				var status = $(this).data('status');
				
				var date=new Date(opening_balance_date);
				var day = date.getDate();
        var month = date.getMonth();
        var year = date.getFullYear();

        var dd=day+'-'+month+'-'+year;
				
    
				$('#edit_bank_branch_asset #edit_ac_number,#edit_bank_branch #edit_bank_name,#swift_code,#edit_bank_branch #edit_bank_branch_name,#edit_bank_branch #ifsc_code,#edit_bank_branch, #edit_acc_type, #bus_bank_country_change,#opening_bank_balance,#opening_balance_date,#status').parents('.input-field').children().addClass('active');
				$('#edit_bank_branch_asset #edit_ac_number').val(bank_account_no);
				$('#edit_bank_branch_asset #edit_bank_name').val(bank_name);
				$('#edit_bank_branch_asset #edit_bank_branch_name').val(bank_branch_name);
				$(".asbnk").addClass('active');	
				$('#edit_bank_branch_asset #ifsc_code').val(ifsc_code);
				$('#edit_bank_branch_asset #swift_code').val(swift_code);
				$('#edit_bank_branch_asset #edit_acc_type option[value="'+account_type+'"]').attr('selected','selected');
				$('#edit_bank_branch_asset #edit_cbank_id').val(cbank_id);
				$('#edit_bank_branch_asset #edit_cmpid').val(cmpid);
				$('#edit_bank_branch_asset #edit_asset_id').val(asset_id);
				$('#edit_bank_branch_asset #bus_bank_country_change option[value="'+cbank_country_code+'"]').attr('selected','selected');
				$('#edit_bank_branch_asset #bank_cur_check_change option[value="'+currency_id+'"]').attr('selected','selected');
				$('#edit_bank_branch_asset #opening_bank_balance').val(opening_bank_balance);
				$('#edit_bank_branch_asset #opening_balance_date').val(dd);
					$('#edit_bank_branch_asset #status option[value="'+status+'"]').attr('selected','selected');

				$('#edit_bank_branch_asset #edit_acc_type').material_select();
				$('#edit_bank_branch_asset #bus_bank_country_change').material_select();
				$('#edit_bank_branch_asset #bank_cur_check_change').material_select();
				$('#edit_bank_branch_asset #status').material_select();
				$(".asbnk").addClass('active');	
				// res = '';
				// $("#ifscORswift_change").html(res);

				// if(cbank_country_code == 101)
				// {
				// 	alert(ifsc_code);
				//   //res = '<label class="full-bg-label" for="ifsc_code">Enter IFS oC Code<span class="required_field">*</span></label> <input id="ifsc_code" name="ifsc_code" class="full-bg adjust-width" type="text">';
				//   res = '<label class="full-bg-label" for="ifsc_code">Enter IFSC Code<span class="required_field">*</span></label><input id="ifsc_code" name="ifsc_code" value="'+ifsc_code+'" class="full-bg adjust-width" type="text">';
				//   $("#ifscORswift_change").html(res);

				// }else{
				// 	alert(cbank_swift_code);
				//   res = '<label class="full-bg-label" for="swift_code">Enter Swift Code<span class="required_field">*</span></label> <input id="swift_code" name="swift_code" value="'+cbank_swift_code+'" class="full-bg adjust-width" type="text">';
				//   $("#ifscORswift_change").html(res);
				// }

			});

			$("#edit_bank_details_asset").submit(function(e){
				e.preventDefault();
			}).validate({
				rules:{
					edit_bank_name:{
						required:true,
					},
					edit_ac_number:{
						required:true,
						digits:true,
					},
					ifsc_code:{

						ifscno:true,
					},
					opening_bank_balance:{
						required:true,
						digits:true,
					},
					opening_balance_date:{
						required:true,
						
					},
				},
				messages:{
					edit_bank_name:{
						required:"Bank Name is required",
					},
					edit_ac_number:{
						required:"Bank Account Number is required",
					},
					ifsc_code:{
						ifscno:"Invalid IFSC Code",
					},

					opening_bank_balance:{
						required:"Opening Bank Balance is required",
					},

					opening_balance_date:{
						required:"Bank Balance Date is required",
					},

				},
				submitHandler:function(form){
						var frm=$(form).serialize();
						//console.log(frm);
								$.ajax({
								url:base_url+'Assets/edit_bank_info',
								type:"POST",
								data:{'csrf_test_name':csrf_hash,"frm":frm},
								success:function(res){

										if(res!= false)
										{
										var data=JSON.parse(res);
										//console.log(data.length);
										var html_1 ='';
										var final_html_1 ='';
										if(data != '')
										{

											var i=0;
											for(i=0;i<data.length;i++){

												if(data[i].status=="Inactive"){
													   var deac="deactive_record";
												   }else{
													    var deac="";
												   }

											if(i == 0)
											   {
												html_1='<div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum '+deac+'"><span class="gstlabel">ENTER BANK ACCOUNT NUMBER </span><p>'+data[i].cbank_account_no+'</p></div><div class="col l6 s6 m6 gstplace '+deac+'"><span class="gstlabel">BANK NAME</span><p>'+data[i].cbank_name+'</p></div><div class="col l1 s1 m1 gstaction"><a class="edited_bank_removed_asset modal-trigger '+deac+'" data-target="removed_existing_bank" data-cbank_id="'+data[i].cbank_id+'" data-asset_id="'+data[i].asset_id+'" data-bus_id="'+data[i].bus_id+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a><a class="edit_bankinfo_asset modal-trigger" data-target="edit_bank_branch_asset" data-acc_no="'+data[i].cbank_account_no+'" data-asset_id="'+data[i].asset_id+'" data-cbank_name="'+data[i].cbank_name+'"data-cbank_branch_name="'+data[i].cbank_branch_name+'" data-cbank_account_type="'+data[i].cbank_account_type+'" data-cbank_ifsc_code="'+data[i].cbank_ifsc_code+'" data-cbank_id="'+data[i].cbank_id+'" data-country_id="'+data[i].country_id+'" data-country_name="'+data[i].country_name+'" data-cbank_currencycode="'+data[i].currency+'" data-cbank_swift_code="'+data[i].cbank_swift_code+'"            data-bus_id="'+data[i].bus_id+'" data-opening_balance="'+data[i].opening_bank_balance+'" data-opening_balance_date="'+data[i].opening_balance_date+'" data-status="'+data[i].status+'" ><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div>';
											   }
											   else
											   {

												 html_1='<div class="bankmore"><div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum '+deac+'"><span class="gstlabel">ENTER BANK ACCOUNT NUMBER </span><p>'+data[i].cbank_account_no+'</p></div><div class="col l6 s6 m6 gstplace '+deac+'"><span class="gstlabel">BANK NAME</span><p>'+data[i].cbank_name+'</p></div><div class="col l1 s1 m1 gstaction"><a class="edited_bank_removed_asset modal-trigger '+deac+'" data-target="removed_existing_bank" data-cbank_id="'+data[i].cbank_id+'" data-asset_id="'+data[i].asset_id+'" data-bus_id="'+data[i].bus_id+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a><a class="edit_bankinfo_asset modal-trigger" data-target="edit_bank_branch_asset" data-acc_no="'+data[i].cbank_account_no+'" data-cbank_name="'+data[i].cbank_name+'"data-cbank_branch_name="'+data[i].cbank_branch_name+'" data-cbank_account_type="'+data[i].cbank_account_type+'" data-cbank_ifsc_code="'+data[i].cbank_ifsc_code+'" data-cbank_id="'+data[i].cbank_id+'" data-asset_id="'+data[i].asset_id+'" data-country_id="'+data[i].country_id+'" data-country_name="'+data[i].country_name+'" data-cbank_currencycode="'+data[i].currency+'" data-cbank_swift_code="'+data[i].cbank_swift_code+'              " data-bus_id="'+data[i].bus_id+'" data-opening_balance="'+data[i].opening_bank_balance+'" data-opening_balance_date="'+data[i].opening_balance_date+'" data-status="'+data[i].status+'"><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div></div>';

											   }

												 final_html_1 = final_html_1 + html_1;
												 $("#edit_asset_bank_list").html(final_html_1);
												 $(".tot_bank").html(data.length);
											}
											Materialize.toast('Bank details updated successfully', 2000,'green rounded');
										}
										else
										{
											Materialize.toast('Bank details updated successfully', 2000,'green rounded');
											html_1='<div class="gst-boxs add-gst">Add Bank Data</div>';
											$("#edit_asset_bank_list").html(html_1);
										}
									}
									else
									{
										Materialize.toast('Error while processing. Please try again', 2000,'red rounded');
									}
									$("#edit_bank_details_asset").find("input[type=text]").val("");
									$("#edit_bank_branch_asset").modal('close');
									$('#showbankboxs i').html('keyboard_arrow_down');
									},
					});
				},
			});



	$('.add_new_bank_asset').off().on('click',function(){

					$('#add_bank_branch_asset #bank_bus_id').val($(this).data('bus_id'));
					$('#add_bank_branch_asset #bank_asset_id').val($(this).data('asset_id'));
			});
			$("#add_bank_details_asset").submit(function(e){
				e.preventDefault();
			}).validate({
				rules:{
					
					
				},
				messages:{
					

				
				},
				submitHandler:function(form){
						var frm=$(form).serialize();
								 //$(form).ajaxSubmit();
								$.ajax({
								url:base_url+'Assets/add_new_bank',
								type:"POST",
								data:{'csrf_test_name':csrf_hash,"frm":frm,},
								success:function(res){
									if(res!= false)
									{
										var data=JSON.parse(res);
										//console.log(data.length);
										var html_1 ='';
										var final_html_1 ='';
										if(data != '')
										{

											var i=0;
											for(i=0;i<data.length;i++){
                                              // alert(data[i].status);
												if(data[i].status=="Inactive"){
													   var deac="deactive_record";
												   }else{
													    var deac="";
												   }
												   console.log(deac);

											if(i == 0)
											   {
												html_1='<div class="gst-boxs add-gst "><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum '+deac+'"><span class="gstlabel">ENTER BANK ACCOUNT NUMBER</span><p>'+data[i].cbank_account_no+'</p></div><div class="col l6 s6 m6 gstplace '+deac+'"><span class="gstlabel">BANK NAME</span><p>'+data[i].cbank_name+'</p></div><div class="col l1 s1 m1 gstaction"><a class="edited_bank_removed modal-trigger '+deac+'" data-target="removed_existing_bank" data-cbank_id="'+data[i].cbank_id+'" data-bus_id="'+data[i].bus_id+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a><a class="edit_bankinfo modal-trigger" data-target="edit_bank_branch" data-acc_no="'+data[i].cbank_account_no+'" data-cbank_name="'+data[i].cbank_name+'"data-cbank_branch_name="'+data[i].cbank_branch_name+'" data-cbank_account_type="'+data[i].cbank_account_type+'" data-cbank_swift_code="'+data[i].cbank_swift_code+'" data-cbank_ifsc_code="'+data[i].cbank_ifsc_code+'" data-cbank_id="'+data[i].cbank_id+'" data-bus_id="'+data[i].bus_id+'"  data-status="'+data[i].status+'" ><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div>';
											   }
											   else
											   {

												 html_1='<div class="bankmore"><div class="gst-boxs add-gst "><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum '+deac+'"><span class="gstlabel">ENTER BANK ACCOUNT NUMBER</span><p>'+data[i].cbank_account_no+'</p></div><div class="col l6 s6 m6 gstplace '+deac+'"><span class="gstlabel">BANK NAME</span><p>'+data[i].cbank_name+'</p></div><div class="col l1 s1 m1 gstaction"><a class="edited_bank_removed modal-trigger '+deac+'" data-target="removed_existing_bank" data-cbank_id="'+data[i].cbank_id+'" data-bus_id="'+data[i].bus_id+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a><a class="edit_bankinfo modal-trigger" data-target="edit_bank_branch" data-acc_no="'+data[i].cbank_account_no+'" data-cbank_name="'+data[i].cbank_name+'"data-cbank_branch_name="'+data[i].cbank_branch_name+'" data-cbank_account_type="'+data[i].cbank_account_type+'" data-cbank_swift_code="'+data[i].cbank_swift_code+'" data-cbank_ifsc_code="'+data[i].cbank_ifsc_code+'" data-cbank_id="'+data[i].cbank_id+'" data-bus_id="'+data[i].bus_id+'"  data-status="'+data[i].status+'"><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div></div>';

											   }

												 final_html_1 = final_html_1 + html_1;
												 $("#edit_asset_bank_list").html(final_html_1);
												 $(".tot_bank").html(data.length);
											}
											Materialize.toast('Bank details updated successfully', 2000,'green rounded');
										}
										else
										{
											Materialize.toast('Bank details updated successfully', 2000,'green rounded');
											html_1='<div class="gst-boxs add-gst">Add Bank Data</div>';
											$("#edit_asset_bank_list").html(html_1);
										}
									}
									else
									{
										Materialize.toast('Error while processing. Please try again', 2000,'red rounded');
									}
									$("#add_bank_details_asset").find("input[type=text]").val("");
									$("#add_bank_branch_asset").modal('close');
									},

					});
				},
			});



//////////////////////////////////////////////////////////////////////////////////////////////////
			$("#add_new_cards").submit(function(e){
				e.preventDefault();
			}).validate({
				rules:{
					
				},
				messages:{
					
				},
				submitHandler:function(form){
						var frm=$(form).serialize();
								 //$(form).ajaxSubmit();
								$.ajax({
								url:base_url+'Profile/add_card_info',
								type:"POST",
								data:{'csrf_test_name':csrf_hash,"frm":frm,},
								success:function(res){
										if(res!= false)
									{
										var data=JSON.parse(res);
										//console.log(data.length);
										var html_1 ='';
										var final_html_1 ='';
										if(data != '')
										{

											var i=0;
											for(i=0;i<data.length;i++){
                                               if(data[i].status=="Inactive"){
													   var deac="deactive_record";
												   }else{
													    var deac="";
												   }
											if(i == 0)
											   {
												html_1='<div class="gst-boxs add-gst '+deac+'"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">CARD TYPE</span><p>'+data[i].card_type+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">BANK NAME</span><p>'+data[i].bank_name+'</p></div><div class="col l1 s1 m1 gstaction"><a class="remove_card_detail modal-trigger" data-target="remove_card_modal" "data-bank_name="'+data[i].bank_name+'" data-card_type="'+data[i].card_type+'" data-card_number="'+data[i].card_number+'" ><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a></div></div></div>';
											   }
											   else
											   {

												 html_1='<div class="cardmore"><div class="gst-boxs add-gst '+deac+'"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">CARD TYPE</span><p>'+data[i].card_type+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">BANK NAME</span><p>'+data[i].bank_name+'</p></div><div class="col l1 s1 m1 gstaction"><a class="remove_card_detail modal-trigger" data-target="remove_card_modal" "data-bank_name="'+data[i].bank_name+'" data-card_type="'+data[i].card_type+'" data-card_number="'+data[i].card_number+'" ><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a></div></div></div>';

											   }

												 final_html_1 = final_html_1 + html_1;
												 $("#company_card_list").html(final_html_1);
												 $(".tot_card").html(data.length);
											}
											Materialize.toast('Card details added successfully', 2000,'green rounded');;
										}
										else
										{
											Materialize.toast('Card details added successfully', 2000,'green rounded');;
											html_1='<div class="gst-boxs add-gst">Add Bank Data</div>';
											$("#company_card_list").html(html_1);
										}
									}
									else
									{
										Materialize.toast('Error while processing. Please try again', 2000,'red rounded');
									}
									$("#add-card").find("input[type=text],select").val("");
									$("#add-card").modal('close');
									$("#edit-card").modal('close');

									

										
									},
						
					});
				},
			});


             $('.add_new_card').off().on('click',function(){

					//$('#addbank_with_existing #bank_bus_id').val($(this).data('company_id'));
					//$('#addbank_with_existing #bank_reg_id').val($(this).data('user_id'));
			});
			$("#add_new_cards_ext").submit(function(e){
				e.preventDefault();
			}).validate({
				rules:{
					
				},
				messages:{
					
				},
				submitHandler:function(form){
						var frm=$(form).serialize();
								 //$(form).ajaxSubmit();
								$.ajax({
								url:base_url+'Profile/add_new_card',
								type:"POST",
								data:{'csrf_test_name':csrf_hash,"frm":frm,},
								success:function(res){
										if(res!= false)
									{
										var data=JSON.parse(res);
										//console.log(data.length);
										var html_1 ='';
										var final_html_1 ='';
										if(data != '')
										{

											var i=0;
											for(i=0;i<data.length;i++){
                                               if(data[i].status=="Inactive"){
													   var deac="deactive_record";
												   }else{
													    var deac="";
												   }
											if(i == 0)
											   {
												html_1='<div class="gst-boxs add-gst '+deac+'"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">CARD TYPE</span><p>'+data[i].card_type+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">BANK NAME</span><p>'+data[i].bank_name+'</p></div><div class="col l1 s1 m1 gstaction"><a class="remove_card_detail modal-trigger" data-target="remove_card_modal" "data-bank_name="'+data[i].bank_name+'" data-card_type="'+data[i].card_type+'" data-card_number="'+data[i].card_number+'" data-status="'+data[i].status+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a></div></div></div>';
											   }
											   else
											   {

												 html_1='<div class="cardmore"><div class="gst-boxs add-gst '+deac+'"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">CARD TYPE</span><p>'+data[i].card_type+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">BANK NAME</span><p>'+data[i].bank_name+'</p></div><div class="col l1 s1 m1 gstaction"><a class="remove_card_detail modal-trigger" data-target="remove_card_modal" "data-bank_name="'+data[i].bank_name+'" data-card_type="'+data[i].card_type+'" data-card_number="'+data[i].card_number+'" data-status="'+data[i].status+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a></div></div></div>';

											   }

												 final_html_1 = final_html_1 + html_1;
												 $("#edit_company_card_list").html(final_html_1);
												 $(".tot_card").html(data.length);
											}
											Materialize.toast('Card details added successfully', 2000,'green rounded');;
										}
										else
										{
											Materialize.toast('Card details added successfully', 2000,'green rounded');;
											html_1='<div class="gst-boxs add-gst">Add Bank Data</div>';
											$("#edit_company_card_list").html(html_1);
										}
									}
									else
									{
										Materialize.toast('Error while processing. Please try again', 2000,'red rounded');
									}
									$("#edit-card").find("input[type=text],select").val("");
									$("#add-card").modal('close');
									$("#edit-card").modal('close');

									

										
									},
						
					});
				},
			});



			$('#company_card_list').off().on('click','.remove_card_detail',function(){
                              alert("hi");
					$('#remove_card_modal #remove_card_type').val($('.remove_card_detail').data('card_type'));
					$('#remove_card_modal #remove_bank_name').val($('.remove_card_detail').data('bank_name'));
					
			});

				$('.remove_card_details').off().on('click',function(){
					
				var card_type = $('#remove_card_type').val();
				var cbank = $('#remove_bank_name').val();
				

				$.ajax({

					url:base_url+'profile/remove_card_details',

					type:"post",

					data:{'csrf_test_name':csrf_hash,"card_type":card_type,"bank":cbank},
					success:function(res){
						if(res!= false)
							{
								var data=JSON.parse(res);
								var html_1 ='';
								var final_html_1 ='';
								if(data != '')
								{
									$("#company_card_list").html("");
									for(var i=0;i<data.length;i++){
                                                   alert(deac);
												if(data[i].status=="Inactive"){
													   var deac="deactive_record";
												   }else{
													    var deac="";
												   }
												   alert(deac);
										if(i == 0)
											   {
												html_1='<div class="gst-boxs add-gst '+deac+'"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">ENTER BANK ACCOUNT NUMBER</span><p>'+data[i].cbank_account_no+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">BANK NAME</span><p>'+data[i].cbank_name+'</p></div><div class="col l1 s1 m1 gstaction"><a class="remove_card_detail modal-trigger" data-target="remove_card_modal" data-acc_no="'+data[i].cbank_account_no+'" data-cbank_name="'+data[i].cbank_name+'"data-cbank_branch_name="'+data[i].cbank_branch_name+'" data-cbank_account_type="'+data[i].cbank_account_type+'" data-cbank_ifsc_code="'+data[i].cbank_ifsc_code+'" ><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a></div></div></div>';
											   }
											   else
											   {

												 html_1='<div class="bankmore"><div class="gst-boxs add-gst '+deac+'"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">ENTER BANK ACCOUNT NUMBER</span><p>'+data[i].cbank_account_no+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">BANK NAME</span><p>'+data[i].cbank_name+'</p></div><div class="col l1 s1 m1 gstaction"><a class="remove_bank_detail modal-trigger" data-target="remove_bank_modal" data-acc_no="'+data[i].cbank_account_no+'" data-cbank_name="'+data[i].cbank_name+'"data-cbank_branch_name="'+data[i].cbank_branch_name+'" data-cbank_account_type="'+data[i].cbank_account_type+'" data-cbank_ifsc_code="'+data[i].cbank_ifsc_code+'" ><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a></div></div></div></div>';

											   }
										     final_html_1 = final_html_1 + html_1;
											 $("#company_card_list").html(final_html_1);
											 $('#showcardboxs i').html('keyboard_arrow_down');
											 $(".tot_card").html(data.length);
										}
										Materialize.toast('Card details deactivated', 2000,'green rounded');
									}
									else
									{
										Materialize.toast('Card details deactivated', 2000,'green rounded');
										html_1='<div class="gst-boxs add-gst">Add Card Data</div>';
										$("#company_card_list").html(html_1);
										$(".tot_card").html(0);
									}
								}
								else
								{
									html_1='<div class="gst-boxs add-gst">Add Card Data</div>';
									$("#company_card_list").html(html_1);
									$(".tot_card").html(0);
								}
						},

					});
				});

			$('#company_bank_list').off().on('click','.remove_bank_detail',function(){

					$('#remove_bank_modal #remove_ac_number').val($('.remove_bank_detail').data('acc_no'));
					$('#remove_bank_modal #remove_cbank_name').val($('.remove_bank_detail').data('cbank_name'));
					$('#remove_bank_modal #remove_cbank_branch_name').val($('.remove_bank_detail').data('cbank_branch_name'));
					$('#remove_bank_modal #remove_account_type').val($('.remove_bank_detail').data('cbank_account_type'));
					$('#remove_bank_modal #remove_ifsc_code').val($('.remove_bank_detail').data('cbank_ifsc_code'));
			});

				$('.remove_bank_details').off().on('click',function(){
				var ac_no = $('#remove_ac_number').val();
				var cbank = $('#remove_cbank_name').val();
				var branch = $('#remove_cbank_branch_name').val();
				var acc_type = $('#remove_account_type').val();
				var ifsc = $('#remove_ifsc_code').val();

				$.ajax({

					url:base_url+'profile/remove_bank_details',

					type:"post",

					data:{'csrf_test_name':csrf_hash,"ac_no":ac_no,"cbank":cbank,"branch":branch,"acc_type":acc_type,"ifsc":ifsc},
					success:function(res){
						if(res!= false)
							{
								var data=JSON.parse(res);
								var html_1 ='';
								var final_html_1 ='';
								if(data != '')
								{
									$("#company_bank_list").html("");
									for(var i=0;i<data.length;i++){
                                                  
												if(data[i].status=="Inactive"){
													   var deac="deactive_record";
												   }else{
													    var deac="";
												   }
												  
										if(i == 0)
											   {
												html_1='<div class="gst-boxs add-gst '+deac+'"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">ENTER BANK ACCOUNT NUMBER</span><p>'+data[i].cbank_account_no+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">BANK NAME</span><p>'+data[i].cbank_name+'</p></div><div class="col l1 s1 m1 gstaction"><a class="remove_bank_detail modal-trigger" data-target="remove_bank_modal" data-acc_no="'+data[i].cbank_account_no+'" data-cbank_name="'+data[i].cbank_name+'"data-cbank_branch_name="'+data[i].cbank_branch_name+'" data-cbank_account_type="'+data[i].cbank_account_type+'" data-cbank_ifsc_code="'+data[i].cbank_ifsc_code+'" ><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a></div></div></div>';
											   }
											   else
											   {

												 html_1='<div class="bankmore"><div class="gst-boxs add-gst '+deac+'"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">ENTER BANK ACCOUNT NUMBER</span><p>'+data[i].cbank_account_no+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">BANK NAME</span><p>'+data[i].cbank_name+'</p></div><div class="col l1 s1 m1 gstaction"><a class="remove_bank_detail modal-trigger" data-target="remove_bank_modal" data-acc_no="'+data[i].cbank_account_no+'" data-cbank_name="'+data[i].cbank_name+'"data-cbank_branch_name="'+data[i].cbank_branch_name+'" data-cbank_account_type="'+data[i].cbank_account_type+'" data-cbank_ifsc_code="'+data[i].cbank_ifsc_code+'" ><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a></div></div></div></div>';

											   }
										     final_html_1 = final_html_1 + html_1;
											 $("#company_bank_list").html(final_html_1);
											 $('#showbankboxs i').html('keyboard_arrow_down');
											 $(".tot_bank").html(data.length);
										}
										Materialize.toast('Bank details deactivated', 2000,'green rounded');
									}
									else
									{
										Materialize.toast('Bank details deactivated', 2000,'green rounded');
										html_1='<div class="gst-boxs add-gst">Add Bank Data</div>';
										$("#company_bank_list").html(html_1);
										$(".tot_bank").html(0);
									}
								}
								else
								{
									html_1='<div class="gst-boxs add-gst">Add Bank Data</div>';
									$("#company_bank_list").html(html_1);
									$(".tot_bank").html(0);
								}
						},

					});
				});
			$('.add_new_bank').off().on('click',function(){

					$('#addbank_with_existing #bank_bus_id').val($(this).data('company_id'));
					$('#addbank_with_existing #bank_reg_id').val($(this).data('user_id'));
			});
			$("#addbank_with_existing_frm").submit(function(e){
				e.preventDefault();
			}).validate({
				rules:{
					bank_name:{
						required:true,
					},
					ac_number:{
						required:true,
						digits:true,
					},
					acc_type:{
						required:true,
					},
					bus_bank_country:{
						required:true,
					},
					bank_cur_check:{
						required:true,
					},
					ifsc_code:{
						required:true,
						ifscno:true,
					},
					opening_bank_balance:{
						required:true,
						digits:true,
					},
					opening_balance_date:{
						required:true,
						
					},
				},
				messages:{
					bank_name:{
						required:"Bank Name is required",
					},
					ac_number:{
						required:"Bank Account Number is required",
					},
					ifsc_code:{
						ifscno:"Invalid IFSC Code",
					},
					acc_type:{
						required:"Account type is required",
					},
					bus_bank_country:{
						required:"Country is required",
					},
					bank_cur_check:{
						required:"Currency is required",
					},
					opening_bank_balance:{
						required:"Opening Bank Balance is required",
					},

					opening_balance_date:{
						required:"Opening Balance Date is required",
					},
				},
				submitHandler:function(form){
						var frm=$(form).serialize();
								 //$(form).ajaxSubmit();
								$.ajax({
								url:base_url+'profile/add_new_bank',
								type:"POST",
								data:{'csrf_test_name':csrf_hash,"frm":frm,},
								success:function(res){
									if(res!= false)
									{
										var data=JSON.parse(res);
										//console.log(data.length);
										var html_1 ='';
										var final_html_1 ='';
										if(data != '')
										{

											var i=0;
											for(i=0;i<data.length;i++){
                                              // alert(data[i].status);
												if(data[i].status=="Inactive"){
													   var deac="deactive_record";
												   }else{
													    var deac="";
												   }
												   console.log(deac);

											if(i == 0)
											   {
												html_1='<div class="gst-boxs add-gst "><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum '+deac+'"><span class="gstlabel">ENTER BANK ACCOUNT NUMBER</span><p>'+data[i].cbank_account_no+'</p></div><div class="col l6 s6 m6 gstplace '+deac+'"><span class="gstlabel">BANK NAME</span><p>'+data[i].cbank_name+'</p></div><div class="col l1 s1 m1 gstaction"><a class="edited_bank_removed modal-trigger '+deac+'" data-target="removed_existing_bank" data-cbank_id="'+data[i].cbank_id+'" data-bus_id="'+data[i].bus_id+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a><a class="edit_bankinfo modal-trigger" data-target="edit_bank_branch" data-acc_no="'+data[i].cbank_account_no+'" data-cbank_name="'+data[i].cbank_name+'"data-cbank_branch_name="'+data[i].cbank_branch_name+'" data-cbank_account_type="'+data[i].cbank_account_type+'" data-cbank_swift_code="'+data[i].cbank_swift_code+'" data-cbank_ifsc_code="'+data[i].cbank_ifsc_code+'" data-cbank_id="'+data[i].cbank_id+'" data-bus_id="'+data[i].bus_id+'" data-opening_balance="'+data[i].opening_bank_balance+'" data-opening_balance_date="'+data[i].opening_balance_date+'" data-status="'+data[i].status+'" ><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div>';
											   }
											   else
											   {

												 html_1='<div class="bankmore"><div class="gst-boxs add-gst "><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum '+deac+'"><span class="gstlabel">ENTER BANK ACCOUNT NUMBER</span><p>'+data[i].cbank_account_no+'</p></div><div class="col l6 s6 m6 gstplace '+deac+'"><span class="gstlabel">BANK NAME</span><p>'+data[i].cbank_name+'</p></div><div class="col l1 s1 m1 gstaction"><a class="edited_bank_removed modal-trigger '+deac+'" data-target="removed_existing_bank" data-cbank_id="'+data[i].cbank_id+'" data-bus_id="'+data[i].bus_id+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a><a class="edit_bankinfo modal-trigger" data-target="edit_bank_branch" data-acc_no="'+data[i].cbank_account_no+'" data-cbank_name="'+data[i].cbank_name+'"data-cbank_branch_name="'+data[i].cbank_branch_name+'" data-cbank_account_type="'+data[i].cbank_account_type+'" data-cbank_swift_code="'+data[i].cbank_swift_code+'" data-cbank_ifsc_code="'+data[i].cbank_ifsc_code+'" data-cbank_id="'+data[i].cbank_id+'" data-bus_id="'+data[i].bus_id+'" data-opening_balance="'+data[i].opening_bank_balance+'" data-opening_balance_date="'+data[i].opening_balance_date+'" data-status="'+data[i].status+'"><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div></div>';

											   }

												 final_html_1 = final_html_1 + html_1;
												 $("#edit_company_bank_list").html(final_html_1);
												 $(".tot_bank").html(data.length);
											}
											Materialize.toast('Bank details updated successfully', 2000,'green rounded');
										}
										else
										{
											Materialize.toast('Bank details updated successfully', 2000,'green rounded');
											html_1='<div class="gst-boxs add-gst">Add Bank Data</div>';
											$("#edit_company_bank_list").html(html_1);
										}
									}
									else
									{
										Materialize.toast('Error while processing. Please try again', 2000,'red rounded');
									}
									$("#addbank_with_existing_frm").find("input[type=text]").val("");
									$("#addbank_with_existing").modal('close');
									},

					});
				},
			});

			$('#edit_company_bank_list').on('click','.edit_bankinfo',function(){

				$("#edit_bank_details").find("input[type=text]").val("");
				var bank_name = $(this).data('cbank_name');
				var bank_branch_name = $(this).data('cbank_branch_name');
				var account_type = $(this).data('cbank_account_type');
				var ifsc_code = $(this).data('cbank_ifsc_code');
				var swift_code = $(this).data('cbank_swift_code');
				var bank_account_no = $(this).data('acc_no');
				var cmpid = $(this).data('bus_id');
				var cbank_id = $(this).data('cbank_id');
				var country_name = $(this).data('country_name');
				var cbank_country_code = $(this).data('country_id');
				var currency_id = $(this).data('cbank_currency_code');
				var currency = $(this).data('cbank_currency');
				var currencycode = $(this).data('cbank_currencycode');
				var cbank_swift_code = $(this).data('cbank_swift_code');
				var opening_bank_balance = $(this).data('opening_balance');
				var opening_balance_date = $(this).data('opening_balance_date');
				var status = $(this).data('status');
				
				var date=new Date(opening_balance_date);
				var day = date.getDate();
        var month = date.getMonth();
        var year = date.getFullYear();

        var dd=day+'-'+month+'-'+year;
				
    
				$('#edit_bank_branch #edit_ac_number,#edit_bank_branch #edit_bank_name,#swift_code,#edit_bank_branch #edit_bank_branch_name,#edit_bank_branch #ifsc_code,#edit_bank_branch, #edit_acc_type, #bus_bank_country_change,#opening_bank_balance,#opening_balance_date,#status').parents('.input-field').children().addClass('active');
				$('#edit_bank_branch #edit_ac_number').val(bank_account_no);
				$('#edit_bank_branch #edit_bank_name').val(bank_name);
				$('#edit_bank_branch #edit_bank_branch_name').val(bank_branch_name);
				$('#edit_bank_branch #ifsc_code').val(ifsc_code);
				$('#edit_bank_branch #swift_code').val(swift_code);
				$('#edit_bank_branch #edit_acc_type option[value="'+account_type+'"]').attr('selected','selected');
				$('#edit_bank_branch #edit_cbank_id').val(cbank_id);
				$('#edit_bank_branch #edit_cmpid').val(cmpid);
				$('#edit_bank_branch #bus_bank_country_change option[value="'+cbank_country_code+'"]').attr('selected','selected');
				$('#edit_bank_branch #bank_cur_check_change option[value="'+currency_id+'"]').attr('selected','selected');
				$('#edit_bank_branch #opening_bank_balance').val(opening_bank_balance);
				$('#edit_bank_branch #opening_balance_date').val(dd);
					$('#edit_bank_branch #status option[value="'+status+'"]').attr('selected','selected');

				$('#edit_bank_branch #edit_acc_type').material_select();
				$('#edit_bank_branch #bus_bank_country_change').material_select();
				$('#edit_bank_branch #bank_cur_check_change').material_select();
				$('#edit_bank_branch #status').material_select();

				// res = '';
				// $("#ifscORswift_change").html(res);

				// if(cbank_country_code == 101)
				// {
				// 	alert(ifsc_code);
				//   //res = '<label class="full-bg-label" for="ifsc_code">Enter IFS oC Code<span class="required_field">*</span></label> <input id="ifsc_code" name="ifsc_code" class="full-bg adjust-width" type="text">';
				//   res = '<label class="full-bg-label" for="ifsc_code">Enter IFSC Code<span class="required_field">*</span></label><input id="ifsc_code" name="ifsc_code" value="'+ifsc_code+'" class="full-bg adjust-width" type="text">';
				//   $("#ifscORswift_change").html(res);

				// }else{
				// 	alert(cbank_swift_code);
				//   res = '<label class="full-bg-label" for="swift_code">Enter Swift Code<span class="required_field">*</span></label> <input id="swift_code" name="swift_code" value="'+cbank_swift_code+'" class="full-bg adjust-width" type="text">';
				//   $("#ifscORswift_change").html(res);
				// }

			});

			$("#edit_bank_details").submit(function(e){
				e.preventDefault();
			}).validate({
				rules:{
					edit_bank_name:{
						required:true,
					},
					edit_ac_number:{
						required:true,
						digits:true,
					},
					ifsc_code:{

						ifscno:true,
					},
					opening_bank_balance:{
						required:true,
						digits:true,
					},
					opening_balance_date:{
						required:true,
						
					},
				},
				messages:{
					edit_bank_name:{
						required:"Bank Name is required",
					},
					edit_ac_number:{
						required:"Bank Account Number is required",
					},
					ifsc_code:{
						ifscno:"Invalid IFSC Code",
					},

					opening_bank_balance:{
						required:"Opening Bank Balance is required",
					},

					opening_balance_date:{
						required:"Bank Balance Date is required",
					},

				},
				submitHandler:function(form){
						var frm=$(form).serialize();
						//console.log(frm);
								$.ajax({
								url:base_url+'profile/edit_bank_info',
								type:"POST",
								data:{'csrf_test_name':csrf_hash,"frm":frm},
								success:function(res){

										if(res!= false)
										{
										var data=JSON.parse(res);
										//console.log(data.length);
										var html_1 ='';
										var final_html_1 ='';
										if(data != '')
										{

											var i=0;
											for(i=0;i<data.length;i++){

												if(data[i].status=="Inactive"){
													   var deac="deactive_record";
												   }else{
													    var deac="";
												   }

											if(i == 0)
											   {
												html_1='<div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum '+deac+'"><span class="gstlabel">ENTER BANK ACCOUNT NUMBER </span><p>'+data[i].cbank_account_no+'</p></div><div class="col l6 s6 m6 gstplace '+deac+'"><span class="gstlabel">BANK NAME</span><p>'+data[i].cbank_name+'</p></div><div class="col l1 s1 m1 gstaction"><a class="edited_bank_removed modal-trigger '+deac+'" data-target="removed_existing_bank" data-cbank_id="'+data[i].cbank_id+'" data-bus_id="'+data[i].bus_id+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a><a class="edit_bankinfo modal-trigger" data-target="edit_bank_branch" data-acc_no="'+data[i].cbank_account_no+'" data-cbank_name="'+data[i].cbank_name+'"data-cbank_branch_name="'+data[i].cbank_branch_name+'" data-cbank_account_type="'+data[i].cbank_account_type+'" data-cbank_ifsc_code="'+data[i].cbank_ifsc_code+'" data-cbank_id="'+data[i].cbank_id+'" data-country_id="'+data[i].country_id+'" data-country_name="'+data[i].country_name+'" data-cbank_currencycode="'+data[i].currency+'" data-cbank_swift_code="'+data[i].cbank_swift_code+'"            data-bus_id="'+data[i].bus_id+'" data-opening_balance="'+data[i].opening_bank_balance+'" data-opening_balance_date="'+data[i].opening_balance_date+'" data-status="'+data[i].status+'" ><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div>';
											   }
											   else
											   {

												 html_1='<div class="bankmore"><div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum '+deac+'"><span class="gstlabel">ENTER BANK ACCOUNT NUMBER </span><p>'+data[i].cbank_account_no+'</p></div><div class="col l6 s6 m6 gstplace '+deac+'"><span class="gstlabel">BANK NAME</span><p>'+data[i].cbank_name+'</p></div><div class="col l1 s1 m1 gstaction"><a class="edited_bank_removed modal-trigger '+deac+'" data-target="removed_existing_bank" data-cbank_id="'+data[i].cbank_id+'" data-bus_id="'+data[i].bus_id+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a><a class="edit_bankinfo modal-trigger" data-target="edit_bank_branch" data-acc_no="'+data[i].cbank_account_no+'" data-cbank_name="'+data[i].cbank_name+'"data-cbank_branch_name="'+data[i].cbank_branch_name+'" data-cbank_account_type="'+data[i].cbank_account_type+'" data-cbank_ifsc_code="'+data[i].cbank_ifsc_code+'" data-cbank_id="'+data[i].cbank_id+'        " data-country_id="'+data[i].country_id+'" data-country_name="'+data[i].country_name+'" data-cbank_currencycode="'+data[i].currency+'" data-cbank_swift_code="'+data[i].cbank_swift_code+'              " data-bus_id="'+data[i].bus_id+'" data-opening_balance="'+data[i].opening_bank_balance+'" data-opening_balance_date="'+data[i].opening_balance_date+'" data-status="'+data[i].status+'"><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div></div>';

											   }

												 final_html_1 = final_html_1 + html_1;
												 $("#edit_company_bank_list").html(final_html_1);
												 $(".tot_bank").html(data.length);
											}
											Materialize.toast('Bank details updated successfully', 2000,'green rounded');
										}
										else
										{
											Materialize.toast('Bank details updated successfully', 2000,'green rounded');
											html_1='<div class="gst-boxs add-gst">Add Bank Data</div>';
											$("#edit_company_bank_list").html(html_1);
										}
									}
									else
									{
										Materialize.toast('Error while processing. Please try again', 2000,'red rounded');
									}
									$("#edit_bank_details").find("input[type=text]").val("");
									$("#edit_bank_branch").modal('close');
									$('#showbankboxs i').html('keyboard_arrow_down');
									},
					});
				},
			});


            $('#edit_company_card_list').on('click','.edit_cardinfo',function(){

				$("#edit_new_cards_ext").find("input[type=text]").val("");
				var bank_name = $(this).data('issue_bank');
				
				var card_type = $(this).data('card_type');

				var ccard_id = $(this).data('ccard_id');

				var status = $(this).data('status');
				

				$('#edit_new_cards_ext #iss_bank').parents('.input-field').children().addClass('active');

				$('#edit_new_cards_ext #card option[value="'+card_type+'"]').attr('selected','selected');
				$('#edit_new_cards_ext #iss_bank').val(bank_name);
				$('#edit_new_cards_ext #ccard_id').val(ccard_id);
				

				$('#edit_new_cards_ext #card').material_select();
				$('#edit_new_cards_ext #status option[value="'+status+'"]').attr('selected','selected');
				$('#edit_new_cards_ext #status').material_select();
				

			});
			$("#edit_new_cards").submit(function(e){
				e.preventDefault();
			}).validate({
				rules:{
					
				},
				messages:{
					

				},
				submitHandler:function(form){
						var frm=$(form).serialize();
						//console.log(frm);
								$.ajax({
								url:base_url+'profile/edit_card_new_info',
								type:"POST",
								data:{'csrf_test_name':csrf_hash,"frm":frm},
								success:function(res){

										if(res!= false)
										{
										var data=JSON.parse(res);
										//console.log(data.length);
										var html_1 ='';
										var final_html_1 ='';
										if(data != '')
										{

											var i=0;
											for(i=0;i<data.length;i++){

												if(data[i].status=="Inactive"){
													   var deac="deactive_record";
												   }else{
													    var deac="";
												   }

											if(i == 0)
											   {
												html_1='<div class="gst-boxs add-gst "><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum '+deac+'"><span class="gstlabel">CARD TYPE </span><p>'+data[i].card_type+'</p></div><div class="col l6 s6 m6 gstplace '+deac+'"><span class="gstlabel">BANK NAME</span><p>'+data[i].bank_name+'</p></div><div class="col l1 s1 m1 gstaction"><a class="edited_card_removed modal-trigger '+deac+'" data-target="removed_existing_card" data-ccard_id="'+data[i].ccard_id+'" data-bus_id="'+data[i].bus_id+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a><a class="edit_cardinfo modal-trigger" data-target="edit-card" data-ccard_id="'+data[i].ccard_id+'" data-bus_id="'+data[i].bus_id+'" data-card_type="'+data[i].card_type+'" data-issue_bank="'+data[i].bank_name+'" data-status="'+data[i].status+'" ><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div>';
											   }
											   else
											   {

												 html_1='<div class="cardmore"><div class="gst-boxs add-gst "><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum '+deac+'"><span class="gstlabel">CARD TYPE </span><p>'+data[i].card_type+'</p></div><div class="col l6 s6 m6 gstplace '+deac+'"><span class="gstlabel">BANK NAME</span><p>'+data[i].bank_name+'</p></div><div class="col l1 s1 m1 gstaction"><a class="edited_card_removed modal-trigger '+deac+'" data-target="removed_existing_card" data-ccard_id="'+data[i].ccard_id+'" data-bus_id="'+data[i].bus_id+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a><a class="edit_cardinfo modal-trigger" data-target="edit-card" data-ccard_id="'+data[i].ccard_id+'" data-bus_id="'+data[i].bus_id+'" data-card_type="'+data[i].card_type+'" data-issue_bank="'+data[i].bank_name+'" data-status="'+data[i].status+'"><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div>';

											   }

												 final_html_1 = final_html_1 + html_1;
												 $("#edit_company_card_list").html(final_html_1);
												 $(".tot_card").html(data.length);
											}
											Materialize.toast('Card details updated successfully', 2000,'green rounded');
										}
										else
										{
											Materialize.toast('Card details updated successfully', 2000,'green rounded');
											html_1='<div class="gst-boxs add-gst">Add Card Data</div>';
											$("#edit_company_card_list").html(html_1);
										}
									}
									else
									{
										Materialize.toast('Error while processing. Please try again', 2000,'red rounded');
									}
									$("#edit_new_cards").find("input[type=text]").val("");
									$("#edit-card-new").modal('close');
									$('#showcardboxs i').html('keyboard_arrow_down');
									},
					});
				},
			});

			$("#edit_new_cards_ext").submit(function(e){
				e.preventDefault();
			}).validate({
				rules:{
					
				},
				messages:{
					

				},
				submitHandler:function(form){
						var frm=$(form).serialize();
						//console.log(frm);
								$.ajax({
								url:base_url+'profile/edit_card_info',
								type:"POST",
								data:{'csrf_test_name':csrf_hash,"frm":frm},
								success:function(res){

										if(res!= false)
										{
										var data=JSON.parse(res);
										//console.log(data.length);
										var html_1 ='';
										var final_html_1 ='';
										if(data != '')
										{

											var i=0;
											for(i=0;i<data.length;i++){

												if(data[i].status=="Inactive"){
													   var deac="deactive_record";
												   }else{
													    var deac="";
												   }

											if(i == 0)
											   {
												html_1='<div class="gst-boxs add-gst "><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum '+deac+'"><span class="gstlabel">CARD TYPE </span><p>'+data[i].card_type+'</p></div><div class="col l6 s6 m6 gstplace '+deac+'"><span class="gstlabel">BANK NAME</span><p>'+data[i].bank_name+'</p></div><div class="col l1 s1 m1 gstaction"><a class="edited_card_removed modal-trigger '+deac+'" data-target="removed_existing_card" data-ccard_id="'+data[i].ccard_id+'" data-bus_id="'+data[i].bus_id+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a><a class="edit_cardinfo modal-trigger" data-target="edit-card" data-ccard_id="'+data[i].ccard_id+'" data-bus_id="'+data[i].bus_id+'" data-card_type="'+data[i].card_type+'" data-issue_bank="'+data[i].bank_name+'" data-status="'+data[i].status+'" ><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div>';
											   }
											   else
											   {

												 html_1='<div class="cardmore"><div class="gst-boxs add-gst "><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum '+deac+'"><span class="gstlabel">CARD TYPE </span><p>'+data[i].card_type+'</p></div><div class="col l6 s6 m6 gstplace '+deac+'"><span class="gstlabel">BANK NAME</span><p>'+data[i].bank_name+'</p></div><div class="col l1 s1 m1 gstaction"><a class="edited_card_removed modal-trigger '+deac+'" data-target="removed_existing_card" data-ccard_id="'+data[i].ccard_id+'" data-bus_id="'+data[i].bus_id+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a><a class="edit_cardinfo modal-trigger" data-target="edit-card" data-ccard_id="'+data[i].ccard_id+'" data-bus_id="'+data[i].bus_id+'" data-card_type="'+data[i].card_type+'" data-issue_bank="'+data[i].bank_name+'" data-status="'+data[i].status+'"><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div>';

											   }

												 final_html_1 = final_html_1 + html_1;
												 $("#edit_company_card_list").html(final_html_1);
												 $(".tot_card").html(data.length);
											}
											Materialize.toast('Card details updated successfully', 2000,'green rounded');
										}
										else
										{
											Materialize.toast('Card details updated successfully', 2000,'green rounded');
											html_1='<div class="gst-boxs add-gst">Add Card Data</div>';
											$("#edit_company_card_list").html(html_1);
										}
									}
									else
									{
										Materialize.toast('Error while processing. Please try again', 2000,'red rounded');
									}
									$("#edit_new_cards_ext").find("input[type=text]").val("");
									$("#edit-card").modal('close');
									$('#showcardboxs i').html('keyboard_arrow_down');
									},
					});
				},
			});
			$('.edited_bank_removed').off().on('click',function(){

					$('#removed_existing_bank #remove_cbank_id').val($(this).data('cbank_id'));
					$('#removed_existing_bank #remove_bus_id').val($(this).data('bus_id'));
			});
			$(".removed_existing_bank_data").click(function(e){

					var cbank_id=$('#removed_existing_bank #remove_cbank_id').val();
					var bus_id= $('#removed_existing_bank #remove_bus_id').val();
						//console.log(frm);
								$.ajax({
								url:base_url+'profile/delete_bank_info',
								type:"POST",
								data:{'csrf_test_name':csrf_hash,"bus_id":bus_id,"cbank_id":cbank_id},
								success:function(res){
									console.log(res);
										if(res!= false)
										{
										var data=JSON.parse(res);
										//console.log(data.length);
										var html_1 ='';
										var final_html_1 ='';
										if(data != '')
										{

											var i=0;
											for(i=0;i<data.length;i++){
                                             	//alert(data[i].status);
												if(data[i].status=="Inactive"){
													   var deac="deactive_record";
												   }else{
													    var deac="";
												   }
												   console.log(deac);
											if(i == 0)
											   {

												html_1='<div class="gst-boxs add-gst "><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum '+deac+'"><span class="gstlabel">ENTER BANK ACCOUNT NUMBER</span><p>'+data[i].cbank_account_no+'</p></div><div class="col l6 s6 m6 gstplace '+deac+'"><span class="gstlabel">BANK NAME</span><p>'+data[i].cbank_name+'</p></div><div class="col l1 s1 m1 gstaction"><a class="edited_bank_removed modal-trigger '+deac+'" data-target="removed_existing_bank" data-cbank_id="'+data[i].cbank_id+'" data-bus_id="'+data[i].bus_id+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a><a class="edit_bankinfo modal-trigger" data-target="edit_bank_branch" data-acc_no="'+data[i].cbank_account_no+'" data-cbank_name="'+data[i].cbank_name+'"data-cbank_branch_name="'+data[i].cbank_branch_name+'" data-cbank_account_type="'+data[i].cbank_account_type+'" data-cbank_ifsc_code="'+data[i].cbank_ifsc_code+'" data-cbank_id="'+data[i].cbank_id+'" data-bus_id="'+data[i].bus_id+'" data-opening_balance="'+data[i].opening_bank_balance+'" data-opening_balance_date="'+data[i].opening_balance_date+'" data-status="'+data[i].status+'" ><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div>';
											   }
											   else
											   {

												 html_1='<div class="bankmore"><div class="gst-boxs add-gst "><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum '+deac+'"><span class="gstlabel">ENTER BANK ACCOUNT NUMBER</span><p>'+data[i].cbank_account_no+'</p></div><div class="col l6 s6 m6 gstplace '+deac+'"><span class="gstlabel">BANK NAME</span><p>'+data[i].cbank_name+'</p></div><div class="col l1 s1 m1 gstaction"><a class="edited_bank_removed modal-trigger '+deac+'" data-target="removed_existing_bank" data-cbank_id="'+data[i].cbank_id+'" data-bus_id="'+data[i].bus_id+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a><a class="edit_bankinfo modal-trigger" data-target="edit_bank_branch" data-acc_no="'+data[i].cbank_account_no+'" data-cbank_name="'+data[i].cbank_name+'"data-cbank_branch_name="'+data[i].cbank_branch_name+'" data-cbank_account_type="'+data[i].cbank_account_type+'" data-cbank_ifsc_code="'+data[i].cbank_ifsc_code+'" data-cbank_id="'+data[i].cbank_id+'" data-bus_id="'+data[i].bus_id+'" data-opening_balance="'+data[i].opening_bank_balance+'" data-opening_balance_date="'+data[i].opening_balance_date+'" data-status="'+data[i].status+'" ><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div></div>';

											   }

												 final_html_1 = final_html_1 + html_1;
												 $("#edit_company_bank_list").html(final_html_1);
												 $(".tot_bank").html(data.length);
											}
											Materialize.toast('Bank details deactivated', 2000,'green rounded');;
										}
										else
										{
											Materialize.toast('Bank details deactivated', 2000,'green rounded');
											html_1='<div class="gst-boxs add-gst">Add Bank Data</div>';
											$("#edit_company_bank_list").html(html_1);
										}
									}
									else
									{
										Materialize.toast('Error while processing. Please try again', 2000,'red rounded');
									}
									$("#edit_bank_details").find("input[type=text]").val("");
									$("#edit_bank_branch").modal('close');
									$('#showbankboxs i').html('keyboard_arrow_down');
									},
					});

			});

             $('.edited_card_removed').off().on('click',function(){

					$('#removed_existing_card #remove_ccard_id').val($(this).data('ccard_id'));
					$('#removed_existing_card #remove_bus_id').val($(this).data('bus_id'));
			});
			$(".removed_existing_card_data").click(function(e){

					var ccard_id=$('#removed_existing_card #remove_ccard_id').val();
					var bus_id= $('#removed_existing_card #remove_bus_id').val();
						//console.log(frm);
								$.ajax({
								url:base_url+'profile/delete_card_info',
								type:"POST",
								data:{'csrf_test_name':csrf_hash,"bus_id":bus_id,"ccard_id":ccard_id},
								success:function(res){
									
										if(res!= false)
										{
										var data=JSON.parse(res);
										//console.log(data.length);
										var html_1 ='';
										var final_html_1 ='';
										if(data != '')
										{

											var i=0;
											for(i=0;i<data.length;i++){
                                             	//alert(data[i].status);
												if(data[i].status=="Inactive"){
													   var deac="deactive_record";
												   }else{
													    var deac="";
												   }
												   console.log(deac);
											if(i == 0)
											   {

												html_1='<div class="gst-boxs add-gst "><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum '+deac+'"><span class="gstlabel">CARD TYPE </span><p>'+data[i].card_type+'</p></div><div class="col l6 s6 m6 gstplace '+deac+'"><span class="gstlabel">BANK NAME</span><p>'+data[i].bank_name+'</p></div><div class="col l1 s1 m1 gstaction"><a class="edited_card_removed modal-trigger '+deac+'" data-target="removed_existing_card" data-ccard_id="'+data[i].ccard_id+'" data-bus_id="'+data[i].bus_id+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a><a class="edit_cardinfo modal-trigger" data-target="edit-card" data-ccard_id="'+data[i].ccard_id+'" data-bus_id="'+data[i].bus_id+'" data-card_type="'+data[i].card_type+'" data-issue_bank="'+data[i].bank_name+'" data-status="'+data[i].status+'" ><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div>';
											   }
											   else
											   {

												 html_1='<div class="cardmore"><div class="gst-boxs add-gst "><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum '+deac+'"><span class="gstlabel">CARD TYPE </span><p>'+data[i].card_type+'</p></div><div class="col l6 s6 m6 gstplace '+deac+'"><span class="gstlabel">BANK NAME</span><p>'+data[i].bank_name+'</p></div><div class="col l1 s1 m1 gstaction"><a class="edited_card_removed modal-trigger '+deac+'" data-target="removed_existing_card" data-ccard_id="'+data[i].ccard_id+'" data-bus_id="'+data[i].bus_id+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a><a class="edit_cardinfo modal-trigger" data-target="edit-card" data-ccard_id="'+data[i].ccard_id+'" data-bus_id="'+data[i].bus_id+'" data-card_type="'+data[i].card_type+'" data-issue_bank="'+data[i].bank_name+'" data-status="'+data[i].status+'"><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div>';

											   }

												 final_html_1 = final_html_1 + html_1;
												 $("#edit_company_card_list").html(final_html_1);
												 $(".tot_card").html(data.length);
											}
											Materialize.toast('Card details deactivated', 2000,'green rounded');
										}
										else
										{
											Materialize.toast('Card details deactivated', 2000,'green rounded');
											html_1='<div class="gst-boxs add-gst">Add Card Info</div>';
											$("#edit_company_card_list").html(html_1);
										}
									}
									else
									{
										Materialize.toast('Error while processing. Please try again', 2000,'red rounded');
									}
									$("#edit_card_details").find("input[type=text]").val("");
									$("#edit_card").modal('close');
									$('#showcardboxs i').html('keyboard_arrow_down');
									},
					});

			});
			$(document).on('click', '.delete_company_legal_doc' ,function(){
				var legal_id = $(this).data('cmp_legal_id');
				$('#remove_company_legal_doc').modal('open');
				$('#remove_company_legal_doc #cmp_legal_doc_id').val(legal_id);
			});

			$('.remove_cmp_legal_doc').on('click',function(){
				var cmp_legal_doc_id = $('#cmp_legal_doc_id').val();
				$.ajax({
					url:base_url+'Company_profile/remove_company_legal_document',
					type:"POST",
					data:{'csrf_test_name':csrf_hash,"cmp_legal_doc_id":cmp_legal_doc_id,},
					success:function(res){
							location.reload();
						},
					});
			});

			$(document).on('click', '.delete_company_po_doc' ,function(){
				var cmp_po_id = $(this).data('cmp_po_id');
				$('#remove_company_po_doc').modal('open');
				$('#remove_company_po_doc #cmp_po_doc_id').val(cmp_po_id);
			});

			$('.remove_cmp_po_doc').on('click',function(){
				var cmp_po_doc_id = $('#cmp_po_doc_id').val();
				$.ajax({
					url:base_url+'Company_profile/remove_company_po_document',
					type:"POST",
					data:{'csrf_test_name':csrf_hash,"cmp_po_doc_id":cmp_po_doc_id,},
					success:function(res){
							location.reload();
						},
					});
			});
			$(document).on('click', '.delete_company_other_doc' ,function(){
				var cmp_other_id = $(this).data('cmp_other_id');
				$('#remove_company_other_doc').modal('open');
				$('#remove_company_other_doc #cmp_other_doc_id').val(cmp_other_id);
			});
			$('.remove_cmp_other_doc').on('click',function(){
				var cmp_other_doc_id = $('#cmp_other_doc_id').val();
				$.ajax({
					url:base_url+'Company_profile/remove_company_other_document',
					type:"POST",
					data:{'csrf_test_name':csrf_hash,"cmp_other_doc_id":cmp_other_doc_id,},
					success:function(res){
							location.reload();
						},
					});
			});
			$(document).on('click', '.delete_company_memo_doc' ,function(){
				var cmp_memo_id = $(this).data('cmp_memo_id');
				$('#remove_company_memo_doc').modal('open');
				$('#remove_company_memo_doc #cmp_memo_doc_id').val(cmp_memo_id);
			});
			$('.remove_cmp_memo_doc').on('click',function(){
				var cmp_memo_doc_id = $('#cmp_memo_doc_id').val();
				$.ajax({
					url:base_url+'Company_profile/remove_company_memo_document',
					type:"POST",
					data:{'csrf_test_name':csrf_hash,"cmp_memo_doc_id":cmp_memo_doc_id,},
					success:function(res){
							location.reload();
						},
					});
			});

			$(document).on('click', '.view_deactive_company_profile',function(){
				var cmp_profile = $(this).data('cmp_profile');
				 $('#view_deactive_cmp_profile').modal('open');
				 $('#view_deactive_cmp_profile #cmp_profile').val(cmp_profile);
			});

			$('.deactive_cmp').on('click',function(){

				var cmp_profile = $('#cmp_profile').val();

				$.ajax({

					url:base_url+'Company_profile/deactive_company_2',

					type:"POST",

					data:{'csrf_test_name':csrf_hash,"cmp_profile":cmp_profile,},

					success:function(res){
							window.location.href=base_url+'Company_profile/manage_company_profile';
						},
					});
			});
			$(document).on('click', '.view_activate_cmp_profile',function(){
				var cmp_profile = $(this).data('cmp_profile');
				 $('#view_active_cmp_profile').modal('open');
				 $('#view_active_cmp_profile #cmp_profile_2').val(cmp_profile);
			});
			$('.active_cmp').on('click',function(){

				var cmp_profile = $('#cmp_profile_2').val();

				$.ajax({

					url:base_url+'Company_profile/activate_company_2',

					type:"POST",

					data:{'csrf_test_name':csrf_hash,"cmp_profile":cmp_profile,},

					success:function(res){
							window.location.href=base_url+'Company_profile/manage_company_profile';
						},
					});
			});


		//$("input[type='checkbox']").click(function(){
				var bulk_profiles = [];
				$("input[id='personal_profile_bulk']").on("click",function(){
				if($(this).is(':checked',true)) {
					$(".personal_profile_bulk_action").prop('checked', true);
					$(".personal_profile_bulk_action:checked").each(function() {

							bulk_profiles.push($(this).val());
						});
						bulk_profiles = bulk_profiles.join(",");
						$('#deactive_multiple_profiles').attr('data-multi_profiles',bulk_profiles);
						$('#download_multiple_profiles').attr('data-multi_download',bulk_profiles);
					}
					else {
						$(".personal_profile_bulk_action").prop('checked',false);
						bulk_profiles = [];
						$('#deactive_multiple_profiles').attr('data-multi_profiles',0);
						$('#download_multiple_profiles').attr('data-multi_download',0);
					}
				});

				$("#deactive_multiple_profiles").on('click', function(e) {
					var profiles_values = $(this).attr('data-multi_profiles');
					var profiles_status= $("#activate").attr('data-act');

					
					if(profiles_values == '' || profiles_values == '0')
					{
						Materialize.toast('Please select a record first', 2000,'red rounded');
					}
					else
					{
						var words = profiles_values.split(",");
					    if(words.length < 2){
					      Materialize.toast('Select two or more records first..!', 2000,'red rounded');
					     }else{
							$.ajax({
								url:base_url+'profile/deactive_multiple_profile',
								type:"POST",
								data:{'csrf_test_name':csrf_hash,"profiles_values":profiles_values,"profiles_status":profiles_status},
								success:function(res){
										if(res == true)
										{
											//window.location.href=base_url+'Profile/manage_personal_profile';
										
											
											if(profiles_status=="reactivate")
					{
						Materialize.toast('Profiles has been successfully Reactivated', 2000,'green rounded');
					}else{
						Materialize.toast('Profiles has been successfully Deactivated', 2000,'green rounded');
						//window.location.href=base_url+'profile/manage_personal_profile';
					}
						window.location.href=base_url+'profile/manage_personal_profile';
                         
										}
										else
										{
											//window.location.href=base_url+'profile/manage_personal_profile';
											profile_datatable(base_path()+'profile/get_personal_profiles/','personal-profile');
											Materialize.toast('Error while processing. Please try again', 2000,'red rounded');
										}
									},
							});
						}
					}
				});
				$("#download_multiple_profiles").off().on('click', function(e) {

					var dnwl_ids = $(this).attr('data-multi_download');
					if(dnwl_ids == '' || dnwl_ids == '0')
					{
						Materialize.toast('Please select a record first', 2000,'red rounded');
					}
					else
					{
						var words = dnwl_ids.split(",");
					    if(words.length < 2){
					      Materialize.toast('Select two or more records first..!', 2000,'red rounded');
					    }else{
						$('#download_multiple_profiles').attr('data-multi_download',0);
						//$('#company-profile').find('input:checkbox').attr('checked',false);
						$('#personal-profile th').find('input[type="checkbox"]').each(function() {
						    $(this).prop('checked', false);
						});
						$("#personal_profile_bulk").prop('checked', false);  
						//alert('hj');

						window.location=base_url+'profile/download_multi_personal_profiles?ids='+dnwl_ids;
						profile_datatable(base_path()+'profile/get_personal_profiles/','personal-profile');
						$('select').material_select();
						Materialize.toast('Personal profile downloaded successfully', 2000,'green rounded');
						}
					}
				});


				var bulk_cmp_profiles = [];
				$("input[id='company_profile_bulk']").on("click",function(){
				if($(this).is(':checked',true)) {
					bulk_cmp_profiles = []
					$(".company_profile_bulk_action").prop('checked', true);
					$(".company_profile_bulk_action:checked").each(function() {
							bulk_cmp_profiles.push($(this).val());
						});
						bulk_cmp_profiles = bulk_cmp_profiles.join(",");
						$('#deactive_multiple_cmp_profiles').attr('data-multi_profiles',bulk_cmp_profiles);
						$('#download_multiple_cmp_profiles').attr('data-multi_download',bulk_cmp_profiles);
					}
					else {
						$('#company-profile tr').find('input[type="checkbox"]').each(function() {
						    $(this).prop('checked', false);
						 });
						bulk_cmp_profiles = [];
						$('#deactive_multiple_cmp_profiles').attr('data-multi_profiles',0);
						$('#download_multiple_cmp_profiles').attr('data-multi_download',0);
					}
				});

				$("#deactive_multiple_cmp_profiles").on('click', function(e) {
					var profiles_values = $(this).attr('data-multi_profiles');
					if(profiles_values == '' || profiles_values == '0')
					{
						Materialize.toast('Please select a record first', 2000,'red rounded');
					}
					else
					{
						var words = profiles_values.split(",");
					    if(words.length < 2){
					      Materialize.toast('Select two or more records first..!', 2000,'red rounded');
					    }else{
							$.ajax({
								url:base_url+'profile/deactive_multiple',
								type:"POST",
								data:{'csrf_test_name':csrf_hash,"profiles_values":profiles_values,},
								success:function(res){
										ajaxDataTableInit(base_path()+'profile/get_company_profiles/','company-profile');
										$('#deactive_multiple_cmp_profiles').attr('data-multi_profiles',0);
										$('#download_multiple_cmp_profiles').attr('data-multi_download',0);
										$('#company-profile th').find('input[type="checkbox"]').each(function() {
										    $(this).prop('checked', false);
										  });
										Materialize.toast('Company Profiles has been successfully deactivated', 2000,'green rounded');
									},
							});
						}
					}
				});
				$("#email_multiple_cmp_profiles").on('click', function(e) {
					var profiles_values = $("#deactive_multiple_cmp_profiles").attr('data-multi_profiles');
					if(profiles_values == '' || profiles_values == '0')
					{
						Materialize.toast('No Record Selected...Select record first..!', 2000,'red rounded');
					}
					else
					{
						var words = profiles_values.split(",");
					    if(words.length < 2){
					      Materialize.toast('Please select a record first', 2000,'red rounded');
					    }else{
							$.ajax({
								url:base_url+'profile/email_multiple_cmp_profiles',
								type:"POST",
								data:{'csrf_test_name':csrf_hash,"profiles_values":profiles_values,},
								success:function(res){
										ajaxDataTableInit(base_path()+'profile/get_company_profiles/','company-profile');
										$('#deactive_multiple_cmp_profiles').attr('data-multi_profiles',0);
										$('#download_multiple_cmp_profiles').attr('data-multi_download',0);
										$('#company-profile th').find('input[type="checkbox"]').each(function() {
										    $(this).prop('checked', false);
										  });
										Materialize.toast('Company Profiles has been successfully email', 2000,'green rounded');
									},
							});
						}
					}
				});
				$("body").on('click','#download_multiple_cmp_profiles',function(e) {

					var dnwl_ids = $('#download_multiple_cmp_profiles').attr('data-multi_download');

					if(dnwl_ids == '' || dnwl_ids == '0')
					{

						Materialize.toast('No Record Selected...Select record first..!', 2000,'red rounded');
					}
					else
					{
						var words = dnwl_ids.split(",");
					    if(words.length < 2){
					      Materialize.toast('Please select a record first', 2000,'red rounded');
					    }else{

                            /*  $.ajax({
								url:base_url+'profile/download-multiple-profiles',
								type:"POST",
								data:{"ids":dnwl_ids,},
								success:function(res){
										ajaxDataTableInit(base_path()+'profile/get_company_profiles/','company-profile');
										$('#deactive_multiple_cmp_profiles').attr('data-multi_profiles',0);
										$('#download_multiple_cmp_profiles').attr('data-multi_download',0);
										$('#company-profile th').find('input[type="checkbox"]').each(function() {
										    $(this).prop('checked', false);
										  });
										Materialize.toast('Company Profiles has been successfully downloaded', 2000,'green rounded');
									},
							});	*/


					    	///////////////////////////////
						$('#deactive_multiple_cmp_profiles').attr('data-multi_profiles',0);
					    $('#download_multiple_cmp_profiles').attr('data-multi_download',0);
						$('#company-profile th').find('input[type="checkbox"]').each(function() {
						   $(this).prop('checked', false);
						  });
                         var link = document.createElement("a");
                        link.download = name;
                       link.href = base_url+'profile/download-multiple-profiles?ids='+dnwl_ids;;
                        link.click();

						//location.href=base_url+'profile/download-multiple-profiles?ids='+dnwl_ids;
						ajaxDataTableInit(base_path()+'profile/get_company_profiles/','company-profile');
						Materialize.toast('Company Profiles has been successfully downloaded', 2000,'green rounded');
						}
					}
				});
				$("body").on('click','#print_multiple_cmp_profiles',function(e) {

					var dnwl_ids = $('#download_multiple_cmp_profiles').attr('data-multi_download');

					if(dnwl_ids == '' || dnwl_ids == '0')
					{

						Materialize.toast('No Record Selected...Select record first..!', 2000,'red rounded');
					}
					else
					{
						var words = dnwl_ids.split(",");
					    if(words.length < 2){
					      Materialize.toast('Please select a record first', 2000,'red rounded');
					    }else{
						$('#deactive_multiple_cmp_profiles').attr('data-multi_profiles',0);
						$('#download_multiple_cmp_profiles').attr('data-multi_download',0);
						//$('#company-profile').find('input:checkbox').attr('checked',false);
						$('#company-profile th').find('input[type="checkbox"]').each(function() {
						    $(this).prop('checked', false);
						  });
						window.location=base_url+'profile/print-multiple-profiles?ids='+dnwl_ids;
						ajaxDataTableInit(base_path()+'profile/get_company_profiles/','company-profile');
						Materialize.toast('Company Profiles has been successfully downloaded', 2000,'green rounded');
						}
					}
				});



				/*--------------EXPENSE-------------------*/

				var bulk_expense = [];
				$("input[id='expense_bulk']").on("click",function(){

				if($(this).is(':checked',true)) {
					$(".expense_bulk_action").prop('checked', true);
					$(".expense_bulk_action:checked").each(function() {
							bulk_expense.push($(this).val());
						});
						bulk_expense = bulk_expense.join(",");

						$('#deactive_multiple_expense').attr('data-multi_expense',bulk_expense);
					}
					else {
						$(".expense_bulk_action").prop('checked',false);
						bulk_expense = [];
						$('#deactive_multiple_expense').attr('data-multi_expense',0);
					}
				});

				$("#deactive_multiple_expense").on('click', function(e) {
					var expense_values = $(this).data('multi_expense');

					if(expense_values == '' || expense_values == '0')
					{
						Materialize.toast('Please select a record first', 2000,'red rounded');
					}
					else
					{
						$.ajax({
							url:base_url+'expense/deactive_multiple_expense',
							type:"POST",
							data:{'csrf_test_name':csrf_hash,"expense_values":expense_values,},
							success:function(res){

					                  if(res == true)
					                  {
					                    window.location.href=base_url+'expense/manage_expense_list';
					                    Materialize.toast('Expenses has been successfully deactivated', 2000,'green rounded');
					                  }
					                  else
					                  {
					                    window.location.href=base_url+'expense/manage_expense_list';
					                    Materialize.toast('Error while processing!', 2000,'red rounded');
					                  }
					                },
						});
					}
				});

				/*--------------EXPENSE-------------------*/


				/*-------------- Start Deactivate Account Section-------------------*/
				$('.deactive_my_account').on('click',function(){

						$('#deactive_acc_otp_modal').modal('open');
						$('#deactive_acc_otp_modal #mobile_no_acc').prop("checked",false);
						$('#deactive_acc_otp_modal #email_address_acc').prop("checked",false);
				});
				$('.sent_account_otp').on('click',function(){
					var mobile_no=0;
					var email_add=0;
					var otp_sentense="An OTP has successfully sent to registered Email Address, Enter it below to Deactivate Account";
					if($('#mobile_no_acc').prop("checked") == false && $('#email_address_acc').prop("checked") == false){
						Materialize.toast('Please select at least one!!', 2000,'red rounded');

					}else{
					  if($('#mobile_no_acc').is(":checked")){
						mobile_no=1;
						otp_sentense="An OTP has successfully sent to registered Mobile Number, Enter it below to Deactivate Account";
					  }
					   if($('#email_address_acc').is(":checked")){
						email_add=1;
						otp_sentense="An OTP has successfully sent to registered Email Address, Enter it below to Deactivate Account";
					  }
					  if($('#mobile_no_acc').prop("checked") == true && $('#email_address_acc').prop("checked") == true){
					  	otp_sentense="An OTP has successfully sent to registered Mobile Number and Email Address, Enter it below to Deactivate Account";
					  }

					 $.ajax({
							type:"POST",
							url: base_url + 'index/deactive_my_account',
							data:{'csrf_test_name':csrf_hash,'mobile_no':mobile_no,'email_add':email_add},
							success: function (res) {
								var data = JSON.parse(res);
								if (data == true) {
									console.log('sdsdp');
									$('#deactive_acc_otp_modal').modal('close');

									$('#deactivate_enter_otp_modal').modal('open');

									$('#deactivate_enter_otp_modal #otp_sentense').text(otp_sentense);

								}else{
									console.log('1244');
								 Materialize.toast('Error.......', 2000,'red rounded');
							}
							}

						});
					}

				});
				$("#enter_otp_mail").submit(function(e){
					e.preventDefault();
						}).validate({
							rules:{

								'acc_otp':{
									required:true,
									number:true,
									minlength:6,
									maxlength:6,
								},

							},
							messages:{

								'acc_otp':{
									required:"OTP is required",

								},

							},
							submitHandler:function(form){

									var frm=$(form).serialize();
											$.ajax({
											url:base_url+'index/match_acc_otp',
											type:"POST",
											data:{'csrf_test_name':csrf_hash,"frm":frm,},
											success:function(res){
												var data = JSON.parse(res);

												if(data!= false)
												{
													$('#deactivate-notification').modal('open');
													setTimeout(function(){ location.reload(); }, 3000);
													//Materialize.toast('Tax Information have been set', 2000,'green rounded');
												}
												else
												{
													Materialize.toast('Error while processing. Please try again', 2000,'red rounded');
												}
												$("#deactivate_enter_otp_modal").find("input[type=text]").val("");
												$("#deactivate_enter_otp_modal").modal('close');


										},

								});
							},
						});
					/*-------------- End Deactivate Account Section-------------------*/
					$("#feedback_frm .rate").on('change',function(e){
						$('#feedback_frm #rate_ratio').text($(this).val());
					});
						$("#feedback_frm").submit(function(e){
							
							e.preventDefault();
							}).validate({
							rules:{

								'rate':{
									required:true,

								},

							},
							messages:{

								'rate':{
									required:"Please give us a feedback",

								},

							},
							errorPlacement: function(error, element) {
		                        if (element.attr("name") == "rate") {
		                            $("#feedback_error").html(error)
		                        }

		                    },
							submitHandler:function(form){
									if(csrf_hash===""){
										csrf_hash=csrf_hash;
									}	
									var frm=$(form).serialize();
											$.ajax({
											url:base_url+'index/feedback',
											type:"POST",
											data:{'csrf_test_name':csrf_hash,"frm":frm,},
											success:function(res){
												//var data = JSON.parse(res);
												var myArray = res.split("@");
												if(myArray[1]){
													csrf_hash=myArray[1];
												}
												if(myArray[0]!= 'false')
												{
													Materialize.toast('Your feedback has been received', 2000,'green rounded');
												}
												else
												{
													Materialize.toast('Error while processing. Please try again', 2000,'red rounded');
												}
												$("#feedback_frm").find("input[type=text],textarea").val("");
												$("#feedback_modal").modal('close');


										},

								});
							},
						});

							// Rating and review in Market Place
								$("#rating_frm .rate").on('change',function(e){
						$('#rating_frm #rate_ratio').text($(this).val());
					});
						$("#rating_frm").submit(function(e){
							e.preventDefault();
							}).validate({
							rules:{

								'rate':{
									required:true,

								},

							},
							messages:{

								'rate':{
									required:"Please give us a Rating",

								},

							},
							errorPlacement: function(error, element) {
		                        if (element.attr("name") == "rate") {
		                            $("#rating_error").html(error)
		                        }

		                    },
							submitHandler:function(form){

									var frm=$(form).serialize();
											$.ajax({
											url:base_url+'Market_place/review',
											type:"POST",
											data:{'csrf_test_name':csrf_hash,"frm":frm,},
											success:function(res){
												var data = JSON.parse(res);

												if(data!= false)
												{
													Materialize.toast('Your feedback has been received', 2000,'green rounded');
												}
												else
												{
													Materialize.toast('Error while processing. Please try again', 2000,'red rounded');
												}
												$("#rating_frm").find("input[type=text],textarea").val("");
												$("#rating_modal").modal('close');


										},

								});
							},
						});

							// Rating and review in Market Place
					/*-------------- Start Logout -------------------*/
					$('.logout_my_account').on('click',function(){

						location.href=base_url+"logout";
					});

					$('.logout_my_account_bus').on('click',function(){

						location.href=base_url+"logout_signup";
					});
					/*-------------- End Logout -------------------*/

				$('.deactive_customers').on('click',function(){
				var cust_profile = $(this).data('customer_id');
				var status = $(this).data('status');
				 $('#view_deactive_customer_profile').modal('open');


				 $('#view_deactive_customer_profile #customer_status').val(status);
				 if(status!="Active"){
				 	status="Deactivate";
				 }else{
				 	status="Activate";
				 }
				 $('#view_deactive_customer_profile .cstatus').text(status);
				 $('#view_deactive_customer_profile #customer_profile').val(cust_profile);
				});

				$('.deactive_customer_view').on('click',function(){

				var customer_profile = $('#customer_profile').val();
				var customer_status = $('#customer_status').val();
				$.ajax({

					url:base_url+'sales/active_deactive_customer',

					type:"POST",

					data:{'csrf_test_name':csrf_hash,'customer_id':customer_profile,'status':customer_status},

					success:function(res){
							if(res == true)
							{
								if(customer_status=="Inactive"){customer_status="Deactivate";}else{customer_status="Activate"; }

								Materialize.toast('Client has been '+customer_status+'d', 2000,'green rounded');
							}
							else
							{
								Materialize.toast('Error while processing!', 2000,'red rounded');
							}
							location.reload(true);
						},
					});
			});
			/*
			$('.activate_customer_2').on('click',function(){
				var cust_profile = $(this).data('customer_profile');
				 $('#view_active_customer_profile').modal('open');
				 $('#view_active_customer_profile #active_customer_profile').val(cust_profile);
				});

				$('.active_customer_view').on('click',function(){

				var customer_profile = $('#active_customer_profile').val();

				$.ajax({

					url:base_url+'My_customers/active_customer_profile',

					type:"POST",

					data:{"customer_profile":customer_profile,},

					success:function(res){
							window.location.href=base_url+'My_customers/list_customers';
						},
					});
			});
*/



			/*$(document).on('click', '.view_accessport' ,function(){
				var change_status=0;
				if($(this).prop('checked')==true){
				$('#view_add_modal-popup').modal('open');
				  change_status=1;
				}else{
					$('#view_modal-popup').modal('open');
				}
				var customer_id = $(this).val();

				$('.portal_access_yes').on('click',function(){
				$.ajax({
					url:base_url+'sales/edit_portal_access',
					type:"POST",
					data:{"customer_id":customer_id,"change_status":change_status},
					success:function(res){
							getCustomers();
						},
					});
				});
				$('.portal_access_no').on('click',function(){
					getCustomers();
				});

				});	*/




			function change_gst_header(){

				var gst_id=$('#branch_gst_list').val();
				console.log(gst_id);
				$.ajax({
					url:base_url+'dashboard/change_branch_gst_info',
					type:"POST",
					data:{'csrf_test_name':csrf_hash,'gst_id':gst_id},
					success:function(res){

						location.reload(true);
					},

				});
			}

			$("#header_company_profiles").off().on('change',function(){
				var company_id=$(this).val();
				//var customer_id=$('#customer_id').val();
				//var supplier_id=$('#supplier_id').val();
				/*$("#create_invoice #company_name").val(company_id);

				$('#company_profile').val(company_id);*/
				$.ajax({
					url:base_url+'dashboard/get_branch_gst_info',
					type:"POST",
					data:{'csrf_test_name':csrf_hash,'bus_id':company_id},
					success:function(res){
						console.log(res);
						if(res==false){
							$("#branch_gst_list").html('');
						}
						location.reload(true);
						//$("#branch_gst_list").html(res);
						//$("#branch_gst_list").parents('.input-field').addClass('label-active');
						//$('#branch_gst_list').material_select();
						//location.reload(true);
					},
					/*complete:function(res){
						$('#company_gst').val($('#branch_gst_list').val());
					},*/
				});
				/*$.ajax({
					'url':base_url+'My_bills/generate_bill_no',
					type:"POST",
					data:{
						'company_id':$('#company_profile').val(),
					},
					success:function(res){
						$("#bs_number").val(res);
					},
				});
				if(customer_id){
					$.ajax({
						url:base_url+'My_invoices/get_customer_gst_info',
						type:"POST",
						data:{"customer_id":customer_id,},
						success:function(res){
							 var res = $.parseJSON(res);
							$("#customer_gstin").html(res[0]);
							$('#customer_gstin').material_select();
							$("#customer_place").html(res[1]);
							$('#customer_place').material_select();
							$('.billing_address span').html(res[2]);
							$('#po_num').html(res[4]);
							$('#po_num').material_select();
							//$('.shipping_address span').html(res[3]);
							//$('.shipping_address').closest('span').html(res[3]);
							//$('.chkbox').parent('.ship_add').html(res[3]);
						},
						complete:function(res){
							$.ajax({
								url:base_url+'My_invoices/get_gst_value',
								type:"POST",
								data:{},
								success:function(res){
									var data = JSON.parse(res);
									$("#tot_gst").val(data.gst);
									var tot_gst = data.gst;
										$.ajax({
											url:base_url+'My_invoices/fetch_gst',
											type:"POST",
											data:{"customer_id":customer_id,"company_id":company_id,},
											success:function(res1){
												res1=$.parseJSON(res1);
												var half_gst = parseFloat($("#tot_gst").val())/2;
												if($("#document_type").val()==5){
													$("#igst").val(parseFloat($('#tot_gst').val()));
													$("#sgst").val("0.00");
													$("#cgst").val("0.00");
													$(".branch_igst").val(parseFloat($('#tot_gst').val()));
													$(".branch_sgst").val("0.00");
													$(".branch_cgst").val("0.00");
												}
												else if(res1==true && $("#document_type").val()!=5){
													$("#igst").val("0.00");
													$("#sgst").val(half_gst);
													$("#cgst").val(half_gst);
													$(".branch_igst").val("0.00");
													$(".branch_sgst").val(half_gst);
													$(".branch_cgst").val(half_gst);
												}
												else
												{
													$("#igst").val(parseFloat($('#tot_gst').val()));
													$("#sgst").val("0.00");
													$("#cgst").val("0.00");
													$(".branch_igst").val(parseFloat($('#tot_gst').val()));
													$(".branch_sgst").val("0.00");
													$(".branch_cgst").val("0.00");
												}

											},
											complete:function(res1){
												$('input[name="invoice_array[]"]').each(function(){
													calculate_total($(this).val());
												});
											},
										});
								},
							});
						},
					});
				}
				if(supplier_id!='' && supplier_id!=undefined){
					$.ajax({
						url:base_url+'my_purchase_invoices/get_supplier_gst_info',
						type:"POST",
						data:{"supplier_id":supplier_id,},
						success:function(res){
							 var res = $.parseJSON(res);
							$("#supplier_gstin").html(res[0]);
							$('#supplier_gstin').material_select();
							$("#supplier_place").html(res[1]);
							$('#supplier_place').material_select();
							$('.billing_address span.bill_add').html(res[2]);
							if($("#same-as-bill-invoice").is(':checked')){
								$('.shipping_address span.ship_add').html(res[2]);
							}
							else{
								$('.shipping_address span.ship_add').html(res[3]);
							}
							$('#po_numbers').html(res[4]);
							$('#po_numbers').material_select();
							//$('.shipping_address span').html(res[3]);
							//$('.shipping_address').closest('span').html(res[3]);
							//$('.chkbox').parent('.ship_add').html(res[3]);
						},
						complete:function(res){
							$.ajax({
								url:base_url+'my_purchase_invoices/get_gst_value',
								type:"POST",
								data:{},
								success:function(res){
									var data = JSON.parse(res);
									$("#tot_gst").val(data.gst);
									var tot_gst = data.gst;
										$.ajax({
											url:base_url+'my_purchase_invoices/fetch_gst',
											type:"POST",
											data:{"supplier_id":supplier_id,"company_id":company_id,},
											success:function(res1){
												res1=$.parseJSON(res1);
												var half_gst = parseFloat($("#tot_gst").val())/2;
												if($("#invoice_type").val()==5){
													$("#igst").val(parseFloat($('#tot_gst').val()));
													$("#sgst").val("0.00");
													$("#cgst").val("0.00");
													$(".branch_igst").val(parseFloat($('#tot_gst').val()));
													$(".branch_sgst").val("0.00");
													$(".branch_cgst").val("0.00");
												}
												else if(res1==true && $("#invoice_type").val()!=5){
													$("#igst").val("0.00");
													$("#sgst").val(half_gst);
													$("#cgst").val(half_gst);
													$(".branch_igst").val("0.00");
													$(".branch_sgst").val(half_gst);
													$(".branch_cgst").val(half_gst);
												}
												else
												{
													$("#igst").val(parseFloat($('#tot_gst').val()));
													$("#sgst").val("0.00");
													$("#cgst").val("0.00");
													$(".branch_igst").val(parseFloat($('#tot_gst').val()));
													$(".branch_sgst").val("0.00");
													$(".branch_cgst").val("0.00");
												}

											},
											complete:function(res1){
												$('input[name="invoice_array[]"]').each(function(){
													calculate_pur_inv_total($(this).val());
												});
											},
										});
								},
							});
						},
					});
				}*/
			});
			$('#customer_gstin').on('change' ,function(){
				var customer_id = $(this).val();
				$('#customer_place option[value="'+customer_id+'"]').attr('selected','selected');
				$('#customer_place').material_select();
			});
			$('#po_num').on('change' ,function(){
				var customer_id = $(this).val();
				$.ajax({
					url:base_url+'My_invoices/get_po_date',
					type:"POST",
					data:{'csrf_test_name':csrf_hash,"customer_id":customer_id,},
					success:function(res){
						$("#po_date").val(res);
					},
				});
			});

			 $("#create_services").validate({
				rules:{
					service_name:{
						required:true,
					},
					hsn_no:{
						required:true,
					},
				},
				messages:{
					service_name:{
						required:"Service Name is required",
					},
					hsn_no:{
						required:"HSN / SAC is required",
					},
				},
			});

			$("#add_other_tax").submit(function(e){
				e.preventDefault();
				$('#add_other_tax input').not('[type="submit"],[type="button"],[type="hidden"]').attr('required','required');
			}).validate({
				rules:{

					/*'tax_name[]':{
						required:true,
					},
					'tax_perc[]':{
						required:true,
						number:true
					},*/
				},
				messages:{

					/*'tax_name':{
						required:"Tax Name is required",
					},
					'tax_perc1':{
						required:"Tax Percentage is required",
					},*/
				},
				submitHandler:function(form){

						var frm=$(form).serialize();
								$.ajax({
								url:base_url+'services/add_services_tax',
								type:"POST",
								data:{'csrf_test_name':csrf_hash,"frm":frm,},
								success:function(res){
									if(res!= false)
									{
										var data=JSON.parse(res);
										var html_1 ='';
										var final_html ='';

										if(data != '')
										{
											for(var i=0;i<data.length;i++){
											if(i == 0)
											   {
												html_1='<div class="gst-boxs add-gst add-ajust"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">NAME OF TAX</span><p>'+data[i].stax_name+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">TAX PERCENTAGE</span><p>'+data[i].stax_percentage+'</p></div><div class="col l1 s1 m1 gstaction"><a class="remove_tax modal-trigger" data-tax_name="'+data[i].stax_name+'" data-tax_perc="'+data[i].stax_percentage+'" data-target="remove_tax_data"><img  src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a></div></div></div>';
											   }
											   else
											   {
												 html_1='<div class="gstmore"><div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">NAME OF TAX</span><p>'+data[i].stax_name+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">TAX PERCENTAGE</span><p>'+data[i].stax_percentage+'</p></div><div class="col l1 s1 m1 gstaction"><a class="remove_tax modal-trigger" data-tax_name="'+data[i].stax_name+'" data-tax_perc="'+data[i].stax_percentage+'" data-target="remove_tax_data"><img  src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a></div></div></div></div>';

											   }
												 final_html = final_html + html_1;

												 $("#tax_info_array").html(final_html);
												 $(".two-i").html(data.length);
											}
											 Materialize.toast('Tax Information have been set', 2000,'green rounded');
										}
										else
										{
											 $("#tax_info_array").html('');
											 $(".two-i").html(data.length);
										}
									}
									else
									{
										Materialize.toast('Error while processing. Please try again', 2000,'red rounded');
									}
									$("#add_other_tax").find("input[type=text], textarea").val("");
									$("#other-alert").modal('close');


								$('#tax_info_array').on('click','.remove_tax',function(){

									var tax_name = $(this).data('tax_name');
									var tax_perc = $(this).data('tax_perc');
									//$('#remove_tax_data').modal('open');
									$('#remove_tax_data #remove_tax_name').val('');
									$('#remove_tax_data #remove_tax_perc').val('');
									$('#remove_tax_data #remove_tax_name').val(tax_name);
									$('#remove_tax_data #remove_tax_perc').val(tax_perc);

								});

									$('.remove_tax_data').off().on('click',function(){

										var tax_name = $('#remove_tax_name').val();
										var tax_perc = $('#remove_tax_perc').val();

										$.ajax({

											url:base_url+'services/remove_service_tax',

											type:"post",

											data:{'csrf_test_name':csrf_hash,"tax_name":tax_name,"tax_perc":tax_perc},

											success:function(res){
													if(res!= false)
													{
														var data=JSON.parse(res);
														var html_1 ='';
														var final_html ='';
														if(data != '')
														{
															for(var i=0;i<data.length;i++){
															if(i == 0)
															   {
																html_1='<div class="gst-boxs add-gst add-ajust"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">NAME OF TAX</span><p>'+data[i].stax_name+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">TAX PERCENTAGE</span><p>'+data[i].stax_percentage+'</p></div><div class="col l1 s1 m1 gstaction"><a class="cur remove_tax modal-trigger" data-tax_name="'+data[i].stax_name+'" data-tax_perc="'+data[i].stax_percentage+'" data-target="remove_tax_data"><img  src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon" ></a></div></div></div>';
															   }
															   else
															   {
																html_1='<div class="gstmore"><div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">NAME OF TAX</span><p>'+data[i].stax_name+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">TAX PERCENTAGE</span><p>'+data[i].stax_percentage+'</p></div><div class="col l1 s1 m1 gstaction"><a class="cur remove_tax modal-trigger" data-tax_name="'+data[i].stax_name+'" data-tax_perc="'+data[i].stax_percentage+'" data-target="remove_tax_data"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon" ></a></div></div></div></div>';

															   }
																 final_html = final_html + html_1;
																 console.log(final_html);
																 $("#tax_info_array").html(final_html);
																 $(".two-i").html(data.length);
															}
															 Materialize.toast('Tax Information has been removed', 2000,'green rounded');
														}
														else
														{

															$("#tax_info_array").html('');
															 $(".two-i").html(0);
														}
													}
													else
													{
														$("#tax_info_array").html('');
														 $(".two-i").html(0);
													}
												},

											});
									});
							},

					});
				},
			});

///////////////////////////////////////////////////////////////////////

$("#add_other_tax_exp").submit(function(e){
				e.preventDefault();
				$('#add_other_tax input').not('[type="submit"],[type="button"],[type="hidden"]').attr('required','required');
			}).validate({
				rules:{

					/*'tax_name[]':{
						required:true,
					},
					'tax_perc[]':{
						required:true,
						number:true
					},*/
				},
				messages:{

					/*'tax_name':{
						required:"Tax Name is required",
					},
					'tax_perc1':{
						required:"Tax Percentage is required",
					},*/
				},
				submitHandler:function(form){

						var frm=$(form).serialize();
								$.ajax({
								url:base_url+'expense/add_services_tax',
								type:"POST",
								data:{'csrf_test_name':csrf_hash,"frm":frm,},
								success:function(res){
									if(res!= false)
									{
										var data=JSON.parse(res);
									
										var html_1 ='';
										var final_html ='';

										if(data != '')
										{
											for(var i=0;i<data.length;i++){
											if(i == 0)
											   {
												html_1='<div class="gst-boxs add-gst add-ajust"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">NAME OF TAX</span><p>'+data[i].stax_name+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">TAX PERCENTAGE</span><p>'+data[i].stax_percentage+'</p></div><div class="col l1 s1 m1 gstaction"><a class="remove_tax modal-trigger" data-tax_name="'+data[i].stax_name+'" data-tax_perc="'+data[i].stax_percentage+'" data-target="remove_tax_data"><img  src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a></div></div></div>';
											   }
											   else
											   {
												 html_1='<div class="gstmore"><div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">NAME OF TAX</span><p>'+data[i].stax_name+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">TAX PERCENTAGE</span><p>'+data[i].stax_percentage+'</p></div><div class="col l1 s1 m1 gstaction"><a class="remove_tax modal-trigger" data-tax_name="'+data[i].stax_name+'" data-tax_perc="'+data[i].stax_percentage+'" data-target="remove_tax_data"><img  src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a></div></div></div></div>';

											   }
												 final_html = final_html + html_1;

												 $("#tax_info_array").html(final_html);
												 $(".two-i").html(data.length);
											}
											 Materialize.toast('Tax Information have been set', 2000,'green rounded');
										}
										else
										{
											 $("#tax_info_array").html('');
											 $(".two-i").html(data.length);
										}
									}
									else
									{
										Materialize.toast('Error while processing. Please try again', 2000,'red rounded');
									}
									$("#add_other_tax").find("input[type=text], textarea").val("");
									$("#other-alert").modal('close');


								$('#tax_info_array').on('click','.remove_tax',function(){

									var tax_name = $(this).data('tax_name');
									var tax_perc = $(this).data('tax_perc');
									//$('#remove_tax_data').modal('open');
									$('#remove_tax_data #remove_tax_name').val('');
									$('#remove_tax_data #remove_tax_perc').val('');
									$('#remove_tax_data #remove_tax_name').val(tax_name);
									$('#remove_tax_data #remove_tax_perc').val(tax_perc);

								});

									$('.remove_tax_data').off().on('click',function(){

										var tax_name = $('#remove_tax_name').val();
										var tax_perc = $('#remove_tax_perc').val();

										$.ajax({

											url:base_url+'expense/remove_service_tax',

											type:"post",

											data:{'csrf_test_name':csrf_hash,"tax_name":tax_name,"tax_perc":tax_perc},

											success:function(res){
													if(res!= false)
													{
														var data=JSON.parse(res);
														var html_1 ='';
														var final_html ='';
														if(data != '')
														{
															for(var i=0;i<data.length;i++){
															if(i == 0)
															   {
																html_1='<div class="gst-boxs add-gst add-ajust"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">NAME OF TAX</span><p>'+data[i].stax_name+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">TAX PERCENTAGE</span><p>'+data[i].stax_percentage+'</p></div><div class="col l1 s1 m1 gstaction"><a class="cur remove_tax modal-trigger" data-tax_name="'+data[i].stax_name+'" data-tax_perc="'+data[i].stax_percentage+'" data-target="remove_tax_data"><img  src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon" ></a></div></div></div>';
															   }
															   else
															   {
																html_1='<div class="gstmore"><div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">NAME OF TAX</span><p>'+data[i].stax_name+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">TAX PERCENTAGE</span><p>'+data[i].stax_percentage+'</p></div><div class="col l1 s1 m1 gstaction"><a class="cur remove_tax modal-trigger" data-tax_name="'+data[i].stax_name+'" data-tax_perc="'+data[i].stax_percentage+'" data-target="remove_tax_data"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon" ></a></div></div></div></div>';

															   }
																 final_html = final_html + html_1;
																 console.log(final_html);
																 $("#tax_info_array").html(final_html);
																 $(".two-i").html(data.length);
															}
															 Materialize.toast('Tax Information has been removed', 2000,'green rounded');
														}
														else
														{

															$("#tax_info_array").html('');
															 $(".two-i").html(0);
														}
													}
													else
													{
														$("#tax_info_array").html('');
														 $(".two-i").html(0);
													}
												},

											});
									});
							},

					});
				},
			});
//////////////////////////////////////Asset///////////////////////
$("#add_other_tax_asset").submit(function(e){
				//e.preventDefault();
				//$('#add_other_tax_asset input').not('[type="submit"],[type="button"],[type="hidden"]').attr('required','required');
			}).validate({
				rules:{

					/*'tax_name[]':{
						required:true,
					},
					'tax_perc[]':{
						required:true,
						number:true
					},*/
				},
				messages:{

					/*'tax_name':{
						required:"Tax Name is required",
					},
					'tax_perc1':{
						required:"Tax Percentage is required",
					},*/
				},
				submitHandler:function(form){

						var frm=$(form).serialize();
								$.ajax({
								url:base_url+'Assets/add_services_tax',
								type:"POST",
								data:{'csrf_test_name':csrf_hash,"frm":frm,},
								success:function(res){
									if(res!= false)
									{
										var data=JSON.parse(res);
									
										var html_1 ='';
										var final_html ='';

										if(data != '')
										{
											for(var i=0;i<data.length;i++){
											if(i == 0)
											   {
												html_1='<div class="gst-boxs add-gst add-ajust"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum "><span class="gstlabel">NAME OF TAX</span><p>'+data[i].stax_name+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">TAX PERCENTAGE</span><p>'+data[i].stax_percentage+'</p></div><div class="col l1 s1 m1 gstaction"><a class="remove_tax modal-trigger" data-tax_name="'+data[i].stax_name+'" data-tax_perc="'+data[i].stax_percentage+'" data-target="remove_tax_data"><img  src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a></div></div></div>';
											   }
											   else
											   {
												 html_1='<div class="gstmore"><div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">NAME OF TAX</span><p>'+data[i].stax_name+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">TAX PERCENTAGE</span><p>'+data[i].stax_percentage+'</p></div><div class="col l1 s1 m1 gstaction"><a class="remove_tax modal-trigger" data-tax_name="'+data[i].stax_name+'" data-tax_perc="'+data[i].stax_percentage+'" data-target="remove_tax_data"><img  src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a></div></div></div></div>';

											   }
												 final_html = final_html + html_1;

												 $("#tax_info_array").html(final_html);
												 $(".two-i").html(data.length);
											}
											 Materialize.toast('Tax Information have been set', 2000,'green rounded');
										}
										else
										{
											 $("#tax_info_array").html('');
											 $(".two-i").html(data.length);
										}
									}
									else
									{
										Materialize.toast('Error while processing. Please try again', 2000,'red rounded');
									}
									$("#add_other_tax_asset").find("input[type=text], textarea").val("");
									$("#other-alert").modal('close');


								$('#tax_info_array').on('click','.remove_tax',function(){

									var tax_name = $(this).data('tax_name');
									var tax_perc = $(this).data('tax_perc');
									//$('#remove_tax_data').modal('open');
									$('#remove_tax_data #remove_tax_name').val('');
									$('#remove_tax_data #remove_tax_perc').val('');
									$('#remove_tax_data #remove_tax_name').val(tax_name);
									$('#remove_tax_data #remove_tax_perc').val(tax_perc);

								});

									$('.remove_tax').off().on('click',function(){

										var tax_name = $(this).data('tax_name');
									    var tax_perc = $(this).data('tax_perc');

										$.ajax({

											url:base_url+'Assets/remove_service_tax',

											type:"post",

											data:{'csrf_test_name':csrf_hash,"tax_name":tax_name,"tax_perc":tax_perc},

											success:function(res){
													if(res!= false)
													{
														//var data=JSON.parse(res);
														var data=JSON.parse(res);
														var html_1 ='';
														var final_html ='';
														if(data != '')
														{
															for(var i=0;i<data.length;i++){
															if(i == 0)
															   {
																html_1='<div class="gst-boxs add-gst add-ajust"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">NAME OF TAX</span><p>'+data[i].stax_name+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">TAX PERCENTAGE</span><p>'+data[i].stax_percentage+'</p></div><div class="col l1 s1 m1 gstaction"><a class="cur remove_tax modal-trigger" data-tax_name="'+data[i].stax_name+'" data-tax_perc="'+data[i].stax_percentage+'" data-target="remove_tax_data"><img  src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon" ></a></div></div></div>';
															   }
															   else
															   {
																html_1='<div class="gstmore"><div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">NAME OF TAX</span><p>'+data[i].stax_name+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">TAX PERCENTAGE</span><p>'+data[i].stax_percentage+'</p></div><div class="col l1 s1 m1 gstaction"><a class="cur remove_tax modal-trigger" data-tax_name="'+data[i].stax_name+'" data-tax_perc="'+data[i].stax_percentage+'" data-target="remove_tax_data"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon" ></a></div></div></div></div>';

															   }
																 final_html = final_html + html_1;
																 console.log(final_html);
																 $("#tax_info_array").html(final_html);
																// $(".two-i").html(data.length);
															}
															 Materialize.toast('Tax Information has been removed', 2000,'green rounded');
														}
														else
														{

															$("#tax_info_array").html('');
															// $(".two-i").html(0);
														}
													}
													else
													{
														$("#tax_info_array").html('');
														// $(".two-i").html(0);
													}
												},

											});
									});
							},

					});
				},
			});

////////////////////////////////////////////////////////////////////////

			/*$('.notif_type').on('click',function(){
				$('.notif_type img').removeClass('selected_notification');
				var type = $(this).data('notif_type');
				if(type==2){
					$('.noti_email').css('display','none');
					$('.noti_mobile').css('display','block');
				}else if(type=='3'){
					$('.noti_mobile').css('display','none');
					$('.noti_email').css('display','block');
				}else{
					$('.noti_mobile,.noti_email').css('display','none');
				}
				$('#notification_type').val(type);
				$(this).find('img').addClass('selected_notification');
			});*/

				$("#add_cust_alert_details").submit(function(e){
				e.preventDefault();
			}).validate({
				rules:{
					alert_name:{
						required:true,
					},
					alert_target:{
						number:true,
					},
					alert_mobile:{
						//mobileno:true,
						required:true,
						digits:true,
					},
					 alert_notification:{
			            required:function(e){
			            	if($('#alert_notification').val()=='' && $('#alert_msg').val()=='' && $('#alert_mail').val()==''){
			            		return true;
			            	}else{
			            		return false;
			            	}
			            }
			        },
				},
				messages:{
					alert_name:{
						required:"Alert Name is required",
					},
					alert_target:{
						number:"Enter Number only",
					},
					alert_mobile:{
						required:"Mobile Number is required",
					},
					alert_notification:{
			            required:"Alert Type is required",
          			},

				},
		        errorPlacement: function(error, element) {
		           if (element.attr("name") == "alert_notification") {
		           	  $("#notification_error").html(error)
		           }else{
		           	error.insertAfter(element);
		           }
		        },
				submitHandler:function(form){

						var frm=$(form).serialize();
								$.ajax({
								url:base_url+'sales/add_cust_alert_info',
								type:"post",
								data:{'csrf_test_name':csrf_hash,"frm":frm,},
								success:function(res){
									if(res!= false)
									{
										var data=JSON.parse(res);
										var html_1 ='';
										var final_html ='';
										var alt_num=$("#add_cust_alert_details #alert_number").val();
										alt_num=alt_num.replace("CA","");

										if(data != '')
										{
											for(var i=0;i<data.length;i++){
												var notification = '';
												   if(data[i].alert_notification == 1) {
													notification = 'Web Reminder';
												   }
												   if(data[i].alert_msg == 1) {
												   		if(notification!=''){
												   			notification = notification+', '+'Message';
												   		}else{
												   			notification = 'Message';
												   		}

												   }
												   if(data[i].alert_mail == 1){
													   	if(notification!=''){
												   			notification = notification+', '+'Email';
												   		}else{
												   			notification = 'Email';
												   		}
														//notification = 'Email';
												   }
												if(i == 0)
											   {
												html_1='<div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
							   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">ALERT NAME</span><p>'+data[i].alert_name+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">NOTIFICATION TYPE</span><p>'+notification+'</p></div><div class="col l1 s1 m1 gstaction"><a data-alert_name="'+data[i].alert_name+'" data-alert_no="'+data[i].alert_number+'" id="remove_client_alert_add_session" class="remove_client_alert_add_session modal-trigger" data-target="remove_alert_data_cust_add"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a></div></div></div>';
											   }
											   else
											   {
												 html_1='<div class="alertmore"><div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
							   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">ALERT NAME</span><p>'+data[i].alert_name+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">NOTIFICATION TYPE</span><p>'+notification+'</p></div><div class="col l1 s1 m1 gstaction"><a data-alert_name="'+data[i].alert_name+'" data-alert_no="'+data[i].alert_number+'" id="remove_client_alert_add_session" class="remove_client_alert_add_session modal-trigger" data-target="remove_alert_data_cust_add"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a></div></div></div>';

											   }
												 final_html = final_html + html_1;
												 $("#alert_cust_info_array").html(final_html);
												// $(".two-i").html(data.length);
											}
											Materialize.toast('An alert has been set!', 2000,'green rounded');
										}
										else
										{
											Materialize.toast('An alert has been set!', 2000,'green rounded');
											html_1='<div class="gst-boxs add-gst">Add Alert Data</div>';
											$("#alert_cust_info_array").html(html_1);
											Materialize.toast('Add alert information!', 2000,'green rounded');
										}
									}
									else
									{
										html_1='<div class="gst-boxs add-gst">Add Alert Data</div>';
										$("#alert_cust_info_array").html(html_1);
										Materialize.toast('Error while processing!', 2000,'red rounded');
									}
									$("#add_cust_alert_details").find("input[type=text],input[type=email],input[type=hidden], textarea").val("");

									var d = new Date();
									var month = d.getMonth()+1;
									var day = d.getDate();

									var output =  (month<10 ? '0' : '') + month + '/' +
									    (day<10 ? '0' : '') + day + '/' + d.getFullYear();
									$("#add_cust_alert_details #alert_date").val(output);

									$("#add_cust_alert_details #alert_number").val("CA"+(pad(parseInt(alt_num)+1,3)));
									$('#add_cust_alert_details a[class="notif_type"][data-notif_type="1"]').find('img').attr('src',base_path()+"asset/css/img/icons/bell-grey.png");
									$('#add_cust_alert_details a[class="notif_type"][data-notif_type="2"]').find('img').attr('src',base_path()+"asset/css/img/icons/msg-grey.png");
									$('#add_cust_alert_details a[class="notif_type"][data-notif_type="3"]').find('img').attr('src',base_path()+"asset/css/img/icons/mail-grey.png");
									$('#add_cust_alert_details #alert_mobile,#add_cust_alert_details #alert_email').attr('readonly','readonly').parents('.input-field').children().removeClass('active');
									$('.notif_type img').removeClass('selected_notification');
									$("#clientalertmodal").modal('close');
							},
					});
				},
			});


			$("#add_alert_details").submit(function(e){
				e.preventDefault();
			}).validate({
				rules:{
					alert_name:{
						required:true,
					},
					alert_target:{
						number:true,
					},
					alert_mobile:{
						//mobileno:true,
						required:true,
						digits:true,
					},
					 alert_notification:{
			            required:function(e){
			            	if($('#alert_notification').val()=='' && $('#alert_msg').val()=='' && $('#alert_mail').val()==''){
			            		return true;
			            	}else{
			            		return false;
			            	}
			            }
			        },
				},
				messages:{
					alert_name:{
						required:"Alert Name is required",
					},
					alert_target:{
						number:"Enter Number only",
					},
					alert_mobile:{
						required:"Mobile Number is required",
					},
					alert_notification:{
			            required:"Alert Type is required",
          			},

				},
		        errorPlacement: function(error, element) {
		           if (element.attr("name") == "alert_notification") {
		           	  $("#notification_error").html(error)
		           }else{
		           	error.insertAfter(element);
		           }
		        },
				submitHandler:function(form){

						var frm=$(form).serialize();
								$.ajax({
								url:base_url+'services/add_alert_info',
								type:"post",
								data:{'csrf_test_name':csrf_hash,"frm":frm,},
								success:function(res){
									if(res!= false)
									{
										var data=JSON.parse(res);
										var html_1 ='';
										var final_html ='';
										var alt_num=$("#add_alert_details #alert_number").val();
										alt_num=alt_num.replace("IA","");

										if(data != '')
										{
											for(var i=0;i<data.length;i++){
												var notification = '';
												   if(data[i].alert_notification == 1) {
													notification = 'Web Reminder';
												   }
												   if(data[i].alert_msg == 1) {
												   		if(notification!=''){
												   			notification = notification+', '+'Message';
												   		}else{
												   			notification = 'Message';
												   		}

												   }
												   if(data[i].alert_mail == 1){
													   	if(notification!=''){
												   			notification = notification+', '+'Email';
												   		}else{
												   			notification = 'Email';
												   		}
														//notification = 'Email';
												   }
												if(i == 0)
											   {
												html_1='<div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
							   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">ALERT NAME</span><p>'+data[i].alert_name+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">NOTIFICATION TYPE</span><p>'+notification+'</p></div><div class="col l1 s1 m1 gstaction"><a data-alert_name="'+data[i].alert_name+'" data-alert_no="'+data[i].alert_number+'" class="remove_service_alert_session modal-trigger" data-target="remove_alert_data"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a></div></div></div>';
											   }
											   else
											   {
												 html_1='<div class="alertmore"><div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
							   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">ALERT NAME</span><p>'+data[i].alert_name+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">NOTIFICATION TYPE</span><p>'+notification+'</p></div><div class="col l1 s1 m1 gstaction"><a data-alert_name="'+data[i].alert_name+'" data-alert_no="'+data[i].alert_number+'" class="remove_service_alert_session modal-trigger" data-target="remove_alert_data"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a></div></div></div>';

											   }
												 final_html = final_html + html_1;
												 $("#alert_info_array").html(final_html);
												// $(".two-i").html(data.length);
											}
											Materialize.toast('An alert has been set!', 2000,'green rounded');
										}
										else
										{
											Materialize.toast('An alert has been set!', 2000,'green rounded');
											html_1='<div class="gst-boxs add-gst">Add Alert Data</div>';
											$("#alert_info_array").html(html_1);
											Materialize.toast('Add alert information!', 2000,'green rounded');
										}
									}
									else
									{
										html_1='<div class="gst-boxs add-gst">Add Alert Data</div>';
										$("#alert_info_array").html(html_1);
										Materialize.toast('Error while processing!', 2000,'red rounded');
									}
									$("#add_alert_details").find("input[type=text],input[type=email],input[type=hidden], textarea").val("");

									var d = new Date();
									var month = d.getMonth()+1;
									var day = d.getDate();

									var output =  (month<10 ? '0' : '') + month + '/' +
									    (day<10 ? '0' : '') + day + '/' + d.getFullYear();
									$("#add_alert_details #alert_date").val(output);

									$("#add_alert_details #alert_number").val("IA"+(pad(parseInt(alt_num)+1,3)));
									$('#add_alert_details a[class="notif_type"][data-notif_type="1"]').find('img').attr('src',base_path()+"asset/css/img/icons/bell-grey.png");
									$('#add_alert_details a[class="notif_type"][data-notif_type="2"]').find('img').attr('src',base_path()+"asset/css/img/icons/msg-grey.png");
									$('#add_alert_details a[class="notif_type"][data-notif_type="3"]').find('img').attr('src',base_path()+"asset/css/img/icons/mail-grey.png");
									$('#add_alert_details #alert_mobile,#add_alert_details #alert_email').attr('readonly','readonly').parents('.input-field').children().removeClass('active');
									$('.notif_type img').removeClass('selected_notification');
									$("#add-alert").modal('close');
							},
					});
				},
			});

			$("#edit_services").validate({
				rules:{
					service_name:{
						required:true,
					},
					hsn_no:{
						required:true,
					},
				},
				messages:{
					service_name:{
						required:"Service Name is required",
					},
					hsn_no:{
						required:"HSN / SAC is required",
					},
				},
			});

			$('#tax_info_array_edit').on('click', '.edit_service_tax' ,function(){

				//$('#edit_tax_modal').modal('open');
				var tax_id = $(this).data('tax_id');
				var service_id = $(this).data('service_id');
				var tax_name = $(this).data('tax_name');
				var tax_percentage = $(this).data('tax_percentage');
				$('#edit_tax_modal #edit_tax_name,#edit_tax_modal #edit_tax_perc').parents('.input-field').children().addClass('active');

				$('#edit_tax_modal #edit_tax_id').val(tax_id);
				$('#edit_tax_modal #edit_tax_service_id').val(service_id);
				$('#edit_tax_modal #edit_tax_name').val(tax_name);
				$('#edit_tax_modal #edit_tax_perc').val(tax_percentage);
			});

			$("#edit_tax_service").submit(function(e){
				e.preventDefault();
			}).validate({
				rules:{

					edit_tax_name:{
						required:true,
					},
					edit_tax_perc:{
						required:true,
					},
				},
				messages:{

					edit_tax_name:{
						required:"TAX Name is required",
					},
					edit_tax_perc:{
						required:"TAX Percentage is required",
					},
				},
				submitHandler:function(form){
						var frm=$(form).serialize();
								$.ajax({
								url:base_url+'services/edit_tax_info',
								type:"POST",
								data:{'csrf_test_name':csrf_hash,"frm":frm,},
								success:function(res){
									//console.log(res);
									if(res!= false)
									{
										var data=JSON.parse(res);
										var html_1 ='';
										var final_html ='';
										if(data != '')
										{
											for(var i=0;i<data.length;i++){
											if(i == 0)
											   {
												html_1='<div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum '+deac+'"><span class="gstlabel">NAME OF TAX</span><p>'+data[i].stax_name+'</p></div><div class="col l6 s6 m6 gstplace '+deac+'"><span class="gstlabel">TAX PERCENTAGE</span><p>'+data[i].stax_percentage+'</p></div><div class="col l1 s1 m1 gstaction"><a class="remove_tax_edit modal-trigger '+deac+'" data-tax_id="'+data[i].stax_id+'" data-service_id="'+data[i].service_id+'" data-tax_name="'+data[i].stax_name+'" data-tax_perc="'+data[i].stax_percentage+'" data-target="edit_remove_tax_data"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a><a class="edit_service_tax modal-trigger" data-tax_id="'+data[i].stax_id+'" data-service_id="'+data[i].service_id+'" data-tax_name="'+data[i].stax_name+'" data-tax_percentage="'+data[i].stax_percentage+'"" data-target="edit_tax_modal"><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div>';
											   }
											   else
											   {
												 html_1='<div class="gstmore"><div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum '+deac+'"><span class="gstlabel">NAME OF TAX</span><p>'+data[i].stax_name+'</p></div><div class="col l6 s6 m6 gstplace '+deac+'"><span class="gstlabel">TAX PERCENTAGE</span><p>'+data[i].stax_percentage+'</p></div><div class="col l1 s1 m1 gstaction"><a class="remove_tax_edit modal-trigger '+deac+'" data-tax_id="'+data[i].stax_id+'" data-service_id="'+data[i].service_id+'" data-tax_name="'+data[i].stax_name+'" data-tax_percentage="'+data[i].stax_percentage+'"  data-target="edit_remove_tax_data"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a><a class="edit_service_tax modal-trigger" data-tax_id="'+data[i].stax_id+'" data-service_id="'+data[i].service_id+'" data-tax_name="'+data[i].stax_name+'" data-tax_percentage="'+data[i].stax_percentage+'"" data-target="edit_tax_modal"><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div></div>';

											   }
												 final_html = final_html + html_1;
												 $("#tax_info_array_edit").html(final_html);
												 $(".two-i").html(data.length);
											}
											 Materialize.toast('Tax information updated successfully !', 2000,'green rounded');
										}
										else
										{
											html_1='';
											$("#tax_info_array_edit").html(html_1);
											 Materialize.toast('ADD Tax DATA!', 2000,'green rounded');
										}
									}
									else
									{
										Materialize.toast('Error while processing. Please try again', 2000,'red rounded');
									}
									$("#edit_tax_service").find("input[type=text], textarea").val("");
									$("#edit_tax_modal").modal('close');

							},

					});
				},
			});


				$('#tax_info_array_edit').on('click', '.edit_service_tax' ,function(){

				//$('#edit_tax_modal').modal('open');
				var tax_id = $(this).data('tax_id');
				var service_id = $(this).data('service_id');
				var tax_name = $(this).data('tax_name');
				var tax_percentage = $(this).data('tax_percentage');
				$('#edit_tax_modal #edit_tax_name,#edit_tax_modal #edit_tax_perc').parents('.input-field').children().addClass('active');

				$('#edit_tax_modal #edit_tax_id').val(tax_id);
				$('#edit_tax_modal #edit_tax_service_id').val(service_id);
				$('#edit_tax_modal #edit_tax_name').val(tax_name);
				$('#edit_tax_modal #edit_tax_perc').val(tax_percentage);
			});

			$("#edit_tax_service").submit(function(e){
				e.preventDefault();
			}).validate({
				rules:{

					edit_tax_name:{
						required:true,
					},
					edit_tax_perc:{
						required:true,
					},
				},
				messages:{

					edit_tax_name:{
						required:"TAX Name is required",
					},
					edit_tax_perc:{
						required:"TAX Percentage is required",
					},
				},
				submitHandler:function(form){
						var frm=$(form).serialize();
								$.ajax({
								url:base_url+'services/edit_tax_info',
								type:"POST",
								data:{'csrf_test_name':csrf_hash,"frm":frm,},
								success:function(res){
									//console.log(res);
									if(res!= false)
									{
										var data=JSON.parse(res);
										var html_1 ='';
										var final_html ='';
										if(data != '')
										{
											for(var i=0;i<data.length;i++){
											if(i == 0)
											   {
												html_1='<div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">NAME OF TAX</span><p>'+data[i].stax_name+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">TAX PERCENTAGE</span><p>'+data[i].stax_percentage+'</p></div><div class="col l1 s1 m1 gstaction"><a class="remove_tax_edit modal-trigger" data-tax_id="'+data[i].stax_id+'" data-service_id="'+data[i].service_id+'" data-tax_name="'+data[i].stax_name+'" data-tax_perc="'+data[i].stax_percentage+'" data-target="edit_remove_tax_data"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a><a class="edit_service_tax modal-trigger" data-tax_id="'+data[i].stax_id+'" data-service_id="'+data[i].service_id+'" data-tax_name="'+data[i].stax_name+'" data-tax_percentage="'+data[i].stax_percentage+'"" data-target="edit_tax_modal"><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div>';
											   }
											   else
											   {
												 html_1='<div class="gstmore"><div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">NAME OF TAX</span><p>'+data[i].stax_name+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">TAX PERCENTAGE</span><p>'+data[i].stax_percentage+'</p></div><div class="col l1 s1 m1 gstaction"><a class="remove_tax_edit modal-trigger" data-tax_id="'+data[i].stax_id+'" data-service_id="'+data[i].service_id+'" data-tax_name="'+data[i].stax_name+'" data-tax_percentage="'+data[i].stax_percentage+'"  data-target="edit_remove_tax_data"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a><a class="edit_service_tax modal-trigger" data-tax_id="'+data[i].stax_id+'" data-service_id="'+data[i].service_id+'" data-tax_name="'+data[i].stax_name+'" data-tax_percentage="'+data[i].stax_percentage+'"" data-target="edit_tax_modal"><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div></div>';

											   }
												 final_html = final_html + html_1;
												 $("#tax_info_array_edit").html(final_html);
												 $(".two-i").html(data.length);
											}
											 Materialize.toast('Tax information updated successfully !', 2000,'green rounded');
										}
										else
										{
											html_1='';
											$("#tax_info_array_edit").html(html_1);
											 Materialize.toast('ADD Tax DATA!', 2000,'green rounded');
										}
									}
									else
									{
										Materialize.toast('Error while processing. Please try again', 2000,'red rounded');
									}
									$("#edit_tax_service").find("input[type=text], textarea").val("");
									$("#edit_tax_modal").modal('close');

							},

					});
				},
			});	

			$('#tax_info_array_asset_edit').on('click', '.edit_service_tax_asset' ,function(){

				//$('#edit_tax_modal').modal('open');
				var tax_id = $(this).data('tax_id');
				var service_id = $(this).data('service_id');
				var tax_name = $(this).data('tax_name');
				var tax_percentage = $(this).data('tax_percentage');
				var status = $(this).data('status');
				$('#edit_tax_asset_modal #edit_tax_name,#edit_tax_asset_modal #edit_tax_perc').parents('.input-field').children().addClass('active');

				$('#edit_tax_asset_modal #edit_tax_id').val(tax_id);
				$('#edit_tax_asset_modal #edit_tax_service_id').val(service_id);
				$('#edit_tax_asset_modal #edit_tax_name').val(tax_name);
				$('#edit_tax_asset_modal #edit_tax_perc').val(tax_percentage);
				$('#status option[value="'+status+'"]').attr('selected',true);
				$('#status').material_select();
			});

			$("#edit_tax_service_asset").submit(function(e){
				e.preventDefault();
			}).validate({
				rules:{

					edit_tax_name:{
						required:true,
					},
					edit_tax_perc:{
						required:true,
					},
				},
				messages:{

					edit_tax_name:{
						required:"TAX Name is required",
					},
					edit_tax_perc:{
						required:"TAX Percentage is required",
					},
				},
				submitHandler:function(form){
						var frm=$(form).serialize();
								$.ajax({
								url:base_url+'Assets/edit_tax_info',
								type:"POST",
								data:{'csrf_test_name':csrf_hash,"frm":frm,},
								success:function(res){
									//console.log(res);
									if(res!= false)
									{
										var data=JSON.parse(res);
										var html_1 ='';
										var final_html ='';
										if(data != '')
										{
											for(var i=0;i<data.length;i++){
												if(data[i].status=="Inactive"){
													   var deac="deactive_record";
												   }else{
													    var deac="";
												   }
											if(i == 0)
											   {
											   	
												html_1='<div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum '+deac+'"><span class="gstlabel">NAME OF TAX</span><p>'+data[i].stax_name+'</p></div><div class="col l6 s6 m6 gstplace '+deac+'"><span class="gstlabel">TAX PERCENTAGE</span><p>'+data[i].stax_percentage+'</p></div><div class="col l1 s1 m1 gstaction"><a class="remove_tax_edit_asset modal-trigger '+deac+'" data-tax_id="'+data[i].stax_id+'" data-service_id="'+data[i].asset_id+'" data-tax_name="'+data[i].stax_name+'" data-tax_perc="'+data[i].stax_percentage+'" data-target="edit_remove_tax_asset_data"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a><a data-tax_id="'+data[i].stax_id+'" data-service_id="'+data[i].asset_id+'" data-tax_name="'+data[i].stax_name+'" data-tax_percentage="'+data[i].stax_percentage+'" data-status="'+data[i].status+'"  class="edit_service_tax_asset modal-trigger" data-target="edit_tax_asset_modal"><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div>';
											   }
											   else
											   {
												 html_1='<div class="gstmore"><div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum '+deac+'"><span class="gstlabel">NAME OF TAX</span><p>'+data[i].stax_name+'</p></div><div class="col l6 s6 m6 gstplace '+deac+'"><span class="gstlabel">TAX PERCENTAGE</span><p>'+data[i].stax_percentage+'</p></div><div class="col l1 s1 m1 gstaction"><a class="remove_tax_edit_asset modal-trigger '+deac+'" data-tax_id="'+data[i].stax_id+'" data-service_id="'+data[i].asset_id+'" data-tax_name="'+data[i].stax_name+'" data-tax_percentage="'+data[i].stax_percentage+'"  data-target="edit_remove_tax_asset_data"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a><a  data-tax_id="'+data[i].stax_id+'" data-service_id="'+data[i].asset_id+'" data-tax_name="'+data[i].stax_name+'" data-tax_percentage="'+data[i].stax_percentage+'" data-status="'+data[i].status+'" class="edit_service_tax_asset modal-trigger" data-target="edit_tax_asset_modal"><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div></div>';

											   }
												 final_html = final_html + html_1;
												 $("#tax_info_array_asset_edit").html(final_html);
												 $(".two-i").html(data.length);
											}
											 Materialize.toast('Tax information updated successfully !', 2000,'green rounded');
										}
										else
										{
											html_1='';
											$("#tax_info_array_asset_edit").html(html_1);
											 Materialize.toast('ADD Tax DATA!', 2000,'green rounded');
										}
									}
									else
									{
										Materialize.toast('Error while processing. Please try again', 2000,'red rounded');
									}
									$("#edit_tax_service_asset").find("input[type=text], textarea").val("");
									$("#edit_tax_asset_modal").modal('close');

							},

					});
				},
			});

			$('#tax_info_array_edit').on('click', '.remove_tax_edit' ,function(){
				var tax_id = $(this).data('tax_id');
				var service_id = $(this).data('service_id');

				//$('#edit_remove_tax_data').modal('open');
				$('#edit_remove_tax_data #remove_tax_id').val(tax_id);
				$('#edit_remove_tax_data #remove_service_id').val(service_id);

				});

				$('.edit_remove_tax').on('click',function(){

										var tax_id = $('#remove_tax_id').val();
										var service_id = $('#remove_service_id').val();

										$.ajax({

											url:base_url+'services/delete_tax_info',

											type:"post",

											data:{'csrf_test_name':csrf_hash,"tax_id":tax_id,"service_id":service_id},

											success:function(res){
													if(res!= false)
													{
														var data=JSON.parse(res);
														var html_1 ='';
														var final_html ='';
														if(data != '')
														{
															for(var i=0;i<data.length;i++){
																if(data[i].status=="Inactive"){
													   var deac="deactive_record";
												   }else{
													    var deac="";
												   }
															if(i == 0)
															   {
																html_1='<div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum '+deac+'"><span class="gstlabel">NAME OF TAX</span><p>'+data[i].stax_name+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">TAX PERCENTAGE</span><p>'+data[i].stax_percentage+'</p></div><div class="col l1 s1 m1 gstaction"><a class="remove_tax_edit_asset modal-trigger" data-tax_id="'+data[i].stax_id+'" data-service_id="'+data[i].service_id+'" data-tax_name="'+data[i].stax_name+'" data-tax_perc="'+data[i].stax_percentage+'" data-target="edit_remove_tax_asset_data"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a><a data-tax_id="'+data[i].stax_id+'" data-service_id="'+data[i].service_id+'" data-tax_name="'+data[i].stax_name+'" data-tax_percentage="'+data[i].stax_percentage+'"" class="edit_service_tax_asset modal-trigger" data-target="edit_tax_asset_modal"><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div>';
															   }
															   else
															   {
																 html_1='<div class="gstmore"><div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum '+deac+'"><span class="gstlabel">NAME OF TAX</span><p>'+data[i].stax_name+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">TAX PERCENTAGE</span><p>'+data[i].stax_percentage+'</p></div><div class="col l1 s1 m1 gstaction"><a class="remove_tax_edit_asset modal-trigger" data-tax_id="'+data[i].stax_id+'" data-service_id="'+data[i].service_id+'" data-tax_name="'+data[i].stax_name+'" data-tax_percentage="'+data[i].stax_percentage+'" data-target="edit_remove_tax_asset_data"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a><a class="edit_service_tax" data-tax_id="'+data[i].stax_id+'" data-service_id="'+data[i].service_id+'" data-tax_name="'+data[i].stax_name+'" data-tax_percentage="'+data[i].stax_percentage+'"" class="edit_service_tax_asset modal-trigger" data-target="edit_tax_asset_modal"><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div></div>';

															   }
																 final_html = final_html + html_1;
																 $("#tax_info_array_edit").html(final_html);
																 $(".two-i").html(data.length);
															}
															Materialize.toast('TAX Info removed successfully !', 2000,'green rounded');
														}
														else
														{
															html_1='';
															$("#tax_info_array_edit").html(html_1);
															$(".two-i").html(0);
															Materialize.toast('ADD TAX DATA!', 2000,'green rounded');
														}
													}
													else
													{
														html_1='';
														$("#tax_info_array_edit").html(html_1);
														$(".two-i").html(0);
														Materialize.toast('Error while processing!', 2000,'red rounded');
													}
												},

											});
									});
                $('#tax_info_array_asset_edit').on('click', '.remove_tax_edit_asset' ,function(){
				var tax_id = $(this).data('tax_id');
				var service_id = $(this).data('service_id');

				//$('#edit_remove_tax_data').modal('open');
				$('#edit_remove_tax_asset_data #remove_tax_id').val(tax_id);
				$('#edit_remove_tax_asset_data #remove_service_id').val(service_id);

				});


				$('.edit_remove_tax_asset').on('click',function(){

										var tax_id = $('#remove_tax_id').val();
										var service_id = $('#remove_service_id').val();

										$.ajax({

											url:base_url+'Assets/delete_tax_info',

											type:"post",

											data:{'csrf_test_name':csrf_hash,"tax_id":tax_id,"service_id":service_id},

											success:function(res){
													if(res!= false)
													{
														var data=JSON.parse(res);
														var html_1 ='';
														var final_html ='';
														if(data != '')
														{
															for(var i=0;i<data.length;i++){
																if(data[i].status=="Inactive"){
													   var deac="deactive_record";
												   }else{
													    var deac="";
												   }
															if(i == 0)
															   {
																html_1='<div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum '+deac+'"><span class="gstlabel">NAME OF TAX</span><p>'+data[i].stax_name+'</p></div><div class="col l6 s6 m6 gstplace '+deac+'"><span class="gstlabel">TAX PERCENTAGE</span><p>'+data[i].stax_percentage+'</p></div><div class="col l1 s1 m1 gstaction"><a class="remove_tax_edit_asset modal-trigger '+deac+'" data-tax_id="'+data[i].stax_id+'" data-service_id="'+data[i].asset_id+'" data-tax_name="'+data[i].stax_name+'" data-tax_perc="'+data[i].stax_percentage+'" data-target="edit_remove_tax_asset_data"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a><a data-tax_id="'+data[i].stax_id+'" data-service_id="'+data[i].asset_id+'" data-tax_name="'+data[i].stax_name+'" data-tax_percentage="'+data[i].stax_percentage+'" data-status="'+data[i].status+'"  class="edit_service_tax_asset modal-trigger" data-target="edit_tax_asset_modal"><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div>';
											   }
											   else
											   {
												 html_1='<div class="gstmore"><div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum '+deac+'"><span class="gstlabel">NAME OF TAX</span><p>'+data[i].stax_name+'</p></div><div class="col l6 s6 m6 gstplace '+deac+'"><span class="gstlabel">TAX PERCENTAGE</span><p>'+data[i].stax_percentage+'</p></div><div class="col l1 s1 m1 gstaction"><a class="remove_tax_edit_asset modal-trigger '+deac+'" data-tax_id="'+data[i].stax_id+'" data-service_id="'+data[i].asset_id+'" data-tax_name="'+data[i].stax_name+'" data-tax_percentage="'+data[i].stax_percentage+'"  data-target="edit_remove_tax_asset_data"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a><a  data-tax_id="'+data[i].stax_id+'" data-service_id="'+data[i].asset_id+'" data-tax_name="'+data[i].stax_name+'" data-tax_percentage="'+data[i].stax_percentage+'" data-status="'+data[i].status+'" class="edit_service_tax_asset modal-trigger" data-target="edit_tax_asset_modal"><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div></div>';
															   }
																 final_html = final_html + html_1;
																 $("#tax_info_array_asset_edit").html(final_html);
																 $(".two-i").html(data.length);
															}
															Materialize.toast('TAX Info removed successfully !', 2000,'green rounded');
														}
														else
														{
															html_1='';
															$("#tax_info_array_asset_edit").html(html_1);
															$(".two-i").html(0);
															Materialize.toast('ADD TAX DATA!', 2000,'green rounded');
														}
													}
													else
													{
														html_1='';
														$("#tax_info_array_asset_edit").html(html_1);
														$(".two-i").html(0);
														Materialize.toast('Error while processing!', 2000,'red rounded');
													}
												},

											});
									});

									$('#alert_info_array_edit').on('click', '.edit_alert_info' ,function(){
										var alert_id = $(this).data('alert_id');
										var service_id = $(this).data('service_id');
										var alert_name = $(this).data('alert_name');
										var alert_date = $(this).data('alert_date');
										var alert_rem = $(this).data('alert_reminder');
										var condition = $(this).data('set_condition');
										var alert_number = $(this).data('alert_number');
										var alert_target = $(this).data('alert_target');
										var alert_mobile = $(this).data('alert_mobile');
										var alert_email = $(this).data('alert_email');
										var notification = $(this).data('alert_notification');
										var alert_msg = $(this).data('alert_msg');
										var alert_mail = $(this).data('alert_mail');
										$("#edit_alert_details").find("input[type=text],input[type=email],input[type=hidden], textarea").val("");
										$('#edit_alert #edit_alert_number,#edit_alert #edit_alert_reminder,#edit_alert #edit_alert_name,#edit_alert #edit_alert_date,#edit_alert #edit_alert_target,#edit_alert_email,#edit_alert_mobile').parents('.input-field').children().addClass('active');
										//$('#edit_alert').modal('open');
										$('#edit_alert #edit_alert_number').val(alert_number);
										$('#edit_alert #edit_alert_id').val(alert_id);
										$('#edit_alert #edit_alert_name').val(alert_name);
										$('#edit_alert #edit_alert_service_id').val(service_id);
										$('#edit_alert #edit_alert_date').val(alert_date);
										$('#edit_alert #edit_alert_target').val(alert_target);
										$('#edit_alert #edit_alert_reminder').val(alert_rem);

										$('#edit_alert #edit_condition option[value="'+condition+'"]').attr('selected','selected');
										$('#edit_condition').material_select();


										$('#edit_alert #edit_alert_notification').val(notification);
										$('#edit_alert #edit_alert_msg').val(alert_msg);
										$('#edit_alert #edit_alert_mail').val(alert_mail);
										$('.notify #edit_alert_mobile,.notify #edit_alert_email').attr('readonly','readonly');
										$('.edit_notif_type img').removeClass('selected_notification');

										if(notification=='1'){
											$('a[class="edit_notif_type"][data-notif_type="1"]').find('img').addClass('selected_notification');
											$('a[class="edit_notif_type"][data-notif_type="1"]').find('img').attr('src',base_path()+"asset/css/img/icons/bell-icon.png");
											$('.notify #edit_alert_notification').val(1);
										}else{
											$('a[class="edit_notif_type"][data-notif_type="1"]').find('img').attr('src',base_path()+"asset/css/img/icons/bell-grey.png");
										}
										if(alert_msg=='1'){
										$('a[class="edit_notif_type"][data-notif_type="2"]').find('img').addClass('selected_notification');
										$('a[class="edit_notif_type"][data-notif_type="2"]').find('img').attr('src',base_path()+"asset/css/img/icons/msg-gicon.png");
										$('.notify #edit_alert_msg').val(1);
										$('#edit_alert #edit_alert_mobile').val(alert_mobile);
										$('.notify #edit_alert_mobile').removeAttr('readonly');
										}else{
											$('a[class="edit_notif_type"][data-notif_type="2"]').find('img').attr('src',base_path()+"asset/css/img/icons/msg-grey.png");
											$('.notify #edit_alert_mobile').attr('readonly','readonly');
										}
										if(alert_mail=='1'){
											$('a[class="edit_notif_type"][data-notif_type="3"]').find('img').addClass('selected_notification');
											$('a[class="edit_notif_type"][data-notif_type="3"]').find('img').attr('src',base_path()+"asset/css/img/icons/mail-icon.png");
											$('.notify #edit_alert_mail').val(1);
											$('#edit_alert #edit_alert_email').val(alert_email);
											$('.notify #edit_alert_email').removeAttr('readonly');
										}else{
											$('a[class="edit_notif_type"][data-notif_type="3"]').find('img').attr('src',base_path()+"asset/css/img/icons/mail-grey.png");
											$('.notify #edit_alert_mail').attr('readonly','readonly');
										}

									});

									$("#edit_alert_details").submit(function(e){
											e.preventDefault();
										}).validate({
											rules:{
												edit_alert_name:{
													required:true,
												},
												edit_alert_target:{
													number:true,
												},
												edit_alert_mobile:{
													//mobileno:true,
													required:true,
													digits:true,
												},
											},
											messages:{
												edit_alert_name:{
													required:"Alert Name is required",
												},
												edit_alert_target:{
													number:"Enter Number only",
												},
												edit_alert_mobile:{
													required:"Mobile Number is required",
												},
											},
											submitHandler:function(form){

													var frm=$(form).serialize();
															$.ajax({
															url:base_url+'services/update_alert_info',
															type:"POST",
															data:{'csrf_test_name':csrf_hash,"frm":frm,},
															success:function(res){
																if(res!= false)
																{
																	var data=JSON.parse(res);
																	var html_1 ='';
																	var final_html ='';
																	if(data != '')
																	{
																		for(var i=0;i<data.length;i++){
																			var notification = '';
																			   if(data[i].alert_notification == 1) {
																				notification = 'Web Reminder';
																			   }
																			   if(data[i].alert_msg == 1) {
																			   		if(notification!=''){
																			   			notification = notification+', '+'Message';
																			   		}else{
																			   			notification = 'Message';
																			   		}

																			   }
																			   if(data[i].alert_mail == 1){
																				   	if(notification!=''){
																			   			notification = notification+', '+'Email';
																			   		}else{
																			   			notification = 'Email';
																			   		}
																					//notification = 'Email';
																			   }
																			   var d = new Date(data[i].alert_date);
																				var month = d.getMonth()+1;
																				var day = d.getDate();

																				var alert_date =  (month<10 ? '0' : '') + month + '/' +
																				    (day<10 ? '0' : '') + day + '/' + d.getFullYear();
																				var dd = new Date(data[i].alert_reminder);
																				var mmonth = dd.getMonth()+1;
																				var dday = dd.getDate();

																				var alert_reminder =  (mmonth<10 ? '0' : '') + mmonth + '/' +
																				    (dday<10 ? '0' : '') + dday + '/' + dd.getFullYear();

																			if(i == 0)
																			   {
																				html_1='<div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
															   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">ALERT NAME</span><p>'+data[i].alert_name+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">NOTIFICATION TYPE</span><p>'+notification+'</p></div><div class="col l1 s1 m1 gstaction"><a class="remove_service_alert_edit modal-trigger" data-alert_id="'+data[i].id+'" data-service_id="'+data[i].parent_id+'" data-target="remove_alert_info"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a><a class="edit_alert_info modal-trigger" data-alert_id="'+data[i].id+'" data-service_id="'+data[i].parent_id+'" data-alert_name="'+data[i].alert_name+'" data-alert_target="'+data[i].alert_target+'" data-set_condition="'+data[i].alert_condition+'" data-alert_number="'+data[i].alert_number+'" data-alert_date="'+alert_date+'" data-alert_reminder="'+alert_reminder+'" data-alert_mobile="'+data[i].alert_mobile+'" data-alert_email="'+data[i].alert_email+'" data-alert_msg="'+data[i].alert_msg+'" data-alert_mail="'+data[i].alert_mail+'" data-alert_notification="'+data[i].alert_notification+'" data-target="edit_alert"><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div>';
																			   }
																			   else
																			   {
																				 html_1='<div class="alertmore"><div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+(i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">ALERT NAME</span><p>'+data[i].alert_name+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">NOTIFICATION TYPE</span><p>'+notification+'</p></div><div class="col l1 s1 m1 gstaction"><a class="remove_service_alert_edit modal-trigger" data-alert_id="'+data[i].id+'" data-service_id="'+data[i].parent_id+'" data-target="remove_alert_info"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a><a class="edit_alert_info modal-trigger" data-alert_id="'+data[i].id+'" data-service_id="'+data[i].parent_id+'" data-alert_name="'+data[i].alert_name+'" data-alert_target="'+data[i].alert_target+'" data-set_condition="'+data[i].alert_condition+'" data-alert_reminder="'+alert_reminder+'" data-alert_number="'+data[i].alert_number+'" data-alert_date="'+alert_date+'" data-alert_mobile="'+data[i].alert_mobile+'"" data-alert_email="'+data[i].alert_email+'" data-alert_msg="'+data[i].alert_msg+'" data-alert_mail="'+data[i].alert_mail+'" data-alert_notification="'+data[i].alert_notification+'" data-target="edit_alert"><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div></div></div>';

																			   }
																			 final_html = final_html + html_1;
																			 $("#alert_info_array_edit").html(final_html);
																		}
																		Materialize.toast('Alert Information updated successfully !', 2000,'green rounded');
																	}
																	else
																	{
																		html_1='';
																		$("#alert_info_array_edit").html(html_1);
																		Materialize.toast('Add alert information!', 2000,'green rounded');
																	}
																}
																else
																{
																	html_1='';
																	$("#alert_info_array_edit").html(html_1);
																	Materialize.toast('Error while processing!', 2000,'red rounded');
																}
																$('.edit_notif_type img').removeClass('selected_notification');
																/*$('a[class="edit_notif_type"][data-notif_type="1"]').find('img').attr('src',base_path()+"asset/css/img/icons/bell-grey.png");
																$('a[class="edit_notif_type"][data-notif_type="2"]').find('img').attr('src',base_path()+"asset/css/img/icons/msg-grey.png");
																$('a[class="edit_notif_type"][data-notif_type="3"]').find('img').attr('src',base_path()+"asset/css/img/icons/mail-grey.png");
													*/
																$("#edit_alert_details").find("input[type=text],input[type=email],input[type=hidden], textarea").val("");
																$("#edit_alert").modal('close');
														},
												});
											},
										});

										$('.open_add_tax').on('click',function(){
											var service = $(this).data('service_id');
											$('#editpage_other-alert').modal('open');
											$('#editpage_other-alert #editpage_tax_service_id').val(service);
										});

							$("#edit_add_other_tax").submit(function(e){
								$('#edit_add_other_tax input').not('[type="submit"],[type="button"],[type="hidden"]').attr('required','required');
								e.preventDefault();
							}).validate({
								rules:{
									/*tax_name1: {
										required:true,
									},
									'tax_name[]':{
										required:true,
									},
									'tax_perc[]':{
										required:true,
									},*/
								},
								messages:{
									/*tax_name1: {
										required:"Tax Name is required",
									},
									'tax_name[]':{
										required:"Tax Name is required",
									},
									'tax_perc[]':{
										required:"Tax Percentage is required",
									},*/
								},
								submitHandler:function(form){

										var frm=$(form).serialize();
												$.ajax({
												url:base_url+'services/add_servicetax_info',
												type:"POST",
												data:{'csrf_test_name':csrf_hash,"frm":frm,},
												success:function(res){
													if(res!= false)
													{
														var data=JSON.parse(res);
														var html_1 ='';
														var final_html ='';
														if(data != '')
														{
															for(var i=0;i<data.length;i++){
															if(i == 0)
															   {
																html_1='<div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">NAME OF TAX</span><p>'+data[i].stax_name+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">TAX PERCENTAGE</span><p>'+data[i].stax_percentage+'</p></div><div class="col l1 s1 m1 gstaction"><a class="remove_tax_edit modal-trigger" data-tax_id="'+data[i].stax_id+'" data-service_id="'+data[i].service_id+'" data-tax_name="'+data[i].stax_name+'" data-tax_perc="'+data[i].tax_percentage+'" data-target="edit_remove_tax_data"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a><a class="edit_service_tax modal-trigger" data-tax_id="'+data[i].stax_id+'" data-service_id="'+data[i].service_id+'" data-tax_name="'+data[i].stax_name+'" data-tax_percentage="'+data[i].stax_percentage+'"" data-target="edit_tax_modal"><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div>';
															   }
															   else
															   {
																html_1='<div class="gstmore"><div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">NAME OF TAX</span><p>'+data[i].stax_name+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">TAX PERCENTAGE</span><p>'+data[i].stax_percentage+'</p></div><div class="col l1 s1 m1 gstaction"><a class="remove_tax_edit modal-trigger" data-tax_id="'+data[i].stax_id+'" data-service_id="'+data[i].service_id+'" data-tax_name="'+data[i].stax_name+'" data-tax_percentage="'+data[i].tax_percentage+'" data-target="edit_remove_tax_data"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a><a class="edit_service_tax modal-trigger" data-tax_id="'+data[i].stax_id+'" data-service_id="'+data[i].service_id+'" data-tax_name="'+data[i].stax_name+'" data-tax_percentage="'+data[i].stax_percentage+'"" data-target="edit_tax_modal"><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div></div>';

															   }
																 final_html = final_html + html_1;
																 $("#tax_info_array_edit").html(final_html);
																 $(".two-i").html(data.length);
															}
															Materialize.toast('TAX Info inserted successfully!', 2000,'green rounded');
														}
														else
														{
															html_1='';
															$("#tax_info_array_edit").html(html_1);
															Materialize.toast('ADD TAX DATA', 2000,'green rounded');
														}
													}
													else
													{
														Materialize.toast('Error while processing. Please try again', 2000,'red rounded');
													}
													$("#edit_add_other_tax").find("input[type=text], textarea").val("");
													$("#editpage_other-alert").modal('close');
											},

									});
								},
							});




							$("#set_services_alert").validate({
								rules:{
									select_service:{
										required:true,
									},
									alert_name:{
										required:true,
									},
								},
								messages:{
									select_service:{
										required:"Please Select Service.",
									},
									alert_name:{
										required:"Alert Name is required",
									},
								},
							});

						$(document).ready( function () {
							$(document).on('click' ,'.edit_remove_cmpgst', function(){
									var id = $(this).data('id');
									var bus_id = $(this).data('companyid');

									$('#remove_company_gstin_modal').modal('open');
									$('#remove_company_gstin_modal #remove_cmp_gst_id').val(id);
									$('#remove_company_gstin_modal #remove_cmp_gst_cmp_id').val(bus_id);

								});

								$('.remove_company_gstin_info').on('click',function(){

										var gst_id = $('#remove_cmp_gst_id').val();
										var companyid = $('#remove_cmp_gst_cmp_id').val();

										$.ajax({
											url:base_url+'profile/delete_gst_info',
											type:"post",
											data:{'csrf_test_name':csrf_hash,"gst_id":gst_id,"companyid":companyid},
											success:function(res){
												//console.log(res);
													//location.reload();
													if(res!= false)
													{
														var data=JSON.parse(res);
														var html_1 ='';
														var final_html ='';
														if(data != '')
														{
															for(var i=0;i<data.length;i++){
																if(data[i].status=="Inactive"){
													   var deac="deactive_record";
												   }else{
													    var deac="";
												   }
												   console.log(deac);
															if(i == 0)
															   {
																html_1='<div class="gst-boxs add-gst "><div class="col l1 s1 m1 sno"><div class="row">'+
															   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum '+deac+'"><span class="gstlabel ">GST NO.</span><p>'+data[i].gst_no+'</p></div><div class="col l6 s6 m6 gstplace '+deac+'"><span class="gstlabel">PLACE OF SUPPLY</span><p>'+data[i].place+'</p></div><div class="col l1 s1 m1 gstaction"><a class="edit_remove_cmpgst '+deac+'"  data-id="'+data[i].gst_id+'"  data-companyid="'+data[i].bus_id+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a><a class="edit_gst_info" data-id="'+data[i].gst_id+'" data-companyid="'+data[i].bus_id+'" data-gst_no="'+data[i].gst_no+'" data-gst_place="'+data[i].place+'" data-status="'+data[i].status+'"><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div>';
															   }
															   else
															   {
																 html_1='<div class="gstmore active"><div class="gst-boxs add-gst "><div class="col l1 s1 m1 sno"><div class="row">'+
															   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum '+deac+'"><span class="gstlabel ">GST NO.</span><p>'+data[i].gst_no+'</p></div><div class="col l6 s6 m6 gstplace '+deac+'"><span class="gstlabel">PLACE OF SUPPLY</span><p>'+data[i].place+'</p></div><div class="col l1 s1 m1 gstaction"><a class="edit_remove_cmpgst '+deac+'"  data-id="'+data[i].gst_id+'"  data-companyid="'+data[i].bus_id+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a><a class="edit_gst_info" data-id="'+data[i].gst_id+'" data-companyid="'+data[i].bus_id+'" data-gst_no="'+data[i].gst_no+'" data-gst_place="'+data[i].place+'" data-status="'+data[i].status+'"><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div></div>';

															   }
																 final_html = final_html + html_1;
																 $("#company_gst_array_list_edit").html(final_html);
																 $(".tot_gst").html(data.length);
																 $('#showgstboxs i').html('keyboard_arrow_up');
															}
															Materialize.toast('GSTIN Details deactivated', 2000,'green rounded');
														}
														else
														{
															Materialize.toast('GSTIN Details deactivated', 2000,'green rounded');
															html_1='<div class="gst-boxs add-gst"></div>';
															$("#company_gst_array_list_edit").html(html_1);
															$(".tot_gst").html(0);
														}
													}
													else
													{
														html_1='<div class="gst-boxs add-gst"></div>';
														$("#company_gst_array_list_edit").html(html_1);
														Materialize.toast('Error while processing!', 2000,'red rounded');
													}
												},
											});
									});

								});
								$('#services_array_list_edit').on('click','.editcmp_remove_service' ,function(){

									var id = $('.editcmp_remove_servFice').data('service_id');
									var company_id = $('.editcmp_remove_service').data('company_id');
									console.log(id+':::'+company_id);
									//$('#remove_company_service_modal').modal('open');
									$('#remove_company_service_modal #rem_cmp_service_id').val(id);
									$('#remove_company_service_modal #rem_service_cmp_id').val(company_id);
								});
								//$('#company_keywords_array_list').on('click','.remove_company_service_info' ,function(){
								$('.remove_company_service_info').on('click',function(){

									var service_id = $('#remove_company_service_modal #rem_cmp_service_id').val();
									var company_id = $('#remove_company_service_modal #rem_service_cmp_id').val();

									$.ajax({
										url:base_url+'profile/delete_service_info',
										type:"post",
										data:{'csrf_test_name':csrf_hash,"service_id":service_id,"company_id":company_id},
										success:function(res){
												if(res != false)
												{
												var data=JSON.parse(res);
												var html_1 ='';
												var final_html ='';
												if(data != '')
												{
													var ser=data[0].bus_services_keywords;
													var bus_id=data[0].bus_id;
													if(ser!=''){

													var services=ser.split('|@|');
													for(var i=0;i<services.length;i++){
													if(i == 0)
													   {
														html_1='<div class="popup-append-box-cover"> <div class="gst-boxs add-gst"> <div class="col l1 s1 m1 sno"> <div class="row">'+ (i+1)+'</div> </div> <div class="col l11 s1 m1"> <!--<div class="col l5 s5 m5 gstnum"><span class="gstlabel">KEYWORD</span> <p>KEYWORD '+(i+1)+'</p> </div>--> <div class="col l11 s6 m6 gstplace"><span class="gstlabel">SERVICE</span> <p>'+services[i]+'</p> </div> <div class="col l1 s1 m1 gstaction"> <a class="editcmp_remove_service modal-trigger" data-target="remove_company_service_modal" data-service_id="'+services[i]+'" data-company_id="'+bus_id+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a> </div> </div> </div> </div>';
													   }
													   else
													   {
														 html_1='<div class="popup-append-box popup-append-box-cover"> <div class="gst-boxs add-gst"> <div class="col l1 s1 m1 sno"> <div class="row">'+ (i+1)+'</div> </div> <div class="col l11 s1 m1"> <!--<div class="col l5 s5 m5 gstnum"><span class="gstlabel">KEYWORD</span> <p>KEYWORD '+(i+1)+'</p> </div>--> <div class="col l11 s6 m6 gstplace"><span class="gstlabel">SERVICE</span> <p>'+services[i]+'</p> </div> <div class="col l1 s1 m1 gstaction"> <a class="editcmp_remove_service modal-trigger" data-target="remove_company_service_modal" data-service_id="'+services[i]+'" data-company_id="'+bus_id+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a> </div> </div> </div> </div>';

													   }
														 final_html = final_html + html_1;
														 $("#services_array_list_edit").html(final_html);
														 $(".tot_service").html(services.length);
													}
													}else{
													$("#services_array_list_edit").html('');
													 $(".tot_service").html(0);
													}
													Materialize.toast('Service Keyword has been removed!!', 2000,'green rounded');
												}
												else
												{
													Materialize.toast('Service Keyword has been removed!!', 2000,'green rounded');
													html_1='<div class="gst-boxs add-gst">Add SERVICES</div>';
													$("#services_array_list_edit").html(html_1);
												}


											} else {
												Materialize.toast('Error while processing...!!', 2000,'red rounded');
											}
											$("#remove_company_service_modal").find("input[type=text], textarea").val("");
											$("#remove_company_service_modal").modal('close');
											},
										});
								});
								$('#company_keywords_array_list').on('click','.cmp_remove_service' ,function(){

									var id = $(this).data('service_id');
									console.log(id);
									//$('#remove_company_service_modal').modal('open');
									$('#remove_cmp_service #remove_cmp_service_id').val(id);

								});
								//$('#company_keywords_array_list').on('click','.remove_company_service_info' ,function(){
								$('.remove_sess_service').on('click',function(){

									var service_id = $('#remove_cmp_service #remove_cmp_service_id').val();

									$.ajax({
										url:base_url+'profile/remove_keywords',
										type:"post",
										data:{'csrf_test_name':csrf_hash,"service_id":service_id},
										success:function(res){
											if(res != false)
											{
											var data=JSON.parse(res);
											var html_1 ='';
											var final_html ='';
											//console.log(data);
											if(data != '')
											{
												var ser=data['bus_services_keywords'];
												//var bus_id=data[0].bus_id;
												//console.log(ser);
												if(ser!=''){
												var services=ser.split('|@|');

												for(var i=0;i<services.length;i++){
												if(i == 0)
												   {
													html_1='<div class="popup-append-box-cover"> <div class="gst-boxs add-gst"> <div class="col l1 s1 m1 sno"> <div class="row">'+ (i+1)+'</div> </div> <div class="col l11 s1 m1"> <!--<div class="col l5 s5 m5 gstnum"><span class="gstlabel">KEYWORD</span> <p>KEYWORD '+(i+1)+'</p> </div>--> <div class="col l11 s6 m6 gstplace"><span class="gstlabel">SERVICE</span> <p>'+services[i]+'</p> </div> <div class="col l1 s1 m1 gstaction"> <a class="cmp_remove_service modal-trigger" data-target="remove_cmp_service" data-service_id="'+services[i]+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a> </div> </div> </div> </div>';
												   }
												   else
												   {
													 html_1='<div class="popup-append-box popup-append-box-cover"> <div class="gst-boxs add-gst"> <div class="col l1 s1 m1 sno"> <div class="row">'+ (i+1)+'</div> </div> <div class="col l11 s1 m1"> <!-- <div class="col l5 s5 m5 gstnum"><span class="gstlabel">KEYWORD</span> <p>KEYWORD '+(i+1)+'</p> </div>--> <div class="col l11 s6 m6 gstplace"><span class="gstlabel">SERVICE</span> <p>'+services[i]+'</p> </div> <div class="col l1 s1 m1 gstaction"> <a class="cmp_remove_service modal-trigger" data-target="remove_cmp_service" data-service_id="'+services[i]+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a> </div> </div> </div> </div>';

												   }
													 final_html = final_html + html_1;
													 $("#company_keywords_array_list").html(final_html);
													 $(".tot_service").html(services.length);
												}
												}else{
													$("#company_keywords_array_list").html('');
													 $(".tot_service").html(0);
												}
												Materialize.toast('Service Keyword has been removed!!', 2000,'green rounded');

											}
											else
											{
												Materialize.toast('Service Keyword has been removed!!', 2000,'green rounded');
												html_1='<div class="gst-boxs add-gst">Add SERVICES</div>';
												$("#company_keywords_array_list").html(html_1);
											}


										} else {
											Materialize.toast('Error while processing...!!', 2000,'red rounded');
										}
										$("#remove_cmp_service").find("input[type=text]").val("");
										$("#remove_cmp_service").modal('close');
										$('#cmp_keyboard i').html('keyboard_arrow_down');
											/*	if(res != false)
												{
												var data=JSON.parse(res);
												var html_1 ='';
												var final_html ='';
												if(data != '')
												{
													var ser=data[0].bus_services_keywords;
													var bus_id=data[0].bus_id;
													//console.log(ser);
													var services=ser.split('|@|');
													for(var i=0;i<services.length;i++){
													if(i == 0)
													   {
														html_1='<div class="popup-append-box-cover"><div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
													   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">KEYWORD</span><p>KEYWORD '+(i+1)+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">SERVICE</span><p>'+services[i]+'</p></div><div class="col l1 s1 m1 gstaction"><a class="editcmp_remove_service modal-trigger" data-target="remove_company_service_modal" data-service_id="'+services[i]+'" data-company_id="'+bus_id+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a></div></div></div></div>';
													   }
													   else
													   {
														 html_1='<div class="popup-append-box popup-append-box-cover"><div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
													   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">KEYWORD</span><p>KEYWORD '+(i+1)+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">SERVICE</span><p>'+services[i]+'</p></div><div class="col l1 s1 m1 LegalRemoveImage"><a class="editcmp_remove_service modal-trigger" data-target="remove_company_service_modal" data-service_id="'+services[i]+'" data-company_id="'+bus_id+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a></div></div></div></div>';

													   }
														 final_html = final_html + html_1;
														 $("#services_array_list_edit").html(final_html);
														 $(".tot_service").html(services.length);
													}
													Materialize.toast('Service Keyword has been removed!!', 2000,'green rounded');
												}
												else
												{
													html_1='<div class="gst-boxs add-gst">Add SERVICES</div>';
													$("#services_array_list_edit").html(html_1);
												}


											} else {
												Materialize.toast('Error while processing...!!', 2000,'red rounded');
											}
											$("#remove_company_service_modal").find("input[type=text], textarea").val("");
											$("#remove_company_service_modal").modal('close');*/
											},
										});
								});

									$('.add_new_service_2').on('click',function(){
										var company_id = $(this).data('company_id');
										$('#edit-add-new-service').modal('open');
										$('#edit-add-new-service #add_for_company_id').val(company_id);
									});
								$("#add_new_service_keywords_2").submit(function(e){
										e.preventDefault();
									}).validate({
										rules:{
											keyword_1:{
												required:true,
											},
										},
										messages:{
											keyword_1:{
												required:"Add at least one service keyword",
											},
										},
										submitHandler:function(form){

												var frm=$(form).serialize();
														$.ajax({
														url:base_url+'profile/add_service_keywords_2',
														type:"POST",
														data:{'csrf_test_name':csrf_hash,"frm":frm,},
														success:function(res){

															if(res != false)
															{
																var data=JSON.parse(res);
																var html_1 ='';
																var final_html ='';
																if(data != '')
																{
																	var ser=data[0].bus_services_keywords;
																	var bus_id=data[0].bus_id;
																	if(ser!=''){

																	var services=ser.split('|@|');
																	for(var i=0;i<services.length;i++){
																	if(i == 0)
																	   {
																		html_1='<div class="popup-append-box-cover"> <div class="gst-boxs add-gst"> <div class="col l1 s1 m1 sno"> <div class="row">'+ (i+1)+'</div> </div> <div class="col l11 s1 m1"> <!--<div class="col l5 s5 m5 gstnum"><span class="gstlabel">KEYWORD</span> <p>KEYWORD '+(i+1)+'</p> </div>--> <div class="col l11 s6 m6 gstplace"><span class="gstlabel">SERVICE</span> <p>'+services[i]+'</p> </div> <div class="col l1 s1 m1 gstaction"> <a class="editcmp_remove_service modal-trigger" data-target="remove_company_service_modal" data-service_id="'+services[i]+'" data-company_id="'+bus_id+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a> </div> </div> </div> </div>';
																	   }
																	   else
																	   {
																		 html_1='<div class="popup-append-box popup-append-box-cover"> <div class="gst-boxs add-gst"> <div class="col l1 s1 m1 sno"> <div class="row">'+ (i+1)+'</div> </div> <div class="col l11 s1 m1"> <!--<div class="col l5 s5 m5 gstnum"><span class="gstlabel">KEYWORD</span> <p>KEYWORD '+(i+1)+'</p> </div>--> <div class="col l11 s6 m6 gstplace"><span class="gstlabel">SERVICE</span> <p>'+services[i]+'</p> </div> <div class="col l1 s1 m1 gstaction"> <a class="editcmp_remove_service modal-trigger" data-target="remove_company_service_modal" data-service_id="'+services[i]+'" data-company_id="'+bus_id+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a> </div> </div> </div> </div>';

																	   }
																		 final_html = final_html + html_1;
																		 $("#services_array_list_edit").html(final_html);
																		 $(".tot_service").html(services.length);
																	}
																	}else{
																			$("#services_array_list_edit").html('');
																			$(".tot_service").html(0);
																		}
																		Materialize.toast('Service Info has been added.', 2000,'green rounded');
																}
																else
																{
																	Materialize.toast('Service Info has been added.', 2000,'green rounded');
																	html_1='<div class="gst-boxs add-gst">Add SERVICES</div>';
																	$("#services_array_list_edit").html(html_1);
																}


															} else {
																Materialize.toast('Error while processing...!!', 2000,'red rounded');
															}
															$("#add_new_service_keywords_2").find("input[type=text], textarea").val("");
															$("#edit-add-new-service").modal('close');
															//location.reload();
														},
											});
										},
									});

								$('.add_new_branch_cmp').on('click',function(){
										var company_id = $(this).data('company_id');
										var user_id = $(this).data('user_id');
										$('#edit-new-branch').modal('open');
										$('#edit-new-branch #add_branch_for_cmp').val(company_id);
										$('#edit-new-branch #add_branch_for_user').val(user_id);
								});
								$("#add_branch_info_2").submit(function(e){
										e.preventDefault();
									}).validate({
										rules:{
											branch_name:{
												required:true,
											},
											branch_city:{
												required:true,
											},
										},
										messages:{
											branch_name:{
												required:"Branch Name is required",
											},
											branch_city:{
												required:"Branch City is required",
											},
										},
										submitHandler:function(form){

												var frm=$(form).serialize();
														$.ajax({
														url:base_url+'profile/add_branch_info_2',
														type:"POST",
														data:{'csrf_test_name':csrf_hash,"frm":frm},
														success:function(res){
															//console.log(res);
															if(res != false)
															{
																var data=JSON.parse(res);
																var html_1 ='';
																var final_html ='';
																if(data != '')
																{
																	for(var i=0;i<data.length;i++){
																	if(i == 0)
																	   {
																		html_1='<div class="popup-append-box-cover"><div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
																	   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">BRANCH</span><p>'+data[i].cbranch_name+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">CITY</span><p>'+data[i].cbranch_city+'</p></div><div class="col l1 s1 m1 gstaction"><a class="editcmp_remove_branch" data-branch_id="'+data[i].cbranch_id+'" data-company_id="'+data[i].bus_id+' "><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a></div></div></div></div>';
																	   }
																	   else
																	   {
																		 html_1='<div class="popup-append-box popup-append-box-cover"><div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
																	   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">BRANCH</span><p>'+data[i].cbranch_name+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">CITY</span><p>'+data[i].cbranch_city+'</p></div><div class="col l1 s1 m1 gstaction"><a class="editcmp_remove_branch" data-branch_id="'+data[i].cbranch_id+'" data-company_id="'+data[i].bus_id+' "><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a></div></div></div></div>';

																	   }
																		 final_html = final_html + html_1;
																		 $("#branch_array_list_edit").html(final_html);
																		 $(".tot_branch").html(data.length);
																	}
																	Materialize.toast('Branch information added', 2000,'green rounded');
																}
																else
																{
																	Materialize.toast('Branch information added', 2000,'green rounded');
																	html_1='<div class="gst-boxs add-gst">Add Branch</div>';
																	$("#branch_array_list_edit").html(html_1);
																}


												} else {
													Materialize.toast('Error while processing...!!', 2000,'red rounded');
												}
												$("#add_branch_info_2").find("input[type=text], textarea").val("");
												$("#edit-new-branch").modal('close');
												//location.reload();
											},
											});
										},
									});
									$('#branch_array_list_edit').on('click' ,'.editcmp_remove_branch',function(){

										var branch_id = $(this).data('branch_id');
										var company_id = $(this).data('company_id');

										$('#remove_company_branch_modal').modal('open');
										$('#remove_company_branch_modal #remove_cmp_branch_id').val(branch_id);
										$('#remove_company_branch_modal #remove_branch_cmp_id').val(company_id);
									});

									$('.remove_company_branch_info').on('click',function(){

										var branch_id = $('#remove_cmp_branch_id').val();
										var company_id = $('#remove_branch_cmp_id').val();

										$.ajax({
											url:base_url+'profile/delete_branch_info',
											type:"post",
											data:{'csrf_test_name':csrf_hash,"branch_id":branch_id,"company_id":company_id},
											success:function(res){
													//console.log(res);
												if(res != false)
													{
														var data=JSON.parse(res);
														var html_1 ='';
														var final_html ='';
														if(data != '')
														{
															for(var i=0;i<data.length;i++){
															if(i == 0)
															   {
																html_1='<div class="popup-append-box-cover"><div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
															   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">BRANCH</span><p>'+data[i].cbranch_name+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">CITY</span><p>'+data[i].cbranch_city+'</p></div><div class="col l1 s1 m1 gstaction"><a class="editcmp_remove_branch" data-branch_id="'+data[i].cbranch_id+'" data-company_id="'+data[i].bus_id+' "><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a></div></div></div></div>';
															   }
															   else
															   {
																 html_1='<div class="popup-append-box popup-append-box-cover"><div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
															   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">BRANCH</span><p>'+data[i].cbranch_name+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">CITY</span><p>'+data[i].cbranch_city+'</p></div><div class="col l1 s1 m1 gstaction"><a class="editcmp_remove_branch" data-branch_id="'+data[i].cbranch_id+'" data-company_id="'+data[i].bus_id+' "><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a></div></div></div></div>';

															   }
																 final_html = final_html + html_1;
																 $("#branch_array_list_edit").html(final_html);
																 $(".tot_branch").html(data.length);
															}
															Materialize.toast('Branch information removed', 2000,'green rounded');
														}
														else
														{
															Materialize.toast('Branch information removed', 2000,'green rounded');
															html_1='<div class="gst-boxs add-gst">Add Branch</div>';
															$("#branch_array_list_edit").html(html_1);
														}
													} else {
														Materialize.toast('Error while processing...!!', 2000,'red rounded');
													}
													$("#remove_company_branch_modal").find("input[type=text], textarea").val("");
													$('#remove_company_branch_modal').modal('close');

												},
											});
									});


						$(document).ready( function () {
								$('#alert_info_array_edit').on('click', '.remove_service_alert_edit' ,function(){
									var alert_id = $(this).data('alert_id');
									var service_id = $(this).data('service_id');

									//$('#remove_alert_info').modal('open');
									$('#remove_alert_info #remove_alert_id').val(alert_id);
									$('#remove_alert_info #remove_alert_service_id').val(service_id);
								});

							$('.edit_remove_alert').on('click',function(){

										var alert_id = $('#remove_alert_id').val();
										var service_id = $('#remove_alert_service_id').val();

										$.ajax({

											url:base_url+'services/delete_alert_info',

											type:"post",

											data:{'csrf_test_name':csrf_hash,"alert_id":alert_id,"service_id":service_id},

											success:function(res){
													if(res!= false)
													{
														var data=JSON.parse(res);
														var html_1 ='';
														var final_html ='';
														if(data != '')
														{
															for(var i=0;i<data.length;i++){
																var notification = '';
																   if(data[i].alert_notification == 1) {
																	notification = 'Web Reminder';
																   }
																   if(data[i].alert_msg == 1) {
																   		if(notification!=''){
																   			notification = notification+', '+'Message';
																   		}else{
																   			notification = 'Message';
																   		}

																   }
																   if(data[i].alert_mail == 1){
																	   	if(notification!=''){
																   			notification = notification+', '+'Email';
																   		}else{
																   			notification = 'Email';
																   		}
																		//notification = 'Email';
																   }
																var d = new Date(data[i].alert_date);
																var month = d.getMonth()+1;
																var day = d.getDate();

																var alert_date =  (month<10 ? '0' : '') + month + '/' +
																    (day<10 ? '0' : '') + day + '/' + d.getFullYear();
																var dd = new Date(data[i].alert_reminder);
																var mmonth = dd.getMonth()+1;
																var dday = dd.getDate();

																var alert_reminder =  (mmonth<10 ? '0' : '') + mmonth + '/' +
																    (dday<10 ? '0' : '') + dday + '/' + dd.getFullYear();

																		if(i == 0)
																		   {
																			html_1='<div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
																		   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">ALERT NAME</span><p>'+data[i].alert_name+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">NOTIFICATION TYPE</span><p>'+notification+'</p></div><div class="col l1 s1 m1 gstaction"><a class="remove_service_alert_edit modal-trigger" data-alert_id="'+data[i].id+'" data-service_id="'+data[i].parent_id+'" data-target="remove_alert_info"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a><a class="edit_alert_info modal-trigger" data-alert_id="'+data[i].id+'" data-service_id="'+data[i].parent_id+'" data-alert_name="'+data[i].alert_name+'" data-alert_target="'+data[i].alert_target+'" data-set_condition="'+data[i].alert_condition+'" data-alert_number="'+data[i].alert_number+'" data-alert_reminder="'+alert_reminder+'" data-alert_date="'+alert_date+'" data-alert_mobile="'+data[i].alert_mobile+'" data-alert_email="'+data[i].alert_email+'" data-alert_msg="'+data[i].alert_msg+'" data-alert_mail="'+data[i].alert_mail+'" data-alert_notification="'+data[i].alert_notification+'" data-target="edit_alert"><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div>';
																		   }
																		   else
																		   {
																			 html_1='<div class="alertmore"><div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+(i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">ALERT NAME</span><p>'+data[i].alert_name+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">NOTIFICATION TYPE</span><p>'+notification+'</p></div><div class="col l1 s1 m1 gstaction"><a class="remove_service_alert_edit modal-trigger" data-alert_id="'+data[i].id+'" data-service_id="'+data[i].parent_id+'" data-target="remove_alert_info"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a><a class="edit_alert_info modal-trigger" data-alert_id="'+data[i].id+'" data-service_id="'+data[i].parent_id+'" data-alert_name="'+data[i].alert_name+'" data-alert_target="'+data[i].alert_target+'" data-alert_reminder="'+alert_reminder+'" data-set_condition="'+data[i].alert_condition+'" data-alert_number="'+data[i].alert_number+'" data-alert_date="'+alert_date+' data-alert_mobile="'+data[i].alert_mobile+'" data-alert_email="'+data[i].alert_email+'" data-alert_msg="'+data[i].alert_msg+'" data-alert_mail="'+data[i].alert_mail+'" data-alert_notification="'+data[i].alert_notification+'" data-target="edit_alert"><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div></div></div>';

																		   }
																 final_html = final_html + html_1;
																 $("#alert_info_array_edit").html(final_html);
																// $(".two-i").html(data.length);
															}
															Materialize.toast('Alert Info removed successfully !', 2000,'green rounded');
														}
														else
														{
															html_1='';
															$("#alert_info_array_edit").html(html_1);
															Materialize.toast('Alert Info removed successfully !', 2000,'green rounded');
														}
													}
													else
													{
														html_1='';
														$("#alert_info_array_edit").html(html_1);
														Materialize.toast('Error while processing!', 2000,'red rounded');
													}
												},

											});
									});
								});
									$('.edit_add_alert_info').on('click',function(){
										var service_id = $(this).data('service_id');

										//$('#edit_add-alert').modal('open');
										$('#edit_add-alert #add_alert_for_service').val(service_id);

										/*var altr=$('#edit_add-alert #alert_number').val();
										//var alt_no=parseInt($('#re'+(altr-1)).data('alert_number'));
										if(=''){
											altr='';
										}else{
										altr=altr+1;
										}
										$('#edit_add-alert #alert_number').val(altr);*/
									});
									$("#edit_add_alert_details").submit(function(e){
										e.preventDefault();
									}).validate({
										rules:{
											alert_name:{
												required:true,
											},
											alert_target:{
												number:true,
											},
											alert_mobile:{
												//mobileno:true,
												required:true,
												digits:true,
											},
											alert_notification:{
									            required:function(e){
									            	if($('#alert_notification').val()=='' && $('#alert_msg').val()=='' && $('#alert_mail').val()==''){
									            		return true;
									            	}else{
									            		return false;
									            	}
									            }
									        },

										},
										messages:{
											alert_name:{
												required:"Alert Name is Required",
											},
											alert_target:{
												number:"Enter Number only",
											},
											alert_mobile:{
												required:"Mobile Number is required",
											},
											alert_notification:{
									            required:"Alert Type is required",
      										},
										},
										errorPlacement: function(error, element) {
								           if (element.attr("name") == "alert_notification") {
								           	  $("#notification_error").html(error)
								           }else{
								           	error.insertAfter(element);
								           }
								        },
										submitHandler:function(form){

												var frm=$(form).serialize();
														$.ajax({

														url:base_url+'services/add_alert_service',
														type:"POST",
														data:{'csrf_test_name':csrf_hash,"frm":frm,},
														success:function(res){
															if(res!= false)
															{
																var data=JSON.parse(res);
																var html_1 ='';
																var final_html ='';
																var alt_num=$("#edit_add_alert_details #alert_number").val();
																alt_num=alt_num.replace("IA","");

																if(data != '')
																{
																	for(var i=0;i<data.length;i++){
																		var notification = '';
																		   if(data[i].alert_notification == 1) {
																				notification = 'Reminder';
																			   }
																			   if(data[i].alert_msg == 1) {
																			   		if(notification!=''){
																			   			notification = notification+', '+'Message';
																			   		}else{
																			   			notification = 'Message';
																			   		}

																			   }
																			   if(data[i].alert_mail == 1){
																				   	if(notification!=''){
																			   			notification = notification+', '+'Email';
																			   		}else{
																			   			notification = 'Email';
																			   		}
																					//notification = 'Email';
																			   }
																	   		var d = new Date(data[i].alert_date);
																			var month = d.getMonth()+1;
																			var day = d.getDate();

																			var alert_date =  (month<10 ? '0' : '') + month + '/' +
																			    (day<10 ? '0' : '') + day + '/' + d.getFullYear();
																			var dd = new Date(data[i].alert_reminder);
																			var mmonth = dd.getMonth()+1;
																			var dday = dd.getDate();

																			var alert_reminder =  (mmonth<10 ? '0' : '') + mmonth + '/' +
																			    (dday<10 ? '0' : '') + dday + '/' + dd.getFullYear();
																	if(i == 0)
																		   {
																			html_1='<div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
														   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">ALERT NAME</span><p>'+data[i].alert_name+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">NOTIFICATION TYPE</span><p>'+notification+'</p></div><div class="col l1 s1 m1 gstaction"><a class="remove_service_alert_edit modal-trigger" data-alert_id="'+data[i].id+'" data-service_id="'+data[i].parent_id+'" data-target="remove_alert_info"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a><a class="edit_alert_info modal-trigger" data-alert_id="'+data[i].id+'" data-service_id="'+data[i].parent_id+'" data-alert_name="'+data[i].alert_name+'" data-alert_target="'+data[i].alert_target+'" data-set_condition="'+data[i].alert_condition+'" data-alert_number="'+data[i].alert_number+'" data-alert_date="'+alert_date+'" data-alert_reminder="'+alert_reminder+'" data-alert_mobile="'+data[i].alert_mobile+'" data-alert_email="'+data[i].alert_email+'" data-alert_mail="'+data[i].alert_mail+'" data-alert_msg="'+data[i].alert_msg+'" data-alert_notification="'+data[i].alert_notification+'"  data-target="edit_alert"><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div>';
																		   }
																		   else
																		   {
																			 html_1='<div class="alertmore"><div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+(i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">ALERT NAME</span><p>'+data[i].alert_name+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">NOTIFICATION TYPE</span><p>'+notification+'</p></div><div class="col l1 s1 m1 gstaction"><a class="remove_service_alert_edit modal-trigger" data-alert_id="'+data[i].id+'" data-service_id="'+data[i].parent_id+'" data-target="remove_alert_info"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a><a class="edit_alert_info modal-trigger" data-alert_id="'+data[i].id+'" data-service_id="'+data[i].parent_id+'" data-alert_name="'+data[i].alert_name+'" data-alert_target="'+data[i].alert_target+'" data-alert_reminder="'+alert_reminder+'" data-set_condition="'+data[i].alert_condition+'" data-alert_number="'+data[i].alert_number+'" data-alert_date="'+alert_date+'" data-alert_mobile="'+data[i].alert_mobile+'"" data-alert_email="'+data[i].alert_email+'" data-alert_mail="'+data[i].alert_mail+'" data-alert_msg="'+data[i].alert_msg+'" data-alert_notification="'+data[i].alert_notification+'" data-target="edit_alert"><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div></div></div>';

																		   }
																		 final_html = final_html + html_1;
																		 $("#alert_info_array_edit").html(final_html);

																	}
																	Materialize.toast('An alert has been set!', 2000,'green rounded');
																}
																else
																{
																	html_1='';
																	$("#alert_info_array_edit").html(html_1);
																	Materialize.toast('Add alert information!', 2000,'green rounded');
																}
															}
															else
															{
																html_1='';
																$("#alert_info_array_edit").html(html_1);
																Materialize.toast('Error while processing!', 2000,'red rounded');
															}

															$("#edit_add_alert_details").find("input[type=text],input[type=email], textarea").val("");

															var d = new Date();
															var month = d.getMonth()+1;
															var day = d.getDate();

															var output =  (month<10 ? '0' : '') + month + '/' +
															    (day<10 ? '0' : '') + day + '/' + d.getFullYear();
															$("#edit_add_alert_details #alert_date").val(output);
															$('.notif_type img').removeClass('selected_notification');
															//$('.noti_mobile,.noti_email').css('display','none');
															$('#edit_add-alert a[class="notif_type"][data-notif_type="1"]').find('img').attr('src',base_path()+"asset/css/img/icons/bell-grey.png");
															$('#edit_add-alert a[class="notif_type"][data-notif_type="2"]').find('img').attr('src',base_path()+"asset/css/img/icons/msg-grey.png");
															$('#edit_add-alert a[class="notif_type"][data-notif_type="3"]').find('img').attr('src',base_path()+"asset/css/img/icons/mail-grey.png");
															$('#edit_add-alert #alert_mobile,#edit_add-alert #alert_email').attr('readonly','readonly').parents('.input-field').children().removeClass('active');
															$("#edit_add_alert_details #alert_number").val("IA"+(pad(parseInt(alt_num)+1,3)));
															$("#edit_add-alert").modal('close');
													},
											});
										},
									});

								$(document).ready( function () {
									$(document).on('click', '.remove_service_alert_session' ,function(){
										var alert_name = $(this).data('alert_name');
										var alert_no = $(this).data('alert_no');

										//$('#remove_alert_data').modal('open');
										$('#remove_alert_data #remove_alert_name').val('');
										$('#remove_alert_data #remove_alert_no').val('');
										$('#remove_alert_data #remove_alert_name').val(alert_name);
										$('#remove_alert_data #remove_alert_no').val(alert_no);
									});

									$('.remove_alert_data_session').on('click',function(){

										var alert_name = $('#remove_alert_name').val();
										var alert_no = $('#remove_alert_no').val();

										$.ajax({

											url:base_url+'services/remove_services_alert',

											type:"post",

											data:{'csrf_test_name':csrf_hash,"alert_name":alert_name,"alert_no":alert_no},

											success:function(res){
												if(res!= false)
												{
													var data=JSON.parse(res);
													//console.log(data);
													var html_1 ='';
													var final_html ='';
													if(data != '')
													{
														for(var i=0;i<data.length;i++){
															var notification = '';

															   if(data[i].alert_notification == 1) {
																notification = 'Reminder';
															   }
															   if(data[i].alert_msg == 1) {
															   		if(notification!=''){
															   			notification = notification+', '+'Message';
															   		}else{
															   			notification = 'Message';
															   		}

															   }
															   if(data[i].alert_mail == 1){
																   	if(notification!=''){
															   			notification = notification+', '+'Email';
															   		}else{
															   			notification = 'Email';
															   		}

															   }
															if(i == 0)
														   {
															html_1='<div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
										   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">ALERT NAME</span><p>'+data[i].alert_name+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">NOTIFICATION TYPE</span><p>'+notification+'</p></div><div class="col l1 s1 m1 gstaction"><a data-alert_name="'+data[i].alert_name+'" data-alert_no="'+data[i].alert_number+'" class="remove_service_alert_session modal-trigger" data-target="remove_alert_data"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a></div></div></div>';
														   }
														   else
														   {
															 html_1='<div class="alertmore"><div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
										   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">ALERT NAME</span><p>'+data[i].alert_name+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">NOTIFICATION TYPE</span><p>'+notification+'</p></div><div class="col l1 s1 m1 gstaction"><a  data-alert_name="'+data[i].alert_name+'" data-alert_no="'+data[i].alert_number+'" class="remove_service_alert_session modal-trigger" data-target="remove_alert_data"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a></div></div></div>';

														   }
															 final_html = final_html + html_1;
															 $("#alert_info_array").html(final_html);
														}
														Materialize.toast('Alert information has been removed!', 2000,'green rounded');
													}
													else
													{
														Materialize.toast('Alert information has been removed!', 2000,'green rounded');
														html_1='<div class="gst-boxs add-gst">Add Alert Data</div>';
														$("#alert_info_array").html(html_1);
														//Materialize.toast('Alert information has been removed!', 2000,'green rounded');
													}
												}
												else
												{
													html_1='<div class="gst-boxs add-gst">Add Alert Data</div>';
													$("#alert_info_array").html(html_1);
													Materialize.toast('Error while processing!', 2000,'red rounded');
												}
												$("#remove_alert_data").modal('close');
												},

											});
									});
								});
									/*var bulk_invoices = [];
										$("input[id='invoices_bulk']").on("click",function(){
										if($(this).is(':checked',true)) {
											$(".invoice_bulk_action").prop('checked', true);
											$(".invoice_bulk_action:checked").each(function() {
													bulk_invoices.push($(this).val());
												});
												bulk_invoices = bulk_invoices.join(",");
												$('#deactive_multiple_invoices').attr('data-multi_invoices',bulk_invoices);
											}
											else {
												$(".invoice_bulk_action").prop('checked',false);
												bulk_invoices = [];
												$('#deactive_multiple_invoices').attr('data-multi_invoices',0);
											}
									});

									$("#deactive_multiple_invoices").on('click', function() {
										var invoice_values = $(this).data('multi_invoices');
										if(invoice_values == '' || invoice_values == '0')
										{
											Materialize.toast('Please select a record first', 2000,'red rounded');
										}
										else
										{
											$.ajax({
												url:base_url+'My_invoices/deactive_multiple',
												type:"POST",
												data:{"invoice_values":invoice_values,},
												success:function(res){
														location.reload();
													},
											});
										}
									});*/

									/* $("#create_invoice").validate({
										rules:{
											invoice_date:{
												required:true,
											},
											customer_id:{
												required:true,
											},
										},
										messages:{
											invoice_date:{
												required:"Invoice Date is required",
											},
											customer_id:{
												required:"Please Select Customer",
											},
										},
									});

									 $("#edit_invoice").validate({
										rules:{
											invoice_date:{
												required:true,
											},
											customer_id:{
												required:true,
											},
										},
										messages:{
											invoice_date:{
												required:"Invoice Date is required",
											},
											customer_id:{
												required:"Please Select Customer",
											},
										},
									});*/

									$('.mail_all_invoice').on('click',function(){
										$.ajax({
											url:base_url+'My_invoices/send_email_to_all_customers',
											type:"POST",
											data:{'csrf_test_name':csrf_hash,},
											success:function(res){
														Materialize.toast('Email sent to all clients', 2000,'green rounded');
												},
											});
									});


								$("#invoice_new_service").submit(function(e){
									e.preventDefault();
								}).validate({
									rules:{
										invoice_service_name:{
											required:true,
										},
										invoice_hsn_sac_no:{
											required:true,
										},
									},
									messages:{
										invoice_service_name:{
											required:"Item / Service Name is required",
										},
										invoice_hsn_sac_no:{
											required:"HSN / SAC Number is required",
										},
									},
									submitHandler:function(form){
											var frm=$(form).serialize();
													$.ajax({
													url:base_url+'My_services/add_new_service_from_invoice',
													type:"POST",
													data:{'csrf_test_name':csrf_hash,"frm":frm,},
													success:function(res){
															if(res != false)
															{
																$("#invoice_new_service").find("input[type=text],textarea,option").val("");
																$('#invoice_add_new_service').modal('close');
																$("#selected_service").html(res);
																$("#selected_service").parents('.input-field').addClass('label-active');
																$('#selected_service').material_select();
																Materialize.toast('Services Details has been inserted successfully.', 2000,'green rounded');
															}
														},
										});
									},
								});

								$('#document_type').on('change', function () {
									$.ajax({
										'url':base_url+'My_invoices/generate_invoice_no',
										type:"POST",
										data:{
											'csrf_test_name':csrf_hash,
											'doc_type':$(this).val(),
											'company_id':$('#company_name').val(),
										},
										success:function(res){
											$("#invoice_no").val(res);
										},
									});
										if($(this).val() == '5') {
											$(".show_sales_export").css('display','block');
										}
										else{
											$(".show_sales_export").css('display','none');
										}
										if($(this).val() == '4') {
											$(".show_sales_recurring").css('display','block');
										}
										else{
											$(".show_sales_recurring").css('display','none');
										}
								});

								$("#create_new_service_m").validate({
									rules:{
										service_name:{
											required:true,
										},
										han_sac_no:{
											required:true,
										},
										sku:{
											required:true,
										},
									},
									messages:{
										service_name:{
											required:"Item Name is required",
										},
										han_sac_no:{
											required:"HSN / SAC is required",
										},
										sku:{
											required:"SKU is required",
										},
									},
								});

			$("#add_other_tax_m").submit(function(e){
				e.preventDefault();
			}).validate({
				rules:{

					'tax_name[]':{
						required:true,
					},
					'tax_perc[]':{
						required:true,
					},
				},
				messages:{

					'tax_name[]':{
						required:"Tax Name is required",
					},
					'tax_perc[]':{
						required:"Tax Percentage is required",
					},
				},
				submitHandler:function(form){

						var frm=$(form).serialize();
								$.ajax({
								url:base_url+'My_services/add_other_tax',
								type:"POST",
								data:{'csrf_test_name':csrf_hash,"frm":frm,},
								success:function(res){
									if(res!= false)
									{
										var data=JSON.parse(res);
										var html_1 ='';
										var final_html ='';

										if(data != '')
										{
											for(var i=0;i<data.length;i++){
											if(i == 0)
											   {
												html_1='<div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">NAME OF TAX</span><p>'+data[i].tax_name+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">TAX PERCENTAGE</span><p>'+data[i].tax_percentage+'</p></div><div class="col l1 s1 m1 gstaction"><a href="javascript:void(0);" class="remove_tax_m" data-tax_name="'+data[i].tax_name+'" data-tax_perc="'+data[i].tax_percentage+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a></div></div></div>';
											   }
											   else
											   {
												 html_1='<div class="gstmore"><div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">NAME OF TAX</span><p>'+data[i].tax_name+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">TAX PERCENTAGE</span><p>'+data[i].tax_percentage+'</p></div><div class="col l1 s1 m1 gstaction"><a href="javascript:void(0);" class="remove_tax_m" data-tax_name="'+data[i].tax_name+'" data-tax_perc="'+data[i].tax_percentage+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a></div></div></div></div>';

											   }
												 final_html = final_html + html_1;
												 $("#tax_info_array_m").html(final_html);
												 $(".tot_other_tax").html(data.length);
											}
											Materialize.toast('Tax Information have been set', 2000,'green rounded');
										}
										else
										{
											Materialize.toast('Tax Information have been set', 2000,'green rounded');
											html_1='<div class="gst-boxs add-gst">Add TAX Data</div>';
											$("#tax_info_array_m").html(html_1);
											$(".tot_other_tax").html(0);
										}
									}
									else
									{
										Materialize.toast('Error while processing. Please try again', 2000,'red rounded');
									}
									$("#add_other_tax_m").find("input[type=text], textarea").val("");
									$("#other-alert").modal('close');

						$(document).ready( function () {
								$(document).on('click', '.remove_tax_m', function(){

									var tax_name = $(this).data('tax_name');
									var tax_perc = $(this).data('tax_perc');

									$('#remove_tax_data_m').modal('open');
									$('#remove_tax_data_m #remove_tax_name').val(tax_name);
									$('#remove_tax_data_m #remove_tax_perc').val(tax_perc);

								});

									$('.remove_tax_data_m').off().on('click',function(){

										var tax_name = $('#remove_tax_name').val();
										var tax_perc = $('#remove_tax_perc').val();

										$.ajax({

											url:base_url+'My_services/remove_tax_info',

											type:"post",

											data:{'csrf_test_name':csrf_hash,"tax_name":tax_name,"tax_perc":tax_perc},

											success:function(res){
													if(res!= false)
													{
														var data=JSON.parse(res);
														//console.log(data);
														var html_1 ='';
														var final_html ='';
														if(data != '')
														{
															for(var i=0;i<data.length;i++){
															if(i == 0)
															   {
																html_1='<div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">NAME OF TAX</span><p>'+data[i].tax_name+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">TAX PERCENTAGE</span><p>'+data[i].tax_percentage+'</p></div><div class="col l1 s1 m1 gstaction"><a href="javascript:void(0);" class="remove_tax_m" data-tax_name="'+data[i].tax_name+'" data-tax_perc="'+data[i].tax_percentage+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a></div></div></div>';
															   }
															   else
															   {
																html_1='<div class="gstmore"><div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">NAME OF TAX</span><p>'+data[i].tax_name+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">TAX PERCENTAGE</span><p>'+data[i].tax_percentage+'</p></div><div class="col l1 s1 m1 gstaction"><a href="javascript:void(0);" class="remove_tax_m" data-tax_name="'+data[i].tax_name+'" data-tax_perc="'+data[i].tax_percentage+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a></div></div></div></div>';

															   }
																 final_html = final_html + html_1;
																 $("#tax_info_array_m").html(final_html);
																 $(".tot_other_tax").html(data.length);
															}
															Materialize.toast('Tax Information has been removed', 2000,'green rounded');
														}
														else
														{
															Materialize.toast('Tax Information has been removed', 2000,'green rounded');
															html_1='<div class="gst-boxs add-gst">Add TAX Data</div>';
															$("#tax_info_array_m").html(html_1);
															$(".tot_other_tax").html(0);
														}
													}
													else
													{
														html_1='<div class="gst-boxs add-gst">Add TAX Data</div>';
														$("#tax_info_array_m").html(html_1);
														$(".tot_other_tax").html(0);
													}
												},

											});
									});

									});
							},

					});
				},
			});


			/*$("#add_alert_details_m").submit(function(e){
				e.preventDefault();
			}).validate({
				rules:{
					alert_name:{
						required:true,
					},
				},
				messages:{
					alert_name:{
						required:"Alert Name is required",
					},
				},
				submitHandler:function(form){

						var frm=$(form).serialize();
								$.ajax({
								url:base_url+'My_services/add_alert_info',
								type:"post",
								data:{"frm":frm,},
								success:function(res){
									if(res!= false)
									{
										var data=JSON.parse(res);
										var html_1 ='';
										var final_html ='';
										if(data != '')
										{
											for(var i=0;i<data.length;i++){
												var notification = '';
												   if(data[i].notification_type == '1') {
													notification = 'Remainder';
												   }else if(data[i].notification_type == '2') {
														notification = 'Message';
												   } else if(data[i].notification_type == '3'){
														notification = 'Email';
												   } else {
													   notification = '';
												   }
												if(i == 0)
											   {
												html_1='<div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
							   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">ALERT NAME</span><p>'+data[i].alert_name+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">NOTIFICATION TYPE</span><p>'+notification+'</p></div><div class="col l1 s1 m1 gstaction"><a href="javascript:void(0);" class="remove_service_alert_session" data-alert_name="'+data[i].alert_name+'" data-alert_no="'+data[i].alert_number+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a></div></div></div>';
											   }
											   else
											   {
												 html_1='<div class="alertmore"><div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
							   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">ALERT NAME</span><p>'+data[i].alert_name+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">NOTIFICATION TYPE</span><p>'+notification+'</p></div><div class="col l1 s1 m1 gstaction"><a href="javascript:void(0);" class="remove_service_alert_session" data-alert_name="'+data[i].alert_name+'" data-alert_no="'+data[i].alert_number+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a></div></div></div>';

											   }
												 final_html = final_html + html_1;
												 $("#alert_info_array_m").html(final_html);
												// $(".two-i").html(data.length);
											}
											Materialize.toast('An alert has been set!', 2000,'green rounded');
										}
										else
										{
											html_1='<div class="gst-boxs add-gst">Add Alert Data</div>';
											$("#alert_info_array_m").html(html_1);
											Materialize.toast('Add alert information!', 2000,'green rounded');
										}
									}
									else
									{
										html_1='<div class="gst-boxs add-gst">Add Alert Data</div>';
										$("#alert_info_array_m").html(html_1);
										Materialize.toast('Error while processing!', 2000,'red rounded');
									}
									$("#add_alert_details_m").find("input[type=text], textarea").val("");
									$('.notif_type img').removeClass('selected_notification');
									$("#add-alert").modal('close');
							},
					});
				},
			});*/

		/*	$(document).ready( function() {
					$(document).on('click', '.remove_service_alert_session' ,function(){
						var alert_name = $(this).data('alert_name');
						var alert_no = $(this).data('alert_no');
						alert('2');
						//$('#remove_alert_data_m').modal('open');
						$('#remove_alert_data_m #remove_alert_name').val(alert_name);
						$('#remove_alert_data_m #remove_alert_no').val(alert_no);
					});

					$('.remove_alert_data_session').off().on('click',function(){

						var alert_name = $('#remove_alert_name').val();
						var alert_no = $('#remove_alert_no').val();

						$.ajax({

							url:base_url+'My_services/remove_alert_from_session',

							type:"post",

							data:{"alert_name":alert_name,"alert_no":alert_no},

							success:function(res){
								if(res!= false)
								{
									var data=JSON.parse(res);
									//console.log(data);
									var html_1 ='';
									var final_html ='';
									if(data != '')
									{
										for(var i=0;i<data.length;i++){
											var notification = '';
											   if(data[i].notification_type == '1') {
												notification = 'Remainder';
											   }else if(data[i].notification_type == '2') {
													notification = 'Message';
											   } else if(data[i].notification_type == '3'){
													notification = 'Email';
											   } else {
												   notification = '';
											   }
											if(i == 0)
										   {
											html_1='<div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
						   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">ALERT NAME</span><p>'+data[i].alert_name+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">NOTIFICATION TYPE</span><p>'+notification+'</p></div><div class="col l1 s1 m1 gstaction"><a href="javascript:void(0);" class="remove_service_alert_session" data-alert_name="'+data[i].alert_name+'" data-alert_no="'+data[i].alert_number+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a></div></div></div>';
										   }
										   else
										   {
											 html_1='<div class="alertmore"><div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
						   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">ALERT NAME</span><p>'+data[i].alert_name+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">NOTIFICATION TYPE</span><p>'+notification+'</p></div><div class="col l1 s1 m1 gstaction"><a href="javascript:void(0);" class="remove_service_alert_session" data-alert_name="'+data[i].alert_name+'" data-alert_no="'+data[i].alert_number+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a></div></div></div>';

										   }
											 final_html = final_html + html_1;
											 $("#alert_info_array_m").html(final_html);
											 //$(".two-i").html(data.length);
										}
										Materialize.toast('Alert information has been removed!', 2000,'green rounded');
									}
									else
									{
										html_1='<div class="gst-boxs add-gst">Add Alert Data</div>';
										$("#alert_info_array_m").html(html_1);
										Materialize.toast('Add alert information!', 2000,'green rounded');
									}
								}
								else
								{
									html_1='<div class="gst-boxs add-gst">Add Alert Data</div>';
									$("#alert_info_array_m").html(html_1);
									Materialize.toast('Alert information has been removed!', 2000,'green rounded');
								}
								$("#remove_alert_data").modal('close');
								},

							});
					});
			});*/




			$(document).ready( function() {
				$(document).on('click', '.edit_service_tax_m' ,function(){
				$('#edit_tax_modal_m').modal('open');
				var tax_id = $(this).data('tax_id');
				var service_id = $(this).data('service_id');
				var tax_name = $(this).data('tax_name');
				var tax_percentage = $(this).data('tax_percentage');

				$('#edit_tax_modal_m #edit_tax_id').val(tax_id);
				$('#edit_tax_modal_m #edit_tax_service_id').val(service_id);
				$('#edit_tax_modal_m #edit_tax_name').val(tax_name);
				$('#edit_tax_modal_m #edit_tax_perc').val(tax_percentage);
			});

			$("#edit_tax_service_m").submit(function(e){
				e.preventDefault();
			}).validate({
				rules:{

					edit_tax_name:{
						required:true,
					},
					edit_tax_perc:{
						required:true,
					},
				},
				messages:{

					edit_tax_name:{
						required:"TAX Name is required",
					},
					edit_tax_perc:{
						required:"TAX Percentage is required",
					},
				},
				submitHandler:function(form){
						var frm=$(form).serialize();
								$.ajax({
								url:base_url+'My_services/edit_tax_info',
								type:"POST",
								data:{'csrf_test_name':csrf_hash,"frm":frm,},
								success:function(res){
									//console.log(res);
									if(res!= false)
									{
										var data=JSON.parse(res);
										var html_1 ='';
										var final_html ='';
										if(data != '')
										{
											for(var i=0;i<data.length;i++){
											if(i == 0)
											   {
												html_1='<div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">NAME OF TAX</span><p>'+data[i].tax_name+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">TAX PERCENTAGE</span><p>'+data[i].tax_percentage+'</p></div><div class="col l1 s1 m1 gstaction"><a href="javascript:void(0);" class="remove_tax_edit_m" data-tax_id="'+data[i].id+'" data-service_id="'+data[i].service_id+'" data-tax_name="'+data[i].tax_name+'" data-tax_perc="'+data[i].tax_percentage+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a><a href="javascript:void(0);" class="edit_service_tax_m" data-tax_id="'+data[i].id+'" data-service_id="'+data[i].service_id+'" data-tax_name="'+data[i].tax_name+'" data-tax_percentage="'+data[i].tax_percentage+'""><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div>';
											   }
											   else
											   {
												 html_1='<div class="gstmore"><div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">NAME OF TAX</span><p>'+data[i].tax_name+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">TAX PERCENTAGE</span><p>'+data[i].tax_percentage+'</p></div><div class="col l1 s1 m1 gstaction"><a href="javascript:void(0);" class="remove_tax_edit_m" data-tax_id="'+data[i].id+'" data-service_id="'+data[i].service_id+'" data-tax_name="'+data[i].tax_name+'" data-tax_percentage="'+data[i].tax_percentage+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a><a href="javascript:void(0);" class="edit_service_tax_m" data-tax_id="'+data[i].id+'" data-service_id="'+data[i].service_id+'" data-tax_name="'+data[i].tax_name+'" data-tax_percentage="'+data[i].tax_percentage+'""><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div></div>';

											   }
												 final_html = final_html + html_1;
												 $("#tax_info_array_m").html(final_html);
												 $(".tot_other_tax").html(data.length);
											}
											Materialize.toast('Tax information updated successfully !', 2000,'green rounded');
										}
										else
										{
											Materialize.toast('Tax information updated successfully !', 2000,'green rounded');
											html_1='<div class="gst-boxs add-gst">Add TAX Data</div>';
											$("#tax_info_array_m").html(html_1);
											 //Materialize.toast('ADD Tax DATA!', 2000,'green rounded');
										}
									}
									else
									{
										Materialize.toast('Error while processing. Please try again', 2000,'red rounded');
									}
									$("#edit_tax_service_m").find("input[type=text], textarea").val("");
									$("#edit_tax_modal_m").modal('close');

							},

					});
				},
				});

				$(document).on('click', '.remove_tax_edit_m' ,function(){

				var tax_id = $(this).data('tax_id');
				var service_id = $(this).data('service_id');

				$('#edit_remove_tax_data_m').modal('open');
				$('#edit_remove_tax_data_m #remove_tax_id').val(tax_id);
				$('#edit_remove_tax_data_m #remove_service_id').val(service_id);

				});

				$('.edit_remove_tax_m').on('click',function(){

										var tax_id = $('#remove_tax_id').val();
										var service_id = $('#remove_service_id').val();

										$.ajax({

											url:base_url+'My_services/delete_tax_info',

											type:"post",

											data:{'csrf_test_name':csrf_hash,"tax_id":tax_id,"service_id":service_id},

											success:function(res){
													if(res!= false)
													{
														var data=JSON.parse(res);
														var html_1 ='';
														var final_html ='';
														if(data != '')
														{
															for(var i=0;i<data.length;i++){
															if(i == 0)
															   {
																html_1='<div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">NAME OF TAX</span><p>'+data[i].tax_name+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">TAX PERCENTAGE</span><p>'+data[i].tax_percentage+'</p></div><div class="col l1 s1 m1 gstaction"><a href="javascript:void(0);" class="remove_tax_edit_m" data-tax_id="'+data[i].id+'" data-service_id="'+data[i].service_id+'" data-tax_name="'+data[i].tax_name+'" data-tax_perc="'+data[i].tax_percentage+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a><a href="javascript:void(0);" class="edit_service_tax_m" data-tax_id="'+data[i].id+'" data-service_id="'+data[i].service_id+'" data-tax_name="'+data[i].tax_name+'" data-tax_percentage="'+data[i].tax_percentage+'""><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div>';
															   }
															   else
															   {
																 html_1='<div class="gstmore"><div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">NAME OF TAX</span><p>'+data[i].tax_name+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">TAX PERCENTAGE</span><p>'+data[i].tax_percentage+'</p></div><div class="col l1 s1 m1 gstaction"><a href="javascript:void(0);" class="remove_tax_edit_m" data-tax_id="'+data[i].id+'" data-service_id="'+data[i].service_id+'" data-tax_name="'+data[i].tax_name+'" data-tax_percentage="'+data[i].tax_percentage+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a><a href="javascript:void(0);" class="edit_service_tax_m" data-tax_id="'+data[i].id+'" data-service_id="'+data[i].service_id+'" data-tax_name="'+data[i].tax_name+'" data-tax_percentage="'+data[i].tax_percentage+'""><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div></div>';

															   }
																 final_html = final_html + html_1;
																 $("#tax_info_array_m").html(final_html);
																 $(".tot_other_tax").html(data.length);
															}
															Materialize.toast('TAX Info removed successfully !', 2000,'green rounded');
														}
														else
														{
															Materialize.toast('TAX Info removed successfully !', 2000,'green rounded');
															//html_1='<div class="gst-boxs add-gst">Add GSTIN Data</div>';
															//$("#tax_info_array_m").html(html_1);
															//Materialize.toast('ADD TAX DATA!', 2000,'green rounded');
														}
													}
													else
													{
														//html_1='<div class="gst-boxs add-gst">Add GSTIN Data</div>';
														//$("#tax_info_array_m").html(html_1);
														Materialize.toast('Error while processing!', 2000,'red rounded');
													}
												},

											});
									});
			});

			$('.open_add_tax_m').on('click',function(){
				var service = $(this).data('service_id');
				$('#edit_other-tax').modal('open');
				$('#edit_other-tax #editpage_tax_service_id').val(service);
			});

			$("#edit_add_other_tax_m").submit(function(e){
								e.preventDefault();
							}).validate({
								rules:{
									tax_name1: {
										required:true,
									},
									'tax_name[]':{
										required:true,
									},
									'tax_perc[]':{
										required:true,
									},
								},
								messages:{
									tax_name1: {
										required:"Tax Name is required",
									},
									'tax_name[]':{
										required:"Tax Name is required",
									},
									'tax_perc[]':{
										required:"Tax Percentage is required",
									},
								},
								submitHandler:function(form){

										var frm=$(form).serialize();
												$.ajax({
												url:base_url+'services/add_servicetax_info',
												type:"POST",
												data:{'csrf_test_name':csrf_hash,"frm":frm,},
												success:function(res){
													if(res!= false)
													{
														var data=JSON.parse(res);
														var html_1 ='';
														var final_html ='';
														if(data != '')
														{
															for(var i=0;i<data.length;i++){
															if(i == 0)
															   {
																html_1='<div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">NAME OF TAX</span><p>'+data[i].stax_name+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">TAX PERCENTAGE</span><p>'+data[i].stax_percentage+'</p></div><div class="col l1 s1 m1 gstaction"><a href="javascript:void(0);" class="remove_tax_edit_m" data-tax_id="'+data[i].stax_id+'" data-service_id="'+data[i].service_id+'" data-tax_name="'+data[i].tax_name+'" data-tax_perc="'+data[i].tax_percentage+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a><a href="javascript:void(0);" class="edit_service_tax_m" data-tax_id="'+data[i].id+'" data-service_id="'+data[i].service_id+'" data-tax_name="'+data[i].stax_name+'" data-tax_percentage="'+data[i].stax_percentage+'""><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div>';
															   }
															   else
															   {
																html_1='<div class="gstmore"><div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">NAME OF TAX</span><p>'+data[i].stax_name+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">TAX PERCENTAGE</span><p>'+data[i].stax_percentage+'</p></div><div class="col l1 s1 m1 gstaction"><a href="javascript:void(0);" class="remove_tax_edit_m" data-tax_id="'+data[i].stax_id+'" data-service_id="'+data[i].service_id+'" data-tax_name="'+data[i].tax_name+'" data-tax_percentage="'+data[i].tax_percentage+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a><a href="javascript:void(0);" class="edit_service_tax_m" data-tax_id="'+data[i].id+'" data-service_id="'+data[i].service_id+'" data-tax_name="'+data[i].stax_name+'" data-tax_percentage="'+data[i].stax_percentage+'""><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div></div>';

															   }
																 final_html = final_html + html_1;
																 $("#tax_info_array_m").html(final_html);
																 $(".tot_other_tax").html(data.length);
															}
															Materialize.toast('TAX Info inserted successfully!', 2000,'green rounded');
														}
														else
														{
															html_1='';
															$("#tax_info_array_m").html(html_1);
															$(".tot_other_tax").html(0);
															Materialize.toast('ADD TAX DATA', 2000,'green rounded');
														}
													}
													else
													{
														Materialize.toast('Error while processing. Please try again', 2000,'red rounded');
													}
													$("#edit_add_other_tax_m").find("input[type=text], textarea").val("");
													$("#edit_other-tax").modal('close');
											},
									});
								},
							});

							/*$('.edit_add_alert_info_m').on('click' ,function(){
										var service_id = $(this).data('service_id');

										$('#edit_add-alert_2').modal('open');
										$('#edit_add-alert_2 #add_alert_for_service').val(service_id);
							});
									$("#edit_add_alert_details_m").submit(function(e){
										e.preventDefault();
									}).validate({
										rules:{
											alert_name:{
												required:true,
											},
										},
										messages:{
											alert_name:{
												required:"Alert Name is required",
											},
										},
										submitHandler:function(form){

												var frm=$(form).serialize();
														$.ajax({
														url:base_url+'My_services/add_alert_info_2',
														type:"POST",
														data:{"frm":frm,},
														success:function(res){
															if(res!= false)
															{
																var data=JSON.parse(res);
																var html_1 ='';
																var final_html ='';
																if(data != '')
																{
																	for(var i=0;i<data.length;i++){
																		var notification = '';
																		   if(data[i].notification_type == '1') {
																			notification = 'Remainder';
																		   }else if(data[i].notification_type == '2') {
																				notification = 'Message';
																		   } else if(data[i].notification_type == '3'){
																				notification = 'Email';
																		   } else {
																			   notification = '';
																		   }
																	if(i == 0)
																	   {
																		html_1='<div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
													   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">ALERT NAME</span><p>'+data[i].alert_name+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">NOTIFICATION TYPE</span><p>'+notification+'</p></div><div class="col l1 s1 m1 gstaction"><a href="javascript:void(0);" class="remove_service_alert_edit_m" data-alert_id="'+data[i].id+'" data-service_id="'+data[i].service_id+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a><a href="javascript:void(0);" class="edit_alert_info_m" data-alert_id="'+data[i].id+'" data-service_id="'+data[i].service_id+'" data-alert_name="'+data[i].alert_name+'" data-alert_on="'+data[i].alert_on+'" data-set_condition="'+data[i].set_condition+'" data-alert_number="'+data[i].alert_number+'" data-alert_interval="'+data[i].alert_interval+'" data-notification_type="'+data[i].notification_type+'"><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div>';
																	   }
																	   else
																	   {
																		 html_1='<div class="alertmore"><div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
													   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">ALERT NAME</span><p>'+data[i].alert_name+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">NOTIFICATION TYPE</span><p>'+notification+'</p></div><div class="col l1 s1 m1 gstaction"><a href="javascript:void(0);" class="remove_service_alert_edit_m" data-alert_id="'+data[i].id+'" data-service_id="'+data[i].service_id+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a><a href="javascript:void(0);" class="edit_alert_info_m" data-alert_id="'+data[i].id+'" data-service_id="'+data[i].service_id+'" data-alert_name="'+data[i].alert_name+'" data-alert_on="'+data[i].alert_on+'" data-set_condition="'+data[i].set_condition+'" data-alert_number="'+data[i].alert_number+'" data-alert_interval="'+data[i].alert_interval+'" data-notification_type="'+data[i].notification_type+'"><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div>';

																	   }
																		 final_html = final_html + html_1;
																		 $("#alert_info_array_m").html(final_html);
																		// $(".two-i").html(data.length);
																	}
																	Materialize.toast('An alert has been set!', 2000,'green rounded');
																}
																else
																{
																	html_1='<div class="gst-boxs add-gst">Add Alert Data</div>';
																	$("#alert_info_array_m").html(html_1);
																	Materialize.toast('Add alert information!', 2000,'green rounded');
																}
															}
															else
															{
																html_1='<div class="gst-boxs add-gst">Add Alert Data</div>';
																$("#alert_info_array_m").html(html_1);
																Materialize.toast('Error while processing!', 2000,'red rounded');
															}
															$("#edit_add_alert_details_m").find("input[type=text], textarea").val("");
															$('.notif_type img').removeClass('selected_notification');
															$("#edit_add-alert_2").modal('close');
													},
											});
										},
									});
						*/
						$(document).ready( function() {
								$(document).on('click' ,'.remove_service_alert_edit_m', function(){
									var alert_id = $(this).data('alert_id');
									var service_id = $(this).data('service_id');

									$('#remove_alert_info_m').modal('open');
									$('#remove_alert_info_m #remove_alert_id').val(alert_id);
									$('#remove_alert_info_m #remove_alert_service_id').val(service_id);
								});

							$('.edit_remove_alert_m').on('click',function(){

										var alert_id = $('#remove_alert_id').val();
										var service_id = $('#remove_alert_service_id').val();

										$.ajax({

											url:base_url+'My_services/delete_alert_info',

											type:"post",

											data:{'csrf_test_name':csrf_hash,"alert_id":alert_id,"service_id":service_id},

											success:function(res){
													if(res!= false)
													{
														var data=JSON.parse(res);
														var html_1 ='';
														var final_html ='';
														if(data != '')
														{
															for(var i=0;i<data.length;i++){
																var notification = '';
																   if(data[i].notification_type == '1') {
																   	notification = 'Reminder';
																   }else if(data[i].notification_type == '2') {
																		notification = 'Message';
																   } else if(data[i].notification_type == '3'){
																		notification = 'Email';
																   } else {
																	   notification = '';
																   }
															if(i == 0)
															   {
																html_1='<div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">ALERT NAME</span><p>'+data[i].alert_name+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">NOTIFICATION TYPE</span><p>'+notification+'</p></div><div class="col l1 s1 m1 gstaction"><a href="javascript:void(0);" class="remove_service_alert_edit_m" data-alert_id="'+data[i].id+'" data-service_id="'+data[i].service_id+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a><a href="javascript:void(0);" class="edit_alert_info_m" data-alert_id="'+data[i].id+'" data-service_id="'+data[i].service_id+'" data-alert_name="'+data[i].alert_name+'" data-alert_on="'+data[i].alert_on+'" data-set_condition="'+data[i].set_condition+'" data-alert_number="'+data[i].alert_number+'" data-alert_interval="'+data[i].alert_interval+'" data-notification_type="'+data[i].notification_type+'"><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div>';
															   }
															   else
															   {
																 html_1='<div class="alertmore"><div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">ALERT NAME</span><p>'+data[i].alert_name+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">NOTIFICATION TYPE</span><p>'+notification+'</p></div><div class="col l1 s1 m1 gstaction"><a href="javascript:void(0);" class="remove_service_alert_edit_m" data-alert_id="'+data[i].id+'" data-service_id="'+data[i].service_id+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a><a href="javascript:void(0);" class="edit_alert_info_m" data-alert_id="'+data[i].id+'" data-service_id="'+data[i].service_id+'" data-alert_name="'+data[i].alert_name+'" data-alert_on="'+data[i].alert_on+'" data-set_condition="'+data[i].set_condition+'" data-alert_number="'+data[i].alert_number+'" data-alert_interval="'+data[i].alert_interval+'" data-notification_type="'+data[i].notification_type+'"><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div>';

															   }
																 final_html = final_html + html_1;
																 $("#alert_info_array_m").html(final_html);
																// $(".two-i").html(data.length);
															}
															Materialize.toast('Alert Info removed successfully !', 2000,'green rounded');
														}
														else
														{
															Materialize.toast('Alert Info removed successfully !', 2000,'green rounded');
															html_1='<div class="gst-boxs add-gst">Add Alert Data</div>';
															$("#alert_info_array_m").html(html_1);
															//Materialize.toast('Add alert information!', 2000,'green rounded');
														}
													}
													else
													{
														html_1='<div class="gst-boxs add-gst">Add Alert Data</div>';
														$("#alert_info_array_m").html(html_1);
														Materialize.toast('Error while processing!', 2000,'red rounded');
													}
												},

											});
									});
								});

								$(document).ready( function () {
									$(document).on('click', '.edit_alert_info_m' ,function(){
										var alert_id = $(this).data('alert_id');
										var service_id = $(this).data('service_id');
										var alert_name = $(this).data('alert_name');
										var alert_on = $(this).data('alert_on');
										var condition = $(this).data('set_condition');
										var alert_number = $(this).data('alert_number');
										var alert_interval = $(this).data('alert_interval');
										var notification_type = $(this).data('notification_type');

										$('#edit_alert').modal('open');
										$('#edit_alert #edit_alert_id').val(alert_id);
										$('#edit_alert #edit_alert_name').val(alert_name);
										$('#edit_alert #edit_alert_service_id').val(service_id);
										$('#edit_alert #edit_set_alert_on option[value="'+alert_on+'"]').attr('selected','selected');
										$('#edit_alert #edit_set_alert_on').material_select();

										$('#edit_alert #edit_condition option[value="'+condition+'"]').attr('selected','selected');
										$('#edit_alert #edit_condition').material_select();

										$('#edit_alert #edit_alert_number').val(alert_number);

										$('#edit_alert #edit_alert_interval option[value="'+alert_interval+'"]').attr('selected','selected');
										$('#edit_alert #edit_alert_interval').material_select();

										$('#edit_alert #edit_notification_type').val(notification_type);
										$(".edit_notif_type").each(function() {
											var selecetd_type = ($(this).data('notif_type'));
											if(notification_type == selecetd_type){
												$(this).find('img').addClass('selected_notification');

											}
										});
									});

									/*$("#edit_alert_details_m").submit(function(e){
											e.preventDefault();
										}).validate({
											rules:{
												edit_alert_name:{
													required:true,
												},
											},
											messages:{
												edit_alert_name:{
													required:"Alert Name is required",
												},
											},
											submitHandler:function(form){

													var frm=$(form).serialize();
															$.ajax({
															url:base_url+'My_services/update_alert_info',
															type:"POST",
															data:{"frm":frm,},
															success:function(res){
																if(res!= false)
																{
																	var data=JSON.parse(res);
																	var html_1 ='';
																	var final_html ='';
																	if(data != '')
																	{
																		for(var i=0;i<data.length;i++){
																			var notification = '';
																			   if(data[i].notification_type == '1') {
																				notification = 'Remainder';
																			   }else if(data[i].notification_type == '2') {
																					notification = 'Message';
																			   } else if(data[i].notification_type == '3'){
																					notification = 'Email';
																			   } else {
																				   notification = '';
																			   }
																		if(i == 0)
																		   {
																			html_1='<div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
														   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">ALERT NAME</span><p>'+data[i].alert_name+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">NOTIFICATION TYPE</span><p>'+notification+'</p></div><div class="col l1 s1 m1 gstaction"><a href="javascript:void(0);" class="remove_service_alert_edit_m" data-alert_id="'+data[i].id+'" data-service_id="'+data[i].service_id+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a><a href="javascript:void(0);" class="edit_alert_info_m" data-alert_id="'+data[i].id+'" data-service_id="'+data[i].service_id+'" data-alert_name="'+data[i].alert_name+'" data-alert_on="'+data[i].alert_on+'" data-set_condition="'+data[i].set_condition+'" data-alert_number="'+data[i].alert_number+'" data-alert_interval="'+data[i].alert_interval+'" data-notification_type="'+data[i].notification_type+'"><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div>';
																		   }
																		   else
																		   {
																			 html_1='<div class="alertmore"><div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+(i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">ALERT NAME</span><p>'+data[i].alert_name+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">NOTIFICATION TYPE</span><p>'+notification+'</p></div><div class="col l1 s1 m1 gstaction"><a href="javascript:void(0);" class="remove_service_alert_edit_m" data-alert_id="'+data[i].id+'" data-service_id="'+data[i].service_id+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a><a href="javascript:void(0);" class="edit_alert_info_m" data-alert_id="'+data[i].id+'" data-service_id="'+data[i].service_id+'" data-alert_name="'+data[i].alert_name+'" data-alert_on="'+data[i].alert_on+'" data-set_condition="'+data[i].set_condition+'" data-alert_number="'+data[i].alert_number+'" data-alert_interval="'+data[i].alert_interval+'" data-notification_type="'+data[i].notification_type+'"><i class="material-icons" style="color: #000;">mode_edit</i></a></div></div></div></div></div>';

																		   }
																			 final_html = final_html + html_1;
																			 $("#alert_info_array_m").html(final_html);
																			// $(".two-i").html(data.length);
																		}
																		Materialize.toast('Alert Information updated successfully', 2000,'green rounded');
																	}
																	else
																	{
																		html_1='<div class="gst-boxs add-gst">Add Alert Data</div>';
																		$("#alert_info_array_m").html(html_1);
																		Materialize.toast('Add alert information!', 2000,'green rounded');
																	}
																}
																else
																{
																	html_1='<div class="gst-boxs add-gst">Add Alert Data</div>';
																	$("#alert_info_array_m").html(html_1);
																	Materialize.toast('Error while processing!', 2000,'red rounded');
																}
																$("#edit_alert_details_m").find("input[type=text], textarea").val("");
																$("#edit_alert").modal('close');
														},
												});
											},
										});
									*/});
								$(document).off().on('click', '.print_service_2' ,function(){
									var service_id = $(this).data('service_id');
									var url = '&flag=' + 1;
									window.location.href = base_url+'My_services/view_service_2/' + service_id + '?' + url;
								});

								$(document).on('click', '.deactivate_service_2_m',function(){
										var service_id = $(this).data('service_id');
										$('#viewpage_deactive_service').modal('open');
										$('#viewpage_deactive_service #service_id').val(service_id);
								});

								$('.view_deactive_service_2').on('click',function(){

										var service = $('#service_id').val();

										$.ajax({

											url:base_url+'My_services/deactive_service_2',

											type:"POST",

											data:{'csrf_test_name':csrf_hash,"service":service,},

											success:function(res){
													window.location.href=base_url+'My_services/manage_services_for_manufacturing';
												},
											});
								});

								$(document).on('click', '.activate_service_2_m',function(){
										var service_id = $(this).data('service_id');
										$('#viewpage_active_service').modal('open');
										$('#viewpage_active_service #activate_service_id').val(service_id);
								});
								$('.view_deactive_service_m').on('click',function(){

										var service = $('#activate_service_id').val();

										$.ajax({

											url:base_url+'My_services/activate_service_2',

											type:"POST",

											data:{'csrf_test_name':csrf_hash,"service":service,},

											success:function(res){
													window.location.href=base_url+'My_services/manage_services_for_manufacturing';
												},
											});
								});





// ---------- Invoice Calculation ---------------






//-------------- Subscription & billing

$(document).ready( function() {
	$(document).on('click','.select_plan', function() {
		var plan = $(this).data('plan');
		var cost = $(this).data('cost');
			$.ajax({
				url:base_url+'My_subscription/select_plan_session',
				type:"POST",
				data:{'csrf_test_name':csrf_hash,"plan":plan,"cost":cost},
				success:function(res){
						window.location.href = base_url+'My_subscription/add_space_to_plan';
				},
			});
	});
	$('input[name="size-radio"]').click(function(){
			var selected_space = $(this).val();
			var space_cost = $(this).data('space_cost');
			$('#space_cost').val(space_cost);
	});

	$(document).on('click','.add_space', function() {
		if (!$("input[name='size-radio']:checked").val()) {
			Materialize.toast('Please Select Any one of these', 2000,'red rounded');
		} else {
			var selected_space = $('input[name=size-radio]:checked').val();
			var space_cost = $('#space_cost').val();
			$.ajax({
				url:base_url+'My_subscription/add_space_to_plan_session',
				type:"POST",
				data:{'csrf_test_name':csrf_hash,"selected_space":selected_space,"space_cost":space_cost},
				success:function(res){
						if(res == true) {
							window.location.href = base_url+'My_subscription/summary_section';
						} else {
							Materialize.toast('Error while processing. Please try again', 2000,'red rounded');
						}
				},
			});
		}
	});
});

// ---------- Bulk Deactivation for my activity history -----

$(document).ready( function () {

	var bulk_activities = [];
				$("input[id='activity_bulk']").on("click",function(){
				if($(this).is(':checked',true)) {
					$(".activities_bulk_action").prop('checked', true);
					$(".activities_bulk_action:checked").each(function() {
							bulk_activities.push($(this).val());
						});
						bulk_activities = bulk_activities.join(",");
						$('#deactive_multiple_activities').attr('data-multi_activity',bulk_activities);
					}
					else {
						$(".activities_bulk_action").prop('checked',false);
						bulk_activities = [];
						$('#deactive_multiple_activities').attr('data-multi_activity',0);
					}
				});

$(document).on('click', "#deactive_multiple_activities", function(e) {
		var activity_values = $(this).data('multi_activity');
		if(activity_values == '' || activity_values == '0')
		{
			Materialize.toast('Please select a record first', 2000,'red rounded');
		}
		else
		{
			$.ajax({
				url:base_url+'My_account/deactive_multiple',
				type:"POST",
				data:{'csrf_test_name':csrf_hash,"activity_values":activity_values,},
				success:function(res){
					location.reload();
					},
			});
		}
	});

	$(document).on('click', ".activate_records", function(e) {
		var module = $(this).data('module');
		$.ajax({
				url:base_url+'My_account/bulk_activate',
				type:"POST",
				data:{'csrf_test_name':csrf_hash,"module":module,},
				success:function(res){
					location.reload();
					},
			});
	});
});

$("#add_new_service_keywords").submit(function(e){
				e.preventDefault();
			}).validate({
				rules:{
					keyword_1:{
						required:true,
					},
				},
				messages:{
					keyword_1:{
						required:"Add At least one service keyword",
					},
				},
				submitHandler:function(form){
						var frm=$(form).serialize();
						if(csrf_hash===""){
							csrf_hash=csrf_hash;
						}
								 //$(form).ajaxSubmit();
								$.ajax({
								url:base_url+'profile/add_keywords',
								type:"POST",
								data:{'csrf_test_name':csrf_hash,"frm":frm,},
								success:function(res){
										//console.log(res);
										/* if(res != false)
										{
											Materialize.toast('Service Info have been set', 2000,'green rounded');
											$("#add_new_service_keywords").find("input[type=text], textarea").val("");
											$("#add-new-service").modal('close');
										} else {
											Materialize.toast('Error while processing...!!', 2000,'red rounded');
										} */
										if(res != false)
										{
											var data=JSON.parse(res);
											if(data['csrf_hash']){
												csrf_hash=data['csrf_hash'];
											}
											var html_1 ='';
											var final_html ='';
											//console.log(data);
											if(data != '')
											{
												var ser=data['bus_services_keywords'];
												//var bus_id=data[0].bus_id;
												//console.log(ser);
												var services=ser.split('|@|');
												for(var i=0;i<services.length;i++){
												if(i == 0)
												   {
													html_1='<div class="popup-append-box-cover"> <div class="gst-boxs add-gst"> <div class="col l1 s1 m1 sno"> <div class="row">'+ (i+1)+'</div> </div> <div class="col l11 s1 m1"> <!--<div class="col l5 s5 m5 gstnum"><span class="gstlabel">KEYWORD</span> <p>KEYWORD '+(i+1)+'</p> </div>--> <div class="col l11 s6 m6 gstplace"><span class="gstlabel">EXPERTISE</span> <p>'+services[i]+'</p> </div> <div class="col l1 s1 m1 gstaction"> <a class="cmp_remove_service modal-trigger" data-target="remove_cmp_service" data-service_id="'+services[i]+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a> </div> </div> </div> </div>';
												   }
												   else
												   {
													 html_1='<div class="popup-append-box popup-append-box-cover"> <div class="gst-boxs add-gst"> <div class="col l1 s1 m1 sno"> <div class="row">'+ (i+1)+'</div> </div> <div class="col l11 s1 m1"> <!--<div class="col l5 s5 m5 gstnum"><span class="gstlabel">KEYWORD</span> <p>KEYWORD '+(i+1)+'</p> </div>--> <div class="col l11 s6 m6 gstplace"><span class="gstlabel">EXPERTISE</span> <p>'+services[i]+'</p> </div> <div class="col l1 s1 m1 gstaction"> <a class="cmp_remove_service modal-trigger" data-target="remove_cmp_service" data-service_id="'+services[i]+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a> </div> </div> </div> </div>';

												   }
													 final_html = final_html + html_1;
													 $("#company_keywords_array_list").html(final_html);
													 $(".tot_service").html(services.length);
												}
												Materialize.toast('Expertise Info have been set.', 2000,'green rounded');
											}
											else
											{
												Materialize.toast('Expertise Info have been set.', 2000,'green rounded');
												html_1='<div class="gst-boxs add-gst">Add Expertise</div>';
												$("#company_keywords_array_list").html(html_1);
											}


										} else {
											Materialize.toast('Error while processing...!!', 2000,'red rounded');
										}
										$("#add-new-service").find("input[type=text], textarea").val("");
										$("#add-new-service").modal('close');

									},
					});
				},
			});

			$("#add_branch_info").submit(function(e){
				e.preventDefault();
			}).validate({
				rules:{
					branch_name:{
						required:true,
					},
					branch_city:{
						required:true,
					},
				},
				messages:{
					branch_name:{
						required:"Branch Name is required",
					},
					branch_city:{
						required:"Branch City is required",
					},
				},
				submitHandler:function(form){
						var frm=$(form).serialize();
								$.ajax({
								url:base_url+'profile/add_branch_info',
								type:"POST",
								data:{'csrf_test_name':csrf_hash,"frm":frm,},
								success:function(res){

									if(res!= false)
									{
										var data=JSON.parse(res);
										//console.log(data.length);
										var html_1 ='';
										var final_html_1 ='';
										if(data != '')
										{

											var i=0;
											for(i=0;i<data.length;i++){

											if(i == 0)
											   {
												html_1='<div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">BRANCH</span><p>'+data[i].cbranch_name+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">CITY</span><p>'+data[i].cbranch_city+'</p></div><div class="col l1 s1 m1 gstaction"><a class="remove_cmp_branch_model" data-branch="'+data[i].cbranch_name+'" data-city="'+data[i].cbranch_city+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a></div></div></div>';
											   }
											   else
											   {

												 html_1='<div class="branchmore"><div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">BRANCH</span><p>'+data[i].cbranch_name+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">CITY</span><p>'+data[i].cbranch_city+'</p></div><div class="col l1 s1 m1 gstaction"><a class="remove_cmp_branch_model" data-branch="'+data[i].cbranch_name+'" data-city="'+data[i].cbranch_city+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a></div></div></div></div>';

											   }

												 final_html_1 = final_html_1 + html_1;
												 $("#company_branch_array_list").html(final_html_1);
												 $(".tot_branch").html(data.length);
											}
											Materialize.toast('Branch Details have been set.', 2000,'green rounded');
										}
										else
										{
											Materialize.toast('Branch Details have been set.', 2000,'green rounded');
											html_1='<div class="gst-boxs add-gst">Add Branch Data</div>';
											$("#company_branch_array_list").html(html_1);
										}
									}
									else
									{
										Materialize.toast('Error while processing. Please try again', 2000,'red rounded');
									}
									$("#add_branch_info").find("input[type=text],input[type=password], textarea").val("");
									$("#new-branch").modal('close');

							},
							complete:function(res){
								$(document).ready( function() {
								$(document).on('click','.remove_cmp_branch_model',function(){

									var branch = $(this).data('branch');
									var city = $(this).data('city');
									$('#remove_company_branch_data').modal('open');
									$('#remove_company_branch_data #remove_branch').val(branch);
									$('#remove_company_branch_data #remove_city').val(city);
								});

									$('.remove_company_branch_data').off().on('click',function(){
										var branch = $('#remove_branch').val();
										var city = $('#remove_city').val();

										$.ajax({

											url:base_url+'profile/remove_branch_info',

											type:"post",

											data:{'csrf_test_name':csrf_hash,"branch":branch,"city":city},

											success:function(res){
												if(res!= false)
													{
														var data=JSON.parse(res);
														var html_1 ='';
														var final_html_1 ='';
														if(data != '')
														{
															$("#company_branch_array_list").html("");
															for(var i=0;i<data.length;i++){
															if(i == 0)
															   {
																html_1='<div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">BRANCH</span><p>'+data[i].cbranch_name+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">CITY</span><p>'+data[i].cbranch_city+'</p></div><div class="col l1 s1 m1 gstaction"><a class="remove_cmp_branch_model" data-branch="'+data[i].cbranch_name+'" data-city="'+data[i].cbranch_city+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a></div></div></div>';
															   }
															   else
															   {
																  html_1+='<div class="gstmore"><div class="gst-boxs add-gst"><div class="col l1 s1 m1 sno"><div class="row">'+
											   (i+1)+'</div></div><div class="col l11 s1 m1"><div class="col l5 s5 m5 gstnum"><span class="gstlabel">BRANCH</span><p>'+data[i].cbranch_name+'</p></div><div class="col l6 s6 m6 gstplace"><span class="gstlabel">CITY</span><p>'+data[i].cbranch_city+'</p></div><div class="col l1 s1 m1 gstaction"><a class="remove_cmp_branch_model" data-branch="'+data[i].cbranch_name+'" data-city="'+data[i].cbranch_city+'"><img src="'+base_url+'asset/images/delete.png" alt="" class="delete-icon"></a></div></div></div></div>';

															   }
															    final_html_1 = final_html_1 + html_1;
																 $("#company_branch_array_list").html(final_html_1);
																 $(".tot_branch").html(data.length);
															}
															Materialize.toast('Branch Details has been removed.', 2000,'green rounded');
														}
														else
														{
															Materialize.toast('Branch Details has been removed.', 2000,'green rounded');
															html_1='<div class="gst-boxs add-gst">Add Branch Data</div>';
															$("#company_branch_array_list").html(html_1);
															$(".tot_branch").html(0);
														}
													}
													else
													{
														html_1='<div class="gst-boxs add-gst">Add Branch Data</div>';
														$("#company_branch_array_list").html(html_1);
														$(".tot_branch").html(0);
													}
												},

											});
										});
									});
							},

					});
				},
			});

/////////////////////add client target ///////////////////

	$("#add_alertclient").submit(function(e){
								e.preventDefault();
							}).validate({
								rules:{
									alert_name: {
										required:true,
									},
									alert_reminder:{
										required:true,
									},
									alert_target:{
										required:true,
									},
								},
								messages:{
									alert_name: {
										required:"Alert Name is required",
									},
									alert_reminder:{
										required:"Alert Reminder is required",
									},
									alert_target:{
										required:"Alert Target is required",
									},
								},
								submitHandler:function(form){

										var frm=$(form).serialize();
												$.ajax({
												url:base_url+'sales/add_client_target',
												type:"POST",
												data:{'csrf_test_name':csrf_hash,"frm":frm,},
												success:function(res){
													if(res!= false)
													{
														var data=JSON.parse(res);



															Materialize.toast('Client alert set successfully!', 2000,'green rounded');


													}
													else
													{
														Materialize.toast('Error while processing. Please try again', 2000,'red rounded');
													}

													$("#clientalertmodal").modal('close');
											},
									});
								},
							});
