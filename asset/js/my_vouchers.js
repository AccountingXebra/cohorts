$(document).ready(function(){
 

   
	 $("#expense_type option").each(function() {
        if($('#db_expense_type').val() == $(this).val())
        {
          $(this).attr('selected','selected');
          $('#expense_type').material_select();
        }
    });
	$(".add_new_row").click(function(e){
		e.preventDefault();
	 	var count = parseInt($("#counter").val())+1;
		var businesstype = $(this).data('businesstype');
		if($('#customer_name').val() == '')
		{
			Materialize.toast('Please select client', 2000,'red rounded');
		}
		else
		{
			if(businesstype == '1')
			{
				var newTr = $('<tr id="row_' + count + '"></tr>');
				$.ajax({
					url:base_url+'My_vouchers/get_exp_type',
 					type:"POST",
					data:{'csrf_test_name':csrf_hash,},
					success:function(res){
						var result_opt=$.parseJSON(res);
						console.log(result_opt)
						var elemt = newTr.html('<input type="hidden" name="expense[]" value="' + count + '"><td class="meal over"><div class="scroll-hover-cover"><span class="hoveraction"><a href="#" class="delete_row" id="row_'+count+'" name="row_'+count+'"><img src="'+base_url+'asset/css/img/icons/delete.png"></a></span><input type="text" placeholder="Expence date" class="btn-date-bill bil-date bdatepicker" name="expense_date_'+ count + '" id="expense_date_'+ count + '"></td><td class="ali-lef"><select class="form-control expense_type" name="expense_type_'+ count + '" id="expense_type_'+ count + '">'+result_opt+'</select></td></td> <td><input placeholder="Expense Perticular" name="expense_perticular_'+ count +'" id="expense_perticular_'+ count +'" type="text" value="1500" class="bill-box voi-cust"></td><td><input placeholder="Expense Perticular" name="expense_amount_'+ count +'" id="expense_amount_'+ count +'" type="text" class="amnt bill-box voi-cust"></td><td><div class="clold"><input type="file" name="attachment_'+ count +'" id="attachment"  class="docup"></div><input type="hidden" name="voucher_old_image_'+ count +'" id="voucher_old_image_'+ count +'" value="" /></td>');

						$('#scrol-id tr.totalamount').before(elemt);
					 	$("#expense_type_"+count).material_select();
						$("#counter").val(count);
						count++;
					},
				});	
			}
			else
			{
				var newTr = $('<tr id="row_' + count + '"></tr>');
				$.ajax({
					url:base_url+'My_vouchers/get_exp_type',
					type:"POST",
					data:{'csrf_test_name':csrf_hash,},
					success:function(res){
						var result_opt=$.parseJSON(res);
						console.log(result_opt)
						var elemt = newTr.html('<input type="hidden" name="expense[]" value="' + count + '"><td class="meal over"><div class="scroll-hover-cover"><span class="hoveraction"><a href="#" class="delete_row" id="row_'+count+'" name="row_'+count+'"><img src="'+base_url+'asset/css/img/icons/delete.png"></a></span><input type="text" placeholder="Expence date" class="btn-date-bill bil-date bdatepicker" name="expense_date_'+ count + '" id="expense_date_'+ count + '"></td><td class="ali-lef"><select class="form-control expense_type" name="expense_type_'+ count + '" id="expense_type_'+ count + '">'+result_opt+'</select></td></td> <td><input placeholder="Expense Perticular" name="expense_perticular_'+ count +'" id="expense_perticular_'+ count +'" type="text" value="1500" class="bill-box voi-cust"></td><td><input placeholder="Expense Perticular" name="expense_amount_'+ count +'" id="expense_amount_'+ count +'" type="text" class="amnt bill-box voi-cust"></td><td><div class="clold"><input type="file" name="attachment_'+ count +'" id="attachment"  class="docup"></div><input type="hidden" name="voucher_old_image_'+ count +'" id="voucher_old_image_'+ count +'" value="" /></td>');

							  $('#scrol-id tr.totalamount').before(elemt);
							$("#expense_type_"+count).material_select();
						

						  $("#counter").val(count);
							 count++;
						},
				});		
			}
		
		}
	});
 
	$(document).on('change', '.amnt', function() {
		   var row_id=$(this).attr('id').split("-");
			calculate_total_exp(row_id[1]);
		});

	var last_res1;
	function calculate_total_exp(id)
	{
		var final_amount=0;
		var row = $('#row_'+id);
		$('.amnt').each(function(){
			if($(this).val()==''){
				$(this).val(0);
			}
			final_amount=parseFloat(final_amount)+parseFloat($(this).val());
		});
		$("#totalamount").val(parseFloat(final_amount));
		$("#total").val(parseFloat(final_amount));
	}

	
	
	$(document).on('click', '.delete_row',  function() {
                var id=$(this).attr('id');
                $(this).closest("tr").remove();
                calculate_total_exp(id);
	});
	$('#customer_name').on('change', function () {
               $.ajax({
                        'url':base_url+'My_vouchers/generate_expense_voucher_no',
                        type:"POST",
                        data:{
								'csrf_test_name':csrf_hash,
                                'company_id':$('#customer_name').val(),
                        },
                        success:function(res){
                                $("#expense_voucher_no").val(res);
                        },
                });
        });
});