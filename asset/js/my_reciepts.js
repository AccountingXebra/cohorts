$(document).ready(function(){
	$('#rc_customer_name').change(function(){
	var customer_id=$(this).val();
	var company_id=$("#header_company_profiles").val();
	$.ajax({
				url:base_url+'My_receipts/get_customer_gst_info',
				type:"POST",
				data:{'csrf_test_name':csrf_hash,"customer_id":customer_id,},
				success:function(res){
					 var res = $.parseJSON(res);
					$("#rc_customer_gstin").html(res[0]);
					$('#rc_customer_gstin').material_select();
					$("#rc_customer_place").html(res[1]);
					$('#rc_customer_place').material_select();
					$('.billing_address span.bill_add').html(res[2]);
					$('.shipping_address span.ship_add').html(res[3]);
				},
				complete:function(res){
							$.ajax({
								url:base_url+'My_receipts/get_gst_value',
								type:"POST",
								data:{'csrf_test_name':csrf_hash,},
								success:function(res){
									var data = JSON.parse(res);
									$("#tot_gst").val(data.gst);
									var tot_gst = data.gst;
										$.ajax({
											url:base_url+'My_receipts/fetch_gst',
											type:"POST",
											data:{'csrf_test_name':csrf_hash,"customer_id":customer_id,"company_id":company_id,},
											success:function(res1){
												res1=$.parseJSON(res1);
												var half_gst = parseFloat($("#tot_gst").val())/2;
												if(res1==true){
													$("#igst").val("0.00");
													$("#sgst").val(half_gst);
													$("#cgst").val(half_gst);
													$(".rc_branch_igst").val("0.00");
													$(".rc_branch_sgst").val(half_gst);
													$(".rc_branch_cgst").val(half_gst);
												}
												else
												{
													$("#igst").val(parseFloat($('#tot_gst').val()));
													$("#sgst").val("0.00");
													$("#cgst").val("0.00");
													$(".rc_branch_igst").val(parseFloat($('#tot_gst').val()));
													$(".rc_branch_sgst").val("0.00");
													$(".rc_branch_cgst").val("0.00");
												}
											},
											complete:function(res1){
												$('input[name="rc_array[]"]').each(function(){
													calculate_total($(this).val());
												});
											},
										});
								},
							});
						},
			});
	  });
	
	$('#rc_customer_gstin').on('change' ,function(){
		var customer_id = $(this).val();
		$('#rc_customer_place option[value="'+customer_id+'"]').attr('selected','selected');
		$('#rc_customer_place').material_select();
	});
	
	$(".edit_rc_billing_add").on("click",function(){
		if($('#rc_customer_name').val() == ''){
			Materialize.toast('Please Select Customer First!!', 2000,'red rounded');
		} else {
				$('#rc_edit_billing_address').modal('open');
				$.ajax({
					url:base_url+'My_invoices/get_all_countries',
					type:"POST",
					data:{'csrf_test_name':csrf_hash,},
					success:function(res){
						$("#rc_edit_billing_address #new_rc_country").html(res);
						$("#rc_edit_billing_address #new_rc_country").parents('.input-field').addClass('label-active');
						$('#rc_edit_billing_address #new_rc_country').material_select();
						},
					});
					$("#new_rc_country").on("change",function(){
						var country_id = $(this).val();
						$.ajax({
							url:base_url+'Company_profile/get_states',
							type:"POST",
							data:{'csrf_test_name':csrf_hash,'country_id':country_id},
							success:function(res){
								$("#new_rc_state").html(res);
								$("#new_rc_state").parents('.input-field').addClass('label-active');
								$('#new_rc_state').material_select();
							},
						});
					});
					$("#new_rc_state").on("change",function(){
						var state_id = $(this).val();
						$.ajax({
							url:base_url+'Company_profile/get_cities',
							type:"POST",
							data:{'csrf_test_name':csrf_hash,'state_id':state_id},
							success:function(res){
							 $("#new_rc_city").html(res);
							 $("#new_rc_city").parents('.input-field').addClass('label-active');
							 $('#new_rc_city').material_select();
							},
						});
					});
			}
		});
		
		$("#edit_rc_billing_address_form").submit(function(e){
				e.preventDefault();
			}).validate({
				rules:{
					new_rc_billing_address:{
						required:true
					},
				},
				messages:{
					new_rc_billing_address:{
						required:"Billing Address is required",
					},
				},
				submitHandler:function(form){
						var frm=$(form).serialize();
								$.ajax({
								url:base_url+'My_receipts/update_billing_address',
								type:"POST",
								data:{'csrf_test_name':csrf_hash,"frm":frm,},
								success:function(res){
									$('.billing_address span.bill_add').html(res);
									$('#rc_edit_billing_address').modal('close');
									Materialize.toast('New billing address set', 2000,'green rounded');
							},						
					});
				},
			});
			
			$(".edit_rc_shipping_add").on("click",function(){
			if($('#rc_customer_name').val() == ''){
				Materialize.toast('Please Select Customer First!!', 2000,'red rounded');
			} else {
				$('#rc_edit_shipping_address').modal('open');
				$.ajax({
					url:base_url+'My_invoices/get_all_countries',
					type:"POST",
					data:{'csrf_test_name':csrf_hash,},
					success:function(res){
						$("#rc_edit_shipping_address #new_rc_shipping_country").html(res);
						$("#rc_edit_shipping_address #new_rc_shipping_country").parents('.input-field').addClass('label-active');
						$('#rc_edit_shipping_address #new_rc_shipping_country').material_select();
						},
					});
					$("#new_rc_shipping_country").on("change",function(){
						var country_id = $(this).val();
						$.ajax({
							url:base_url+'Company_profile/get_states',
							type:"POST",
							data:{'csrf_test_name':csrf_hash,'country_id':country_id},
							success:function(res){
								$("#new_rc_shipping_state").html(res);
								$("#new_rc_shipping_state").parents('.input-field').addClass('label-active');
								$('#new_rc_shipping_state').material_select();
							},
						});
					});
					$("#new_rc_shipping_state").on("change",function(){
						var state_id = $(this).val();
						$.ajax({
							url:base_url+'Company_profile/get_cities',
							type:"POST",
							data:{'csrf_test_name':csrf_hash,'state_id':state_id},
							success:function(res){
							 $("#new_rc_shipping_city").html(res);
							 $("#new_rc_shipping_city").parents('.input-field').addClass('label-active');
							 $('#new_rc_shipping_city').material_select();
							},
						});
					});
				}
			});
		
			$("#edit_rc_shipping_address_form").submit(function(e){
				e.preventDefault();
			}).validate({
				rules:{
					new_rc_shipping_address:{
						required:true
					},
				},
				messages:{
					new_rc_shipping_address:{
						required:"Shipping Address is required",
					},
				},
				submitHandler:function(form){
						var frm=$(form).serialize();
								$.ajax({
								url:base_url+'My_receipts/update_shipping_address',
								type:"POST",
								data:{'csrf_test_name':csrf_hash,"frm":frm,},
								success:function(res){
									$('.shipping_address span.ship_add').html(res);
									$('#rc_edit_shipping_address').modal('close');
									Materialize.toast('New Shipping Address have been set!!', 2000,'green rounded');
							},						
					});
				},
			});
		
		$('#same-as-bill-rc').click(function(){
				if($(this).is(':checked')){
					if($('#rc_customer_name').val() == ''){
					Materialize.toast('Please Select Customer First!!', 2000,'red rounded');
				} else{
					$(".edit_rc_billing_add").hide();
						$.ajax({
									url:base_url+'My_receipts/dc_bill_ship_same',
									type:"POST",
									data:{'csrf_test_name':csrf_hash,},
									success:function(res){
										$('.shipping_address span.ship_add').html(res);
									},						
							});
					}
				}
				else{
					$(".edit_rc_billing_add").show();
				}
	});
	
			$("#add_receipt").validate({
				rules:{
					rc_no:{
						required:true,
					},
					rc_date:{
						required:true,
					},
				},
				messages:{
					rc_no:{
						required:"Advance Receipt Number is required",
					},
					rc_date:{
						required:"Advance Receipt Date is required",
					},
				},
			});
						
 	var count = 1;
	$(".add_new_row").click(function(e){
		e.preventDefault();
		var businesstype = $(this).data('businesstype');
		//alert(businesstype);
		if($('#rc_customer_name').val() == '')
		{
			Materialize.toast('Please select client', 2000,'red rounded');
		}
		else
		{
			if(businesstype != '1')
			{
				var newTr = $('<tr id="row_' + count + '"></tr>');
				$.ajax({
					url:base_url+'My_receipts/get_services',
					type:"POST",
					data:{'csrf_test_name':csrf_hash,},
					success:function(res){
						var result_opt=$.parseJSON(res);
						var elemt = newTr.html('<input type="hidden" name="rc_array[]" value="' + count + '"><td class="meal over"><div class="scroll-hover-cover"><span class="hoveraction"><a href="#" class="delete_rc_row" id="'+count+'" name="'+count+'"><img src="'+base_url+'asset/css/img/icons/delete.png"></a></span><select class="select-wrapper tabl-select border-radius-6 btn-dropdown-select select-like-dropdown create-bil-suyp sales-in-drop rc_service" id="service_'+count+'" name="service_'+count+'"><option value="">Select Service</option></select></td><td class="ali-lef"><input type="text"  required="required" autofocus="autofocus" class="form-control" id="particular_' + count + '" value="" name="particular_' + count + '"></td><td><input type="text"  required="required" autofocus="autofocus" class="form-control" id="hsn_sac_' + count + '" value="" name="hsn_sac_' + count + '" readonly></td><td><input type="text"  required="required" autofocus="autofocus" class="form-control rc_qty" id="qty_' + count + '" name="qty_' + count + '" value="1"></td><td><select class="select-wrapper tabl-select border-radius-6 btn-dropdown-select select-like-dropdown create-bil-suyp sales-in-drop rc_units" id="unit_'+count+'" name="unit_'+count+'"><option value="">Select Unit</option></select></td><td><input type="text"  required="required" autofocus="autofocus" class="form-control rc_rate" id="rate_' + count + '" name="rate_' + count + '" value="0"></td><td class="meal"><input type="text" required="required" autofocus="autofocus" class="form-control rc_discount" id="discount_' + count + '" value="0.00" name="discount_' + count + '"><input type="hidden" required="required" autofocus="autofocus" class="form-control rc_disc_amt" id="discount_amt_' + count + '" value="0.00" name="discount_amt_' + count + '"></td><td class="meal"><input type="text" autofocus="autofocus" class="rc_igst_amt form-control" id="igst_amt_' + count + '" name="igst_amt_' + count + '" value="0.00" readonly ></td><td class="meal"><input type="text" autofocus="autofocus" class="rc_branch_igst form-control" id="igst_' + count + '" name="igst_' + count + '" value="'+ $("#igst").val() +'" readonly></td><td class="meal"><input type="text" autofocus="autofocus" class="rc_cgst_amt form-control" id="cgst_amt_' + count + '" value="0.00" name="cgst_amt_' + count + '" readonly ></td><td class="meal"><input type="text" autofocus="autofocus" class="rc_branch_cgst form-control" id="cgst_' + count + '" name="cgst_' + count + '"  value="'+ $("#cgst").val() +'" readonly></td><td class="meal"><input type="text" autofocus="autofocus" class="rc_sgst_amt form-control" id="sgst_amt_' + count + '" value="0.00" name="sgst_amt_' + count + '" readonly ></td><td class="meal"><input type="text" autofocus="autofocus" class="rc_branch_sgst form-control" id="sgst_' + count + '" name="sgst_' + count + '" value="'+ $("#sgst").val() +'" readonly></td><td><input type="text" autofocus="autofocus" class="form-control rc_cess" id="cess_' + count + '" value="0" name="cess_' + count + '"><input type="hidden" autofocus="autofocus" class="rc_cess_amt form-control" id="cess_amt_' + count + '" value="" name="cess_amt_' + count + '" readonly ></td><td class="last-btr"></td><td class="fix num-s meal credit-ntns"><input type="text" autofocus="autofocus" class="form-control rc_amnt" id="amount_' + count + '" value="0" name="amount_' + count + '" readonly ><input type="hidden" class="rc_sub_amt_class" id="sub_tot_amount_' + count + '" value="0" name="sub_tot_amount_' + count + '" readonly ></td>');

						$('#scrol-id tr.totalamount').before(elemt);
					 	$("#service_"+count).html(result_opt[0]);
						$("#unit_"+count).html(result_opt[1]);
						$("#unit_"+count).material_select();
						$('select').not('.disabled').material_select();
						$("#counter").val(count);
						count++;
						},
				});	
			}
			else
			{
				var newTr = $('<tr id="row_' + count + '"></tr>');
				$.ajax({
					url:base_url+'My_invoices/get_services',
					type:"POST",
					data:{'csrf_test_name':csrf_hash,},
					success:function(res){
						var result_opt=$.parseJSON(res);
						var elemt = newTr.html('<input type="hidden" name="rc_array[]" value="' + count + '"><td class="meal over"><div class="scroll-hover-cover"><span class="hoveraction"><a href="#" class="delete_rc_row" id="'+count+'" name="'+count+'"><img src="'+base_url+'asset/css/img/icons/delete.png"></a></span><select class="select-wrapper tabl-select border-radius-6 btn-dropdown-select select-like-dropdown create-bil-suyp sales-in-drop rc_service" id="service_'+count+'" name="service_'+count+'"><option value="">Select Service</option></select></td><td class="ali-lef"><input type="text"  required="required" autofocus="autofocus" class="form-control" id="particular_' + count + '" value="" name="particular_' + count + '"></td><td><input type="text"  required="required" autofocus="autofocus" class="form-control" id="hsn_sac_' + count + '" value="" name="hsn_sac_' + count + '" readonly></td><td><input type="text"  required="required" autofocus="autofocus" class="form-control rc_qty" id="qty_' + count + '" name="qty_' + count + '" value="1"></td><td><input type="text"  required="required" autofocus="autofocus" class="form-control rc_rate" id="rate_' + count + '" name="rate_' + count + '" value="0"></td><td class="meal"><input type="text" required="required" autofocus="autofocus" class="form-control rc_discount" id="discount_' + count + '" value="0.00" name="discount_' + count + '"><input type="hidden" required="required" autofocus="autofocus" class="form-control rc_disc_amt" id="discount_amt_' + count + '" value="0.00" name="discount_amt_' + count + '"></td><td class="meal"><input type="text" autofocus="autofocus" class="rc_igst_amt form-control" id="igst_amt_' + count + '" name="igst_amt_' + count + '" value="0.00" readonly ></td><td class="meal"><input type="text" autofocus="autofocus" class="rc_branch_igst form-control" id="igst_' + count + '" name="igst_' + count + '" value="'+ $("#igst").val() +'" readonly></td><td class="meal"><input type="text" autofocus="autofocus" class="rc_cgst_amt form-control" id="cgst_amt_' + count + '" value="0.00" name="cgst_amt_' + count + '" readonly ></td><td class="meal"><input type="text" autofocus="autofocus" class="rc_branch_cgst form-control" id="cgst_' + count + '" name="cgst_' + count + '"  value="'+ $("#cgst").val() +'" readonly></td><td class="meal"><input type="text" autofocus="autofocus" class="rc_sgst_amt form-control" id="sgst_amt_' + count + '" value="0.00" name="sgst_amt_' + count + '" readonly ></td><td class="meal"><input type="text" autofocus="autofocus" class="rc_branch_sgst form-control" id="sgst_' + count + '" name="sgst_' + count + '" value="'+ $("#sgst").val() +'" readonly></td><td><input type="text" autofocus="autofocus" class="form-control rc_cess" id="cess_' + count + '" value="0" name="cess_' + count + '"><input type="hidden" autofocus="autofocus" class="rc_cess_amt form-control" id="cess_amt_' + count + '" value="" name="cess_amt_' + count + '" readonly ></td><td class="last-btr"></td><td class="fix num-s meal credit-ntns"><input type="text" autofocus="autofocus" class="form-control rc_amnt" id="amount_' + count + '" value="0" name="amount_' + count + '" readonly ><input type="hidden" class="rc_sub_amt_class" id="sub_tot_amount_' + count + '" value="0" name="sub_tot_amount_' + count + '" readonly ></td>');
													
							$('#scrol-id tr.totalamount').before(elemt);
							$("#service_"+count).html(result_opt[0]);
							//$("#unit_"+count).html(result_opt[1]);
							//$("#unit_"+count).material_select();
							$('select').not('.disabled').material_select();
							$("#counter").val(count);
							count++;
						},
				});		
			}
		}
	});
	
$(document).ready( function() {							 
	 $(document).on('click','.delete_rc_row',function(){							 
		var id=$(this).attr('id');
		calculate_total(id);
		$(this).closest("tr").remove();
	});
														
	$(document).on('change', '.rc_qty,.rc_rate,.rc_cess,.rc_discount', function () {
		var row_id=$(this).attr('id').split("_");	 
		calculate_total(row_id[1]);
	});
});

	$(document).off().on('change', 'select.rc_service',  function () {
		if($(this).val() == 'add_new_service') {
			$.ajax({
				url:base_url+'My_invoices/get_units',
				type:"POST",
				data:{'csrf_test_name':csrf_hash,},
				success:function(res){
						$('#invoice_add_new_service').modal('open');
						$("#invoice_unit").html(res);
						$("#invoice_unit").parents('.input-field').addClass('label-active');
						$('#invoice_unit').material_select();
					},
			});	
		} 
		else 
		{
			var row_id=$(this).attr('id').split("_");
			var service_id = $(this).val();
			calculate_total(row_id[1]);
		}
	});

	
	function calculate_total(id)
	{
	var tot = 0;
		var row = $('#row_'+id);
		var quantity = row.find('#qty_'+id).val();
		var rate = row.find('#rate_'+id).val();
		var service = row.find('#service_'+id).val();
		var discount = row.find('#discount_'+id).val();
		var cess = row.find('#cess_'+id).val();
		var igst = row.find('#igst_'+id).val();
		var cgst = row.find('#cgst_'+id).val();
		var sgst = row.find('#sgst_'+id).val();
		var customer = $("#rc_customer_name").val();

		$.ajax({
				url:base_url+'My_receipts/get_service_info',
				type:"POST",
				data:{'csrf_test_name':csrf_hash,"service_id":service},
				success:function(res){
					var data=JSON.parse(res);
						$("#hsn_sac_"+id).val(data[0].hsn_sac_no);	
						$('#'+'unit_'+id + ' option[value="'+data[0].unit+'"]').attr('selected','selected');
						 $("#unit_"+id).material_select();
						var tot=parseFloat(quantity) * parseFloat(rate);
						$("#sub_tot_amount_"+id).val(tot);
						var tot_gst=parseFloat(igst)+parseFloat(sgst)+parseFloat(cgst);
						var applicable_cess=(parseFloat(tot)*parseFloat(cess))/100;
						$("#cess_amt_"+id).val(applicable_cess);
						var applicable_disc=(parseFloat(tot)*parseFloat(discount))/100;
						$("#discount_amt_"+id).val(applicable_disc);
						$('#igst_amt_'+id).val((parseFloat(tot)*parseFloat(igst))/100);
						$('#cgst_amt_'+id).val((parseFloat(tot)*parseFloat(cgst))/100);
						$('#sgst_amt_'+id).val((parseFloat(tot)*parseFloat(sgst))/100);
						var applicable_gst=(parseFloat(tot) * parseFloat(tot_gst))/100;
						//tot=(tot+applicable_gst+applicable_cess)-applicable_disc;
						
						 
						if ($('#reversecharge').length>0){
						tot=(tot)-applicable_disc;
					}
					else{
						tot=(tot+applicable_gst+applicable_cess)-applicable_disc;
					}
					$("#amount_"+id).val(parseFloat(tot));
					total=0;
					$('.rc_sub_amt_class').each(function(){
						if($(this).val()==''){
							$(this).val(0);
						}
						total=parseFloat(total)+parseFloat($(this).val());
					});
					$("#total").val(parseFloat(total));
					cess_amt=0;
					$('.rc_cess_amt').each(function(){
						if($(this).val()==''){
							$(this).val(0);
						}
						cess_amt=parseFloat(cess_amt)+parseFloat($(this).val());
					});
					$(".cess_tot_amt").val(parseFloat(cess_amt));
					discount_amt=0;
						$('.rc_disc_amt').each(function(){
							if($(this).val()==''){
								$(this).val(0);
							}
							discount_amt=parseFloat(discount_amt)+parseFloat($(this).val());
							console.log(discount_amt);
						});
						$(".disc_tot_amt").val(parseFloat(discount_amt));
						igst_tot_amt=0;
						$('.rc_igst_amt').each(function(){
							if($(this).val()==''){
								$(this).val(0);
							}
							igst_tot_amt=parseFloat(igst_tot_amt)+parseFloat($(this).val());
						});
						$(".igst_tot_amt").val(parseFloat(igst_tot_amt));
						cgst_tot_amt=0;
						$('.rc_cgst_amt').each(function(){
							if($(this).val()==''){
								$(this).val(0);
							}
							cgst_tot_amt=parseFloat(cgst_tot_amt)+parseFloat($(this).val());
						});
						$(".cgst_tot_amt").val(parseFloat(cgst_tot_amt));
						sgst_tot_amt=0;
						$('.rc_sgst_amt').each(function(){
							if($(this).val()==''){
								$(this).val(0);
							}
							sgst_tot_amt=parseFloat(sgst_tot_amt)+parseFloat($(this).val());
						});
						$(".sgst_tot_amt").val(parseFloat(sgst_tot_amt));
						final_amount=0;
						$('.rc_amnt').each(function(){
							if($(this).val()==''){
								$(this).val(0);
							}
							final_amount=parseFloat(final_amount)+parseFloat($(this).val());
						});
						$("#totalamount").val(parseFloat(final_amount));
						$(".grandtotalamount").html(parseFloat(final_amount));
				
						if ($('#reversecharge').length>0){	reverse_charge=parseFloat($("input[name='rev_igst_tot_amt']").val())+parseFloat($("input[name='rev_cgst_tot_amt']").val())+parseFloat($("input[name='rev_sgst_tot_amt']").val())+parseFloat($("input[name='rev_cess_tot_amt']").val());

								$(".rev_charge_amt").val(reverse_charge);

							}
					},
			});

	}
});